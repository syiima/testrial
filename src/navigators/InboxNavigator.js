import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Inbox from '../screens/Inbox'
import InboxAnnouncements from '../screens/InboxAnnouncements';
import InboxUpdate from '../screens/InboxUpdate';
import InboxNotifications from '../screens/InboxNotifications';


const InboxNavigator = createStackNavigator({
    Inbox:{screen: Inbox},
    InboxAnnouncements: {screen: InboxAnnouncements},
    InboxNotifications: {screen : InboxNotifications},
    InboxUpdate:{screen: InboxUpdate},
  },{
    headerMode:'none',
    initialRouteName: 'Inbox',
    navigationOptions: {
      gesturesEnabled: false
    }
  })
  

export default createAppContainer(InboxNavigator) 
  