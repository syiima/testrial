import React from 'react';
import { View, Text } from 'react-native';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import firebase from '@react-native-firebase/app';
import { inject, observer } from 'mobx-react';
import JailMonkey from 'jail-monkey'

import {X} from "../util/config"



import LoginRegNavigator from './LoginRegNavigator';
import BottomNavigator from './BottomNavigator';
import SplashScreen from '../screens/SplashScreen'
import JailBreak from "../screens/JailBreak";
import Update from "../screens/NewUpdate"
import Onboarding from '../screens/Onboarding';

class MainAppNavigator extends React.Component{

  componentDidMount(){
    console.warn("this is BETA", this.props.mobx_auth.check);

      this.props.mobx_loader.setLoading(true);
      this.checkRoot()
    }
  
  componentWillUnmount(){
      firebase.database().ref('consts/version_code').off()
  }

  checkRoot(){
      let that = this
      // is this device JailBroken on iOS/Android?
      if(JailMonkey.isJailBroken() === true){
          this.props.mobx_auth.setCheck(5);
      } else {
          setTimeout(() => {
          this.auth()
          this.versionChecker()
          }, 1200);   
      }
  }

  versionChecker = () => {
      let that = this;
      let APP_VERSION = this.props.mobx_config.constV;
      firebase.database().ref('consts/version_code').on('value', function(snapshot){
        if(snapshot.exists()){
          if(APP_VERSION < snapshot.val()){
            that.promptVersionChanger();
          }
        } else {
            //
        }
      })
  }

  promptVersionChanger = () => {
      let that = this;
      that.props.mobx_auth.setCheck(4);
      that.props.mobx_loader.setLoading(false);
  }

  auth(){
      let that = this;
      let ONB 
  
      firebase.database().ref(`consts`).once('value', (snp)=> {
        if(snp.exists){
          let X = snp.val().plate 
          that.props.mobx_config.setDB(X)

          ONB = snp.val().onboarding
        }
      }).then(() => {
          firebase.auth().onAuthStateChanged((user) => {
              if (user) {
                that.props.mobx_auth.setFUID(user.uid);
                let DATA;
                firebase.database().ref('users/' + user.uid).once('value', (snapshot) => {
                    DATA = snapshot.val();
                    // console.warn('hoii', DATA);
                    that.props.mobx_auth.setProfile(DATA);
                    //console.log(this.props.mobx_auth)
                    that.props.mobx_auth.setEMAIL(DATA.email);
                    that.props.mobx_auth.setNAME(DATA.name);
                    that.props.mobx_auth.setPHONE(DATA.Phone);
    
    
                    if(DATA.onboarding === undefined || DATA.onboarding < ONB){
                      firebase.database().ref('users/' + user.uid + '/onboarding/').set(ONB)
                      that.props.mobx_auth.setCheck(2);
                    } else {
                        that.props.mobx_auth.setCheck(3);
                    }
                })
               
                that.props.mobx_loader.setLoading(false);
                // that.bookListener()
              } else {
                that.props.mobx_auth.setCheck(1);
                that.props.mobx_loader.setLoading(false);
              }
          })
      })
  
     
  }
  

    
  render(){
      console.warn("this is BETA", this.props.mobx_auth.check);

      if(X.config === 0) {
          this.props.mobx_config.setConfig(0)
          // console.warn("this is BETA");
      }
      if(X.config === 1) {
          this.props.mobx_config.setConfig(1)
          // console.warn("this is PRODUCTION");
      }

      if(this.props.mobx_auth.check === 0){
          return <SplashScreen/>
        }
      if(this.props.mobx_auth.check === 1){
          return <LoginRegNavigator/>
      }
      if(this.props.mobx_auth.check === 2){
          return <Onboarding/>
      }
      if(this.props.mobx_auth.check === 3){
          return <BottomNavigator/>
      }
      if(this.props.mobx_auth.check === 4){
          return <Update/>
      }
      if(this.props.mobx_auth.check === 5){
          return <JailBreak />
      }
  
      return null;
  }
}

MainAppNavigator = inject('mobx_auth','mobx_loader', 'mobx_carkey', 'mobx_config')(observer(MainAppNavigator))
export default MainAppNavigator
