import React from 'react';
import { View, Text, Image, Dimensions } from 'react-native';

import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';

import InboxNavigator from './InboxNavigator'
import TrackNavigator from './TrackNavigator'
import HomeNavigator from './HomeNavigator'

const BottomTab = createBottomTabNavigator({
    Home: {screen: HomeNavigator,},
    Track:{ screen: TrackNavigator},
    Inbox: {screen: InboxNavigator},
  }, {
      defaultNavigationOptions: ({ navigation }) => ({
        tabBarVisible: navigation.state.index === 0,
        tabBarIcon: ({ focused, tintColor }) => {
          const { routeName } = navigation.state;
          let IconComponent = Ionicons;
          let image;
          let labelName;
          if (routeName === 'Home') {
            image = focused
            ? require('../assets/bottomIcons/homeActive.png')
            : require('../assets/bottomIcons/homeInactive.png')
            labelName = 'Home'
          } 
          if (routeName === 'Track') {
            image = focused
            ? require('../assets/bottomIcons/trackActive.png')
            : require('../assets/bottomIcons/trackInactive.png')
            labelName = 'Track'
          }
          if (routeName === 'Inbox') {
            image = focused
            ? require('../assets/bottomIcons/inboxActive.png')
            : require('../assets/bottomIcons/inboxInactive.png')
            labelName = 'Inbox'
          }

          // You can return any component that you like here!
          return (
            <View style={{alignItems:'center', justifyContent:'center',width:Dimensions.get('window').width /3-10, flex:1, paddingBottom:5,}} >
                    <Image
                        source={image}
                        style={{height:30, width:30, }}
                    />
                    <Text style={{color: tintColor,fontFamily: "ProximaNova-Bold",fontSize:12,paddingBottom:5,textAlign:'center', }}>{labelName}</Text>
                  </View>
          );
        },
      }),
      tabBarOptions : {
        activeTintColor: '#080880',
        inactiveTintColor: '#7a7a7a',
        showLabel: false,
        style: {
          bottom: 0, width:'100%',
          backgroundColor:'#fafdff', borderTopWidth:0.5, borderTopColor:'#0000' ,  marginBottom:0,
          ...Platform.select({
              ios: {
                  shadowColor: '#000',
                  shadowRadius: 2,
                  shadowOffset: { width: 0, height: 3 },
                  shadowOpacity: 0.6,
              },
              android: {
                  elevation: 2
              },
          }),}
      }
    });
  
  
export default createAppContainer(BottomTab);
  