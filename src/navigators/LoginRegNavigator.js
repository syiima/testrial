import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import LogOption from '../screens/LogOption';
import LoginScreen from '../screens/LoginScreen';
import RegisterScreen from '../screens/RegisterScreen';
import ForgotPassword from '../screens/ForgotPassword';

const LoginAndRegisterPage = createStackNavigator({
  LogOption :{screen :LogOption},
  LoginScreen : {screen : LoginScreen},
  RegisterScreen : {screen : RegisterScreen},
  ForgotPassword :{screen : ForgotPassword},
  },{
    headerMode:'none',
    initialRouteName: 'LogOption',
    navigationOptions: {
      gesturesEnabled: false
    }
  })

export default createAppContainer(LoginAndRegisterPage)