import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import HomeScreen from '../screens/HomeScreen';
import HomeServices from "../screens/HomeServices";
import Profile from '../screens/Profile';
import ProfileEdit from '../screens/ProfileEdit';
import ProfileEdit2 from '../screens/ProfileEdit2';
import Settings from '../screens/Settings';
import Onboarding from '../screens/Onboarding';
import RoadAssisstance from '../screens/RoadAssisstance';

import VehicleAdd from '../screens/VehicleAdd';
import VehicleProfile from '../screens/VehicleProfile';
import VehicleRoadtax from '../screens/VehicleRoadtax';
import VehicleDetails from '../screens/VehicleDetails';
import VehicleInsurance from '../screens/VehicleInsurance';
import VehicleRegistration from '../screens/VehicleRegistration';
import VehicleEdit from '../screens/VehicleEdit';
import VehicleEdit2 from '../screens/VehicleEdit2';
import VehicleHealth from '../screens/VehicleHealth';
import VehicleHealthMotor from '../screens/VehicleHealthMotor';
import VehicleHistory from '../screens/VehicleHistory';
import VehicleHistoryManual from '../screens/VehicleHistoryManual'
import VehicleHistoryManualEdit from '../screens/VehicleHistoryManualEdit';
import VerificationGrant from '../screens/VerificationGrant';
import VerificationRequest from '../screens/VerificationRequest';
import VehicleOwnership from '../screens/VehicleOwnership';

import WorkshopListing from '../screens/WorkshopListing'
import WorkshopDetails from '../screens/WorkshopDetails'
import WorkshopBookingAppointment from '../screens/WorkshopBookingAppointment';
import WorkshopScan from '../screens/WorkshopScan';
import WorkshopReviews from '../screens/WorkshopReviews';
import WorkshopAppointment from '../screens/WorkshopAppointment'
import WorkshopQuotation from '../screens/WorkshopQuotation'
import WorkshopPerformService from '../screens/WorkshopPerformService'
import WorkshopPayment from '../screens/WorkshopPayment'
import WorkshopFeedback from '../screens/WorkshopFeedback'
import WorkshopMotorcycleHealth from '../screens/WorkshopMotorcycleHealth';
import WorkshopVehicleHealth from '../screens/WorkshopVehicleHealth'
import BillPlzWebview from '../components/BillPlzWebview'
import TngWebview from '../components/TngWebview';

import Webview from '../components/Webview'
import WebviewInsurance from '../components/WebviewInsurance'
import PDFWebview from '../components/PDFWebview'
import MarketplaceDetails from '../screens/MarketplaceDetails';
import WebviewMarketplace from '../components/WebviewMarketplace';
import Rewards from '../screens/Rewards';
import RewardsPrimo from '../screens/RewardsPrimo'
import RewardsPrimoApply from '../screens/RewardsPrimoApply'
import RewardsPrimoStatus from '../screens/RewardsPrimoStatus';
import RewardsStatusDesc from '../screens/RewardsStatusDesc'
import Referrals from '../screens/Referrals';
import ReferFriend from '../screens/ReferFriend';
import ReferFriendHIW from '../screens/ReferFriendHIW'
import ReferMerchant from '../screens/ReferMerchant';
import DirectLending from '../screens/DirectLending';
import MarketingSecContent from '../components/MarketingSecContent';
import MarketingSectionsCard from '../components/MarketingSectionsCard';


//SCAN 
import QRScreen from '../screens/QRScreen';
import QRScan from '../screens/QRScan';

import TrackBookingHistoryDetails from '../screens/TrackBookingHistoryDetails'
import TrackSOTGHistory from '../screens/TrackSOTGHistory';


const HomeNavigator = createStackNavigator({
  HomeScreen:{screen: HomeScreen},
  HomeServices:{screen: HomeServices},
  Profile:{screen:Profile},
  ProfileEdit:{screen:ProfileEdit},
  ProfileEdit2:{screen:ProfileEdit2},
  Settings:{screen :Settings},
  Onboarding:{screen:Onboarding},
  RoadAssisstance:{screen:RoadAssisstance},
  QRScan: {screen :QRScan},
  
  VehicleAdd: {screen: VehicleAdd},
  VehicleProfile:{screen:VehicleProfile},
  VehicleDetails:{screen:VehicleDetails},
  VehicleEdit:{screen: VehicleEdit},
  VehicleEdit2: {screen: VehicleEdit2},
  VehicleHealth:{screen: VehicleHealth},
  VehicleHealthMotor :{screen :VehicleHealthMotor},
  VehicleInsurance:{screen:VehicleInsurance},
  VehicleRoadtax: {screen:VehicleRoadtax},
  VehicleRegistration: {screen: VehicleRegistration},
  VehicleHistory: {screen: VehicleHistory},
  VehicleHistoryManual : {screen :VehicleHistoryManual},
  VehicleHistoryManualEdit: {screen: VehicleHistoryManualEdit},
  TrackBookingHistoryDetails :{screen :TrackBookingHistoryDetails},
  TrackSOTGHistory :{screen :TrackSOTGHistory},
  VerificationGrant :{screen: VerificationGrant},
  VerificationRequest :{screen: VerificationRequest},
  VehicleOwnership: {screen: VehicleOwnership},

  WorkshopListing:{screen:WorkshopListing},
  WorkshopDetails:{screen: WorkshopDetails},
  WorkshopBookingAppointment: {screen:WorkshopBookingAppointment},
  WorkshopScan: {screen: WorkshopScan},
  WorkshopReviews: {screen: WorkshopReviews},
  WorkshopAppointment :{screen: WorkshopAppointment},
  WorkshopQuotation:{screen :WorkshopQuotation},
  WorkshopPerformService :{screen:WorkshopPerformService},
  WorkshopPayment:{screen:WorkshopPayment},
  WorkshopFeedback : {screen : WorkshopFeedback},
  WorkshopVehicleHealth : {screen: WorkshopVehicleHealth},
  WorkshopMotorcycleHealth :{screen: WorkshopMotorcycleHealth},
  BillPlzWebview:{screen :BillPlzWebview},
  TngWebview:{screen :TngWebview},
  
  Webview: {screen: Webview},
  WebviewInsurance:{screen :WebviewInsurance},
  PDFWebview :{screen: PDFWebview},
  WebviewMarketplace: {screen: WebviewMarketplace},
  MarketplaceDetails: {screen: MarketplaceDetails},
  Rewards: {screen: Rewards},
  RewardsPrimo: {screen:RewardsPrimo},
  RewardsPrimoApply :{screen: RewardsPrimoApply},
  RewardsPrimoStatus:{screen:RewardsPrimoStatus},
  RewardsStatusDesc: {screen: RewardsStatusDesc},
  Referrals:{screen: Referrals},
  ReferFriend: {screen: ReferFriend},
  ReferFriendHIW: {screen: ReferFriendHIW},
  ReferMerchant: {screen: ReferMerchant},
  DirectLending : {screen: DirectLending},
  MarketingSectionsCard: {screen: MarketingSectionsCard},
  MarketingSecContent: {screen: MarketingSecContent},


},{
  headerMode:'none',
  initialRouteName: 'HomeScreen',
  navigationOptions: {
    gesturesEnabled: false
  }
})

export default createAppContainer(HomeNavigator)