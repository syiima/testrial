import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Track from '../screens/TrackScreen';
import TrackBookingHistory from '../screens/TrackBookingHistory2'
import TrackActivities from '../screens/TrackActivities';
import TrackPayment from '../screens/TrackPayment';
import PDFConverter from '../components/PDFConverter'
import TrackBookingHistoryDetails from '../screens/TrackBookingHistoryDetails'
import TrackSOTGHistory from '../screens/TrackSOTGHistory';
import TrackEditHistory from '../screens/TrackEditHistory';

import WorkshopListing from '../screens/WorkshopListing'
import WorkshopDetails from '../screens/WorkshopDetails'
import WorkshopBookingAppointment from '../screens/WorkshopBookingAppointment';
import WorkshopScan from '../screens/WorkshopScan';
import WorkshopReviews from '../screens/WorkshopReviews';
import WorkshopAppointment from '../screens/WorkshopAppointment'
import WorkshopQuotation from '../screens/WorkshopQuotation'
import WorkshopPerformService from '../screens/WorkshopPerformService'
import WorkshopPayment from '../screens/WorkshopPayment'
import WorkshopFeedback from '../screens/WorkshopFeedback'
import WorkshopMotorcycleHealth from '../screens/WorkshopMotorcycleHealth';
import WorkshopVehicleHealth from '../screens/WorkshopVehicleHealth'
import BillPlzWebview from '../components/BillPlzWebview'
import TngWebview from '../components/TngWebview';


const TrackNavigator = createStackNavigator({
    Track : {screen : Track},
    TrackBookingHistory: {screen:TrackBookingHistory},
    TrackActivities:{screen :TrackActivities},
    TrackPayment: {screen :TrackPayment},
    PDFConverter : {screen :PDFConverter},
    TrackBookingHistoryDetails : {screen :TrackBookingHistoryDetails},
    TrackSOTGHistory :{screen :TrackSOTGHistory},
    TrackEditHistory :{screen: TrackEditHistory},
    WorkshopListing :{screen: WorkshopListing},
    WorkshopDetails:{screen : WorkshopDetails},
    WorkshopBookingAppointment: {screen:WorkshopBookingAppointment},
    WorkshopScan: {screen: WorkshopScan},
    WorkshopReviews: {screen: WorkshopReviews},
    WorkshopAppointment :{screen: WorkshopAppointment},
    WorkshopQuotation:{screen :WorkshopQuotation},
    WorkshopPerformService :{screen:WorkshopPerformService},
    WorkshopPayment:{screen:WorkshopPayment},
    WorkshopFeedback : {screen : WorkshopFeedback},
    WorkshopVehicleHealth:{screen :WorkshopVehicleHealth},
    WorkshopMotorcycleHealth :{screen:WorkshopMotorcycleHealth},
    BillPlzWebview:{screen :BillPlzWebview},
    TngWebview : {screen: TngWebview},


  },{
    headerMode:'none',
    initialRouteName: 'Track',
    navigationOptions: {
      gesturesEnabled: false
    }
  })

export default createAppContainer(TrackNavigator)