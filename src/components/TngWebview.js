import React, {Component} from 'react';
import {Platform, Alert, StyleSheet, Text, ActivityIndicator, View, Button, ImageBackground,SafeAreaView, TextInput, ScrollView,Image, Dimensions, TouchableOpacity, KeyboardAvoidingView} from 'react-native';
import { inject, observer, Provider } from 'mobx-react';
import {WebView}  from 'react-native-webview'

class TngWebview extends React.Component{
    constructor(props){
        super(props);
    
        this.state = {
          current_webview_url: '',
          total_url: [],
        }
    }

    componentDidMount(){
      this.runInt()
    }


    runInt(){
      //timeout
      setTimeout(() => {
        this.runOrderQ()
      }, 120000);
    }

    runOrderQ(){
      let acquirementId = this.props.navigation.state.params.acquirementId
      let NM = {
        acquirementId: acquirementId
      }
      // console.log('checkURL',acquirementId)

      let URL 

      if(this.props.mobx_config.config === 0){
        URL = 'http://pay-staging.serv.my/api/touchngo/orderQuery'
      }
      if(this.props.mobx_config.config === 1){
        URL = 'https://pay.serv.my/api/touchngo/orderQuery'
      }

      fetch(URL, {
        method: 'POST', 
        headers: {
          "Content-Type": "application/json",
          token : 'ofhAei9pg0WMZJ5O8kGSf6aA0yCeKZJryxfn0rFK6uvicPvKkgQ5tHNzbUpU77Ks'
        },
        body: JSON.stringify(NM)
      })
    }

    _onNavigationStateChangeVersion2(webViewState){
        let acquirementId = this.props.navigation.state.params.acquirementId
        
        console.log('webview', webViewState);
        let url = webViewState.url;
        let NM = {
          acquirementId: acquirementId
        }

        let URL 

        if(this.props.mobx_config.config === 0){
          URL = 'http://pay-staging.serv.my/api/touchngo/orderQuery'
        }
        if(this.props.mobx_config.config === 1){
          URL = 'https://pay.serv.my/api/touchngo/orderQuery'
        }

        let waitingURL = 'https://nav.serv.my/aGeVTGVjeklhTWgHRTUcEdhiQthpOtoFpbWsBMCZ.html'

        //trigger URL everytime open webview and after 1 min 
        fetch(URL, {
          method: 'POST', 
          headers: {
            "Content-Type": "application/json",
            token : 'ofhAei9pg0WMZJ5O8kGSf6aA0yCeKZJryxfn0rFK6uvicPvKkgQ5tHNzbUpU77Ks'
          },
          body: JSON.stringify(NM)
        }).then((resp) => {
          // console.warn('ahkahkdak');
        })
      }


    render(){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#080880'}}>
                <View style={{flex:1, backgroundColor:'#fff'}}>
                <WebView
                    useWebKit={true}
                    source={{
                        uri: this.props.navigation.state.params.url_link,           
                        }}
                    ref="webview"
                    onNavigationStateChange={this._onNavigationStateChangeVersion2.bind(this)}
                />
                </View>
        
        </SafeAreaView>
        )
    }
}

TngWebview = inject('mobx_config')(observer(TngWebview))
export default TngWebview
