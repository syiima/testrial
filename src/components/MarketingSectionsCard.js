import React from 'react';
import {View, Text, StyleSheet, Image, FlatList,SafeAreaView, ScrollView, TouchableOpacity, Dimensions, TouchableWithoutFeedback} from "react-native";

const WIDTH = Dimensions.get('window').width
const HEIGHT = Dimensions.get('window').height

export default class MarketingSectionsCard extends React.Component {
  

    navigate = (item) => {
        let display
        console.log("Ada", this.props.navigation)
        if(item.content !== undefined) {
            this.props.navigation.navigate('MarketingSecContent', {data: item.content}) // link content new page
        } else {
            let LINK = item.link
            this.props.navigation.navigate('Webview', {link:LINK});// link webview 
        }
    }


    marketingSections = () => {
        let details = this.props.data
        let show
        let SORTED
        let sec = []
        if(details !== undefined) {
            let keys = Object.keys(details)
            keys.forEach((secID) => {
                let a = details[secID]
                a.id = secID 
                if( a.timestamp !== undefined){
                    sec.push(a)

                }
            
            })
            SORTED = sec.sort((a,b) => a.timestamp - b.timestamp)

            console.log('oiii', SORTED);

            show = SORTED.map((item, index) => (
                <TouchableWithoutFeedback onPress={() => this.navigate(item)}>
                    <View style={index === 0 ? styles.first : styles.last}>
                        <View style={styles.sizeImage}>
                        <Image source={{uri: item.image}} style={styles.sizeImage} />
                        </View>
                        <View style={{marginLeft:4}}>
                        <Text style={styles.txtName}>{item.name}</Text>
                        <Text style={styles.txtDescription}>{item.description}</Text>
                        </View>
                    </View>
                </TouchableWithoutFeedback>

            ))
       } 
       return show
    }
    render() {
        return (
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                {this.marketingSections()}    
            </ScrollView> 
            
        )
    }
}


const styles = StyleSheet.create({
    first :{
        width:WIDTH /2.35, margin:10,marginLeft:20,
    },
    last: {
        width:WIDTH /2.35, margin:10,marginLeft:0,
    },
    sizeImage: {
        width:WIDTH  /2.35, 
        height:WIDTH / 2.35,  
        borderRadius:10,
        resizeMode:'cover'
    },
    txtName: {
        fontFamily:"ProximaNova-Bold",
        fontSize:14,
        marginTop:15,
        // marginBottom:5,

    }, 
    txtDescription: {
        // fontFamily:"ProximaNova-Bold",
        color:'#080880',
        fontSize:13,
        marginTop:2,
    }
})