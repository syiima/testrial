import React from "react"
import { StyleSheet, Text, Modal,SafeAreaView,Platform, KeyboardAvoidingView,View, Dimensions} from "react-native"
// padding:this.props.padding? this.props.padding : 20,
const WIDTH = Dimensions.get('window').width

class MiddleModal extends React.Component{
    render(){
        return(
            <Modal
                transparent={true}
                visible={this.props.modalName}
                animationType="slide">
                <KeyboardAvoidingView
                    behavior={(Platform.OS === 'ios') ? "padding" : null} enabled
                    style={{ flex: 1,  }}
                >
                <SafeAreaView style={styles.bgModal}>
                    <View style={{...styles.innermodal, height: this.props.height ? this.props.height : 'auto', padding: this.props.padding == true ? 0 : 20  }} >
                      
                        {this.props.children}      

                    </View>
                </SafeAreaView>
                </KeyboardAvoidingView>
            </Modal>
        )
    }
}

export default MiddleModal

const styles = StyleSheet.create({
    bgModal: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0.4)',
        
    },
    innermodal: {
        elevation: 20,
        borderRadius: 10,
        width:WIDTH - 30,
        backgroundColor: '#fff',
        ...Platform.select({
            ios: {
               shadowColor: '#000',
               shadowOpacity: 0.22,
               shadowRadius: 2.22,
              shadowOffset: { width: 0, height: 1 },
            },
            android: {
              elevation: 3
            },
           }),
    },
    
})