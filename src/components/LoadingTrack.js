import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import Icon from "react-native-vector-icons/FontAwesome"

import receipt from '../assets/icons/receipt.png'
import record from '../assets/icons/record.png'

class LoadingTrack extends React.Component{
    render(){
        return(
                <View style={{flex:1,backgroundColor:'#fafdff', margin:15 }}>
                {/* <Header backButton = {true}  headerTitle = 'Track' /> */}
                    
                    <View style={{margin:5}}>
                        <Text style={{fontFamily:'ProximaNova-Bold',fontSize:20, marginBottom:15,}}>My Vehicles</Text>
                      
                        <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('TrackPayment', {data: this.state.car_history})}>
                        <View style={styles.box}>
                            <View style={styles.icon}>
                                <Image source={receipt} style={{height:20, width:20}} />
                            </View>
                            <View>
                                <Text style={styles.topText}>Payment History</Text>
                                <Text style={styles.bottomText}>Made 0 payments on SERV app </Text>
                            </View>
                            <Icon name="angle-right" color='#000' size={20} style={{position:'absolute', right:15, bottom:15,}} />
                        </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('TrackBookingHistory', {history: this.state.car_history})}>
                        <View style={styles.box}>
                            <View style={styles.icon}>
                            <Image source={record} style={{height:20, width:20}} />
                            </View>
                            <View>
                                <Text style={styles.topText}  >Booking History</Text>
                                <Text style={styles.bottomText} >(0) items</Text>
                            </View>
                            <Icon name="angle-right" color='#000' size={20} style={{position:'absolute', right:15, bottom:15,}} />
                        </View>
                        </TouchableWithoutFeedback>
                        
                        <View style={{marginBottom:20,}}>
                            <View style={{marginTop:20,  flexDirection:'row', alignItems:'center'}}>
                                <View>
                                <Text style={{fontSize:12}}>Ongoing Activity</Text>
                                </View>
                                <View style={styles.separatorLine}/>
                            </View>

                            <View style={styles.ongoing}>      
                                <View style={{flexDirection: 'row',justifyContent:'space-between', alignItems:'center' }}>
                                    <View style={{flex:4,marginRight:10 }}>
                                        <View style={{backgroundColor:'#ccc',height: 12, marginTop:5}} />
                                        <View style={{backgroundColor:'#ccc',height: 10, marginTop:5}} />
                                        <View style={{backgroundColor:'#ccc',height: 10, marginTop:5}} />
                                    </View>
                                    <View style={{flex:0.5, }} />
                                    <Icon name="angle-right" size={20} style={{position:'absolute', right:10, bottom:15,}} />
                                </View>
                            </View>
                            <View style={styles.ongoing}>      
                                <View style={{flexDirection: 'row',justifyContent:'space-between', alignItems:'center' }}>
                                    <View style={{flex:4,marginRight:10 }}>
                                        <View style={{backgroundColor:'#ccc',height: 12, marginTop:5}} />
                                        <View style={{backgroundColor:'#ccc',height: 10, marginTop:5}} />
                                        <View style={{backgroundColor:'#ccc',height: 10, marginTop:5}} />
                                    </View>
                                    <View style={{flex:0.5, }} />
                                    <Icon name="angle-right" size={20} style={{position:'absolute', right:10, bottom:15,}} />
                                </View>
                            </View>
                            <View style={styles.ongoing}>      
                                <View style={{flexDirection: 'row',justifyContent:'space-between', alignItems:'center' }}>
                                    <View style={{flex:4,marginRight:10 }}>
                                        <View style={{backgroundColor:'#ccc',height: 12, marginTop:5}} />
                                        <View style={{backgroundColor:'#ccc',height: 10, marginTop:5}} />
                                        <View style={{backgroundColor:'#ccc',height: 10, marginTop:5}} />
                                    </View>
                                    <View style={{flex:0.5, }} />
                                    <Icon name="angle-right" size={20} style={{position:'absolute', right:10, bottom:15,}} />
                                </View>
                            </View>


                        </View>
                        
            

                    </View>
                </View>
        )
    }
}

export default LoadingTrack

const styles = StyleSheet.create({
    box: { flexDirection:'row', alignItems:'center', borderRadius:10, marginBottom:15, padding:15,backgroundColor:'#fff',
    ...Platform.select({
        ios: {
          shadowColor: '#000',
          shadowRadius: 2.22,
          shadowOffset: { width: 0, height: 1 },
          shadowOpacity: 0.22,
        },
        android: {
          elevation: 3
        },
       }),
    },
    imageBox:{
        width: Dimensions.get('window').width /3.8,
        height:Dimensions.get('window').width /3,
        margin:6,
        borderRadius:10,marginBottom:20,backgroundColor:'#fff',
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 2.22,
              shadowOffset: { width: 0, height: 1 },
              shadowOpacity: 0.22,
            },
            android: {
              elevation: 3
            },
           }),
    },
    icon:{width:40, height:40, marginRight:15, borderRadius:20, backgroundColor:'#efefef', alignItems:'center', justifyContent:'center'},
    topText :{fontSize:16, marginBottom: 6, fontFamily: "ProximaNova-Bold",},
    bottomText:{ fontSize:13, color:'grey'},
    separatorLine: {
        flex: 1,
        borderWidth: StyleSheet.hairlineWidth,
        height: StyleSheet.hairlineWidth,
        borderColor: '#9B9FA4',
        marginLeft:15,
    },
    ongoing: {
        padding:15,
        marginTop:15,
        backgroundColor:'#fff',
        borderRadius:10,
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 2.22,
              shadowOffset: { width: 0, height: 1 },
              shadowOpacity: 0.22,
            },
            android: {
              elevation: 3
            },
          }),
    },

})