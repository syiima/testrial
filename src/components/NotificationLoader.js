import React from "react"
import { StyleSheet,View} from "react-native"

class NotificationLoader extends React.Component{
    render(){
        return(
            <View style={{flex:1}}>
                <View style={styles.box}>
                    <View style={styles.circle} />
                    <View style={{ flexDirection:'column',flex:2, marginLeft:15 }}>
                        <View style={{height:16, backgroundColor:'#ccc'}} />
                        <View style={styles.topText}/>
                        <View style={{marginTop:3, backgroundColor:'#ccc', height:11}}/>
                    </View>
                </View>
                <View style={styles.box}>
                    <View style={styles.circle} />
                    <View style={{ flexDirection:'column', flex:2, marginLeft:15 }}>
                        <View style={{height:16, backgroundColor:'#ccc'}} />
                        <View style={styles.topText}/>
                        <View style={{marginTop:3, backgroundColor:'#ccc', height:11}}/>
                    </View>
                </View>
                <View style={styles.box}>
                    <View style={styles.circle} />
                    <View style={{ flexDirection:'column',flex:2, marginLeft:15 }}>
                        <View style={{height:16, backgroundColor:'#ccc'}} />
                        <View style={styles.topText}/>
                        <View style={{marginTop:3, backgroundColor:'#ccc', height:11}}/>
                    </View>
                </View>
                <View style={styles.box}>
                    <View style={styles.circle} />
                    <View style={{ flexDirection:'column',flex:2, marginLeft:15 }}>
                        <View style={{height:16, backgroundColor:'#ccc'}} />
                        <View style={styles.topText}/>
                        <View style={{marginTop:3, backgroundColor:'#ccc', height:11}}/>
                    </View>
                </View>
            </View>
        )
    }
}
export default NotificationLoader

const styles = StyleSheet.create({
    box :{
        flexDirection:'row',
        borderBottomColor:'#ccc',
        borderBottomWidth:1,
        marginLeft: 20, 
        marginRight:15, 
        padding:15,
        paddingLeft:0
    },
    topText :{backgroundColor:'#ccc',height:13, marginTop: 6},
    bottomText:{ height:13, backgroundColor:'#ccc'},

    circle :{backgroundColor:'#ccc',  borderRadius: 20,  height: 40, width:40, alignItems:'center', justifyContent:'center'}
})