import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/FontAwesome"

import Header from './Header.js'
import Collapsible from 'react-native-collapsible';
import Moment from 'moment';

class HistoryComponent extends React.Component{
    constructor(){
        super()
        this.state = {
            collapsed: false,
        }
    }
    
    componentDidMount(){
    }
    
    openDets = (x) => {
        if(x.type !== undefined && x.type === 'walkin'){
            firebase.analytics().logScreenView({
                screen_name: 'History_Walkin',
                screen_class: 'History_Walkin',
              })
            this.props.navigation.navigate('TrackBookingHistoryDetails', {
                item:x, 
                quotation: x.quotation.masterItems, 
            })
        } else {
            firebase.analytics().logScreenView({
                screen_name: 'History_Checklist',
                screen_class: 'History_Checklist',
              })
            this.props.navigation.navigate(
                "TrackSOTGHistory",{
                    item:x, 
                    quotation: x.quotation.masterItems, 
                    requested: x.quotation.requested_services
                })

        }
    }

    openManual = (x) => {
        this.props.navigation.navigate("TrackEditHistory",{item:x, onNavigateBack : this.goBack})
    }

    goBack = () => {
        this.props.navigation.state.params.onNavigateBack()
        this.props.navigation.popToTop()
    }


    render(){
        return(
            <View >
            <TouchableWithoutFeedback onPress={() => this.setState({ collapsed: !this.state.collapsed })}>
            <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
                <Text style={{fontFamily: "ProximaNova-Bold", fontSize:16,marginTop:15, marginBottom:15}} >{this.props.data.month}  </Text>
                {this.state.collapsed === false ? <Icon name='sort-up' size={20} /> : <Icon name='sort-down' size={20}  /> }

            </View>
            </TouchableWithoutFeedback>
               
                {this.props.data.history.map(item => {
                    return item.type === 'self input' ?  
                    <Collapsible collapsed={this.state.collapsed} style={{margin:3}} >
                    <TouchableWithoutFeedback onPress={() => this.openManual(item)}>
                        <View style={styles.box}>
                        <Text style={{fontFamily: "ProximaNova-Bold",color:'#080880',fontSize:16  }} >Manual Input</Text> 
                            <Text style={{fontFamily: "ProximaNova-Bold",marginTop:3, fontSize:15}} >{Moment(item.date).format('DD MMMM YYYY')} </Text>
                            <Text style={{marginTop:3,fontSize:12 }}>{item.customer_carplate} </Text>
                            <Text style={{fontSize:12, marginTop:3  }} >{item.workshop}</Text>
                            <Icon name="angle-right" color='#000' size={20} style={{position:'absolute', right:15, bottom:15,}} />
                        </View>
                    </TouchableWithoutFeedback>
                    </Collapsible>
                    :
                    <Collapsible collapsed={this.state.collapsed} style={{margin:3}} >
                    <TouchableWithoutFeedback onPress={() => this.openDets(item) }>
                        <View style={styles.box}>
                        {item.type !== undefined ? <Text style={{fontFamily: "ProximaNova-Bold",color:'#080880',fontSize:16  }} >Workshop Appointment</Text> : <Text style={{fontFamily: "ProximaNova-Bold",color:'#080880',fontSize:16  }} >On-The-Go</Text>}
                        <Text style={{fontFamily: "ProximaNova-Bold",marginTop:3, fontSize:15}} >{item.customer_day} {item.customer_month} {item.customer_year} ({item.customer_time}) </Text>
                        <Text style={{marginTop:3,fontSize:12 }}>{item.customer_carplate} </Text>
                        {/* <Text style={{marginTop:3,fontSize:12 }}>{item.customer_carmake} {item.customer_carmodel} </Text> */}
                        {item.type === 'walkin' && item.customer_nameplace !== undefined ? <Text style={{fontSize:12, marginTop:3  }} >{item.customer_nameplace}</Text> : <Text style={{fontSize:12, marginTop:3  }} >{item.customer_address}</Text>}

                        {item.type !== undefined ? <Text style={{fontSize:12  }} >Category: Drive-In</Text> : <Text style={{fontSize:12 }} >Category: On-The-Go</Text>}

                        <Icon name="angle-right" color='#000' size={20} style={{position:'absolute', right:15, bottom:15,}} />
                    </View>
                    </TouchableWithoutFeedback>
                    </Collapsible>
                })}
               
        </View>
        )
    }
}

HistoryComponent = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(HistoryComponent))
export default HistoryComponent

const styles = StyleSheet.create({
    box :{
        backgroundColor:'#fff',
        borderRadius:10,
        marginBottom:15,
        padding:15,
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 3 },
              shadowOpacity: 0.3,
            },
            android: {
              elevation: 3
            },
          }),
    }
})