import React, { Component } from 'react'
import { View, Text, TouchableWithoutFeedback } from 'react-native'
import Icon from "react-native-vector-icons/FontAwesome";


class PickerTextInput extends Component {

  render() {
  
    return (
        <TouchableWithoutFeedback onPress={this.props.onPress} >
            <View style={{marginBottom:20}} >
                <Text style={{fontWeight:'bold', fontSize:13,marginBottom:10, color: this.props.color ? this.props.color : '#6e6d6d' }}>{this.props.title}</Text>

                <View style={styles.box}>
                    <Text>{this.props.value}</Text>
                    <Icon name="sort-down" color="#ccc" size={20} />
                </View>
            
            </View>
        </TouchableWithoutFeedback>
      );
  }
}

const styles = ({
    box :{
        borderWidth:1, borderColor:'#ccc',borderRadius:5, padding:10, flexDirection:'row', justifyContent:'space-between', alignItems:'center'
    }
})

export default PickerTextInput;
