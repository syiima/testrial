import React, { Component } from 'react'
import { View, Text, TextInput } from 'react-native'


class FormTextInput extends Component {

  render() {
  
    return (
      <View style={{marginBottom:20}} >
        <Text style={{fontWeight:'bold', fontSize:13,marginBottom:10, color: this.props.color ? this.props.color : '#686868' }}>{this.props.title}</Text>

        <View style={{padding:(Platform.OS === 'ios') ? 10 : 5, borderWidth:1, borderColor:'#a7a7a7',borderRadius:5 }}>
            <TextInput 
                placeholder={this.props.placeholder}
                placeholderTextColor={this.props.placeholderTextColor ?  this.props.placeholderTextColor : "#a7a7a7"}
                keyboardType={this.props.keyboardType ? this.props.keyboardType : 'default'}
                multiline = {this.props.multiline ? this.props.multiline : false}
                autoCorrect={false}
                autoCapitalize = {this.props.autoCapitalize ? this.props.autoCapitalize : "none" }
                underlineColorAndroid="transparent" 
                onChangeText={this.props.onChangeText}
                value={this.props.value}
                style={{fontFamily:'ProximaNova-Regular', padding:0,}}
                blurOnSubmit={false}
                returnKeyType={this.props.returnKeyType ? this.props.returnKeyType : 'next'}
                onSubmitEditing={this.props.onSubmitEditing}
                ref={this.props.inputRef}


            />
        </View>
       
      </View>
      );
  }
}

export default FormTextInput;
