import React from "react"
import { StyleSheet, SafeAreaView, Dimensions, Text,TouchableOpacity, View,ActivityIndicator,} from "react-native"
import Icon from 'react-native-vector-icons/AntDesign'
import { WebView } from 'react-native-webview';

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

class Webview extends React.Component{
    constructor(){
        super()
        this.state = {
            visible : true
        }
    }

    hideSpinner() {
        this.setState({ visible: false });
    }

    render(){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#080880'}} >
                <View style={{backgroundColor:'#fafdff', flex:1}}>
                
                <TouchableOpacity style={styles.backButton} onPress={() => this.props.navigation.goBack(null)}>
                <Icon name='leftcircle' size={18} color='#fff'  />
                <Text style={{color:'#fff', fontFamily: "ProximaNova-Bold", marginLeft:14}}> Back </Text>
                </TouchableOpacity>
                    
                    <WebView
                        useWebKit={true}
                            onLoad={() => this.hideSpinner()}
                            style={{ flex: 1 }}
                            ref={webview => (this.webview = webview)}
                            source={{ uri: this.props.navigation.state.params.link }}
                        />
                
                
                        {this.state.visible && (
                            <ActivityIndicator
                            color="#080880"
                            style={{ position: "absolute", top: height / 2, left: width / 2 }}
                            size="large"
                            />
                        )}
                </View>
            </SafeAreaView>
        )
    }
}
export default Webview


const styles = StyleSheet.create({
   
    backButton:{
        borderRadius:50,
        alignSelf:'flex-start',
        margin:10,
        padding:10,paddingRight:20, paddingLeft:20,
        backgroundColor:'#080880',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:"center"
      },

})