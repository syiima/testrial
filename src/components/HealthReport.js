import React, {Component} from 'react';
import {StyleSheet, Modal, Platform,ImageBackground,Text,Alert, TouchableWithoutFeedback, View, SafeAreaView, Image, FlatList,
  TouchableOpacity,ScrollView, Button,Dimensions,ActivityIndicator,Linking} from 'react-native';
import firebase from '@react-native-firebase/app';
import { inject, observer, Provider } from 'mobx-react';
import Icon from "react-native-vector-icons/FontAwesome";
import AntDesign from 'react-native-vector-icons/AntDesign';
import { PieChart, ProgressCircle } from 'react-native-svg-charts'
import Moment from 'moment';


class HealthReport extends React.Component{
    constructor(){
        super()
        this.state = {
            finalPercent: 0.0,
            health: false,
            next_service: ''
        }
    }

    componentDidMount(){
        this.calcHealth()
    }

    calcHealth(){
        let that = this;
        let FUID = this.props.mobx_auth.FUID;
        let CAR_KEY = this.props.mobx_carkey.CarKey;
        let PLATE = this.props.mobx_carkey.PLATE
        let finalSum = 0

        firebase.database().ref(`plate_number/${PLATE}/car_health`).once('value').then((snapshot) => {
            if(snapshot.exists()){
                if(snapshot.val().customer_car_mileage !== undefined){
                    let KEYS = Object.keys(snapshot.val())
                    let sum = 0
    
                    KEYS.forEach((item) => {
                        let X = item
                        // console.warn('haha', X);
    
                        if(X !== 'VIN_number' && X !== 'customer_car_mileage' && X !== 'customer_next_car_mileage' && X !== 'extra_notes' && X !== 'next_service_date' && X !== 'roadtax_expiry_date' && X !== 'timestamp' && X !== 'recommended_service-0' &&  X !== 'estimate_cost-0' && X !== 'recommended_service-1' ){
    
                            if(snapshot.val()[X] === 'good' || snapshot.val()[X] === 'worn'){
                                if(snapshot.val()[X] === 'good'){ 
                                    // snapshot.val()[X] = '5'
                                    sum += parseFloat('5')
                                    // console.warn('hey', sum);
                                }
                                if(snapshot.val()[X] === 'worn'){ 
                                    // snapshot.val()[X] = '3'
                                    sum += parseFloat('3')
                                    // console.warn('heyx', sum)
                                } 
                            } else { 
                            sum += parseInt(snapshot.val()[X])
                            }                            
                        }
                        })
                        finalSum = parseFloat((sum / (26 * 5) * 100).toFixed(0))
                        // console.warn('total', finalSum);
                        that.setState({finalPercent: finalSum, health: true, next_service: snapshot.val().next_service_date})
                } else {
                    this.setState({health: false})
                }
                    
            } else {
                this.setState({health: false})
            }
        })
    }



  _renderNextDueService = () => {
    let DATA = this.state.next_service
    let display
    let FINALDATE

    let M = Moment(DATA).format('MM/DD/YYYY')

    if(M === 'Invalid date'){
        let DATE = DATA.split('-')[0]
        let MONTH = DATA.split('-')[1]
        let YEAR = DATA.split('-')[2]

        FINALDATE = MONTH + '/' + DATE + '/' + YEAR 
    } else {
        FINALDATE = M
    }


      let TODAY_DATE = Moment().format('MM/DD/YYYY')
      let TODAYEPOCH = new Date(TODAY_DATE).getTime()/1000.0
      // let TODAY_MONTH = Moment().format('MM')
      // let TODAY_YEAR = Moment().format('YYYY');

    //   let FINALDATE = MONTH + '/' + DATE + '/' + YEAR 
      let USERDATE = new Date(FINALDATE).getTime()/1000.0

      if(TODAYEPOCH < USERDATE){
          display = 
          <View style={{marginTop:10, padding:4, backgroundColor:'green', borderRadius:5}}>
              <Text style={{fontSize:11, color:'#fff', fontFamily:'ProximaNova-Bold', textAlign:'center'}}>Next service: {DATA}</Text>
          </View>
      } 
      if(TODAYEPOCH > USERDATE) {
          display = 
          <View style={{marginTop:10, padding:4, backgroundColor:'red', borderRadius:5}}>
              <Text style={{fontSize:11, color:'#fff', fontFamily:'ProximaNova-Bold', textAlign:'center'}}>Service due:  {DATA}</Text>
          </View>
      }      
        return display
    }


    _goto = () => {
        let FUID = this.props.mobx_auth.FUID;
        let PLATE = this.props.mobx_carkey.PLATE

        firebase.analytics().logScreenView({
            screen_name: 'CarHealth',
            screen_class: 'CarHealth',
          });
        firebase.database().ref(`plate_number/${PLATE}/health_check`).set(false)

        this.props.navigation.navigate('VehicleHealth')

    }

    render(){
        let commentary ;

        if(this.state.finalPercent >= 80 ){
            commentary = 
            <View >
                <Text style={{fontFamily:'ProximaNova-Bold', fontSize:13, marginBottom:10}}>Excellent! Keep it up.</Text>
                <Text style={{fontSize:11}}>We keep track of every service so you don't have to. </Text>
            </View>   
        }
        if(this.state.finalPercent < 80 && this.state.finalPercent > 40){
            commentary = 
            <View >
                <Text style={{fontFamily:'ProximaNova-Bold', fontSize:13, marginBottom:10}}>Looking good but can be improved.</Text>
                <Text style={{fontSize:11}}>We keep track of every service so you don't have to. </Text>
            </View>   
        }
        if(this.state.finalPercent <= 40){
            commentary = 
            <View >
                <Text style={{fontFamily:'ProximaNova-Bold', fontSize:13, marginBottom:10}}>Woah! Let’s get you SERVed right away.</Text>
                <Text style={{fontSize:11}}>We keep track of every service so you don't have to. </Text>
            </View>   
        }

        if(this.state.health === false){
            commentary = 
            <View >
                <Text style={{fontFamily:'ProximaNova-Bold', fontSize:13, marginBottom:10}}>Oops… Looks like there’s no record.</Text>
                <Text style={{fontSize:11}}>We keep track of every service so you don't have to. </Text>
            </View>  
        }

        let render_color;

        if(this.state.finalPercent <=40 ){ render_color = 'red'}
        if(this.state.finalPercent >= 41 && this.state.finalPercent < 80){render_color = '#e9c91d'}
        if(this.state.finalPercent >= 80){render_color = '#008000'}

        return(
            <View>
            <TouchableWithoutFeedback onPress={this._goto}>


            <View style={styles.box} >
                 <View style={ styles.smolCircle }>
                    <ProgressCircle
                        style={styles.insideC}
                        progress={this.state.finalPercent /100}
                        strokeWidth={6}
                        progressColor={render_color}
                    />
                    <View style={this.state.next_service !== '' ? styles.ava : styles.NA }>
                      {this.state.health === true ? 
                      <Text style={{fontFamily:'ProximaNova-Bold'}}>{this.state.finalPercent}%</Text>
                    : <Text style={{fontFamily:'ProximaNova-Bold'}}> NA </Text>}
                    </View>
                </View>
                
                <View style={{flexShrink:1, flex:3, marginLeft:10, marginRight:10 }}>
                {commentary}
                {this._renderNextDueService()}
                </View>
                

                {this.state.health === true ? 
                
                <View style={{justifyContent:'flex-end', flex:1, }}>
                    {/* <Text style={{fontSize:11, marginRight:2, flexShrink:1}}>Have a sneak peek</Text> */}
                    <Icon name="angle-right" size={25} style={{alignSelf:'flex-end', marginRight:10}} />

                </View>
                : null}

            </View>
            </TouchableWithoutFeedback>
            </View>
        )
    }
}

HealthReport = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(HealthReport))
export default HealthReport

const styles = StyleSheet.create({
    box:{
       borderRadius:15,backgroundColor:'#fff', flexDirection:'row', alignItems:'center' , padding:15, paddingRight:5, backgroundColor:'#fff', marginTop:5, marginBottom:15, flexWrap:'wrap',
       ...Platform.select({
           ios: {
             shadowColor: '#000',
             shadowRadius: 3,
             shadowOffset: { width: 0, height: 3 },
             shadowOpacity: 0.3,
           },
           android: {
             elevation: 3
           },
         }),
    },
    smolCircle: {flex:1, alignItems:'center', justifyContent:'center',},
    insideC:{width: Dimensions.get('window').width / 6, height:Dimensions.get('window').width / 6, justifyContent: 'center'},
    ava:{position:'absolute', top:'30%', left:'50%', marginLeft: -(Dimensions.get('window').width / 6)/5, },
    NA :{position:'absolute', top:'50%', left:'50%', marginLeft: -(Dimensions.get('window').width / 6)/5, marginTop:-(Dimensions.get('window').width / 6)/8,}
})