import React, { Component } from 'react';
import{Text, View, SafeAreaView, Dimensions,Image, ActivityIndicator, BackHandler, Platform, TouchableOpacity,StyleSheet} from 'react-native';
import { WebView } from 'react-native-webview';
import { inject, observer, Provider } from 'mobx-react';
import firebase from '@react-native-firebase/app';

import Icon from 'react-native-vector-icons/AntDesign'
import FIcon from 'react-native-vector-icons/FontAwesome5'

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width


class WebviewMarketplace extends React.Component{

  constructor(props) {
      super(props);
      this.state = { 
        visible: true,
        key:1,
        canGoForward: false,
        canGoBack: false,
        push_key:'',

        currentURL: '',
      };
    }
    
    componentDidMount(){
        this.createKey()
        if (Platform.OS === 'android') {
            BackHandler.addEventListener('hardwareBackPress', this.onAndroidBackPress);
        }
    }
    componentWillUnmount(){
        if (Platform.OS === 'android') {
            BackHandler.removeEventListener('hardwareBackPress', this.onAndroidBackPress);
        }
    }

    onAndroidBackPress = () => {
        let KEY = this.state.push_key
            let TIME = Date.now()
            this.props.navigation.state.params.backItem(TIME, KEY);
            this.props.navigation.goBack()
    }


    createKey(){
        let MARKETID=  this.props.navigation.state.params.item.market_key
        let KEY = firebase.database().ref(`marketplace_engagement/${MARKETID}`).push().key;

        this.setState({push_key :KEY})
    }

    hideSpinner() {
        this.setState({ visible: false });
    }

    goBackHome = () => {
        let KEY = this.state.push_key
        let TIME = Date.now()
        this.props.navigation.state.params.backItem(TIME, KEY);
        this.props.navigation.goBack()
    }

    goForward = () => {
        let MARKETID=  this.props.navigation.state.params.item.market_key
        let KEY = this.state.push_key
        let URL = this.state.currentURL

        if (this.webview && this.state.canGoForward) {
            this.webview.goForward();
            DATA = {
                urlpath:URL,
                timestamp : Date.now()
            }
            
            firebase.database().ref(`marketplace_engagement/${MARKETID}/${KEY}/interactions/`).push(DATA)
        }
    };
    
    // go back to the last page
    goBack = () => {
        let MARKETID=  this.props.navigation.state.params.item.market_key
        let KEY = this.state.push_key
        let URL = this.state.currentURL


        if (this.webview && this.state.canGoBack) {
            this.webview.goBack();

            DATA = {
            urlpath:URL,
            timestamp : Date.now()
        }
        
        firebase.database().ref(`marketplace_engagement/${MARKETID}/${KEY}/interactions/`).push(DATA)

        }
    };

    saveURL = () => {
        let MARKETID=  this.props.navigation.state.params.item.market_key
        let KEY = this.state.push_key
        let URL = this.state.currentURL
        let INTIAL = this.props.navigation.state.params.item.link


        if (URL !== INTIAL) {
            // this.webview.goBack();

            DATA = {
            urlpath:URL,
            timestamp : Date.now()
        }
        
        firebase.database().ref(`marketplace_engagement/${MARKETID}/${KEY}/interactions/`).push(DATA)

        }
    }

    onNavigationStateChangeVersion2(webViewState){
       
        if (webViewState.canGoBack === true) {
          this.setState({ canGoBack: true, currentURL : webViewState.url });
          this.saveURL()
        } else {
          this.setState({ currentURL : webViewState.url });
          this.saveURL()
        }

        if (webViewState.canGoForward === true) {
          this.setState({ canGoForward: true, currentURL : webViewState.url });
        }
    
    }

    render(){
      let bottomBut

      if(this.state.canGoBack === true && this.state.canGoForward === true){
        bottomBut = 
        <View style={{flexDirection:'row', padding:10}}>
          <Icon name='left' size={18}  color='#080880' onPress={this.goBack} style={{marginLeft:30}} />
          <Icon name='right' size={18} color='#080880' style={{marginLeft:60}}  onPress={this.goForward} />
        </View>

      }
      if(this.state.canGoBack === true && this.state.canGoForward === false){
        bottomBut = 
        <View style={{flexDirection:'row', padding:10}}>
          <Icon name='left' size={18} color='#080880' onPress={this.goBack} style={{marginLeft:30}} />
          {/* <Icon name='right' size={15} style={{marginLeft:15}}  onPress={this.goForward} /> */}
        </View>
      }
      if(this.state.canGoBack === false && this.state.canGoForward === true){
        bottomBut = 
        <View style={{flexDirection:'row', padding:10}}>
          <Icon name='left' size={18}  onPress={this.goBack} color='#ccc' style={{marginLeft:30}} />
          <Icon name='right' size={18} color='#080880' style={{marginLeft:60}}  onPress={this.goForward} />
        </View>

      }

    //   const injectedJs = `
    //   window.postMessage(window.location.href);
    // `;



        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#080880'}} >
            <View style={{backgroundColor:'#fafdff', flex:1}}>
              <View style={styles.backButton} >
              <TouchableOpacity style={{marginLeft:5}}  onPress={this.goBackHome}>
                <FIcon name='chevron-circle-left' size={30} color='#fff'  />
                {/* <Text style={{color:'white', fontFamily: "ProximaNova-Bold", marginLeft:14}}> Back </Text> */}
              </TouchableOpacity>

              <View style={{backgroundColor:'white',flexDirection:'row', alignItems:'center', borderRadius:50,padding:10,paddingLeft:20, paddingRight:20,}}>
                <Text style={{fontSize:12, fontFamily:'ProximaNova-Bold'}}>Powered by</Text>
                <Image source={{uri: this.props.navigation.state.params.item.logo}} style={{height:20, width:40, marginLeft:10, resizeMode:'contain'}} />
              </View>
              </View>
              
      
      
              <WebView
              useWebKit={true}
                onLoad={() => this.hideSpinner()}
                style={{ flex: 1 }}
                key={this.state.key}
                ref={webview => (this.webview = webview)}
                source={{ uri: this.props.navigation.state.params.item.link }}
                onNavigationStateChange={this.onNavigationStateChangeVersion2.bind(this)}
                
              />
      
      
              {this.state.visible && (
                <ActivityIndicator
                  color="#080880"
                  style={{ position: "absolute", top: height / 2, left: width / 2 }}
                  size="large"
                />
              )}

              {bottomBut}
      
              </View>
            </SafeAreaView>
          );
        }
      }
      
WebviewMarketplace = inject('mobx_auth', 'mobx_config')(observer(WebviewMarketplace))
export default WebviewMarketplace      

const styles = StyleSheet.create({
    backButton:{
        borderRadius:50,
        margin:10,
        // alignSelf:'flex-start',
        padding:5,
        backgroundColor:'#080880',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:"space-between"
      },
})