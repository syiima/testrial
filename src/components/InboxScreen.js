import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,RefreshControl, Modal, Alert,Dimensions, TouchableWithoutFeedback, TouchableOpacity, ScrollView, Image, Button, TextInput, Linking} from "react-native"
import firebase from '@react-native-firebase/app';
import { inject, observer, Provider } from 'mobx-react';
import Moment from 'moment';

import Header from "./Header.js"

class InboxScreen extends React.Component{
    constructor(){
        super()
        this.state = {
            data: '',
            isLoading : false,
        }
    }
    
    componentDidMount(){
        this.fetchData()
    }
    
    fetchData(){
        let x = this.props.navigation.state.params.headerTitle
        let that = this
        let FUID = this.props.mobx_auth.FUID
        let DB 
        let NOTI = []

        if(x === 'Announcements'){ DB = `announcement` } else { DB = `users/${FUID}/personal_notification`}
        
        firebase.database().ref(DB).once('value', (snp) => {
            if(snp.exists()){
                let x = snp.val()

                KEY = Object.keys(snp.val());

                KEY.forEach( (key_id) => {
                  let a = snp.val()[key_id];
                  a.key = key_id;
                //   if(a.unread !== undefined && a.unread === false){
                    // that.setState({got_noti: true})
                    // }
                NOTI.push(a);

                })

                let SORTED = NOTI.sort((a,b) => b.timestamp - a.timestamp)
                that.setState({data:SORTED, isLoading :false});
                 
            } else {
                that.setState({isLoading: false})
            }
        }) 
    }

    renderContent = () => {
        let DATA = this.state.data
        let display 
    
        if(DATA !== ''){
            display = DATA.map((item) => (
                <TouchableWithoutFeedback>
                    <View style={styles.box}>
                        <View>
                        <Text style={styles.topText}>{item.title}  </Text>
                            <Text style={styles.bottomText}>{item.body}  </Text>
                            <View >
                            <Text style={styles.timeText}>{Moment(item.timestamp).format("DD MMM")} at {Moment(item.timestamp).format('HH:MM A')} </Text>
                            {item.unread !== undefined && item.unread === false ? <View style={{height:10, width:10, borderRadius:5, backgroundColor:'red', position:'absolute', right:4 }} /> : null }
                            </View>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            ))
        }
    
        return display
    }

    render(){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#080880'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                    
                    <Header backButton={true} headerTitle={this.props.navigation.state.params.headerTitle}  onPress={() => this.props.navigation.goBack()} />

                   <ScrollView style={{padding:15}} showsVerticalScrollIndicator={false} >
                    {this.renderContent()}

                    <View style={{height: 25}}/>
                   </ScrollView>
                    
                </View>
            </SafeAreaView>
        )
    }
}

InboxScreen = inject('mobx_auth',)(observer(InboxScreen))
export default InboxScreen

const styles = StyleSheet.create({
    box: { flexDirection:'row', alignItems:'center', borderRadius:10, marginBottom:15, padding:15,backgroundColor:'white',
    ...Platform.select({
        ios: {
          shadowColor: '#000',
          shadowRadius: 2.22,
          shadowOffset: { width: 0, height: 1 },
          shadowOpacity: 0.22,
        },
        android: {
          elevation: 3
        },
       }),
    },
    topText :{fontSize:16, fontFamily: 'ProximaNova-Bold',marginBottom: 6},
    bottomText:{fontSize: 13, color:'#363434', marginBottom:10},
    timeText:{color:'grey', fontSize:11}
})