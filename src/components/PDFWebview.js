
import React from 'react';
import { StyleSheet, Dimensions, View,TouchableWithoutFeedback,Text, SafeAreaView } from 'react-native';
import Pdf from 'react-native-pdf';
import MaterialIcon from 'react-native-vector-icons/FontAwesome';

import Header  from './Header';

class PDFWebview extends React.Component{

    constructor(props) {
        super(props);
        this.state = {

        };
      }



  render() {
      const source= {uri: this.props.navigation.state.params.link}
    return (

        <SafeAreaView style={{flex:1, backgroundColor:'#080880'}} >
          <View style={{backgroundColor:'#fff', flex:1}}>
            <Header backButton = {true}  headerTitle = 'Insurance' onPress={() => this.props.navigation.goBack()} />
        
                <Pdf
                    source={source}
                    onLoadComplete={(numberOfPages,filePath)=>{
                        console.log(`number of pages: ${numberOfPages}`);
                    }}
                    onPageChanged={(page,numberOfPages)=>{
                        console.log(`current page: ${page}`);
                    }}
                    onError={(error)=>{
                        // console.log(error);
                        Alert.alert('Sorry', 'An error occured. Please contact SERV Team if the issue still persists');
                    }}
                    style={styles.pdf}/>

              </View>
            </SafeAreaView>
    )
  }
}


export default PDFWebview

const styles = {
    top:{
        backgroundColor:'#080880',
        padding:20,
        borderBottomLeftRadius:50,
        flexDirection:'row',
        alignItems:'center'
      },
    pdf: {
        flex:1,
        width:Dimensions.get('window').width,
    },

  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  overlay: {
    position: 'absolute',
    padding: 16,
    right: 0,
    left: 0,
    alignItems: 'center'
  },

};;
