import React from "react"
import { StyleSheet, Text, View,Dimensions, TouchableWithoutFeedback, } from "react-native"

class BorderButton extends React.Component{
    render(){
        return(
            <TouchableWithoutFeedback onPress={this.props.onPress} >
                <View style={{...styles.button, borderColor:this.props.borderColor, width:this.props.width, marginTop:this.props.marginTop }}>
                <Text style={{...styles.text, color:this.props.borderColor}} >{this.props.title}</Text>
            </View>
            </TouchableWithoutFeedback>
        )
    }
}

export default BorderButton

const styles = StyleSheet.create({
    button:{
        borderRadius:5,
        padding:15,
        alignItems:'center',
        backgroundColor:'#fff',
        borderWidth:1,
        alignSelf:'center'
    }, 
    text:{
        fontFamily:'ProximaNova-Bold', 
        fontSize:18,  
        textAlign:'center',

    },
})