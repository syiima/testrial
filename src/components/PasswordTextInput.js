import React, { Component } from 'react'
import { View, Text, TextInput } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

class PasswordTextInput extends Component {

   
  render() {
  
    return (
      <View style={{marginBottom:20}} >
        <Text style={{fontWeight:'bold', fontSize:13,marginBottom:10, color:'#686868' }}>{this.props.title}</Text>

        <View style={{padding:(Platform.OS === 'ios') ? 10 : 5,margin: 0, borderWidth:1, borderColor:'#a7a7a7',borderRadius:5, flexDirection:'row', alignItems:'center' }}>
            <TextInput 
                placeholder={this.props.placeholder}
                secureTextEntry ={this.props.hidePassword}
                placeholderTextColor="#a7a7a7"
                autoCorrect={false}
                autoCapitalize = "none" 
                underlineColorAndroid="transparent" 
                onChangeText={this.props.onChangeText}
                value={this.props.value}
                style={{fontFamily:'ProximaNova-Regular', flex:3, padding:0}}
                returnKeyType={this.props.returnKeyType ? this.props.returnKeyType : 'next'}
                onSubmitEditing={this.props.onSubmitEditing}
                ref={this.props.inputRef}
            />
            <View style={{paddingRight:4,flex:0.5, alignItems: 'flex-end', justifyContent: 'flex-end', }}>
                <Icon name={this.props.hidePassword  === true ? "eye-slash" : "eye"} size={18} color="#a7a7a7"  onPress={this.props.onPress} activeOpacity = { 0.8 } />
            </View>
        </View>
        {this.props.bottom === true ? <Text style={{color:'#686868', fontSize:12, marginTop:5}}>Must be at least 6 characters.</Text> : null}

       
      </View>
      );
  }
}

export default PasswordTextInput;
