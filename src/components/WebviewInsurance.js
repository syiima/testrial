import React, { Component } from 'react';
import{Text, View, SafeAreaView, Dimensions, ActivityIndicator,Modal, TextInput, TouchableOpacity,StyleSheet,Keyboard, KeyboardAvoidingView} from 'react-native';
import { WebView } from 'react-native-webview';
import { inject, observer, Provider } from 'mobx-react';
import {X} from '../util/Config';
import firebase from '@react-native-firebase/app';

import {faCoffee, faLaughBeam,faSmile, faFrown } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'

import Icon from 'react-native-vector-icons/AntDesign'
const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

class Insurance extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      visible: true,
      feedbackModel: false,

      rate: '',
      merchID: '',
      comment: '' };
  }

  hideSpinner() {
    this.setState({ visible: false });
  }


  goBack = () => {this.setState({feedbackModel: true})}

  pwafeedback = () => {
    let CATEGORY = this.props.navigation.state.params.category
    let COMP = this.props.navigation.state.params.company
    this.setState({feedbackModel: false})

    firebase.database().ref(`pwa_feedback/`).push({
      rate: this.state.rate,
      text: this.state.comment,
      timestamp: Date.now(),
      category:CATEGORY,
      company: COMP,
      market_id : '',
      user: this.props.mobx_auth.FUID

    })

    this.props.navigation.goBack(null)
    firebase.analytics().logScreenView({
      screen_name: 'Homepage',
      screen_class: 'Homepage',
    })
  
  }
  



  render() {

    return (
      <SafeAreaView style={{flex:1, backgroundColor:'#080880'}} >
        <View style={{backgroundColor:'#fff', flex:1}}>
        
        <TouchableOpacity style={styles.backButton} onPress={this.goBack}>
          <Icon name='leftcircle' size={18} color='#fff'  />
          <Text style={{color:'#fff', fontFamily: "ProximaNova-Bold", marginLeft:14}}> Back </Text>
        </TouchableOpacity>

        <Modal 
          transparent={true}
          animationType="fade"
          visible={this.state.feedbackModel}>
            <KeyboardAvoidingView
              behavior={(Platform.OS === 'ios') ? "padding" : null} enabled
              style={{ flex: 1, }}
              >
            <SafeAreaView style={styles.modalBackground} >
              <View style={styles.modal} >

                <Text style={{fontFamily:'ProximaNova-Bold', fontSize:20, marginBottom:15, marginTop:10, textAlign:'center'}}> Rate us</Text>
                <Text style={{fontFamily:'ProximaNova-Bold', fontSize:15, marginBottom:15, textAlign:'center'}}>We love to hear from you! How was your experience ?</Text>
                
                <View style={{flexDirection:'row', justifyContent:'space-around', alignItems:'center'}}>
                  <FontAwesomeIcon icon={ faSmile } color={this.state.rate === 'good' ? '#FAD440' : 'grey'} size={50} onPress={() => this.setState({rate:'good'}) } />
                  <FontAwesomeIcon icon={ faFrown } color={this.state.rate === 'bad' ? 'tomato' : 'grey'}  size={50} onPress={() => this.setState({rate:'bad'}) }/>
                </View>
                

                <View style={styles.boxy}>
                  <TextInput 
                    multiline = {true}  
                    value={this.state.comment} 
                    onChangeText={(x)=>this.setState({comment:x})} 
                    underlineColorAndroid="transparent" 
                    placeholder="What can we improve? (Optional)" 
                    onSubmitEditing={()=>{Keyboard.dismiss()}}
                    placeholderTextColor= 'grey'/>
                </View>
                

                <View style={{marginTop:15, }}>
                  <TouchableOpacity onPress={this.pwafeedback} style={{ padding:15,marginBottom:10, alignItems:'center'}}>
                    <Text style={{color:'#080880', fontSize:15, fontFamily: "ProximaNova-Bold"}}>Submit</Text>
                  </TouchableOpacity>
                  
                </View>

              </View>
            </SafeAreaView>
            </KeyboardAvoidingView>
        </Modal>

        <WebView
          useWebKit={true}
            onLoad={() => this.hideSpinner()}
            style={{ flex: 1 }}
            source={{ uri: this.props.navigation.state.params.url_link}}
          />


        {this.state.visible && (
          <ActivityIndicator
            color="#080880"
            style={{ position: "absolute", top: height / 2, left: width / 2 }}
            size="large"
          />
        )}

          </View>
      </SafeAreaView>
    );
  }
}

Insurance = inject('mobx_auth', 'mobx_config')(observer(Insurance))
export default Insurance



const styles = StyleSheet.create({
  backButton:{
    borderRadius:50,
    margin:10,
    alignSelf:'flex-start',
    padding:10,paddingRight:20, paddingLeft:20,
    backgroundColor:'#080880',
    flexDirection:'row',
    alignItems:'center',
    justifyContent:"center"
  },
  //modal
  modalBackground: {
    flex: 1,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  modal: {
    // elevation: 20,
    borderRadius: 20,
    width:'90%',
    padding:15,
    backgroundColor: '#fff',
  },
  boxy: {
    borderWidth:0.5, padding:(Platform.OS === 'ios') ? 10 : null, paddingLeft:(Platform.OS === 'ios') ? null : 10, marginTop:15, borderColor:'#ccc', borderRadius:20,
  }
})
