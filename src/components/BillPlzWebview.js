import React, {Component} from 'react';
import {View,SafeAreaView,TouchableOpacity, Text, StyleSheet} from 'react-native';
import { inject, observer, Provider } from 'mobx-react';
import { credentials } from "../util/BillPlzCredentials.js"
import WebView from 'react-native-webview'
import Icon from 'react-native-vector-icons/AntDesign'

class BillPlzWebview extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      current_webview_url: '',
      total_url: [],
    }
  }

  _onNavigationStateChangeVersion2(webViewState){
   
  }

  goback = () => {
    this.props.navigation.goBack(null)
    this.props.navigation.state.params.backBut()
  }

  render(){
    return(
      <SafeAreaView style={{flex:1, backgroundColor:'#080880'}}>
            <View style={{flex:1, backgroundColor:'#fff'}}>
                <TouchableOpacity style={styles.backButton} onPress={this.goback}>
                    <Icon name='leftcircle' size={18} color='#fff'  />
                    <Text style={{color:'#fff', fontFamily: "ProximaNova-Bold", marginLeft:14}}> Back </Text>
                </TouchableOpacity>
                <WebView
                    useWebKit={true}
                    source={{
                        uri: this.props.navigation.state.params.url_link,
                        headers: { Authorization: credentials.header_auth }
                        }}
                    ref="webview"
                    onNavigationStateChange={this._onNavigationStateChangeVersion2.bind(this)}
                />
            </View>
      
      </SafeAreaView>
    )
  }
}

BillPlzWebview = inject('mobx_auth', 'mobx_config')(observer(BillPlzWebview))
export default BillPlzWebview;

const styles = StyleSheet.create({
   
  backButton:{
      borderRadius:50,
      alignSelf:'flex-start',
      margin:10,
      padding:10,paddingRight:20, paddingLeft:20,
      backgroundColor:'#080880',
      flexDirection:'row',
      alignItems:'center',
      justifyContent:"center"
    },

})
