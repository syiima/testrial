import React from "react";
import {View, Text,SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions} from "react-native";

import Header from "../components/Header";

const WIDTH = Dimensions.get('window').width
const HEIGHT = Dimensions.get('window').height

export default class MarketingSecContent extends React.Component{ 
    
    navigate = (data_content) => {
        if(data_content.link !== '') {
            let LINK = data_content.link
            this.props.navigation.navigate('Webview', {link: LINK})
        } 
    }

    render() {
        // console.warn('child refesh');
        let data_content = this.props.navigation.state.params.data
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true} headerTitle='Details'  onPress={() => this.props.navigation.goBack()}/>
                <Image source={{uri:data_content.image}} style={styles.imageBody}></Image>
                    <View style={{margin:20}}>
                        <Text style={styles.title}>{data_content.header}</Text>
                        <Text style={styles.desc}>{data_content.body}</Text>
                    </View>
                {data_content.link !== undefined && data_content.link !== '' ? 
                    <TouchableOpacity style={styles.buttonSee} onPress={() => this.navigate(data_content)}>
                    <Text style={styles.text}>Learn more</Text>
                </TouchableOpacity>
                : null}
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    title:{
        fontFamily: "ProximaNova-Bold",
        fontSize:30,
        textAlign:'justify',
        marginBottom: 20, 
        // paddingTop:20
    },
    desc:{
        fontSize:15,
        textAlign:'justify',
        marginBottom: 20, 
    },
    imageBody: {
        width:WIDTH - 30,
        height:(WIDTH - 30) /2, 
        borderRadius: 10,
        // borderWidth:1,
        resizeMode:'contain',
        marginTop:20,
        alignSelf:'center'
    }, 
    buttonSee:{
        borderRadius:5,
        padding:15,
        borderWidth:2,
        backgroundColor:'#000080',
        width:WIDTH - 30,
        alignItems:'center',
        alignSelf: 'center',
        position:'absolute',
        bottom:20,
    }, 
    text:{
        fontFamily:'ProximaNova-Bold', 
        fontSize:18, 
        color:'#fff', 
        textAlign:'center'
    },

})