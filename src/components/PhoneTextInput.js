import React, { Component } from 'react'
import { View, Text, TextInput } from 'react-native'


class PhoneTextInput extends Component {

  render() {
  
    return (
      <View style={{marginBottom:20}} >
        <Text style={{fontWeight:'bold', fontSize:13,marginBottom:10, color: this.props.color ? this.props.color : '#686868' }}>{this.props.title}</Text>

        <View style={{borderWidth:1, borderColor:'#a7a7a7',borderRadius:5 , flexDirection:'row'}}>
            <View style={{borderRightWidth:1, borderRightColor:'#a7a7a7',}}>
                <Text style={{color:'#686868', fontWeight:'bold', padding:10}} >+60</Text>
            </View>
            <View style={{width: '80%', marginLeft:10, justifyContent:'center'}} >
            <TextInput 
                placeholder={this.props.placeholder}
                placeholderTextColor={this.props.placeholderTextColor ?  this.props.placeholderTextColor : "#a7a7a7"}
                keyboardType='number-pad'
                multiline = {false}
                autoCorrect={false}
                autoCapitalize = "none" 
                underlineColorAndroid="transparent" 
                onChangeText={this.props.onChangeText}
                value={this.props.value}
                style={{fontFamily:'ProximaNova-Regular',padding:0,  }}
                returnKeyType={this.props.returnKeyType ? this.props.returnKeyType : 'next'}
                onSubmitEditing={this.props.onSubmitEditing}
                ref={this.props.inputRef}
            />
            </View>
            
        </View>
       
      </View>
      );
  }
}

export default PhoneTextInput;
