import React, {Component} from 'react';
import {StyleSheet,Text, Alert,View, SafeAreaView,TouchableOpacity, TouchableWithoutFeedback,} from 'react-native';
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'

class WorkshopQuotationConditional extends React.Component{
    constructor(){
        super()
        this.state = {
            collapsed : true,
        }
    }


    toggleExpanded = () => {
        //Toggling the state of single Collapsible
        this.setState({ collapsed: !this.state.collapsed });
    };

    popOut = (index) => {
        // let UID = this.props.mobx_auth.FUID
        let BOOKING_ID = this.props.mobx_retail.R_WALKIN_BOOKID
        let DETAILS = this.props.item.masterItems // semua data 
        let WS_ID = this.props.mobx_retail.R_ID 
        let WSMAIN_ID = this.props.mobx_retail.R_GROUPID
       
        firebase.database().ref(`request_walkin/${WSMAIN_ID}/${WS_ID}/${BOOKING_ID}/quotation/masterItems/${index}`).update({
            added: false
        }).then(() => {
            this.props.passrefresh()
        })
    }

    popIn = (index) => {
        // let UID = this.props.mobx_auth.FUID
        let BOOKING_ID = this.props.mobx_retail.R_WALKIN_BOOKID
        let DETAILS = this.props.item.masterItems // semua data 
        let WS_ID = this.props.mobx_retail.R_ID 
        let WSMAIN_ID = this.props.mobx_retail.R_GROUPID
        
        firebase.database().ref(`request_walkin/${WSMAIN_ID}/${WS_ID}/${BOOKING_ID}/quotation/masterItems/${index}`).update({
            added: true
       }).then(() => {
        this.props.passrefresh()
       })
      
    }
    renderContent = () => {
        let DATA = this.props.item.masterItems
        let KEYS = Object.keys(DATA)
        let display;
        let SERV = []
        for (let i = 0; i < DATA.length; i++) {
            let a = DATA[i]
            a.initialIndex = i
            if(a.added === false) {
                SERV.push(a)
               
             } else {
                 SERV.unshift(a) 
             }    
                        
        }

        if(this.props.status !== 'Requesting' && this.props.status !== 'Pending'){
            display = SERV.map((item, index) => (
                item.added === true || item.added === undefined ? 
                
                <View style={index !== 0 ?  styles.norm : styles.last}>
                    <View style={{flexDirection:'row', flexShrink:1, justifyContent:'space-between', }}>
                            <Text style={styles.boldText}>{item.item.charAt(0) + item.item.slice(1)}</Text>
                                <View style={{flexDirection:'row', marginLeft:10}}>
                                    <Text style={ styles.textrm}>RM </Text>
                                    <Text style={styles.textPrice }>{item.price.toFixed(2)}</Text>
                                </View>     
                        </View>
    
                        <TouchableWithoutFeedback onPress={() => this.popOut(item.initialIndex)}>
                        <View style={{flexDirection:'row', flexShrink:1,  justifyContent:'space-between',}}>
                                    <View style={{marginTop:15,marginRight:10,  flex:2.3,}}>  
                                        <Text style={styles.textQuant}>Quantity: {item.quantity} </Text>
                                        <Text style={styles.textWarranty }>Description : {item.description !== '' ? item.description : 'Not Available'}</Text>
                                        <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                                        {item.warranty !== undefined && item.warranty !== 'No warranty'  ?  
                                            <View>
                                                <Text style={styles.textWarranty }>Warranty : {item.warranty}</Text>
                                            </View> 
                                        : null}
                                        
                                        </View>
                                    </View>
    
                                </View>
                        </TouchableWithoutFeedback> 
                </View>
                : 
                null
    
    
            )) 
        } else {
            display = SERV.map((item, index) => (
                item.added === true || item.added === undefined ? 
                <TouchableWithoutFeedback onPress={() => this.popOut(item.initialIndex)}>
                <View style={index !== 0 ?  styles.norm : styles.last}>
                    <View style={{flexDirection:'row', flexShrink:1, justifyContent:'space-between', }}>
                            <Text style={styles.boldText}>{item.item.charAt(0) + item.item.slice(1)}</Text>
                                <View style={{flexDirection:'row', marginLeft:10}}>
                                    <Text style={ styles.textrm}>RM </Text>
                                    <Text style={styles.textPrice }>{item.price.toFixed(2)}</Text>
                                </View>     
                    </View>

                    <View style={{flexDirection:'row', flexShrink:1,  justifyContent:'space-between',}}>
                        <View style={{marginTop:15,marginRight:10,  flex:2.3,}}>  
                            <Text style={styles.textQuant}>Quantity: {item.quantity} </Text>
                            <Text style={styles.textWarranty }>Description : {item.description !== '' ? item.description : 'Not Available'}</Text>
                            <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                            {item.warranty !== undefined && item.warranty !== 'No warranty'  ?  
                                <View>
                                    <Text style={styles.textWarranty }>Warranty : {item.warranty}</Text>
                                </View> 
                            : null}
                            
                            </View>
                        </View>    
                        <Text style={{color:'#C8C2C1',alignSelf:'flex-end', fontFamily:'ProximaNova-Bold'}}>Remove</Text>
                    </View>
                </View>
                </TouchableWithoutFeedback> 

                : 
                <TouchableWithoutFeedback  onPress={() => this.popIn(item.initialIndex)}>
                <View style={index !== 0 ?  styles.norm : styles.last}>
                    <View style={{flexDirection:'row', flexShrink:1, justifyContent:'space-between', }}>
                        <Text style={styles.greyText}>{item.item.charAt(0) + item.item.slice(1)}</Text>
                            <View style={{flexDirection:'row', marginLeft:10}}>
                                <Text style={styles.txtGreyRM}>RM </Text>
                                <Text style={styles.txtPriceGrey }>{item.price.toFixed(2)}</Text>
                            </View>     
                    </View>
    
                    <View style={{flexDirection:'row', flexShrink:1, justifyContent:'space-between', }}>
                        <View style={{marginTop:15,marginRight:10, flex:2.3}}>  
                            <Text style={styles.txtQuntGrey}>Quantity: {item.quantity} </Text>
                            <Text style={ styles.txtWarrGrey}>Description : {item.description !== '' ? item.description : 'Not Available'}</Text>
                            <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                            {item.warranty !== undefined && item.warranty !== 'No warranty'  ?  
                                <View>
                                    <Text style={styles.txtWarrGrey }>Warranty : {item.warranty}</Text>
                                </View> 
                            : null}
                            
                            </View>
                        </View>
                        <Text style={{ fontFamily:'ProximaNova-Bold', alignSelf:'flex-end',}}>Add</Text>

                    </View>
                </View>
                </TouchableWithoutFeedback>

            )) 
        }

       
        
        return display

    }

    calcSub = () => {
        let X = this.props.item.masterItems
        let sum = 0

        for (let i = 0; i < X.length; i++) {
            // console.warn('price', X[i].price)
            let a = X[i]
            if(a.added === true || a.added === undefined){
                sum += a.price
            }  
            
            
        }
        
        return sum
    }


    render(){
        return(
            <View>
                
            {/* <TouchableWithoutFeedback onPress={this.toggleExpanded}> */}
            {this.props.status !== 'Requesting' && this.props.status !== 'Pending' ?
                <View style={{flexDirection:'row', marginBottom:15,  justifyContent:'space-between',}}>
                    <Text style={{fontSize:17, fontFamily:'ProximaNova-Bold'}}>Request Details</Text>
                    {/* {this.state.collapsed === false 
                        ?   <AntIcon name='up' size={15}/>
                        :   <AntIcon name='right' size={15}/> 
                    } */}
                    
                </View> 
            : null }
            {/* </TouchableWithoutFeedback>    */}

            {/* <Collapsible collapsed={this.state.collapsed} align="center" > */}
                {this.renderContent()}


                {this.props.status !== 'Requesting' && this.props.status !== 'Pending' ?
                 <View style={{borderTopWidth:1, borderTopColor:'#080880',marginTop:20}}>
                    <View style={{flexDirection:'row', justifyContent:'space-between', padding:5}}>
                        <Text >Subtotal</Text>
                        <View style={{flexDirection:'row'}}>
                            <Text style={{fontSize:12}}>RM</Text>
                            <Text style={{ fontSize:20}}>{(this.calcSub()).toFixed(2)}</Text>
                        </View>
                    </View>
                    <View style={{flexDirection:'row', justifyContent:'space-between',  padding:5}}>
                        <Text >Service Tax @ {this.props.item.tax} % </Text>
                        <View style={{flexDirection:'row'}}>
                            <Text style={{fontSize:12}}>RM</Text>
                            <Text style={{ fontSize:20}}>{(this.props.item.tax / 100 * this.calcSub()).toFixed(2)}</Text>
                        </View>
                    </View>
                    <View style={{flexDirection:'row', justifyContent:'space-between', padding:5}}>
                        <Text >Discount</Text>
                        <View style={{flexDirection:'row'}}>
                            <Text style={{fontSize:12}}>RM</Text>
                            <Text style={{fontSize:20}}>{this.props.item.discount.toFixed(2)}</Text>
                        </View>
                    </View>
                    {this.props.item.deposit !== undefined ?
                    <View style={{flexDirection:'row', justifyContent:'space-between', padding:5}}>
                        <Text >Deposit</Text>
                        <View style={{flexDirection:'row'}}>
                            <Text style={{fontSize:12}}>RM</Text>
                            <Text style={{fontSize:20}}>{this.props.item.deposit.toFixed(2)}</Text>
                        </View>
                    </View>
                    : null }

                    {this.props.item.discount_note !== undefined && this.props.item.discount_note !== ''  ? 
                        <View style={{flexDirection:'row', justifyContent:'space-between', padding:5}}>
                            <Text >Discount notes : {this.props.item.discount_note} </Text>  
                        </View> 
                    : null}


                </View>
                : null}

                {/* </Collapsible> */}
               
            </View>
        )
    }
}

WorkshopQuotationConditional = inject('mobx_auth','mobx_retail')(observer(WorkshopQuotationConditional))
export default WorkshopQuotationConditional


const styles = StyleSheet.create({
    boldText :{fontFamily:'ProximaNova-Bold'},
    norm :{paddingTop:10, borderTopColor:'#080880', borderTopWidth:1, marginBottom:10},
    last:{marginTop:10,marginBottom:10},

    textrm: {fontSize:11, fontFamily: "ProximaNova-Bold",},
    textPrice: {fontSize:18, fontFamily: "ProximaNova-Bold",},
    textQuant: {fontSize: 12},
    textWarranty: {fontSize:12, marginTop:5},

    greyText :{color:'#C8C2C1',fontFamily: "ProximaNova-Bold",},
    txtGreyRM: {color:'#C8C2C1',fontSize:11, fontFamily: "ProximaNova-Bold",},
    txtPriceGrey: {color:'#C8C2C1',fontSize:18, fontFamily: "ProximaNova-Bold",},
    txtWarrGrey:{color:'#C8C2C1',fontSize:12, marginTop:5},
    txtQuntGrey:{color:'#C8C2C1',fontSize:12},

})