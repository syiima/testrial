import React from 'react';
import {ScrollView, View, Image, TouchableWithoutFeedback, StyleSheet,Dimensions}  from 'react-native'
import firebase from '@react-native-firebase/app';

const WIDTH = Dimensions.get('window').width
const constImages = [{
    banner_image: require('../assets/banner/Directlending.png'),
    id: 'directLending',
    link: 'https://www.directlending.com.my/serv/servApply.html'
},{
    banner_image: require('../assets/banner/Tngbanner.png'),
    id: 'tng',
    link: 'https://www.touchngo.com.my/terms-conditions/serv-with-tng-ewallet-rm15-cashback'
},{
    banner_image: require('../assets/banner/feedback.png'),
    id: 'feedback',
    link: ''
}]

export default class HomeBannerCarousel extends React.Component{
    scrollRef = React.createRef()
    constructor(){
        super()
        this.state = {
            selectedIndex: 0,
            images:[],
        }
    }


    componentDidMount(){
        this.getImgMarketing()
    }

    getImgMarketing = () => {
        let y = []
        //let imgResult = []; // declare new array to get all image from firebase storage.
        firebase.database().ref(`marketing_banner/`).once('value', snapshot => {
            if(snapshot.exists()){
                snapshot.forEach((child) =>{
                    let DATA = child.val();
                    y.push(DATA)
                })

                let SORTED = y.sort((a,b) => b.timestamp - a.timestamp)
                this.setState({images: SORTED})
                
            } else {
                this.setState({images: []})
            }
                // console.warn("HAIIII", y)
        }).then(() => {
            this.timeSet()
        })
    } 



    timeSet = () => {
        let DATA

        if(this.state.images.length !== 0){
            DATA = this.state.images
        } else {
            DATA = constImages
        } 

        setInterval(() => {
            this.setState(prev => ({selectedIndex: 
                prev.selectedIndex === DATA.length - 1 
                ? 0 
                : prev.selectedIndex + 1
            }),
            () => {
                this.scrollRef.current.scrollTo({
                    animated: true,
                    y: 0,
                    x: WIDTH * this.state.selectedIndex
                })
            })
        }, 4000);
    }


    setSelectedIndex = event => {
        //width view
        const viewSize = event.nativeEvent.layoutMeasurement.width
        //current position
        const contOffset = event.nativeEvent.contentOffset.x

        const selectedIndex = Math.floor(contOffset / viewSize)

        this.setState({selectedIndex})
    }


    navigate = (x) => {
        if(x.id === 'feedback'){
            firebase.analytics().logScreenView({
                screen_name: 'ImproveServ_homepage',
                screen_class: 'ImproveServ_homepage',
              })
            this.props.navigation.navigate('Settings')
        } else {
            let LINK = x.link
            this.props.navigation.navigate('Webview', {link: LINK})
        }
    }

    renderBanner = () => {
        let display
        let DATA, hd

        if(this.state.images.length !== 0){
            DATA = this.state.images
        } else {

            DATA = constImages
            hd = true
        }

        display = DATA.map(item => (
            <TouchableWithoutFeedback onPress={() => this.navigate(item)} >
            <View style={styles.banner}>
                {hd === true ? <Image source={item.banner_image} style={styles.image} /> :  <Image source={{uri:item.banner_image}} style={styles.image} />}
            </View>
            </TouchableWithoutFeedback>
        ))
        return display
    }

    renderButton = () => {
        let display
        let DATA

        if(this.state.images.length !== 0){
            DATA = this.state.images
        } else {
            DATA = constImages
        }
        
        display = DATA.map((item,i) => (
            <View
                key={i}
                style={i === this.state.selectedIndex ? styles.active : styles.inactive}
                />
            )) 

        return display
    }

    render(){
        return(
            <View style={{paddingBottom: 15}}>
                <ScrollView 
                    horizontal ={true} 
                    pagingEnabled ={true} 
                    showsHorizontalScrollIndicator={false} 
                    onMomentumScrollEnd={this.setSelectedIndex}
                    ref={this.scrollRef}
                    >
                    
                    {this.renderBanner()}

                </ScrollView>
                <View style={styles.circleDiv}>
                    {this.renderButton()}
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    // banner:{
    //     marginRight: 7.5,
    //     marginLeft:7.5,
    //     width:WIDTH - 30, 
    //     alignSelf:'center',
    // },
    banner:{
        
        height: WIDTH / 3,
        margin:15,
        marginBottom:0,
        width:WIDTH - 30,
        marginBottom:0,
        // backgroundColor:'white',
        alignItems:'center',
        justifyContent:'center'
    },
    image :{
        width:WIDTH - 30, 
        height: WIDTH / 3, 
        resizeMode:'cover', 
        borderRadius:15,
    },

    circleDiv : {
        position: 'absolute',
        bottom: -5, 
        height:10,
        flexDirection:'row',
        justifyContent:'center',
        alignItems: 'center',
        width: '100%'
    },
    inactive: {
        height: 6, 
        width:6 ,
        borderRadius: 3,
        margin:3,
        // backgroundColor: 'rgba(250, 152, 16,0.5)'
        backgroundColor:'rgba(0,0,128, 0.5)'
    },
    active: {
        height: 6,
        width:12,
        borderRadius: 3,
        margin:3,
        backgroundColor:'#080880'
    }
})