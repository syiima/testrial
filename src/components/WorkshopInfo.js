import React from "react"
import { StyleSheet,  Text, View,Image, } from "react-native"


class WorkshopInfo extends React.Component{
    render(){
        let item = this.props.data
        return(
            <View style={styles.topBox}>
                <View style={{flexDirection:'row', alignItems:'center', padding:15}}>
                    <View style={{marginRight:15}}>
                        {this.props.logo !== '' 
                            ?  <Image source={{uri:this.props.logo}} style={{height: 60, width:60, borderRadius:30,resizeMode:'contain' }}/>
                            :  <Image source={{uri:'https://images.says.com/uploads/story_source/source_image/450525/475e.jpg'}} style={{height: 60, width:60, borderRadius:30}}/>}
                    </View>
                    <View style={{flexShrink:1}} >
                        <Text style={{fontSize:16, fontFamily:'ProximaNova-Bold'}}>{item.name}</Text>
                        <Text style={{fontSize:13}}>{item.description}</Text>
                    </View>
                </View>                
            </View>
        )
    }
}

export default WorkshopInfo

const styles = StyleSheet.create({
    topBox: {
        backgroundColor:'#fff',
        borderRadius : 15,
        marginBottom:5,
       
        
    },

})