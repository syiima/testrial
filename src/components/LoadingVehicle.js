import React from "react"
import { StyleSheet,Platform, Text, View,Dimensions,Image, } from "react-native"
import Icon from "react-native-vector-icons/AntDesign"

import vehicleEmpty from '../assets/emptyCar.png'
import addCar from '../assets/addCar.png'

class LoadingVehicle extends React.Component{
    render(){
        return(
            <View style={{flex:1,backgroundColor:'#080880', flexDirection:'row',  marginTop:20, marginBottom: 35 }}>
                
                <View style={{alignItems:'center', justifyContent:'center',marginLeft:20, marginBottom:15 }} >
                    <View style={styles.addcarstyle}>
                        <Image source={addCar} style={styles.vehicleImg}/>
                    </View>
                    <View style={styles.emptybox} />
                </View>

                <View style={{alignItems:'center', justifyContent:'center', marginBottom:15 }} >
                    <View style={{ borderRadius:10, margin:5, marginBottom:10, }}>
                        <Image source={vehicleEmpty} resizeMode="cover" borderRadius={10} style={styles.vehicleImg}/> 
                    </View>
                    <View style={styles.emptybox} />
                </View>

                <View style={{alignItems:'center', justifyContent:'center', marginBottom:15 }} >
                    <View style={{ borderRadius:10, margin:5, marginBottom:10, }}>
                        <Image source={vehicleEmpty} resizeMode="cover" borderRadius={10} style={styles.vehicleImg}/> 
                    </View>
                    <View style={styles.emptybox} />
                </View>

                <View style={{alignItems:'center', justifyContent:'center', marginBottom:15 }} >
                    <View style={{ borderRadius:10, margin:5, marginBottom:10, }}>
                        <Image source={vehicleEmpty} resizeMode="cover" borderRadius={10} style={styles.vehicleImg}/> 
                    </View>
                    <View style={styles.emptybox} />
                </View>

                
            </View>
        )
    }
}

export default LoadingVehicle

const styles = StyleSheet.create({
    addcarstyle :{
        borderRadius:10,marginBottom:10,marginRight:5,
        // borderWidth:0.5,borderColor:'#ccc',
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 2},
              shadowOpacity: 0.4,
            },
            android: {
              elevation: 2
            },
          }),

    },
    vehicleImg:{
        width: Dimensions.get('window').width /3.7,
        height:Dimensions.get('window').width /3,
        borderRadius:10,
        resizeMode:'cover',
        
    
        // backgroundColor:'#fff'
    },
    emptybox :{
        width: Dimensions.get('window').width /6,
        height:13, 
        backgroundColor:'#ccc',
    }
})