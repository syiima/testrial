import React from "react"
import {Text, View,Dimensions, TouchableWithoutFeedback, } from "react-native"

class RoundedButton extends React.Component{
    render(){
        return(
            <TouchableWithoutFeedback onPress={this.props.onPress} >
                <View style={{...styles.button, 
                backgroundColor:this.props.bgColor,
                width:this.props.width,
                marginTop: this.props.marginTop }}>
                <Text style={styles.text} >{this.props.title}</Text>
            </View>
            </TouchableWithoutFeedback>
        )
    }
}


const styles = ({
    button:{
        borderRadius:10,
        alignItems:'center',
        marginBottom:20,
        borderWidth:2,
        borderRadius:30,
        padding:12,
    }, 
    text:{
        fontFamily:'ProximaNova-Bold', 
        fontSize:18, 
        color:'#fff', 
        textAlign:'center'
    },
})

export default RoundedButton