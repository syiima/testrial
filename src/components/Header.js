import React from "react"
import { StyleSheet, Text, TouchableWithoutFeedback, View} from "react-native"
import Icon from "react-native-vector-icons/FontAwesome"

class Header extends React.Component{
    render(){
        return(
            <TouchableWithoutFeedback onPress={this.props.onPress}>
            <View style={styles.top}>
                {this.props.backButton === true ?
                    <Icon name="chevron-left" size={25} color='#fff' style={{ marginLeft:10}}/>
                 : <View style={{width:20}} /> }
                <Text style={{color:'#fff', fontFamily: "ProximaNova-Bold", fontSize:25 }}>{this.props.headerTitle}</Text>
                <View style={{width:20}} />
            </View>
            </TouchableWithoutFeedback>

        )
    }
}

export default Header

const styles = StyleSheet.create({
    top:{
        backgroundColor:'#080880',
        padding:20,
        borderBottomLeftRadius:20,
        borderBottomRightRadius: 20,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
    },
})