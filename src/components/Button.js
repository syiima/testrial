import React from "react"
import { StyleSheet, Text, View,Dimensions, TouchableWithoutFeedback, } from "react-native"

class Button extends React.Component{
    render(){
        return(
            <TouchableWithoutFeedback onPress={this.props.onPress} >
                <View style={{...styles.button,borderColor:this.props.bgColor,  backgroundColor:this.props.bgColor, width: this.props.width, marginTop:this.props.marginTop }}>
                    <Text style={styles.text} >{this.props.title}</Text>
                </View>
            </TouchableWithoutFeedback>
        )
    }
}

export default Button

const styles = StyleSheet.create({
    button:{
        borderRadius:5,
        padding:15,
        borderWidth:2,
        // width:Dimensions.get('window').width - 30,
        alignItems:'center',
        alignSelf: 'center'
    }, 
    text:{
        fontFamily:'ProximaNova-Bold', 
        fontSize:18, 
        color:'#fff', 
        textAlign:'center'
    },
})