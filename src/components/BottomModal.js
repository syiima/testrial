import React from "react"
import { StyleSheet, Text,Dimensions, Modal,SafeAreaView,TouchableWithoutFeedback,Platform, View} from "react-native"

class BottomModal extends React.Component{
    render(){
        return(
            <Modal
                transparent={true}
                visible={this.props.modalName}
                animationType="slide"
                >
                    <TouchableWithoutFeedback onPress={this.props.onPress}>
                        <SafeAreaView style={styles.bgModal}>
                            <View style={[styles.innermodal, this.props.style]}>
                                {this.props.children}
                            </View>
                        </SafeAreaView>
                    </TouchableWithoutFeedback>
            </Modal>
        )
    }
}

export default BottomModal

const styles = StyleSheet.create({
    bgModal: {
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0.4)',
      },
    innerModal: {     
        borderTopLeftRadius: 10,
        borderTopRightRadius:10,
        width:Dimensions.get('window').width,
        position:'absolute',
        bottom:0,
        backgroundColor: '#fff',
            ...Platform.select({
                ios: {
                    shadowColor: '#000',
                    shadowRadius: 2,
                    shadowOffset: { width: 0, height: 3 },
                    shadowOpacity: 0.6,
                },
                android: {
                    elevation: 2
                },
            }),
      },
      

})