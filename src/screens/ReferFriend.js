import React from "react"
import { StyleSheet, SafeAreaView,Platform,Alert,Share, Text,Dimensions, View,Image,TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/Ionicons"

import Header from '../components/Header.js'
import BorderButton from "../components/BorderButton"
import Button from '../components/Button'
import friends from '../assets/friends.png'

const WIDTH = Dimensions.get('window').width - 70

class ReferFriend extends React.Component{
    constructor(){
        super()
        this.state = {
            referal_code : '',
            isLoading:true,
            total_friends: 0,
            total_referral:'',
            copied: false,
            registered_vehicle: 0
        }
    }
    
    componentDidMount(){
        this.checkCode()
    }

    checkCode(){
        let FUID = this.props.mobx_auth.FUID

        firebase.database().ref(`users/${FUID}/referral_code`).once('value').then((snp) => {
            if(snp.exists()){
                let x = snp.val().code !== undefined ? snp.val().code : ''
  
                if(x !== ''){
                    this.getRef(x)
                }

                this.setState({referal_code : x, isLoading:false})
            } else {
                this.setState({isLoading:false})
            }
        })
    }  
    getRef(x){
        let CODE = x
        let REF = []

        firebase.database().ref(`referral_code/${CODE}/referral`).once('value').then((snp) => {
            if(snp.exists()){
                let COUNT = 0
                let VEHREG = 0

                KEY = Object.keys(snp.val());
                KEY.forEach((ref_id) => {
                    let a = snp.val()[ref_id];
                    a.ref_id = ref_id;
    
                    COUNT = COUNT + 1
                    if(a.vehicle === true){
                        VEHREG = VEHREG + 1
                    }
                    
                })

                // console.warn('hmm ', COUNT);

                this.setState({total_friends: COUNT, registered_vehicle : VEHREG})
            }
        })
    }

    createCode = () => {
        //crossCHECK dengan referral_code make sure tak sama

        let cont, disp 
        var chars = {'I':'X','O':'X', '0':'2', '1':'3'};

        let CODE = this.state.referal_code
        let NAME = this.props.mobx_auth.NAME
        let CNAME = NAME.toUpperCase()
        CNAME = CNAME.replace(/[IO]/g, m => chars[m])

        let FUID = this.props.mobx_auth.FUID
        let CFUID = FUID.toUpperCase()
        CFUID = CFUID.replace(/[IO01]/g, m => chars[m])

        let one  = CNAME.charAt(0) + CNAME.charAt(1) + CNAME.charAt(2)
        let two  = CFUID.charAt(0) + CFUID.charAt(1) + CFUID.charAt(2)
        
        getRandomInt = (max) => {
            var num = Math.floor(Math.random() * Math.floor(9));

            return (num === 0 || num === 1) ? getRandomInt(max) : num;
        }
        
        if(this.state.isLoading === false){
            if(CODE === ''){
                cont = (one).toUpperCase() + getRandomInt(5) +  (two).toUpperCase() + getRandomInt(9)

                firebase.database().ref(`users/${FUID}/referral_code`).update({
                    code: cont,
                    timestamp_created: Date.now()
                })

                firebase.database().ref(`referral_code/${cont}`).update({
                    FUID: FUID,
                    timestamp_created: Date.now()
                })

                this.setState({referal_code: cont})
            } else {
                cont = CODE
            }
        

            disp = 
            <TouchableWithoutFeedback style={{marginTop:5}} onPress={() => this.onShare('code')}>
            <View style={{flexDirection: 'row', alignItems:'center', marginTop:5}}>
                <Icon name='copy-outline' size={10} style={{marginRight:5}} />
                <Text style={{fontFamily: "ProximaNova-Bold", fontSize:16, }}  >{cont} </Text>
            </View>
            </TouchableWithoutFeedback>

            return disp
        }
    }

    onShare = async (x) => {
        let CODE = this.state.referal_code
        let LINK = 'https://bit.ly/2XbeFQF'
        // let LINK = 'http://onelink.to/h6avc5'
        let result 

        try {
            if(x === 'code'){
                result = await Share.share({
                    message:
                        `${CODE}`,
                  });
            } else {
                result = await Share.share({
                    message:
                        `Can you believe it?! 😱 \r\n\r\nYou can now book your your car service online with just a simple touch of the app!\r\n\r\nWith SERV, you can now keep track of your vehicle records, booking car services online and even renew your insurance or roadtax! There's no longer a need for long waits at the workshop or awkward conversations with the mechanic. Plus, you can call for breakdown assistance anywhere, anytime when your car has broken down in the middle of nowhere. \r\n\r\nGet SERVed today 🚗\r\n\r\nMy special code:\r\n${CODE}\r\n\r\nSign up at : ${LINK} \r\n\r\n`,
         
                  });
            }
          
          if (result.action === Share.sharedAction) {
            if (result.activityType) {
              // shared with activity type of result.activityType
            } else {
              // shared
            }
          } else if (result.action === Share.dismissedAction) {
            // dismissed
          }
        } catch (error) {
            console.warn('err', error.message);
            Alert.alert('Sorry', 'An error occured. Please contact SERV Team if the issue still persists');
        }
    };

    

    tncLink = () => {
        let LINK = 'https://serv.my/referral-program-terms-conditions/'
        this.props.navigation.navigate('Webview', {link:LINK} )
    }

    
    render(){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                    <Header backButton = {true}  headerTitle = 'Refer a friend' onPress={() => this.props.navigation.goBack()} />
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={styles.scroll}>
                            <Text style={styles.totle}>Tell friends about SERV!</Text>
                            <Image source={friends} style={{height: Dimensions.get('window').width /2.2,width:'100%',resizeMode:'contain', alignSelf:'center'}} />
                            <Text style={styles.botTotle}>Share your code</Text>
                            {/* <Text style={{fontFamily: "ProximaNova-Bold",marginBottom:15}}>Have a friend who loves their vehicle?</Text> */}
                            <Text>Share your code so both of you can enjoy the perks of maintaining your vehicles with SERV!</Text>
                            <Text style={{fontFamily: "ProximaNova-Bold",marginTop:10}}>SERV Referral Rewards is governed by the SERV Referral Rewards <Text onPress={this.tncLink} style={{fontFamily: "ProximaNova-Bold",color:"#080880",}} > Terms & Conditions </Text></Text>

                            <View style={{flexDirection:'row', justifyContent:'space-between', marginTop:15 }}>
                                <View style={styles.countBox} >
                                    <View style={{ alignItems:'center',height:'30%',  justifyContent:'center', marginBottom:20, }}>
                                        <Text style={{fontSize:25,textAlign:'center', fontFamily: "ProximaNova-Bold",}}>{this.state.total_friends}</Text>
                                    </View>
                                    <Text style={{fontSize:14, textAlign:'center'}}>Used my code</Text>
                                    
                                </View>
                                <View style={styles.countBox} >
                                    <View style={{alignItems:'center',height:'30%', justifyContent:'center', marginBottom:20, }}>
                                        <Text style={{fontSize:25,fontFamily: "ProximaNova-Bold",}}>{this.state.registered_vehicle}</Text>
                                    </View>
                                        <Text style={{fontSize:14, textAlign:'center'}}>Registered vehicle</Text>
                                    
                                </View>
                            </View>

                            <Button 
                                onPress={() => {
                                    firebase.analytics().logScreenView({
                                        screen_name: 'ReferFriendHIW',
                                        screen_class: 'ReferFriendHIW',
                                      })
                                    this.props.navigation.navigate('ReferFriendHIW')}} 
                                title='How it works' 
                                bgColor='#080880' width= {WIDTH} marginTop={30} />
                            
                            
                            
                        </View>
                        <View style={{height:70}} />
                    </ScrollView>

                    <View style={{flexDirection:'row',alignItems:'center', position:'absolute', backgroundColor:'#fff', bottom:0, justifyContent:'space-between', padding:15, borderTopColor:'#ccc', borderTopWidth: 0.5}}>
                        <View style={{flex:2}} >
                            <Text style={{color:'grey', fontSize:12,fontFamily: "ProximaNova-Bold",}} >Your Referral Code</Text>
                            
                            {this.createCode()}
                        </View>
                        <View style={{flex:2.2}}>
                            <TouchableWithoutFeedback onPress={() => this.onShare('message') }>
                            <View style={styles.button} >
                                <Text style={{color:'#fff', fontSize:14, fontFamily:'ProximaNova-Bold', textAlign:'center'}} >Share Now</Text>
                            </View>
                            </TouchableWithoutFeedback>
                            
                        </View>
                    </View>

                    
                </View>
            </SafeAreaView>
        )
    }
}

ReferFriend = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(ReferFriend))
export default ReferFriend

const styles = StyleSheet.create({
    scroll :{
        backgroundColor:'#fff',
        borderRadius:15,
        margin:15,
        padding:20,
        paddingTop:0,
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 3 },
              shadowOpacity: 0.3,
            },
            android: {
              elevation: 3
            },
          }),
    },
    totle :{fontSize:18, fontFamily: "ProximaNova-Bold", color:"#080880" ,marginBottom:15, marginTop:15, textAlign:'center' },
    botTotle:{fontSize:20, fontFamily: "ProximaNova-Bold", color:"#080880", marginBottom:10 },
    button: {
        backgroundColor:'#080880',
        borderRadius:6,
        padding:15

    },
    countBox :{
        borderRadius:10,
        alignItems:'center',
        justifyContent:'center',
        height: Dimensions.get('window').width / 3,
        width:'48%',
        backgroundColor:'#fff',
        padding:20,
        ...Platform.select({
            ios: {
                shadowColor: "#000",
                shadowOffset: {width: 0, height: 2,},
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
            },
            android: {
              elevation: 5
            },
        }),
    },
})