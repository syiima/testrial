import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,KeyboardAvoidingView,FlatList, ActivityIndicator, Alert,Dimensions, TouchableWithoutFeedback, TouchableOpacity, ScrollView, Image, TextInput, Linking} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/AntDesign"
import MaterialIcon from "react-native-vector-icons/MaterialIcons";

import ImagePicker from 'react-native-image-picker';
import { getStates, getCities } from "malaysia-postcodes";

import FormTextInput from "../components/FormTextInput"
import PhoneTextInput from "../components/PhoneTextInput.js"
import PickerTextInput from "../components/PickerTextInput"
import Header from "../components/Header"
import RoundedBorderButton from "../components/RoundedBorderButton"
import MiddleModal from "../components/MiddleModal"
import profile from '../assets/empty_profile.png'

class ProfileEdit2 extends React.Component{
    constructor(){
        super()
        this.state = {
            phone: '',
            address:'',
            city:'',
            postal: '',
            State:'',
            country: '',
            uploading:false,
            modalStates: false,
            modalCountry: false,
        }
    }

    componentDidMount(){
        this.setInfos()
    }

    setInfos(){
        let DATA = this.props.navigation.state.params.data

        let NUM = DATA.Phone
        let PHONE
        if(NUM[0] === '+'){
            PHONE = NUM.slice(3)
          } else {
            PHONE = NUM.slice(1)
        }

        this.setState({
            phone: PHONE,
            address: DATA.Address !== undefined ? DATA.Address : '',
            city: DATA.City !== undefined ? DATA.City : '',
            postal: DATA.PostalCode !== undefined ? DATA.PostalCode : '',
            State: DATA.State !== undefined && DATA.State !== '' ? DATA.State : 'Select state',
            country: DATA.Country !== "" && DATA.Country !== undefined ? DATA.Country : 'Select country',
            

        })
    }

    savedata = () => {
        let FUID = this.props.mobx_auth.FUID 

        let PHONE = this.state.phone 
        let ADDRESS  = this.state.address
        let CITY = this.state.city 
        let POSTAL  = this.state.postal
        let STATE = this.state.State 
        let COUNTRY  = this.state.country


        if(PHONE === ''){
            Alert.alert('Sorry', 'Contact number cannot be empty')
            return 
        }

        if(PHONE !== ''){ this.setState({uploading: true})}
        if(COUNTRY === 'Select country'){ COUNTRY = ''}
        if(STATE === 'Select state'){ STATE = ''}

        firebase.database().ref(`users/${FUID}`).update({
            Phone: '+60' + PHONE,
            Address:ADDRESS,
            City: CITY,
            PostalCode:POSTAL,
            State: STATE,
            Country: COUNTRY
        })

        this.props.navigation.state.params.refresh_profile()
        this.props.navigation.goBack(null)

    }

    render(){
        let STATES = getStates();
        let COUNTRY = ['Malaysia', 'Brunei', 'Singapore', 'Indonesia']
        let content_states = []
        let content_country = []

        for(var i = 0; i < STATES.length; i++){
            let value = STATES[i];
            content_states.push(
              <View >
                <TouchableOpacity
                  style={{padding:10, borderBottomColor:'#ccc', borderBottomWidth:0.5}}
                  onPress={()=> this.setState({State:value,modalStates:false})} >
                  <Text style={{fontSize:16}}>{STATES[i]}</Text>
                </TouchableOpacity>
              </View>
            )
        }
        for(var i = 0; i < COUNTRY.length; i++){
            let value = COUNTRY[i];
            content_country.push(
              <View >
                <TouchableOpacity
                  style={{padding:10, borderBottomColor:'#ccc', borderBottomWidth:0.5}}
                  onPress={()=> this.setState({country:value,modalCountry:false})} >
                  <Text style={{fontSize:16}}>{COUNTRY[i]}</Text>
                </TouchableOpacity>
              </View>
            )
        }

        let saveButton 

        if(this.state.uploading === false){
            saveButton = 
            <RoundedBorderButton onPress={this.savedata} 
                title='Save'
                borderColor='#080880' marginTop={20} />
        } else {
            saveButton = 
            <RoundedBorderButton borderColor='#080880' marginTop={20} title='Updating ... ' />
        }

        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#080880'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Edit Profile' onPress={() => this.props.navigation.goBack()} />
                <KeyboardAvoidingView
                    behavior={(Platform.OS === 'ios') ? "padding" : null} enabled
                    style={{ flex: 1, }}
                >
                <ScrollView style={{flex:1,backgroundColor:'white',padding:20, }} showsVerticalScrollIndicator={false} >


                        <PhoneTextInput 
                            title= 'PHONE NUMBER'
                            value={this.state.phone}
                            placeholder= 'eg: 12456904'
                            onChangeText={(x) => this.setState({phone:x})}
                        />

                        <FormTextInput 
                            title= 'ADDRESS'
                            value={this.state.address}
                            multiline={true}
                            placeholder= 'eg: 40, Jalan Lima'
                            onChangeText={(x) => this.setState({address:x})}
                        />
                        <FormTextInput 
                            title= 'CITY'
                            value={this.state.city}
                            placeholder= 'eg: Kerian, Mentakab'
                            onChangeText={(x) => this.setState({city:x})}
                        />

                        <FormTextInput 
                            title= 'POSTAL CODE'
                            value={this.state.postal}
                            placeholder= 'eg: 53100, 32020'
                            keyboardType="phone-pad"
                            onChangeText={(x) => this.setState({postal:x})}
                        />

                        <PickerTextInput 
                            title= 'STATE'
                            value={this.state.State} 
                            onPress={() => this.setState({modalStates:true})} 
                        /> 

                        <PickerTextInput 
                            title= 'COUNTRY'
                            value={this.state.country} 
                            onPress={() => this.setState({modalCountry:true})} 
                        /> 

                        
                        {saveButton}


                        <View style={{height:25}} />

                    </ScrollView>

                    <MiddleModal modalName ={this.state.modalStates}  height="94%"
                    children={
                        <View style={{flex:1}} >
                            <Text style={{fontSize:20,marginBottom:15, fontFamily: "ProximaNova-Bold"}}>Select State</Text>
                            <ScrollView showsVerticalScrollIndicator={false} style={{ width:'100%',}}>
                                {content_states}
                            </ScrollView>

                            <TouchableOpacity style={styles.button} onPress={()=> this.setState({modalStates:false})}>
                                <Text style={styles.text} >Close</Text>
                            </TouchableOpacity>

                        </View>
                    
                  
                } />
                <MiddleModal modalName ={this.state.modalCountry}  children={
                    <View>
                       <Text style={{fontSize:20,marginBottom:15, fontFamily: "ProximaNova-Bold"}}>Select Country</Text>
                           
                       {content_country}
                        
                       <TouchableOpacity style={styles.button} onPress={()=> this.setState({modalCountry:false})}>
                            <Text style={styles.text} >Close</Text>
                        </TouchableOpacity>
                    </View>
                } />



                    </KeyboardAvoidingView>
                </View>
            </SafeAreaView>
        )
    }
}

ProfileEdit2 = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(ProfileEdit2))
export default ProfileEdit2

const styles = StyleSheet.create({
    boxText:{marginBottom:15, flexDirection:'row', alignItems:'center'},
    leftSide:{width:Dimensions.get('window').width /4.3, marginRight:10},
    image:{height:Dimensions.get('window').width/2,width:Dimensions.get('window').width - 40, resizeMode:'cover', alignSelf:'center', marginBottom:25,borderRadius: 10,},
    button :{
        marginTop:15,
        borderRadius:10,
        padding:15,
        backgroundColor:'#080880',
        width: Dimensions.get('window').width - 70,
    },
    text :{
        fontFamily:'ProximaNova-Bold', 
        fontSize:18, 
        color:'white', 
        textAlign:'center'
    },
    innerModal: {     
        borderTopLeftRadius: 10,
        borderTopRightRadius:10,
        width:Dimensions.get('window').width,
        position:'absolute',
        bottom:0,
        backgroundColor: '#fff',
            ...Platform.select({
                ios: {
                    shadowColor: '#000',
                    shadowRadius: 2,
                    shadowOffset: { width: 0, height: 3 },
                    shadowOpacity: 0.6,
                },
                android: {
                    elevation: 2
                },
            }),
    },

})