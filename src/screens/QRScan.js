import React, {Component} from 'react';
import {Platform, Alert,Vibration, Modal,StyleSheet, Text, View, Button, ImageBackground,SafeAreaView, TextInput, ScrollView,Image, Dimensions, TouchableOpacity, ActivityIndicator, TouchableWithoutFeedback, KeyboardAvoidingView} from 'react-native';
import firebase from '@react-native-firebase/app';
import { inject, observer, Provider } from 'mobx-react';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import AntIcon from "react-native-vector-icons/AntDesign";
import Feather from 'react-native-vector-icons/Feather';

import { RNCamera,} from 'react-native-camera';
import BarcodeMask from 'react-native-barcode-mask';
import Moment from 'moment';
import { NavigationActions, StackActions } from 'react-navigation';
import Header from '../components/Header.js'


const CONST_WIDTH = Dimensions.get('window').width;

class QRScan extends React.Component{
    constructor(props) {
        super(props);
        this.camera = null;
        this.barcodeCodes = [];

        this.state = {
            camera: {
                type: RNCamera.Constants.Type.back,
                  flashMode: RNCamera.Constants.FlashMode.auto,
                  barcodeFinderVisible: true,
                // value: 'haha',
              },
            path: null,
            data: '',
        
            scan_state : false,
            value:'hello',
    
            group_ID : '',
            ws_ID: '',
        //   car_picture:'',
    
    
        };
      }

    async onBarCodeRead(scanResult){
        this.setState({scan_state : true})
        let that = this;
        if(scanResult.data != null){
          if(!this.barcodeCodes.includes(scanResult.data)){            
            Vibration.vibrate()
            this.barcodeCodes = scanResult.data
            
            await this.setState({value: scanResult.data}) //value = walking|groupID|wsID

            let SCTYPE = this.state.value.split('|')[0]
            let KEY 
            if(SCTYPE === 'walkin'){
                this.setState({
                    group_ID : this.state.value.split('|')[1],
                    ws_ID :this.state.value.split('|')[2]
                })
                let GROUP_ID = this.state.value.split('|')[1]
                let WORKSHOP_ID = this.state.value.split('|')[2]
                let ADDRESS = ''
                let NAMEPLACE =  ''
                let UID = this.props.mobx_auth.FUID;
                let NAME = this.props.mobx_auth.NAME;
                let EMAIL  =this.props.mobx_auth.EMAIL;
                let PHONE  = this.props.mobx_auth.PHONE;
                let CARTYPE = this.props.mobx_carkey.VTYPE;
                let SAMETYPE

                firebase.database().ref('retail/' + GROUP_ID + '/' + WORKSHOP_ID).once('value').then((snapshot) => {
                    let X = snapshot.val()

                    if(X.category !== undefined){
                      let CAT = X.category
                        if(CARTYPE === 'Motorcycle'){
                          if(CAT.includes(CARTYPE)){ SAMETYPE = true } 
                          else { SAMETYPE = false }
                        } else {
                          SAMETYPE = true
                        }
                    } else {
                        SAMETYPE = true
                    }


                    ADDRESS = X.address
                    NAMEPLACE = X.name
                }).then(() => {

                  if(SAMETYPE === true){
                    //1.getKey booking req
                    KEY = firebase.database().ref('request_walkin/' + GROUP_ID + '/' + WORKSHOP_ID).push().key;

                    //2.get req_id
                    let ID = NAME.charAt(0) + NAME.charAt(1) + NAME.charAt(2) + KEY.charAt(1) + KEY.charAt(2) + KEY.charAt(3) + KEY.charAt(4) + KEY.charAt(5) + KEY.charAt(6) + KEY.charAt(7) + KEY.charAt(8);
                    //new
                    let NOTES = '';
                
                    //Data collected from the request form
                    let BOOK_TIME = Moment().format('h:mm A')
                    let BOOK_DAY = Moment().format('DD')
                    let BOOK_MONTH = Moment().format('MMMM')
                    let BOOK_YEAR = Moment().format('YYYY');
                    //Car DATA
                    let CAR_MAKE = this.props.mobx_carkey.MAKE;
                    let CAR_MODEL = this.props.mobx_carkey.MODEL;
                    let CAR_PLATE = this.props.mobx_carkey.PLATE;
                    let CAR_YEAR = this.props.mobx_carkey.YEAR;
                    let CAR_CC = this.props.mobx_carkey.CARCC;
                    let CAR_TRANSMISSION = this.props.mobx_carkey.TRANSMISSION;

                    //check user's app version and OS
                    let APP_VERSION = this.props.mobx_config.Version;
                    let APP_PLATFORM =  Platform.OS;



                  const REQ_WALKIN = {
                    _requestID: ID,
                    customer_address: ADDRESS,
                    customer_nameplace: NAMEPLACE,
                    //
                    customer_carmake: CAR_MAKE,
                    customer_carmodel: CAR_MODEL,
                    customer_carplate: CAR_PLATE,
                    customer_caryear: CAR_YEAR,
                    customer_cartransmission : CAR_TRANSMISSION,
                    customer_carcc: CAR_CC,
                    customer_cartype: CARTYPE,

                    customer_email: EMAIL,
                    customer_name: NAME,
                    customer_phone: PHONE,
            
                    customer_notes: NOTES,
                    customer_FUID: UID,

                    customer_day: BOOK_DAY ,
                    customer_month: BOOK_MONTH ,
                    customer_year: BOOK_YEAR,
                    customer_time: BOOK_TIME,
            
                    quotation : {
                      status: false,
                    },
                    timestamp: firebase.database.ServerValue.TIMESTAMP,
            
                    //check user's app version and OS
                    app_version: APP_VERSION,
                    app_platform: APP_PLATFORM,
                    status: 'Requesting',
                  }

                  //3.update req_walkin
                  firebase.database().ref('request_walkin/' + GROUP_ID + '/' + WORKSHOP_ID + '/' + KEY).update(REQ_WALKIN)

                  //4.update user booking stats
                  firebase.database().ref(`plate_number/${CAR_PLATE}`).update({
                      book_walkin: {
                        _book_id: KEY,
                        _book_status: 'Requesting',
                        retail_id:WORKSHOP_ID,
                        retail_main:  GROUP_ID,
                        day: BOOK_DAY,
                        month: BOOK_MONTH,
                        year: BOOK_YEAR,
                        time: BOOK_TIME,
                        booking_by: NAME
                      },
                    })

                    //5.update dekat retail user last visit
                    this.lastSeen()
                    this.navigateBooking('Requesting',KEY, WORKSHOP_ID, GROUP_ID )
                  } else {
                    Alert.alert('No service provided for selected vehicle', 'Please ensure that you have selected the correct vehicle.')
                    this.props.navigation.popToTop()
                  }
  

                    

                })

                
                
                } else {
                  // console.warn('nani??!!', TYPE);
                  
                }

          }
        }

        return
      }

      navigateBooking = (a,b,c,d) => {

        let status = this.props.mobx_retail.setSTATUS(a);
        let bookID =  this.props.mobx_retail.setR_WALKIN_BOOKID(b);
        let wsID =   this.props.mobx_retail.setR_ID(c);
        let groupID =   this.props.mobx_retail.setR_GROUPID(d);


        const navigateAction = StackActions.replace({
          routeName: 'WorkshopPerformService',
          action: NavigationActions.navigate({ routeName: 'WorkshopPerformService' }),
        });
        this.props.navigation.dispatch(navigateAction);
      }

      lastSeen = () => {
        let that = this;
        let FUID = this.props.mobx_auth.FUID;
        let GROUP_ID = this.state.group_ID
        let WORKSHOP_ID = this.state.ws_ID
      
        firebase.database().ref('users/' + FUID).update({app_platform: Platform.OS})
        const userStatusRef = firebase.database().ref('retail/' + GROUP_ID + '/' + WORKSHOP_ID + '/customers/' + FUID)   

        const userHistoryRef = firebase.database().ref('retail/' + GROUP_ID + '/' + WORKSHOP_ID + '/customers/' + FUID + '/user_activity/')
      
        const isOfflineForDatabase = {
            last_visited: firebase.database.ServerValue.TIMESTAMP,
        };
      
        const isOnlineForDatabase = {
            last_visited: firebase.database.ServerValue.TIMESTAMP,
        };
        firebase.database().ref('.info/connected').on('value', (snapshot) => {
          let DATA;
            // If we're not currently connected, don't do anything.
            if (snapshot.val() == false) {
                return;
            };
      
            userStatusRef.once('value').then((snapshot) => {
      
              if(snapshot.exists()) {
                DATA = {
                  last_visited : snapshot.val().last_visited,
                }
              } else {console.log('nothing');}
      
            }).then(() => {
      
              userStatusRef.onDisconnect().update(isOfflineForDatabase)
      
            }).then(() => {
                userHistoryRef.push().set(DATA)
            }).then(() => {
                userStatusRef.update(isOnlineForDatabase);
            })
        });
      }

      
    render(){
        let scan_qrcode;

        if(this.state.scan_state === true){
        scan_qrcode =
            <View style={{padding:15, flex:1, backgroundColor:'white', alignItems:'center', justifyContent:'center', borderRadius:8}}>
            <ActivityIndicator style={{ height: 30, width: 30, marginLeft:10, alignSelf:'center'}} size="small" color="#080880" />
            <Text> Loading... </Text>
            </View>
        }

        if(this.state.scan_state === false){
        scan_qrcode =
        <RNCamera
            ref={ref => {
                this.camera = ref;
            }}
            captureAudio={false}
            barcodeFinderVisible={this.state.camera.barcodeFinderVisible}
            barcodeFinderWidth={280}
            barcodeFinderHeight={220}
            barcodeFinderBorderColor="white"
            barcodeFinderBorderWidth={2}
            defaultTouchToFocus
            flashMode={this.state.camera.flashMode}
            mirrorImage={false}
            onBarCodeRead={this.onBarCodeRead.bind(this)}
            onFocusChanged={() => {}}
            onZoomChanged={() => {}}
            style={styles.preview}
            type={this.state.camera.type}
            >
                <BarcodeMask edgeColor={'#080880'} showAnimatedLine={true}/>
            </RNCamera>
        
        }

        return(
          <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#1d1d1d', }}>
                <Header backButton = {true} onPress={() => this.props.navigation.goBack()}  headerTitle = 'Scan QR' />
          
                {scan_qrcode}

              </View>
        </SafeAreaView>
        )
    }   
}

QRScan = inject('mobx_auth','mobx_retail', 'mobx_config', 'mobx_carkey', 'mobx_retail')(observer(QRScan))
export default QRScan

const styles = StyleSheet.create({
top: {
    backgroundColor:'#080880',
    borderRadius: 50, 
    margin: 15,
    marginBottom:0,
    padding:10,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between',
    zIndex:2,
    ...Platform.select({
      ios: {
        shadowColor: '#000',
        shadowRadius: 3,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.6,
      },
      android: {
        elevation: 2
      },
    }),
},
container: {
  flex: 1
},
preview: {
  flex: 1,
  justifyContent: 'flex-end',
  alignItems: 'center'
},
overlay: {
  position: 'absolute',
  padding: 16,
  right: 0,
  left: 0,
  alignItems: 'center'
},
topOverlay: {
  top: 20,
  left:15,
  flex: 1,
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center'
},
bottomOverlay: {
  bottom: 0,
  margin: 15,
  backgroundColor: '#fcd736',
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center'
},
enterBarcodeManualButton: {
  padding: 15,
  backgroundColor: 'white',
  borderRadius: 40
},
scanScreenMessage: {
  fontSize: 18,
  color: 'black',
  textAlign: 'center',
  alignItems: 'center',
  justifyContent: 'center'
}

})