import React from "react"
import {Platform, Alert,Vibration, Modal,StyleSheet, Text, View, Button, ImageBackground,SafeAreaView, TextInput, ScrollView,Image, Dimensions, TouchableOpacity, ActivityIndicator, TouchableWithoutFeedback, KeyboardAvoidingView} from 'react-native';
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/AntDesign"

import Header from '../components/Header.js'
import { RNCamera,} from 'react-native-camera';
import BarcodeMask from 'react-native-barcode-mask';
import Moment from 'moment';
import { NavigationActions, StackActions } from 'react-navigation';


class WorkshopScan extends React.Component{
    constructor(props) {
        super(props);
        this.camera = null;
        this.barcodeCodes = [];

        this.state = {
            camera: {
                type: RNCamera.Constants.Type.back,
                  flashMode: RNCamera.Constants.FlashMode.auto,
                  barcodeFinderVisible: true,
                // value: 'haha',
              },
            path: null,
            data: '',
        
            scan_state : false,
            value:'',
            screen : 2
        };
    }

    async onBarCodeRead(scanResult){
        this.setState({scan_state : true})
        let that = this;

        if(scanResult.data != null){
          if(!this.barcodeCodes.includes(scanResult.data)){
            Vibration.vibrate()
            this.barcodeCodes = scanResult.data

            await this.setState({value: scanResult.data}) //value = walking|groupID|wsID

            let TYPE = this.state.value.split('|')[0]
            let WALKIN_ID = this.state.value.split('|')[2]
            let FB = this.props.navigation.state.params;
            let KEY 
            let STATUS = this.props.mobx_retail.STATUS
            let CARTYPE = this.props.mobx_carkey.VTYPE
            
            if(STATUS === 'Appointment' && WALKIN_ID ===  FB.workshopID){
              this.lastSeen()
              if(FB.wsdata !== undefined){
                let CAT = FB.wsdata.category
                if(CAT !== undefined){
                  if(CARTYPE === 'Motorcycle'){
                    if(CAT.includes(CARTYPE)){
                      this.convertAppointment()
                    } else {
                      Alert.alert('Different workshop QR', 'Please make sure you\'ve selected the right appointment for the right workshop' )
                      this.props.navigation.goBack(null)
                    }
                  } else {
                    this.convertAppointment()
                  }
                } else {
                  this.convertAppointment()
                }
               
              } else {
                this.convertAppointment()
              }

            }
            if(STATUS === 'Appointment' && WALKIN_ID !==  FB.workshopID){
              this.setState({scan_state : false, screen: 2, value: '',})
              Alert.alert('Different workshop QR', 'Please make sure you\'ve selected the right appointment for the right workshop' )
              this.props.navigation.goBack(null)
              
              return
            }

            if(STATUS !== 'Appointment' && TYPE === 'walkin' && WALKIN_ID ===  FB.workshopID){
            //USER INFO
            

                let UID = this.props.mobx_auth.FUID;
                let NAME = this.props.mobx_auth.NAME;
                let EMAIL  =this.props.mobx_auth.EMAIL;
                let PHONE  = this.props.mobx_auth.PHONE;
            
                //new
                let NOTES = '';
                let ADDRESS = FB.address
                let NAMEPLACE =  FB.nameplace
            
                //Data collected from the request form
                let BOOK_TIME = Moment().format('h:mm A')
                let BOOK_DAY = Moment().format('DD')
                let BOOK_MONTH = Moment().format('MMMM')
                let BOOK_YEAR = Moment().format('YYYY');
                //Car DATA
                let CAR_KEY = this.props.mobx_carkey.CarKey;
                let CAR_MAKE = this.props.mobx_carkey.MAKE;
                let CAR_MODEL = this.props.mobx_carkey.MODEL;
                let CAR_PLATE = this.props.mobx_carkey.PLATE;
                let CAR_YEAR = this.props.mobx_carkey.YEAR;
                let CAR_CC = this.props.mobx_carkey.CARCC;
                let CAR_TRANSMISSION = this.props.mobx_carkey.TRANSMISSION;

                //check user's app version and OS
                let APP_VERSION = this.props.mobx_config.Version;
                let APP_PLATFORM =  Platform.OS;


                let GROUP_ID = FB.groupID
                let WORKSHOP_ID = FB.workshopID
                
                //1.getKey booking req
                KEY = firebase.database().ref(`request_walkin/${GROUP_ID}/${WORKSHOP_ID}/`).push().key;

                //2.get req_id
                let ID = NAME.charAt(0) + NAME.charAt(1) + NAME.charAt(2) + KEY.charAt(1) + KEY.charAt(2) + KEY.charAt(3) + KEY.charAt(4) + KEY.charAt(5) + KEY.charAt(6) + KEY.charAt(7) + KEY.charAt(8);


                const REQ_WALKIN = {
                _requestID: ID,
                customer_address: ADDRESS,
                customer_nameplace: NAMEPLACE,
                //
                customer_carmake: CAR_MAKE,
                customer_carmodel: CAR_MODEL,
                customer_carplate: CAR_PLATE,
                customer_caryear: CAR_YEAR,
                customer_cartransmission : CAR_TRANSMISSION,
                customer_carcc: CAR_CC,
        
                customer_email: EMAIL,
                customer_name: NAME,
                customer_phone: PHONE,
        
                customer_notes: NOTES,
                customer_FUID: UID,

                customer_day: BOOK_DAY ,
                customer_month: BOOK_MONTH ,
                customer_year: BOOK_YEAR,
                customer_time: BOOK_TIME,
        
                quotation : {
                    status: false,
                },
                timestamp: firebase.database.ServerValue.TIMESTAMP,
        
                //check user's app version and OS
                app_version: APP_VERSION,
                app_platform: APP_PLATFORM,
                status: 'Requesting',
            }

            //3.update req_walkin
            firebase.database().ref(`request_walkin/${GROUP_ID}/${WORKSHOP_ID}/${KEY}/`).update(REQ_WALKIN)

            //4.update user booking stats
            firebase.database().ref(`plate_number/${CAR_PLATE}`).update({
                book_walkin: {
                  _book_id: KEY,
                  _book_status: 'Requesting',
                  retail_id:WORKSHOP_ID,
                  retail_main:  GROUP_ID,
                  booking_by: NAME,
                  day: BOOK_DAY,
                  month: BOOK_MONTH,
                  year: BOOK_YEAR,
                  time: BOOK_TIME
                },
              })

            //5.update dekat retail user last visit
            this.lastSeen()
              // console.warn('yashhaa', TYPE);

              let status = this.props.mobx_retail.setSTATUS('Requesting');
              let bookID =  this.props.mobx_retail.setR_WALKIN_BOOKID(KEY);
              let wsID =   this.props.mobx_retail.setR_ID(WORKSHOP_ID);
              let groupID =   this.props.mobx_retail.setR_GROUPID(GROUP_ID);

              const navigateAction = StackActions.replace({
                routeName: 'WorkshopPerformService',
                action: NavigationActions.navigate({ routeName: 'WorkshopPerformService' }),
              });
              this.props.navigation.dispatch(navigateAction);

              // this.props.navigation.navigate('Perform_Service')
              
            }

            if(WALKIN_ID !==  FB.workshopID) {
              this.setState({scan_state : false, screen: 2, value: '',})
              Alert.alert('Sorry', 'Please make sure you\'ve selected the right workshop' )
              this.props.navigation.goBack(null)
              
              return
              // console.warn('nani??!!', TYPE);
              
            }

          }
        }
        this.setState({scan_state : false, screen: 2, value: '', })
        return
      }


    lastSeen = () => {
        let that = this;
        let FUID = this.props.mobx_auth.FUID;
        let GROUP_ID = this.props.navigation.state.params.groupID
        let WORKSHOP_ID = this.props.navigation.state.params.workshopID
      
        firebase.database().ref('users/' + FUID).update({app_platform: Platform.OS})
        const userStatusRef = firebase.database().ref(`retail/${GROUP_ID}/${WORKSHOP_ID}/customers/${FUID}/`)   

        const userHistoryRef = firebase.database().ref(`retail/${GROUP_ID}/${WORKSHOP_ID}/customers/${FUID}/user_activity/`)
      
        const isOfflineForDatabase = {
            last_visited: firebase.database.ServerValue.TIMESTAMP,
        };
      
        const isOnlineForDatabase = {
            last_visited: firebase.database.ServerValue.TIMESTAMP,
        };
        firebase.database().ref('.info/connected').on('value', (snapshot) => {
          let DATA;
            // If we're not currently connected, don't do anything.
            if (snapshot.val() == false) {
                return;
            };
      
            userStatusRef.once('value').then((snapshot) => {
      
              if(snapshot.exists()) {
                DATA = {
                  last_visited : snapshot.val().last_visited,
                }
              } else {console.log('nothing');}
      
            }).then(() => {
      
              userStatusRef.onDisconnect().update(isOfflineForDatabase)
      
            }).then(() => {
                userHistoryRef.push(DATA)
            }).then(() => {
                userStatusRef.update(isOnlineForDatabase);
            })
        });
    }

    convertAppointment = () => {
      let FB = this.props.navigation.state.params;
        let STATUS = this.props.mobx_retail.STATUS


        if(STATUS === 'Appointment'){
          let GROUP_ID = this.props.mobx_retail.R_GROUPID
          let WORKSHOP_ID = this.props.mobx_retail.R_ID

          let bookID =  this.props.mobx_retail.R_WALKIN_BOOKID
          let PLATE = this.props.mobx_carkey.PLATE
          let UID = this.props.mobx_auth.FUID;
          let BOOK_DAY, BOOK_MONTH, BOOK_TIME, BOOK_YEAR

          firebase.database().ref('appointment_walkin/' + GROUP_ID + '/' + WORKSHOP_ID + '/' + bookID).once('value')
            .then((snapshot) => {
            if(snapshot.exists()){
              let X = snapshot.val()
              let DATE = X.customer_day + ' ' + X.customer_month + ' ' + X.customer_year
              BOOK_DAY = X.customer_day
              BOOK_MONTH = X.customer_month
              BOOK_YEAR= X.customer_year
              BOOK_TIME =  X.customer_time

              firebase.database().ref('request_walkin/' + GROUP_ID + '/' + WORKSHOP_ID + '/' + bookID).update(X)
              
              firebase.database().ref('request_walkin/' + GROUP_ID + '/' + WORKSHOP_ID + '/' + bookID + '/appointment/')
              .update({
                timestamp: X.timestamp,
                date:  DATE,
                time: X.customer_time
              })
            }
          })
          .then(() => {
              //4.update user booking stats
              firebase.database().ref(`plate_number/${PLATE}`).update({
              book_walkin: {
                _book_id: bookID,
                _book_status: 'Requesting',
                retail_id:WORKSHOP_ID,
                retail_main:  GROUP_ID,
                day: BOOK_DAY,
                month: BOOK_MONTH,
                year: BOOK_YEAR,
                time: BOOK_TIME,
              },
            })
        

          })
          .then(() => {
            let BOOK_TIME = Moment().format('h:mm A')

            firebase.database().ref('request_walkin/' + GROUP_ID + '/' + WORKSHOP_ID + '/' + bookID).update({
              status:'Requesting',
              timestamp: firebase.database.ServerValue.TIMESTAMP,
              customer_time: BOOK_TIME
            })
          })
          .then(() => {
            firebase.database().ref('appointment_walkin/' + GROUP_ID + '/' + WORKSHOP_ID + '/' + bookID).remove()
          })

          let status = this.props.mobx_retail.setSTATUS('Requesting');

          const navigateAction = StackActions.replace({
            routeName: 'WorkshopPerformService',
            action: NavigationActions.navigate({ routeName: 'WorkshopPerformService' }),
          });
          this.props.navigation.dispatch(navigateAction);

        } 
    }
    
    render(){
        let scan_qrcode;

        if(this.state.scan_state === true){
        scan_qrcode =
            <View style={{padding:15, flex:1, backgroundColor:'white', alignItems:'center', justifyContent:'center', borderRadius:8}}>
            <ActivityIndicator style={{ height: 30, width: 30, marginLeft:10, alignSelf:'center'}} size="small" color="#080880" />
            <Text> Loading... </Text>
            </View>
        }

        if(this.state.scan_state === false && this.state.screen === 2){
            scan_qrcode =
            <RNCamera
                ref={ref => {
                  this.camera = ref;
                }}
                captureAudio={false}
                barcodeFinderVisible={this.state.camera.barcodeFinderVisible}
                barcodeFinderWidth={280}
                barcodeFinderHeight={220}
                barcodeFinderBorderColor="white"
                barcodeFinderBorderWidth={2}
                defaultTouchToFocus
                flashMode={this.state.camera.flashMode}
                mirrorImage={false}
                onBarCodeRead={this.onBarCodeRead.bind(this)}
                
                onFocusChanged={() => {}}
                onZoomChanged={() => {}}
                style={styles.preview}
                type={this.state.camera.type}
                >
                  <BarcodeMask edgeColor={'#080880'} showAnimatedLine={true}/>
                </RNCamera>
            
        }
      

        return(
          <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
          <View style={{flex:1,backgroundColor:'#1d1d1d', }}>
                <Header backButton = {true} onPress={() => this.props.navigation.goBack()}  headerTitle = 'Scan QR' />
                    
                    {scan_qrcode}
                    
                </View>
            </SafeAreaView>
        )
    }
}

WorkshopScan = inject('mobx_auth','mobx_retail', 'mobx_carkey', 'mobx_config')(observer(WorkshopScan))
export default WorkshopScan

const styles = StyleSheet.create({
  container: {
      flex: 1
    },
    preview: {
      flex: 1,
      justifyContent: 'flex-end',
      alignItems: 'center'
    },
    overlay: {
      position: 'absolute',
      padding: 16,
      right: 0,
      left: 0,
      alignItems: 'center'
    },
    topOverlay: {
      top: 20,
      left:15,
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center'
    },
    bottomOverlay: {
      bottom: 0,
      margin: 15,
      backgroundColor: '#fcd736',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center'
    },
    enterBarcodeManualButton: {
      padding: 15,
      backgroundColor: 'white',
      borderRadius: 40
    },
    scanScreenMessage: {
      fontSize: 18,
      color: 'black',
      textAlign: 'center',
      alignItems: 'center',
      justifyContent: 'center'
    },
    box: {     
      alignSelf:'center',
      borderRadius:20,
      width:Dimensions.get('window').width - 40,
      position:'absolute',
      top: Dimensions.get('window').width / 5,
      
      backgroundColor: '#fff',
          ...Platform.select({
              ios: {
                  shadowColor: '#000',
                  shadowRadius: 2,
                  shadowOffset: { width: 0, height: 3 },
                  shadowOpacity: 0.6,
              },
              android: {
                  elevation: 2
              },
          }),
  },
    
})