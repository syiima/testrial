import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,Image,ActivityIndicator, PermissionsAndroid, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/AntDesign"
import MaterialIcon from "react-native-vector-icons/MaterialIcons";
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import Moment from "moment";

import Header from '../components/Header.js'
import BottomModal from "../components/BottomModal.js"
import camera from '../assets/icons/camera.png'
import library from '../assets/icons/library.png'

class VehicleRegistration extends React.Component{
    constructor(){
        super()
        this.state = {
            view: 1,
            modalPicture: false,
            file :'',
            data :'',
            uploading :false,
        }
    }
    
    componentDidMount(){
      this.fetchGrant()
    }

    fetchGrant(){
      let PLATE = this.props.mobx_carkey.PLATE

      firebase.database().ref(`plate_number/${PLATE}/car_registration`).once('value', (snp) => {
        if(snp.exists()){
          let X = snp.val()

          
          this.setState({ data : X, view: 2, uploading: false,})
        } else {
            this.setState({view: 1, uploading :false,})
        }
      })
    }
    
    openCamera = async () => {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: "App Camera Permission",
            message:"App needs access to your camera to snap photos",
          }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            const options = {quality:1,maxWidth: 800, maxHeight: 800 }

            launchCamera(options, (response) => {
              if (response.didCancel) {
                this.setState({modalPicture: false})
                // alert('You cancelled image picker');
              } if (response.error) {
                Alert.alert('Sorry', "Please try again");
                this.setState({modalPicture: false})
              } 
              if(!response.didCancel && !response.error) {
                const source = response.assets[0].uri;
                this.setState({
                  file: source,
                  modalPicture: false,
                  // imagey: arrImage
                });
                this.updateGrant()
              
              }
            });
        } else {
          console.log('Camera permission denied')
        }
      } catch (error) {
        console.warn(error)
      }  

    }
      
    openLibrary = () => {
        const options = {quality:1, maxWidth: 800, maxHeight: 800}
          launchImageLibrary(options, (response) => {
            this.setState({modalPicture: false})
              if(response.didCancel){
                this.setState({modalPicture: false})
              } 
              if (response.error) {
                Alert.alert('Sorry', "Please try again");
                this.setState({modalPicture: false})
              } 
              if(!response.didCancel && !response.error) {
                const source = response.assets[0].uri;
                  this.setState({
                    file:source,
                    modalPicture: false,
                  });
                  this.updateGrant()
              }
          });
    
    }

    updateGrant = () => {
        this.setState({loading: true,uploading:true, modalPicture:false})
        let that = this
        let UID = this.props.mobx_auth.FUID;


        let FILE = this.state.file

        this.setState({modalLoading: true, modalPicture:false})

        let PLATE = this.props.mobx_carkey.PLATE
  

        const refFile= firebase.storage().ref().child(`vehicle_registration/${UID}/${PLATE}`)
        const uploadVehicReg = Platform.OS === 'ios' ? FILE.replace('file://', '') : FILE;
        let uploadTask = refFile.putFile(uploadVehicReg);
        uploadTask.on('state_changed', function(snapshot) {
              
          //UPLOADING IMAGE
          let progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          let yawn  = progress.toFixed(2)
          that.setState({progressVal: yawn, percentage: progress.toString()})
          // console.warn('Upload is ' + progress + '% done');
          switch (snapshot.state) {
            case 'running':
              break;
            case 'success':
              snapshot.ref.getDownloadURL()
              .then((downloadURL) => {
                // console.warn('ehh', downloadURL);

                
                firebase.database().ref(`plate_number/${PLATE}/car_registration`)
                  .update({
                    file: downloadURL,
                    timestamp_added :firebase.database.ServerValue.TIMESTAMP,
                  });
              })
              .then(() => {
                  that.setState({uploading:false,})
                  that.refresh()
              })
              
              break;
            default:
              break;
          }
        });

    }

    refresh = () => {
      this.fetchGrant()
      this.setState({isVisible: false, uploading:false})
    }

     
    remove = () => {
      let UID = this.props.mobx_auth.FUID;
      let PLATE = this.props.mobx_carkey.PLATE


      firebase.database().ref(`plate_number/${PLATE}/car_registration`)
      .remove()
      .then(() => {
      this.setState({data: '',view:1,  uploading:false})
      })        
    }

    viewFile = () => {
      let LINK = this.state.data.file
      this.props.navigation.navigate('Webview', {link: LINK})
    }

    dateUpdated = () => {
      let display , date
      let DATA = this.state.data 

      if(DATA.timestamp_approved !== undefined){
        date = Moment(DATA.timestamp_approved).format('DD MMM YYYY')
      } else {
        date = Moment(DATA.timestamp_added).format('DD MMM YYYY')
      }

      display = <Text style={{fontFamily: "ProximaNova-Bold",color:"#080880", marginBottom:15}}>Uploaded on: {date} </Text>
      return display 
    }

    render(){
        let content 

        if(this.state.view === 1){
            content = 
              <View style={styles.box}>
                  <Text style={{fontFamily: "ProximaNova-Bold",textAlign:'center', fontSize:15}}>Vehicle Registration</Text>
                  {/* <View style={styles.img} /> */}
                  <TouchableOpacity style={styles.button} onPress={() => this.setState({modalPicture: true}) } >
                      <MaterialIcon name= 'photo-camera'  size={18} color='#fff'/>
                      <Text style={{color:'#fff', marginLeft:15, fontSize:13, fontFamily: "ProximaNova-Bold",}}>Upload Document</Text>
                  </TouchableOpacity>
              </View>

        } else {
          if(this.state.uploading === true){
            content = 
            <View style={styles.box} >
              <View style={{...styles.img, alignItems:'center', justifyContent:'center'}}>
                <ActivityIndicator color = '#080880' size = "large"/>
              </View>
              <View style={{flexDirection:'row', justifyContent:'space-between',marginTop:20,}}>
                <View style={styles.leftButton}  >
                    <Text style={{color:'#080880',fontSize:13, fontFamily: "ProximaNova-Bold",}}>Remove</Text>
                </View>
                <View style={styles.rightButton} >
                    <Text style={{color:'#fff',  fontSize:13, fontFamily: "ProximaNova-Bold",}}>View</Text>
                </View>
              </View>
            </View>
          } 
          if(this.state.uploading === false && this.state.data !== '') {
            content = 
            <View style={styles.box} >
              {this.dateUpdated()}
              <Image source={{uri :this.state.data.file}} style={styles.img} />
              <View style={{flexDirection:'row', justifyContent:'space-between',marginTop:20,}}>
                <TouchableOpacity style={styles.leftButton} onPress={this.remove} >
                    <Text style={{color:'#080880',fontSize:13, fontFamily: "ProximaNova-Bold",}}>Remove</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.rightButton} onPress={this.viewFile} >
                    <Text style={{color:'#fff',  fontSize:13, fontFamily: "ProximaNova-Bold",}}>View</Text>
                </TouchableOpacity>
              </View>
            </View>
          }
          
        }

        return(
          <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
          <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Vehicle Registration' onPress={() => this.props.navigation.goBack()} />
                
                  <View style={{margin:20}}>

                    {content}
                  </View>
                    
                    <BottomModal
                        modalName={this.state.modalPicture}
                        onPress={() => this.setState({modalPicture:false})}
                        style={styles.innerModal}
                        children = {
                        <View >
                            <View style={{padding:20, paddingBottom:0, alignItems:'center'}}>
                                <Text style={{textAlign:'center', fontFamily:'ProximaNova-Bold', fontSize:20, marginBottom:5}}>Update Photo</Text>             
                            </View>

                            <View style={{margin:30, marginTop:20,  alignItems:'center', justifyContent:'space-around', flexDirection:'row'}}>
                                <TouchableOpacity  onPress={this.openCamera} style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                                    <Image source={camera} style={{height:40, width:40}} />
                                    <Text style={{textAlign:'center'}}>Camera</Text>
                                </TouchableOpacity>
                            
                            
                                <TouchableOpacity  onPress={this.openLibrary} style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                                    <Image source={library} style={{height:40, width:40}} />
                                    <Text style={{textAlign:'center'}}>Library</Text>
                                </TouchableOpacity>

                            </View>
                        </View>
                        }
                    />

                    {this.state.uploading === true ? 
                        <View style={styles.bottomPop}>
                            <Text>Uploading. Please wait... </Text>
                        </View>   
                    : null }

                </View>
            </SafeAreaView>
        )
    }
}

VehicleRegistration = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(VehicleRegistration))
export default VehicleRegistration

const styles = StyleSheet.create({
  box :{
    backgroundColor:'#fff',
    borderRadius:10,
    marginBottom:15,
    padding:15,
    ...Platform.select({
        ios: {
          shadowColor: '#000',
          shadowRadius: 3,
          shadowOffset: { width: 0, height: 3 },
          shadowOpacity: 0.3,
        },
        android: {
          elevation: 3
        },
      }),
  },
    innerModal: {     
        borderTopLeftRadius: 10,
        borderTopRightRadius:10,
        width:Dimensions.get('window').width,
        position:'absolute',
        bottom:0,
        backgroundColor: '#fff',
            ...Platform.select({
                ios: {
                    shadowColor: '#000',
                    shadowRadius: 2,
                    shadowOffset: { width: 0, height: 3 },
                    shadowOpacity: 0.6,
                },
                android: {
                    elevation: 2
                },
            }),
    },
    button:{
      borderRadius:30,
      width:Dimensions.get('window').width - 80,
      alignItems:'center',
      justifyContent:'center',
      marginTop:15,
      padding:12,
      backgroundColor:'#080880',
      flexDirection:'row',
  }, 
    leftButton:{
      borderRadius:10,
      width:'47%',
      alignItems:'center',
      justifyContent:'center',
      padding:12,
      borderColor:'#080880',
      borderWidth:1
    },
    rightButton:{
      borderRadius:10,
      width:'47%',
      alignItems:'center',
      justifyContent:'center',
      padding:12,
      borderColor:'#080880',
      borderWidth:1,
      backgroundColor:'#080880',
    },
    img :{
      borderRadius:15,
      height: Dimensions.get('window').width /2,
      width: Dimensions.get('window').width - 80
    },  
    bottomPop : {
      position: 'absolute', bottom:20, backgroundColor:'#fff', alignSelf:'center', padding:15, borderRadius:20,
      ...Platform.select({
        ios: {
           shadowColor: '#000',
           shadowOpacity: 0.22,
           shadowRadius: 2.22,
          shadowOffset: { width: 0, height: 1 },
        },
        android: {
          elevation: 3
        },
      }),
  },
})