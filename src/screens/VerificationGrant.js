import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,Image, TouchableWithoutFeedback,PermissionsAndroid, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialIcon from "react-native-vector-icons/MaterialIcons";
import AntIcon from 'react-native-vector-icons/AntDesign'
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

import Header from '../components/Header.js'
import BottomModal from "../components/BottomModal.js"
import Button from '../components/Button.js'
import verify from '../assets/verification.png'
import camera from '../assets/icons/camera.png'
import library from '../assets/icons/library.png'

CONST_WIDTH = Dimensions.get('window').width;
CONST_HEIGHT = Dimensions.get('window').height;

class VerificationGrant extends React.Component{
    constructor(){
        super()
        this.state = {
            grantt: '',
            modalPicture: false,
            finalImage :'',
            isLoading:false
        }
    }

    openCamera = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                    title: "App Camera Permission",
                    message:"App needs access to your camera ",
                    buttonNeutral: "Ask Me Later",
                    buttonNegative: "Cancel",
                    buttonPositive: "OK",
                }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                const options = {quality:1}
                launchCamera(options, (response) => {
              
                    if(response.didCancel){
                        this.setState({modalPicture: false})
                        // console.warn('kenapa cancel');
                    }
                    if (response.error) {
                        Alert.alert('Sorry', "Please try again");
                        this.setState({modalPicture: false})
                    } if(!response.didCancel && !response.error) {
                        const source = response.assets[0].uri;
                        this.setState({
                        grantt: source,
                        modalPicture: false,
                        isLoading:true
                        // imagey: arrImage
                        });
                    this.publish()
                    }
                });
            } else {
                console.log('Camera permission denied ')
            } 
        } catch (error) {
            console.warn(error)
        }
    }

    openLibrary = () => {
        let options
        if(Platform.OS === 'ios'){
            options = {quality:1}
        } else{
            options = {quality:1, maxWidth: 800, maxHeight: 800 }
        }
        launchImageLibrary(options, (response) => {
        this.setState({modalPicture: false})
            if(response.didCancel){
            this.setState({modalPicture: false})
            } 
            if (response.error) {
            Alert.alert('Sorry', "Please try again");
            this.setState({modalPicture: false})
            } 
            if(!response.didCancel && !response.error) {
                const source = response.assets[0].uri;
                this.setState({
                    grantt: source,
                    modalPicture: false,
                    isLoading:true
                });
                this.publish()
            }
        });
      
      }
      
      
    publish = async() => {
        let FUID = this.props.mobx_auth.FUID
        let PLATE = this.props.mobx_carkey.PLATE
        
        const imageuri= this.state.grantt;
    
        const URLVehicGrant = Platform.OS === 'ios' ? imageuri.replace('file://', '') : imageuri;
        const refFile = await firebase.storage().ref().child(`plate_number_verification/${PLATE}/${FUID}`)
        await refFile.putFile(URLVehicGrant)
        const result = refFile.getDownloadURL().then(url => {
          let IMAGE_URL = url;
            this.setState({grantt: IMAGE_URL, isLoading:false})
    
        })
          .catch(error => {
            Alert.alert('Sorry', 'An error occured. Please contact SERV Team if the issue still persists');
          });
      
      }
    
    verify = () => {
        let FUID = this.props.mobx_auth.FUID
        let PLATE = this.props.mobx_carkey.PLATE
        let GRANTT = this.state.grantt 
        let FROM = this.props.navigation.state.params.from
        let NAME = this.props.mobx_auth.NAME 
        let PHONE = this.props.mobx_auth.PHONE
    
          firebase.database().ref(`plate_number_verification/${PLATE}/${FUID}`).update({
            grantt: GRANTT,
            status: 'pending',
            name: NAME,
            phone: PHONE,
            timestamp: Date.now()
          })
        
    
        firebase.database().ref(`users/${FUID}/vehicles/${PLATE}`).update({
          status: 'pending', 
          true_owner: true
        })
    
        this.telegramNotify()
      
    }
    
    telegramNotify = () => {
        let  TELEGRAM_URL, TEXT
        let  NAME = this.props.mobx_auth.NAME
        let PLATE = this.props.mobx_carkey.PLATE
        let PHONE = this.props.mobx_auth.PHONE
        let EMAIL = this.props.mobx_auth.EMAIL
        let FROM = this.props.navigation.state.params.from
    
        if (this.props.mobx_config.config === 0) {TELEGRAM_URL = 'https://api.telegram.org/bot272822956:AAEduqgWA9gt6tpxYHKp2h1Opr4ZFNftyNc/sendMessage?chat_id=-1001396604012&text=';  }
        if (this.props.mobx_config.config === 1) {TELEGRAM_URL = 'https://api.telegram.org/bot272822956:AAEduqgWA9gt6tpxYHKp2h1Opr4ZFNftyNc/sendMessage?chat_id=-1001476910252&text='}
    
        if(Platform.OS === 'ios'){
           TEXT = 'A new vehicle verification request was just made on the SERV User app.\n\nUser name : ' + NAME.toUpperCase() +  ' \nUser plate : '  + PLATE + ' \nUser email : '  + EMAIL + ' \nPhone no : ' + PHONE
        } else {
           TEXT = 'A new vehicle verification request was just made on the SERV User app.%0A%0AUser name: ' + NAME.toUpperCase() + ' %0ACustomer plate : ' + PLATE + ' %0AUser email : '  + EMAIL +  '%0APhone no : ' + PHONE
        }
        let final = TELEGRAM_URL + TEXT 
    
        fetch(final)
        .then(() => {
            this.props.navigation.state.params.refresh_profile()
            this.props.navigation.goBack()
      
        })
     
    }
    

    render(){
        let buttonbot, imageGrantt
        if(this.state.grantt === ''){
            buttonbot = 
            <Button 
                onPress={() => this.setState({modalPicture: true})}
                title='Upload Vehicle Registration Certificate' marginTop={20} width={CONST_WIDTH - 30} bgColor='#080880'  />
        } 
        if(this.state.isLoading === true){
            buttonbot = 
            <Button title='Uploading Image...  ' marginTop={20} width={CONST_WIDTH - 30} bgColor='#080880'  />
            
        } else {
            if(this.state.grantt !== 'loading' && this.state.grantt !== '' ){
                buttonbot = 
                <Button 
                   onPress={this.verify}
                   title='Get Verified' marginTop={20} width={CONST_WIDTH - 30} bgColor='#080880'  />
    
                imageGrantt = 
                <TouchableWithoutFeedback onPress={() => this.setState({grantt: ''})}>
                    <View style={{height:150, width:150,marginTop: 25, alignSelf:'center' }}>
                        <Image source={{uri:this.state.grantt}} style={{height:150, width:150, borderRadius:10,}} />
                        <AntIcon name="closecircle" size={20} color="tomato" style={{position:'absolute', right:-5, top:-5}} />
                    </View> 
                </TouchableWithoutFeedback>
            }
    
        }

        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Verification' onPress={() => this.props.navigation.goBack()} />
                    
                <View style={{flex:1, margin:15}}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <Image source={verify} style={{height:Dimensions.get('window').height / 4, width:Dimensions.get('window').width - 60,resizeMode:'contain', alignSelf:'center'}}/>

                        <View  style={{marginLeft: 10, marginRight: 10}}>
                        <Text style={{fontSize:16,color:'#080880', marginTop:10, fontFamily:'ProximaNova-Bold' }}>Get Verified</Text>
                        <Text style={{fontSize:14,color:'#080880', marginTop:10, fontFamily:'ProximaNova-Bold' }}>Would you like to upload your vehicle registration certificate to get verified as the true owner?</Text>

                        </View>

                        {imageGrantt}
                    </ScrollView>
                   
                   
                    <BottomModal
                        modalName={this.state.modalPicture}
                        onPress={() => this.setState({modalPicture:false})}
                        style={styles.innerModal}
                        children = {
                        <View >
                            <View style={{padding:20, paddingBottom:0, alignItems:'center'}}>
                                <Text style={{textAlign:'center', fontFamily:'ProximaNova-Bold', fontSize:20, marginBottom:5}}>Update Photo</Text>             
                            </View>

                            <View style={{margin:30, marginTop:20,  alignItems:'center', justifyContent:'space-around', flexDirection:'row'}}>
                                <TouchableOpacity  onPress={this.openCamera} style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                                    <Image source={camera} style={{height:40, width:40}} />
                                    <Text style={{textAlign:'center'}}>Camera</Text>
                                </TouchableOpacity>
                            
                            
                                <TouchableOpacity  onPress={this.openLibrary} style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                                    <Image source={library} style={{height:40, width:40}} />
                                    <Text style={{textAlign:'center'}}>Library</Text>
                                </TouchableOpacity>

                            </View>
                        </View>
                        }
                    />

                    {buttonbot}
                </View>
                    
                </View>
            </SafeAreaView>
        )
    }
}

VerificationGrant = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(VerificationGrant))
export default VerificationGrant

const styles = StyleSheet.create({
    innerModal: {     
        borderTopLeftRadius: 10,
        borderTopRightRadius:10,
        width:Dimensions.get('window').width,
        position:'absolute',
        bottom:0,
        backgroundColor: '#fff',
            ...Platform.select({
                ios: {
                    shadowColor: '#000',
                    shadowRadius: 2,
                    shadowOffset: { width: 0, height: 3 },
                    shadowOpacity: 0.6,
                },
                android: {
                    elevation: 2
                },
            }),
    },
})