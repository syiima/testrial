import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/AntDesign"
import FontIcon from "react-native-vector-icons/FontAwesome"

import Header from '../components/Header.js'

class MarketplaceDetails extends React.Component{
    constructor(){
        super()
        this.state = {
            data:'',
            refreshing: true,
            markey:'',
            time_open: '',
            feedbackModal: false,
            rate:'',
            comment: '',
            merchID: ''
        }
    }
    
    componentDidMount(){
        this.fetchMarketplace()
    }
    
    fetchMarketplace(){
        let that = this
        let TYPE = this.props.navigation.state.params.type        
        let DATA = []

        firebase.database().ref(`marketplace`).once('value', (snp) => {
            if(snp.exists()){
                let x = snp.val()
                KEY = Object.keys(snp.val());
    
                KEY.forEach( (key_id) => {
                    let a = snp.val()[key_id];
                    a.market_key = key_id
        
                    if(a.category === TYPE){
                        DATA.push(a)
                    } 
                
                })

    
                that.setState({data: DATA, refreshing:false})
            } else {
                that.setState({data: '', refreshing: false})
            }
        }) 
    }



    navigateBack = (TIME, KEY) => {
        this.setState({feedbackModal: true, merchID: KEY })
        let MARKETID = this.state.markey

        DATA = {
            FUID: this.props.mobx_auth.FUID,
            time_open : this.state.time_open,
            time_closed: TIME
        }
        
        firebase.database().ref(`marketplace_engagement/${MARKETID}/${KEY}`).update(DATA)
    }  

    openWebview = (x) => {
        let TIME = Date.now()
        let KEY = x.market_key
        this.setState({time_open: TIME, markey: KEY })
        let LINK = x.link
        

        this.props.navigation.navigate('WebviewMarketplace', {
            item: x,
            backItem: this.navigateBack,
        })
    }

    renderContent = () => {
        let display, data,STATUS
        let DATA = this.state.data 

        if(DATA !== ''){
            let TYPE = this.props.navigation.state.params.type    
            if(TYPE === 'Grocery Delivery'){
                STATUS = 
                <View style={styles.bigTitle}>
                <Text style={styles.title}>Get necessities from your home</Text>

                <Text style={styles.desc}>Browse a variety of products brought to you by our partners and get it delivered to you, from the convenience of your home.</Text>
                </View>

            }
            if(TYPE === 'Online Services'){
                STATUS = 
                <View style={styles.bigTitle}>
                <Text style={styles.title}>Explore our merchants and let us SERV you</Text>

                <Text style={styles.desc}>Do more with SERV and get services for all your daily needs.</Text>
                </View>

            }
            if(TYPE === 'eBazaar'){
                STATUS = 
                <View style={styles.bigTitle}>
                <Text style={styles.title}>Shop like you always do</Text>

                <Text style={styles.desc}>Get access to your favourite local merchants and shops online.</Text>
                </View>

            }

            data = DATA.map(item => (
                <TouchableWithoutFeedback onPress = {() => this.openWebview(item)} >
                    <View style={styles.box} >
                    <Image source={{uri: item.logo}} style={styles.icon}/>
                    <View style={{flexShrink:1,flex:2}} >
                        <Text style={styles.topText} >{item.name}</Text>
                        <Text style={styles.bottomText} >{item.description} </Text>
                    </View>
                    <FontIcon name="angle-right" color='#000' size={20} style={{position:'absolute', right:15, bottom:15,}} />
                </View>
                </TouchableWithoutFeedback>
            ))
        }

        display = 
        <View>
            {STATUS}
            {data}
        </View>
        return display 
    }
    render(){
        let TYPE = this.props.navigation.state.params.type   

        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = {TYPE} onPress={() => this.props.navigation.goBack()} />
                    
                    <ScrollView>
                        {this.renderContent()}

                    </ScrollView>
                    
                </View>
            </SafeAreaView>
        )
    }
}

MarketplaceDetails = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(MarketplaceDetails))
export default MarketplaceDetails

const styles = StyleSheet.create({
    box:{ 
        backgroundColor:'#fff', flexDirection:'row', alignItems:'center', 
        padding:20,  borderRadius:10, margin:20, marginTop:0,
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowRadius: 3,
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.3,
            },
            android: {
                elevation: 3
            },
        }),
    },
    icon:{
        backgroundColor:'#efefef',
        height:40, width:40,
        borderRadius:20,
        marginRight:15,
        resizeMode:'contain'
    },
    topText:{
        fontFamily: "ProximaNova-Bold",
        marginBottom:10
    },
    bottomText:{
        color:'#888888'
    },
    title: {fontFamily:'ProximaNova-Bold', color:'#080880', fontSize:18,marginBottom:10,},
    desc:{ fontSize:13, },
    bigTitle:{margin:20, marginBottom:30,}
})