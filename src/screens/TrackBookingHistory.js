import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/FontAwesome"

import Header from '../components/Header.js'
import emptyPage from '../assets/icons/emptyBooking.png'
import HistoryComponent from "../components/HistoryComponent.js"

class DisplayHistory extends React.Component{
    constructor(){
        super()
        this.state = {
            history: '',
            collapsed: false,
        }
    }
    
    componentDidMount(){
        this.sortAgain()
    }
    
    sortAgain = () => {
        let DATA = this.props.data
        let TEMP =[]
        let b ={}

        for (const month in DATA) { 
            let MONTH = DATA[month];
            MONTH.month = month

             b = {
                month: month,
                history:MONTH
            }

            TEMP.push(b)
                
        }  
        // console.log('month', TEMP)
        this.setState({history: TEMP})

    }


    showMonth = () => {
        let display
        let details 
        let DATA = this.state.history

        if(DATA !== undefined && DATA.length !== 0){
            // console.log('heiii', DATA);
            // let MONTH = DATA
           
            display = DATA.map(item => (
                <HistoryComponent data={item} navigation={this.props.navigation}/>
                
            ))

            // display = 
            // <View>
            //     <Text style={{fontFamily: "ProximaNova-Bold", fontSize:16, marginBottom:15}} >{DATA.month}  </Text>
            //         {details}
            // </View>
        }

        return display 
    }
    render(){
        // console.log('hee', this.props.data, typeof(this.props.data));
        return(
            <View>
                {this.showMonth()}
            </View>
        )
    }
}


class TrackBookingHistory extends React.Component{
    constructor(){
        super()
        this.state = {
            history: '',
            view:1

        }
    }
    
    componentDidMount(){
        this.sort()
    }
    
    sort = () => {
        let DATA = this.props.navigation.state.params.history
        if(DATA !== ''){
            let sorted = {}, b =[]
            let TEMP =[]
           
            for (const key in DATA) {
                let b =  DATA[key];
                let year = parseInt(b.customer_year)
                let month = b.customer_month
                if (typeof sorted[year] === "undefined") {
                    sorted[year] = {};
                }
            
                if (typeof sorted[year][month] === "undefined") {
                    sorted[year][month] = [];
                    sorted[year][month].month = month
                }
            
              sorted[year][month].push(b);  
            }
            for (const year in sorted) { 
                let YEAR = sorted[year];

                let b = {
                    year: year,
                    history:YEAR
                }
    
                TEMP.unshift(b)
                    
            }   
            this.setState({history: TEMP, view:2})
        } else {
            this.setState({history :'', view:1})
        }

       
      
    }

    showHistory = () => {
        let display 
        let DATA = this.state.history

       if(DATA !== ''){
        //    console.log('hoii', DATA);
            let ITEM = DATA.map(item => (
                <View style={{}} >
                    {/* <TouchableWithoutFeedback onPress={() => this.setState({currentView: item.year})} > */}
                    <View style={{marginTop:15,marginBottom:10, borderBottomColor:'#7a7a7a',borderBottomWidth:0.5, paddingBottom:5}}>
                        <Text style={{fontFamily: "ProximaNova-Bold",fontSize:16}} >{item.year}</Text>
                    </View>
                    {/* </TouchableWithoutFeedback> */}
                    
                   
                    <DisplayHistory data={item.history} navigation={this.props.navigation} />
                    
                </View> 
                

            ))
            display = 
            <View >
                {ITEM}
            </View>

       }
        return display 
    }



    render(){
        let content 

        if(this.state.view === 1){
            content = 
            <View style={{flex:1, alignItems:'center', justifyContent:'center', margin:20, marginTop:-40}}>
                <Image source={emptyPage} style={{height: 200, width:200, resizeMode:'contain'}} />
                <Text style={{fontSize:22, fontFamily: "ProximaNova-Bold", marginTop:15, marginBottom:10}}>No Records</Text>
                <Text style={{color:'#a7a7a7'}} >Looks like you haven't completed any services yet.</Text>
              </View>
        } else {
            content = 
            <ScrollView contentContainerStyle={{padding:15}}>
                {this.showHistory()}

                <View style={{height:30}} />
            </ScrollView>
        }

        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Booking History' onPress={() => this.props.navigation.goBack()} />
                    
                    {content}
                    
                </View>
            </SafeAreaView>
        )
    }
}

TrackBookingHistory = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(TrackBookingHistory))
export default TrackBookingHistory

const styles = StyleSheet.create({
    box :{
        backgroundColor:'#fff',
        borderRadius:10,
        marginBottom:15,
        padding:15,
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 3 },
              shadowOpacity: 0.3,
            },
            android: {
              elevation: 3
            },
          }),
    }
})
