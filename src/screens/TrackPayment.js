import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/FontAwesome"

import Header from '../components/Header.js'
import emptyTrans from '../assets/icons/emptyTrans.png'

const months = {
    'January' : 'Jan',
    'February' : 'Feb',
    'March' : 'Mar',
    'April' : 'Apr',
    'May' : 'May',
    'June' : 'June',
    'July' : 'July',
    'August' : 'Aug',
    'September' : 'Sept',
    'October' : 'Oct',
    'November' : 'Nov',
    'December' : 'Dec'
    }

class TrackPayment extends React.Component{
    

    goto = (item) => {
        let X = item.retail_main 

      
            
        firebase.analytics().logScreenView({
            screen_name: 'PDFConverter',
            screen_class: 'PDFConverter',
          }) 
        this.props.navigation.navigate('PDFConverter', {item:item})
    
    }

    renderTransactions = () => {
        let DATA = this.props.navigation.state.params.data

        let display

        if(DATA.length !== 0){
            display = DATA.map((item, index) => {
                return item.quotation !== undefined ?

                <TouchableWithoutFeedback onPress={() => this.goto(item)}>
                <View>
                    <View style={styles.historyBoard}>
                        <View style={{flexDirection: 'row',justifyContent:'space-between', alignItems:'center' }}>
                            <View style={{flex:3, }}>
                                <Text style={{fontFamily: "ProximaNova-Bold", fontSize:15}}>Payment</Text>

                                <Text style={{ fontSize:12, marginTop:3}}>{item.customer_carmake} {item.customer_carplate} </Text>
                                <Text style={{ fontSize:12, marginTop:3}}>Category : {item.type === 'sotg' || item.type === undefined ? 'Service On The Go' : 'Drive-In'} </Text>

                                <Text style={{ fontSize:11, color:'#080880', fontFamily:'ProximaNova-Bold', marginTop:3}}>{item.customer_day} {months[item.customer_month]}, {item.customer_time} </Text>
                            
                            </View>
                        <View style={{flex:1.5, alignItems:'center', justifyContent:'center' }}>
                            <Text style={{fontFamily: "ProximaNova-Bold", color:'#080880', fontSize:18}}>- RM {parseFloat(item.quotation.total_price).toFixed(2)}</Text>
                            <View style={{marginTop:3, borderWidth:1, borderColor:'#e85b1a', padding:8}}>
                                <Text style={{textAlign:'center',fontFamily: "ProximaNova-Bold", fontSize:11, color:'#e85b1a', borderRadius:5}}>View Receipt</Text>
                            </View>
                        </View>
                        </View>

                    </View>
                    {/* <Icon name="angle-right" color='#000' size={20} style={{position:'absolute', right:15, bottom:15,}} /> */}
                </View>
            </TouchableWithoutFeedback> 
            : null
            })

        } else {
        display = 
        <View style={{flex:1, alignItems:'center', justifyContent:'center',marginTop:30}}>
            <Image source={emptyTrans} style={{height: 200, width:200, resizeMode:'contain'}} />
            <Text style={{fontSize:22, fontFamily: "ProximaNova-Bold", marginTop:15, marginBottom:10}}>No Payment History</Text>
            <Text style={{color:'#a7a7a7'}} >Looks like you haven't made any payment yet.</Text>
        </View>

      }
        return display
    }


    render(){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Payment History' onPress={() => this.props.navigation.goBack()} />
                    
                <Text style={{ fontFamily: "ProximaNova-Bold", fontSize:17, margin:15}}>SERV Payment Activity</Text>

                <ScrollView showsVerticalScrollIndicator ={false} >
                    {this.renderTransactions()}
                </ScrollView>
                    
                </View>
            </SafeAreaView>
        )
    }
}

TrackPayment = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(TrackPayment))
export default TrackPayment

const styles = StyleSheet.create({
    historyBoard: {
        padding:10,
        marginLeft:15,
        marginRight:15,
        borderBottomWidth:1,
        borderBottomColor:'#ccc',
        // backgroundColor:'#fafdff',
        borderRadius:10,
      },
})