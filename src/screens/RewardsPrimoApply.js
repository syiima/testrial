import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,ActivityIndicator,Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from 'react-native-vector-icons/FontAwesome'
import AntDesign from 'react-native-vector-icons/AntDesign'
import ImagePicker from 'react-native-image-picker';

import Header from '../components/Header.js'
import Button from '../components/Button'
import BorderButton from '../components/BorderButton'
import MiddleModal from '../components/MiddleModal'
import BottomModal from '../components/BottomModal'
import PickerTextInput from '../components/PickerTextInput'

import car from '../assets/car_unselect.png'
import driver from '../assets/driver.png'
import medic from '../assets/medic.png'

const WIDTH = Dimensions.get('window').width - 70
const WIDTH2 =Dimensions.get('window').width  -40
class RewardsPrimoApply extends React.Component{
    constructor(){
        super()
        this.state = {
            name:'',
            phone:'',
            email:'',
            vehicleData: '',
            companyList:'',
            front_type: '',
            modalVehicle: false,
            modalRole: false,
            modalCompany: false,
            modalDone: false,
            vehicleSelected: 'Select vehicle',
            role: 'Select a role',
            companyName: 'Select company name',
            screenshot: '',
            key:'',
            loading: false,
            roleKey:'',
            frontLinersKey:'',
        }
    }

    componentDidMount(){
        this.fetchVehicles()
    }
    
    fetchVehicles(){
        let that = this;
        let FUID = this.props.mobx_auth.FUID;
        let KEY = [];
    
        firebase.database().ref(`users/${FUID}/`).once('value', function(snp){
          if(snp.exists()){
            let p = snp.val()

            if(p.vehicles !== undefined){
                KEY = Object.keys(snp.val().vehicles);
            }
    
            that.setState({name: p.name, phone: p.Phone, email:p.email, vehicleData:KEY,})
          }
        })
    
    }

    fetchApplied(key){
        let that = this;
        let PLATE = key
        let UID = this.props.mobx_auth.FUID;
        let APPLIED_KEY = []
    
        console.warn('hello', key);
    
        firebase.database().ref(`plate_number/${PLATE}/business/`).once('value').then((snapshot) => {
          if(snapshot.exists()){
            let KEY = Object.keys(snapshot.val());
    
            KEY.forEach( (biz_key) => {
              // let a = snapshot.val()[biz_key];
              if(snapshot.val()[biz_key].verified === true || snapshot.val()[biz_key].verified === false){
                APPLIED_KEY.push(biz_key)
              }
            })    
            this.fetchCompany(APPLIED_KEY)
          } else {
            this.fetchCompany(APPLIED_KEY)
          }
        })
    
    }

    fetchCompany(applied){
        let APPLIED = applied;
        let that = this;
        let USER_TYPE = []
        let FRONTLY = []
    
        firebase.database().ref(`user_type`).once('value').then((snapshot) => {
          if(snapshot.exists()){
            let keys = Object.keys(snapshot.val());
            // console.warn("dah apply dlm fetch company", APPLIED);
    
            keys.forEach( (v_key) => {
                let y = snapshot.val()[v_key];
                y.type_key = v_key;
                if(APPLIED.indexOf(v_key) === -1 && y.name !== 'Frontliners'){
                    
                    USER_TYPE.push(y);
                } 
                if(APPLIED.indexOf(v_key) === -1 && y.name === 'Frontliners'){
                    this.setState({frontLinersKey: v_key})
                    // FRONTLY.push(y);
                } 
            })
    
            that.setState({companyList:USER_TYPE, });
          } else {
            // console.log("Firebase is empty");
            that.setState({front_type:'', companyList:''})
          }
        })
    }
    
    cancel = () => {
        this.setState({role:'', vehicleSelected: '', companyName: '', screenshot: ''})
        this.props.navigation.goBack()
    }

    getVerified = () => {
        this.setState({loading:true})
    
        let that = this;
        let FUID = this.props.mobx_auth.FUID;
        let PLATE = this.state.vehicleSelected
        let USERTYPE_KEY = this.state.roleKey
        // let ARRIMAGE = this.state.imagey
        if(that.state.vehicleSelected === 'Select vehicle'){
          Alert.alert("Oh-oh!", "Please select a vehicle to be verified")
          that.setState({loading:false})
          return;
        }
    
        if(that.state.companyName === 'Choose your company'){
          Alert.alert("Oh-oh!", "Please choose a company you represent")
          that.setState({loading:false})
          return;
        }
        if(that.state.screenshot === ''){
          Alert.alert("Oh-oh!", "Please upload a screenshot of your profile with info needed")
          that.setState({loading:false})
          return;
        }
    
        const imageuri =that.state.screenshot
    
        //push into firebase storage
        const refFile= firebase.storage().ref().child('bubu').child(FUID).child(PLATE).child(USERTYPE_KEY);

            let IMAGE_URL;

            refFile.putFile(imageuri).then(url => {
                refFile.getDownloadURL().then((url) => {
                    IMAGE_URL = url;
                })
               
            // console.warn('img', url)

            })
            .then(() =>{
            //update application
            firebase.database().ref(`business_approval/${FUID}/${USERTYPE_KEY}/${PLATE}/`).update({
                name: this.state.name,
                Phone: this.state.phone,
                email: this.state.email,
                timestamp: Date.now()
            })
            }).then(() => {
            // update on users info
            firebase.database().ref(`plate_number/${PLATE}/business/${USERTYPE_KEY}/`).update({
                name:that.state.companyName,
                verified: false,
                screenshot: IMAGE_URL,
                date_applied: Date.now(),
            }).then(() =>{
                that.setState({modalVisible:false, loading:false, modalUser:false, modalVehicle:false, modalFront: false, companyName: 'Choose your company', screenshot:'', vehicleSelected:'Select Vehicle'})
            }).then(() => {
                that.setState({modalDone: true})
            })

            }).catch(error => {
            console.warn(error.message);
            Alert.alert('Sorry, please try again.', 'If issue persists, do contact our customer service.')
            that.setState({loading: false})
            });
    
    
              // Alert.alert('Success', "Your application is being processed.Thank you for your patience.")
        }
    
    

    //==== 

    selected = (plate) => {
        this.setState({vehicleSelected :plate, modalVehicle:false,  })
        this.fetchApplied(plate)
    }
    
    vehicleList = () => {
        let display 
        let DATA = this.state.vehicleData 

        if(DATA !== ''){
            display = DATA.map((item, index) => (
                <TouchableOpacity style={index !== 0 ?  styles.norm : styles.last}
                    onPress={() => {this.selected(item)}}>
                    <Image source={car} style={styles.icon}/>
                    <Text style={{fontSize:17,}}>{item}</Text>
                </TouchableOpacity>
        
              ))
        }

        return display 
    }

    roleList = () => {
        let display 
        display = 
        <View>
            <TouchableOpacity style={styles.norm }
                    onPress={() => this.setState({role:'Medical Staff',companyName:'Frontliners', roleKey:this.state.frontLinersKey, modalRole:false}) }>
                <Image source={medic} style={styles.icon}/>
                <Text style={{fontSize:17,}}>Medical Staff</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.last}
            onPress={() =>  this.setState({role:'eHailing', modalRole:false})}>
                <Image source={driver} style={styles.icon}/>
                <Text style={{fontSize:17,}}>e-Hailing Driver</Text>
            </TouchableOpacity>
        </View>

        return display 

    }

    handleCompany = () => {
        this.setState({roleKey: item.type_key, companyName: item.name,modalCompany:false,})
    }

    companyList = () => {
        let display 
        let LIST = this.state.companyList

        if(LIST !== ''){
            display = LIST.map((item, index) => (
                (item.type_key !== this.state.key ? 
                  <TouchableOpacity style={index !== 0 ?  styles.norm : styles.last}
                    onPress={() => {this.handleCompany(item)}}>
                      <Image source={item.logo !== '' ? {uri:item.logo} : driver} style={styles.icon}/>
                      <Text style={{fontSize:17,}}>{item.name}</Text>
                    </TouchableOpacity>
                  : null)
                ))
            
        }
        return display 

    }

    pickImage = () => {
        const options = {quality:1,maxWidth: 800, maxHeight: 800 }
        if(this.state.screenshot !== ''){
          alert('You can only upload 1 picture maximum')
        } else {
        ImagePicker.launchImageLibrary(options, response => {
          if (response.didCancel) {
            // alert('You cancelled image picker');
          } else if (response.error) {
            Alert.alert('Sorry', 'An error occured. Please contact SERV Team if the issue still persists');
          } else {
              // arrImage.push(response.uri)
            // const source = {response.uri };
              this.setState({
                screenshot: response.uri
                // imagey: arrImage
              });
          }
        });
      }
    
    };

      

    checkVehi = () => {
        if(this.state.vehicleSelected === 'Select vehicle'){
          Alert.alert('Oh-oh!', 'Please select your vehicle first')
    
        } else {
          this.setState({modalRole:true})
        }
    }

    render(){

        let submitButton;
        if(this.state.loading === false){
        submitButton =
            <Button 
                onPress={this.getVerified}
                bgColor='#080880'
                title='Verify Me'
                marginTop={30}
                width= {WIDTH2}
           />
        } else {
        submitButton =
            <View style={{borderWidth:2, borderColor:'#080880',backgroundColor:'#080880', borderRadius:10, padding:15, width:WIDTH2, marginTop:15, alignItems:'center'}}>
                <ActivityIndicator style={{ alignSelf:'center'}} size="small" color="#fff" />
            </View>
        }

        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#080880'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Get Verified' onPress={this.cancel} />
                    
                    <ScrollView style={{padding:20}} >
                        <View style={{ marginBottom:15, padding:15, paddingBottom:0}}>
                            <Text style={{color:'#080880', fontFamily: "ProximaNova-Bold", textAlign:'center', fontSize:20, marginBottom:10}}>You're the best and let's get you rewarded!</Text>
                            <Text style={{fontSize:11}}>Snap a picture or a screenshot of any form of acknowledgement that shows you are on duty during this MCO (eg: staff ID, driver profile page, letter etc.) and upload it here. We will review and verify your documents.</Text>
                            <Text style={{fontSize:11, marginTop:10}}>We will review and verify your documents.</Text>
                        </View>

                        <PickerTextInput 
                            title= 'VEHICLE'
                            value={this.state.vehicleSelected} 
                            onPress={() => this.setState({modalVehicle:true})} 
                        />
                        <PickerTextInput 
                            title= 'ROLE'
                            value={this.state.role} 
                            onPress={this.checkVehi} 
                        />
                        {this.state.role === 'eHailing' ?
                        <PickerTextInput 
                            title= 'COMPANY'
                            value={this.state.companyName} 
                            onPress={() => this.setState({modalCompany:true})} 
                        />
                        : null}

                        {this.state.screenshot === '' ? 
                            <View style={{flexDirection:'row', alignItems:'center', marginTop:10, marginBottom:10}}>
                                <TouchableOpacity onPress={this.pickImage} style={styles.chooseButton}>
                                  <Icon name='upload' size={15} color='#fff' />
                                </TouchableOpacity>
                                <View style={{borderBottomColor:'#080880', borderBottomWidth:2, paddingBottom:6, width:'80%'}}>
                                <Text style={{color:'grey', fontSize:12}}>No file is chosen</Text>
                                </View>
                                
                            </View>
                            
                            : null }
                        
                        <Text style={{fontSize:10,color:'tomato' }}>*Please make sure your uploaded documents contain your name that is the same as in your profile. Do not forget to update and verify your profile's contact details.</Text>
                        <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-around'}}>
                        {/** Display selected image */}
                        {this.state.screenshot ? (
                            <View style={{marginBottom:10, marginTop:20}}>
                            <Image
                            source={{uri:this.state.screenshot}}
                            style={{height:100, width:100, alignItems:'center',}}
                            />
                            <AntDesign name='closecircle' size={20} style={{position:'absolute', right:-5, top:-6}} onPress={() => this.setState({screenshot:''})} color='#e85b1a' />
                            </View>
                        ) : null}

                        </View>

                        {submitButton}
                    </ScrollView>
                    

                    <MiddleModal modalName={this.state.modalVehicle} height="94%"           
                        children={
                            <View style={{flex:1}} >                                
                                <Text style={{fontSize:20,marginBottom:15, fontFamily: "ProximaNova-Bold"}}>Select Your Vehicle</Text>
                                    <ScrollView showsVerticalScrollIndicator={false}>
                                    {this.vehicleList()}
                                    </ScrollView>

                                    <Button 
                                        onPress={()=> this.setState({modalVehicle:false})}
                                        title='Close'
                                        bgColor='#080880'
                                        width={WIDTH}
                                        marginTop={20}
                                    />

                            </View>
                    } />


                    <BottomModal
                        modalName={this.state.modalRole}
                        onPress={() => this.setState({modalPicture:false})}
                        style={styles.innerModal}
                        children = {
                        <View style={{padding:20}} >
                            <Text style={{fontSize:20,marginBottom:15, fontFamily: "ProximaNova-Bold"}}>Select Your Role</Text>

                            {this.roleList()}

                            <Button 
                                onPress={()=> this.setState({modalRole:false})}
                                title='Close'
                                bgColor='#080880'
                                width={WIDTH}
                                marginTop={20}
                            />
                        
                        </View>
                        }
                    />

                    <MiddleModal modalName={this.state.modalCompany} height="94%"           
                        children={
                            <View style={{flex:1}} >                                
                                <Text style={{fontSize:20,marginBottom:15, fontFamily: "ProximaNova-Bold"}}>Select Your Company</Text>
                                    <ScrollView showsVerticalScrollIndicator={false}>
                                    {this.companyList()}
                                    </ScrollView>

                                    <Button 
                                        onPress={()=> this.setState({modalCompany:false})}
                                        title='Close'
                                        bgColor='#080880'
                                        width={WIDTH}
                                        marginTop={20}
                                    />

                            </View>
                    } />

                    <MiddleModal modalName={this.state.modalDone}
                        children ={
                            <View>
                                <Text style={{color:'#080880', fontFamily: "ProximaNova-Bold", fontSize:25, marginBottom:10, marginTop:10, textAlign:'center'}}>Thank you for your submission. We will notify you in a bit.</Text>
                                <Text style={{fontSize:12, textAlign:'center', marginBottom:25}} >Submission verification usually takes around 2 to 7 working days to get reviewed and approved. We will send you a notification once you’re good to go.</Text>

                                    {/* <Image source={stats} style={{height: Dimensions.get('window').width/4, width: Dimensions.get('window').width/4, marginBottom:10}} />  */}
                                
                                <Button 
                                    onPress={this.cancel}
                                    title='Got it!'
                                    width={WIDTH}
                                    marginTop={15}
                                    bgColor='#080880'
                                />
                                
                            </View>
                        }
                    />

                </View>
            </SafeAreaView>
        )
    }
}

RewardsPrimoApply = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(RewardsPrimoApply))
export default RewardsPrimoApply

const styles = StyleSheet.create({
    norm : {padding:10, borderTopColor:'#ccc', borderTopWidth:0.5, flexDirection:'row', alignItems:'center',  flexDirection:'row',  },
    last: {padding:10, flexDirection:'row', alignItems:'center',  flexDirection:'row', },
    icon: { 
        height:Dimensions.get('window').width / 8, 
        width:Dimensions.get('window').width / 8, 
        marginRight: 25, 
        resizeMode:'contain', 
        // borderWidth:0.5, 
        // borderColor:'#ccc'
    },
    chooseButton: {
        backgroundColor:'#080880', alignItems:'center', justifyContent:'center',marginRight:10, padding:8, paddingLeft:14, paddingRight:14, borderRadius:5,
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowRadius: 2,
                shadowOffset: { width: 0, height: 3},
                shadowOpacity: 0.4,
            },
            android: {
                elevation: 3
            },
        }),
    },
    innerModal: {     
        borderTopLeftRadius: 10,
        borderTopRightRadius:10,
        width:Dimensions.get('window').width,
        position:'absolute',
        bottom:0,
        backgroundColor: '#fff',
            ...Platform.select({
                ios: {
                    shadowColor: '#000',
                    shadowRadius: 2,
                    shadowOffset: { width: 0, height: 3 },
                    shadowOpacity: 0.6,
                },
                android: {
                    elevation: 2
                },
            }),
    },

  
  
})