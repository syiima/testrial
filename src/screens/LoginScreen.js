import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text,Keyboard, View,KeyboardAvoidingView,StatusBar,Alert,Dimensions,Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/AntDesign"
import { cloud_serv_provideradmin_sandbox, cloud_serv_provideradmin_production, cloud_serv_user_sandbox, cloud_serv_user_production } from "../util/FirebaseCloudFunctions.js"
import AsyncStorage from '@react-native-async-storage/async-storage';

import Header from '../components/Header.js'
import FormTextInput from '../components/FormTextInput'
import Loader from '../components/Loader.js';
import Button from '../components/Button'
import serv from '../assets/servnew.png'
// import serv from '../assets/servOld.png'
import servS from '../assets/servS_white.png'
import PasswordTextInput from "../components/PasswordTextInput.js"

const CONST_HEIGHT = Dimensions.get('window').height;
const CONST_WIDTH = Dimensions.get('window').width;

class LoginScreen extends React.Component{
    constructor(){
        super()
        this.state = {
            email: '',
            password : '',
            relogin :0,
            hidePassword: true,
            error: ''
        }
    }
    
    componentDidMount(){
        this.getConst()
        this.generateToken()
        this.props.mobx_loader.setLoading(false);

    }
    
    getConst(){
        firebase.database().ref(`consts/`).once('value').then((snp) => {
          if(snp.exists()){
            let X = snp.val().relogin
            this.setState({relogin: X})
          }
        })
    }
    
    
    generateToken = async() => {
        let urlP, urlU;
        if(this.props.mobx_config.config === 0) {
           urlP = cloud_serv_provideradmin_sandbox.createCSRFP
           urlU = cloud_serv_user_sandbox.createCSRFU
        }
        if(this.props.mobx_config.config === 1) {
           urlP = cloud_serv_provideradmin_production.createCSRFP
           urlU = cloud_serv_user_production.createCSRFU
        }
    
        let finalP = urlP + '?device=mobile';
        let finalU = urlU + '?device=mobile';
    
        await fetch(finalP)
        .then( res => res.json())
        .then(res =>  {
          let CSRFTokenP = res.data.CSRFTokenP
          let CSRFSecretP = res.data.CSRFSecretP
    
          let tokenP = AsyncStorage.setItem('CSRFTokenP', CSRFTokenP)
          let secretP = AsyncStorage.setItem('CSRFSecretP', CSRFSecretP)
        })
    
        await fetch(finalU)
        .then( res => res.json())
        .then(res =>  {
          let CSRFTokenU = res.data.CSRFTokenU
          let CSRFSecretU = res.data.CSRFSecretU
    
          let tokenU = AsyncStorage.setItem('CSRFTokenU', CSRFTokenU)
          let secretU = AsyncStorage.setItem('CSRFSecretU', CSRFSecretU)
        })
        
    }

      
    login = () => {
        let that = this;
        if(this.state.email == ''){
          Alert.alert("Sorry","Email cannot be empty")
          return;
        }
        if(this.state.password == ''){
          Alert.alert("Sorry","Password cannot be empty")
          return;
        }
    
        that.props.mobx_loader.setLoading(true);

        const {email, password} = this.state;
    
        if(this.state.email && this.state.password){
          firebase.auth().signInWithEmailAndPassword(email, password)
          .then((userData) => {
            let uid = userData.user.uid;
    
            firebase.database().ref("users/" + uid).update({relogin: this.state.relogin})
    
            that.props.mobx_auth.setCheck(3);
            that.props.mobx_auth.setFUID(userData.user.uid);
            that.props.mobx_loader.setLoading(false);
            Keyboard.dismiss();
          }).catch((err) => {
            //   console.warn('err', err);
                let MSG ='Login failed. Your email or password is incorrect. Please try again.'
            // alert(err)
            that.props.mobx_loader.setLoading(false);
            that.setState({error:MSG})
            Keyboard.dismiss() 
            that.props.mobx_loader.setLoading(false);
    
          })
        }
    }

    forgotPassword = () => {
        this.props.navigation.navigate('ForgotPassword')
    }

    register = () => {
        this.props.navigation.navigate('RegisterScreen')
    }

    managePasswordVisibility = () => {
        this.setState({ hidePassword: !this.state.hidePassword });
    }
    
    render(){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <StatusBar barStyle="light-content" backgroundColor='#1d1d1d'/>
                <KeyboardAvoidingView
                behavior={(Platform.OS === 'ios') ? "padding" : null} enabled
                style={{ flex: 1,backgroundColor:'#080880' }}
                >
                <Loader loading={this.props.mobx_loader.loading}/>
                
                    
                <View style={{backgroundColor: '#fafdff',  flex:1, }}>
                <View style={styles.halfCircle} >
                        <Image source={servS}  style={styles.logo} />
                    </View>
                   
                    <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flex:1, padding:20,justifyContent:'center',}}>
                    
                        
                        
                        
                        <View style={{marginBottom:50}}>
                        <Text style={styles.mainTitle} >Welcome back! </Text>
                        <Text style={styles.smolTitle}>Login to continue</Text>
                        </View>
                        

                        <FormTextInput 
                            title= 'EMAIL'
                            value={this.state.email}
                            placeholder= 'eg: myemail@gmail.com'
                            keyboardType='email-address'
                            onChangeText={(x) => this.setState({email:x})}
                            onSubmitEditing={() => this.password.focus()}
                        />

                        <PasswordTextInput 
                            title= 'PASSWORD'
                            value={this.state.password}
                            placeholder= 'password here'
                            onChangeText={(x) => this.setState({password:x})}
                            hidePassword ={this.state.hidePassword}
                            onPress={this.managePasswordVisibility}
                            bottom={false}
                            returnKeyType='done'
                            inputRef={ref => this.password = ref}
                        />

                        <TouchableOpacity onPress={this.forgotPassword} style={{alignSelf:'flex-end', marginBottom:20}}>
                            <Text style={{fontSize:13,color:'#686868', fontFamily:'ProximaNova-Bold',  }}>Forgot Password?</Text>
                        </TouchableOpacity>

                    
                        <Button 
                            onPress={this.login}
                            title='Log me in'
                            bgColor='#080880'
                            width = {CONST_WIDTH - 40}
                        />

                        <View style={{paddingLeft:20, marginTop:10,paddingRight:20}}>
                            <Text style={{color:'red', textAlign:'center'}}>{this.state.error}</Text>
                        </View>

                        <TouchableOpacity onPress={this.register} style={{marginTop:20,}}>
                            <Text style={{color:'#686868', textAlign:'center', fontFamily: "ProximaNova-Bold"}}>New here? <Text style={{color:'#080880',fontFamily: "ProximaNova-Bold",}}>Create a new account</Text></Text>
                        </TouchableOpacity>


                    </ScrollView>
                </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}

LoginScreen = inject('mobx_auth', 'mobx_loader', 'mobx_config')(observer(LoginScreen))
export default LoginScreen

const styles = StyleSheet.create({
    top :{
        backgroundColor:'#080880',alignItems:'center', justifyContent: 'center',padding:20, paddingTop:40,
        width: Dimensions.get('window').width,
        borderBottomLeftRadius: Dimensions.get('window').width,
        borderBottomRightRadius: Dimensions.get('window').width,
        position: 'absolute',
        top: -20
    },
    logo: {
        paddingTop: 10, 
        // borderWidth:1,
        
        height: Dimensions.get('window').width /10,
        width :Dimensions.get('window').width / 3,
        resizeMode: 'contain'
    },
    mainTitle :{fontSize:35, fontFamily:'ProximaNova-Bold', marginBottom:10,},
    smolTitle:{  fontSize:16},
    halfCircle: {
        backgroundColor:'#080880',
        // height: Dimensions.get('window').width/4, 
        // width: Dimensions.get('window').width,
        // borderBottomLeftRadius: Dimensions.get('window').width / 3,
        // borderBottomRightRadius:Dimensions.get('window').width / 4,
        borderBottomLeftRadius:20,
        borderBottomRightRadius: 20,
        padding:20,
        alignItems:'center',
        justifyContent:'center',
        // position:'absolute',
        // top: 0
    }

})