import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/AntDesign"

import Header from '../components/Header.js'

class RewardsStatusDesc extends React.Component{
    render(){
        let status;

        if(this.props.navigation.state.params.item.verified === false){
        status =
        <View style={{...styles.boxy, backgroundColor:'#FEF2CC'}}>
            <Text style={{...styles.boldText, fontSize:18, marginBottom:15,}}>We are reviewing your application and will notify you shortly.</Text>
            <Text style={{fontSize:11}}>Please be patient and await for our team to review your application. We will send a notification through the app for the results.</Text>
        </View>
        }
        if(this.props.navigation.state.params.item.verified === true){
        status =
        <View style={{...styles.boxy, backgroundColor:'#D9EAD3'}}>
            <Text style={{...styles.boldText, fontSize:18, marginBottom:15,}}>Congratulations! All your hard work will now be rewarded with more perks. Keep the good work.</Text>
            <Text style={{fontSize:11}}>Your application have been reviewed and approved by our team. Enjoy your rewards by going to Perks page.</Text>
        </View>
        }
        if(this.props.navigation.state.params.item.verified === 'decline'){
        status =
        <View style={{...styles.boxy, backgroundColor:'#F4CCCC'}}>
            <Text style={{...styles.boldText, fontSize:18, marginBottom:15,}}>Uh oh. It seems your application was declined by our team.</Text>
            <Text style={{fontSize:11}}>We have reviewed and found certain information was not up to our standards. You may however submit another application and try again.</Text>
        </View>
        }

        let bottomText;
        if(this.props.navigation.state.params.item.verified === false){
        bottomText =
            <View style={{...styles.stats, backgroundColor:'#ffce00', }}>
            <Text style={{fontFamily: "ProximaNova-Bold", color:'white'}}>Pending</Text>
            </View>
        }
        if(this.props.navigation.state.params.item.verified === true){
        bottomText =
            <View style={{...styles.stats,backgroundColor:'#38cc26'}}>
            <Text style={{fontFamily: "ProximaNova-Bold", color:'white'}}>Approved</Text>
            </View>
        }
        if(this.props.navigation.state.params.item.verified === 'decline'){
        bottomText =
        <View style={{...styles.stats, backgroundColor:'#db3035'}}>
            <Text style={{fontFamily: "ProximaNova-Bold", color:'white'}}>Rejected</Text>
        </View>
            
        }

        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#080880'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Status Details' onPress={() => this.props.navigation.goBack()} />
                    
                <ScrollView showsVerticalScrollIndicator={false}>
                {/* <Image source={review_stats} style={{height:Dimensions.get('window').width / 2, alignSelf:'center', resizeMode:'contain',  marginTop:15 }} /> */}

                    {status}
                    <View style={{...styles.boxy, backgroundColor:'#fff', marginBottom:20}}>
                        <Text style={{ fontSize:12}}>STATUS</Text>
                        {bottomText}

                        <Text style={{fontSize:12, marginTop:15}}>COMPANY</Text>
                        <View style={{flexDirection:'row', alignItems:'center', marginTop:5}}>
                            {/* <Image source={taxidriver} style={{height:Dimensions.get('window').width / 8, width:Dimensions.get('window').width / 8, marginRight: 15, resizeMode:'contain' }} /> */}
                            <Text style={styles.boldText}>{this.props.navigation.state.params.item.name}</Text>
                        </View>

                        <Text style={{ fontSize:12, marginTop:15}}>DOCUMENT SUBMITTED</Text>
                        <View style={styles.ssimage}>
                            <Image source={{uri:this.props.navigation.state.params.item.screenshot}} style={{height:80, width:80, borderRadius:5,}}/>
                        </View> 

                    </View>


                </ScrollView>

                    
                </View>
            </SafeAreaView>
        )
    }
}

RewardsStatusDesc = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(RewardsStatusDesc))
export default RewardsStatusDesc

const styles = StyleSheet.create({
    stats: {borderRadius:4, padding:5, marginTop:5, alignSelf:'flex-start', paddingLeft:15, paddingRight:15},
    boldText:{fontFamily:'ProximaNova-Bold'},
    ssimage : {height:80, width:80, marginTop:15,marginBottom:10, borderRadius:15, backgroundColor:'white',
    ...Platform.select({
      ios: {
        shadowColor: '#000',
        shadowRadius: 3,
        shadowOffset: { width: 0, height: 2},
        shadowOpacity: 0.4,
      },
      android: {
        elevation: 3
      },
    }),
  },
  boxy:{
    padding:15, 
    margin:20, 
    borderRadius:15, 
    marginBottom:0,
    ...Platform.select({
        ios: {
          shadowColor: '#000',
          shadowRadius: 3,
          shadowOffset: { width: 0, height: 2 },
          shadowOpacity: 0.4,
        },
        android: {
          elevation: 2
        },
      }),
},
})