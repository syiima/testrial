import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/MaterialCommunityIcons"

import Header from '../components/Header.js'
import step1 from '../assets/icons/step1.png'
import step2 from '../assets/icons/step2.png'
import step3 from '../assets/icons/step3.png'

class ReferFriendHIW extends React.Component{

    tncLink = () => {
        let LINK = 'https://serv.my/referral-program-terms-conditions/'
        this.props.navigation.navigate('Webview', {link:LINK} )
    }


    render(){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                    <Header backButton = {true}  headerTitle = 'How it works' onPress={() => this.props.navigation.goBack()} />

                    <ScrollView contentContainerStyle={{padding:25}} showsVerticalScrollIndicator={false} >
                        <Text style={styles.totle}>Sharing is caring!</Text>

                        <Text style={{marginBottom: 20}} >Keep your loved ones safe by introducing them to the SERV app to ensure their vehicles are well-serviced and running at their peak performance!</Text>

                        <Text style={{fontFamily: "ProximaNova-Bold",color:"#080880", fontSize:16, textAlign:'center' }}>Here's how to get started:</Text>
                        
                        <View style={{marginTop:20 }}>
                            <View style={{marginBottom: 20, alignItems:'center', flexDirection:'row'}}>
                                <Image source={step1} style={styles.img} />
                                {/* <View style={{marginRight:15, backgroundColor:'#080880', height:60, width:60, borderRadius:30, alignItems:'center', justifyContent:'center'}}>
                                    <Icon name='account' size={30} color='#fff' />
                                </View> */}
                                <View style={{flex:1}}>
                                <Text style={{fontFamily: "ProximaNova-Bold", color:"#080880", fontSize:18}}>Step 1 </Text>
                                <Text style={{marginTop:10, fontSize:14}}>Share your special code with your loved ones</Text>
                                </View>
                            </View>
                            <View style={{marginBottom: 40,alignItems:'center',  flexDirection:'row', }}>
                                <Image source={step2} style={styles.img}/>
                                <View style={{flex:1}} >
                                    <Text style={{fontFamily: "ProximaNova-Bold", color:"#080880",  fontSize:18}}>Step 2 </Text>
                                    <Text style={{marginTop:10,fontSize:14 }}>Get them onboard by using the code during registration</Text>
                                </View>
                            </View>
                            <View style={{marginBottom: 40,alignItems:'center',  flexDirection:'row', marginRight:15}}>
                                <Image source={step3} style={styles.img} />
                                <View style={{flex:1,}}>
                                    <Text style={{fontFamily: "ProximaNova-Bold", color:"#080880",  fontSize:18}}>Step 3 </Text>
                                    <Text style={{marginTop:10,fontSize:14 }}>Now both of you can enjoy discounts on your next service</Text>
                                </View>
                            </View>

                            <TouchableWithoutFeedback onPress={this.tncLink}>
                                <Text style={{fontFamily: "ProximaNova-Bold", color:"#080880", fontSize:14}}>*Terms & Conditions Applied</Text>
                            </TouchableWithoutFeedback>
                        </View>

                        <View style={{height: 25}} />
                    
                    </ScrollView>
                </View>
            </SafeAreaView>
        )
    }
}

ReferFriendHIW = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(ReferFriendHIW))
export default ReferFriendHIW

const styles = StyleSheet.create({
    totle:{
        fontFamily: "ProximaNova-Bold",
        color:"#080880",
        fontSize: 25,
        marginBottom:25,
        textAlign:'center'
    },
    img :{ height:Dimensions.get('window').width / 4, width:Dimensions.get('window').width / 4,marginRight:15,},
})