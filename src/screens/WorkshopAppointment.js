import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,Linking,PermissionsAndroid, Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'

import AntIcon from "react-native-vector-icons/AntDesign";
import Icon from "react-native-vector-icons/FontAwesome";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Moment from 'moment';
import * as AddCalendarEvent from 'react-native-add-calendar-event';
import { NavigationActions, StackActions } from 'react-navigation';

import Header from '../components/Header.js'
import MiddleModal from "../components/MiddleModal.js";
import BottomModal from "../components/BottomModal.js"
import Button from '../components/Button'

import maps_logo from '../assets/maps_logo.png'
import waze_logo from '../assets/waze_logo.png'
import Appointment from '../assets/appointment.png'

const WIDTH = Dimensions.get('window').width - 30


class WorkshopAppointment extends React.Component{
    constructor(){
        super()
        this.state = {
            requestData: '',
            ws_data: '',
            logo: '',
            
            modalMap: false,
            modalCancel:false,
            add:'',
            name:'',
            ownerID: ''
        }
    }

    componentDidMount(){
        this.fetchBooking()
        this.fetchWorkshopDets()
        this.statusListener()

    }

    fetchBooking(){
        let that = this
        let WS_ID =   this.props.mobx_retail.R_ID
        let WSMAIN_ID =   this.props.mobx_retail.R_GROUPID
        let BOOKID =   this.props.mobx_retail.R_WALKIN_BOOKID
        let PLATE  = this.props.mobx_carkey.PLATE

        firebase.database().ref(`appointment_walkin/${WSMAIN_ID}/${WS_ID}/${BOOKID}`).once('value').then((snapshot) => {
            if(snapshot.exists()){
                let X = snapshot.val()
                this.setState({requestData : X})
            }
        })

        firebase.database().ref(`plate_number/${PLATE}/owner/`).once('value', (snp) => {
            if(snp.exists()){
                let ID = snp.val().FUID

                this.setState({ownerID: ID})
            }
        })
    }

    fetchWorkshopDets(){
        let that = this
        let WSDATA = []
        let BOOK_ID =  this.props.mobx_retail.R_WALKIN_BOOKID
        let RETAIL =   this.props.mobx_retail.R_ID
        let RETAIL_MAIN =   this.props.mobx_retail.R_GROUPID
        // console.warn('hellllooo', WSMAIN_ID, WS_ID);
        
        firebase.database().ref(`retail/${RETAIL_MAIN}/${RETAIL}/`).once('value').then((snapshot) => {
            if(snapshot.exists()){
                let a = snapshot.val()
                that.setState({ws_data: a})
            }
        })

        firebase.database().ref(`retail_main/${RETAIL_MAIN}/logo`).once('value').then((snapshot) => {
            if(snapshot.exists()){
                let b = snapshot.val()
                that.setState({logo: b})
            }
        })

    }

    statusListener(){
        let that = this;
        let VEHICLEID = this.props.mobx_carkey.CarKey;
        let FUID = this.props.mobx_auth.FUID;
        let PLATE = this.props.mobx_carkey.PLATE;

        firebase.database().ref(`plate_number/${PLATE}/book_walkin/`).on('child_changed', async function(snp){
            if(snp.exists()){
                if(snp.val() === 'Appointment Declined' || snp.val() === 'Appointment' || snp.val() === 'Appointment Accepted' || snp.val() === 'Appointment Changed'){
                    that.fetchBooking()
                    that.fetchWorkshopDets()
                }
            }
        })
    }

    //===== 
    getLatLongWS = async(item, x) => {
        this.setState({modalMap: false})
        let ADDRESS = item.address
        let URL = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + ADDRESS +'&key=AIzaSyDV3WviHkCYt82VA90LiRLLMB8RGQwGSYA' 

        await fetch(URL)
          .then(data => data.json())
          .then(data => {
            let LAT = data.results[0].geometry.location.lat
            let LONG = data.results[0].geometry.location.lng

            if(LAT !== 0 || LONG !== 0) {
                if(x === 'gmaps'){
                    firebase.analytics().logScreenView({
                        screen_name: 'gmaps_appt',
                        screen_class: 'gmaps_appt',
                      }) 
                    Linking.openURL('https://www.google.com/maps/search/?api=1&query=' + LAT + ',' + LONG)
    
                } else {
                    firebase.analytics().logScreenView({
                        screen_name: 'waze_appt',
                        screen_class: 'waze_appt',
                      }) 
                    Linking.openURL('https://waze.com/ul?ll=' + LAT + ',' + LONG + '&navigate=yes');
                }
            } else {
                Alert.alert('Sorry', 'The workshop haven\'t updated their location.')
            }

          })
    }


    callWS = (item) => {
        let PHONE = item.phone

        if(PHONE === ''){
        Alert.alert('Sorry', 'The workshop haven\'t updated their phone number.')
        } else {
          if (Platform.OS === 'android') { phoneNumber = `tel:${PHONE}`; }
          else {phoneNumber = `telprompt:${PHONE}`; }
          Linking.openURL(phoneNumber);
        }
    }


    //==== render === 
    renderOpenClose = (x) => {
        let display
        let DATA = x.business_hrs
        let TODAY = (Moment().format('dddd')).toLowerCase()
        let TIME = Moment().format('hh:mm:ss') //current
        let STATUS;
    
        if(DATA !== undefined){
        for (const d_key in DATA) {          
            if(TODAY === d_key){
            //closed
            if(DATA[d_key].open === 'closed'){
                STATUS = 'Closed'
            }             
            if(DATA[d_key].open !== 'closed') {
    
                // open
                let finalH
                let HOURo
                let HOUR = DATA[d_key].open.split(':')[0]
                let MINUTE =  (DATA[d_key].open.split(':')[1]).substring(0,2)
                
                if(HOUR < 10){
                    HOUR += '0'
                }
    
                let AMPM = (DATA[d_key].open.split(':')[1]).substring(4,2)
                // // console.warn('hmmm', AMPM);
                if(AMPM === 'PM'){
                    let x = HOUR
                    let y = '12'

                    HOURo = parseFloat(x) + parseFloat(y)
                }  else {
                    HOURo = HOUR
                }
                finalH = HOUR.toString()
                let FINAL = finalH + ':' + MINUTE + ':' + '00'
                
    
                //CLOSE
                let HOURc
                let finalC
                let HOURC = DATA[d_key].close.split(':')[0]
                let MINUTEC =  (DATA[d_key].close.split(':')[1]).substring(0,2)
                
                if(HOURC < 10){
                    HOURC = 0 + HOURC
                }
    
                let AMPMC = (DATA[d_key].close.split(':')[1]).substring(4,2)
                // console.warn('hmmm', MINUTEC);
                if(AMPMC === 'PM'){
                    let x = HOURC
                    let y = 12

                    HOURc = parseFloat(x) + parseFloat(y)
                } else{
                    HOURc = HOURC
                }
                finalC = HOURc.toString()
                let CLOSE = finalC + ':' + MINUTEC + ':' + '00'
                // check if open
                if(TIME > FINAL || TIME < CLOSE ){
                STATUS = 'Closed. Will open at ' + DATA[d_key].open
                }
                if(TIME < FINAL){
                STATUS = 'Open until ' + DATA[d_key].close
                }
                
            }
            }
            
        }
        } else {
        STATUS = 'Not Available'
        }
        
        display = <Text style={{fontSize:13, marginBottom:10, marginTop:10}}>{STATUS}</Text>
        return display
    }

    showReason = (reason) => {
        let disp, bigdisp

        if (typeof reason === 'string'){
            bigdisp = 
            <View style={{flexDirection:'row'}}>
                <Text style={{flex:1}}>Reason</Text>
                <Text style={styles.boldText}>: </Text>
                <Text style={{...styles.boldText, fontSize:17, flex:1.5}}>{reason} </Text>
               
            </View>

        }
        if (typeof reason === 'object'){
            disp = reason.map((x, index) => (
            <Text style={{...styles.boldText, fontSize:17, flex:1.5}}>{index + 1}. {x} </Text>
            ))

            bigdisp = 
            <View>
            <View style={{flexDirection:'row'}}>
                <Text style={{flex:1}}>Reason</Text>
                <Text style={styles.boldText}>: </Text>
                <Text style={{...styles.boldText, fontSize:17, flex:1.5}}> </Text>
                
            </View>
            {disp}
            </View>
           

        }
        return bigdisp
    }

    renderTime = () => {
        let item = this.state.requestData
        let STATUS =  this.props.mobx_retail.STATUS
        let FUID = this.props.mobx_auth.FUID
        let display

        if(item !== ''){

        display =
            <View style={{padding:20}}>
                <View style={{flexDirection:'row'}}>
                    <Text style={{flex:1}}>Date</Text>
                    <Text style={styles.boldText}>: </Text>
                    <Text style={{...styles.boldText, fontSize:17, flex:1.5}}>{item.customer_day} {item.customer_month} {item.customer_year}</Text>
                </View>
                <View style={{flexDirection:'row'}}>
                    <Text style={{flex:1}}>Time</Text>
                    <Text style={styles.boldText}>: </Text>
                    <Text style={{...styles.boldText, fontSize:17, flex:1.5}}>{item.customer_time} </Text>
                </View>
                <View style={{flexDirection:'row'}}>
                    <Text style={{flex:1}}>Vehicle Plate No</Text>
                    <Text style={styles.boldText}>: </Text>
                    <Text style={{...styles.boldText, fontSize:17, flex:1.5}}>{item.customer_carplate} </Text>
                </View>
                {item.reason !== '' ? this.showReason(item.reason)  : null}
                {item.remark !== '' && item.remark !== undefined ? <View style={{flexDirection:'row'}}>
                    <Text style={{flex:1}}>Remark</Text>
                    <Text style={styles.boldText}>: </Text>
                    <Text style={{...styles.boldText, fontSize:17, flex:1.5}}>{item.remark} </Text>
                </View> : null}
                {item.customer_FUID !== FUID ? 
                <View style={{flexDirection:'row'}}>
                    <Text style={{flex:1}}>Booked by</Text>
                    <Text style={styles.boldText}>: </Text>
                    <Text style={{...styles.boldText, fontSize:17, flex:1.5}}>{item.customer_name} </Text>
                </View> : null }
            </View>
        } 

        return display
    }

    renderWS = () => {
        let item = this.state.ws_data
        let LOGO = this.state.logo
        let dsiaply

        if(item !== ''){
            dsiaply =
                <View style={styles.box}>
                    <View style={{flexDirection:'row'}}>
                        {LOGO !== '' 
                        ? <Image source={{uri: LOGO}} style={{width:Dimensions.get('window').width / 5, height:Dimensions.get('window').width /5  ,resizeMode:'contain', borderRadius:10}}  />
                        : <Image source={{uri:'https://images.says.com/uploads/story_source/source_image/450525/475e.jpg'}} style={{width:Dimensions.get('window').width / 5, height:Dimensions.get('window').width /5  ,resizeMode:'contain', borderRadius:10}}  />
                        }
                        <View style={{flexShrink:1,flex:3, marginLeft:10,  marginRight: 10}}>
                            <Text style={{fontSize:16, fontFamily:'ProximaNova-Bold'}}>{item.name}</Text>
                            {this.renderOpenClose(item)}

                            <View style={{flexShrink:1, marginTop:10, flexDirection:'row', alignItems:'center'}}>
                                <TouchableWithoutFeedback onPress={() => this.setState({modalMap:true})}>
                                    <View style={styles.outerLayer}>
                                        <MaterialCommunityIcons name='navigation' size={15} color='#080880' />
                                        <Text style={{color:"#080880",marginLeft:8}} >Map</Text>
                                    </View>
                                </TouchableWithoutFeedback>
                                <TouchableWithoutFeedback onPress={() => {this.callWS(item)}}>
                                    <View style={styles.outerLayer}>
                                        <Icon name='phone' size={15} color='#080880' />
                                        <Text style={{color:"#080880",marginLeft:8}} >Call</Text>
                                    </View>
                                </TouchableWithoutFeedback>
                                
                            </View>
                        </View>
                    </View>

                    
                    <BottomModal
                        modalName={this.state.modalMap}
                        onPress={() => this.setState({modalMap:false})}
                        style={styles.innerModal}
                        children = {
                        <View >
                            <View style={{padding:20, paddingBottom:0, alignItems:'center'}}>
                                <Text style={{textAlign:'center', fontFamily:'ProximaNova-Bold', fontSize:20, marginBottom:5}}>Open with</Text>             
                            </View>

                            <View style={{margin:30, marginTop:20,  alignItems:'center', justifyContent:'space-around', flexDirection:'row'}}>
                                <TouchableOpacity  onPress={() => {this.getLatLongWS(item, 'gmaps')}} style={{}}>
                                    <Image source={maps_logo} style={{height:50, width:50, marginRight:10, resizeMode:'contain'}}/>
                                    <Text style={{textAlign:'center'}}>Maps</Text>
                                </TouchableOpacity>
                                
                                <TouchableOpacity onPress={() => {this.getLatLongWS(item,'waze')}} style={{}}>
                                    <Image source={waze_logo} style={{height:50, width:50, resizeMode:'contain'}}/>
                                    <Text style={{textAlign:'center'}}>Waze</Text>
                                </TouchableOpacity>

                            </View>

                            <Button bgColor='#080880' title='Cancel' width={WIDTH}  onPress={()=> this.setState({modalMap:false, latlong: ''})} />
                            <View style={{height: 15}} /> 

                        </View>
                        }
                    />  
                </View>
        }
        return dsiaply
    }



    //==== APPT RELATED ==== 
    cancelAppt = (x) => {        
        let GROUP_ID = this.props.mobx_retail.R_GROUPID
        let WORKSHOP_ID = this.props.mobx_retail.R_ID

        let bookID =  this.props.mobx_retail.R_WALKIN_BOOKID
        let PLATE = this.props.mobx_carkey.PLATE;
        let FUID = this.props.mobx_auth.FUID;
        this.setState({modalCancel:false})


        firebase.database().ref(`appointment_walkin/${GROUP_ID}/${WORKSHOP_ID}/${bookID}`)
            .once('value')
            .then((snapshot) => {
            if(snapshot.exists()){
              let X = snapshot.val()
              // console.warn('ehh', X);
              firebase.database().ref(`appointment_archive_walkin/${GROUP_ID}/${WORKSHOP_ID}/${bookID}`).update(X) 
              
              firebase.database().ref(`appointment_archive_walkin/${GROUP_ID}/${WORKSHOP_ID}/${bookID}`)
              .update({timestamp_archived: firebase.database.ServerValue.TIMESTAMP})              
            }
          })
          .then(() => {
              firebase.database().ref(`plate_number/${PLATE}/book_walkin/`).update({
                _book_id: '',
                _book_status: '',
                retail_id:'',
                retail_main:  '',
                day: '',
                time:'',
                year: '',
                month:'',
                booking_by:'',
                },
            )
          })
          .then(() => {
            firebase.database().ref(`appointment_walkin/${GROUP_ID}/${WORKSHOP_ID}/${bookID}/`).remove()
          }).then(() => {
            Alert.alert('Success', 'Your previous appointment has been deleted.')            
          }).then(() => {
            if(x === 'new'){
                let status = this.props.mobx_retail.setSTATUS('');
                firebase.analytics().logScreenView({
                    screen_name: 'BookAppointment_deleted',
                    screen_class: 'BookAppointment_deleted',
                  }) 

                const navigateAction = StackActions.replace({
                    routeName: 'WorkshopBookingAppointment',
                    action: NavigationActions.navigate({ routeName: 'WorkshopBookingAppointment' }),
                  });
                  this.props.navigation.dispatch(navigateAction);
            }  else {
                this.props.navigation.popToTop()
            }
          })
        
    }

    acceptAppt = () => {
        let GROUP_ID = this.props.mobx_retail.R_GROUPID
        let WORKSHOP_ID = this.props.mobx_retail.R_ID

        let bookID =  this.props.mobx_retail.R_WALKIN_BOOKID
        let PLATE = this.props.mobx_carkey.PLATE;
        let FUID = this.props.mobx_auth.FUID;
        
        firebase.database().ref(`plate_number/${PLATE}/book_walkin/`).update({_book_status: 'Appointment Accepted'})
        
        firebase.database().ref(`appointment_walkin/${GROUP_ID}/${WORKSHOP_ID}/${bookID}`)
            .update({status: 'Appointment Accepted'})
            .then(() => {
                Alert.alert('Thank you!', 'Confirmation on the appointment date and time has been sent to the workshop.')
                this.fetchBooking()
            })
        
        
    }

    getPermission = async() => {
        if(Platform.OS === 'android'){
            const grantedRead = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.READ_CALENDAR,
            {
              title: "CALENDAR PERMISSION",
              message: "App needs access to read calendar for appointment date",
            }
        );
        const grantedWrite = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_CALENDAR,
            {
                title: "CALENDAR PERMISSION",
                message: "App needs access to write reminder on calendar ",
            }
        );
    
        if (grantedRead === 'granted' && grantedWrite === 'granted') {
              this.setReminder()
          } else {
            Alert.alert('Sorry', 'Please allow access to calendar in order to set a reminder.')
          }
    
        } else {
            this.setReminder()
        }
    }
    setReminder = () => {
        let item = this.state.requestData
        let NAMEPLACE
        var months = {
            'January' : '01',
            'February' : '02',
            'March' : '03',
            'April' : '04',
            'May' : '05',
            'June' : '06',
            'July' : '07',
            'August' : '08',
            'September' : '09',
            'October' : '10',
            'November' : '11',
            'December' : '12'
        }


        if(item.length !== 0){
           
                let DAY = item.customer_day
                let DDAY
                if(DAY.length < 1){
                    DDAY = '0' + DAY
                } else {
                    DDAY = DAY
                }

                let MONTH = months[item.customer_month]
                let YEAR = item.customer_year
                let TIME = item.customer_time
                
                let HOURo
                let HOUR = TIME.split(':')[0]

                if(HOUR < 10){
                    HOUR = '0' + HOUR
                }
                let MINUTE =  (TIME.split(':')[1]).substring(0,2)
                let AMPM = (TIME.split(':')[1]).substring(2,4)

                if(AMPM === 'PM'){
                    let x = HOUR
                    let y = '12'

                    HOURo = parseFloat(x) + parseFloat(y)
                }  else {
                    HOURo = HOUR
                }

                let FINALH = HOURo.toString()
                
                // let START = YEAR + '/' + MONTH + '/' + DDAY  + 'T' + FINALH + ':' + '00:00.000Z'
                let START = YEAR + '-' + MONTH  + '-' + DDAY + ' ' + FINALH + ':00:00'

                let gg = Moment(START).toDate().toISOString()

                console.warn('hmm',START, gg);   


                let EVENT_TITLE = 'Workshop Appointment Reminder';

                const eventConfig = {
                    title: EVENT_TITLE,
                    startDate: gg,
                    endDate: gg,
                    notes: 'Get ready to SERVice your vehicle!'
                };

                AddCalendarEvent.presentEventCreatingDialog(eventConfig)
                .then(eventInfo => {
                // alert(JSON.stringify(eventInfo));
                })
                .catch(error => {
                    console.warn('err', error.message);
                Alert.alert('Oh-oh!','Please make sure you\'ve allowed Calendar access for SERV app');
                });


        
        }
    }

    scan = (item) => {
        this.props.mobx_retail.setSTATUS('Appointment')
        this.props.mobx_carkey.setPLATE(item.customer_carplate)
        this.props.mobx_carkey.setVTYPE(item.customer_cartype)
        let ADD = this.state.add
        let PLACE = this.state.name
        // console.warn('hmmm', ADD, PLACE);
        firebase.analytics().logScreenView({
            screen_name: 'ScanWorkshop_appt',
            screen_class: 'ScanWorkshop_appt',
          }) 
        this.props.navigation.navigate('WorkshopScan', {
            // qrData : FINAL,
            groupID: this.props.mobx_retail.R_GROUPID,
            workshopID: this.props.mobx_retail.R_ID,
            address: ADD,
            nameplace: PLACE,
            wsdata : this.state.ws_data
        })
    }

    statusBox = () => {
        let disp, TOPTEXT, BUTTONS, BGCOLOR 
        let DATA = this.state.requestData
        let IMG = <Image source={Appointment} style={{height: Dimensions.get('window').width /4, width:Dimensions.get('window').width /4, resizeMode:'contain', marginBottom:15, marginTop:15}}/>


        if(DATA.length !==  0){
            if(DATA.status === 'Appointment Changed'){
                BGCOLOR = '#FCE5CD'

                TOPTEXT = 
                <View>
                    <Text style={{fontFamily:'ProximaNova-Bold', fontSize:18, marginBottom:15}}>Date or Time has been changed! </Text>
                    <Text style={{fontSize:11}}>Please check on the changed date or time given by the workshop. If you agree with the changes, tap on Accept or Decline if you do not agree with the changes.</Text>
                </View>

                BUTTONS = 
                <View style={{flexDirection:'row', alignItems:'center', marginTop:15}}>
                    <TouchableWithoutFeedback onPress={this.acceptAppt}>
                    <View style={{justifyContent:'center', alignItems:'center', flex:3,marginRight:10,}}>
                        <View style={styles.outerCircle}>
                            <MaterialCommunityIcons name='thumb-up' size={15} color='#fff' />
                        </View>
                        <Text style={{marginTop:10, fontFamily:'ProximaNova-Bold',fontSize:11,  textAlign:'center'}}>Accept</Text>
                    </View>
                    </TouchableWithoutFeedback>
                    
                    <Text style={{fontFamily:'ProximaNova-Bold'}}> OR </Text>
                    
                    <TouchableWithoutFeedback onPress={() => this.setState({modalCancel:true})} >
                    <View style={{justifyContent:'center', alignItems:'center', flex:3,marginRight:10, }}>
                        <View style={styles.outerCircle}>
                            <MaterialCommunityIcons name='thumb-down' size={15} color='#fff' />
                        </View>
                        <Text style={{ marginTop:10, fontSize:11,fontFamily:'ProximaNova-Bold',  textAlign:'center'}}>Decline</Text>
                    </View>
                    </TouchableWithoutFeedback>
                </View>    
            }

            if(DATA.status === 'Appointment Declined'){
                BGCOLOR = '#FCE5CD'
                TOPTEXT = 
                <View>
                    <Text style={{fontFamily:'ProximaNova-Bold', fontSize:18, marginBottom:15}}>We are sorry!</Text>
                    <Text style={{fontSize:11}}>The workshop would be unavailable for the <Text style={{fontFamily:'ProximaNova-Bold'}}>reason : {DATA.decline_reason} </Text> </Text>
                    <Text style={{fontSize:11, marginTop:10}}>If you still want to service your vehicle at this workshop, please try creating another appointment with them.</Text>
                </View>

                BUTTONS  =
                <View style={{flexDirection:'row', alignItems:'center', marginTop:15}}>
                    <TouchableWithoutFeedback onPress={() => this.cancelAppt('clear')}>
                    <View style={{justifyContent:'center', alignItems:'center', flex:3,marginRight:10,}}>
                        <View style={styles.outerCircle}>
                            <MaterialCommunityIcons name='check-bold' size={15} color='#fff' />
                        </View>
                        <Text style={{marginTop:10, fontFamily:'ProximaNova-Bold',fontSize:11,  textAlign:'center'}}>I understand</Text>
                    </View>
                    </TouchableWithoutFeedback>
                    
                    <Text style={{fontFamily:'ProximaNova-Bold'}}> OR </Text>
                    
                    <TouchableWithoutFeedback onPress={() => this.cancelAppt('new')} >
                    <View style={{justifyContent:'center', alignItems:'center', flex:3,marginRight:10, }}>
                        <View style={styles.outerCircle}>
                            <MaterialCommunityIcons name='calendar-month-outline' size={15} color='#fff' />
                        </View>
                        <Text style={{ marginTop:10, fontSize:11,fontFamily:'ProximaNova-Bold',  textAlign:'center'}}>Book new appointment</Text>
                    </View>
                    </TouchableWithoutFeedback>
                </View>    
            }

            if(DATA.status === 'Appointment Accepted' || DATA.status === 'Appointment' ){
                BGCOLOR = '#d4f3fff0'
                TOPTEXT = 
                <View>
                    <Text style={{fontFamily:'ProximaNova-Bold', fontSize:18, marginBottom:15}}>Don't forget your appointment!</Text>
                    <Text style={{fontSize:14}}>Your appointment has been made. Please ensure that you arrive on time for the service provider to perform a preliminary inspection of your vehicle.</Text>
                </View>

                BUTTONS = 
                <View style={{flexDirection:'row', alignItems:'center', marginTop:20}}>
                    <TouchableWithoutFeedback onPress={this.getPermission}>
                    <View style={{justifyContent:'center', alignItems:'center', flex:3,marginRight:10,}}>
                        <View style={styles.outerCircle}>
                            <MaterialCommunityIcons name='alarm' size={15} color='#fff' />
                        </View>
                        <Text style={{marginTop:10, fontFamily:'ProximaNova-Bold',fontSize:13,  textAlign:'center'}}>Set Reminder </Text>
                    </View>
                    </TouchableWithoutFeedback>
                    
                    <Text style={{fontFamily:'ProximaNova-Bold'}}> OR </Text>
                    
                    <TouchableWithoutFeedback onPress={() => this.scan(DATA)} >
                    <View style={{justifyContent:'center', alignItems:'center', flex:3,marginRight:10, }}>
                        <View style={styles.outerCircle}>
                            <MaterialCommunityIcons name='qrcode-scan' size={15} color='#fff' />
                        </View>
                        <Text style={{ marginTop:10, fontSize:13,fontFamily:'ProximaNova-Bold',  textAlign:'center'}}>Scan SQR</Text>
                    </View>
                    </TouchableWithoutFeedback>
                </View>
            }
            disp = 
            <View style={{...styles.stats, backgroundColor:BGCOLOR}}>

                {TOPTEXT}
            
                {BUTTONS}
            </View>
        }
        return disp
    }
    cancelBottom = () => {
        let disp
        let DATA = this.state.requestData
        let OWNERID = this.state.ownerID
        let FUID = this.props.mobx_auth.FUID

        if(DATA.length !==  0){
            
            if(DATA.status === 'Appointment' || DATA.status === 'Appointment Accepted'){
                if(DATA.customer_FUID === FUID || (DATA.customer_FUID !== OWNERID && FUID === OWNERID ) )
                disp = 
                <TouchableWithoutFeedback onPress={() => this.setState({modalCancel:true})}>
                    <View style={{marginBottom:40, marginTop:20}}>
                        <Text style={{fontFamily:'ProximaNova-Bold',fontSize:16, color:'red', textAlign:'center'}}>Wait, please cancel my appointment</Text>
                    </View>
                </TouchableWithoutFeedback>
            } 
        }

        return disp
    }


    render(){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Appointment Details' onPress={() => this.props.navigation.popToTop()} />
                    
                <ScrollView showsVerticalScrollIndicator={false}>

                    {this.statusBox()}
                    {this.renderWS()}

                    <Text style={{fontFamily:'ProximaNova-Bold', fontSize:17, marginLeft:20, marginTop:10}}>Appointment Details</Text>
                    <View style={styles.topBox}> 
                        {this.renderTime()}
                    </View>

                    <MiddleModal modalName ={this.state.modalCancel}
                        children = {
                            <View >
                                <Text style={{ color:'black', fontFamily: "ProximaNova-Bold", fontSize:30, marginTop:15}}>Heads Up!</Text>
                                <Text style={{marginTop:20, color:'#080880', fontFamily: "ProximaNova-Bold", }}>You are about to cancel your appointment. You will relinquish all data regarding this appointment. 
                                </Text>

                                <Text style={{fontFamily:'ProximaNova-Bold', marginTop:15, color:'red',marginBottom:30}}>Are you sure?</Text>
                                <View>
                                    <TouchableOpacity onPress={() => this.setState({modalCancel:false})} style={{backgroundColor:'#080880', borderRadius:6, padding:15, marginTop:45, paddingLeft:20, paddingRight:20, marginBottom:15, alignItems:'center'}}>
                                    <Text style={{color:'white', fontFamily: "ProximaNova-Bold"}}>Go Back</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.cancelAppt('clear')} style={{ padding:15, paddingLeft:20, paddingRight:20, alignItems:'center'}}>
                                    <Text style={{color:'red', fontFamily: "ProximaNova-Bold"}}>Yes, I want to cancel my appointment</Text>
                                    </TouchableOpacity>
                                </View>


                            </View>
                        }
                    />

                    {this.cancelBottom()}

                </ScrollView>
                    
                </View>
            </SafeAreaView>
        )
    }
}

WorkshopAppointment = inject('mobx_auth','mobx_retail', 'mobx_carkey', 'mobx_config')(observer(WorkshopAppointment))
export default WorkshopAppointment

const styles = StyleSheet.create({
    innerModal: {     
        borderTopLeftRadius: 10,
        borderTopRightRadius:10,
        width:Dimensions.get('window').width,
        position:'absolute',
        bottom:0,
        backgroundColor: '#fff',
            ...Platform.select({
                ios: {
                    shadowColor: '#000',
                    shadowRadius: 2,
                    shadowOffset: { width: 0, height: 3 },
                    shadowOpacity: 0.6,
                },
                android: {
                    elevation: 2
                },
            }),
    },
    stats:{
        padding:15, margin:15, borderRadius:15, marginBottom:0,
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 0.6,
            },
            android: {
              elevation: 3
            },
          }),
    },
    outerLayer: {
        backgroundColor:'#fff',
        borderWidth:0.5, 
        borderColor:'#080880',
        borderRadius:10,
        padding:10,
        paddingTop:8, 
        paddingBottom:8,
        marginRight:8,
        flexDirection:'row',
        // width:36,
        // height:36,
        alignItems:'center',
        justifyContent:'center',
    },
    topBox: {
        backgroundColor:'white',
        borderRadius : 15,
        // borderBottomRightRadius:20,
        margin:15,
        // padding:20,
        // paddingTop:50,
        marginBottom:20,
       
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 0.6,
            },
            android: {
              elevation: 2
            },
          }),
    },
    box:{
        borderRadius: 15, 
        margin:15,
        padding:15,
        backgroundColor:'white',
        flexDirection:'row',
        alignItems:'center',
        flexShrink:1,
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 0.6,
            },
            android: {
              elevation: 2
            },
          }),
    },
    outerCircle:{backgroundColor:'#080880', width:40, height:40, borderRadius:20, alignItems:'center', justifyContent:'center',      },
    noouterCircle:{borderColor:'#080880', backgroundColor:'white', borderWidth:0.5, width:40, height:40, borderRadius:20, alignItems:'center', justifyContent:'center'},
    boldText:{fontFamily:'ProximaNova-Bold'},


})