import React, {Component} from 'react';
import {StyleSheet, Modal, Platform,TextInput,Text,Alert, TouchableWithoutFeedback, View, SafeAreaView, Image, KeyboardAvoidingView, RefreshControl,
  TouchableOpacity,ScrollView, Button,Dimensions,ActivityIndicator,Linking} from 'react-native';
import firebase from '@react-native-firebase/app';
import { inject, observer, Provider } from 'mobx-react';
import Icon from "react-native-vector-icons/FontAwesome";
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { cloud_serv_user_sandbox, cloud_serv_user_production  } from "../util/FirebaseCloudFunctions.js"
import AsyncStorage from '@react-native-async-storage/async-storage';
import { getStates, getCities } from "malaysia-postcodes";

import logo from '../assets/DL-logo.png';
import ladydriver from '../assets/driver.png';
import calendarlady from '../assets/appointment.png';


class Direct_lending extends React.Component{
    constructor(){
        super()
        this.state = {
            phone: '',
            city: '',
            states: '',
            tokenU:'',
            secretU:'',
            workshop_refer: false,
            workshop_name: '',
            workshop_phone: '',
            get_updated: 'Yes',
            submitted: false,
            modalStates: false,
            modalCity: false,
            isLoading:false
        }
    }

    componentDidMount(){
        this.checkUser()
        this.getToken()
    }

    checkUser(){
        let FUID = this.props.mobx_auth.FUID
        let that = this

        firebase.database().ref(`direct_lending/${FUID}`).once('value', (snp) => {
            if(snp.exists()){
                that.setState({submitted :true})
            }
        })

        firebase.database().ref(`users/${FUID}/`).once('value', (snap) => {
            if(snap.exists()){
                let PHONE = snap.val().Phone 
                if(PHONE[0] === '+'){
                    PHONE = PHONE.substring(2)
                    
                }
                that.setState({phone: PHONE})
            }
        })
    }

    getToken = async() => {
        let tokenU =  await AsyncStorage.getItem('CSRFTokenU')
        let secretU =  await AsyncStorage.getItem('CSRFSecretU')

        this.setState({tokenU : tokenU, secretU : secretU})

    }

    checkfirst = () => {
        let FUID = this.props.mobx_auth.FUID
        let PHONE = this.state.phone
        let REFER = this.state.workshop_refer === true ? 'Yes' : 'No'
        let GET_UPDATE =  this.state.get_updated
        let CITY =  this.state.city
        let STATES = this.state.states
        let KEY = this.props.mobx_auth.FUID 
        let WSNAME = this.state.workshop_name 
        let WSPHONE = this.state.workshop_phone

        if(PHONE === '' || STATES === '' || CITY === ''){
            Alert.alert('Sorry', 'Please fill in the details')
            return 
        }
        if(REFER === true && (WSNAME === '' || WSPHONE === '') ){
            Alert.alert('Sorry', 'Please fill in the details')
            return
        }

        if(this.state.submitted === true){
            Alert.alert(
                'You have submitted this form before',
                'Do you want to resubmit and overwrite the previous submission? ',
                [
                  {text: 'No'},
      
                  {text: 'Yes', onPress: this.runCF},
                ]
              );
        } else {
            this.runCF()
        }

      
    }


    runCF = () => {
        firebase.analytics().logEvent('click_interest', { name: 'direct_lending_interest'})
        this.setState({isLoading:true})
        let URL 
        if (this.props.mobx_config.config === 0) {URL = cloud_serv_user_sandbox.directLending}
        if (this.props.mobx_config.config === 1) {URL = cloud_serv_user_production.directLending}

        let PHONE = this.state.phone
        let REFER = this.state.workshop_refer === true ? 'Yes' : 'No'
        let GET_UPDATE =  this.state.get_updated
        let CITY =  this.state.city
        let STATES = this.state.states
        let KEY = this.props.mobx_auth.FUID 
        let WSNAME = this.state.workshop_name 
        let WSPHONE = this.state.workshop_phone

        let userkey = '?userkey=' + KEY;
        let phone = '&phone=' + PHONE;
        let get_updated = '&get_updated=' + GET_UPDATE
        let workshop_refer = '&workshop_refer=' + REFER
        let workshop_name = '&workshop_name=' + WSNAME
        let workshop_phone = '&workshop_phone=' + WSPHONE
        let city = '&city=' + CITY 
        let states = '&states=' + STATES
        

        let finalURL = URL + userkey + phone  + get_updated + workshop_refer + workshop_name + workshop_phone + city + states

        fetch(finalURL, {
            credentials:true, 
            headers: {
              csrftoken: this.state.tokenU,
              csrfsecret: this.state.secretU
            }
        }).then(() => {
            this.setState({phone:'', city:'', states:'',workshop_refer:false, workshop_name:'', workshop_phone:'', isLoading:false})
        }).then(() => {
            // console.warn('yeh?');
            Alert.alert('Thank you!', 'We will inform you through messages for any new infos.')
            this.props.navigation.popToTop();
          }).catch((error) => { 
            Alert.alert('Sorry', 'An error occured. Please contact SERV Team if the issue still persists');  
            })
    }


    openFAQ = () => {
        firebase.analytics().logEvent('DL_faq', { name: 'direct_lending_faq'})
        let LINK = 'https://www.directlending.com.my/serv-faq'
        this.props.navigation.navigate('Webview', {link : LINK})
    }

    goback = () => {
        firebase.analytics().logEvent('DL_backbutton', { name: 'direct_lending_topBack'})
        this.props.navigation.goBack()
    }


    
    

    render(){
        let STATES = getStates();
        let content_city = []
        let content_states = []

        for(var i = 0; i < STATES.length; i++){
            let value = STATES[i];
            content_states.push(
              <View >
                <TouchableOpacity
                  style={{padding:10, borderBottomColor:'#ccc', borderBottomWidth:0.5}}
                  onPress={()=> this.setState({states:value,modalStates:false})} >
                  <Text style={{fontSize:16}}>{STATES[i]}</Text>
                </TouchableOpacity>
              </View>
            )
          }

        
        if(this.state.states !== ''){
            let CITY = getCities(this.state.states);

            for(var i = 0; i < CITY.length; i++){
            let value = CITY[i];
            content_city.push(
                <View >
                <TouchableOpacity
                    style={{padding:10, borderBottomColor:'#ccc', borderBottomWidth:0.5}}
                    onPress={()=> this.setState({city:value,modalCity:false})} >
                    <Text style={{fontSize:16}}>{CITY[i]}</Text>
                </TouchableOpacity>
                </View>
            )}
        }


        let button 

        if(this.state.isLoading === false){
            button = 
            <TouchableWithoutFeedback onPress={this.checkfirst}>
                <View style={styles.contButton} >
                    <Text style={{fontFamily:'ProximaNova-Bold', color:'white', fontSize:16}} >Stay Informed</Text>
                </View>
            </TouchableWithoutFeedback>
        } else {
            button = 
                <View style={styles.contButton} >
                    <Text style={{fontFamily:'ProximaNova-Bold', color:'white', fontSize:16}} >Submitting ... </Text>
                </View>
        }
          

        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}} >
                <View style={{backgroundColor:'#fafdff', flex:1}}>
                
                <TouchableWithoutFeedback onPress={this.goback}>
                    <View style={styles.top}>
                        <Icon name="chevron-left" size={25} color='white' style={{marginRight:20, marginLeft:10}}/>
                    <Text style={{color:'white', fontFamily: "ProximaNova-Bold", fontSize:25 }}>SERV x Direct Lending</Text>
                    <Text style={{fontSize:11,position:'absolute', bottom:10, right:15, color:'white'}}>Coming soon</Text>
                    </View>
                </TouchableWithoutFeedback>

                <Modal
                    transparent={true}
                    animationType="slide"
                    visible={this.state.modalStates}
                    onRequestClose={() => {console.log('close modal')}}>
                    <SafeAreaView style={styles.modalBackground}>
                        <View style={styles.modal}>
                        <Text style={{fontSize:20, fontFamily: "ProximaNova-Bold"}}>Select State</Text>
                        <ScrollView showsVerticalScrollIndicator={false} style={{ width:'100%',}}>
                            {content_states}
                        </ScrollView>
                        <TouchableOpacity style={styles.closeButton} onPress={()=> this.setState({modalStates:false})}>
                            <Text style={{color:'white', fontSize:20}}> Close </Text>
                        </TouchableOpacity>
                        </View>
                    </SafeAreaView>
                    </Modal>

                <Modal
                    transparent={true}
                    animationType="slide"
                    visible={this.state.modalCity}
                    onRequestClose={() => {console.log('close modal')}}>
                    <SafeAreaView style={styles.modalBackground}>
                        <View style={styles.modal}>
                        <Text style={{fontSize:20, fontFamily: "ProximaNova-Bold"}}>Select City</Text>
                        <ScrollView showsVerticalScrollIndicator={false} style={{ width:'100%',}}>
                            {content_city}
                        </ScrollView>
                        <TouchableOpacity style={styles.closeButton} onPress={()=> this.setState({modalCity:false})}>
                            <Text style={{color:'white', fontSize:20}}> Close </Text>
                        </TouchableOpacity>
                        </View>
                    </SafeAreaView>
                </Modal>

                <KeyboardAvoidingView
                    behavior={(Platform.OS === 'ios') ? "padding" : null} enabled style={{flex:1}}
                    >
                    <ScrollView>

                    <View style={{margin:15}}>
                        <View style={{...styles.box, padding:0,}} >
                            <View style={{backgroundColor:'#d9ebf2',  flexDirection:'row', borderRadius:10}}>
                                <Image source={ladydriver} style={{flex:2, height: Dimensions.get('window').width /2, width:Dimensions.get('window').width /2 - 40, resizeMode:'contain',marginRight:15}}/>
                                <View style={{flex:2, justifyContent:'center', marginRight:15}}>
                                    <Text style={{fontFamily:'ProximaNova-Bold', color:'#080880',fontSize:16, marginBottom:10}} >Drive SAFELY with a little help</Text>
                                    <Text style={{fontSize:12}} >Get access to fast, flexible and affordable financing for all your vehicle needs </Text>
                                </View>
                            </View>
                            <View style={{ flexDirection:'row', alignItems:'center', justifyContent:'center', padding:10}} >
                                <Text style={{fontSize:12}} >In collaboration with: </Text>
                                <Image source={logo} style={{marginLeft:15, height:40,resizeMode:'contain', width: Dimensions.get('window').width / 3 }} />
                            </View>
                        </View>

                        <View style={styles.box} >
                            <Text style={{fontFamily:'ProximaNova-Bold',color:'#080880', fontSize:16,textAlign:'center', marginBottom: 15}} >It's quick and easy!</Text>
                            <View style={styles.numBox}>
                                <View style={styles.numcircle}>
                                    <Text style={styles.num}>1</Text>
                                </View>
                                <Text>Get prequalified</Text>
                            </View>
                            <View style={styles.numBox}>
                                <View style={styles.numcircle}>
                                    <Text style={styles.num}>2</Text>
                                </View>
                                <Text>Get vehicle fixed</Text>
                            </View>
                            <View style={styles.numBox}>
                                <View style={styles.numcircle}>
                                    <Text style={styles.num}>3</Text>
                                </View>
                                <Text>E-sign financing documents</Text>
                            </View>
                            <View style={styles.numBox}>
                                <View style={styles.numcircle}>
                                    <Text style={styles.num}>4</Text>
                                </View>
                                <Text>Auto service bill paid</Text>
                            </View>

                            <Text style={{marginTop:10, fontFamily:'ProximaNova-Bold',color:'#080880', fontSize:14}}> Exclusively for SERV users</Text>
                        </View>
                        <View style={styles.box} >
                            <Text style={{fontFamily:'ProximaNova-Bold', color:'#080880',fontSize:16,textAlign:'center',  marginBottom: 15}}>Get more time to pay</Text>
                            <View style={{flexDirection:'row'}}>
                            <View style={{flex:2,justifyContent:'center',marginRight:15}} >
                                <Text>Select the <Text style={{fontFamily:'ProximaNova-Bold'}}>payment terms</Text> that suit you best </Text>
                                <View style={{justifyContent:'center', alignItems:'center', marginTop:10}} >
                                    <Text style={{fontFamily:'ProximaNova-Bold', fontSize:16, color:'#080880'}}  >3 months</Text>
                                    <Text style={{fontSize:12, marginTop:5, marginBottom:5}} >or</Text>
                                    <Text style={{fontFamily:'ProximaNova-Bold', fontSize:16, color:'#080880'}} >6 months</Text>
                                </View>
                            </View>
                            <Image source={calendarlady} style={{flex:2, height: Dimensions.get('window').width /2, width:Dimensions.get('window').width /2 - 40, resizeMode:'contain',}}/>
                            </View>
                        </View>
                        <View style={styles.box} >
                            <Text style={{fontFamily:'ProximaNova-Bold',color:'#080880', fontSize:16,textAlign:'center',  marginBottom: 15}}>Fair pricing</Text>
                            <View style={{flexDirection:'row', marginBottom:20, marginTop:15, justifyContent:'center'}}>
                                <Text  >Profit Rate: </Text>
                                <View style={{flexDirection:'row'}}>
                                    <Text style={{fontFamily:'ProximaNova-Bold', color:'#080880', fontSize:35}} >0.8 - 1.5</Text>
                                    <Text style={{fontFamily:'ProximaNova-Bold',alignSelf:'flex-end', fontSize:20}}> % </Text>
                                </View>
                                <Text style={{alignSelf:'flex-end'}} >per month</Text>
                            </View>
                            
                            <View style={{flexDirection:'row', alignItems:'center',  marginBottom:15, marginRight:10}}>
                                <Ionicons name='thumbs-up-outline' size={20} color='#080880'/>
                            <Text style={{marginLeft:15, }} >Pricing based on credit profile and repayment history</Text>
                            </View>
                        
                            <View style={{flexDirection:'row', alignItems:'center',  marginBottom:15}}>
                                <Icon name='moon-o' size={20} style={{marginRight:15}} color='#080880' />
                                <Text><Text style={{fontFamily:'ProximaNova-Bold'}}>Syariah</Text> compliant financing </Text>
                            </View>
                            <View style={{ marginTop:10, flexDirection:'row', flexWrap:'wrap', flexShrink:1}} >
                                <Text style={{fontFamily:'ProximaNova-Bold'}}>Want to know more?</Text>

                                <View style={{flexDirection:'row', alignItems:'center',flexWrap:'wrap', flexShrink:1}}>
                                    <Text>Find out more on our  </Text>
                                    <Text style={{fontFamily:'ProximaNova-Bold'}} onPress={this.openFAQ} >FAQs</Text>
                     
                                    <Text> and feel free to </Text>
                                    <TouchableOpacity onPress={() => Linking.openURL('mailto:support@directlending.com.my')}>
                                        <Text style={{fontFamily:'ProximaNova-Bold'}} >contact us</Text>
                                    </TouchableOpacity>
                                   
                                </View>
                                
                            </View>
                        </View>

                       

                        <View style={styles.box} >
                            <Text style={{fontFamily:'ProximaNova-Bold',color:'#080880', textAlign:'center', fontSize:16, marginBottom: 15 }}>Stay updated & Get Early Bird Priviliges</Text>
                           <View style={{marginTop:25}}>
                           <View style={{marginBottom: 15, }} >
                                <Text style={{fontSize:11, fontFamily:'ProximaNova-Bold'}}>CONTACT NUMBER</Text>
                                <View style={{borderBottomWidth:0.5,marginTop:10, borderRadius:6,  padding:(Platform.OS === 'ios') ? 10 : null,}}>
                                    <TextInput 
                                        value={this.state.phone} 
                                        onChangeText={(x) => this.setState({phone:x})}  
                                        style={{fontFamily:'ProximaNova-Regular'}}  
                                        keyboardType = 'numeric'
                                        placeholder='Your Contact Number' underlineColorAndroid='transparent' />
                                </View>
                            </View>
                            <View style={{marginBottom: 15, marginTop:10}} >
                                <Text style={{fontSize:11, fontFamily:'ProximaNova-Bold'}}>LOCATION</Text>
                                <View style={{flexDirection:'row',justifyContent:'space-between',  marginTop:10,width:Dimensions.get('window').width - 60}}>
                                    <TouchableWithoutFeedback onPress={() => this.setState({modalStates: !this.state.modalStates}) }>
                                    <View style={{borderBottomWidth:0.5,width:'45%',  borderRadius:6,flexDirection:'row',justifyContent:'space-between', alignItems:'center', padding:10}}>
                                        {this.state.states === '' ?  <Text style={{color:'#ccc'}} >State</Text> : <Text>{this.state.states}</Text>}
                                        <Icon name = 'angle-down' size={15} />
                                    </View>
                                    </TouchableWithoutFeedback>
                                    {this.state.states === '' ? 
                                    <TouchableWithoutFeedback onPress={() => Alert.alert('Sorry', 'Please select state first') }>
                                    <View style={{borderBottomWidth:0.5,width:'45%', borderRadius:6,flexDirection:'row',justifyContent:'space-between', alignItems:'center', padding:10}}>
                                        {this.state.city === '' ? <Text style={{color:'#ccc'}} >City</Text> : <Text >{this.state.city}</Text>}
                                        <Icon name = 'angle-down' size={15} />
                                    </View>
                                    </TouchableWithoutFeedback>
                                    : 
                                    <TouchableWithoutFeedback onPress={() => this.setState({modalCity: !this.state.modalCity}) }>
                                    <View style={{borderBottomWidth:0.5,width:'45%', borderRadius:6,flexDirection:'row',justifyContent:'space-between', alignItems:'center', padding:10}}>
                                        {this.state.city === '' ? <Text style={{color:'#ccc'}} >City</Text> : <Text >{this.state.city}</Text>}
                                        <Icon name = 'angle-down' size={15} />
                                    </View>
                                    </TouchableWithoutFeedback>
                                    }
                                </View>
                            </View>
                           </View>
                            <View style={{marginBottom: 15, flexDirection:'row', marginTop:10,alignItems:'center'}} >
                                <TouchableOpacity  onPress={() => this.setState({workshop_refer: !this.state.workshop_refer})}>
                                    {!this.state.workshop_refer ?<Icon2 name='checkbox-blank-circle-outline' size={20} style={{marginRight:2}}/> : <Icon2 name='checkbox-marked-circle-outline' size={20} /> }
                                </TouchableOpacity>
                                <Text style={{marginLeft:6, fontSize:12}} >Would you like to refer workshop to participate?</Text>
                            </View>

                            {this.state.workshop_refer === true ? 
                            <View style={{marginTop:10,}}>
                                <Text style={{fontSize:11, fontFamily:'ProximaNova-Bold'}}>WORKSHOP DETAILS </Text>
                                <View style={{marginBottom: 15, marginTop:10}} >
                                    <View style={{borderBottomWidth:0.5,borderRadius:6, padding:(Platform.OS === 'ios') ? 10 : null,}}>
                                        <TextInput value={this.state.workshop_name} onChangeText={(x) => this.setState({workshop_name:x})} style={{fontFamily:'ProximaNova-Regular'}} placeholder='Workshop Name'  underlineColorAndroid='transparent' />
                                    </View>
                                </View>
                                <View style={{marginBottom: 15}} >
                                    <View style={{borderBottomWidth:0.5,borderRadius:6, padding:(Platform.OS === 'ios') ? 10 : null,}}>
                                        <TextInput value={this.state.workshop_phone} onChangeText={(x) => this.setState({workshop_phone:x})}  keyboardType = 'numeric' style={{fontFamily:'ProximaNova-Regular'}}  placeholder='Workshop Contact Number (optional) ' underlineColorAndroid='transparent' />
                                    </View>
                                </View>

                            </View> : null }

                            {button}
                            
                        </View>
                    </View>
                    </ScrollView>

                    </KeyboardAvoidingView>
                </View>
            </SafeAreaView>
        )
    }
}

Direct_lending = inject('mobx_auth','mobx_config', 'mobx_carkey')(observer(Direct_lending))
export default Direct_lending

const styles = StyleSheet.create({
    top:{
        backgroundColor:'#080880',
        padding:20,
        paddingBottom:35,
        borderBottomLeftRadius:20,
        borderBottomRightRadius: 20,
        flexDirection:'row',
        alignItems:'center'
      },
      contButton: {
        backgroundColor:'#080880', 
        padding:14,
        paddingRight:20, 
        paddingLeft:20, 
        alignItems:'center', 
        justifyContent:'center', 
        borderRadius:10, 
        marginTop: 30
    },
    box : {
        marginBottom: 15, backgroundColor:'white', borderRadius:10, padding:15,
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 2},
              shadowOpacity: 0.4,
            },
            android: {
              elevation: 3
            },
          }),
    },
    modalBackground: {
        flex: 1,
        alignItems:'center',
        backgroundColor: 'rgba(0,0,0,0.7)',
    },
    modal: {
        padding:10,
        elevation: 20,
        // marginBottom:30,
        borderRadius: 10,
        width:'90%',
        // display: 'flex',
        alignItems: 'center',
        borderColor: '#ccc',
        borderWidth: 1,
        borderStyle: 'solid',
        backgroundColor: 'white',
        justifyContent: 'space-around'
    },
    closeButton: {
    backgroundColor: '#080880',
    borderRadius: 6,
    padding: 10,
    alignItems: 'center',
    width:'100%',
    marginTop:15
    },
    numBox:{flexDirection:'row', alignItems:'center', marginBottom:5},
    numcircle:{borderWidth:1.5, borderColor:'#080880', marginRight:10, height:20, width:20, borderRadius:10, alignItems:'center', justifyContent:'center'},
    num:{fontFamily:'ProximaNova-Bold', fontSize:11, }
    

})