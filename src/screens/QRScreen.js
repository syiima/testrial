import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text,Image, View,Alert,Dimensions, ActivityIndicator,TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/AntDesign"

import ImagePicker from 'react-native-image-picker';
import { RNCamera,} from 'react-native-camera';
import BarcodeMask from 'react-native-barcode-mask';
import { NavigationEvents, NavigationActions } from 'react-navigation';

import Header from '../components/Header.js'
import BottomModal from '../components/BottomModal.js'
import vehicles from '../assets/vehicles_placeholder.png'

class ScanQR extends React.Component{
    constructor(props) {
        super(props);
        this.camera = null;
        this.barcodeCodes = [];

        this.state = {
            camera: {
                type: RNCamera.Constants.Type.back,
                  flashMode: RNCamera.Constants.FlashMode.auto,
                  barcodeFinderVisible: true,
                // value: 'haha',
              },
            path: null,        
            scan_state : false,    
            modalVehicle: true,
            vehicledata: '',
            vehicles: false,
        };
    }

    componentDidMount(){
      this.fetchVehicles()
      this.statusListener()
    }

    componentWillUnmount() {
      let FUID = this.props.mobx_auth.FUID;
      firebase.database().ref(`plate_number`).off()
      firebase.database().ref('users/' + FUID + '/vehicles').off()
    }

    statusListener(){
      let that = this
      let FUID = this.props.mobx_auth.FUID;
      firebase.database().ref(`plate_number`).on('child_changed', (snp) => {
        if(snp.exists()){
          that.fetchVehicles()
        } 
      })
    }

    refresh = () => {
      this.fetchVehicles()
    }
  
    

    fetchVehicles(){
      let that = this;
      let FUID = this.props.mobx_auth.FUID;
      let ALLPLATE = []
      
      firebase.database().ref(`users/${FUID}/vehicles`).on('value', (snapshot) => {
        let KEY = [];
        if(snapshot.exists()){
          KEY = Object.keys(snapshot.val())
          let V_DATA = [];
          KEY.forEach((key_id) => {
            
              let PLATE  = key_id 
              let STATUS = snapshot.val()[key_id].status
              let OWNERSHIP = snapshot.val()[key_id].true_owner

              if(STATUS === 'approved'){
                firebase.database().ref(`plate_number/${PLATE}`).once('value', (snp) => {
                  if(snp.exists()){
                    let a = snp.val()
                    a.plate = PLATE
                    a.true_owner = OWNERSHIP
                    a.status = STATUS
  
                    if(a.book_walkin === undefined){
                      V_DATA.push(a);
                    } else {
                      if(a.book_walkin._book_id === ''){
                        V_DATA.push(a);
                      }
                    }
                    that.setState({vehicledata:V_DATA,vehicles:true})
                  }
                })      
                
                that.setState({vehicles:true})
              }
              
          })
        } else {
          // console.log("Firebase is empty");
          that.setState({vehicles:false,vehicledata:[]});
        }
      })
  
   
    }

    goHome = () => {
      const navigateAction = NavigationActions.navigate({
        routeName: 'HomeScreen',
        action: NavigationActions.navigate({ routeName: 'HomeScreen' }),
      });
      this.props.navigation.dispatch(navigateAction);
      
    }

    showVehicle = () => {
      let display 
      let DATA = this.state.vehicledata 

      if(DATA !== ''){
        display = DATA.map(item => (
          <TouchableOpacity
            onPress={() => {
              this.setState({modalVehicle:false})
              let that = this;
              let model =  this.props.mobx_carkey.setMODEL(item.model);
              let make =   this.props.mobx_carkey.setMAKE(item.make);
              let carcc =   this.props.mobx_carkey.setCARCC(item.carcc);
              let trans =   this.props.mobx_carkey.setTRANSMISSION(item.transmission);
              let year =   this.props.mobx_carkey.setYEAR(item.year);
              let plate =   this.props.mobx_carkey.setPLATE(item.plate);

              that.props.navigation.navigate("QRScan")
              
            }}
            style={{flexDirection:'row', alignItems:'center',borderBottomWidth:0.5,paddingBottom:10, borderBottomColor:'#ccc',  margin:15, marginBottom:0}}>
            <View style={{flexDirection:'row', alignItems:'center', flex:3}}>
              <View style={{height:40, width:40, marginRight:15}}>
                <Image source={vehicles} style={{height:40, width:40}}/>
              </View>
              <View>
                <Text>{item.make} {item.model}</Text>
                <Text>{item.carcc}L / {item.year}</Text>
              </View>
            </View>
            <View style={{flex:1, }}>
              <Text>{item.plate}</Text>
            </View>

          </TouchableOpacity>
        ))
      }
      return display
    }

    renderVehicle = () => {
      let display 
      let DATA = this.state.vehicledata
      let EXIST = this.state.vehicles

      if(EXIST === false){
        display = 
        <View style={styles.boxCenter}>          
           <Text style={{textAlign:'center', color:'#080880', fontFamily: "ProximaNova-Bold", fontSize:18}}>No vehicle</Text>
           <Text style={{textAlign:'center',marginTop:10}} >Please add at least one vehicle to use QR scan</Text>

           <TouchableOpacity style={{padding:15,marginTop:30, backgroundColor:'#080880', borderRadius:10}} onPress={this.goHome}>  
             <Text style={{textAlign:'center', color:'#fff', fontFamily: "ProximaNova-Bold", fontSize:14}} >Alright, got it!</Text>
           </TouchableOpacity>

           <Text style={{marginTop:15, fontSize:12, color:'red'}}>*At the moment, QR Code Scan is only for workshop drive in </Text>

        </View>
      } else {
        if(DATA.length !== 0){
          display = 
          <View style={styles.box}>          
                        
            <View style={{padding:15,borderBottomColor:'#080880', alignItems:'center',}}>
              <Text style={{textAlign:'center', color:'#080880', fontFamily: "ProximaNova-Bold", fontSize:18}}>Select Vehicle</Text>
              <Text style={{marginTop:10, fontSize:10, color:'red'}}>*At the moment, QR Code Scan is only for workshop drive in </Text>
            </View>
            <ScrollView style={{height:Dimensions.get('window').height / 2}}>
              {this.showVehicle()}
            </ScrollView> 
          </View>
        } else {
          display = 
          <View style={styles.boxCenter}>          
                        
            <Text style={{textAlign:'center', color:'#080880', fontFamily: "ProximaNova-Bold", fontSize:18}}>No vehicle</Text>
            <Text style={{textAlign:'center',marginTop:10}} >If your vehicle is not listed here, it is because you have already requested a service for it.</Text>
            <Text style={{textAlign:'center',marginTop:10}} >You can check your vehicles status by tapping on the <Text style={{fontFamily: "ProximaNova-Bold",}}> WORKSHOP</Text> icon</Text>

            <TouchableOpacity style={{padding:15,marginTop:30, backgroundColor:'#080880', borderRadius:10}} onPress={this.goHome}>  
              <Text style={{textAlign:'center', color:'#fff', fontFamily: "ProximaNova-Bold", fontSize:14}} >Alright, got it!</Text>
            </TouchableOpacity>

            <Text style={{marginTop:15, fontSize:12, color:'red'}}>*At the moment, QR Code Scan is only for workshop drive in </Text>

          </View>
        }

      }

      
        
       
      return display 
    }
  

    render(){
        let scan_qrcode;

        if(this.state.scan_state === false){
            scan_qrcode =
            <RNCamera
                ref={ref => {
                  this.camera = ref;
                }}
                captureAudio={false}
                barcodeFinderVisible={this.state.camera.barcodeFinderVisible}
                barcodeFinderWidth={280}
                barcodeFinderHeight={220}
                barcodeFinderBorderColor="white"
                barcodeFinderBorderWidth={2}
                defaultTouchToFocus
                flashMode={this.state.camera.flashMode}
                mirrorImage={false}
                // onBarCodeRead={this.onBarCodeRead.bind(this)}
                onFocusChanged={() => {}}
                onZoomChanged={() => {}}
                style={styles.preview}
                type={this.state.camera.type}
                >
                  <BarcodeMask edgeColor={'#080880'} showAnimatedLine={true}/>
            </RNCamera>
            
          }

        return(
          <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
            <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <NavigationEvents onDidFocus={() => this.refresh()} />

                <Header backButton = {false}  headerTitle = 'QR Code' />
                    
                    {/* {scan_qrcode} */}
                    
                    
                        
                    {this.renderVehicle()}
              
                  
                </View>
            </SafeAreaView>
        )
    }
}

ScanQR = inject('mobx_auth','mobx_retail', 'mobx_carkey', 'mobx_config')(observer(ScanQR))
export default ScanQR

const styles = StyleSheet.create({
    container: {
        flex: 1
      },
      preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
      },
      overlay: {
        position: 'absolute',
        padding: 16,
        right: 0,
        left: 0,
        alignItems: 'center'
      },
      topOverlay: {
        top: 20,
        left:15,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
      },
      bottomOverlay: {
        bottom: 0,
        margin: 15,
        backgroundColor: '#fcd736',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
      },
      enterBarcodeManualButton: {
        padding: 15,
        backgroundColor: 'white',
        borderRadius: 40
      },
      scanScreenMessage: {
        fontSize: 18,
        color: 'black',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center'
      },
      boxCenter:{
        alignSelf:'center',
        borderRadius:20,
        padding:20,
        width:Dimensions.get('window').width - 40,
        position:'absolute',
        top: Dimensions.get('window').width / 2,
        backgroundColor: '#fff',
            ...Platform.select({
                ios: {
                    shadowColor: '#000',
                    shadowRadius: 2,
                    shadowOffset: { width: 0, height: 3 },
                    shadowOpacity: 0.6,
                },
                android: {
                    elevation: 2
                },
            }),
      },
      box: {     
        alignSelf:'center',
        borderRadius:20,
        padding:20,
        width:Dimensions.get('window').width - 40,
        position:'absolute',
        top: Dimensions.get('window').width / 5,
        
        backgroundColor: '#fff',
            ...Platform.select({
                ios: {
                    shadowColor: '#000',
                    shadowRadius: 2,
                    shadowOffset: { width: 0, height: 3 },
                    shadowOpacity: 0.6,
                },
                android: {
                    elevation: 2
                },
            }),
    },
      
})