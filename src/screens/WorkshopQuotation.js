import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,Image, TouchableWithoutFeedback, TouchableOpacity,Linking, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/AntDesign"
import { NavigationActions, StackActions } from 'react-navigation';

import Header from '../components/Header.js'
import WorkshopInfo from "../components/WorkshopInfo.js"
import Conditional from "../components/WorkshopQuotationConditional.js"
import MiddleModal from "../components/MiddleModal.js"

class WorkshopQuotation extends React.Component{
    constructor(){
        super()
        this.state = {
            requestData :'',
            quote: '',
            logo: '',
            ws_data :'',
            status_quote: 'Pending',
            reason_cancel : '',
            display_view :'',
            clearing: false,
            modalCancel: false,
            status: '',
            subtotal:0,
            finaltotal: 0,
            tax:0

        }
    }
    
    componentDidMount(){
        this.fetchRequest()
        this.fetchWorkshopDets()
        this.statusListener()
    }

    fetchRequest = () => {
        let that =this
        let FUID = this.props.mobx_auth.FUID
        let BOOK_ID =  this.props.mobx_retail.R_WALKIN_BOOKID
        let WS_ID =   this.props.mobx_retail.R_ID
        let WSMAIN_ID =   this.props.mobx_retail.R_GROUPID
        let QUOTATION, X, STATUS

        firebase.database().ref(`request_walkin/${WSMAIN_ID}/${WS_ID}/${BOOK_ID}`).once('value').then((snapshot) => {
            if(snapshot.exists()){
                X = snapshot.val()
                QUOTATION = X.quotation
                STATUS = X.status
            } 

            that.setState({requestData : X, quote: QUOTATION, status: STATUS})
            this.calculations(QUOTATION)
        })
    }

    fetchWorkshopDets(){
        let that = this
        let WSDATA = []
        let BOOK_ID =  this.props.mobx_retail.R_WALKIN_BOOKID
        let RETAIL =   this.props.mobx_retail.R_ID
        let RETAIL_MAIN =   this.props.mobx_retail.R_GROUPID
        // console.warn('hellllooo', WSMAIN_ID, WS_ID);
        
        firebase.database().ref(`retail/${RETAIL_MAIN}/${RETAIL}/`).once('value').then((snapshot) => {
            if(snapshot.exists()){
                let a = snapshot.val()
                that.setState({ws_data: a})
            }
        })

        firebase.database().ref(`retail_main/${RETAIL_MAIN}/logo`).once('value').then((snapshot) => {
            if(snapshot.exists()){
                let b = snapshot.val()
                that.setState({logo: b})
            }
        })

    }

    statusListener(){
        let that = this;
        let CAR_KEY = this.props.mobx_carkey.CarKey;
        let PLATE = this.props.mobx_carkey.PLATE
        let UID = this.props.mobx_auth.FUID;
    
        firebase.database().ref(`plate_number/${PLATE}/book_walkin`).on('child_changed', async function(snapshot) {
          if(snapshot.exists()){
            if(snapshot.val() === "Pending"){
                // await Alert.alert("Your quotation for car " + PLATE + ' is out')
                that.gotopage('WorkshopQuotation')
                // that.props.navigation.navigate('NewMain')
                }
  
            
          } else {
            // console.warn("empty")
          }
    
        })
    }

    gotopage = (x) => {
        let RETAIL =   this.props.mobx_retail.R_ID
        let RETAIL_MAIN =   this.props.mobx_retail.R_GROUPID
        
        if(x === 'WorkshopQuotation'){
            let status = this.props.mobx_retail.setSTATUS('Pending');
            let wsID =   this.props.mobx_retail.setR_ID(RETAIL);
            let groupID =   this.props.mobx_retail.setR_GROUPID(RETAIL_MAIN);

            firebase.analytics().logScreenView({
                screen_name: 'Quotation',
                screen_class: 'Quotation',
              }) 


            const navigateAction = StackActions.replace({
                routeName: 'WorkshopQuotation',
                action: NavigationActions.navigate({ routeName: 'WorkshopQuotation' }),
              });
              this.props.navigation.dispatch(navigateAction);

        }
        if(x === 'New'){
            firebase.analytics().logScreenView({
                screen_name: 'WorkshopList',
                screen_class: 'WorkshopList',
              }) 
            const navigateAction = StackActions.replace({
                routeName: 'WorkshopListing',
                action: NavigationActions.navigate({ routeName: 'WorkshopListing' }),
              });
              this.props.navigation.dispatch(navigateAction);
        }
        if(x === 'Accepted'){
            let status = this.props.mobx_retail.setSTATUS('Accepted');
            firebase.analytics().logScreenView({
                screen_name: 'PerformService',
                screen_class: 'PerformService',
              }) 

            const navigateAction = StackActions.replace({
                routeName: 'WorkshopPerformService',
                action: NavigationActions.navigate({ routeName: 'WorkshopPerformService' }),
              });
              this.props.navigation.dispatch(navigateAction);
        }
    }


    //======== render ===== 
    renderItem = () => {
        let DATA = this.state.quote
        let STATUS = this.state.status
        let display

        if(DATA !== ''){
           display = 
               <Conditional item={DATA} status={STATUS} passrefresh={this.fetchRequest}/>
           
        } 
        return display

    }
    renderTime = () => {
        let item = this.state.requestData
        let STATUS =   this.props.mobx_retail.STATUS
        let display

        if(item !== '' && STATUS === 'Pending'){
            display = 
                <View style={{padding:20}}>
                    <View style={{flexDirection:'row'}}>
                        <Text style={{flex:1}}>Quotation No</Text>
                        <Text style={styles.boldText}>: </Text>
                        <Text style={{...styles.boldText, fontSize:17, flex:1.5}}>{item._requestID}</Text>
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <Text style={{flex:1}}>Date</Text>
                        <Text style={styles.boldText}>: </Text>
                        <Text style={{...styles.boldText, fontSize:17, flex:1.5}}>{item.customer_day} {item.customer_month} {item.customer_year}</Text>
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <Text style={{flex:1}}>Time</Text>
                        <Text style={styles.boldText}>: </Text>
                        <Text style={{...styles.boldText, fontSize:17, flex:1.5}}>{item.customer_time} </Text>
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <Text style={{flex:1}}>Vehicle Plate No</Text>
                        <Text style={styles.boldText}>: </Text>
                        <Text style={{...styles.boldText, fontSize:17, flex:1.5}}>{item.customer_carplate} </Text>
                    </View>
                </View>
      
        } 
        if(item !== '' && STATUS === 'Cancel' ||  this.state.status_quote === 'Cancel'){
            display = 
                <View style={{padding:20, flexDirection:'row', alignItems:'center', justifyContent:'space-between'}}>
                    <View >
                        <Text style={{...styles.boldText, fontSize:17, }}>{item.customer_carplate}</Text>
                        <Text style={{fontSize:12, }}>{item.customer_carmake} {item.customer_carmodel}</Text>
                        <Text style={{fontSize:12}}>Ref: {item._requestID} </Text>
                    </View>
                    <View >
                        <Text style={{color:'red', fontFamily:'ProximaNova-Bold'}}>Cancelled Request</Text>
                        <Text style={{color:'#080880', fontFamily:'ProximaNova-Bold', textAlign:'right'}}>{item.customer_day} {item.customer_month} {item.customer_year}</Text>
                        <Text style={{color:'#080880', fontFamily:'ProximaNova-Bold', textAlign:'right'}}>{item.customer_time}</Text>
                    </View>
                </View>
        }
        return display
    }


    calculations = (quote) => {
        let item = quote 
        
        if(item !== undefined){
            let X = item.masterItems
            let sum = 0
            for (let i = 0; i < X.length; i++) {
                let a = X[i]
    
                if(a.added === true || a.added === undefined){
                    sum += a.price
                }
                
                // console.warn('fokk', X[i], sum);

            }


            let TAX = item.tax / 100 * sum
            TAX = parseFloat(TAX.toFixed(1))

            let totPrice = sum + TAX - item.discount - item.deposit
            totPrice = totPrice.toFixed(2)

            console.warn('fokkk', totPrice, sum, TAX, item.discount, item.deposit);

            this.setState({finaltotal: totPrice, subtotal : sum, tax:TAX})
        }

    }

    showPrice = () => {
        let item = this.state.quote
        let display

        if(item !== '' ){

            display = 
                <View>  
                <View style={styles.boxPrice}>
                    <Text>Subtotal</Text>
                        <View style={{flexDirection:'row'}}>
                            <Text style={{...styles.boldText, fontSize:12}}>RM </Text>
                            <Text style={{...styles.boldText, fontSize:24}}>{this.state.subtotal.toFixed(2) }</Text>
                        </View>
                    </View>
                    <View style={styles.boxPrice}>
                    <Text>Tax @ {item.tax}% </Text>
                        <View style={{flexDirection:'row'}}>
                            <Text style={{...styles.boldText, fontSize:12}}>RM</Text>
                            <Text style={{...styles.boldText, fontSize:24}}>{this.state.tax.toFixed(2)}</Text>
                        </View>
                    </View>
                    <View style={styles.boxPrice}>
                    <Text>Discount</Text>
                        <View style={{flexDirection:'row'}}>
                            <Text style={{...styles.boldText, fontSize:12}}>RM </Text>
                            <Text style={{...styles.boldText, fontSize:24}}>{item.discount.toFixed(2)}</Text>
                        </View>
                    </View>
                    {item.deposit !== undefined ? 
                    <View style={styles.boxPrice}>
                    <Text>Deposit</Text>
                        <View style={{flexDirection:'row'}}>
                            <Text style={{...styles.boldText, fontSize:12}}>RM </Text>
                            <Text style={{...styles.boldText, fontSize:24}}>{item.deposit.toFixed(2)}</Text>
                        </View>
                    </View>
                    : null}

                    <View style={{flexDirection:'row', justifyContent:'space-between', padding:5,marginTop: 15, marginBottom:15}}>
                    <Text>Final Total</Text>
                        <View style={{flexDirection:'row'}}>
                            <Text style={{...styles.boldText, fontSize:12}}>RM </Text>
                            <Text style={{...styles.boldText, fontSize:24}}>{this.state.finaltotal}</Text>
                        </View>
                    </View>

                </View>
        }

        return display
    }

  
    reject = () => {
        let WKIN_DATA

        this.setState({status_quote: 'Cancel'})
        //1.tukar bokin
        let UID = this.props.mobx_auth.FUID
        let BOOKING_ID =  this.props.mobx_retail.R_WALKIN_BOOKID
        let WORKSHOP_ID =   this.props.mobx_retail.R_ID
        let WORKSHOP_MAIN =   this.props.mobx_retail.R_GROUPID
        let PLATE = this.props.mobx_carkey.PLATE

        let DATA = {
            reason: this.state.reason_cancel,
            timestamp: firebase.database.ServerValue.TIMESTAMP, 
            user_id: UID,
            vehicle_id: PLATE,
        }

        let REASON = {
            reason: this.state.reason_cancel,
            timestamp: firebase.database.ServerValue.TIMESTAMP, 
        }

        let CLEARDATA = {
            _book_id :'',
            _book_status: '',
            retail_id: '',
            retail_main: '',
            booking_by:'',
            day: '',
            month: '',
            year: '',
            time: '',
        } 


        firebase.database().ref(`request_walkin/${WORKSHOP_MAIN}/${WORKSHOP_ID}/${BOOKING_ID}/`)
        .update({status:'Cancel'})
        .then(() => {
            firebase.database().ref(`delete_walkin/${WORKSHOP_MAIN}/${WORKSHOP_ID}/${BOOKING_ID}/`).update(DATA)
            firebase.database().ref(`request_walkin/${WORKSHOP_MAIN}/${WORKSHOP_ID}/${BOOKING_ID}/cancel_walkin/`).update(REASON)

        }).then(() => {
            firebase.database().ref(`request_walkin/${WORKSHOP_MAIN}/${WORKSHOP_ID}/${BOOKING_ID}/`).once('value').then((snapshot) => {
                if(snapshot.exists()){
                    WKIN_DATA = snapshot.val()
                    WKIN_DATA.timestamp_delete = firebase.database.ServerValue.TIMESTAMP
                    firebase.database().ref(`history_walkin/${WORKSHOP_MAIN}/${WORKSHOP_ID}/${BOOKING_ID}/`).update(WKIN_DATA)
                    
                } 
            })
        }).then(() => {
            //2.remove from req_wal
            firebase.database().ref(`request_walkin/${WORKSHOP_MAIN}/${WORKSHOP_ID}/${BOOKING_ID}/`).remove()    
            this.setState({modalCancel: false})

        }).then(() => {
            //3.clear user booking
            firebase.database().ref(`plate_number/${PLATE}/book_walkin/`).update(CLEARDATA)
            
        }).then(() => {
            this.gotopage('New')
            Alert.alert('Your previous request has been deleted!', 'You can continue with a new service.')
        })
        
    }

    accept = () => {
        let UID = this.props.mobx_auth.FUID
        let BOOKING_ID =  this.props.mobx_retail.R_WALKIN_BOOKID
        let WORKSHOP_ID =   this.props.mobx_retail.R_ID
        let WORKSHOP_MAIN =   this.props.mobx_retail.R_GROUPID
        let PLATE = this.props.mobx_carkey.PLATE

        firebase.database().ref(`plate_number/${PLATE}/book_walkin/`).update({_book_status: 'Accepted'})

        firebase.database().ref(`request_walkin/${WORKSHOP_MAIN}/${WORKSHOP_ID}/${BOOKING_ID}/quotation/`).update({
            status:true,
            total_price: (this.state.finaltotal).toString()
        })

        firebase.database().ref(`request_walkin/${WORKSHOP_MAIN}/${WORKSHOP_ID}/${BOOKING_ID}/`).update({status:'Accepted'})

        this.gotopage('Accepted')
        Alert.alert('Thank you!', 'Our service provider will now proceed with the service.')
    }

    deleteRequest = () => {
        let that = this
        this.setState({clearing: true})
        //1.tukar bokin
        let BOOKING_ID =  this.props.mobx_retail.R_WALKIN_BOOKID
        let WORKSHOP_ID =   this.props.mobx_retail.R_ID
        let WORKSHOP_MAIN =   this.props.mobx_retail.R_GROUPID
        let FUID = this.props.mobx_auth.FUID
        let PLATE = this.props.mobx_carkey.PLATE
        
        let DATA = {
            _book_id :'',
            _book_status: '',
            retail_id: '',
            retail_main: '',
            booking_by:'',
            day: '',
            month: '',
            year: '',
            time: '',
        }        

        // //1.push to history
        let WKIN_DATA
       
        firebase.database().ref(`request_walkin/${WORKSHOP_MAIN}/${WORKSHOP_ID}/${BOOKING_ID}/`).once('value').then((snapshot) => {
            if(snapshot.exists()){
                WKIN_DATA = snapshot.val()
                WKIN_DATA.timestamp_delete = firebase.database.ServerValue.TIMESTAMP
                firebase.database().ref(`history_walkin/${WORKSHOP_MAIN}/${WORKSHOP_ID}/${BOOKING_ID}/`).update(WKIN_DATA)
                
            } 
        }).then(() => {
            //2.remove from req_wal
            firebase.database().ref(`request_walkin/${WORKSHOP_MAIN}/${WORKSHOP_ID}/${BOOKING_ID}/`).remove()                
        }).then(() => {
            //3.clear user booking
            firebase.database().ref(`plate_number/${PLATE}/book_walkin/`).update(DATA)
            
        }).then(() => {
            that.gotopage('New')
            Alert.alert('Your previous request has been deleted!', 'You can continue with a new service.')
        })
    }

    render(){
        let render_displayView;
        let STATUS = this.props.mobx_retail.STATUS

        if(STATUS === 'Pending'){
            render_displayView = 
            <View>
                <TouchableWithoutFeedback onPress={this.accept}>
                    <View style={styles.button}>
                        <Text style={{fontFamily:'ProximaNova-Bold', color:'white', textAlign:'center'}}>Accept Quotation</Text>
                    </View>
                    </TouchableWithoutFeedback>

                    <MiddleModal modalName= {this.state.modalCancel}
                        children= {
                            <View>
                                <Text style={{ color:'black', fontFamily: "ProximaNova-Bold", fontSize:30, marginTop:15}}>Heads Up!</Text>
                                <Text style={{marginTop:20, color:'#080880', fontFamily: "ProximaNova-Bold", }}>You are about to cancel your request. You will relinquish all data regarding this request. 
                                </Text>
                                <Text style={{fontFamily:'ProximaNova-Bold', marginTop:15, color:'red',marginBottom:30}}>Are you sure?</Text>
                                <View>
                                    
                                <TouchableOpacity onPress={() => this.setState({modalCancel:false})} style={{backgroundColor:'#080880', borderRadius:6, padding:15, marginTop:45, paddingLeft:20, paddingRight:20, marginBottom:15, alignItems:'center'}}>
                                    <Text style={{color:'white', fontFamily: "ProximaNova-Bold"}}>Go Back</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={this.reject} style={{ padding:15, paddingLeft:20, paddingRight:20, alignItems:'center'}}>
                                    <Text style={{color:'red', fontFamily: "ProximaNova-Bold"}}>Yes, I want to cancel my request</Text>
                                </TouchableOpacity>
                                </View>
                            </View>
                        }
                    />
                    

                    <TouchableWithoutFeedback onPress={() => this.setState({modalCancel: true})}>
                    <View style={{marginBottom:15, marginTop:15}}>
                        <Text style={{fontFamily:'ProximaNova-Bold', color:'red', textAlign:'center'}}>No, I want to cancel my request</Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        } 
        if(STATUS === 'Cancel' || this.state.display_view === 'reject') {
            render_displayView =
            <View style={{margin:15}}>
                <Text style={{color:'red', fontFamily:'ProximaNova-Bold', textAlign:'center'}}>You've cancelled your request! Hm-maybe talk to the workshop guy to delete it..?</Text>
            </View> 
        }

        let button_delete 
        if(this.state.clearing === false){
            button_delete = 
            <TouchableOpacity style={{...styles.button, position:'absolute', bottom: 20, marginBottom: 0, alignSelf:'center', width:Dimensions.get('window').width - 30}} onPress={()=>{
                Alert.alert(
                    "Are you sure you want to continue with a new service?",
                    'Your current booking details will be deleted',
                    [
                    {text: 'Confirm', onPress:()=> this.deleteRequest()},
                    {text: 'Go back', },
                    ]
                )
                }}>
                    <Text style={{color:'white', fontFamily:'ProximaNova-Bold', textAlign:'center',fontSize:16,}}>Continue with a new service</Text>
                </TouchableOpacity>
        } else {
            button_delete = 
            <View style={{...styles.button, position:'absolute', bottom: 20, marginBottom: 0, alignSelf:'center', width:Dimensions.get('window').width - 30}} >
                <Text style={{color:'white', fontFamily:'ProximaNova-Bold', textAlign:'center',fontSize:16,}}>Deleting... </Text>
            </View>
        }

        let ws_info 
        if(this.state.ws_data === ''){
            ws_info = 
            <View style={{backgroundColor:'white', borderRadius:15, marginTop:20, padding:15}}>
                <View style={{flexDirection:'row', marginBottom:20}}>
                    <View style={{heihgt:60, width: 60, backgroundColor:'#fafdff', borderRadius:30,marginRight:15}}/>
                    <View>
                        <View style={{backgroundColor:'#fafdff', height:16, marginBottom:10}}/>
                        <View style={{backgroundColor:'#fafdff', height:16, marginBottom:10}}/>
                    </View>
                </View>

            </View>
        } else {
            ws_info = <WorkshopInfo navigation={this.props.navigation} data= {this.state.ws_data} logo={this.state.logo} />
        }

        let content_view
        if(STATUS === 'Pending'){
            content_view = 
            <View style={{flex:1}}>
                <Header backButton = {true}  headerTitle = 'Quotation' onPress={() =>  this.props.navigation.popToTop()} />

                    <ScrollView  showsVerticalScrollIndicator={false}>
                    <View style={styles.topBox}>
                        {ws_info}
                        {this.renderTime()}
                    </View>

                    <View>
                        <Text style={{fontFamily:'ProximaNova-Bold', fontSize:17, marginLeft:20, marginTop:10}}>Items</Text>
                        <View style={{...styles.stats, backgroundColor:'white', padding:20}}>
                            {this.renderItem()}
                        </View>
                    </View>

                    <View style={{...styles.stats, backgroundColor:'#FCE5CD', marginBottom:20, marginTop:30}}>
                        {this.showPrice()}

                        <Text style={{fontSize:12, padding:5, fontFamily:'ProximaNova-Bold'}}>Disclaimer</Text>
                        <Text style={{fontSize:12, padding:5}}>a) The service will only begin once you accept the quotation. </Text>
                        <Text style={{fontSize:12, padding:5}}>b) Item(s) in the quotation can be removed or re-added based on your needs. Please consult with your mechanic to understand more about the quoted item(s).</Text>
                        <Text style={{fontSize:12, padding:5}}>c) The workshop shall provide a new quotation or may refuse to conduct the service altogether if the item(s) removed may affect the service agreed in the edited quote. </Text>
                        <Text style={{fontSize:12, padding:5}}>d) Please consider renegotiating verbally with your workshop if you are not satisfied with the item(s) and pricing of the quotation before clicking the “Cancel Request” link below. </Text>
                        
                        <View style={{marginTop:20, }}>
                            <Text style={{fontSize:12, padding:5, fontFamily:'ProximaNova-Bold', textAlign:'center', marginBottom:10}} onPress={()=> Linking.openURL('https://serv.my/terms-conditions/')}>Terms & Condition</Text>
                            {render_displayView}
                        </View>

                        
                    </View>

                    </ScrollView>

            </View>
        } 

        if(STATUS === 'Cancel' || this.state.display_view === 'reject') {
            content_view = 
            <View style={{flex:1}}>
                <Header backButton = {true}  headerTitle = 'Continue' onPress={() =>  this.props.navigation.popToTop()} />
                <View style={{margin:20, alignItems:'center', justifyContent:'center'}}>
                    <Text style={{fontSize:24, fontFamily:'ProximaNova-Bold', color:'#080880', marginBottom:15, marginTop:15}}>Oops! Looks like you have some unfinished business</Text>

                    <Text style={{fontFamily:'ProximaNova-Bold', fontSize:12, marginBottom: 15}}>You've recently cancelled a request during a walk-in service and that request is still pending cancellation on your side.</Text>

                </View>

                <View style={{margin:20, marginBottom:0, flexDirection:'row', alignItems:'center'}}>
                    <Text style={{fontSize:12}}>Cancelled Request</Text>
                    <View style={styles.separatorLine}/>
                </View>

                <View style={{...styles.topBox, marginTop: 30}}>
                    {this.renderTime()}

                </View>

                {button_delete}

            </View>
        }



        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                
                    
                    {content_view}
                    
                </View>
            </SafeAreaView>
        )
    }
}

WorkshopQuotation = inject('mobx_auth','mobx_retail', 'mobx_carkey', 'mobx_config')(observer(WorkshopQuotation))
export default WorkshopQuotation

const styles = StyleSheet.create({
    boldText :{fontFamily: "ProximaNova-Bold",},
    boxPrice :{flexDirection:'row', justifyContent:'space-between', padding:5},
    topBox: {
        backgroundColor:'white',
        borderRadius : 15,
        // borderBottomRightRadius:20,
        margin:15,
        // padding:20,
        // paddingTop:50,
        marginBottom:20,
       
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 0.6,
            },
            android: {
              elevation: 2
            },
          }),
    },
    separatorLine: {
        flex: 1,
        borderWidth: StyleSheet.hairlineWidth,
        height: StyleSheet.hairlineWidth,
        borderColor: '#9B9FA4',
        marginLeft:15,
    },
    stats:{
        padding:15, margin:15, borderRadius:15, marginBottom:0,
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 0.4,
            },
            android: {
              elevation: 2
            },
          }),
    },
    button:{
        backgroundColor:'#080880', borderRadius:10, padding:15, marginBottom:15,
         ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 0.6,
            },
            android: {
              elevation: 2
            },
          }),
    },

})