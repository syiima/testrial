import React from 'react';
import { StyleSheet, SafeAreaView,Platform, Text, View,AppState,Alert,FlatList, RefreshControl, StatusBar,Dimensions, TouchableWithoutFeedback, TouchableOpacity, ScrollView, Image, Button, TextInput, Linking} from 'react-native';

import { inject, observer } from 'mobx-react';
import firebase from '@react-native-firebase/app';
import Icon from 'react-native-vector-icons/AntDesign';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { NavigationActions, StackActions } from 'react-navigation';

import servS from '../assets/servS_white.png'
import profile from '../assets/empty_profile.png'
import vehicles from '../assets/vehicles_placeholder.png'
import vehicleEmpty from '../assets/emptyCar.png'
import motorEmpty from '../assets/emptyMotor.png'
import addCar from '../assets/addCar.png'
import addButton from '../assets/add_button.png'
// import service1 from '../assets/icons/drivein.png'
// import service2 from '../assets/icons/sos.png'
// import service3 from '../assets/icons/insurance.png'
import service1 from '../assets/icons/workshop2.png'
import service2 from '../assets/icons/sos2.png'
import service3 from '../assets/icons/insurance2.png'
import qr from '../assets/icons/qr2.png'

import more from '../assets/icons/more.png'
import allianz from '../assets/logo/allianzLogo.png'
import sureplify from '../assets/logo/sureman.png'
import policystreet from '../assets/logo/policystreet.png'

import MiddleModal from '../components/MiddleModal';
import BottomModal from '../components/BottomModal';
import HomeWorkshop from './HomeWorkshop';
import HomeBannerCarousel from '../components/HomeBannerCarousel'
import LoadingVehicle from '../components/LoadingVehicle';
import MarketingSectionsCard from '../components/MarketingSectionsCard';

const WIDTH = Dimensions.get('window').width

class HomeScreen extends React.Component{
    constructor(){
        super()
        this.state = {
            view:2,
            v_data: '',
            refreshing: false,
            loading:false,
            check_v: false,
            referral_valid: false,
            refer_code:'',
            picture_user:'',
            modalVehicle :false,
            modalInsurance: false,
            name: '',
            email :'',
            phone: '',
            marketingPopup: '',
            showsections: [],
            showcategory: [],
            marketing_sections: '',
            marketing_categ: '',
            marketing_art: ''


        }
    }

    componentDidMount(){
        this.fetchVehicles()
        this.fetchUserPhoto()
        this.getRegion()
        this.fetchConsts()
        this.checkPermission()
        // this.createNotificationListeners()
        this.fetchPopup()
        this.statusListener()
        this.lastSeen()
        this.marketing_category()
        AppState.addEventListener('change', 
        this.handleAppStateChange);
        this.isMounted = false

    }


    componentWillUnmount(){
        let FUID = this.props.mobx_auth.FUID;
        firebase.database().ref(`users/${FUID}/vehicles`).off()
        // this.notificationListener();
        this.notificationOpenedListener();
        AppState.removeEventListener('change', this.handleAppStateChange);
        this.isMounted = false
    }

    statusListener(){
        let that = this
        let FUID = this.props.mobx_auth.FUID;
        firebase.database().ref(`plate_number`).on('child_changed', (snp) => {
          if(snp.exists()){
                that.fetchVehicles()
          }
              
        })
      }

      
    fetchVehicles(){
        let that = this;
        let FUID = this.props.mobx_auth.FUID;
        let ALLPLATE = []
        firebase.database().ref(`users/${FUID}/vehicles`).on('value', (snapshot) => {
          let KEY = [];
          if(snapshot.exists()){
            KEY = Object.keys(snapshot.val())
            let V_DATA = [];
            KEY.forEach((key_id) => {
              
                let PLATE  = key_id 
                let STATUS = snapshot.val()[key_id].status
                let OWNERSHIP = snapshot.val()[key_id].true_owner
    
                firebase.database().ref(`plate_number/${PLATE}`).once('value', (snp) => {
                  if(snp.exists()){
                    let a = snp.val()
                    a.plate = PLATE
                    a.true_owner = OWNERSHIP
                    a.status = STATUS
        
                    V_DATA.push(a);
                    let SORTED = V_DATA.sort((a,b) => b.timestamp - a.timestamp)
                    that.setState({v_data:SORTED, view:3,refreshing: false, loading:false, check_v:true, allplate:KEY});
            
    
                  }
                })
            })
        
          } else {
            that.setState({view:1,refreshing: false, loading:false,  check_v: false, v_data:[]});
          }
        })
    
        firebase.database().ref(`users/${FUID}/referral_code`).once('value', (snp) => {
          if(snp.exists()){
            let G = snp.val()
            if(G.registration_code !== undefined){
              let REF = G.registration_code
              if(G.registration_redeemed === undefined){
                this.setState({referral_valid : true, refer_code: REF}) 
              } else { 
              }
            }
          }
        })
    }

    fetchUserPhoto = () => {
        let that = this;
        let FUID = this.props.mobx_auth.FUID;
        let PICTURE
    
        firebase.database().ref(`users/${FUID}/user_picture/`).once('value').then((snapshot) => {
          if(snapshot.exists()){
           PICTURE = snapshot.val().downloadURL
            this.setState({picture_user: PICTURE })            
          } 
        })

        firebase.database().ref('users/' + FUID).update({
            version: this.props.mobx_config.Version
        })

        let EMAIL, NAME
    
        firebase.database().ref('users/' + FUID).once('value').then((snapshot) => {
            if(snapshot.exists()){
                let X = snapshot.val()
                if(X.email === undefined){
                    firebase.database().ref('retail_admin/' + FUID).once('value').then((snp) => {
                        if(snp.exists()){
                        let b = snp.val()
            
                        EMAIL = b.email
                        NAME = b.name
            
                      
                        }
                    })
                } else {
                    EMAIL = X.email
                    NAME = X.name
                }
        
                firebase.database().ref(`flat_users/${FUID}`).update({
                    name: X.name,
                    email: X.email,
                    phone: X.Phone,
                    platform: Platform.OS,
                    register_timestamp: X.register_timestamp !== undefined ? X.register_timestamp : 1486702687,
                    photo: PICTURE !== undefined ? PICTURE : '',
                    
            });
    
                that.props.mobx_auth.setEMAIL(X.email);
                that.props.mobx_auth.setNAME(X.name);
                that.props.mobx_auth.setPHONE(X.Phone);
        
                let VER 
        
                if(X.Phone_Verified === true){
                    VER = true
                } else {
                    VER = false
                }
        
                this.setState({
                    name: snapshot.val().name,
                    email: snapshot.val().email,
                    phone : snapshot.val().Phone,
                    verified: VER,
                    modalVer: !VER
                })
            }
        })
    }

    getRegion = async () => {
        let that = this;
        let FUID = this.props.mobx_auth.FUID;
    
        let URL = 'https://ipapi.co/json/'
    
        await fetch(URL)
        .then(data => data.json())
        .then(data => {
            if (this.isMounted) {
                let REGION =  data.country_name
                let REGCODE = data.country_code
        
            firebase.database().ref(`users/${FUID}`).update({ 
                region: REGION,
                regionCode: REGCODE,
                version:this.props.mobx_config.Version
            })
        
            firebase.database().ref(`flat_users/${FUID}`).update({ 
                region: REGION,
                regionCode: REGCODE
            })
        
            }    
        })
    
    }
    
    forcelogout = async(x) => {
        if(x === 'exist'){
          let tokenP = await AsyncStorage.removeItem('CSRFTokenP')
          let secretP = await AsyncStorage.removeItem('CSRFSecretP')
  
          let tokenU = await AsyncStorage.removeItem('CSRFTokenU')
          let secretU = await AsyncStorage.removeItem('CSRFSecretU')
        }
  
        Alert.alert('A better experience for you','We updated our app to serve you a better experience. To start enjoying SERV again, please relogin to your account.')
        
        firebase.auth().signOut().then((hmm) =>{
          this.props.mobx_auth.setCheck(1);
        }).catch( (err) => {
          alert(err)
        })
    }

    fetchConsts(){
        let FUID = this.props.mobx_auth.FUID
        let RELOGIN
    
        firebase.database().ref('consts/').once('value').then((snapshot) => {
          if(snapshot.exists()){
            let X = snapshot.val()    
            if(X.relogin !== undefined){
              RELOGIN = X.relogin
            }
          }
        })
        .then(async() => {
            let tokenP =   AsyncStorage.getItem('CSRFTokenP')
            let secretP =   AsyncStorage.getItem('CSRFSecretP')
        
            let tokenU =  AsyncStorage.getItem('CSRFTokenU')
            let secretU =  AsyncStorage.getItem('CSRFSecretU')
        
            firebase.database().ref(`users/${FUID}/`).once('value').then((snp) => {
                if(snp.exists()){
                let M = snp.val()
                if(M.relogin !== undefined){
                    if(M.relogin < RELOGIN){
                    firebase.database().ref(`users/${FUID}/`)
                    .update({relogin: RELOGIN})
                    .then(() => {
                        this.forcelogout('exist')
                    })
                    
                    
                    } 
                    if(tokenP === null || secretP === null ||  tokenU === null ||  secretU === null ){
                    this.forcelogout('none')
                    }
                    
                } else {
                    firebase.database().ref(`users/${FUID}/`)
                    .update({relogin: RELOGIN})
                    .then(() => {
                    this.forcelogout('none')
                    })
                }
                }
            })
        })    
    }
    
    handleAppStateChange = (nextAppState) => {
        if (nextAppState === 'inactive') {
        // console.log('the app is closed');
          this.setState({closeBoxplate: false})
        }    
    }

    //marketing 
    fetchPopup =  async () => {
        let INPUT = await firebase.database().ref('/marketing_popup').once('value').then(snapshot => {
            //1 check data marketing exists ke tak 
            if (snapshot.exists()) {  //1b if snapshot.exists == true (means ada data)
               
                let q = snapshot.val(); // not need to loop inside the database because there is not key unique. 
                this.setState({ marketingPopup: q})
            } else {
                //1c if data tak adaa, display loading true, 
                //console.warn("mana ada data")
                this.setState({ marketingPopup:''})
            }
        }) 
    }

    marketing_category =  () => {
        let SEC = []
        firebase.database().ref(`marketing_sections`).once('value').then((snp) => {
            if(snp.exists()){
                let keys = Object.keys(snp.val())
                keys.forEach((secID) => {
                    let a = snp.val()[secID]
                    a.id = secID 

                    SEC.push(a)
                
                })
                this.setState({marketing_sections: SEC})
            }
        })
        let CATE = []
        firebase.database().ref(`marketing_categories`).once('value').then((snp) => {
            if(snp.exists()){
                let keys = Object.keys(snp.val())
                keys.forEach((catId) => {
                    let b = snp.val()[catId]
                    b.id = catId 

                    CATE.push(b)
                })
                this.setState({marketing_categ :CATE})
            }
        }).then(() => {
            this.sortMarketing()
        })
        
    }

    sortMarketing = () => {
        let X = this.state.marketing_sections
        let Y = this.state.marketing_categ
        let ALL = []

        for (const key in X) {
            for (const catkey in Y) {
                if(key === catkey){
                    let b = {
                        cat_name : Y[catkey].name,
                        sec : X[key]
                    }

                    ALL.push(b)
                }
                this.setState({marketing_art: ALL})
            }
        }
    }


    
    //===== PUSH NOTI =======
    
    async checkPermission(){
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
            this.getToken();
        } else {
            // alert("We can't proceed without permission to send you notifications");
            this.requestPermission();
        }
    }  
    async getToken() {
        let fcmToken = await AsyncStorage.getItem('fcmToken');
        if (!fcmToken) {
            fcmToken = await firebase.messaging().getToken();
            if (fcmToken) {
                await AsyncStorage.setItem('fcmToken', fcmToken);
                let FUID = this.props.mobx_auth.FUID;
                firebase.database().ref(`users/${FUID}`).update({pushToken: fcmToken});
                
                firebase.messaging().subscribeToTopic('announcements');
                firebase.messaging().subscribeToTopic('promoCode');

    
            }
        } else {
            // do some work
            // console.warn('Device_token')
            // console.warn(fcmToken)
            await AsyncStorage.setItem('fcmToken', fcmToken);
            let FUID = this.props.mobx_auth.FUID;
            firebase.database().ref(`users/${FUID}`).update({pushToken: fcmToken});
        }
    }
    //2
    async requestPermission() {
        try {
            await firebase.messaging().requestPermission();
            // User has authorised
            this.getToken();
        } catch (error) {
        // Alert.alert('Sorry', 'why no allow notification?? :(');
            this.setState({permission:false})
            // User has rejected permissions
            console.log('permission rejected');
        }
    }
    async createNotificationListeners() {
        // PushNotification.configure({
            

        //     // (required) Called when a remote or local notification is opened or received
        //     onNotification: function(notification) {
        //         console.log("NOTIFICATION:", notification.title, notification.message);
                
        //         // process the notification here
        //         // required on iOS only
        //         // notification.finish(PushNotificationIOS.FetchResult.NoData);
        //         },

        //     //ios
        //     permissions: {
        //         alert: true,
        //         badge: true,
        //         sound: true,
        //     },
        //     popInitialNotification: true,
        //     requestPermissions: true,

        //     // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
        //     onRegistrationError: function (err) {
        //         console.log(err.message, err);
        //     },

            
        // })

        // PushNotification.createChannel(
        //     {
        //         channelId: 'fcm_fallback_notification_channel', // (required)
        //         channelName: 'NotificaitonChannel', // (required)
        //     },
        //     (created) => console.log(`createChannel returned '${created}`),
        // );

        

    /*
    * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
    * */
        firebase.messaging().onNotificationOpenedApp(remoteMessage => {
            let NOTI = remoteMessage.notification
            // console.log(
            //   'Notification caused app to open from background state:',
            //   remoteMessage.notification,
            // );
            this.showAlert(NOTI.title, NOTI.body);
            // navigation.navigate(remoteMessage.data.type);
        });

        /*
        * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
        * */
        /*
        * Triggered when a particular notification has been received in foreground 
        * while app is open
        * * */


        firebase.messaging().onMessage(remoteMessage => {
            let NOTI = remoteMessage.notification
            this.showAlert(NOTI.title, NOTI.body);
            // navigation.navigate(remoteMessage.data.type);
          });



    }
    showAlert(title, body) {
    Alert.alert(
        title, body,
        [
            { text: 'OK', onPress: () => console.warn('OK Pressed') },
        ],
        { cancelable: false },
    );
    }

    lastSeen = () => {
        let that = this;
        let FUID = this.props.mobx_auth.FUID;
      
        firebase.database().ref(`users/${FUID}`).update({app_platform: Platform.OS})
        // Create a reference to this user's specific status node.
        // This is where we will store data about being online/offline.
        const userStatusRef = firebase.database().ref('users/' + FUID + '/last_seen/');
        const flatUserRef = firebase.database().ref('flat_users/' + FUID + '/last_seen/');
      
        // We'll create two constants which we will write to
        // the Realtime database when this device is offline
        // or online.
        const isOfflineForDatabase = {
            status: 'offline',
            last_changed: firebase.database.ServerValue.TIMESTAMP,
        };
      
        const isOnlineForDatabase = {
            status: 'online',
            last_changed: firebase.database.ServerValue.TIMESTAMP,
        };
      
        // Create a reference to the special '.info/connected' path in
        // Realtime Database. This path returns `true` when connected
        // and `false` when disconnected.
        firebase.database().ref(`.info/connected`).on('value', (snapshot) => {
          let DATA;
            // If we're not currently connected, don't do anything.
            if (snapshot.val() == false) {
                return;
            };
      
            // If we are currently connected, then use the 'onDisconnect()'
            // method to add a set which will only trigger once this
            // client has disconnected by closing the app,
            // losing internet, or any other means.
            userStatusRef.once('value').then((snapshot) => {
      
              if(snapshot.exists()) {
                DATA = {
                  last_changed : snapshot.val().last_changed,
                  status : snapshot.val().status
                }
              } 
            }).then(() => {
      
              userStatusRef.onDisconnect().update(isOfflineForDatabase)
      
            }).then(() => {
                // The promise returned from .onDisconnect().set() will
                // resolve as soon as the server acknowledges the onDisconnect()
                // request, NOT once we've actually disconnected:
                // https://firebase.google.com/docs/reference/js/firebase.database.OnDisconnect
      
                // We can now safely set ourselves as 'online' knowing that the
                // server will mark us as offline once we lose connection.
                userStatusRef.update(isOnlineForDatabase);
            }).then (() => {
              flatUserRef.set(firebase.database.ServerValue.TIMESTAMP)
            })
            //uncomment for online history status
            // .then(() => {
            //   userHistoryRef.push().set(isOnlineForDatabase)
            // })
        });
    }
      
    // ====== RENDER ======= 
    
    renderVehicle = () => {
        let display 
        let DATA = this.state.v_data

        if(DATA !== ''){
            display = DATA.map(item => (
                <View style={{alignItems:'center', justifyContent:'center', marginBottom:15 }} >
                <TouchableWithoutFeedback onPress={() => {
                    let Plate = this.props.mobx_carkey.setPLATE(item.plate);
                    firebase.analytics().logScreenView({
                        screen_name: 'CarProfile',
                        screen_class: 'CarProfile',
                      })
                    this.props.navigation.navigate('VehicleProfile')}}>

                    <View style={{ borderRadius:10, margin:5, marginBottom:10, }}>
                        {item.picture_car !== '' && item.picture_car !== undefined ?
                        <Image source={{uri: item.picture_car.downloadURL}} style={styles.vehicleImg2} borderRadius={10} />
                        : <Image source={item.type_desc === 'Motorcycle' ? motorEmpty : vehicleEmpty} resizeMode="cover" borderRadius={10} style={styles.vehicleImg}/> }
                        
                    </View>
                </TouchableWithoutFeedback>
                <Text style={{fontSize:15,fontFamily:'ProximaNova-Bold', color:'#fff'}}>{item.plate}</Text>
                </View>
            ))
        }


        return display 
    }

    callbackModal = () => {this.setState({ modalVehicle:false }) }

    vehicleList = () => {
        let display
        let DATA = this.state.v_data

        if(DATA.length !== 0){
            display = 
            <FlatList
                data={DATA}
                renderItem={({item}) =>
                <View>
                    {item.book_walkin === undefined || item.book_walkin._book_id === '' ?
                        <TouchableOpacity
                            onPress={() => {
                                let that = this;
                                let model =  this.props.mobx_carkey.setMODEL(item.model);
                                let make =   this.props.mobx_carkey.setMAKE(item.make);
                                let carcc =   this.props.mobx_carkey.setCARCC(item.carcc);
                                let trans =   this.props.mobx_carkey.setTRANSMISSION(item.transmission);
                                let year =   this.props.mobx_carkey.setYEAR(item.year);
                                let plate =   this.props.mobx_carkey.setPLATE(item.plate);
                                let type =   this.props.mobx_carkey.setVTYPE(item.type_desc);

                                let nmame =   this.props.mobx_auth.setNAME(this.state.name);
                                let emel =   this.props.mobx_auth.setEMAIL(this.state.email);
                                let pong =   this.props.mobx_auth.setPHONE(this.state.phone);
                                
                                this.setState({modalVehicle:false})

                                this.props.navigation.navigate("QRScan")
                            }}
                            style={{padding:15, flexDirection:'row', justifyContent:'space-between', alignItems:'center', borderBottomWidth:0.5, borderBottomColor:'#ccc', marginLeft:15, marginRight:15}}>
                            <View style={{height:40, width:40,}}>
                            <Image source={item.type_desc === 'Motorcycle' ? motorEmpty : vehicleEmpty} style={{height:40, width:40}}/>
                            </View>
                            <View>
                            <Text>{item.make} {item.model}</Text>
                            <Text>{item.carcc}L / {item.year}</Text>
                            </View>
                            <View>
                            <Text>{item.plate}</Text>
                            </View>

                        </TouchableOpacity>
                    : null }
                </View>
                }

            />
        } else {
            display = 
            <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                <Text style={{fontFamily: "ProximaNova-Bold",}} >Please add at least one vehicle first</Text>
            </View>
        } 

        return display
    }

    insuranceNavigation = (x) => {
        let LINK 
        if(x === 'Sureplify'){ 
            LINK = 'https://www.sureplify.com/#/?aff=SERV'
            firebase.analytics().logScreenView({
                screen_name: 'Insurance_sureplify',
                screen_class: 'Insurance_sureplify',
              })
            
        } 
        if(x === 'PolicyStreet' ){
            LINK = 'https://www.policystreet.com/motor?utm_source=serv&utm_medium=referral&utm_campaign=anything'
            firebase.analytics().logScreenView({
                screen_name: 'Insurance_PolicyStreet',
                screen_class: 'Insurance_PolicyStreet',
              }) 
        }  
        if(x === 'Allianz'){
            LINK = "https://getquote.allianz.com.my/motor-online?utm_source=SERV&utm_medium=app&utm_campaign=stayathome"
            firebase.analytics().logScreenView({
                screen_name: 'Allianz',
                screen_class: 'Allianz',
              })
        }
        
        let uid = this.props.mobx_auth.FUID;


        firebase.database().ref('Insurance/' + uid ).push({
            opened: firebase.database.ServerValue.TIMESTAMP
        })
        this.props.navigation.navigate('WebviewInsurance', {url_link: LINK, company: x})
    }

    refreshList = () => {
        this.setState({refreshing: true})
        this.fetchVehicles()
        this.marketing_category()
    }

    openWorkshop = () => {
        this.props.navigation.navigate('WorkshopListing')
    }

    openServ = () => {
        let LINK = 'https://serv.my/'
        this.props.navigation.navigate('Webview', {link: LINK})
    }

    //==== marketing popup ==== 
    navigate = (obj) => {
        let LINK = obj.link;
        //console.warn("eee",LINK)
        this.props.navigation.navigate('Webview', {link: LINK})
        this.setState({ modalPopup: false})
        
    }

     // render data into react
    popOutmarketing = () => {
        let display 
        let obj = this.state.marketingPopup;
        
        if(obj.banner_image !== undefined && obj.banner_image !== ''){
            display = 
            <MiddleModal modalName ={this.state.modalPopup} height={(WIDTH - 60) * 1.5}  padding={true}  children={
                <View  style={{padding:0, }}> 
                    <Image source={{ uri: obj.banner_image}} style={{width: WIDTH - 30, height:(WIDTH - 60) * 1.5, borderRadius: 10 }} />
                    <View style={styles.displayButton}>
                        <TouchableOpacity style={{...styles.buttonPop, backgroundColor: '#fff', borderColor: '#080880', borderWidth:2}} onPress={() => this.setState({modalPopup: false})}>
                            <Text style={{fontFamily: "ProximaNova-Bold",color: '#080880'}}>Maybe later</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{...styles.buttonPop, backgroundColor:'#080880', borderColor: '#080880', borderWidth: 2,}} onPress={() => this.navigate(obj)}>
                            <Text style={{fontFamily: "ProximaNova-Bold",color: '#fff'}}>View</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            }/>
        } 
         
    
        return display;
    }

    //==== marketing ==== 
    marketing_cate = () =>{
       
        let display 

        let ALL = this.state.marketing_art
        if(ALL.length !== 0){
            display = ALL.map(item => (
                <View >
                <Text style={{fontFamily: "ProximaNova-Bold",marginLeft:20, marginTop:20, fontSize:16}}>{item.cat_name}</Text>
                    <MarketingSectionsCard data={item.sec} navigation={this.props.navigation} />
                </View> 
            ))
    }

    return display
  }





    render(){

        let topContent
        if(this.state.view === 1){
            topContent = 
            <TouchableWithoutFeedback onPress={() => {
                firebase.analytics().logScreenView({
                    screen_name: 'AddCar_exist',
                    screen_class: 'AddCar_exist',
                  }) 
                this.props.navigation.navigate('VehicleAdd',  {refresh_car : this.refreshList })}}>
            <View style={{flexDirection:'row', alignItems:'center', margin:20, marginBottom: 35}} >
                <Image source={vehicles} style={{width:Dimensions.get('window').width /2 - 50, height: Dimensions.get('window').width /2 - 50,marginRight: 15, resizeMode:'contain' }}/>
                
                <View style={{alignItems:'center', justifyContent:'center', margin:10}} >
                    <Image source={addButton} style={styles.img}/> 
                    <Text style={{fontSize:16,fontFamily:'ProximaNova-Bold', color:'#fff', marginTop:15}}>Add your first vehicle</Text>
                </View>
                
            </View>
            </TouchableWithoutFeedback>
        } 
        
        if(this.state.view === 2){
            topContent = <LoadingVehicle />
        }

        if(this.state.view === 3){
            topContent = 
            <View style={{flexDirection:'row', alignItems:'center', marginTop:20, marginBottom: 35}} >
                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                    <TouchableWithoutFeedback onPress={() => {
                         firebase.analytics().logScreenView({
                            screen_name: 'AddCar_exist',
                            screen_class: 'AddCar_exist',
                          }) 
                         this.props.navigation.navigate('VehicleAdd',  {refresh_car : this.refreshList })}}>
                    
                    <View style={{alignItems:'center', justifyContent:'center',marginLeft:20, marginBottom:15 }} >
                        <View style={styles.addcarstyle}>
                            <Image source={addCar} style={styles.vehicleImg}/>
                            {/* <Image source={addButton} style={{position:'absolute', bottom:10, right:10, height:50, width:50}}/>  */}
                        </View>
                        <Text style={{fontSize:15,fontFamily:'ProximaNova-Bold', color:'#fff'}}>Add New</Text>
                    </View>
                    </TouchableWithoutFeedback>
                    
                    {this.renderVehicle()}
                    <View style={{width:40}} />
                </ScrollView>
            </View>
        }

        let User 
        if(this.state.picture_user !== ''){
            User = <Image source={{uri:this.state.picture_user}} style={{...styles.img, borderRadius:18, resizeMode:'cover'}} />
 
        } else {
            User = <Image source={profile} style={styles.img} />
        }
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <StatusBar barStyle="light-content" backgroundColor='#1d1d1d' />

                    {this.state.marketingPopup !== '' ? this.popOutmarketing() : null}

                    <ScrollView showsVerticalScrollIndicator={false}  
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this.refreshList}
                            />
                        }>
                    <View style={{backgroundColor:'#080880', borderBottomLeftRadius:20,borderBottomRightRadius: 20  }}>
                        <View style={{flexDirection:'row', justifyContent:'space-between', padding:20,}}>
                            <TouchableWithoutFeedback onPress={this.openServ}>
                            <Image source={servS} style={styles.img} />
                            </TouchableWithoutFeedback>
                            
                            <TouchableWithoutFeedback onPress={() => {
                                firebase.analytics().logScreenView({
                                    screen_name: 'Profile_homepage',
                                    screen_class: 'Profile_homepage',
                                  }) 
                                this.props.navigation.navigate('Profile', {refresh_photo: this.fetchUserPhoto})}}>
                                <View style={{borderWidth:0.5, borderColor:'#fff', height:42, width: 42, borderRadius:21, alignItems:'center', justifyContent:'center'}}>
                                {User}
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                        <Text style={{fontFamily:'ProximaNova-Bold',color:'#fff', fontSize:20, paddingLeft:20,}}>My Vehicles</Text>
                        {topContent}
                    </View>
                    <TouchableWithoutFeedback onPress={() => this.setState({modalVehicle: true})} >
                    <View style={styles.middleBox}>
                        <View style={styles.separator} >
                            <Image source={qr} style={{height:30, width:30}} />
                        </View>
                        
                        <View  style={{flexShrink:1}} >
                            <Text style={{fontFamily: "ProximaNova-Bold",}} >Scan QR Code</Text>
                            <Text style={{marginTop:5, fontSize:11}}>Click here to scan a Workshop QR code to service your vehicle</Text>
                        </View>
                    </View>
                    </TouchableWithoutFeedback>

                    <View style={{borderBottomWidth:0.5, borderBottomColor:'#ccc'}}>
                    <View style={{flexDirection:'row', justifyContent:'space-between', margin:20, marginBottom:0}} >
                        <TouchableWithoutFeedback onPress={this.openWorkshop} >
                        <View style={styles.icon}>
                            <Image source={service1} style={styles.iconService} />
                            <Text style={styles.textIcon}>Workshop</Text>
                        </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('RoadAssisstance')} >
                        <View style={styles.icon}>
                            <Image source={service2} style={styles.iconService} />
                            <Text style={styles.textIcon}>Road Assistance</Text>
                        </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress={() => this.setState({modalInsurance: true})}>
                        <View style={styles.icon}>
                            <Image source={service3} style={styles.iconService} />
                            <Text style={styles.textIcon}>Insurance & Road Tax</Text>
                        </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('HomeServices', {vehicleData: this.state.v_data})} >
                        <View style={styles.icon}>
                            <Image source={more} style={styles.iconService} />
                            <Text style={styles.textIcon}>More</Text>
                        </View>
                        </TouchableWithoutFeedback>
                    </View>
                    </View>

                    <Text style={{marginTop:20,marginLeft:20, fontSize:16, fontFamily: "ProximaNova-Bold",}}>What's new</Text>
                    <HomeBannerCarousel navigation={this.props.navigation} />
                    
                    <View style={{height:20}} />
                    
                    {this.marketing_cate()}

                    <View style={{height:20}} />

                    </ScrollView>


                    <BottomModal
                        modalName={this.state.modalVehicle}
                        onPress={() => this.setState({modalVehicle:false})}
                        style={styles.innerModal}
                        children = {
                            <View>          
                                <View style={{padding:15,borderBottomColor:'#080880', alignItems:'center',}}>
                                    <Text style={{textAlign:'center', color:'#080880', fontFamily: "ProximaNova-Bold", fontSize:18}}>Select Vehicle</Text>
                                    <Text style={{marginTop:10,textAlign:'center', color:'red', fontFamily: "ProximaNova-Bold", fontSize:10}}>*If your vehicle is not listed here it is because you have already requested a service for it.</Text>
                                </View>
                                
                                
                                <ScrollView style={{height:Dimensions.get('window').height / 2}}>
                                    {this.vehicleList()}
                                </ScrollView>
                      
                          </View>
                        }
                    />
                    <BottomModal
                        modalName={this.state.modalInsurance}
                        onPress={() => this.setState({modalInsurance:false})}
                        style={styles.innerModal}
                        children = {
                            <View >    
                                <Icon name= 'close' color='#080880' size={25} style={{position:'absolute', right:20, top:10}} />      
                                <View style={{padding:20, alignItems:'center',paddingBottom:0}}>
                                    <Text style={{fontSize:20, fontFamily: "ProximaNova-Bold", marginBottom:10, textAlign:'center', color:'#080880',}}>Insurance & Road Tax</Text>
                                    <Text style={{fontFamily:'ProximaNova-Bold', fontSize:12, textAlign:'center',}} >Our Insurance & Road Tax service helps you to renew or check on your insurance from the comfort of your home.</Text>
                                </View>
                                <View style={{flexDirection:'row',  justifyContent:'space-around', alignItems:'flex-start', display:'flex', flexWrap:'wrap', margin:15 }}>
                                        {/* <TouchableWithoutFeedback  onPress={() => this.setState({modalInsurance:false}, () =>  this.insuranceNavigation('Sureplify') )}>
                                        <View style={styles.jenisService}  >
                                            <Image source={sureplify} style={{height:50, width:50,resizeMode:'contain'}}/>
                                            <Text style={styles.textS}>Sureplify</Text>
                                        </View>
                                    </TouchableWithoutFeedback> */}
                                    <TouchableWithoutFeedback  onPress={() => this.setState({modalInsurance:false},() => this.insuranceNavigation('PolicyStreet') )}>
                                        <View style={styles.jenisService}  >
                                            <Image source={policystreet} style={{height:50, width:50,resizeMode:'contain'}}/>
                                            <Text style={styles.textS}>PolicyStreet</Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                    <TouchableWithoutFeedback  onPress={() => this.setState({modalInsurance:false},() => this.insuranceNavigation('Allianz') )}>
                                        <View style={styles.jenisService}  >
                                            <Image source={allianz} style={{height:50, width:50,resizeMode:'contain'}}/>
                                            <Text style={styles.textS}>Allianz</Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                      
                          </View>
                        }
                    />

                    
                </View>
            </SafeAreaView>
        )
    }
}

HomeScreen = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(HomeScreen))
export default HomeScreen 


const styles = StyleSheet.create({
    img :{height:36, width:36,resizeMode:'contain', alignSelf:'center'}, 
    service : {height:20, width:20},
    iconService:{
        resizeMode:'cover', alignSelf:'center',
        height:55, width:55
    },
    icon:{
        width:(Dimensions.get('window').width - 30) /4 , 
        marginBottom:20,
        // alignItems:'center',
        // justifyContent:'center',
        paddingLeft:5, 
        paddingRight:5,
    },
    textIcon:{
        // marginTop:14,
        textAlign:'center',
        fontFamily: "ProximaNova-Bold",
        fontSize:13
    },
    vehicleImg:{
        width: Dimensions.get('window').width /3.7,
        height:Dimensions.get('window').width /3,
        borderRadius:10,
        resizeMode:'cover',
        
    
        // backgroundColor:'#fff'
    },
    vehicleImg2:{
        // width:100,
        width: Dimensions.get('window').width /3.8,
        height:Dimensions.get('window').width /3,
        
        resizeMode:'cover',
        borderRadius:10,
        backgroundColor:'#fff'
    },
    innerModal: {     
        borderTopLeftRadius: 10,
        borderTopRightRadius:10,
        width:Dimensions.get('window').width,
        position:'absolute',
        bottom:0,
        backgroundColor: '#fff',
            ...Platform.select({
                ios: {
                    shadowColor: '#000',
                    shadowRadius: 2,
                    shadowOffset: { width: 0, height: 3 },
                    shadowOpacity: 0.6,
                },
                android: {
                    elevation: 2
                },
            }),
    },
    jenisService:{
        // backgroundColor:'white',
        // marginRight:15,
        marginBottom:15,
        width:Dimensions.get('window').width / 3.75,
        height:Dimensions.get('window').width / 3.6,
        alignItems:'center',
        justifyContent:'center',
      },
      textS: {marginTop:15, textAlign:'center', fontFamily:'ProximaNova-Bold', fontSize:12},
      middleBox :{
        backgroundColor:'#fff',flexDirection:'row',alignItems:'center', width: Dimensions.get('window').width - 60, padding:10, borderRadius:10,flexDirection:'row', marginLeft:30, marginTop:-30,
        ...Platform.select({
          ios: {
            shadowColor: '#000',
            shadowRadius: 3,
            shadowOffset: { width: 0, height: 2},
            shadowOpacity: 0.4,
          },
          android: {
            elevation: 2
          },
        }),
      },
      separator:{
        borderRightWidth: 1,
        borderRightColor: '#ccc',
        marginRight: 15,
        paddingRight:15,
        paddingLeft:15
        // marginBottom:10,
      },
    addcarstyle :{
        borderRadius:10,marginBottom:10,marginRight:5,
        // borderWidth:0.5,borderColor:'#ccc',
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 2},
              shadowOpacity: 0.4,
            },
            android: {
              elevation: 2
            },
          }),

    },
    displayButton:{
        bottom: 15,
        // left:10, 
        // right:10,
        // borderWidth:1,
        position:'absolute',
        justifyContent:'space-around',
        flexDirection:'row',
        width:'100%',
        // margin:10,
        marginBottom:0
    },
    buttonPop: {padding:15,width:'44%', borderRadius:10, alignItems:'center', justifyContent:'center'},


})