import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Image,Dimensions,RefreshControl, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/FontAwesome"
import { NavigationEvents } from 'react-navigation';

import AsyncStorage from '@react-native-async-storage/async-storage';
import Moment from 'moment';

import LoadingTrack from "../components/LoadingTrack.js"
import Header from '../components/Header.js'
import vehicles from '../assets/vehicles_placeholder.png'
import receipt from '../assets/icons/receipt.png'
import record from '../assets/icons/record.png'

const months = {
    'January' : 'Jan',
    'February' : 'Feb',
    'March' : 'Mar',
    'April' : 'Apr',
    'May' : 'May',
    'June' : 'June',
    'July' : 'July',
    'August' : 'Aug',
    'September' : 'Sept',
    'October' : 'Oct',
    'November' : 'Nov',
    'December' : 'Dec'
}


class TrackScreen extends React.Component{
    constructor(){
        super()
        this.state = {
            vehicleData : '',
            ongoing_act: '',
            car_history :'',
            totalPayment: 0,
            totalSelf: 0,
            isLoading :false,
            refreshing :false,
            screen : 1
        }
    }
    
    componentDidMount(){
        this.fetchHistory()
    }
    
    fetchHistory(){
        // this.setState({screen: '3'})
        let that = this;
        let KEY =[];
        let FUID = this.props.mobx_auth.FUID;
        let NAME = this.props.mobx_auth.NAME;
        
        
        let CAR_HISTORY = [];
        let COUNT = 0
        let SCOUNT = 0
        // let ONGOIN = []
        // let ONGOINW = []

        // once
        firebase.database().ref(`users/${FUID}/vehicles/`).once('value', function(snp) {
            if(snp.exists()){
                let ONGOIN = []
                let ONGOINW = []
                let VDATA = []

                let KEY = Object.keys(snp.val())
                KEY.forEach( (key_id) => {
                    let PLATE = key_id
                    
                    firebase.database().ref(`plate_number/${PLATE}`).once('value', function(snap){
                        if(snap.exists()){
                            // 1. book_history 
                            let VAL = snap.val()

                            let NOM
                            if(NAME !== undefined){
                                NOM = NAME.charAt(0) + NAME.charAt(1) + NAME.charAt(2)
                            } else {
                                let name = VAL.owner.name
                                NOM = name.charAt(0) + name.charAt(1) + name.charAt(2)
                            }
                            VAL.plate = PLATE
                            VAL.type_desc = VAL.type_desc
                            let book_Data= VAL.book_history;
                            if(book_Data!== undefined){
                                Object.keys(book_Data).map((key_book)=>{
                                    if(book_Data[key_book].type !== 'self input' ){
                                        if(book_Data[key_book].assign.status !== 'deleted' &&  book_Data[key_book].quotation !==  undefined ){
                                            if(book_Data[key_book].quotation.total_price !== undefined){
                                                COUNT = COUNT + 1
                                            }
                                            let data_history= book_Data[key_book];
                                            data_history.bookID = key_book
                                            data_history.vehicletype = VAL.type_desc
    
                                            CAR_HISTORY.push(data_history)
                                        }
                                    } else {
                                        SCOUNT = SCOUNT + 1
                                        let data_history= book_Data[key_book];
                                        data_history.customer_carplate = PLATE
                                        data_history.vehicletype = VAL.type_desc
                                        data_history.bookID = key_book
                                        CAR_HISTORY.push(data_history)

                                    }
                                    
                                })
                            }

                        
                            //3. ongoing drivein 
                            let ongoing_walkin = VAL.book_walkin
                            if(ongoing_walkin !== undefined){
                                if(ongoing_walkin._book_id !== '' ){
                                    let B = {}
                                    let bookID = ongoing_walkin._book_id
                                    B.book_id = bookID
                                    B.book_status = ongoing_walkin._book_status
                                    B.booking_by = ongoing_walkin.booking_by !== undefined ? ongoing_walkin.booking_by : ''
                                    B.mainID = ongoing_walkin.retail_main
                                    B.outletID = ongoing_walkin.retail_id
                                    B.type = 'Drive-In'
                                    B.day = ongoing_walkin.day
                                    B.month = ongoing_walkin.month
                                    B.year = ongoing_walkin.year
                                    B.time = ongoing_walkin.time
                                    B.carplate = PLATE
                                    B.carmake = VAL.make
                                    B.carmodel = VAL.model
                                    B.cartype = VAL.type_desc
                                    B._requestID = NOM + bookID.charAt(1) + bookID.charAt(2) + bookID.charAt(3) + bookID.charAt(4) + bookID.charAt(5) + bookID.charAt(6) + bookID.charAt(7) + bookID.charAt(8);

                                    ONGOIN.push(B)   
                                }
                            }


                            if(CAR_HISTORY.length === 0) {
                                that.setState({screen:2,refreshing: false, isLoading: false,  ongoing_act: ONGOIN, });
                            }
                            if(CAR_HISTORY.length !== 0) {
                                let SORTED = CAR_HISTORY.sort((a,b) => b.timestamp - a.timestamp)
                                that.setState({car_history:SORTED, isLoading: false, screen: 2, ongoing_act: ONGOIN,totalPayment:COUNT,  totalSelf: SCOUNT  });
                            }
                        }

                    })
                })
            }
        })

    }



    ///====== 
    ongoingActivity = () => {
        let DATA = this.state.ongoing_act
        // let HALTH = this.state.car_health
        let display, status

        if(DATA.length !== 0){
            display =  DATA.map((item) => (
                
                <TouchableWithoutFeedback onPress={() => this.navigateTo(item)}>
                <View style={styles.ongoing}>      
                    <View style={{flexDirection: 'row',justifyContent:'space-between', alignItems:'center' }}>
                        <View style={{flex:3,  }}>
                        <Text style={{fontFamily: "ProximaNova-Bold", fontSize:18}}>{item.carplate}</Text>

                        <View style={{flexDirection:'row'}}>
                            <Text style={{ fontSize:12}}>{item.carmake} </Text>
                            <Text style={{ fontSize:12}}>{item.carmodel} </Text>
                        </View>
                        
                        <Text style={{ fontSize:12}} >Ref: {item._requestID.toUpperCase()}</Text>
                        <Text style={{ fontSize:12}} >Category: {item.type}</Text>
                        </View>
                        <View style={{flex:2, }}>
                        
                        {   item.book_status === 'Unpaid' || item.book_status === 'Complete' ? <Text style={{...styles.stats, color:'#e85b1a', }}>Awaiting Payment</Text>  :
                            item.book_status === 'Appointment Accepted' || item.book_status === 'Appointment'  && item.type === 'Drive-In' ? <Text style={{...styles.stats, color:'green'}}>View Appointment</Text> :
                            item.book_status ==='Appointment Declined' && item.type === 'Drive-In' ? <Text style={{...styles.stats, color:'#e85b1a'}}>Appointment Declined</Text> :
                            item.book_status ==='Appointment Changed' && item.type === 'Drive-In' ? <Text style={{...styles.stats, color:'#ffce00'}}>Appointment Adjusted</Text> :
                            item.book_status === 'Requesting' && item.type === 'Drive-In' ? <Text style={{...styles.stats, color:'#ffce00'}}>Undergoing Inspection</Text> :
                            item.book_status === 'Accepted' || item.book_status === 'Pending Complete'  && item.type === 'Drive-In' ? <Text style={{...styles.stats, color:'green'}}>Performing Service</Text> :
                            item.book_status === 'Pending' && item.type === 'Drive-In' ? <Text style={{...styles.stats, color:'#ffce00'}}>Review Quotation</Text> :
                            item.book_status === 'Cancel' && item.type === 'Drive-In' ? <Text style={{...styles.stats, color:'#e85b1a'}}>Pending Cancellation</Text> :

                            <Text style={{...styles.stats,color:'green'}}>Rate Service</Text>
                        }

                        <Text style={{fontFamily:'ProximaNova-Bold', color:'#080880', textAlign:'right'}}>{item.day} {months[item.month]} {item.year}</Text>
                        <Text style={{fontFamily:'ProximaNova-Bold', fontSize:11, color:'#080880', textAlign:'right'}}>{item.time}</Text>
                        </View>

                        <View style={{flex:0.5, }} />
                        <Icon name="angle-right" size={20} style={{position:'absolute', right:10, bottom:15,}} />

                    </View>
                </View>

                </TouchableWithoutFeedback>
            ))
        } else {
            display = 
            <View style={{alignItems:'center', justifyContent:'center', flexShrink:1,  marginLeft:15, marginRight:15}}>
                <Text style={{fontSize:11,marginTop:15, textAlign:'center'}}>No current ongoing service. Book now. </Text>
            </View>
        }

        return display

    }

    navigateTo = (item) => {
        let STATS = item.book_status
        // let CKEY = item.carkey
        let PLATE = item.carplate
        let FUID = this.props.mobx_auth.FUID
        let C_PLATE = this.props.mobx_carkey.setPLATE(PLATE);
        let CARTYPE = this.props.mobx_carkey.setVTYPE(item.cartype);
        let BOOKID = item.book_id

        //walkin
        if(item.type === 'Drive-In'){
            let MID = item.mainID
            let OID = item.outletID
            let BOOKID = item.book_id
    
            let status = this.props.mobx_retail.setSTATUS(STATS);
            let bookID =  this.props.mobx_retail.setR_WALKIN_BOOKID(BOOKID);
            let wsID =   this.props.mobx_retail.setR_ID(OID);
            let groupID =   this.props.mobx_retail.setR_GROUPID(MID);
    
            if(STATS === 'Requesting'   ){this.props.navigation.navigate('WorkshopPerformService' )}
            if(STATS === 'Accepted' || STATS === 'Pending Complete'    ){this.props.navigation.navigate('WorkshopPerformService')}
            if(STATS === 'Pending'      ){this.props.navigation.navigate('WorkshopQuotation')}
            if(STATS === 'Complete'     ){this.props.navigation.navigate('WorkshopPayment')}
            if(STATS === 'Cancel'       ){this.props.navigation.navigate('WorkshopQuotation')}
            if(STATS === 'Paid'         ){this.props.navigation.navigate('WorkshopFeedback')}
            if(STATS.includes('Appointment')  ){this.props.navigation.navigate('WorkshopAppointment') }
        }
        

    }


    statusListener = () => {
        // console.warn('hmmmm');
        this.fetchHistory()
    }


    render(){
        let content

         //loading
         if(this.state.screen === 1){
            content = 
            <ScrollView showsVerticalScrollIndicator={false} style={{padding:10}} 
            refreshControl={
                <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this.statusListener}
                />
            }>
            <LoadingTrack />

            </ScrollView>
        }

        //with data
        if(this.state.isLoading === false && this.state.screen === 2){
            content = 
            <ScrollView showsVerticalScrollIndicator={false} style={{padding:10}} 
            refreshControl={
                <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this.statusListener}
                />
            }>
                <View style={{margin:5}}>
                    <Text style={{fontFamily:'ProximaNova-Bold',fontSize:20, marginBottom:15,}}>My Vehicles</Text>
                
                    <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('TrackPayment', {data: this.state.car_history})}>
                    <View style={styles.box}>
                        <View style={styles.icon}>
                            <Image source={receipt} style={{height:20, width:20}} />
                        </View>
                        <View>
                            <Text style={styles.topText}>Payment History</Text>
                            <Text style={styles.bottomText}>Made {this.state.totalPayment} payments on SERV app </Text>
                        </View>
                        <Icon name="angle-right" color='#000' size={20} style={{position:'absolute', right:15, bottom:15,}} />
                    </View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('TrackBookingHistory', {history: this.state.car_history, onNavigateBack : this.statusListener})}>                    
                    <View style={styles.box}>
                        <View style={styles.icon}>
                        <Image source={record} style={{height:20, width:20}} />
                        </View>
                        <View>
                            <Text style={styles.topText}  >Booking History</Text>
                            <Text style={styles.bottomText} >({this.state.totalPayment + this.state.totalSelf}) items</Text>
                        </View>
                        <Icon name="angle-right" color='#000' size={20} style={{position:'absolute', right:15, bottom:15,}} />
                    </View>
                    </TouchableWithoutFeedback>
                    
                    <View style={{marginBottom:20,}}>
                        <View style={{marginTop:20,  flexDirection:'row', alignItems:'center'}}>
                            <View>
                            <Text style={{fontSize:12}}>Ongoing Activity</Text>
                            {/* {this._redDot()} */}
                            </View>
                            
                            <View style={styles.separatorLine}/>
                        </View>
                
                        {this.ongoingActivity()}
                    </View>
                </View>

            </ScrollView>

        }

        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <NavigationEvents onDidFocus={() => this.statusListener()} />
                <Header backButton ={false}  headerTitle = 'Track'  />
                    
                    {content}

                </View>
            </SafeAreaView>
        )
    }
}

TrackScreen = inject('mobx_auth','mobx_retail', 'mobx_carkey', 'mobx_config')(observer(TrackScreen))
export default TrackScreen

const styles = StyleSheet.create({
    box: { flexDirection:'row', alignItems:'center', borderRadius:10, marginBottom:15, padding:15,backgroundColor:'#fff',
    ...Platform.select({
        ios: {
          shadowColor: '#000',
          shadowRadius: 2.22,
          shadowOffset: { width: 0, height: 1 },
          shadowOpacity: 0.22,
        },
        android: {
          elevation: 3
        },
       }),
    },
    imageBox:{
        width: Dimensions.get('window').width /3.8,
        height:Dimensions.get('window').width /3,
        margin:6,
        borderRadius:10,marginBottom:20,backgroundColor:'#fff',
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 2.22,
              shadowOffset: { width: 0, height: 1 },
              shadowOpacity: 0.22,
            },
            android: {
              elevation: 3
            },
           }),
    },
    icon:{width:40, height:40, marginRight:15, borderRadius:20, backgroundColor:'#efefef', alignItems:'center', justifyContent:'center'},

    topText :{fontSize:16, marginBottom: 6, fontFamily: "ProximaNova-Bold",},
    bottomText:{ fontSize:13, color:'grey'},
    vehicleImg:{
        width: Dimensions.get('window').width /3.7,
        height:Dimensions.get('window').width /3,
        resizeMode:'contain',
        borderRadius:10,
        // backgroundColor:'#fff'
    },
    vehicleImg2:{
        // width:100,
        width: Dimensions.get('window').width /3.8,
        height:Dimensions.get('window').width /3,
        resizeMode:'cover',
        borderRadius:10,
        backgroundColor:'#fff'
    },
    separatorLine: {
        flex: 1,
        borderWidth: StyleSheet.hairlineWidth,
        height: StyleSheet.hairlineWidth,
        borderColor: '#9B9FA4',
        marginLeft:15,
    },
    ongoing: {
        padding:15,
        marginTop:15,
        backgroundColor:'#fff',
        borderRadius:10,
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 2.22,
              shadowOffset: { width: 0, height: 1 },
              shadowOpacity: 0.22,
            },
            android: {
              elevation: 3
            },
          }),
    },
    stats :{fontFamily:'ProximaNova-Bold', textAlign:'right'}




})