import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/AntDesign"

import QRCode from 'react-native-qrcode-svg';

import Header from '../components/Header.js'
import Button from "../components/Button"

const WIDTH = Dimensions.get('window').width - 70

class ReferMerchant extends React.Component{
    render(){
        let item = this.props.navigation.state.params.item

        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>

                <Header backButton = {true}  headerTitle = 'Refer a Merchant' onPress={() => this.props.navigation.goBack()} />
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={styles.scroll}>
                        <View style={{marginBottom:15, alignItems:'center', justifyContent:'center'}}>
                            <Text style={{fontFamily: "ProximaNova-Bold",textAlign:'center', fontSize:18,color:'#080880', }}>Register a workshop to be our merchant!</Text>
                        </View>

                        <View style={{overflow:'hidden', padding:20, alignSelf:'center'}}>
                        <QRCode
                            value={item.form_link}
                            size={150}
                            color='darkblue'
                            backgroundColor='white'/>
                        </View>

                        <Text style={styles.totle} >Refer a  Local Workshop</Text>
                        <Text style={{marginBottom:15}}>Whether you have your favourite go-to workshop or you want to support workshops in your area, have the workshop owner scan this QR code to register with SERV for FREE, complete with our Workshop Management System with no extra fees.</Text>
                        <Text style={{marginBottom:15}}>Your referred person could be anyone managing service centres, workshops, car care, car accessories and any other vehicle-related services.</Text>
                        <Text style={{marginBottom:15}}>Want to see your preferred workshop listed on the SERV app?</Text>
                        <Text style={{marginBottom:15, fontFamily: "ProximaNova-Bold"}}>Help them register for FREE now!</Text>

                        {/* <Button
                            onPress={() => this.props.navigation.navigate('Webview', {link:item.tnc} )}
                            title='Read the terms and conditions' 
                            bgColor='#080880' width= {WIDTH} marginTop={30} /> */}
                        
                    </View>
                </ScrollView>
                    
                   
                    
                </View>
            </SafeAreaView>
        )
    }
}

ReferMerchant = inject('mobx_auth')(observer(ReferMerchant))
export default ReferMerchant

const styles = StyleSheet.create({
    scroll :{
        backgroundColor:'#fff',
        borderRadius:15,
        margin:15,
        padding:20,
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 3 },
              shadowOpacity: 0.3,
            },
            android: {
              elevation: 3
            },
          }),
    },
    totle :{fontSize:18, fontFamily: "ProximaNova-Bold", color:"#080880" ,marginBottom:25, marginTop:15 },

})