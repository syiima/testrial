import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,Image, TouchableWithoutFeedback,ActivityIndicator, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import AntIcon from "react-native-vector-icons/AntDesign";
import Icon from 'react-native-vector-icons/Fontisto';
import { NavigationActions, StackActions } from 'react-navigation';

import Header from '../components/Header.js'
import WorkshopInfo from '../components/WorkshopInfo'
import WorkshopQuotationConditional from '../components/WorkshopQuotationConditional'
import FormTextInput from "../components/FormTextInput.js"

import Button from "../components/Button.js"
const WIDTH = Dimensions.get('window').width - 40

class WorkshopFeedback extends React.Component{
    constructor(){
        super()
        this.state = {
            ws_data: '',
            logo : '',
            requestData: '',
            quote :'',
            ass_mech :'',
            ass_id : '',
            submit_state: false,
            rate_star: '',
            rate_text: '',
            rate_name: '',
            anon:false,
            archived :false,
            loyalty_card : false,
            loyalty_max: 0,
            user_loyalty: false,
            userloy_data: '',
            expiry: '',
            status :'',
        }
    }
    
    componentDidMount(){
        this.fetchWSInfo()
        this.fetchRequest()
        this.checkUserLoyalty()
        this.checkLoyalty()
    }

    fetchWSInfo(){
        let that = this
        let BOOK_ID =  this.props.mobx_retail.R_WALKIN_BOOKID
        let WS_ID =   this.props.mobx_retail.R_ID
        let WSMAIN_ID =   this.props.mobx_retail.R_GROUPID

        firebase.database().ref(`retail/${WSMAIN_ID}/${WS_ID}`).once('value').then((snapshot) => {
            if(snapshot.exists()){
                let a = snapshot.val()
                that.setState({ws_data: a})

            }
        })

        firebase.database().ref(`retail_main/${WSMAIN_ID}/logo`).once('value').then((snapshot) => {
            if(snapshot.exists()){
                let b = snapshot.val()
                that.setState({logo: b})
            }
        })

    }

    fetchRequest(){
        let that =this
        let DATA = []
        let BOOK_ID =  this.props.mobx_retail.R_WALKIN_BOOKID
        let WS_ID =   this.props.mobx_retail.R_ID
        let WSMAIN_ID =   this.props.mobx_retail.R_GROUPID

        firebase.database().ref(`request_walkin/${WSMAIN_ID}/${WS_ID}/${BOOK_ID}`).once('value', function(snapshot){
            if(snapshot.exists()){
                let X = snapshot.val()
                let QUOTATION = X.quotation
                let STATUS = X.status

                if(X.assigned_mechanic !== undefined){
                    that.setState({ass_mech: X.assigned_mechanic, ass_id: X.assigned_id})
                }

                that.setState({requestData : X, quote: QUOTATION, status: STATUS})
      
            } else {
                that.searchHistory()
            }
          })
    }

    checkUserLoyalty(){
        let that = this
        let UID = this.props.mobx_auth.FUID;
        let WSMAIN_ID =   this.props.mobx_retail.R_GROUPID

        firebase.database().ref(`users/${UID}/loyalty/${WSMAIN_ID}`).once('value').then((snapshot) => {
            if(snapshot.exists()){
                let X = snapshot.val()

                if(X !== undefined){
                    this.setState({user_loyalty: true, userloy_data: X.stamps,  })
                }                
            } else {
                this.setState({userloy_data: ''})
            }
        })
    }

    checkLoyalty(){
        let that = this
        let WSMAIN_ID =   this.props.mobx_retail.R_GROUPID

        firebase.database().ref(`retail_main/${WSMAIN_ID}/loyalty/`).once('value').then((snapshot) => {
            if(snapshot.exists()){
                let X = snapshot.val()

                let CURRENT_TIME = Date.now()                
                if(X.validity > CURRENT_TIME){
                    this.setState({loyalty_card: true,loyalty_max: X.max, expiry:X.validity })
                } else {
                    this.setState({loyalty_max: X.max, expiry:X.validity, loyalty_card: false})
                }
            }
        })

    }

    searchHistory(){        
        let that =this
        let DATA = []
        let BOOK_ID =  this.props.mobx_retail.R_WALKIN_BOOKID
        let WS_ID =   this.props.mobx_retail.R_ID
        let WSMAIN_ID =   this.props.mobx_retail.R_GROUPID
        let X, QUOTATION
                
        firebase.database().ref(`history_walkin/${WSMAIN_ID}/${WS_ID}/${BOOK_ID}/`).once('value').then((snapshot) => {
            if(snapshot.exists()){
                let X = snapshot.val()
                let QUOTATION = X.quotation
                let STATUS = X.status

                if(X.assigned_mechanic !== undefined){
                    that.setState({ass_mech: X.assigned_mechanic, ass_id: X.assigned_id})
                }

                that.setState({requestData : X, quote: QUOTATION,status: STATUS, archived: true})
            } else {
                // console.warn('ehh0', BOOK_ID);
                
            }
            
        })
    }



    //======= 
    renderInfo = () => {
        let item = this.state.requestData
        let display
  
        if(item !== ''){
            display =    
            <View style={{padding:20}}>
                    <View style={{flexDirection:'row'}}>
                    <Text style={{flex:1}}>Quotation No</Text>
                    <Text style={styles.boldText}>: </Text>
                    <Text style={{...styles.boldText, fontSize:17, flex:1.5}}>{item._requestID}</Text>
                </View>
                <View style={{flexDirection:'row'}}>
                    <Text style={{flex:1}}>Date</Text>
                    <Text style={styles.boldText}>: </Text>
                    <Text style={{...styles.boldText, fontSize:17, flex:1.5}}>{item.customer_day} {item.customer_month} {item.customer_year}</Text>
                </View>
                <View style={{flexDirection:'row'}}>
                    <Text style={{flex:1}}>Time</Text>
                    <Text style={styles.boldText}>: </Text>
                    <Text style={{...styles.boldText, fontSize:17, flex:1.5}}>{item.customer_time} </Text>
                </View>
                <View style={{flexDirection:'row'}}>
                    <Text style={{flex:1}}>Vehicle Plate No</Text>
                    <Text style={styles.boldText}>: </Text>
                    <Text style={{...styles.boldText, fontSize:17, flex:1.5}}>{item.customer_carplate} </Text>
                </View>
            </View>
        
        }
        return display
    }

    renderQuote = () => {
        let item = this.state.quote
        let STATUS = this.state.status
        let display
        if(item !== ''){
            display = 
                <View>
                    <View style={{...styles.stats, backgroundColor:'#FCE5CD', marginTop:20, borderBottomLeftRadius:0, borderBottomRightRadius:0,}}>
                    
                        <View style={{flexDirection:'row', justifyContent:'space-between', marginBottom:15, padding:5}}>
                        <Text style={styles.boldText}>Your Total</Text>
                            <View style={{flexDirection:'row'}}>
                                <Text style={{...styles.boldText, fontSize:12}}>RM</Text>
                                <Text style={{...styles.boldText, fontSize:24}}>{item.total_price}</Text>
                            </View>
                        </View>
  
                        {this.renderInfo()}
                    </View>

                    <View style={styles.boxItem}>
                        <WorkshopQuotationConditional item={item} status={STATUS} />
                    </View>
  
  
                </View>
            
        }
        return display
    }

    submitFeedback = () => {
        let that = this
        //check history_walkin
        let UID = this.props.mobx_auth.FUID;
        let BOOK_ID =  this.props.mobx_retail.R_WALKIN_BOOKID
        let WS_ID =   this.props.mobx_retail.R_ID
        let WSMAIN_ID =   this.props.mobx_retail.R_GROUPID

        let PLATE = this.props.mobx_carkey.PLATE
        let VTYPE = this.props.mobx_carkey.VTYPE
        let ASS_ID = this.state.ass_id
        let ASS_MECH = this.state.ass_mech

        if(this.state.rate_star !== ''){
            this.setState({submit_state : true})
            let STAMPS = {}
            let DATA = this.state.userloy_data
            let MAX = this.state.loyalty_max
            let NAME = this.props.mobx_auth.NAME.split(' ')[0]
            if(this.state.anon === true){ 
                NAME = 'Anonymous'
            }
    
            if(DATA.length !== 0 && MAX !== DATA.length){
            let X = DATA.length
        
                STAMPS[X] = {
                    timestamp : firebase.database.ServerValue.TIMESTAMP,
                    workshop_id : WS_ID,
                    booking_ID : BOOK_ID
                }
                                
            }
            if(DATA.length === 0){
                let X = DATA.length
            
                STAMPS[X] = {
                    timestamp :firebase.database.ServerValue.TIMESTAMP,
                    workshop_id : WS_ID,
                    booking_ID : BOOK_ID
                }
            }
                
            let REVIEW = {
                FUID: UID,
                rate: this.state.rate_star,
                text: this.state.rate_text,
                name: NAME,
                timestamp: firebase.database.ServerValue.TIMESTAMP,
                assigned_id: ASS_ID,
                assigned_mechanic: ASS_MECH
            }
    
            //1.update review //check if masih wujud
            if(this.state.archived === true){
                firebase.database().ref(`history_walkin/${WSMAIN_ID}/${WS_ID}/${BOOK_ID}/feedback/`).update({
                    rate: this.state.rate_star,
                    text: this.state.rate_text,
                    timestamp: firebase.database.ServerValue.TIMESTAMP,
                    assigned_id: ASS_ID,
                    assigned_mechanic: ASS_MECH
                })
            } else {
                firebase.database().ref(`request_walkin/${WSMAIN_ID}/${WS_ID}/${BOOK_ID}/feedback/`).update({
                    rate: this.state.rate_star,
                    text: this.state.rate_text,
                    timestamp: firebase.database.ServerValue.TIMESTAMP,
                    assigned_id: ASS_ID,
                    assigned_mechanic: ASS_MECH
                })
            }

            //belum expired 
            if(this.state.loyalty_card === true && DATA.length < MAX){
                // console.warn('manaa');
                 firebase.database().ref(`users/${UID}/loyalty/${WSMAIN_ID}/expired_date/`).once('value').then((snapshot) => {
                    if(snapshot.exists()){
                        //dah ada 
                    } else {
                        firebase.database().ref(`users/${UID}/loyalty/${WSMAIN_ID}/`).update({expired_date : this.state.expiry})
                    }
                })
               
                firebase.database().ref(`users/${UID}/loyalty/${WSMAIN_ID}/stamps/`).update(STAMPS)
                firebase.database().ref(`users/${UID}/loyalty/${WSMAIN_ID}/`).update({type: 'workshop'})
            } 

            //dah expired 
            if(this.state.loyalty_card === false && DATA.length < MAX){
                let B = {}

                firebase.database().ref(`users/${UID}/loyalty/${WSMAIN_ID}/`).once('value')
                .then((snapshot) => {
                    if(snapshot.exists()){
                        let B = snapshot.val()

                        // B.push(X)
                        // console.warn('apa day', X);
                        firebase.database().ref(`users/${UID}/expired_loyalty/${WSMAIN_ID}/`).update(B)
                    }
                }).then(() => {
                    firebase.database().ref(`users/${UID}/loyalty/${WSMAIN_ID}/`).remove()
                }) 

            }
    
            //3.clear booking
            firebase.database().ref(`plate_number/${PLATE}/book_walkin/`).update({
                _book_id: '',
                _book_status: '',
                retail_id: '',
                retail_main: '',
                time: '',
                day: '',
                month: '',
                year: '',
                booking_by:''

            }).then(() => {
                // 4.update workshop
                firebase.database().ref(`retail/${WSMAIN_ID}/${WS_ID}/reviews/${BOOK_ID}/`).update(REVIEW)

            }).then(() => {
                if(VTYPE === 'Motorcycle'){
                    this.showHealth('Motorcycle')     
                } else {
                    this.showHealth('Car')
                }

            })
    
        } else {
            this.setState({submit_state : false})
            Alert.alert('Do rate our service so we can SERV you better.')
            return
            
        }
    }

    showHealth = (x) => {
        let VTYPE = this.props.mobx_carkey.VTYPE
        let PLATE = this.props.mobx_carkey.PLATE

        if(x === 'Motorcycle'){
            const navigateAction = StackActions.replace({
                routeName: 'WorkshopMotorcycleHealth',
                action: NavigationActions.navigate({ routeName: 'WorkshopMotorcycleHealth' }),
              });
            this.props.navigation.dispatch(navigateAction);
            
        } 
        if(x === 'Car') {
            const navigateAction = StackActions.replace({
                routeName: 'WorkshopVehicleHealth',
                action: NavigationActions.navigate({ routeName: 'WorkshopVehicleHealth' }),
              });
              this.props.navigation.dispatch(navigateAction);
        }
      
    }



    
    render(){
        let submitButton;

        if(this.state.submit_state === false){
            submitButton =
            <Button 
                onPress={this.submitFeedback}
                title='Submit' bgColor='#080880' width={WIDTH} />

          
        } else {
            submitButton =
            <View style={{...styles.submitBox, flexDirection:'row', alignItems:"center", justifyContent:'center'}}>
                <ActivityIndicator style={{ height: 30, width: 30,  marginRight:10,alignSelf:'center'}} size="small" color="white" />
                <Text style={{color:'white'}}>Submitting... </Text>
            </View>
        }



        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Feedback' onPress={() => this.props.navigation.popToTop()} />
                    
                    <ScrollView>
                        <View style={{...styles.stats, backgroundColor:'#fff'}}>
                            <Text style={{fontFamily:'ProximaNova-Bold', fontSize:18, marginBottom:15, textAlign:'center'}}>Let's rate your service</Text>
                            <Text style={{textAlign:'center'}}>What did you think about the service provided?</Text>

                            <View style={{flexDirection:'row',marginBottom:40, alignItems:'center', marginTop:15, justifyContent:'center'}}>
                                <AntIcon name='star' color={this.state.rate_star === '' ? '#e7e7e7' : '#ffce00'} size={40} style={{marginRight:2}}
                                onPress={() => this.setState({rate_star:'1'})}/>

                                <AntIcon name='star' color={this.state.rate_star === '1' || this.state.rate_star  === '' ? '#e7e7e7' : '#ffce00'} size={40} style={{marginRight:2}}
                                onPress={() => this.setState({rate_star:'2'})}/>

                                <AntIcon name='star' color={this.state.rate_star === '4' || this.state.rate_star === '5' || this.state.rate_star === '3' ? '#ffce00' : '#e7e7e7'} size={40} style={{marginRight:2}}
                                onPress={() => this.setState({rate_star:'3'})}/>

                                <AntIcon name='star' color={this.state.rate_star === '4' || this.state.rate_star === '5' ? '#ffce00' : '#e7e7e7'} size={40} style={{marginRight:2}} 
                                onPress={() => this.setState({rate_star:'4'})} />

                                <AntIcon name='star' color={this.state.rate_star === '5' ? '#ffce00' : '#e7e7e7'} size={40} style={{marginRight:2}}
                                onPress={() => this.setState({rate_star:'5'})}/>
                            </View>



                            <FormTextInput 
                                title = 'Anything else to add?'
                                placeholder = 'Help others to find a good workshop'
                                multiline = {true} 
                                value={this.state.rate_text} 
                                onChangeText={(x)=> this.setState({ rate_text: x })}
                            /> 

                            <View style={{flexDirection:'row', alignItems:'center', marginTop:15}} >

                            <TouchableOpacity  onPress={() => this.setState({anon: !this.state.anon})}>
                                {!this.state.anon ?<Icon name='checkbox-passive' size={20} style={{marginRight:2}}/> : <Icon name='checkbox-active' size={20} /> }
                                </TouchableOpacity>
                                <Text style={{marginLeft:6}} >Review anonymously</Text>
                            </View>


                    
                        </View>

                        <View style={styles.topBox}>
                            <WorkshopInfo navigation={this.props.navigation} data= {this.state.ws_data} logo={this.state.logo} />
                        </View>

                        {this.renderQuote()}

                    </ScrollView>
                    
                    <View style={{backgroundColor:'white',  padding:15,}}>
                        {submitButton}
                    </View>

                </View>
            </SafeAreaView>
        )
    }
}

WorkshopFeedback = inject('mobx_auth','mobx_retail', 'mobx_carkey', 'mobx_config')(observer(WorkshopFeedback))
export default WorkshopFeedback

const styles = StyleSheet.create({
    stats:{
        padding:15, margin:15, borderRadius:15, marginBottom:0,
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 0.6,
            },
            android: {
              elevation: 2
            },
          }),
    },
    boldText: {fontFamily:'ProximaNova-Bold'},
    submitBox: {backgroundColor:'#080880',  padding:10, borderRadius:10,},    
    topBox: {
        backgroundColor:'white',
        borderRadius : 15,
        margin:15,
        marginTop:30,
       
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 0.6,
            },
            android: {
              elevation: 2
            },
          }),
    },
    boxItem:{
        padding:15, margin:15, borderBottomLeftRadius:15, borderBottomRightRadius:15, marginTop:0, backgroundColor:'white',
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 0.6,
            },
            android: {
              elevation: 2
            },
          }),
    },

})