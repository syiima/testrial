import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Image,Alert, Dimensions, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'

import Icon from 'react-native-vector-icons/AntDesign'
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Entypo from "react-native-vector-icons/Entypo";

import MiddleModal from '../components/MiddleModal'
import FormTextInput from '../components/FormTextInput'

import vin_loc from '../assets/vin_loc.png';

class VehicleDetails extends React.Component{
    constructor(){
        super()
        this.state = {
            isLoading: true,
            make: '',
            model: '',
            transmission: '',
            year: '',
            car_plate: '',
            colour: '',
            cc: '',
            tire_s:'',
            tire_d:'',
            nickname: '',
            type_id: '',
            VIN_number: '',
            updatedVin: '',

            modalVIN: false,


        }
    }
    
    componentDidMount(){
        this.fetchData()
        this.fetchOthers()
    }

    fetchData(){
        let that = this
        let FUID = this.props.mobx_auth.FUID
        let PLATE = this.props.mobx_carkey.PLATE
        
        firebase.database().ref(`plate_number/${PLATE}`).once('value', (snp) => {
            if(snp.exists()){
                let x = snp.val()
    
                    that.setState({ 
                        make: x.make,
                        model: x.model,
                        transmission: x.transmission,
                        year: x.year,
                        colour: x.colour,
                        cc: x.carcc,
                        type_id: x.type,
                        nickname: x.nickname !== undefined ? x.nickname : '',

                        tire_d: x.cartire_d !== undefined ? x.cartire_d : '',
                        tire_s: x.cartire_s !== undefined ? x.cartire_s : '',
                        car_notes: x.car_notes !== undefined ? x.car_notes : '',
                        isLoading:false

                    })
            } else {
                that.setState({isLoading:false})
              }
        }) 
    }

    fetchOthers(){
        let that = this;
        let PLATE = this.props.mobx_carkey.PLATE
        let VTYPE = this.props.mobx_carkey.VTYPE
        let REF 

        if(VTYPE === 'Motorcycle'){ REF = `plate_number/${PLATE}/motor_health`} 
        else { REF = `plate_number/${PLATE}/car_health`}

        firebase.database().ref(REF).once('value', function(snapshot){
            if(snapshot.exists()){
                that.setState({
                    VIN_number: snapshot.val().VIN_number !== undefined ? snapshot.val().VIN_number : '',
                    updatedVin: snapshot.val().VIN_number !== undefined ? snapshot.val().VIN_number : '',
                    // roadtax_expiry: snapshot.val().roadtax_expiry_date !== undefined ? snapshot.val().roadtax_expiry_date : '',
                    // insur_file: snapshot.val().insurance !== undefined ? snapshot.val().insurance : '',
                    isLoading:false
                })
            }else {
                that.setState({isLoading:false})
            }
        })
    }
    

    refreshVIN = () => {
        this.setState({modalVIN:false})
        this.fetchOthers()
    }

    updateVIN = () => {
        let that = this;
        let PLATE = this.props.mobx_carkey.PLATE
        let UPDATED = this.state.updatedVin
            
        firebase.database().ref(`plate_number/${PLATE}/car_health`).update({VIN_number: UPDATED})
        this.setState({VIN_number:  UPDATED, modalVIN: false})
    }

    handleRefresh = () => {
        this.setState({isLoading:true})
        this.fetchData()
      }
    

    editDets = () => {
        let PLATE = this.props.mobx_carkey.PLATE
        firebase.analytics().logScreenView({
            screen_name: 'EditDetails1_car',
            screen_class: 'EditDetails1_car',
          }) 
        this.props.navigation.navigate('VehicleEdit', {
            make: this.state.make,
            model:    this.state.model,
            year: this.state.year,
            type_id: this.state.type_id,
            nickname: this.state.nickname,
            onNvaigateBack: this.handleRefresh
    
        })
    }
    
    editDets2 = () => {
        let TRANS = this.state.transmission
        let CT
        if(TRANS === 'Auto'){
            CT = 'Automatic'
        } else {
            CT = TRANS
        }

        firebase.analytics().logScreenView({
            screen_name: 'EditDetails2_car',
            screen_class: 'EditDetails2_car',
          }) 
        this.props.navigation.navigate('VehicleEdit2', {
            transmission: CT,
            colour: this.state.colour,
            cc: this.state.cc,
            tire_d: this.state.tire_d,
            tire_s: this.state.tire_s,
            type_desc: this.state.type_desc,
            onNvaigateBack: this.handleRefresh

        })
    }

    goback = () => {
        this.props.navigation.state.params.referesh()
        this.props.navigation.goBack(null)
    }

      

    render(){
        const x = this.state
        let vinny
        if(this.state.VIN_number !== ''){
            vinny =
            <TouchableWithoutFeedback onPress={() => this.setState({modalVIN: true})}>
            <View style={{alignItems:'center', justifyContent:'center', marginTop:30, marginBottom:10}}>
                <Text style={{color:'#fff', fontFamily:'ProximaNova-Bold', fontSize:30}}>{this.state.VIN_number.toUpperCase()}</Text>
                <Text style={{color:'#fff', fontFamily:'ProximaNova-Bold'}}>Vehicle Identification Number (VIN)</Text>
            </View>
            </TouchableWithoutFeedback>
        
        } else {
            vinny =
            <TouchableWithoutFeedback onPress={() => this.setState({modalVIN: true})}>
                <View style={{flexShrink:1, alignItems:'center', justifyContent:'center', marginTop:30,}}>
                    <Text style={{color:'#fff', fontFamily:'ProximaNova-Bold', fontSize:30}}>Not Available</Text>
                    <Text style={{color:'#fff', fontFamily:'ProximaNova-Bold'}}>Vehicle Identification Number (VIN)</Text>
                    <Text style={{color:'#fff', fontSize:11, textAlign:'center', marginTop:10}}>*Your VIN will be updated when you book your first service or tap to update</Text>
                </View>
            </TouchableWithoutFeedback>

        }

        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                    <ScrollView showsVerticalScrollIndicator={false} >
                        
                        <View style={styles.top}>
                            <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between'}}>
                                <TouchableWithoutFeedback onPress={this.goback}>
                                    <View style={{flexDirection:'row', alignItems:'center', flex:2  }}>
                                        <FontAwesome name="chevron-left" size={25} color='#fff' style={{ marginLeft:10, marginRight: 10}}/>
                                    
                                    </View>
                                </TouchableWithoutFeedback>
                                
                                <Text style={{color:'#fff', fontFamily: "ProximaNova-Bold", fontSize:25,}}>Vehicle Info</Text>
                                
                                <TouchableWithoutFeedback onPress={() => this.setState({modalVIN: true})}>
                                <View style={{flex:2, }}>
                                
                                    <Entypo name='dots-three-vertical' size={25} color='#fff' style={{alignSelf:'flex-end'}} />
                                </View>
                                </TouchableWithoutFeedback>
                            </View>

                            {vinny}

                        </View>

                        {/* DETS */}
                        <View style={{margin:20,}}>
                            <TouchableWithoutFeedback onPress={this.editDets}>
                                <View style={{flexDirection: 'row', alignItems:'center', justifyContent:'space-between',margin:5, marginTop:10,}}>
                                    <Text style={{fontSize:18, fontFamily: 'ProximaNova-Bold', }}>Details</Text>
                                    <Icon name='edit' size={20} />
                                </View>
                            </TouchableWithoutFeedback>
                            <View style={styles.box}>
                                <View style={styles.viewTitle}>
                                    <Text style={styles.smolFont}>MAKE</Text>
                                    <Text style={styles.bigFont}>{x.make} </Text>
                                </View>
                                <View style={styles.viewTitle}>
                                    <Text style={styles.smolFont}>MODEL</Text>
                                    <Text style={styles.bigFont}>{x.model} </Text>
                                </View>
                                <View style={styles.viewTitle}>
                                    <Text style={styles.smolFont}>YEAR</Text>
                                    <Text style={styles.bigFont}>{x.year} </Text>
                                </View>
                                <View style={styles.viewTitle}>
                                    <Text style={styles.smolFont}>PLATE NUMBER</Text>
                                    <Text style={styles.bigFont}>{this.props.mobx_carkey.PLATE} </Text>
                                </View>
                                <View >
                                    <Text style={styles.smolFont}>NICKNAME</Text>
                                    <Text style={styles.bigFont}>{x.nickname !== '' ? x.nickname : 'Not Available'} </Text>
                                </View>
                            </View>

                        </View>

                        {/* EXTRA DETS */}
                        <View style={{marginLeft:20, marginRight: 20}}>
                            <View style={{flexDirection: 'row', alignItems:'center', justifyContent:'space-between', marginTop:10, marginBottom:4, marginLeft:5, marginRight:5}}>
                                <View style={{flexDirection:'row', alignItems:'center',flex:2}}>
                                    <Text style={{fontSize:18, fontFamily: 'ProximaNova-Bold', }}>Extra Details</Text>
                                </View>
                                <TouchableWithoutFeedback onPress={this.editDets2}>
                                    <View style={{flex:2,}}>
                                    <Icon name='edit' size={20}  style={{alignSelf:'flex-end'}}/>
                                    </View>
                                </TouchableWithoutFeedback> 
                            </View>
                        
                            <View style={styles.box}>
                                <View style={styles.viewTitle}>
                                    <Text style={styles.smolFont}>COLOUR</Text>
                                    <Text style={styles.bigFont}>{x.colour !== '' ? x.colour : 'Not Available'} </Text>
                                </View>
                                <View style={styles.viewTitle}>
                                    <Text style={styles.smolFont}>TRANSMISSION</Text>
                                    <Text style={styles.bigFont}>{x.transmission} </Text>
                                </View>
                                <View style={styles.viewTitle}>
                                    <Text style={styles.smolFont}>CC</Text>
                                    <Text style={styles.bigFont}>{x.cc} </Text>
                                </View>
                                {x.type_desc !== 'Motorcycle' ?
                                <View>
                                    <View style={styles.viewTitle}>
                                        <Text style={styles.smolFont}>RIM SIZE</Text>
                                        <Text style={styles.bigFont}>{x.tire_d !== '' ? x.tire_d + ' inches' : 'Not Available'} </Text>
                                    </View>
                                    <View >
                                        <Text style={styles.smolFont}>TIRE SIZE</Text>
                                        <Text style={styles.bigFont}>{x.tire_s !== '' ? x.tire_s : 'Not Available'} </Text>
                                    </View>
                                </View> : null }

                            </View>

                        </View>

                        <View style={{height:25}} />

                    </ScrollView>

                    <MiddleModal
                        modalName={this.state.modalVIN} children={
                            <View  >
                                <Text style = {{marginTop:20,fontSize:16,textAlign:'center', fontFamily: "ProximaNova-Bold",color:'#080880', marginBottom:15}}>Not sure where your VIN Number is? Here's a guide on where to find it.</Text>
                                <Image source={vin_loc} style={{height: Dimensions.get('window').width/2, width: '100%', alignSelf:'center',resizeMode:'contain', paddingLeft:15, paddingRight:15 }} />
                                <FormTextInput 
                                    title='VIN Number'
                                    value={this.state.updatedVin} 
                                    onChangeText={(x)=>this.setState({updatedVin:x})} 
                                    placeholder="Eg: 4Y1SL65848Z411439" 
                                />
                                
                                <View>
                                    <TouchableOpacity onPress={this.updateVIN} style={{backgroundColor:'#080880', borderRadius:6, padding:15, marginTop:45, paddingLeft:20, paddingRight:20, marginBottom:15, alignItems:'center'}}>
                                        <Text style={{color:'white', fontFamily: "ProximaNova-Bold"}}>Update VIN Number</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.setState({modalVIN: false, updatedVin: ''})} style={{ padding:15, paddingLeft:20, paddingRight:20, alignItems:'center'}}>
                                        <Text style={{color:'red', fontFamily: "ProximaNova-Bold"}}>Maybe later</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>


                        } />
                        

                </View>
            </SafeAreaView>
        )
    }
}

VehicleDetails = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(VehicleDetails))
export default VehicleDetails

const styles = StyleSheet.create({
    top:{
        backgroundColor:'#080880',
        padding:20,
        borderBottomLeftRadius:25,
        borderBottomRightRadius: 25,
    },
    viewTitle: {marginBottom:20},
    smolFont: {fontSize:11, marginBottom:5, color:'#6e6d6d',},
    bigFont: {fontFamily:'ProximaNova-Bold' },
    box:{ 
        backgroundColor:'#fff', padding:15, borderRadius:10, marginTop:10,
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowRadius: 3,
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.6,
            },
            android: {
                elevation: 3
            },
        }),
    },

})