import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,KeyboardAvoidingView, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/AntDesign"

import Header from '../components/Header.js'
import FormTextInput from '../components/FormTextInput'
import Button from '../components/Button'

const CONST_WIDTH = Dimensions.get('window').width;

class ForgotPassword extends React.Component{
    constructor(){
        super()
        this.state = {
          email: ''
        }
    }

    register = () => {
        this.props.navigation.navigate("RegisterScreen");
    }
    
    login = () => {
        this.props.navigation.navigate("LoginScreen");
    }
    
    forgotPassword = () => {
        let that = this;
    
        if(this.state.email == ''){
            Alert.alert("Oops!",'Please type in your email to reset your password')
            return;
        }
        firebase.auth().sendPasswordResetEmail(that.state.email)
            .then( (user) => {
                Alert.alert('An email has been sent to reset your password', "Please check your email")
                that.props.navigation.popToTop()

            }).catch((e) => {
                if(e.code === 'auth/invalid-email'){
                Alert.alert("Error", 'Please type in a valid email registered with SERV')
                }
                if(e.code === 'auth/user-not-found'){
                Alert.alert("Sorry", 'The email you entered is not registered with SERV')
                }
                
            })
      

    }

    
    render(){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#fafdff'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <KeyboardAvoidingView
                behavior={(Platform.OS === 'ios') ? "padding" : null} enabled
                style={{ flex: 1, }}
                >
                    <View style={{flex:1, padding:20, justifyContent:'center'}} >
                        <View style={{marginBottom:50}}>
                            <Text style={styles.mainTitle}>Things happen.</Text>
                            <Text style={styles.smolTitle}>Let us help you with your account. Please enter your email below and we will send you a reset link.</Text>
                        </View>

                        <FormTextInput 
                            title= 'EMAIL'
                            value={this.state.email}
                            placeholder= 'eg: myemail@gmail.com'
                            keyboardType='email-address'
                            onChangeText={(x) => this.setState({email:x})}
                        />

                        <View style={{height:15}} />

                        <Button 
                            onPress={this.forgotPassword}
                            title='Reset Password'
                            bgColor='#080880'
                            width = {CONST_WIDTH - 40}
                        />

                        <TouchableOpacity onPress={this.register} style={{marginTop:30,}}>
                        <Text style={{color:'#686868', textAlign:'center', fontFamily: "ProximaNova-Bold"}}>New here? <Text style={{color:'#080880',fontFamily: "ProximaNova-Bold",}}>Create a new account</Text></Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.login} style={{marginTop:20,}}>
                            <Text style={{color:'#686868',textAlign:'center', fontFamily: "ProximaNova-Bold"}}> Already have an account? <Text style={{fontFamily: "ProximaNova-Bold",color:"#080880",}}> Login now</Text> </Text>
                        </TouchableOpacity>

                    </View>
                </KeyboardAvoidingView>
                </View>
            </SafeAreaView>
        )
    }
}

ForgotPassword = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(ForgotPassword))
export default ForgotPassword

const styles = StyleSheet.create({
    mainTitle :{fontSize:30, fontFamily:'ProximaNova-Bold', marginBottom:10},
    smolTitle:{  fontSize:14},
})