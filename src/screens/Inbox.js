import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,RefreshControl,FlatList, Modal, Alert,Dimensions, TouchableWithoutFeedback, TouchableOpacity, ScrollView, Image, Button, TextInput, Linking} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'

import Icon from "react-native-vector-icons/Ionicons"
import FontIcon from "react-native-vector-icons/FontAwesome"

import Header from "../components/Header.js"
import announcement from '../assets/icons/announcement.png'
import update from '../assets/icons/updateApp.png'
import inbox from '../assets/icons/inbox.png'

class Inbox extends React.Component{

    render(){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                    
                    <Header backButton={false} headerTitle='Inbox' />

                    <ScrollView showsVerticalScrollIndicator={false} style={{padding:10}} >
                        <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('InboxAnnouncements')}>
                        <View style={styles.box}>
                            <View style={styles.iconView}>
                                <Image source={announcement} style={styles.icon}  />
                            </View>
                            <View>
                                <Text style={styles.topText}  >SERV Announcements</Text>
                                <Text style={styles.bottomText} >Stay updated on our latest news</Text>
                            </View>
                            <FontIcon name="angle-right" color='#000' size={20} style={{position:'absolute', right:15, bottom:15,}} />
                        </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('InboxNotifications')}>
                        <View style={styles.box}>
                            <View style={styles.iconView} >
                                <Image source={inbox} style={styles.icon}  />
                            </View>
                            <View >
                                <Text style={styles.topText} >Personal Notifications</Text>
                                <Text style={styles.bottomText} >Check your app notifications</Text>
                            </View>
                            <FontIcon name="angle-right" color='#000' size={20} style={{position:'absolute', right:15, bottom:15,}} />
                        </View>
                        </TouchableWithoutFeedback>
                        
                        <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('InboxUpdate')}>
                        <View style={styles.box}>
                            <View style={styles.iconView} >
                                <Image source={update} style={styles.icon}  />
                            </View>
                            <View>
                                <Text style={styles.topText} >SERV App Updates</Text>
                                <Text style={styles.bottomText} >Always keep your app updated</Text>
                            </View>
                            <FontIcon name="angle-right" color='#000' size={20} style={{position:'absolute', right:15, bottom:15,}} />
                        </View>
                        </TouchableWithoutFeedback>
                        <View style={{height: 25}} />
                    </ScrollView>
                    
                </View>
            </SafeAreaView>
        )
    }
}

Inbox = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(Inbox))
export default Inbox

const styles = StyleSheet.create({
    box: { flexDirection:'row', alignItems:'center', borderRadius:10,marginLeft:5, marginRight:5, marginTop:15, padding:20,backgroundColor:'#fff',
    ...Platform.select({
        ios: {
          shadowColor: '#000',
          shadowRadius: 2.22,
          shadowOffset: { width: 0, height: 1 },
          shadowOpacity: 0.22,
        },
        android: {
          elevation: 3
        },
       }),
    },
    topText :{fontSize:16, marginBottom: 6},
    bottomText:{ fontSize:13, color:'grey'},
    iconView:{width:40, height:40, marginRight:15, borderRadius:20, backgroundColor:'#efefef', alignItems:'center', justifyContent:'center'},
    icon:{height:28, width:28}
})