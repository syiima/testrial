import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Image,Alert, Dimensions, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/FontAwesome"

import Header from '../components/Header.js'
import BottomModal from "../components/BottomModal.js"
import HomeWorkshop from "./HomeWorkshop.js"

import service1 from '../assets/icons/workshop2.png'
import service2 from '../assets/icons/sos2.png'
import service3 from '../assets/icons/insurance2.png'
import battery from '../assets/icons/battery.png'

import other1 from '../assets/icons/rewards2.png'
import other2 from '../assets/icons/grocery2.png'
import other3 from '../assets/icons/bazaar2.png'
import other4 from '../assets/icons/online2.png'
import allianz from '../assets/logo/allianzLogo.png'
import sureplify from '../assets/logo/sureman.png'
import policystreet from '../assets/logo/policystreet.png'

class HomeServices extends React.Component{
    constructor(){
        super()
        this.state = {
            modalInsurance: false,
            modalVehicle:false,
        }
    }

    insuranceNavigation = (x) => {
        let LINK 
        if(x === 'Sureplify'){ 
            LINK = 'https://www.sureplify.com/#/?aff=SERV'
            firebase.analytics().logScreenView({
                screen_name: 'Insurance_sureplify',
                screen_class: 'Insurance_sureplify',
              })        } 
        if(x === 'PolicyStreet' ){
            LINK = 'https://www.policystreet.com/motor?utm_source=serv&utm_medium=referral&utm_campaign=anything'
            firebase.analytics().logScreenView({
                screen_name: 'Insurance_PolicyStreet',
                screen_class: 'Insurance_PolicyStreet',
              }) 
        }  
        if(x === 'Allianz'){
            LINK = "https://getquote.allianz.com.my/motor-online?utm_source=SERV&utm_medium=app&utm_campaign=stayathome"
            firebase.analytics().logScreenView({
                screen_name: 'Allianz',
                screen_class: 'Allianz',
              })
        }
        
        let uid = this.props.mobx_auth.FUID;


        firebase.database().ref('Insurance/' + uid ).push({
            opened: firebase.database.ServerValue.TIMESTAMP
        })
        this.props.navigation.navigate('WebviewInsurance', {url_link: LINK,category:'Insurance', company: x})
    }

    callbackModal = () => {this.setState({ modalVehicle:false }) }

    vehicleList = () => {
        let display
        let DATA = this.props.navigation.state.params.vehicleData

        if(DATA.length !== 0){
            display = DATA.map((item, index) => (
              <HomeWorkshop item={item} navigation={this.props.navigation} closeModal={this.callbackModal} />
            ))
        } else {
            display = <Text>Please add at least one vehicle first</Text>
        } 

        return display
    }


    checkVehicle = () => {
        this.props.navigation.navigate('WorkshopListing')
        // let DATA = this.props.navigation.state.params.vehicleData

        // if(DATA !== '' && DATA !== undefined){
        //     // this.setState({modalVehicle:true})  
        //     this.props.navigation.navigate('WorkshopListing')
        // } else {
        //     Alert.alert('No vehicle','Please add at least one vehicle first. Thank you!')
        // }
    }

    openBattery = () => {
        let LINK
        let UID = this.props.mobx_auth.FUID;
        
        if (this.props.mobx_config.config === 0){
            LINK = "https://staging.jualbaterikereta.com/mobile/booking?source=serv"
        } else {
            LINK = "https://jualbaterikereta.com/mobile/booking?source=serv"
        }

        firebase.database().ref(`Bateriku/${UID}/`).push({
            opened: firebase.database.ServerValue.TIMESTAMP
          })

        this.props.navigation.navigate('WebviewInsurance', {url_link: LINK,category:'Bateriku', company: 'Bateriku'})
    }

    render(){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                    <TouchableWithoutFeedback onPress={() => this.props.navigation.goBack()}>
                        <View style={styles.top}>
                            <Icon name="chevron-left" size={25} color='#000' style={{ marginLeft:10}}/>
                        </View>
                    </TouchableWithoutFeedback>
                
                    <ScrollView style={{padding:15, paddingTop:10}} showsVerticalScrollIndicator={false}>
                    <View style={{margin:5}}>
                    <Text style={{fontFamily: "ProximaNova-Bold",fontSize: 17}}>Vehicle Services</Text>
                    <View style={{flexDirection:'row', flexWrap:'wrap', justifyContent:'flex-start', marginTop:30, marginBottom:30,paddingBottom:15, borderBottomColor:'#ccc', borderBottomWidth:0.25}} >
                        <TouchableWithoutFeedback onPress={this.checkVehicle} >
                        <View style={styles.icon}>
                            <Image source={service1} style={styles.img} />
                            <Text style={styles.textIcon}>Workshop</Text>
                        </View>
                        </TouchableWithoutFeedback>
                        {/* <View style={styles.icon}>
                            <Image source={service3} style={styles.img} />
                            <Text style={styles.textIcon}>Vehicle Care</Text>
                        </View> */}
                        <TouchableWithoutFeedback onPress={() => this.setState({modalInsurance: true})}>
                        <View style={styles.icon}>
                            <Image source={service3} style={styles.img} />
                            <Text style={styles.textIcon}>Insurance & Road Tax</Text>
                        </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('RoadAssisstance')} >
                        <View style={styles.icon}>
                            <Image source={service2} style={styles.img} />
                            <Text style={styles.textIcon}>Road Assistance</Text>
                        </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress={this.openBattery} >
                        <View style={styles.icon}>
                            <Image source={battery} style={styles.img} />
                            <Text style={styles.textIcon}>Battery</Text>
                            <Text style={{...styles.textIcon, marginTop:0}}>Replacement</Text>
                        </View>
                        </TouchableWithoutFeedback>
                        {/* <View style={styles.icon}>
                            <Image source={service1} style={styles.img} />
                            <Text style={styles.textIcon}>Emergency</Text>
                        </View> */}
                    </View>


                    <Text style={{fontFamily: "ProximaNova-Bold",fontSize: 17}}>Others</Text>
                    <View style={{flexDirection:'row', flexWrap:'wrap',justifyContent:'flex-start', marginTop:40, marginBottom:30}} >
                        {/* <View style={styles.icon}>
                            <Image source={service1} style={styles.img} />
                            <Text style={styles.textIcon}>Offers</Text>
                        </View> */}
                        <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Rewards')} >
                        <View style={styles.icon}>
                            <Image source={other1} style={styles.img} />
                            <Text style={styles.textIcon}>Rewards</Text>
                        </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('MarketplaceDetails', {type: 'Grocery Delivery'})} >
                        <View style={styles.icon}>
                            <Image source={other2} style={styles.img} />
                            <Text style={styles.textIcon}>Groceries</Text>
                        </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('MarketplaceDetails', {type: 'eBazaar'})} >
                        <View style={styles.icon}>
                            <Image source={other3} style={styles.img} />
                            <Text style={styles.textIcon}>e-Bazaar</Text>
                        </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('MarketplaceDetails', {type: 'Online Services'})} >
                        <View style={styles.icon}>
                            <Image source={other4} style={styles.img} />
                            <Text style={styles.textIcon}>Online Services</Text>
                        </View>
                        </TouchableWithoutFeedback>
                        
                    </View>
                    </View>
                    </ScrollView>

                    <BottomModal
                        modalName={this.state.modalVehicle}
                        onPress={() => this.setState({modalVehicle:false})}
                        style={styles.innerModal}
                        children = {
                            <View>          
                                <View style={{padding:15,borderBottomColor:'#080880', alignItems:'center',}}>
                                    <Text style={{textAlign:'center', color:'#080880', fontFamily: "ProximaNova-Bold", fontSize:18}}>Select Vehicle</Text>
                                </View>
                                <View style={{margin:15, backgroundColor:'grey', borderRadius:50, alignItems:'center', padding:3 }}>
                                    <Text style={{color:'white'}}>My Vehicles</Text>
                                </View>
                                
                                <ScrollView style={{height:Dimensions.get('window').height / 2}}>
                                    {this.vehicleList()}
                                </ScrollView>
                      
                          </View>
                        }
                    />
                    
                    
                    <BottomModal
                        modalName={this.state.modalInsurance}
                        onPress={() => this.setState({modalInsurance:false})}
                        style={styles.innerModal}
                        children = {
                            <View>          
                                <View style={{padding:15, alignItems:'center',}}>
                                    <Text style={{fontSize:20, fontFamily: "ProximaNova-Bold", marginBottom:10, textAlign:'center', color:'#080880',}}>Insurance & Road Tax</Text>
                                    <Text style={{fontFamily:'ProximaNova-Bold', fontSize:12, textAlign:'center', }} >Our Insurance & Road Tax service helps you to renew or check on your insurance from the comfort of your home.</Text>
                                </View>
                                <View style={{flexDirection:'row',  justifyContent:'space-around', alignItems:'flex-start', display:'flex', flexWrap:'wrap', marginTop:15, marginBottom:15 }}>
                                        {/* <TouchableWithoutFeedback  onPress={() => this.setState({modalInsurance:false}, () =>  this.insuranceNavigation('Sureplify') )}>
                                        <View style={styles.jenisService}  >
                                            <Image source={sureplify} style={{height:50, width:50,resizeMode:'contain'}}/>
                                            <Text style={styles.textS}>Sureplify</Text>
                                        </View>
                                    </TouchableWithoutFeedback> */}
                                    <TouchableWithoutFeedback  onPress={() => this.setState({modalInsurance:false},() => this.insuranceNavigation('PolicyStreet') )}>
                                        <View style={styles.jenisService}  >
                                            <Image source={policystreet} style={{height:50, width:50,resizeMode:'contain'}}/>
                                            <Text style={styles.textS}>PolicyStreet</Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                    <TouchableWithoutFeedback  onPress={() => this.setState({modalInsurance:false},() => this.insuranceNavigation('Allianz') )}>
                                        <View style={styles.jenisService}  >
                                            <Image source={allianz} style={{height:50, width:50,resizeMode:'contain'}}/>
                                            <Text style={styles.textS}>Allianz</Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                      
                          </View>
                        }
                    />


                </View>
            </SafeAreaView>
        )
    }
}

HomeServices = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(HomeServices))
export default HomeServices

const styles = StyleSheet.create({
    top:{
        padding:20,
    },
    icon:{
        width:(Dimensions.get('window').width - 40) /4 , 
        marginBottom:20,
        // alignItems:'center',
        // justifyContent:'center',
        paddingLeft:5, 
        paddingRight:5,
    },
    img:{
        resizeMode:'contain', alignSelf:'center',
        height:60, width:60
    }, 
    textIcon:{
        marginTop:4,
        textAlign:'center',
        fontFamily: "ProximaNova-Bold",
        fontSize:11
    },
    innerModal: {     
        borderTopLeftRadius: 10,
        borderTopRightRadius:10,
        width:Dimensions.get('window').width,
        position:'absolute',
        bottom:0,
        backgroundColor: '#fff',
            ...Platform.select({
                ios: {
                    shadowColor: '#000',
                    shadowRadius: 2,
                    shadowOffset: { width: 0, height: 3 },
                    shadowOpacity: 0.6,
                },
                android: {
                    elevation: 2
                },
            }),
    },
    jenisService:{
        // backgroundColor:'white',
        marginBottom:15,
        width:Dimensions.get('window').width / 3.75,
        height:Dimensions.get('window').width / 3.6,
        alignItems:'center',
        justifyContent:'center',
      },
      textS: {marginTop:15, textAlign:'center', fontFamily:'ProximaNova-Bold', fontSize:12},

})