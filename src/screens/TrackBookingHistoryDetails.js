import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,TextInput, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/AntDesign"

import Header from '../components/Header.js'

class BookingHistoryDetails extends React.Component{
  constructor(){
    super()
    this.state = {
      editMode: false,
      mileage: '',
      data: '',
      vehicletype: ''

    }
  }
  
  componentDidMount(){
    let ITEM = this.props.navigation.state.params.item
    let PLATE =  ITEM.customer_carplate
    let ID = ITEM.bookID
    this.fetchHistory(PLATE, ID)
    this.setState({vehicletype : ITEM.vehicletype, })
  }
  
  fetchHistory = (x,y) => {
    let PLATE = x 
    let ID = y 

    firebase.database().ref(`plate_number/${PLATE}/book_history/${ID}`).once('value').then((snp) => {
      if(snp.exists()){
        let VAL = snp.val()
        let MILE
        if(VAL.checklist.customer_car_mileage !== undefined){
          MILE = VAL.checklist.customer_car_mileage
        } else { MILE = ''}

        VAL.customer_car_mileage = MILE
        VAL.customer_carplate = PLATE 
        VAL.bookID = ID

        this.setState({data: VAL, mileage: MILE})
      }
    })
  }

  displayService = () => {
    let DATA = this.props.navigation.state.params.quotation
    let display

    display = DATA.map((item, ind) => (
        <View style={{flexDirection:'row',justifyContent:'space-between', alignItems:'center', width:'88%', flexShrink:1, padding:5 }}>
          <View style={{flexDirection:'row', alignItems:'center', width:'70%', marginRight:5}}>
            <View  style={{flexShrink: 1}}>
              <Text style={{fontFamily: "ProximaNova-Bold",marginBottom:5}}>{item.item}</Text>
              {item.description !== '' ? <Text >{item.description}</Text> : null}
              {item.warranty !== undefined ? 
              <View style={{backgroundColor:'#080880', padding:4, paddingLeft:8, paddingRight:8, borderRadius:10, marginTop:5, alignSelf:'flex-start'}}>
                <Text style={{color:'white', fontFamily:'ProximaNova-Bold', textAlign:'center'}} >Warranty: {item.warranty}</Text>
                </View>
                  : null}
            </View>
          </View>

            <View style={{alignItems:'center', flexDirection:'row', alignSelf:'center', width:'35%', marginLeft:10}}>
              <Text style={{fontSize:14,}} >RM </Text>
              <Text style={{fontSize:20}}>{parseFloat(item.price).toFixed(2)} </Text>
            </View>
        </View>
        ))
  
    
    return display
  }

  update = () => {
    this.setState({editMode: false})
    let item = this.state.data
    let PLATE =  item.customer_carplate
    let ID = item.bookID
    let MILEAGE = this.state.mileage

    
    firebase.database().ref(`plate_number/${PLATE}/book_history/${ID}/checklist`)
        .update({customer_car_mileage: MILEAGE})
        .then(() => {
            this.fetchHistory(PLATE, ID)
        })

  }

    render(){
      let item = this.state.data

      let details 
      if(this.state.vehicletype === 'Motorcycle'){
        if(this.state.editMode === true){
          details = 
          <View>
            <TouchableWithoutFeedback onPress={this.update} >
            <View style={{flexDirection: 'row', alignItems:'center', justifyContent:'space-between',marginBottom:15}}>
              <Text style={{fontFamily: "ProximaNova-Bold",fontSize:16, }}>Service Details </Text>
              <View style={{flexDirection:'row', alignItems:'center'}}>
                <Text style={{fontSize:16, }}>Done </Text>
                <Icon name='check' size={20}/>
              </View>

            </View>
            </TouchableWithoutFeedback>
            <View style={styles.box}>
                <View style={styles.infoBox}>
                      <Text style={{flex:1,color:'#7a7a7a' }}>Date</Text>
                      <Text style={{fontFamily: "ProximaNova-Bold",flex:3, color:'#7a7a7a'}}>{item.customer_day} {item.customer_month} {item.customer_year}</Text>
                  </View>
                  <View style={styles.infoBox}>
                      <Text style={{flex:1, color:'#7a7a7a' }}>Time</Text>
                      <Text style={{fontFamily: "ProximaNova-Bold",flex:3,color:'#7a7a7a' }}>{item.customer_time}</Text>
                  </View>
                  <View style={styles.infoBox}>
                      <Text style={{flex:1, color:'#7a7a7a' }}>Location</Text>
                      <Text style={{fontFamily: "ProximaNova-Bold",flex:3, color:'#7a7a7a'}}>{item.customer_nameplace !== '' && item.customer_nameplace !== undefined ? item.customer_nameplace :'Not Available'  }</Text>
                </View>
                <View style={styles.infoBox}>
                    <Text style={{flex:1,  }}>Mileage</Text>
                    <TextInput 
                      value={this.state.mileage}
                      keyboardType="numeric"
                      onChangeText={(x) => this.setState({mileage: x})}
                      style={{ padding:(Platform.OS === 'ios') ? 10 : 5, borderWidth:1, borderColor:'#a7a7a7',borderRadius:5 ,borderWidth: 0.5, flex:3, fontFamily: "ProximaNova-Bold"}}
                      autoCorrect={false}
                      autoCapitalize = "none" 
                      underlineColorAndroid="transparent" 
                    /> 
                </View>
            </View>
          </View>
        } else {
          details = 
          <View>
            <TouchableWithoutFeedback onPress={() => this.setState({editMode: true}) } >
            <View style={{flexDirection: 'row', alignItems:'center', justifyContent:'space-between',marginBottom:15}}>
              <Text style={{fontFamily: "ProximaNova-Bold",fontSize:16, }}>Service Details </Text>
              <View style={{flexDirection:'row', alignItems:'center'}}>
                <Text style={{fontSize:16, }}>Edit </Text>
                <Icon name='edit' size={20}/>
              </View>

            </View>
            </TouchableWithoutFeedback>
            <View style={styles.box}>
                <View style={styles.infoBox}>
                      <Text style={{flex:1, }}>Date</Text>
                      <Text style={{fontFamily: "ProximaNova-Bold",flex:3}}>{item.customer_day} {item.customer_month} {item.customer_year}</Text>
                  </View>
                  <View style={styles.infoBox}>
                      <Text style={{flex:1,  }}>Time</Text>
                      <Text style={{fontFamily: "ProximaNova-Bold",flex:3 }}>{item.customer_time}</Text>
                  </View>
                  <View style={styles.infoBox}>
                      <Text style={{flex:1,  }}>Location</Text>
                      <Text style={{fontFamily: "ProximaNova-Bold",flex:3}}>{item.customer_nameplace !== '' && item.customer_nameplace !== undefined ? item.customer_nameplace :'Not Available'  }</Text>
                </View>
                <View style={styles.infoBox}>
                    <Text style={{flex:1,  }}>Mileage</Text>
                    <Text style={{fontFamily: "ProximaNova-Bold",flex:3 }}>{this.state.mileage !== '' ? this.state.mileage : 'Not Available'}</Text>
                </View>
            </View>
          </View>
        }
        
      } else {
        details = 
        <View>
          <Text style={styles.mainTitle}>Service Details </Text>
          <View style={styles.box}>
              <View style={styles.infoBox}>
                    <Text style={{flex:1, }}>Date</Text>
                    <Text style={{fontFamily: "ProximaNova-Bold",flex:3}}>{item.customer_day} {item.customer_month} {item.customer_year}</Text>
                </View>
                <View style={styles.infoBox}>
                    <Text style={{flex:1,  }}>Time</Text>
                    <Text style={{fontFamily: "ProximaNova-Bold",flex:3 }}>{item.customer_time}</Text>
                </View>
                <View style={styles.infoBox}>
                    <Text style={{flex:1,  }}>Location</Text>
                    <Text style={{fontFamily: "ProximaNova-Bold",flex:3}}>{item.customer_nameplace !== '' && item.customer_nameplace !== undefined ? item.customer_nameplace :'Not Available'  }</Text>
              </View>
              <View style={styles.infoBox}>
                  <Text style={{flex:1,  }}>Mileage</Text>
                  <Text style={{fontFamily: "ProximaNova-Bold",flex:3 }}>{this.state.mileage !== '' ? this.state.mileage : 'Not Available'}</Text>
              </View>
          </View>
        </View>
      }

      return(
        <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
        <View style={{flex:1,backgroundColor:'#fafdff', }}>
              <Header backButton = {true}  headerTitle = 'History Details' onPress={() => this.props.navigation.goBack()} />
                  
                  <ScrollView contentContainerStyle={{padding:17}} showsVerticalScrollIndicator={false}>
                      <View style={{margin:3}}>
                      <Text style={styles.mainTitle}>Vehicle Details </Text>
                      <View style={styles.box}>
                          <View style={styles.infoBox}>
                                <Text style={{flex:1, }}>Plate</Text>
                                <Text style={{fontFamily: "ProximaNova-Bold",flex:3}}>{item.customer_carplate}</Text>
                            </View>
                            <View style={styles.infoBox}>
                                <Text style={{flex:1,  }}>Model</Text>
                                <Text style={{fontFamily: "ProximaNova-Bold",flex:3 }}>{item.customer_carmake} {item.customer_carmodel}</Text>
                            </View>
                            
                            <View style={styles.infoBox}>
                                <Text style={{flex:1,  }}>Year</Text>
                                <Text style={{fontFamily: "ProximaNova-Bold",flex:3}}>{item.customer_caryear}</Text>

                          </View>
                      </View>

                      {details}

                      <Text style={styles.mainTitle}>Services Done </Text>
                      <View style={styles.box}>
                          
                      
                          {this.displayService()}
                      </View>
                      </View>

                  </ScrollView>
                  
              </View>
          </SafeAreaView>
      )
    }
}

BookingHistoryDetails = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(BookingHistoryDetails))
export default BookingHistoryDetails

const styles = StyleSheet.create({
    mainTitle :{ fontFamily: "ProximaNova-Bold",fontSize:16, marginBottom:15},
    infoBox :{flexDirection:'row',alignItems:'center', marginBottom:5},
    box :{
        backgroundColor:'#fff',
        borderRadius:10,
        marginBottom:30,
        // marginTop:10,
        padding:15,
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 3 },
              shadowOpacity: 0.3,
            },
            android: {
              elevation: 3
            },
          }),
    },
    desc :{fontFamily: "ProximaNova-Bold", marginBottom:5},
})