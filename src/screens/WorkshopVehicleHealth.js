import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,TouchableWithoutFeedback, ImageBackground,RefreshControl,FlatList, Alert,Dimensions, TouchableOpacity, ScrollView, Image, Button, TextInput, Picker} from 'react-native';
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/FontAwesome"
import { PieChart, ProgressCircle } from 'react-native-svg-charts'
import Moment from 'moment';

import Loader from "../components/Loader";
import emptyPage from '../assets/icons/emptyBooking.png'
import Header from '../components/Header.js'

class WorkshopVehicleHealth extends React.Component{
    constructor(){
        super()
        this.state = {
            car_health: '',
            refreshing: false,
            screen:1,
      
            upah_data:'',
            date: '',
            car_info: '',
      
            finalPercent: 0, finalInt : 0, finalUH: 0, finalT:0,
      
            underV:'',
            underH:'',
            tyreS:'',
      
            isLoading:true,
            collapsed: true,
        }
    }

    componentDidMount(){
        this.fetchVehicle()
    }

    fetchVehicle(){
        let that = this
        let FUID = this.props.mobx_auth.FUID
        let PLATE = this.props.mobx_carkey.PLATE
        let VTYPE = this.props.mobx_carkey.VTYPE;
        let REF 

        let DATA = [];
        let DATA2 = [];
        let DATA3 = [];
        let DATA4 = [];
        
     
        firebase.database().ref(`plate_number/${PLATE}/car_health`).once('value', (snapshot) => {
            if(snapshot.exists()){
                let x = snapshot.val()

                if(x.customer_car_mileage !== undefined){
                    let keys = Object.keys(snapshot.val())
                    let sumT = 0
                    let sumInt = 0
                    let sumUV = 0
                    let sumUH = 0
                    let sum = 0

                    let CAR_INFO = {
                        VIN_number: snapshot.val().VIN_number,
                        customer_car_mileage: snapshot.val().customer_car_mileage,
                        customer_next_car_mileage:snapshot.val().customer_next_car_mileage,
                        next_service_date:snapshot.val().next_service_date,
                        roadtax_expiry_date:snapshot.val().roadtax_expiry_date,
                        extra_notes: snapshot.val().extra_notes !== undefined ? snapshot.val().extra_notes : 'No notes to display',
                    }

                    keys.forEach((snp) => {
                        if(snp === 'windshield' || snp === 'wiper' || snp === 'lights_exterior' || snp === 'lights_interior'){
                        // console.warn("apa", snp);
                        let a = {}
                        let NAME = snp
                        if(NAME !== 'lights_interior' || NAME !== 'lights_exterior') {
                            a.name = NAME
                        }
                        if(NAME === 'lights_interior'){
                            a.name = 'Interior Lights';
                        }
                        if(NAME === 'lights_exterior'){
                            a.name = 'Exterior Lights';
                        }
        
                        if(snapshot.val()[snp] === 'good'){ a.condition = 5}
                        if(snapshot.val()[snp] === 'worn'){a.condition = 3}
                        if(snapshot.val()[snp] !== 'good' && snapshot.val()[snp] !== 'worn') {a.condition = parseFloat(snapshot.val()[snp]);}
        
                        
                            sum += parseInt(a.condition)
                            sumInt += parseInt(a.condition)
                        DATA.unshift(a)
        
                        }
        
                        if(snp === 'ac' || snp === 'brake_pad' || snp === 'brake_lines' || snp === 'steering_system' || snp === 'cv_boots' || snp === 'exhaust'){
                        let b = {}
                        let NAME = snp
                        if(NAME !== 'ac') {
                            b.name = NAME
                        }
                        if(NAME === 'ac'){
                            b.name = 'Aircond Operation';
                        }
                        if(snapshot.val()[snp] === 'good'){ b.condition = 5}
                        if(snapshot.val()[snp] === 'worn'){b.condition = 3}
                        if(snapshot.val()[snp] !== 'good' && snapshot.val()[snp] !== 'worn') {b.condition = parseFloat(snapshot.val()[snp]);}
        
                            sum += parseInt(b.condition)
                            sumUV += parseInt(b.condition)
                        // }
        
                        DATA2.unshift(b)
        
                        }
        
                        if(snp === 'engine_oil' || snp === 'brake_fluid' || snp === 'power_steering' || snp === 'transmission_fluid' || snp === 'washer_fluid' || snp === 'belts_hoses' || snp === 'coolant' || snp === 'radiator' || snp === 'radiator_hose' || snp === 'air_filter' || snp === 'cabin_filter' || snp === 'battery'){
                        let c = {}
                        let NAME = snp
                        if(NAME !== 'belts_hoses' ) {
                            c.name = NAME
                        }
                        if(NAME === 'belts_hoses'){
                        c.name = 'Belts & Hoses';
                        }
                        if(snapshot.val()[snp] === 'good'){ c.condition = 5}
                        if(snapshot.val()[snp] === 'worn'){c.condition = 3}
                        if(snapshot.val()[snp] !== 'good' && snapshot.val()[snp] !== 'worn') {c.condition = parseFloat(snapshot.val()[snp]);}
        
                            sum += parseInt(c.condition)
                            sumUH += parseInt(c.condition)
        
                        DATA3.unshift(c)
                        // console.warn('hmm sum');
        
                        }
                        if(snp === 'tyre_left_front' || snp === 'tyre_left_rear' || snp === 'tyre_right_front' || snp === 'tyre_right_rear'){
                        let d = {}
                        let NAME = snp
                        if(NAME !== 'belts_hoses' ) {
                            d.name = NAME
                        }
                        if(snapshot.val()[snp] === 'good'){ d.condition = 5}
                        if(snapshot.val()[snp] === 'worn'){d.condition = 3}
                        if(snapshot.val()[snp] !== 'good' && snapshot.val()[snp] !== 'worn') {d.condition = parseFloat(snapshot.val()[snp]);}
        
                        
                            sum += parseInt(d.condition)
                            sumT += parseInt(d.condition)
                        
                        // }
                        DATA4.unshift(d)
        
                        }
        
                    })
        
                    finalSum = parseFloat((sum / (26 * 5) * 100).toFixed(0))
                    finalSumUV = parseFloat((sumUV / (6 * 5) * 100).toFixed(0))
                    finalSumUH = parseFloat((sumUH / (12 * 5) * 100).toFixed(0))
                    finalSumT = parseFloat((sumT / (4 * 5) * 100).toFixed(0))
                    finalSumInt = parseFloat((sumInt / (4 * 5) * 100).toFixed(0))
                
                
                    that.setState({
                        upah_data: DATA,
                        underV: DATA2,
                        underH:DATA3,
                        tyreS:DATA4,
                        car_info: CAR_INFO,
                        screen: 2,
                        refreshing: false,
                        isLoading:false,

                        finalPercent : finalSum,
                        finalInt: finalSumInt,
                        finalUV: finalSumUV,
                        finalUH: finalSumUH,
                        finalT: finalSumT,
                    })

                    this.rearrange();

                } else {
                    that.setState({refreshing: false, isLoading: false, screen:1})
                }

            
            } else{
                that.setState({refreshing:false, isLoading: false, screen:1})
            }
        })

        
    }

    rearrange = () =>{
        //arrange alphabetically
        var dataToArrange = this.state.upah_data;
    
        dataToArrange.sort(function(a, b) {
          var nameA = a.name.toUpperCase(); // ignore upper and lowercase
          var nameB = b.name.toUpperCase(); // ignore upper and lowercase
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }
    
          // names must be equal
          return 0;
        });
    
        this.setState({finaldata: dataToArrange})
    }

    Refreshplz = () => {
        this.setState({refreshing: true});
        this.fetchVehicle();
    }
      

    //==== render ===== 

    renderItem = ({item}) => (
        <View style={styles.circleBox}>
            <Text style={{textAlign:'center',  marginRight: 5}}>
            {item.name !== undefined ?
                item.name.split('_').map((string,index) => (<Text key={index}>{string.charAt(0).toUpperCase() + string.slice(1)}{' '}</Text>)) :
                null
            }
            </Text>
            {item.condition === 'good' || item.condition === 5 ?
            <View style={{flexDirection:'row', alignItems:'center'}}>
                <Icon name="circle" color="#15ba12" size={14} style={{marginRight:4}} />
                <Icon name="circle" color="#15ba12" size={14} style={{marginRight:4}} />
                <Icon name="circle" color="#15ba12" size={14} style={{marginRight:4}} />
                <Icon name="circle" color="#15ba12" size={14} style={{marginRight:4}} />
                <Icon name="circle" color="#15ba12" size={14} style={{marginRight:4}} />

            </View>
            :

            item.condition === 'worn' || item.condition === 2 || item.condition === 3 ||  item.condition === 4 ?
            <View style={{flexDirection:'row', alignItems:'center'}}>
                <Icon name="circle" color="#e9c91d" size={14} style={{marginRight:4}} />
                <Icon name="circle" color="#e9c91d" size={14} style={{marginRight:4}} />
                <Icon name="circle" color="#e9c91d" size={14} style={{marginRight:4}} />
                <Icon name="circle-o" color="#000" size={14} style={{marginRight:4}} />
                <Icon name="circle-o" color="#000" size={14} style={{marginRight:4}} />

            </View>
            :
            item.condition === 1 ?
                <View style={{flexDirection:'row', alignItems:'center'}}>
                <Icon name="circle" color="tomato" size={14} style={{marginRight:4}} />
                <Icon name="circle-o" color="#000" size={14} style={{marginRight:4}} />
                <Icon name="circle-o" color="#000" size={14} style={{marginRight:4}} />
                <Icon name="circle-o" color="#000" size={14} style={{marginRight:4}} />
                <Icon name="circle-o" color="#000" size={14} style={{marginRight:4}} />
                </View>
            :

                <View style={{flexDirection:'row', alignItems:'center'}}>
                <Icon name="circle-o" color="#000" size={14} style={{marginRight:4}} />
                <Icon name="circle-o" color="#000" size={14} style={{marginRight:4}} />
                <Icon name="circle-o" color="#000" size={14} style={{marginRight:4}} />
                <Icon name="circle-o" color="#000" size={14} style={{marginRight:4}} />
                <Icon name="circle-o" color="#000" size={14} style={{marginRight:4}} />
                </View>

        }
        </View>
    )

    render_color = (X) => {
        let ITEM = X
        let color_disp

            if(ITEM <=20 ){ color_disp = 'red'}
            if(ITEM >= 21){color_disp = 'rgb(233, 201, 29)'}
            if(ITEM >= 80){color_disp = '#008000'}

        return color_disp

    }


    render(){

        let content;

        if(this.state.screen === 1){
            content=
                <View style={{flex:1}}>

                    <Header backButton = {true}  headerTitle = 'Health & Stats' onPress={() => this.props.navigation.popToTop()} />

                    <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                        <Image source={emptyPage} style={{height: 180, width:180, resizeMode:'contain'}} />
                        <Text style={{fontSize:22, fontFamily: "ProximaNova-Bold", marginTop:15, marginBottom:10}}>No History</Text>
                        <Text>Get your vehicle serviced to see Health & Stats</Text>
                    </View>
                </View>
        }
        if(this.state.screen === 2){
            content=
            <ScrollView
              style={{flex:1,}} refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this.Refreshplz}
                />
            }
              showsVerticalScrollIndicator={false}>
      
                <View style={styles.top}>
                    <TouchableWithoutFeedback onPress={() => this.props.navigation.popToTop()}>
                        <View style={{flexDirection:'row', alignItems:'center', margin:15}}>
                            <Icon name="chevron-left" color="white" size={25}  style={{marginRight:20, marginLeft:10}}/>
                        <Text style={{color:'#fff', fontWeight:'bold', fontSize:20, }}>Health & Stats</Text>
                        </View>
                    </TouchableWithoutFeedback >
        
        
                    <View style={{alignItems:'center', justifyContent:'center', marginTop:15, paddingBottom:15}}>
            
                        <View >
                            <ProgressCircle
                                style={ styles.progCircle }
                                progress={this.state.finalPercent /100}
                                strokeWidth={22}
                                startAngle={ -Math.PI * 0.75 }
                                endAngle={ Math.PI * 0.75 }
                                progressColor={this.render_color(this.state.finalPercent)}
                            />
                            <View style={{position:'absolute', top:'40%', left:'15%', }} >
                                <Text style={{fontSize:38,textAlign:'center', fontFamily: "ProximaNova-Bold", color:'#fff'}}>{this.state.finalPercent}%</Text>
                                <Text style={{color:'#fff', fontWeight:'bold', fontSize:13, marginTop:6,}}>Health Score</Text>
                            </View>
                            
                        </View>
                    </View>
        
                </View>
      
                <View style={{margin:15}}>
                    <View style={{...styles.box, backgroundColor:'#fb8900', marginTop:5, marginBottom:15}} >
                    <Text style={{fontFamily: "ProximaNova-Bold",color:'#fff', fontSize:14, marginBottom:5,}}>Next Appointment</Text>
                        <Text style={{color:'#fff',fontWeight:'bold', fontSize:20}} >{this.state.car_info.next_service_date}</Text>
                    </View>

                    <View style={{flexDirection:'row',justifyContent:'space-between', marginBottom:15}}>
                        <View style={{...styles.mileBox, backgroundColor:'#fff'}} >
                            <Text style={{textAlign:'center', fontWeight:'bold'}} >Total Mileage</Text>
                            <Text style={{marginTop:10,fontSize:24,marginBottom:3, fontWeight:'bold'}} >{this.state.car_info.customer_car_mileage}</Text>
                            <Text style={{ fontWeight:'bold'}} >KM</Text>
                        </View>
                        <View style={{...styles.mileBox, backgroundColor:'#080880'}} >
                            <Text style={{color:'#fff', textAlign:'center', fontWeight:'bold'}} >Next Service Mileage</Text>
                            <Text style={{marginTop:10,color:'#fff',fontSize:24,marginBottom:3, fontWeight:'bold'}}>{this.state.car_info.customer_next_car_mileage}</Text>
                            <Text style={{color:'#fff',  fontWeight:'bold'}} >KM</Text>
                        </View>
                    </View>

                    <View style={{...styles.box, backgroundColor:'#fff'}} >
                        <View style={{flexDirection:'row', justifyContent:'space-between', marginBottom:15}}>
                            <Text style={{fontWeight:'bold'}} >Extra Notes</Text>
                            <Text style={{fontStyle:'italic',fontSize:13, color:'#080880'}} >Last Updated: {Moment(this.state.car_info.timestamp).format('DD-MMM-YYYY')} </Text>
                        </View>
                        {this.state.car_info.extra_notes !== ''? <Text style={{fontWeight:'bold', color:'#080880'}} >{this.state.car_info.extra_notes} </Text> : <Text style={{fontWeight:'bold', color:'#080880'}}>No extra notes </Text> }

                    </View>


                    <Text style={{fontFamily: "ProximaNova-Bold",textAlign:'center', fontSize:20, marginTop:10,}}>SUMMARY</Text>


                    <View style={styles.innerContent}>
                            <View style={ styles.smolCircle }>
                            <ProgressCircle
                                style={styles.insideC}
                                progress={this.state.finalInt /100}
                                strokeWidth={4}
                                progressColor={this.render_color(this.state.finalInt)}
                            />
                            <View style={styles.percentS}>
                                <Text style={{fontFamily:'ProximaNova-Bold'}} >{this.state.finalInt}%</Text>
                            </View>
                            </View>
                            <View style={{ flexShrink:1,flex:3,marginRight:5}} >
                                <Text style={{ fontFamily: "ProximaNova-Bold",marginBottom:8, fontSize:16 }}>Interior / Exterior</Text>
                                <Text style={{fontSize:12}}>Simple status check for your overall vehicle condition from both inside and outside</Text>
                            </View>
                        </View>
                    <View style={styles.innerContent}>
                        <View style={ styles.smolCircle }>
                        <ProgressCircle
                            style={styles.insideC}
                            progress={this.state.finalUV /100}
                            strokeWidth={4}
                            progressColor={this.render_color(this.state.finalUV )}
                        />
                        <View style={styles.percentS}>
                            <Text style={{fontFamily:'ProximaNova-Bold'}} >{this.state.finalUV}%</Text>
                        </View>
                        </View>
                        <View style={{ flexShrink:1,flex:3,marginRight:5}} >
                            <Text style={{ fontFamily: "ProximaNova-Bold", fontSize:16,marginBottom:8,  }}>Under Vehicle</Text>
                            <Text style={{fontSize:12}}>Status check for parts, components and structure below your vehicle</Text>
                        </View>
                    </View>
                    <View style={styles.innerContent}>
                        <View style={ styles.smolCircle }>
                        <ProgressCircle
                            style={styles.insideC}
                            progress={this.state.finalUH /100}
                            strokeWidth={4}
                            progressColor={this.render_color(this.state.finalUH)}
                        />
                        <View style={styles.percentS}>
                            <Text style={{fontFamily:'ProximaNova-Bold'}}>{this.state.finalUH}%</Text>
                        </View>
                        </View>
                        <View style={{ flexShrink:1,flex:3,marginRight:5}} >
                            <Text style={{ fontFamily: "ProximaNova-Bold", marginBottom:8, fontSize:16 }}>Under Hood</Text>
                            <Text style={{fontSize:12}}>Condition of your vehicle's internal engine and parts that is important for your vehicle to run</Text>
                        </View>
    
                    </View>
                    <View style={{...styles.innerContent, marginBottom:5}}>
                            <View style={ styles.smolCircle }>
                            <ProgressCircle
                                style={styles.insideC}
                                progress={this.state.finalT /100}
                                strokeWidth={4}
                                progressColor={this.render_color(this.state.finalT)}
                            />
                            <View style={styles.percentS}>
                                <Text style={{fontFamily:'ProximaNova-Bold'}}>{this.state.finalT}%</Text>
                            </View>
                            </View>
                            <View style={{ flexShrink:1,flex:3,marginRight:5}} >
                                <Text style={{ fontFamily: "ProximaNova-Bold",marginBottom:8,  fontSize:16 }}>Tyre</Text>
                                <Text style={{fontSize:12}}>Your tyre wear & tear condition, protecting your vehicle from the road</Text>
                            </View>
                        </View>
      
      
                    <Text style={{ fontFamily: "ProximaNova-Bold",textAlign:'center', fontSize:20, marginTop:20, marginBottom:15}}>DETAILS</Text>
            
                    <View style={styles.outerBox}>
                        <View style={styles.sideText}>
                            <Text numberOfLines={1} style={styles.innerText} >INTERIOR / EXTERIOR</Text>
                        </View>
                        <View style={styles.boxInfo}>
                            <FlatList
                                data={this.state.finaldata}
                                listKey="interior"
                                contentContainerStyle={styles.flatStyle}
                                renderItem= {this.renderItem}
                            />
                        </View>
            
                    </View>
            
                    <View style={styles.outerBox}>
                        <View style={styles.sideText}>
                            <Text numberOfLines={1} style={styles.innerText} >UNDERVEHICLE</Text>
                        </View>
                        <View style={styles.boxInfo}>
                        <FlatList
                            data={this.state.underV}
                            listKey="undervehicle"
                            contentContainerStyle={styles.flatStyle}
                            renderItem= {this.renderItem}
                        />
                        </View>
            
                    </View>
            
                    <View style={styles.outerBox}>
                        <View style={styles.sideText}>
                            <Text numberOfLines={1} style={styles.innerText}  >UNDERHOOD</Text>
                        </View>
                        <View style={styles.boxInfo}>
                        <FlatList
                            data={this.state.underH}
                            listKey="underhood"
                            contentContainerStyle={styles.flatStyle}
                            renderItem= {this.renderItem}
                        />
                        </View>
            
                    </View>
            
                    <View style={styles.outerBox}>
                        <View style={styles.sideText}>
                        <Text style={{color:'black', fontFamily: "ProximaNova-Bold", transform: [{ rotate: '270deg'}] , width:50}} >TYRE</Text>
                        </View>
                        <View style={styles.boxInfo}>
                        <FlatList
                            data={this.state.tyreS}
                            listKey="tyre"
                            contentContainerStyle={styles.flatStyle}
                            renderItem= {this.renderItem}
                        />
                    </View>
      
                </View>
                </View>
              </ScrollView>
      
              }
      
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                
                <Loader loading={this.state.isLoading} />

                {content}

                </View>
            </SafeAreaView>
        )
    }
}

WorkshopVehicleHealth = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(WorkshopVehicleHealth))
export default WorkshopVehicleHealth

const styles = StyleSheet.create({
    top:{
        backgroundColor:'#080880',
        borderBottomLeftRadius:50,
        width:'100%'
    },
    topempty:{
        backgroundColor:'#080880',
        padding:20,
        borderBottomLeftRadius:50,
        flexDirection:'row',
        alignItems:'center'
    },
    box:{
        borderRadius: 10,
        padding:20,
        marginBottom:20,
        ...Platform.select({
            ios: {
                shadowColor: "#000",
                shadowOffset: {width: 0, height: 2,},
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
            },
            android: {
              elevation: 5
            },
        }),
    },
    mileBox :{
        borderRadius:10,
        alignItems:'center',
        justifyContent:'center',
        width:'48%',
        padding:15,
        ...Platform.select({
            ios: {
                shadowColor: "#000",
                shadowOffset: {width: 0, height: 2,},
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
            },
            android: {
              elevation: 5
            },
        }),
    },
    innerContent: {
        // flexWrap:'wrap', flexShrink:1,
        borderRadius:15,
        padding:10,
        marginTop:15,
        marginLeft:3, marginRight:3,
        flexDirection:'row',
        alignItems:'center',
        backgroundColor:'white',
        ...Platform.select({
            ios: {
                shadowColor: "#000",
                shadowOffset: {width: 0, height: 2,},
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
            },
            android: {
              elevation: 5
            },
        }),
    
    },
    smolCircle: {marginLeft:-10, marginRight:5, width: Dimensions.get('window').width / 5, height:Dimensions.get('window').width / 5, alignItems:'center', justifyContent:'center'},
    insideC:{width: Dimensions.get('window').width / 6, height:Dimensions.get('window').width / 6,},
    progCircle:{
        marginTop:15,width: Dimensions.get('window').width / 2, height:Dimensions.get('window').width / 2,
        ...Platform.select({
            ios: {
                shadowColor: "#000",
                shadowOffset: {width: 0, height: 2,},
                shadowOpacity: 0.23,
                shadowRadius: 2.62,
            },
            android: {
              elevation: 4
            },
        }),
    },
    percentS:{position:'absolute', top:'50%', left:'50%', marginLeft: -(Dimensions.get('window').width / 6)/6, marginTop:-(Dimensions.get('window').width / 6)/8},
    outerBox:{
        marginLeft:15,
        flexDirection:'row',    marginBottom:15,
        borderTopLeftRadius:20,
        borderBottomLeftRadius:20,
    },
    boxStyle:{
        backgroundColor: 'white',
        // flexDirection:'row',
        marginLeft:15,
        marginRight:15,
        marginBottom:15,
        borderBottomLeftRadius:10,
        borderBottomRightRadius:10,
        padding:10,

    },
    sideText:{
        // backgroundColor:'#d9ebf2',
        alignItems:'center',justifyContent:'center',
        width: 50,
    },
    innerText:{
        textAlign:'right',
        transform: [
            { rotate: '270deg' },
            // { translateX: -OFFSET },
            // { translateY: OFFSET }
        ],
        // color:'white',
        width: 150,
        fontFamily: "ProximaNova-Bold"
    },
    boxInfo:{
        backgroundColor:'white',
        alignSelf:'stretch',
        flex:8,
        // width:'100%',
        ...Platform.select({
            ios: {
                shadowColor: "#000",
                shadowOffset: {width: 0, height: 1,},
                shadowOpacity: 0.22,
                shadowRadius: 2.22,
            },
            android: {
              elevation: 3
            },
        }),
         borderTopLeftRadius:20,
        borderBottomLeftRadius:20,
        paddingRight:15,
    
    },
    flatStyle:{
        backgroundColor: '#fff',
        // borderWidth:StyleSheet.hairlineWidth,
            // borderBottomWidth:0,
        flex:8,
        // marginRight:15,
        borderBottomLeftRadius:10,
        borderTopLeftRadius:10,
    },
    circleBox:{
        backgroundColor:'#fff',
       
        flex:1,
        paddingTop:20,
        paddingBottom:20,
        paddingLeft:15,
        paddingRight:15,
        // marginLeft:16,
        marginRight:16,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomLeftRadius:20,
        borderTopLeftRadius: 20,
    },
    
    
    

})