import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,TextInput,Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import AntIcon from "react-native-vector-icons/AntDesign";

import Header from '../components/Header.js'
import emptyPage from '../assets/icons/emptyBooking.png'
import HistoryComponent from "../components/HistoryComponent.js"
import Moment from 'moment';


class DisplayHistory extends React.Component{
    constructor(){
        super()
        this.state = {
            history: '',
            collapsed: false,
        }
    }
    
    componentDidMount(){
        this.sortAgain()
    }
    
    sortAgain = () => {
        let DATA = this.props.data
        let TEMP =[]
        let b ={}

        for (const month in DATA) { 
            let MONTH = DATA[month];
            MONTH.month = month

             b = {
                month: month,
                history:MONTH
            }

            TEMP.push(b)
                
        }  
        // console.log('month', TEMP)
        this.setState({history: TEMP})

    }


    showMonth = () => {
        let display
        let details 
        let DATA = this.state.history

        if(DATA !== undefined && DATA.length !== 0){
            // console.log('heiii', DATA);
            // let MONTH = DATA
           
            display = DATA.map(item => (
                <HistoryComponent data={item} navigation={this.props.navigation} />
                
            ))

            // display = 
            // <View>
            //     <Text style={{fontFamily: "ProximaNova-Bold", fontSize:16, marginBottom:15}} >{DATA.month}  </Text>
            //         {details}
            // </View>
        }

        return display 
    }
    render(){
        // console.log('hee', this.props.data, typeof(this.props.data));
        return(
            <View>
                {this.showMonth()}
            </View>
        )
    }
}


class TrackBookingHistory2 extends React.Component{
    constructor(){
        super()
        this.state = {
            history: '',
            view:1,
            searchText: '',
            filteredDATA: []

        }
    }
    
    componentDidMount(){
        this.sort()
    }
    
    sort = () => {
        let DATA = this.props.navigation.state.params.history
        if(DATA !== ''){
            // console.log('fokk', DATA);

            let sorted = {}, b =[]
            let TEMP =[]
           
            for (const key in DATA) {
                let b =  DATA[key];
                let year; 
                if(b.customer_year !== undefined){ year = parseInt(b.customer_year) } else { year =  parseInt(Moment(b.date).format('YYYY')) }
                let month 
                if(b.customer_month !== undefined){ month = b.customer_month } else { month = Moment(b.date).format('MMMM') }
                if (typeof sorted[year] === "undefined") {
                    sorted[year] = {};
                }
            
                if (typeof sorted[year][month] === "undefined") {
                    sorted[year][month] = [];
                    sorted[year][month].month = month
                }
            
              sorted[year][month].push(b);  
            }
            for (const year in sorted) { 
                let YEAR = sorted[year];

                let b = {
                    year: year,
                    history:YEAR
                }
    
                TEMP.unshift(b)
                    
            }   
            this.setState({history: TEMP, view:2})
        } else {
            this.setState({history :'', view:1})
        }

       
      
    }

    handleSubmit = () => {
        this.setState({searchText:'', filteredData:[]})
    }

    searchHistory = () => {
        let display 
        let DATA = this.props.navigation.state.params.history
        let SEARCHTEXT = this.state.searchText
        if(DATA.length !== 0){
            let filteredDATA = SEARCHTEXT !== '' ? DATA.filter(
                (item) => {
                    const itemData = `${item.customer_carplate.toLowerCase()} ${item.customer_year} `

                    let final = itemData.indexOf(SEARCHTEXT.toLowerCase())

                    if(final !== -1){
                        return final !== -1
                    // } else {
                    //     let CAT = item.quotation.masterItems
                    //     if(CAT !== undefined && CAT !== '' ){
                    //         let P = []
                            
                    //         CAT.forEach(x => {
                    //             let M = `${x.item} ${x.description} `
                    //             P.push(M)
                    //         });
                    //         console.warn('oiii', P);
                    //         if(P === SEARCHTEXT){
                                
                    //             return P
                    //         }

                    //         // let K = P.indexOf(SEARCHTEXT)
                    //         // let H = P.indexOf(SEARCHTEXT.toLowerCase()) !== -1;    
                            

                    //         // return H
                    //     }
                    }
                }
            ) : DATA



            if(filteredDATA.length !== 0){
                display = filteredDATA.map(item => {
                    return item.type === 'self input' ?  
                    <TouchableWithoutFeedback onPress={() => this.openManual(item)}>
                    <View style={styles.box}>
                      <Text style={{fontFamily: "ProximaNova-Bold",color:'#080880',fontSize:16  }} >Manual Input</Text> 
                        <Text style={{fontFamily: "ProximaNova-Bold",marginTop:3, fontSize:15}} >{Moment(item.date).format('DD MMMM YYYY')} </Text>
                        <Text style={{marginTop:3,fontSize:12 }}>{item.customer_carplate} </Text>
                        <Text style={{fontSize:12, marginTop:3  }} >{item.workshop}</Text>
                        <Icon name="angle-right" color='#000' size={20} style={{position:'absolute', right:15, bottom:15,}} />
                    </View>
                    </TouchableWithoutFeedback>
                    :
                    <TouchableWithoutFeedback onPress={() => this.openDets(item)}>
                    <View style={styles.box}>
                        {item.type !== undefined ? <Text style={{fontFamily: "ProximaNova-Bold",color:'#080880',fontSize:16  }} >Workshop Appointment</Text> : <Text style={{fontFamily: "ProximaNova-Bold",color:'#080880',fontSize:16  }} >On-The-Go</Text>}
                        {item.customer_month !== undefined ? <Text style={{fontFamily: "ProximaNova-Bold",marginTop:3, fontSize:15}} >{item.customer_day} {item.customer_month} {item.customer_year} </Text> : <Text style={{fontFamily: "ProximaNova-Bold",marginTop:3, fontSize:15}} >{Moment(item.date).format('DD MMMM YYYY')} </Text>}
                        <Text style={{marginTop:3,fontSize:12 }}>{item.customer_carplate} </Text>
                        {item.type === 'walkin' && item.customer_nameplace !== undefined ? <Text style={{fontSize:12, marginTop:3  }} >{item.customer_nameplace}</Text> : <Text style={{fontSize:12, marginTop:3  }} >{item.customer_address}</Text>}
                        {item.type !== undefined ? <Text style={{fontSize:12  }} >Category: Drive-In</Text> : <Text style={{fontSize:12 }} >Category: On-The-Go</Text>}
                        <Icon name="angle-right" color='#000' size={20} style={{position:'absolute', right:15, bottom:15,}} />
                    </View>
                    </TouchableWithoutFeedback>

                })  

            }
            if(this.state.searchText !== '' && filteredDATA.length === 0){
                display =
                <View style={{flex:1, alignItems:'center', justifyContent:'center', margin:15, flexShrink:1}}>
                    <AntIcon name='search1' size={30} />
                    <View style={{flexDirection:'row',marginTop:10, flexShrink:1, flexWrap:'wrap', justifyContent:'center', alignItems:'center'}}>
                        <Text style={{fontSize:15}}>We couldn't find anything for </Text>
                        <Text style={{fontFamily:'ProximaNova-Bold', fontSize:15}}>{this.state.searchText} </Text>
                    </View>

                    <Text style={{fontSize:12, marginTop:10}}>Try different or less specific keywords.</Text>
                </View>
            }
        }

        return display 
    }

    openDets = (x) => {
        if(x.type !== undefined && x.type === 'walkin'){
            firebase.analytics().logScreenView({
                screen_name: 'History_Walkin',
                screen_class: 'History_Walkin',
              })
            this.props.navigation.navigate('TrackBookingHistoryDetails', {
                item:x, 
                quotation: x.quotation.masterItems, 
            })
        } else {
            firebase.analytics().logScreenView({
                screen_name: 'History_Checklist',
                screen_class: 'History_Checklist',
              })
            
            this.props.navigation.navigate(
                "TrackSOTGHistory",{
                    item:x, 
                    quotation: x.quotation.masterItems, 
                    requested: x.quotation.requested_services
                })

        }
    }

    openManual = (x) => {
        this.props.navigation.navigate("TrackEditHistory",{item:x, onNavigateBack : this.goBack})
    }

    goBack = () => {
        this.props.navigation.state.params.onNavigateBack()
        this.props.navigation.goBack(null)
    }



    //without filter 
    showHistory = () => {
        let display 
        let DATA = this.state.history

       if(DATA !== ''){
        //    console.log('hoii', DATA);
            let ITEM = DATA.map(item => (
                <View style={{}} >
                    {/* <TouchableWithoutFeedback onPress={() => this.setState({currentView: item.year})} > */}
                    <View style={{marginTop:15,marginBottom:10, borderBottomColor:'#7a7a7a',borderBottomWidth:0.5, paddingBottom:5}}>
                        <Text style={{fontFamily: "ProximaNova-Bold",fontSize:16}} >{item.year}</Text>
                    </View>
                    {/* </TouchableWithoutFeedback> */}
                    
                   
                    <DisplayHistory data={item.history} navigation={this.props.navigation} />
                    
                </View> 
                

            ))
            display = 
            <View >
                {ITEM}
            </View>

       }
        return display 
    }


    render(){
        let content 

        if(this.state.view === 1){
            content = 
            <View style={{flex:1, alignItems:'center', justifyContent:'center', margin:20, marginTop:-40}}>
                <Image source={emptyPage} style={{height: 200, width:200, resizeMode:'contain'}} />
                <Text style={{fontSize:22, fontFamily: "ProximaNova-Bold", marginTop:15, marginBottom:10}}>No Records</Text>
                <Text style={{color:'#a7a7a7'}} >Looks like you haven't completed any services yet.</Text>
              </View>
        } else {
            content = 
            <ScrollView contentContainerStyle={{padding:15, paddingTop:0}}>
                {this.state.searchText !== '' ? this.searchHistory() : this.showHistory()} 

                <View style={{height:30}} />
            </ScrollView>
        }

        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Booking History' onPress={() => this.props.navigation.goBack()} />
                    
                    <View style={{margin:10,marginTop:0}}>
                    <View style={styles.searchBar}>
                        <TextInput style={{marginRight:15, width:'80%', color:'#000000', fontFamily:'ProximaNova-Regular'}}
                            placeholder='Search for plate number'
                            placeholderTextColor = 'grey'
                            autoCapitalize="none"
                            onChangeText={(x) => this.setState({searchText:x})}
                            value={this.state.searchText}
                            />
                        {this.state.searchText === '' ?
                            <AntIcon name='search1' size={15} color='#6e6d6d' />
                                :
                            <TouchableWithoutFeedback onPress={this.handleSubmit} >
                             <AntIcon name='close' size={15} />
                           </TouchableWithoutFeedback>
                        }
                    </View>
                    </View>

                    {content}
                    
                </View>
            </SafeAreaView>
        )
    }
}

TrackBookingHistory2 = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(TrackBookingHistory2))
export default TrackBookingHistory2

const styles = StyleSheet.create({
    box :{
        backgroundColor:'#fff',
        borderRadius:10,
        marginBottom:15,
        padding:15,
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 3 },
              shadowOpacity: 0.3,
            },
            android: {
              elevation: 3
            },
          }),
    },
    searchBar:{backgroundColor:'#fff', borderWidth:1,padding:(Platform.OS === 'ios') ? 10 : 0, paddingLeft:(Platform.OS === 'ios') ? 10 : 10, paddingRight:(Platform.OS === 'ios') ? 10 : 10,borderRadius:24, borderColor:'#ccc', flexDirection:'row', alignItems:'center', justifyContent:'space-between', flexShrink:1, marginTop:20},

})