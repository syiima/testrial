import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Moment from 'moment';
import Icon from 'react-native-vector-icons/FontAwesome'
import CIcon from 'react-native-vector-icons/SimpleLineIcons';

import Header from '../components/Header.js'

import AC from '../assets/sotgIcon/ac.png';
import airFilter from '../assets/sotgIcon/airfilter.png';
import Battery from '../assets/sotgIcon/battery.png';
import Bulb from '../assets/sotgIcon/bulb.png';
import GearOil from '../assets/sotgIcon/gearoil.png';
import Pump from '../assets/sotgIcon/pump.png';
import SparkPlug from '../assets/sotgIcon/sparkplug.png';
import Wiper from '../assets/sotgIcon/wiper.png';
import Brakepad from '../assets/sotgIcon/brakePad.png';
import oilfilter from '../assets/sotgIcon/oilfilter.png';

import basic from '../assets/sotgIcon/basic.png';
import premium from '../assets/sotgIcon/premium.png';
import inspect from '../assets/sotgIcon/inspect.png';

class TrackSOTGHistory extends React.Component{

    displayService = () => {
        let ALL_DATA = this.props.navigation.state.params.quotation
        let REQUESTED = this.props.navigation.state.params.requested
        let services = Object.keys(this.props.navigation.state.params.requested)
        let display, contnt;
        let requested_services = [], main_services;
     
       if(ALL_DATA !== undefined){
        services.forEach((item, index) => {
         //  console.warn('test', item, REQUESTED[item]);
          
          let image, brand, price, serviceType;
     
          if(item === 'basic_premium' ){
            if(REQUESTED[item] === 'Basic'){
              serviceType = 'Basic Services'
              image = basic
              brand = ALL_DATA[9] !== undefined ? ALL_DATA[9].brand : ""
              price = ALL_DATA[9] !== undefined ? ALL_DATA[9].price : ""
             }
            if(REQUESTED[item] === 'Premium'){
             serviceType = 'Premium Services'
             image = premium
             brand = ALL_DATA[9] !== undefined ? ALL_DATA[9].brand : ""
             price = ALL_DATA[9] !== undefined ? ALL_DATA[9].price : ""
         }
           if(REQUESTED[item] === 'Inspection'){
             serviceType = 'Car Health Check'
             image = inspect
             brand = ALL_DATA[9] !== undefined ? ALL_DATA[9].brand : ""
             price = ALL_DATA[9] !== undefined ? ALL_DATA[9].price : ""
         }
     
     
           main_services = 
            <View style={{flexDirection:'row',justifyContent:'space-between', alignItems:'center', width:'90%' }}>
                <View style={{flexDirection:'row', alignItems:'center', width:'70%', marginRight:15, }}>
                <Image source={image} style={{height:50, width:50, marginRight:15, }}/>
                <View  style={{flexShrink: 1}}>
                <Text style={{fontFamily: "ProximaNova-Bold",}}>{serviceType}</Text>
                {brand !== '' ? <Text >{brand}</Text> : null}
                </View>
                </View>
    
                {price !== '' ?
                <View style={{alignItems:'center', flexDirection:'row', alignSelf:'center', width:'25%'}}>
                <Text style={{fontSize:12,}} >RM </Text>
                    <Text style={{fontSize:20}}>{parseFloat(price).toFixed(2)}</Text> 
                </View>
                : null}
    
    
            </View>

          }
     
     
          if(item !== 'basic_premium' && REQUESTED[item] !== 'No' ){
           let image, brand, price, name;
     
           if(item === 'isAC'){
             name = ALL_DATA[0].item
             brand = ALL_DATA[0].brand
             price = ALL_DATA[0].price
             image = AC
           }
           if(item === 'isAirFilter'){
             name = ALL_DATA[1].item
             brand = ALL_DATA[1].brand
             price = ALL_DATA[1].price
             image = airFilter
           }
           if(item === 'isBattery'){
             name = ALL_DATA[2].item
             brand = ALL_DATA[2].brand
             price = ALL_DATA[2].price
             image = Battery
           }
           if(item === 'isBrakePad'){
             name = ALL_DATA[3].item
             brand = ALL_DATA[3].brand
             price = ALL_DATA[3].price
             image = Brakepad
           }
           if(item === 'isGearOil'){
             name = ALL_DATA[4].item
             brand = ALL_DATA[4].brand
             price = ALL_DATA[4].price
             image = GearOil
           }
           if(item === 'isLightBulb'){
             name = ALL_DATA[5].item
             brand = ALL_DATA[5].brand
             price = ALL_DATA[5].price
             image = Bulb
           }
           if(item === 'isSparkPlug'){
             name = ALL_DATA[6].item
             brand = ALL_DATA[6].brand
             price = ALL_DATA[6].price
             image = SparkPlug
           }
           if(item === 'isTyrePump'){
             name = ALL_DATA[7].item
             brand = ALL_DATA[7].brand
             price = ALL_DATA[7].price
             image = Pump
           }
           if(item === 'isWiper'){
             name = ALL_DATA[8].item
             brand = ALL_DATA[8].brand
             price = ALL_DATA[8].price
             image = Wiper
           }
     
           let a ={
             name: name,
             brand: brand,
             image: image,
             price: price
           }
     
           requested_services.push(a)
     
          }
     
     
        })
     
     
        display = 
        <View>
            <View >
               <Text style={styles.mainTitle}>Type of Service</Text>
                <View style={styles.box}>       
                    {main_services}
                </View>
            </View>
     
          {requested_services.length !== 0 ?
           <View >
                <Text style={styles.mainTitle}>Add-on Services</Text>
                <View style={styles.box} >
                {requested_services.map((item, ind) => (
                    <View style={{flexDirection:'row',justifyContent:'space-between', alignItems:'center', width:'88%', flexShrink:1, }}>
                    <View style={{flexDirection:'row', alignItems:'center', width:'70%', marginRight:5}}>
                        <Image source={item.image} style={{height:50, width:50, marginRight:15, }}/>
                        <View  style={{flexShrink: 1}}>
                        <Text style={{fontFamily: "ProximaNova-Bold",}}>{item.name}</Text>
                        {item.brand !== '' ? <Text >{item.brand}</Text> : null}
                        </View>
                    </View>
        
                    <View style={{alignItems:'center', flexDirection:'row', alignSelf:'center', width:'35%',}}>
                        <Text style={{fontSize:14,}} >RM </Text>
                        <Text style={{fontSize:20}}>{parseFloat(item.price).toFixed(2)} </Text>
                    </View>
                    </View>
                ))
                }
                </View>
        
             </View>
           : null}
        </View>
     
     
     
        contnt = 
            <View>
            {display}
            </View>
        
            
        }
         
        return contnt;
    }

    render(){
        let item = this.props.navigation.state.params.item

        return(
          <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
          <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'History Details' onPress={() => this.props.navigation.goBack()} />
                    
                    <ScrollView style={{padding:17, flex:1}} showsVerticalScrollIndicator={false}>
                        <View style={{margin:3}}>
                        <Text style={styles.mainTitle}>Vehicle Details </Text>
                        <View style={styles.box}>
                            <View style={styles.infoBox}>
                                  <Text style={{flex:1, }}>Plate</Text>
                                  <Text style={{fontFamily: "ProximaNova-Bold",flex:3}}>{item.customer_carplate}</Text>
                              </View>
                              <View style={styles.infoBox}>
                                  <Text style={{flex:1,  }}>Model</Text>
                                  <Text style={{fontFamily: "ProximaNova-Bold",flex:3 }}>{item.customer_carmake} {item.customer_carmodel}</Text>
                              </View>
                              
                              <View style={styles.infoBox}>
                                  <Text style={{flex:1,  }}>Year</Text>
                                  <Text style={{fontFamily: "ProximaNova-Bold",flex:3}}>{item.customer_caryear}</Text>

                            </View>
                        </View>

                        <Text style={styles.mainTitle}>Service Details </Text>
                        <View style={styles.box}>
                            <View style={styles.infoBox}>
                                  <Text style={{flex:1, }}>Date</Text>
                                  <Text style={{fontFamily: "ProximaNova-Bold",flex:3}}>{item.customer_day} {item.customer_month} {item.customer_year}</Text>
                              </View>
                              <View style={styles.infoBox}>
                                  <Text style={{flex:1,  }}>Time</Text>
                                  <Text style={{fontFamily: "ProximaNova-Bold",flex:3 }}>{item.customer_time}</Text>
                              </View>
                              
                              <View style={styles.infoBox}>
                                  <Text style={{flex:1,  }}>Location</Text>
                                  <Text style={{fontFamily: "ProximaNova-Bold",flex:3}}>{item.customer_address !== '' && item.customer_address !== undefined ? item.customer_address :'Not Available'  }</Text>

                            </View>

                        </View>

                        
                        {this.displayService()}

                        <View style={{height: 20}} />

                        </View>
                    </ScrollView>
                    
                </View>
            </SafeAreaView>
        )
    }
}

TrackSOTGHistory = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(TrackSOTGHistory))
export default TrackSOTGHistory

const styles = StyleSheet.create({
    mainTitle :{ fontFamily: "ProximaNova-Bold",fontSize:16, marginBottom:15},
    infoBox :{flexDirection:'row',alignItems:'center', marginBottom:5},
    box :{
        backgroundColor:'#fff',
        borderRadius:10,
        marginBottom:20,
        padding:15,
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 3 },
              shadowOpacity: 0.3,
            },
            android: {
              elevation: 3
            },
          }),
    },
    desc :{fontFamily: "ProximaNova-Bold", marginBottom:5},
    boxStyle:{
        backgroundColor:'white',
        borderWidth:2,
        borderColor:'#080880',
        borderRadius:8,
        marginBottom:15,
        marginTop:30,
   
        paddingLeft:10,
        paddingBottom:10,
        paddingTop:10,
        // width:'90%'
      },
})