import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'

import vehicles from '../assets/vehicles_placeholder.png'

class HomeWorkshop extends React.Component{
    constructor(){
        super()
        this.state = {
            stats_walkin:'',
            status: '',

        }
    }

    componentDidMount(){
        this.runset()
    }

    runset(){
        let N = this.props.item.book_walkin
        if(N !== undefined){
          let M = N._book_status
          this.setState({status: M})
        } 
    }

    navigateTo = (item) => {
        this.props.closeModal()
        let FUID = this.props.mobx_auth.FUID;

        let model =  this.props.mobx_carkey.setMODEL(item.model);
        let make =   this.props.mobx_carkey.setMAKE(item.make);
        let carcc =   this.props.mobx_carkey.setCARCC(item.carcc);
        let trans =   this.props.mobx_carkey.setTRANSMISSION(item.transmission);
        let year =   this.props.mobx_carkey.setYEAR(item.year);
        let PLATE =   this.props.mobx_carkey.setPLATE(item.plate);

        let BOOK_STATS = this.state.status;
        let DATA = this.props.item.book_walkin


        if(DATA !== '' && DATA !== undefined ){
        
        let BOOK_ID = DATA._book_id;
        let RETAIL_ID = DATA.retail_id;
        let RETAIL_MAIN = DATA.retail_main;

        let status = this.props.mobx_retail.setSTATUS(BOOK_STATS);
        let bookID =  this.props.mobx_retail.setR_WALKIN_BOOKID(BOOK_ID);
        let wsID =   this.props.mobx_retail.setR_ID(RETAIL_ID);
        let groupID =   this.props.mobx_retail.setR_GROUPID(RETAIL_MAIN);
        
        if(BOOK_STATS === 'Requesting'){ //Undergoing inspevtion
            firebase.analytics().logScreenView({
                screen_name: 'PerformService_home',
                screen_class: 'PerformService_home',
              })
            this.props.navigation.navigate('WorkshopPerformService' )
        }
        if(BOOK_STATS === 'Accepted'){ //performing service
            firebase.analytics().logScreenView({
                screen_name: 'PerformService_home',
                screen_class: 'PerformService_home',
              })
            this.props.navigation.navigate('WorkshopPerformService')
        }
        if(BOOK_STATS === 'Pending'){ //review quot, 
            firebase.analytics().logScreenView({
                screen_name: 'Quotation_home',
                screen_class: 'Quotation_home',
              })

            this.props.navigation.navigate('WorkshopQuotation')
        }
        if(BOOK_STATS === 'Complete' || BOOK_STATS === 'Fail'){ //make payment
            firebase.analytics().logScreenView({
                screen_name: 'Payment_home',
                screen_class: 'Payment_home',
              })

            firebase.database().ref(`plate_number/${PLATE}/book_walkin/`).update({_book_status: 'Complete'})
            this.props.navigation.navigate('WorkshopPayment')
        }
        if(BOOK_STATS === 'Cancel'){ //// review quot
            firebase.analytics().logScreenView({
                screen_name: 'Quotation_home',
                screen_class: 'Quotation_home',
              })
            this.props.navigation.navigate('WorkshopQuotation')
        }

        if(BOOK_STATS === 'Paid'){ //Done : Rate service 
            firebase.analytics().logScreenView({
                screen_name: 'Receipt_home',
                screen_class: 'Receipt_home',
              })
            this.props.navigation.navigate('WorkshopFeedback')
        }

        if(BOOK_STATS.includes('Appointment')){ //appont
            firebase.analytics().logScreenView({
                screen_name: 'Appointment_home',
                screen_class: 'Appointment_home',
              })
            this.props.navigation.navigate('WorkshopAppointment')
        }

        }
        if(BOOK_STATS === ''){
            firebase.analytics().logScreenView({
                screen_name: 'WorkshopList',
                screen_class: 'WorkshopList',
              })
            this.props.navigation.navigate('WorkshopListing')
        }
    }


    renderStatus = () => {
        let STATS = this.state.status
        let status_car
  
        if(STATS === undefined || STATS === ''){
          status_car = 
          <View style={{backgroundColor:'#080880', borderRadius:10, padding:4,alignItems:'center', justifyContent:'center'}}>  
              <Text style={{fontFamily:'ProximaNova-Bold', color:'#fff', textAlign:'center'}}>Explore Merchants</Text>
          </View>
        }
        if(STATS === 'Complete' || STATS === 'Fail'){
          status_car = 
          <View style={{backgroundColor:'#FF6665', borderRadius:10, padding:4, alignItems:'center', justifyContent:'center'}}>
              <Text style={{fontSize:11, color:'#fff'}}>Ongoing:</Text>
              <Text style={{fontFamily:'ProximaNova-Bold', color:'#fff', textAlign:'center'}}>Make payment</Text>
          </View>
        }
        if(STATS === 'Accepted'){
          status_car = 
          <View style={{backgroundColor:'green', borderRadius:10, padding:4, alignItems:'center', justifyContent:'center'}}>
              <Text style={{fontSize:11, color:'#fff'}}>Ongoing:</Text>
              <Text style={{fontFamily:'ProximaNova-Bold', color:'#fff', textAlign:'center'}}>Performing Service</Text>
          </View>
        }
  
        if(STATS === 'Requesting'){
            status_car = 
            <View style={{backgroundColor:'#c49e00', borderRadius:10, padding:4, alignItems:'center', justifyContent:'center'}}>
                <Text style={{fontSize:11, color:'#fff'}}>Ongoing:</Text>
                <Text style={{fontFamily:'ProximaNova-Bold', color:'#fff', textAlign:'center'}}>Undergoing Inspection</Text>
            </View>
        }
        if(STATS === 'Pending'){
            status_car = 
            <View style={{backgroundColor:'#c49e00', borderRadius:10, padding:4, alignItems:'center', justifyContent:'center'}}>
                <Text style={{fontSize:11, color:'#fff'}}>Ongoing:</Text>
                <Text style={{fontFamily:'ProximaNova-Bold', color:'#fff', textAlign:'center'}}>Review Quotation</Text>
            </View>
        }
        if(STATS === 'Paid'){
          status_car = 
          <View style={{backgroundColor:'green', borderRadius:10, padding:4, alignItems:'center', justifyContent:'center'}}>
              <Text style={{fontSize:11, color:'#fff'}}>Done:</Text>
              <Text style={{fontFamily:'ProximaNova-Bold', color:'#fff', textAlign:'center'}}>Rate service</Text>
          </View>
        }
        if(STATS === 'Cancel'){
          status_car = 
          <View style={{backgroundColor:'#FF6665', borderRadius:10, padding:4, alignItems:'center', justifyContent:'center'}}>
              <Text style={{fontSize:11, color:'#fff'}}>Waiting:</Text>
              <Text style={{fontFamily:'ProximaNova-Bold', color:'#fff', textAlign:'center'}}>Pending Cancellation</Text>
          </View>
        }
  
        if(STATS === 'Appointment' || STATS === 'Appointment Accepted'){
          status_car = 
          <View style={{backgroundColor:'green', borderRadius:10, padding:4, alignItems:'center', justifyContent:'center'}}>
              <Text style={{fontSize:11, color:'#fff'}}>Ongoing:</Text>
              <Text style={{fontFamily:'ProximaNova-Bold', color:'#fff', textAlign:'center'}}>View Appointment</Text>
          </View>
        }
        if(STATS === 'Appointment Declined' ){
          status_car = 
          <View style={{backgroundColor:'#FF6665', borderRadius:10, padding:4, alignItems:'center', justifyContent:'center'}}>
              <Text style={{fontSize:11, color:'#fff'}}>Ongoing:</Text>
              <Text style={{fontFamily:'ProximaNova-Bold', color:'#fff', textAlign:'center'}}>Appointment Changed</Text>
          </View>
        }
        if(STATS === 'Appointment Changed' ){
          status_car = 
          <View style={{backgroundColor:'#c49e00', borderRadius:10, padding:4, alignItems:'center', justifyContent:'center'}}>
              <Text style={{fontSize:11, color:'#fff'}}>Ongoing:</Text>
              <Text style={{fontFamily:'ProximaNova-Bold', color:'#fff', textAlign:'center'}}>Appointment Adjusted</Text>
          </View>
        }
  
        if(STATS === ''){
          status_car = 
          <View style={{backgroundColor:'#080880', borderRadius:10, padding:4,alignItems:'center', justifyContent:'center'}}>  
              <Text style={{fontFamily:'ProximaNova-Bold', color:'#fff', textAlign:'center'}}>Explore Merchants</Text>
          </View>
        }
  
  
        return status_car
    }
  
    showVehicleProfile = () => {
        let PLATE = this.props.item.plate
        let Plate = this.props.mobx_carkey.setPLATE(PLATE);
        this.props.closeModal()

        firebase.analytics().logScreenView({
            screen_name: 'CarProfile',
            screen_class: 'CarProfile',
          })
        this.props.navigation.navigate('VehicleProfile')
    }

    
    render(){
        let item = this.props.item 

        let content, stats 

        if(item.status === 'approved'){
            content = 
            <TouchableOpacity
            onPress={() => this.navigateTo(this.props.item)}
            style={{padding:15, flexDirection:'row', justifyContent:'space-between', alignItems:'center', borderBottomWidth:0.5, borderBottomColor:'#ccc', marginLeft:10, marginRight:10}}>
            <View style={{height:40, width:40,flex:1, marginRight:15 }}>
                <Image source={vehicles} style={{height:40, width:40, borderRadius:20}}/>
            </View>
            <View style={{flex:2, marginRight:10}} >
                <Text style={{fontFamily:'ProximaNova-Bold'}} >{item.plate}</Text>
                <Text>{this.props.item.make} {this.props.item.model}</Text>
            </View>
            <View style={{flex:2}} >
                {this.renderStatus()}
            </View>

            </TouchableOpacity>
        }

        if(item.true_owner === false){
            stats = 
            <View style={{backgroundColor:'tomato', borderRadius:10, padding:4, alignItems:'center', justifyContent:'center'}}>
                <Text style={{fontSize:11, color:'white'}}>Status:</Text>
                <Text style={{fontFamily:'ProximaNova-Bold', color:'white', textAlign:'center'}}>Pending Approval</Text>
            </View>
        } else {
            stats = 
            <View style={{backgroundColor:'tomato', borderRadius:10, padding:4, alignItems:'center', justifyContent:'center'}}>
                <Text style={{fontSize:11, color:'white'}}>Status:</Text>
                <Text style={{fontFamily:'ProximaNova-Bold', color:'white', textAlign:'center'}}>Pending Verification</Text>
            </View>
            
        }
        if(item.status === 'pending' || item.status === 'verify'){
            content = 
            <TouchableWithoutFeedback onPress={this.showVehicleProfile} >
            <View style={{padding:15, flexDirection:'row', justifyContent:'space-between', alignItems:'center', borderBottomWidth:0.5, borderBottomColor:'#ccc', marginLeft:10, marginRight:10}}>
              <View style={{height:40, width:40,flex:1, marginRight:15 }}>
                  <Image source={vehicles} style={{height:40, width:40, borderRadius:20}}/>
              </View>
              <View style={{flex:2, marginRight:10}} >
                  <Text style={{fontFamily:'ProximaNova-Bold'}} >{item.plate}</Text>
                  <Text>{item.make} {item.model}</Text>
              </View>
              <View style={{flex:2}} >
                {stats}
              </View>
                
            </View>
            </TouchableWithoutFeedback>
           
          }


        return(
            <View>
                {content}
            </View>
        )
    }
}

HomeWorkshop = inject('mobx_auth','mobx_retail', 'mobx_carkey', 'mobx_config')(observer(HomeWorkshop))
export default HomeWorkshop

const styles = StyleSheet.create({

})