import React from "react"
import {Platform, Alert, Modal,StyleSheet, Text,Linking, View,ActivityIndicator, FlatList, PermissionsAndroid,SafeAreaView, TextInput, ScrollView,Image, Dimensions, TouchableOpacity, TouchableWithoutFeedback,KeyboardAvoidingView} from 'react-native';
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/FontAwesome"
import AntIcon from "react-native-vector-icons/AntDesign";
import Feather from 'react-native-vector-icons/Feather';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Geolocation from '@react-native-community/geolocation'
import Moment from 'moment';

import BottomModal from "../components/BottomModal.js"
import Button from '../components/Button'

import maps_logo from '../assets/maps_logo.png'
import waze_logo from '../assets/waze_logo.png'
import emptyLogo from '../assets/icons/emptyLogo.png'

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 0
const LONGITUDE = 0
const LATITUDE_DELTA = parseFloat(0.01);
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const WIDTH = Dimensions.get('window').width - 30

class WorkshopListing extends React.Component{
    constructor(){
        super()
        this.state = {
            main_data: '',
            workshop_dets : [],
            a: {
                latitude: LATITUDE ,
                longitude: LONGITUDE ,
                address:'',
                nameplace: ''
              },
            dist : '',
            searchText: '',
            filteredData :'',
            modalMap: false,
            check_loc: false, 
            view_loading: '1',

            ws_dist:0,
            error:'',
            latlong: ''
        }
    }
    
    componentDidMount(){
        this.getPosition()
        this.getPermission() 
        // this.fetchWorkshops()
    }

    getPermission = async() => {
        if(Platform.OS === 'android'){
            const grantedRead = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
                title: "Location permission",
                message: "App needs access to your location to show workshops in your area",
                }
            );
            const grantedCoarse = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
                    {
                    title: "Location permission",
                    message: "App needs access to your location to show workshops in your area",
                    }
                );
    
        if (grantedRead === 'granted' || grantedCoarse === 'granted') {
            this.getPosition()
          } else {
            this.setState({view_loading: '4'})
            Alert.alert('Sorry', 'Please allow access to location in order to see all merchants')
          }
    
        } else {
            this.getPosition()
        }
    }

    fetchWorkshops(){
        // this.getPosition()
        let that = this;
        let DATA = [], ALL_OUTLETS, ALL_MAIN

        firebase.database().ref('retail/').once('value').then((snapshot) => {
            if(snapshot.exists()){
               ALL_OUTLETS = snapshot.val() //main + outlets
            //    console.warn('hmm', ALL_OUTLETS);
            }
        })
        
        firebase.database().ref('retail_main/').once('value').then((snp) => {
            if(snp.exists()){
                ALL_MAIN = snp.val() //all main
                // console.log('hmm', ALL_MAIN);
            }
        }).then(() => {
            this.renderData(ALL_OUTLETS, ALL_MAIN)

        })


        
    }

    getPosition=async()=>{
        console.log('tekan?')
       await Geolocation.getCurrentPosition(
    
        async  (position) => {
    
          await this.setState({
              a:{
                ...this.state.a,
                latitude: parseFloat(position.coords.latitude),
                longitude:parseFloat(position.coords.longitude),
                address: '',
                nameplace: ''
              }
            },()=>{console.log('bello',this.state.a)})
    
        
          let url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='+ this.state.a.latitude+','+this.state.a.longitude+'&key=AIzaSyDV3WviHkCYt82VA90LiRLLMB8RGQwGSYA';

          await fetch(url)
          .then(data => data.json())
          .then(data => this.setState({
            a:{
               ...this.state.a,
               nameplace:  this.state.a.nameplace,
               address: data.results[0].formatted_address,
               check_loc:false,
              }
            }, ()=>{ console.log('inside 2', this.state.a.address)}
          ))
          .then(() => {
              this.fetchWorkshops()
          })
          



          },
    
          (error) => this.setState({ view_loading:'4' }),
          { enableHighAccuracy: true, timeout: 50000,},
          
        );
    }

    getName = async()=>{
        let url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='+ this.state.a.latitude+','+this.state.a.longitude+'&key=AIzaSyDV3WviHkCYt82VA90LiRLLMB8RGQwGSYA';
        if(this.state.a.address==''){
    
         await fetch(url)
          .then(data => data.json())
          .then(data => this.setState({
            a:{
               ...this.state.a,
               nameplace:  this.state.a.nameplace,
               address: data.results[0].formatted_address
              }
            }, ()=>{ console.log('inside 2', this.state.a.address)}
          ))

        //   this._renderLists()
        }
    }


    renderData = async(outlets, main) => {
        let TEMP = []

        if(outlets.length !== 0 && main.length !== 0){
            for (const key in outlets) {
                let CURR_MAIN = outlets[key]; //data outler
                // key == retail_mainID
                // console.log('ehh', key);
                for (const id in CURR_MAIN) {
                    // id == retailmain_id
                    let CURR_OUTL = CURR_MAIN[id]; // data outlet
    
                    if(CURR_OUTL.display === '3' && main[key].logo !== ''){
                    // if(main[key].logo !== undefined && main[key].logo !== '' && CURR_OUTL.display === '3'){
                        // main[key].logo !== '' &&
                        // console.log('hello', CURR_OUTL);
                        let b = {}
                        let CATE
                        b = CURR_OUTL
                        b.logo = main[key].logo
                        b.workshop_id = id
                        b.group_id = key

                        if(CURR_OUTL.preferred === undefined){
                            b.preferred = false
                        } else { 
                            b.preferred = CURR_OUTL.preferred}


                        if(CURR_OUTL.category !== undefined){
                            let CAT = CURR_OUTL.category      
                            if(CAT.includes('Motorcycle')){
                                CATE = 'Motorcycle'
                            } else {
                                CATE = 'Car'
                            }
                                
                            
                        } else {
                            CATE = 'Car'
                        }

                        b.type = CATE



                        if(CURR_OUTL.location !== undefined){
                            let distance
                            let LAT = this.state.a.latitude
                            let LONG = this.state.a.longitude  
                            // let LAT = 3.140853
                            // let LONG = 101.693207
                            
                            let lat, long
                            
                            // console.log('hii', CURR_OUTL);
                            lat = b.location.lat
                            long = b.location.lng

                            let p = 0.017453292519943295;    // Math.PI / 180
                            let c = Math.cos;
                            let a = 0.5 - c((lat - LAT) * p)/2 + 
                                    c(LAT * p) * c(lat * p) * 
                                    (1 - c((long - LONG) * p))/2;

                            distance = (12742 * Math.asin(Math.sqrt(a))).toFixed(2); // 2 * R; R = 6371 km
                            b.distance = parseFloat(distance)

                            TEMP.push(b);
                        }else {
                            let ApiURL = 'https://maps.googleapis.com/maps/api/distancematrix/json?'
                            let workshop_add = CURR_OUTL.address
                            let current
                            // if(this.props.mobx_config.config === 0) { 
                                // current = '26A, Off, Jln Kuching, Taman City, 51200 Kuala Lumpur, Federal Territory of Kuala Lumpur' 
                            // } else {
                              current = this.state.a.address
                            // }
                            let finalApiURL = ApiURL + '&origins=' + current + '&destinations=' + workshop_add + '&key=AIzaSyDV3WviHkCYt82VA90LiRLLMB8RGQwGSYA'
                            

                            await fetch(finalApiURL)
                            .then(data => data.json())
                            .then(data => {
                                let distance
                                if(data.rows[0].elements[0].status === "OK"){
                                    distance =  data.rows[0].elements[0].distance.text
                                    distance=distance.replace(/,/g, '')
                                    distance=parseFloat(distance)
                                    // console.warn(distance, b.workshop_id)

                                } else {
                                    distance = 'Not Available'
                                }
                                b.distance = distance
                                TEMP.push(b);
                            })
                        }
                        let SORTED = TEMP.sort((a, b) => (a.distance - b.distance))
                        SORTED.sort(function(x, y) { return y.preferred - x.preferred }); 


                        this.setState({workshop_dets: SORTED, view_loading:'2'})
                    }
                        
                    
                }
                
            }
        }
    }


    // ====== FUNCTIONS ====== 
    getLatLongWS = async(x) => {
        // console.warn('item');
        let item = this.state.latlong
        this.setState({modalMap: false})
        let ADDRESS = item.address
        let LAT, LONG
        if(item.location !== undefined){
            LAT = item.location.lat
            LONG = item.location.lng
    
            if(x === 'gmaps'){
                Linking.openURL('https://www.google.com/maps/search/?api=1&query=' + LAT + ',' + LONG)

            } else {
                Linking.openURL('https://waze.com/ul?ll=' + LAT + ',' + LONG + '&navigate=yes');
            }
        } else {
            let URL = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + ADDRESS +'&key=AIzaSyDV3WviHkCYt82VA90LiRLLMB8RGQwGSYA' 
            let fetchResult =  await fetch(URL); // call API
            let Result =  await fetchResult.json()
    
            await fetch(URL)
                .then(data => data.json())
                .then(data => {
                    LAT = data.results[0].geometry.location.lat
                    LONG = data.results[0].geometry.location.lng
    
                if(LAT !== 0 || LONG !== 0) {
                    if(x === 'gmaps'){
                        firebase.analytics().logScreenView({
                            screen_name: 'gmaps_list',
                            screen_class: 'gmaps_list',
                          }) 
                        Linking.openURL('https://www.google.com/maps/search/?api=1&query=' + LAT + ',' + LONG)
        
                    } else {
                        firebase.analytics().logScreenView({
                            screen_name: 'waze_list',
                            screen_class: 'waze_list',
                          }) 
                        Linking.openURL('https://waze.com/ul?ll=' + LAT + ',' + LONG + '&navigate=yes');
                    }
                } else {
                    Alert.alert('Sorry', 'The workshop haven\'t updated their location.')
                }
    
                })
        }        
    }
    
    
    bookAppt = (item) => {
        firebase.analytics().logScreenView({
            screen_name: 'BookAppointment_list',
            screen_class: 'BookAppointment_list',
          }) 

        // console.warn('ehh', item.group_id);
        let WS_ID =   this.props.mobx_retail.setR_ID(item.workshop_id)
        let WSMAIN_ID =   this.props.mobx_retail.setR_GROUPID(item.group_id)
        let ADD = this.props.mobx_retail.setADDRESS(item.address)
        let NAMEP = this.props.mobx_retail.setNAMEPLACE(item.name)

        this.props.navigation.navigate('WorkshopBookingAppointment', {
            // qrData : FINAL,
            groupID: item.group_id,
            workshopID: item.workshop_id,
            address: item.address,
            nameplace: item.name,
        })
    }

    
    handleSubmit = () => {
        this.setState({searchText:'', filteredData:[]})
    }

    workshopList = () => {
        let show_contnet 
        let DATA = this.state.workshop_dets
        let SEARCHTEXT = this.state.searchText
        if(DATA.length !== 0){
            let filteredDATA = SEARCHTEXT !== '' ? DATA.filter(
                (item) => {
                    const itemData = `${item.name.toLowerCase()} ${item.address.toLowerCase()} `

                    let final = itemData.indexOf(SEARCHTEXT.toLowerCase())

                    if(final !== -1){
                        return final !== -1
                    } else {
                        let CAT = item.category
                        if(CAT !== undefined){
                            let P = []
                            
                            CAT.forEach(x => {
                                if(x !== null){
                                    let M = x.toLowerCase()
                                    P.push(M)
                                }
                               
                            });

                            if(item.distance < 40) {
                                return  P.indexOf(SEARCHTEXT.toLowerCase()) !== -1;   
                            }

                        }
                    }
                }
            ) : DATA.filter(
                (item) => {
                    return item.distance < 40 
                }
            )

            //got data
            if(this.state.view_loading === '2'){
                show_contnet = 
                
                <FlatList
                data = {filteredDATA}
                extraData = {this.state}
                renderItem={({item, index}) => 
                <View>
                    <View style={styles.box}>
                        <TouchableWithoutFeedback onPress={() => {
                             firebase.analytics().logScreenView({
                                screen_name: 'BookAppointment_list',
                                screen_class: 'BookAppointment_list',
                              }) 
                            this.props.navigation.navigate('WorkshopDetails', {
                            item: item, group_id: item.group_id, address: this.state.a.address})}}>
                            <View style={{flexDirection:'row'}}>
                                {item.logo !== '' ? 
                                <Image source={{uri: item.logo}} style={{width:Dimensions.get('window').width / 5, height:Dimensions.get('window').width /5  ,resizeMode:'contain', borderRadius:10}}  />

                                :
                                <Image source={emptyLogo} style={{width:Dimensions.get('window').width / 5, height:Dimensions.get('window').width /5  ,resizeMode:'contain', borderRadius:10}}  />
                            }
                            <View style={{flexShrink:1,flex:3, marginLeft:10,  marginRight: 10}}>
                                {item.preferred === true ?  <Text style={{fontSize:14,color:'#e85b1a', marginBottom:3, fontFamily:'ProximaNova-Bold'}}>PREFERRED</Text> : null}
                                <Text style={{fontSize:16, fontFamily:'ProximaNova-Bold'}}>{item.name.toUpperCase()}</Text>
                                                            
                                {this.renderStars(item)}

                                {/* <CalcDist address={item.address} users_address ={this.state.a.address} /> */}
                                <Text style={{fontFamily: "ProximaNova-Bold",color:"#080880",marginBottom:6, marginTop:3}} >{parseFloat(item.distance)} km away</Text>
                                {this.renderOpenClose(item)}

                                <View style={{flexShrink:1, marginTop:10, marginBottom:15, flexDirection:'row', alignItems:'center'}}>
                                    <TouchableWithoutFeedback onPress={() => this.setState({modalMap:true, latlong: item })}>
                                        <View style={styles.outerLayer}>
                                            <MaterialCommunityIcons name='navigation' size={15} color='#080880' />
                                            <Text style={{color:"#080880",marginLeft:8}} >Map</Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                    <TouchableWithoutFeedback onPress={() => this.bookAppt(item)}>
                                        <View style={styles.outerLayer}>
                                            <MaterialCommunityIcons name='calendar-month-outline' size={15} color='#080880' />
                                            <Text style={{color:"#080880",marginLeft:8}} >Appointment</Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                    
                                </View>


                            </View>
                            </View>
                            </TouchableWithoutFeedback>

                            <BottomModal
                                modalName={this.state.modalMap}
                                onPress={() => this.setState({modalMap:false})}
                                style={styles.innerModal}
                                children = {
                                <View >
                                    <View style={{padding:20, paddingBottom:0, alignItems:'center'}}>
                                        <Text style={{textAlign:'center', fontFamily:'ProximaNova-Bold', fontSize:20, marginBottom:5}}>Open with</Text>             
                                    </View>

                                    <View style={{margin:30, marginTop:20,  alignItems:'center', justifyContent:'space-around', flexDirection:'row'}}>
                                        <TouchableOpacity  onPress={() => {this.getLatLongWS('gmaps')}} style={{}}>
                                            <Image source={maps_logo} style={{height:50, width:50, marginRight:10, resizeMode:'contain'}}/>
                                            <Text style={{textAlign:'center'}}>Maps</Text>
                                        </TouchableOpacity>
                                        
                                        <TouchableOpacity onPress={() => {this.getLatLongWS('waze')}} style={{}}>
                                            <Image source={waze_logo} style={{height:50, width:50, resizeMode:'contain'}}/>
                                            <Text style={{textAlign:'center'}}>Waze</Text>
                                        </TouchableOpacity>

                                    </View>

                                    <TouchableOpacity onPress={()=> this.setState({modalMap:false, latlong: ''})} style={{marginBottom: 15}} >
                                        <Button bgColor='#080880' title='Cancel' width={WIDTH}  />
                                    </TouchableOpacity>
                                </View>
                                }
                            />

                            
                        </View>
                    </View>
                }
                
                />
            }

            //no search found
            if(this.state.view_loading === '2' && this.state.searchText !== '' && filteredDATA.length === 0){
                show_contnet = 
                <View style={{flex:1, alignItems:'center', justifyContent:'center', margin:15, flexShrink:1}}>
                    <AntIcon name='search1' size={30} />
                    <View style={{flexDirection:'row',marginTop:10, flexShrink:1, flexWrap:'wrap', justifyContent:'center', alignItems:'center'}}>
                        <Text style={{fontSize:15}}>We couldn't find anything for </Text>
                        <Text style={{fontFamily:'ProximaNova-Bold', fontSize:15}}>{this.state.searchText} </Text>
                    </View>

                    <Text style={{fontSize:12, marginTop:10}}>Try different or less specific keywords.</Text>
                </View>
            }

            //no workshop
            if(this.state.view_loading === '3'){
                show_contnet = 
                <View style={{flex:1, alignItems:'center', justifyContent:'center'}} > 
                    <Text style={{color:'#080880'}}>No workshops available</Text>
                </View>
            }
            
        }

        //loading
        if(this.state.view_loading  === '1'){
            show_contnet = 
            <View style={{flex:1, alignItems:'center', justifyContent:'center'}} > 
                <ActivityIndicator style={{ height: 30, width: 30, alignSelf:'center'}} size="small" color="#080880" />
                <Text style={{color:'#080880'}}>Loading...</Text>
            </View>
        }

        //location not allowed ?
        if( this.state.view_loading  === '4'){
            show_contnet = 
            <View style={{flex:1,margin:20, alignItems:'center', justifyContent:'center'}} >                     
                <Feather name='alert-triangle' size={50} color='red' />
                <View style={{alignItems:'center', marginTop:20, justifyContent:'center'}}>
                    <Text style={{color:'#080880', fontFamily:'ProximaNova-Bold', fontSize:16}}>Oops! Something went wrong!</Text>
                    <Text style={{color:'#080880',textAlign:'center', fontFamily:'ProximaNova-Bold', fontSize:16, marginTop:15}}>Please make sure you've allowed Location access for SERV app.</Text>
                </View>
            </View>
        }
        return show_contnet
    }


    categoryList = () => {
        let array = ['Motorcycle', 'Tyres', 'Exhaust', 'Aircond', 'Body & Paint','Repair & Maintenance','Specialist', 'Battery','Tuning',]
        let disp
        
        disp = array.map((item) => (
            <TouchableWithoutFeedback onPress={() => this.setState({searchText: item}) }>
                <View style={this.state.searchText !== item ? styles.catTitle : styles.colorTitle}>
                    <Text style={this.state.searchText !== item ? {fontFamily:'ProximaNova-Bold'} : {fontFamily:'ProximaNova-Bold', color: '#fff'}} >{item}</Text>
                </View>
            </TouchableWithoutFeedback>
        ))

        return disp
    }

    renderOpenClose = (x) => {
        let display
        let DATA = x.business_hrs
        let TODAY = (Moment().format('dddd')).toLowerCase()
        let TIME = Moment().format('HH:mm:ss') //current
        let STATUS;
    
        if(DATA !== undefined){
        for (const d_key in DATA) {          
            if(TODAY === d_key){
            //closed
            if(DATA[d_key].open === 'closed' || DATA[d_key].open === ''){
                STATUS = 'Closed'
            }             
            if(DATA[d_key].open !== 'closed' &&  DATA[d_key].open !== '') {
    
                // open
                let finalH
                let HOURo
                let HOUR = DATA[d_key].open.split(':')[0]
                let MINUTE =  (DATA[d_key].open.split(':')[1]).substring(0,2)
                
                if(HOUR < 10){
                    HOUR = '0' + HOUR
                }
    
                let AMPM = (DATA[d_key].open.split(':')[1]).substring(4,2)
                // // console.warn('hmmm', AMPM);
                if(AMPM === 'PM'){
                    let x = HOUR
                    let y = '12'

                    HOURo = parseFloat(x) + parseFloat(y)
                }  else {
                    HOURo = HOUR
                }
                finalH = HOUR.toString()
                let FINAL = finalH + ':' + MINUTE + ':' + '00'
                
    
                //CLOSE
                let HOURc
                let finalC
                let HOURC = DATA[d_key].close.split(':')[0]
                let MINUTEC =  (DATA[d_key].close.split(':')[1]).substring(0,2)
                
                if(HOURC < 10){
                    HOURC = 0 + HOURC
                }
    
                let AMPMC = (DATA[d_key].close.split(':')[1]).substring(4,2)
                // console.warn('hmmm', MINUTEC);
                if(AMPMC === 'PM'){
                    let x = HOURC
                    let y = 12

                    HOURc = parseFloat(x) + parseFloat(y)
                } else{
                    HOURc = HOURC
                }
                finalC = HOURc.toString()
                let CLOSE = finalC + ':' + MINUTEC + ':' + '00'
                
                if(TIME > FINAL && TIME < CLOSE ){
                    STATUS = 'Open until ' + DATA[d_key].close
                
                }
                if(TIME < FINAL || TIME > CLOSE){
                    STATUS = 'Closed. Will open at ' + DATA[d_key].open
                }
                
            }
            }
            
            
        }
        } else {
        STATUS = 'Not Available'
        }
        
        display = <Text style={{fontSize:11, marginBottom:15, color:'#6e6d6d'}}>{STATUS}</Text>
        return display
    }

    renderStars = (item) => {
        let STARS
    
        if(item.reviews_overall === undefined){
          STARS = 
          <View style={{flexDirection:'row', alignItems:'center', marginTop:5, marginBottom:5}}>
              <Icon name='star-o' size={15} style={{marginRight:2}}/>
              <Icon name='star-o' size={15} style={{marginRight:2}}/>
              <Icon name='star-o' size={15} style={{marginRight:2}}/>
              <Icon name='star-o' size={15} style={{marginRight:2}}/>
              <Icon name='star-o' size={15} style={{marginRight:2}}/>
          </View>
    
        } else {
            let X = item.reviews_overall
            if( X.rate === 5){
                STARS = 
                <View style={{flexDirection:'row', alignItems:'center', marginTop:5, marginBottom:5}}>
                      <Icon name='star' color='#ffce00' size={15} style={{marginRight:2}}/>
                      <Icon name='star' color='#ffce00' size={15} style={{marginRight:2}}/>
                      <Icon name='star' color='#ffce00'  size={15} style={{marginRight:2}}/>
                      <Icon name='star' color='#ffce00'  size={15} style={{marginRight:2}}/>
                      <Icon name='star' color='#ffce00'  size={15} style={{marginRight:2}}/>
                  </View>
            }
              if( X.rate <= 1){
              STARS = 
              <View style={{flexDirection:'row', alignItems:'center', marginTop:5, marginBottom:5}}>
                  <Icon name='star' color='#ffce00' size={15} style={{marginRight:2}}/>
                  <Icon name='star' color= '#ccc' size={15} style={{marginRight:2}}/>
                  <Icon name='star' color= '#ccc'  size={15} style={{marginRight:2}}/>
                  <Icon name='star' color= '#ccc'  size={15} style={{marginRight:2}}/>
                  <Icon name='star' color= '#ccc'  size={15} style={{marginRight:2}}/>
              </View>
              }
              if(X.rate >= 2 && X.rate < 3 ){
                  STARS = 
                  <View style={{flexDirection:'row', alignItems:'center', marginTop:5, marginBottom:5}}>
                      <Icon name='star' color='#ffce00' size={15} style={{marginRight:2}}/>
                      <Icon name='star' color='#ffce00'  size={15} style={{marginRight:2}}/>
                      {X.rate === 2 ? <Icon name='star'  color='#ccc' size={15} style={{marginRight:2}}/> :
                      <View style={{marginRight:2}}>
                              <Icon name='star'  color='#ccc' size={15}  />
                          <Icon name='star-half'  color='#ffce00' size={15} style={{position:'absolute', top: 0}} />
                      </View>    
                      }
                      
                      <Icon name='star' color='#ccc'  size={15} style={{marginRight:2}}/>
                      <Icon name='star' color='#ccc'  size={15} style={{marginRight:2}}/>
                  </View>
                  }
              if( X.rate >= 3 && X.rate < 4){
                  STARS = 
                  <View style={{flexDirection:'row', alignItems:'center', marginTop:5, marginBottom:5}}>
                      <Icon name='star' color='#ffce00' size={15} style={{marginRight:2}}/>
                      <Icon name='star' color='#ffce00' size={15} style={{marginRight:2}}/>
                      <Icon name='star' color='#ffce00'  size={15} style={{marginRight:2}}/>
                      {X.rate === 3 ? <Icon name='star'  color='#ccc' size={15} style={{marginRight:2}}/> :
                      <View style={{marginRight:2}}>
                              <Icon name='star'  color='#ccc' size={15}  />
                          <Icon name='star-half'  color='#ffce00' size={15} style={{position:'absolute', top: 0}} />
                      </View>    
                      }
                      <Icon name='star' color='#ccc'  size={15} style={{marginRight:2}}/>
                  </View>
                  }
              if( X.rate >= 4 && X.rate < 5){
                  STARS = 
                  <View style={{flexDirection:'row', alignItems:'center', marginTop:5, marginBottom:5}}>
                      <Icon name='star' color='#ffce00' size={15} style={{marginRight:2}}/>
                      <Icon name='star' color='#ffce00' size={15} style={{marginRight:2}}/>
                      <Icon name='star' color='#ffce00'  size={15} style={{marginRight:2}}/>
                      <Icon name='star' color='#ffce00'  size={15} style={{marginRight:2}}/>
                      {X.rate === 4 ? <Icon name='star'  color='#ccc' size={15} style={{marginRight:2}}/> :
                      <View style={{marginRight:2}}>
                              <Icon name='star'  color='#ccc' size={15}  />
                          <Icon name='star-half'  color='#ffce00' size={15} style={{position:'absolute', top: 0}} />
                      </View>    
                      }
                      
                  </View>
               }
       
        }
    
        return STARS
    }


    render(){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                
                <View style={styles.top}>
                    <TouchableWithoutFeedback onPress={() => this.props.navigation.goBack()}>
                    <View style={{flexDirection:'row',alignItems:'center', justifyContent:'space-between'}}>
                        <Icon name="chevron-left" size={25} color='white' style={{ marginLeft:10}}/>
                        <Text style={{color:'#fff', fontFamily: "ProximaNova-Bold", fontSize:25 }}>Workshops</Text>
                        <View style={{width:20}} />
                    </View>
                    </TouchableWithoutFeedback>
            

                    <View style={styles.searchBar}>
                        <TextInput style={{marginRight:15, width:'80%', color:'#000000', fontFamily:'ProximaNova-Regular'}}
                            placeholder='Search for a merchant'
                            placeholderTextColor = 'grey'
                            onChangeText={(x) => this.setState({searchText:x})}
                            value={this.state.searchText}
                            />
                        {this.state.searchText === '' ?
                            <AntIcon name='search1' size={15} color='#6e6d6d' />
                                :
                            <TouchableWithoutFeedback onPress={this.handleSubmit} >
                             <AntIcon name='close' size={15} />
                           </TouchableWithoutFeedback>
                        }
                        </View>

                </View>
                
                    
                <View style={{ margin: 15,}} >
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                        {this.categoryList()}
                        
                    </ScrollView>
                </View>

                    {this.workshopList()}
                    
                </View>
            </SafeAreaView>
        )
    }
}

WorkshopListing = inject('mobx_auth','mobx_retail','mobx_carkey', 'mobx_config')(observer(WorkshopListing))
export default WorkshopListing

const styles = StyleSheet.create({
    top:{
        backgroundColor:'#080880',
        padding:20,
        borderBottomLeftRadius:25,
        borderBottomRightRadius: 25,
        
    },
    searchBar:{backgroundColor:'#fff', borderWidth:1,padding:(Platform.OS === 'ios') ? 10 : 0, paddingLeft:(Platform.OS === 'ios') ? 10 : 10, paddingRight:(Platform.OS === 'ios') ? 10 : 10,borderRadius:24, borderColor:'#ccc', flexDirection:'row', alignItems:'center', justifyContent:'space-between', flexShrink:1, marginTop:20},
    catTitle:{
        backgroundColor:'#fff',
        padding:10,
        paddingLeft:10, 
        paddingRight:10,
        borderRadius:15,
        margin:4,
          ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 2,
              shadowOffset: { width: 0, height: 1 },
              shadowOpacity: 0.3,
            },
            android: {
              elevation: 2
            },
          }),
    },
    colorTitle:{
        backgroundColor:'#080880',
        padding:10,
        paddingLeft:10, 
        paddingRight:10,
        borderRadius:15,
        margin:4,
          ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 2,
              shadowOffset: { width: 0, height: 1 },
              shadowOpacity: 0.3,
            },
            android: {
              elevation: 2
            },
          }),
    },
    box:{
        borderRadius: 15, 
        margin:15,
        padding:15,
        paddingBottom:0,
        backgroundColor:'#fff',
        marginTop:0,
        // flexDirection:'row',
        // alignItems:'center',
        flexShrink:1,
       
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 3 },
              shadowOpacity: 0.3,
            },
            android: {
              elevation: 3
            },
          }),
    },

    outerLayer: {
        backgroundColor:'#fff',
        borderWidth:0.5, 
        borderColor:'#080880',
        borderRadius:10,
        padding:10,
        paddingTop:8, 
        paddingBottom:8,
        marginRight:8,
        flexDirection:'row',
        // width:36,
        // height:36,
        alignItems:'center',
        justifyContent:'center',
    },
    innerModal: {     
        borderTopLeftRadius: 10,
        borderTopRightRadius:10,
        width:Dimensions.get('window').width,
        position:'absolute',
        bottom:0,
        backgroundColor: '#fff',
            ...Platform.select({
                ios: {
                    shadowColor: '#000',
                    shadowRadius: 2,
                    shadowOffset: { width: 0, height: 3 },
                    shadowOpacity: 0.6,
                },
                android: {
                    elevation: 2
                },
            }),
    },

})
