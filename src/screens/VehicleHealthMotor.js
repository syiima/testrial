import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,TouchableWithoutFeedback, ImageBackground,RefreshControl,FlatList, Alert,Dimensions, TouchableOpacity, ScrollView, Image, Button, TextInput, Picker} from 'react-native';
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/FontAwesome"
import { PieChart, ProgressCircle } from 'react-native-svg-charts'
import Moment from 'moment';

import Loader from "../components/Loader";
import emptyPage from '../assets/icons/emptyBooking.png'
import Header from '../components/Header.js'

class VehicleHealthMotor extends React.Component{
    constructor(){
        super()
        this.state = {
            car_health: '',
            refreshing: false,
            screen:1,
      
            upah_data:'',
            date: '',
            car_info: '',
      
            finalPercent: 0, finalInt : 0, finalUH: 0, finalT:0,
      
            underV:'',
            underH:'',
            tyreS:'',
      
            isLoading:true,
            collapsed: true,
        }
    }

    componentDidMount(){
        this.fetchVehicle()
    }

    fetchVehicle(){
        let that = this
        let FUID = this.props.mobx_auth.FUID
        let PLATE = this.props.mobx_carkey.PLATE
        let DATA = [];
        let DATA2 = [];
        let DATA3 = [];
        let DATA4 = [];
        
        firebase.database().ref(`plate_number/${PLATE}/motor_health`).once('value', (snapshot) => {
            if(snapshot.exists()){
                let x = snapshot.val()

                if(x.kickstand !== undefined){
                    let keys = Object.keys(snapshot.val())
                    let sumT = 0
                    let sumInt = 0
                    let sumUV = 0
                    let sumUH = 0
                    let sum = 0

                    let CAR_INFO = {
                        VIN_number: snapshot.val().VIN_number,
                        next_service_date:snapshot.val().next_service_date,
                        roadtax_expiry_date:snapshot.val().roadtax_expiry_date,
                        extra_notes: snapshot.val().extra_notes !== '' ? snapshot.val().extra_notes : 'No notes to display',
                    }

                    keys.forEach((snp) => {
                        if(snp !== 'next_service_date' && snp !== 'roadtax_expiry_date' && snp != 'VIN_number'  && snp !== 'timestamp' && snp !== 'extra_notes' && snp !== 'customer_car_mileage' && snp !== 'customer_next_car_mileage'){
                            // console.warn("apa", snp);
                            let a = {}
                            let NAME = snp
                                
                            a.condition = parseFloat(snapshot.val()[snp])
                            a.name = NAME
                            sum += parseInt(a.condition)
                            sumInt += parseInt(a.condition)
                          
                            DATA.unshift(a)
          
                        }
          
          
                    })
          
                    finalSum = parseFloat((sum / (6 * 5) * 100).toFixed(0))
                
                    console.warn('yoo', DATA);
                
                    that.setState({
                        upah_data: DATA,
                        car_info: CAR_INFO,
                        screen: 2,
                        refreshing: false,
                        isLoading:false,

                        finalPercent : finalSum,
                        
                    })

                    this.rearrange();

                } else {
                    that.setState({refreshing: false, isLoading: false, screen:1})
                }

               
            } else{
                that.setState({refreshing:false, isLoading: false, screen:1})
            }
        })
    }

    rearrange = () =>{
        //arrange alphabetically
        var dataToArrange = this.state.upah_data;
    
        dataToArrange.sort(function(a, b) {
          var nameA = a.name.toUpperCase(); // ignore upper and lowercase
          var nameB = b.name.toUpperCase(); // ignore upper and lowercase
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }
    
          // names must be equal
          return 0;
        });
    
        this.setState({finaldata: dataToArrange})
    }

    Refreshplz = () => {
        this.setState({refreshing: true});
        this.fetchVehicle();
    }
      

    //==== render ===== 

    renderItem = ({item}) => (
        <View style={styles.circleBox}>
        <Text style={{textAlign:'center',  marginRight: 5}}>
        {item.name !== undefined ?
            item.name.split('_').map((string,index) => (<Text key={index}>{string.charAt(0).toUpperCase() + string.slice(1)}{' '}</Text>)) :
            null
        }
        </Text>
        {item.condition === 5 ?
        <View style={{flexDirection:'row', alignItems:'center'}}>
            <Icon name="circle" color="#15ba12" size={14} style={{marginRight:4}} />
            <Icon name="circle" color="#15ba12" size={14} style={{marginRight:4}} />
            <Icon name="circle" color="#15ba12" size={14} style={{marginRight:4}} />
            <Icon name="circle" color="#15ba12" size={14} style={{marginRight:4}} />
            <Icon name="circle" color="#15ba12" size={14} style={{marginRight:4}} />

        </View>
        :

        item.condition === 2 || item.condition === 3 ||  item.condition === 4 ?
        <View style={{flexDirection:'row', alignItems:'center'}}>
            <Icon name="circle" color="#e9c91d" size={14} style={{marginRight:4}} />
            <Icon name="circle" color="#e9c91d" size={14} style={{marginRight:4}} />
            <Icon name="circle" color="#e9c91d" size={14} style={{marginRight:4}} />
            <Icon name="circle-o" color="#000" size={14} style={{marginRight:4}} />
            <Icon name="circle-o" color="#000" size={14} style={{marginRight:4}} />

        </View>
        :
        item.condition === 1 ?
            <View style={{flexDirection:'row', alignItems:'center'}}>
            <Icon name="circle" color="tomato" size={14} style={{marginRight:4}} />
            <Icon name="circle-o" color="#000" size={14} style={{marginRight:4}} />
            <Icon name="circle-o" color="#000" size={14} style={{marginRight:4}} />
            <Icon name="circle-o" color="#000" size={14} style={{marginRight:4}} />
            <Icon name="circle-o" color="#000" size={14} style={{marginRight:4}} />
            </View>
        :

            <View style={{flexDirection:'row', alignItems:'center'}}>
            <Icon name="circle-o" color="#000" size={14} style={{marginRight:4}} />
            <Icon name="circle-o" color="#000" size={14} style={{marginRight:4}} />
            <Icon name="circle-o" color="#000" size={14} style={{marginRight:4}} />
            <Icon name="circle-o" color="#000" size={14} style={{marginRight:4}} />
            <Icon name="circle-o" color="#000" size={14} style={{marginRight:4}} />
            </View>

    }
    </View>

    )

    render_color = (X) => {
        let ITEM = X
        let color_disp

            if(ITEM <=20 ){ color_disp = 'red'}
            if(ITEM >= 21){color_disp = 'rgb(233, 201, 29)'}
            if(ITEM >= 80){color_disp = '#008000'}

        return color_disp

    }


    render(){

        let content;

        if(this.state.screen === 1){
            content=
                <View style={{flex:1}}>

                    <Header backButton = {true}  headerTitle = 'Health & Stats' onPress={() => this.props.navigation.goBack()} />

                    <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                        <Image source={emptyPage} style={{height: 180, width:180, resizeMode:'contain'}} />
                        <Text style={{fontSize:22, fontFamily: "ProximaNova-Bold", marginTop:15, marginBottom:10}}>No History</Text>
                        <Text>Get your motorcycle serviced to see Health & Stats</Text>
                    </View>
                </View>
        }
        if(this.state.screen === 2){
            content=
            <ScrollView
              style={{flex:1,}} refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this.Refreshplz}
                />
            }
              showsVerticalScrollIndicator={false}>
      
                <View style={styles.top}>
                    <TouchableWithoutFeedback onPress={() => this.props.navigation.goBack(null)}>
                        <View style={{flexDirection:'row', alignItems:'center', margin:15}}>
                            <Icon name="chevron-left" color="white" size={25}  style={{marginRight:20, marginLeft:10}}/>
                        <Text style={{color:'#fff', fontWeight:'bold', fontSize:20, }}>Health & Stats</Text>
                        </View>
                    </TouchableWithoutFeedback >
        
        
                    <View style={{alignItems:'center', justifyContent:'center', marginTop:15, paddingBottom:15}}>
            
                        <View >
                            <ProgressCircle
                                style={ styles.progCircle }
                                progress={this.state.finalPercent /100}
                                strokeWidth={22}
                                startAngle={ -Math.PI * 0.75 }
                                endAngle={ Math.PI * 0.75 }
                                progressColor={this.render_color(this.state.finalPercent)}
                            />
                            <View style={{position:'absolute', top:'40%', left:'15%', }} >
                                <Text style={{fontSize:38,textAlign:'center', fontFamily: "ProximaNova-Bold", color:'#fff'}}>{this.state.finalPercent}%</Text>
                                <Text style={{color:'#fff', fontWeight:'bold', fontSize:13, marginTop:6,}}>Health Score</Text>
                            </View>
                            
                        </View>
                    </View>
        
                </View>
      
                <View style={{margin:15}}>
                    <View style={{...styles.box, backgroundColor:'#fff'}} >
                        <View style={{flexDirection:'row', justifyContent:'space-between', marginBottom:15}}>
                            <Text style={{fontWeight:'bold'}} >Extra Notes</Text>
                            <Text style={{fontStyle:'italic',fontSize:13, color:'#080880'}} >Last Updated: {Moment(this.state.car_info.timestamp).format('DD-MMM-YYYY')} </Text>
                        </View>
                        {this.state.car_info.extra_notes !== ''? <Text style={{fontWeight:'bold', color:'#080880'}} >{this.state.car_info.extra_notes} </Text> : <Text style={{fontWeight:'bold', color:'#080880'}}>No extra notes </Text> }

                    </View>
        
                    <Text style={{ fontFamily: "ProximaNova-Bold",textAlign:'center', fontSize:20, marginTop:20, marginBottom:15}}>SUMMARY</Text>

                    <View style={styles.outerBox}>
                        <View style={styles.sideText}>
                            <Text numberOfLines={1} style={styles.innerText} >DETAILS</Text>
                        </View>
                        <View style={styles.boxInfo}>
                            <FlatList
                                data={this.state.finaldata}
                                listKey="interior"
                                contentContainerStyle={styles.flatStyle}
                                renderItem= {this.renderItem}
                            />
                        </View>
            
                    </View>
            
                </View>
                
              </ScrollView>
      
              }
      
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                
                <Loader loading={this.state.isLoading} />

                {content}

                </View>
            </SafeAreaView>
        )
    }
}

VehicleHealthMotor = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(VehicleHealthMotor))
export default VehicleHealthMotor

const styles = StyleSheet.create({
    top:{
        backgroundColor:'#080880',
        borderBottomLeftRadius:50,
        width:'100%'
    },
    topempty:{
        backgroundColor:'#080880',
        padding:20,
        borderBottomLeftRadius:50,
        flexDirection:'row',
        alignItems:'center'
    },
    box:{
        borderRadius: 10,
        padding:20,
        marginBottom:20,
        ...Platform.select({
            ios: {
                shadowColor: "#000",
                shadowOffset: {width: 0, height: 2,},
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
            },
            android: {
              elevation: 5
            },
        }),
    },
    mileBox :{
        borderRadius:10,
        alignItems:'center',
        justifyContent:'center',
        width:'48%',
        padding:15,
        ...Platform.select({
            ios: {
                shadowColor: "#000",
                shadowOffset: {width: 0, height: 2,},
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
            },
            android: {
              elevation: 5
            },
        }),
    },
    innerContent: {
        // flexWrap:'wrap', flexShrink:1,
        borderRadius:15,
        padding:10,
        marginTop:15,
        marginLeft:3, marginRight:3,
        flexDirection:'row',
        alignItems:'center',
        backgroundColor:'white',
        ...Platform.select({
            ios: {
                shadowColor: "#000",
                shadowOffset: {width: 0, height: 2,},
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
            },
            android: {
              elevation: 5
            },
        }),
    
    },
    smolCircle: {marginLeft:-10, marginRight:5, width: Dimensions.get('window').width / 5, height:Dimensions.get('window').width / 5, alignItems:'center', justifyContent:'center'},
    insideC:{width: Dimensions.get('window').width / 6, height:Dimensions.get('window').width / 6,},
    progCircle:{
        marginTop:15,width: Dimensions.get('window').width / 2, height:Dimensions.get('window').width / 2,
        ...Platform.select({
            ios: {
                shadowColor: "#000",
                shadowOffset: {width: 0, height: 2,},
                shadowOpacity: 0.23,
                shadowRadius: 2.62,
            },
            android: {
              elevation: 4
            },
        }),
    },
    percentS:{position:'absolute', top:'50%', left:'50%', marginLeft: -(Dimensions.get('window').width / 6)/6, marginTop:-(Dimensions.get('window').width / 6)/8},
    outerBox:{
        marginLeft:15,
        flexDirection:'row',    marginBottom:15,
        borderTopLeftRadius:20,
        borderBottomLeftRadius:20,
    },
    boxStyle:{
        backgroundColor: 'white',
        // flexDirection:'row',
        marginLeft:15,
        marginRight:15,
        marginBottom:15,
        borderBottomLeftRadius:10,
        borderBottomRightRadius:10,
        padding:10,

    },
    sideText:{
        // backgroundColor:'#d9ebf2',
        alignItems:'center',justifyContent:'center',
        width: 50,
    },
    innerText:{
        textAlign:'right',
        transform: [
            { rotate: '270deg' },
            // { translateX: -OFFSET },
            // { translateY: OFFSET }
        ],
        // color:'white',
        width: 150,
        fontFamily: "ProximaNova-Bold"
    },
    boxInfo:{
        backgroundColor:'white',
        alignSelf:'stretch',
        flex:8,
        // width:'100%',
        ...Platform.select({
            ios: {
                shadowColor: "#000",
                shadowOffset: {width: 0, height: 1,},
                shadowOpacity: 0.22,
                shadowRadius: 2.22,
            },
            android: {
              elevation: 3
            },
        }),
         borderTopLeftRadius:20,
        borderBottomLeftRadius:20,
        paddingRight:15,
    
    },
    flatStyle:{
        backgroundColor: '#fff',
        // borderWidth:StyleSheet.hairlineWidth,
            // borderBottomWidth:0,
        flex:8,
        // marginRight:15,
        borderBottomLeftRadius:10,
        borderTopLeftRadius:10,
    },
    circleBox:{
        backgroundColor:'#fff',
       
        flex:1,
        paddingTop:20,
        paddingBottom:20,
        paddingLeft:15,
        paddingRight:15,
        // marginLeft:16,
        marginRight:16,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomLeftRadius:20,
        borderTopLeftRadius: 20,
    },
    
    
    

})