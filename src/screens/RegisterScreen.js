import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,StatusBar,ActivityIndicator,KeyboardAvoidingView,Linking, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/AntDesign"
import { cloud_serv_provideradmin_sandbox, cloud_serv_provideradmin_production, cloud_serv_user_sandbox, cloud_serv_user_production } from "../util/FirebaseCloudFunctions.js"
import AsyncStorage from '@react-native-async-storage/async-storage';
import CheckBox from 'react-native-check-box';

import Header from '../components/Header.js'
import FormTextInput from '../components/FormTextInput'
import Button from '../components/Button'
import PasswordTextInput from "../components/PasswordTextInput.js"
import PhoneTextInput from "../components/PhoneTextInput.js"

class RegisterScreen extends React.Component{
    constructor(){
        super()
        this.state = {
            existing_code :'',
            referal_code :'',
            relogin :0,
            loading: false,
            name: '',
            email: '',
            phone: '',
            password :'',
            confirm_password: '',
            isChecked: false,
            hidePassword: true

        }
    }
    
    componentDidMount(){
        this.generateToken()
        this.getRefCode()
    }
    
    
    generateToken = async() => {
        let urlP, urlU;
        if(this.props.mobx_config.config === 0) {
           urlP = cloud_serv_provideradmin_sandbox.createCSRFP
           urlU = cloud_serv_user_sandbox.createCSRFU
        }
        if(this.props.mobx_config.config === 1) {
           urlP = cloud_serv_provideradmin_production.createCSRFP
           urlU = cloud_serv_user_production.createCSRFU
        }
    
        let finalP = urlP + '?device=mobile';
        let finalU = urlU + '?device=mobile';
    
        await fetch(finalP)
        .then( res => res.json())
        .then(res =>  {
          let CSRFTokenP = res.data.CSRFTokenP
          let CSRFSecretP = res.data.CSRFSecretP
    
          let tokenP = AsyncStorage.setItem('CSRFTokenP', CSRFTokenP)
          let secretP = AsyncStorage.setItem('CSRFSecretP', CSRFSecretP)
    
          // console.warn('PPPPPPPPP', tokenP, secretP);
        })
    
        await fetch(finalU)
        .then( res => res.json())
        .then(res =>  {
          let CSRFTokenU = res.data.CSRFTokenU
          let CSRFSecretU = res.data.CSRFSecretU
    
          let tokenU = AsyncStorage.setItem('CSRFTokenU', CSRFTokenU)
          let secretU = AsyncStorage.setItem('CSRFSecretU', CSRFSecretU)
    
          // console.warn('UUUUUU', tokenU, secretU);
        })
        
    }

    getRefCode(){
        let ALL = []
        firebase.database().ref(`referral_code`).once('value').then((snp) => {
          if(snp.exists()){
            let KEY = Object.keys(snp.val())    
            this.setState({existing_code: KEY})
          }
        })

        firebase.database().ref(`consts/`).once('value').then((snp) => {
            if(snp.exists()){
              let X = snp.val().relogin
              this.setState({relogin: X})      
            }
         })

    }

    checkReferal = () => {
        let CODE = (this.state.referal_code).toUpperCase()
        let ALL = this.state.existing_code
        if(ALL.length !== 0 ){
            if(CODE !== ''){
                if(ALL.indexOf(CODE) !== -1){
                this.checkInfos()
                } 
                if(ALL.indexOf(CODE) === -1){
                Alert.alert('Sorry', 'The referral code that you entered is invalid. Please make sure you typed in the correct code')
                return
                }
        
            } else {
                this.checkInfos()
            }
    
        } 
        if(ALL.length === 0 && CODE !== ''){
          Alert.alert('Sorry', 'The referral code that you entered is invalid. Please make sure you typed in the correct code')
          return
        }    
    }

    checkInfos = () => {
        let NAME = this.state.name
        let PHONE = this.state.phone
        let EMAIL = this.state.email
        let PASSWORD = this.state.password
        let CONFIRM_PASSWORD = this.state.confirm_password
        let CHECKED = this.state.isChecked
    
    
        if(NAME === ''){
          alert("Name cannot be empty")
          return;
        }
    
        if(PHONE === ''){
          alert("Phone cannot be empty")
          return;
        }
        if(EMAIL === ''){
          alert("Email cannot be empty")
          return;
        }
        if(PASSWORD === ''){
          alert("Password cannot be empty")
          return;
        }
        if(CONFIRM_PASSWORD === ''){
          alert("Confirm Password cannot be empty")
          return;
        }
        if(CONFIRM_PASSWORD !== PASSWORD){
          alert("Password is not same")
          return;
        }
        if(CHECKED === false){
          Alert.alert("Registration Failed", "Please accept the terms and conditions")
          return;
        } 
    
        if(NAME !== '' && PHONE !== '' && EMAIL !== '' && PASSWORD !== '' && CHECKED === true ){
            this.setState({loading: true})
            this.register()
        }
    }


    register = () => {
        let that = this;
        let NAME = this.state.name
        let PHONE = this.state.phone
        let EMAIL = this.state.email
        let PASSWORD = this.state.password
        let CONFIRM_PASSWORD = this.state.confirm_password
        let CHECKED = this.state.isChecked
        
        const {email, confirm_password} = this.state;
        if(this.state.email && (this.state.password === this.state.confirm_password)){
    
          firebase.auth().createUserWithEmailAndPassword(email, confirm_password).then((userData) => {
            // Alert.alert('Hooray!!', " Your registration is successful!");
            let uid = userData.user.uid;
            let email = userData.user.email
            let name = NAME;
            let Phone = PHONE;
    
            that.props.mobx_auth.setCheck(2);
            // console.warn('setcheck', this.props.mobx_auth.check);
    
            firebase.database().ref("users/" + uid).update({
                name: NAME,
                Phone: '+60' + PHONE,
                email: EMAIL,
                register_timestamp: firebase.database.ServerValue.TIMESTAMP,
                relogin: this.state.relogin,
                nickname: '',
                app_version: this.props.mobx_config.Version,

            })
    
            if(this.state.referal_code !== ''){
              let CODE = (this.state.referal_code).toUpperCase()
              
              //update referral
              firebase.database().ref(`referral_code/${CODE}/referral/${uid}`).update({
                register: true,
                vehicle: false,
                timestamp_registered: Date.now()
              })
    
              //update on user
              firebase.database().ref(`users/${uid}/referral_code`).update({
                registration_code: CODE
              })
              
            }
    
          }).catch((err) => {
            this.setState({loading: false})
          })
        }
    }

    back = () => {
        this.setState({name: '', Phone:'', email: '', password:'', confirm_password:''})
        this.props.navigation.navigate('LoginScreen')
    }

    managePasswordVisibility = () => {
      this.setState({ hidePassword: !this.state.hidePassword });
    }

    handleChange = (text) => {
      let T = text.replace(/[`~!@#$%^&*()_|+\-=?\s;:'",.<>\{\}\[\]\\\/]/gi, '')
      let X = T.trim()
      X = X.toUpperCase()
      
      this.setState({referal_code:X})
  }

    render(){
        let sign_button
        if(this.state.loading === false){
            sign_button = 
            <TouchableOpacity onPress={this.checkReferal} style={styles.regbutton}>
                <Text style={{fontSize: 18,fontFamily: "ProximaNova-Bold",textAlign: 'center',color: '#fff'}}>Sign me up</Text>
            </TouchableOpacity>
        } else {
            sign_button =
            <View style={{...styles.regbutton, flexDirection:'row', alignItems:'center', justifyContent: 'center'}}>
                <ActivityIndicator style={{ height: 20, width: 20, alignSelf:'center', marginRight:10}} size="small" color="#fff" />
                <Text style={{fontSize: 18,fontFamily: "ProximaNova-Bold",textAlign: 'center',color: '#fff'}}>Signing up... </Text>
            </View>
        }

        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
              <StatusBar barStyle="light-content" backgroundColor='#1d1d1d'/>
                <KeyboardAvoidingView
                behavior={(Platform.OS === 'ios') ? "padding" : null} enabled
                style={{ flex: 1, backgroundColor:'#fafdff', justifyContent:'center'}}
                >
                <ScrollView showsVerticalScrollIndicator={false} style={{flex:1}}>

                <View style={{backgroundColor: '#fafdff', padding:20, flex:1,}}>
                    
                    <View style={{marginBottom: 50}}>
                    <Text style={styles.mainTitle} >Sign up </Text>
                    <Text style={styles.smolTitle}>To start using the SERV app</Text>
                    </View>
                
                    <FormTextInput 
                        title= 'NAME'
                        value={this.state.name}
                        placeholder= 'eg: Adilah, Mike'
                        onChangeText={(x) => this.setState({name:x})}
                        onSubmitEditing={() => this.phone.focus()}
                    />

                    <PhoneTextInput 
                        title= 'PHONE NUMBER'
                        value={this.state.phone}
                        placeholder= 'eg: 12456904'
                        onChangeText={(x) => this.setState({phone:x})}
                        inputRef={ref => this.phone = ref}
                        onSubmitEditing={() => this.email.focus()}
                    />

                    <FormTextInput 
                        title= 'EMAIL'
                        value={this.state.email}
                        placeholder= 'eg: myemail@gmail.com'
                        keyboardType='email-address'
                        onChangeText={(x) => this.setState({email:x})}
                        inputRef={ref => this.email = ref}
                        onSubmitEditing={() => this.password.focus()}
                    />
                    <PasswordTextInput 
                          title= 'PASSWORD'
                          value={this.state.password}
                          placeholder= 'password here'
                          onChangeText={(x) => this.setState({password:x})}
                          hidePassword ={this.state.hidePassword}
                          onPress={this.managePasswordVisibility}
                          bottom={true}
                          inputRef={ref => this.password = ref}
                          onSubmitEditing={() => this.confirmpassword.focus()}
                      />
                      <PasswordTextInput 
                          title= 'CONFIRM PASSWORD'
                          value={this.state.confirm_password}
                          placeholder= 'reconfirm password'
                          onChangeText={(x) => this.setState({confirm_password:x})}
                          hidePassword ={this.state.hidePassword}
                          onPress={this.managePasswordVisibility}
                          bottom={false}
                          inputRef={ref => this.confirmpassword = ref}
                          onSubmitEditing={() => this.referral.focus()}
                      />

                    <FormTextInput 
                        title= 'REFERRAL CODE '
                        value={this.state.referal_code}
                        placeholder= 'got referral code? (Optional)'
                        keyboardType={Platform.OS === 'ios' ? 'default' : 'visible-password'}
                        onChangeText={this.handleChange}
                        returnKeyType='done'
                        inputRef={ref => this.referral = ref}
                    />
                    
                    <View style={{flexDirection:'row',margin:10, marginLeft:0}}>
                        <CheckBox style={{marginRight: 5}}
                            onClick={ () => {this.setState({isChecked:!this.state.isChecked})}}
                            isChecked={this.state.isChecked}
                            checkBoxColor="#e85b1a"/>
                        <Text style={{color:'#686868', }}>By creating an account you agree to our <Text onPress={()=> Linking.openURL('https://serv.my/terms-conditions/')} style={{color:'#686868', textDecorationLine:'underline'}}>Terms & Conditions and Privacy Policy</Text></Text>
                    </View>

                    {sign_button}

                    <TouchableOpacity onPress={this.back} style={{padding:10, marginTop:15}}>
                        <Text style={{color:'#686868',textAlign:'center', fontFamily: "ProximaNova-Bold"}}> Already have an account? <Text style={{fontFamily: "ProximaNova-Bold",color:"#080880",}}> Login now</Text> </Text>
                    </TouchableOpacity>

                </View>
                  <View style= {{height:30}} />
                </ScrollView>
                </KeyboardAvoidingView>
                
            </SafeAreaView>
        )
    }
}

RegisterScreen = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(RegisterScreen))
export default RegisterScreen

const styles = StyleSheet.create({
    mainTitle :{fontSize:35, fontFamily:'ProximaNova-Bold',marginBottom:10},
    smolTitle:{fontSize:16},
    regbutton:{
        alignSelf: 'stretch',
        marginTop:10,
        marginBottom: 10,padding: 15,borderRadius: 5,backgroundColor: '#080880',
        ...Platform.select({
          ios: {
            shadowColor: '#000',
            shadowRadius: 3,
            shadowOffset: { width: 0, height: 2},
            shadowOpacity: 0.4,
          },
          android: {
            elevation: 3
          },
        }),
      },
    
})