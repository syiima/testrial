import React, {Component} from 'react';
import { ImageBackground } from 'react-native';
import { View,Dimensions, Image, SafeAreaView} from 'react-native';

import Splash from "../assets/splashScreen.jpg"

export default class Splashscreen extends React.Component{
    render(){
        return(
            <ImageBackground
                source={Splash}
                style={{
                    height: Dimensions.get('window').height,
                    width: Dimensions.get('window').width
                 }}
            />
        )
    }
}

