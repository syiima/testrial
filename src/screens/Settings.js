import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text,Modal, View, TouchableWithoutFeedback, TouchableOpacity, ScrollView,Alert,TextInput, Image, Dimensions} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';

import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-async-storage/async-storage';

import Header from '../components/Header.js'
import Button from '../components/Button'
import MiddleModal from "../components/MiddleModal.js";
import FormTextInput from "../components/FormTextInput.js"

import FAQ from '../assets/icons/faq.png';
import Out from '../assets/icons/exit.png';
import Refunds from '../assets/icons/terms.png';
import Policy from '../assets/icons/policy.png';
import Help from '../assets/icons/help.png';
import Bug from '../assets/icons/bug.png';
import Feedback from '../assets/icons/feedback.png';
import Tutorial from '../assets/icons/tutorial.png'

const WIDTH = Dimensions.get('window').width - 30

class Settings extends React.Component{
    constructor(){
        super()
        this.state = {
            modalLogout: false,
            modalBug:false,
            description: '',
            screenshot:'',
      
            modalF:false,
            suggestion:'',
            progressVal :0,
            percentage: ''
        }
    }


    openWebview = (x) => {
        let LINK = x

        this.props.navigation.navigate('Webview', {link: LINK})
    }

    clearToken = async() => {
        let tokenP = await AsyncStorage.removeItem('CSRFTokenP')
        let secretP = await AsyncStorage.removeItem('CSRFSecretP')
    
        let tokenU = await AsyncStorage.removeItem('CSRFTokenU')
        let secretU =await  AsyncStorage.removeItem('CSRFSecretU')
      }
    
    logout = () => {
    firebase.analytics().logEvent('button_pressed', { name: 'logout'})

        let that = this;
        firebase.auth().signOut().then((hmm) =>{
        this.clearToken()
        // console.log("Signing out")
        that.props.mobx_auth.setCheck(1);
        }).catch( (err) => {
        alert(err)
        })
    }


    // =============== FUNC ===================
  
    openLibrary = () => {
        const options = {quality:1, maxWidth: 800, maxHeight: 800}

        ImagePicker.launchImageLibrary(options, (response) => {
            if(response.didCancel){
            } 
            if(!response.didCancel && !response.error) {
            this.setState({
                screenshot: response.uri,
                // imagey: arrImage
            });
            // console.warn("apa", this.state.new_picture);
            }
        });

    }

    _bugReport = () => {
        firebase.analytics().logEvent('button_pressed', { name: 'report_bug'})
        let that = this
        let FUID = this.props.mobx_auth.FUID
        let EMAIL = this.props.mobx_auth.EMAIL
        let NAME = this.props.mobx_auth.NAME
        let PHONE = this.props.mobx_auth.PHONE

        let DESC = this.state.description
        let SS = this.state.screenshot

        let DATA = {
            user_id: FUID,
            timestamp: firebase.database.ServerValue.TIMESTAMP,
            suggestion: DESC,
            email: EMAIL,
            name: NAME,
            phone: PHONE,
            platform: Platform.OS,
            version: this.props.mobx_config.Version,
        }

        if(DESC === ''){
        Alert.alert('Please write the details of the bug that happened', 'Or else we won\'t be able to fix it :( ')
        return
        } else {
        KEY = firebase.database().ref(`bug_report/`).push().key;

        if(SS !== ''){
            const refFile= firebase.storage().ref().child('bug_report').child(KEY);
        
            let uploadTask = refFile.putFile(SS);

            uploadTask.on('state_changed', function(snapshot){
            let progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            let yawn  = progress.toFixed(2)
            that.setState({progressVal: yawn, percentage: progress.toString()})
            console.log('Upload is ' + progress + '% done');
            switch (snapshot.state) {
                case 'running':
                break;
                case 'success':
                snapshot.ref.getDownloadURL()
                .then((downloadURL) => {
                    // console.warn('ehh', downloadURL);
                    firebase.database().ref('bug_report/' + KEY).update(DATA)
                    firebase.database().ref('bug_report/' + KEY).update({screenshot: downloadURL});
                })
                .then(() => {
                    that.setState({modalBug: false, description: '', screenshot:'',percentage:'', progressVal:0})
                })
                .then(() => {
                    setTimeout(() => Alert.alert('Thank you!', 'We\'ve received your report.') , 500)
                    
                })
                break;
                default:
                    break;
                }
            })
        } else {
            this.reportTelegram()
            firebase.database().ref(`bug_report/${KEY}`).update(DATA)
            firebase.database().ref(`bug_report/${KEY}`).update({screenshot: ''})
            .then(() => {
            that.setState({modalBug: false, description: '', screenshot:'',  percentage:'', progressVal:0})
            })
            .then(() => {
            setTimeout(() => Alert.alert('Thank you!', 'We\'ve received your report.'), 500);   
            })
        } 
        }
    }

    reportTelegram = () => {
        let TELEGRAM_URL, TELE_TEXT
        let EMAIL = this.props.mobx_auth.EMAIL
        let NAME = this.props.mobx_auth.NAME
        let PHONE = this.props.mobx_auth.PHONE
        let DESC = this.state.description
        let PLATFORM = Platform.OS
        let VERSION = this.props.mobx_config.Version

        if (this.props.mobx_config.config === 0) {
            TELEGRAM_URL = 'https://api.telegram.org/bot272822956:AAEduqgWA9gt6tpxYHKp2h1Opr4ZFNftyNc/sendMessage?chat_id=-1001396604012&text=';
        }
        if (this.props.mobx_config.config === 1) {
            TELEGRAM_URL = 'https://api.telegram.org/bot272822956:AAEduqgWA9gt6tpxYHKp2h1Opr4ZFNftyNc/sendMessage?chat_id=-1001476910252&text='
        }

        if(PLATFORM === 'ios'){
            TELE_TEXT = 'A wild bug report appeared!\n\nUser name : ' + NAME.toUpperCase() + ' \nPhone no : ' + PHONE +  ' \nEmail : ' + EMAIL + ' \nDescription : '  + DESC + ' \nVersion : ' + VERSION + ' [' + PLATFORM + ']'
      
        } else {
            TELE_TEXT = 'A wild bug report appeared!%0A%0AUser name : ' + NAME.toUpperCase() + '%0APhone no : ' + PHONE + '%0AEmail : ' + EMAIL + '%0ADescription : ' + DESC + '%0AVersion : ' + VERSION + ' [' + PLATFORM + ']'
        }


        let final = TELEGRAM_URL + TELE_TEXT
        fetch(final)
    }


    feedback = () => {
        firebase.analytics().logEvent('button_pressed', { name: 'feedback'})
        let FUID = this.props.mobx_auth.FUID
        let EMAIL = this.props.mobx_auth.EMAIL
        let NAME = this.props.mobx_auth.NAME

        let SUGG = this.state.suggestion

        let DATA = {
            user_id: FUID,
            timestamp: firebase.database.ServerValue.TIMESTAMP,
            suggestion: SUGG,
            email: EMAIL,
            name: NAME,
            platform: 'user app',

        }

        if(SUGG === ''){
        Alert.alert('Wait!', 'Did you forget to write in your feedback?')
        return
        } else {
        firebase.database().ref('feature_suggestion/')
        .push(DATA)
        .then(() => {
            this.setState({modalF: false, suggestion:'',})
        })
        .then(() => {
            setTimeout(() => Alert.alert('Thank you!', 'We\'ve received your feedback.'), 500);
            
        })
        }

        
    }


    _renederPerce = () => {
        let yaj
        let PERF = this.state.percentage + '%'
        if(this.state.percentage !== ''){
        yaj = 
            <View style={{height:6, marginTop:10, backgroundColor:'#ccc', borderRadius:4}}>
            <View style={{position:'absolute', backgroundColor: "#8BED4F", width:PERF, height:6, borderRadius:4}} />
            </View>
        }

        return yaj
    }
    
    goBack = () => {
        this.setState({screenshot: '', suggestion:'', description :''})
        this.props.navigation.goBack()
    }
    render(){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                    
                <Header backButton = {true}  headerTitle = 'Settings & Support' onPress={this.goBack} />

                <ScrollView showsVerticalScrollIndicator={false} >

                    <Text style={{padding:15, color:'#000',fontSize:18,marginTop:15, fontFamily: "ProximaNova-Bold"}}> Help & Support</Text>

                    <TouchableOpacity onPress={()=> this.openWebview('http://serv.my/privacy-policy')} 
                        style={styles.boxStyle}>
                    <View style={{flexDirection:'row', alignItems:'center', flex:3, flexShrink:1}}>
                        <Image source={Policy} style={{height:30, width:30,}}/>
                        <View style={{flexShrink:1, marginLeft:10, marginRight:10}}>
                            <Text style={styles.title}>Privacy Policy </Text>
                            <Text style={styles.desc}>Learn how we protect your data and privacy</Text>
                        </View>
                        
                        </View>
                        <Icon style={{paddingRight: 10,}} name="angle-right" size={27} color="#363434" />
                    </TouchableOpacity>


                    <TouchableOpacity  onPress={ ()=> this.openWebview('https://serv.my/refund-policy/') } style={styles.boxStyle}>
                    <View style={{flexDirection:'row', alignItems:'center', flex:3, flexShrink:1}}>
                        <Image source={Refunds} style={{height:30, width:30,}}/>
                        <View style={{flexShrink:1, marginLeft:10, marginRight:10}}>
                            <Text style={styles.title}>Refunds </Text>
                            <Text style={styles.desc}>We strive to deliver the best.Learn how our refund policies work</Text>
                        </View>
                        
                        </View>
                        <Icon style={{paddingRight: 10,}} name="angle-right" size={27} color="#363434" />
                    </TouchableOpacity>

                    <TouchableOpacity onPress={ ()=> this.openWebview('https://serv.my/faq/') } style={styles.boxStyle}>
                        <View style={{flexDirection:'row', alignItems:'center', flex:3, flexShrink:1}}>
                        <Image source={FAQ} style={{height:30, width:30,}}/>
                        <View style={{flexShrink:1, marginLeft:10,marginRight:10}}>
                            <Text style={styles.title}>FAQ </Text>
                            <Text style={styles.desc}>Have a questions? Find the possible answers here</Text>
                        </View>
                        
                        </View>
                        <Icon style={{paddingRight: 10,}} name="angle-right" size={27} color="#363434" />
                    </TouchableOpacity>

                    <TouchableOpacity onPress={()=> this.openWebview('https://serv.my/contact/')} style={styles.boxStyle}>
                        <View style={{flexDirection:'row', alignItems:'center', flex:3, flexShrink:1}}>
                        <Image source={Help} style={{height:17, width:17, marginLeft:6, marginRight:6}}/>
                        <View style={{flexShrink:1, marginLeft:10, marginRight:10}}>
                            <Text style={styles.title}>Help Center </Text>
                            <Text style={styles.desc}>Experiencing issues? Contact us and we’ll guide you through tough times</Text>
                        </View>
                        
                        </View>
                        <Icon style={{paddingRight: 10,}} name="angle-right" size={27} color="#363434" />
                    </TouchableOpacity>


                    <Text style={{padding:15,paddingTop:20, color:'#000',fontSize:18, fontFamily: "ProximaNova-Bold"}}> Improve SERV</Text>
                    <TouchableOpacity onPress={() => this.setState({modalF: true})} style={styles.boxStyle}>
                        <View style={{flexDirection:'row', alignItems:'center', flex:3, flexShrink:1}}>
                        <Image source={Feedback} style={{height:25, width:25,}}/>
                        <View style={{flexShrink:1, marginLeft:15, marginRight:10}}>
                        <Text style={styles.title}>Feedback </Text>
                        <Text style={styles.desc}>Tell us how we can improve your SERV experience</Text>
                        </View>
                        
                        </View>
                        <Icon style={{paddingRight: 10,}} name="angle-right" size={27} color="#363434" />
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.setState({modalBug: true})} style={styles.boxStyle}>
                        <View style={{flexDirection:'row', alignItems:'center', flex:3, flexShrink:1}}>
                        <Image source={Bug} style={{height:25, width:25,}}/>
                        <View style={{flexShrink:1, marginLeft:15, marginRight:10}}>
                        <Text style={styles.title}>Bug Report </Text>
                        <Text style={styles.desc}>A wild bug appeared! Let us know and we’ll get it fixed</Text>
                        </View>
                        
                        </View>
                        <Icon style={{paddingRight: 10,}} name="angle-right" size={27} color="#363434" />
                    </TouchableOpacity>



                    <Text style={{padding:15,paddingTop:20, color:'#000',fontSize:18, fontFamily: "ProximaNova-Bold"}}> About App</Text>
                        <View style={styles.boxStyle}>
                        
                        <View style={{flexDirection:'row', alignItems:'center', flex:3, flexShrink:1}}>
                            <Image source={Out} style={{ height:30, width:30}}/>
                            <View style={{flexShrink:1, marginLeft:10, marginRight:10}}>
                            <Text style={styles.title}>Version</Text>
                            <Text style={styles.desc}>SERV {this.props.mobx_config.Version}</Text>
                            </View>
                        </View>
                        {/* <Icon style={{paddingRight: 10,}} name="angle-right" size={27} color="#080880" /> */}
                        </View>

                        <TouchableOpacity style={styles.boxStyle} onPress={() => this.props.navigation.navigate('Onboarding', {from :'settings'})} >
                            <View style={{flexDirection:'row', alignItems:'center', flex:3, flexShrink:1}}>
                            <Image source={Tutorial} style={{height:25, width:25, }}/>
                            <View style={{flexShrink:1, marginLeft:10, marginRight:10}}>
                                <Text style={styles.title}>Irah's Tutorial </Text>
                                <Text style={styles.desc}>Learn how to use SERV</Text>
                            </View>
                            
                            </View>
                            <Icon style={{paddingRight: 10,}} name="angle-right" size={27} color="#363434" />
                        </TouchableOpacity>


                    <TouchableOpacity style={{alignSelf:'center',marginBottom:25, marginTop:25  }} onPress={() => this.setState({modalLogout: true})}>
                        <Button onPress={this.logout} bgColor='#DA2D4C' title='Logout from my account' width={WIDTH} />
                    </TouchableOpacity>


                </ScrollView>
                    

                {/* MODAL */}
                <MiddleModal modalName ={this.state.modalLogout}  children={
                    <View>
                        
                        <Text style={{ color:'black', fontFamily: "ProximaNova-Bold", fontSize:30, marginTop:15}}>Heads Up!</Text>
                        <Text style={{marginTop:20, color:'#363434', }}>Are you sure you want to logout of your account? If yes, you won't be able to enjoy the services we provide exclusively for our users.</Text>
                        
                        <TouchableOpacity onPress={() => this.setState({modalLogout: false})} style={{backgroundColor:'#080880', borderRadius:6, padding:20, marginTop:45, paddingLeft:20, paddingRight:20,alignItems:'center'}}>
                        <Text style={{color:'white', fontFamily: "ProximaNova-Bold"}}>Go Back</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.logout} style={{ padding:20, marginTop:5, paddingLeft:20, paddingRight:20, alignItems:'center'}}>
                        <Text style={{color:'red', fontFamily: "ProximaNova-Bold"}}>Yes, I want to logout</Text>
                        </TouchableOpacity>
                        
                    </View> 
                    } 
                />


                <MiddleModal modalName ={this.state.modalF}  children={
                    <View>
                        <Text style={{ color:'black', fontFamily: "ProximaNova-Bold", fontSize:20, marginTop:15, marginBottom:20}}>Leave Feedback</Text>
                           
                        <FormTextInput 
                            title = 'FEEDBACK'
                            placeholder='Write your feedback here'
                            value={this.state.suggestion} 
                            onChangeText={(x) => this.setState({suggestion:x})}

                        /> 
                        
                        <TouchableOpacity onPress={this.feedback}  style={{backgroundColor:'#080880', borderRadius:6, padding:17, marginTop:45, paddingLeft:20, paddingRight:20,alignItems:'center'}}>
                            <Text style={{color:'white', fontFamily: "ProximaNova-Bold"}}>Submit Feedback</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setState({modalF: false, suggestion: ''})} style={{ padding:20, marginTop:5, paddingLeft:20, paddingRight:20, alignItems:'center'}}>
                            <Text style={{color:'#080880', fontFamily: "ProximaNova-Bold"}}>Go Back</Text>
                        </TouchableOpacity>
                    </View>
                } />
                
                   
                <MiddleModal modalName={this.state.modalBug} children={
                    <View>
                        <Text style={{ color:'black', fontFamily: "ProximaNova-Bold", fontSize:20,marginBottom:20, marginTop:15}}>Submit a report</Text>

                            <FormTextInput 
                                title = 'DETAILS'
                                placeholder='eg: I cannot logout, I cannot delete my vehicle'
                                value={this.state.description} 
                                onChangeText={(x) => this.setState({description:x})}
                                multiline= {true}
                            /> 
                           

                            {this.state.screenshot !== '' ? 
                                <TouchableWithoutFeedback onPress={() => this.setState({screenshot: ''})}>
                                    <View style={{marginTop:20,}} >
                                        <Text style={{fontWeight:'bold', fontSize:13,marginBottom:10, color:'#6e6d6d' }}>ATTACHED FILE</Text>                
                                        <View style={styles.boxy}>
                                            <View style={{flexDirection:'row',padding: (Platform.OS === 'ios') ? 0 : 10, alignItems:'center',justifyContent:'space-between',}}>
                                                <View style={{flexDirection:'row', alignItems:'center'}}>
                                                    <MaterialIcon name = 'paperclip' size={20} style={{marginRight:15}} /> 
                                                    <Text>File 1 </Text>
                                                </View>
                                                    <MaterialIcon name = 'close' size={20} /> 
                                            </View>
                                        </View>
                                    </View>
                                </TouchableWithoutFeedback>
                            
                            : 
                                <TouchableWithoutFeedback onPress={this.openLibrary}>
                                    <View style={{marginTop:20,}} >
                                        <Text style={{fontWeight:'bold', fontSize:13,marginBottom:10, color: '#6e6d6d' }}>ATTACHMENT (Optional)</Text>                
                                        <View style={styles.boxy}>
                                            <Text style={{padding: (Platform.OS === 'ios') ? 0 : 10}}>Add a screenshot here </Text>
                                        </View>
                                    </View>
                                   
                                </TouchableWithoutFeedback>
                            }

                                
                            <View style={{marginTop:45}}>
                            {this._renederPerce()}
                            </View>
                            <TouchableOpacity onPress={this._bugReport}   style={{backgroundColor:'#080880', borderRadius:6, padding:17, marginTop:10, paddingLeft:20, paddingRight:20,alignItems:'center'}}>
                                <Text style={{color:'white', fontFamily: "ProximaNova-Bold"}}>Submit Report</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.setState({modalBug: false, description: '', screenshot: ''})} style={{ padding:20, marginTop:5, paddingLeft:20, paddingRight:20, alignItems:'center'}}>
                            <Text style={{color:'#080880', fontFamily: "ProximaNova-Bold"}}>Go Back</Text>
                            </TouchableOpacity>

                        </View>
                } />

        


                </View>
            </SafeAreaView>
        )
    }
}
Settings = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(Settings))
export default Settings


const styles = StyleSheet.create({
    boxStyle: {
        backgroundColor: '#fff',
        flexDirection:'row', 
        padding:15, 
        justifyContent:'space-between',
        margin:15,
        marginTop:0,
        borderRadius:10,
        ...Platform.select({
         ios: {
            shadowColor: '#000',
            shadowOpacity: 0.22,
            shadowRadius: 2.22,
           shadowOffset: { width: 0, height: 1 },
         },
         android: {
           elevation: 3
         },
        }),
      },
    title : {fontSize:16, fontFamily:'ProximaNova-Bold'},
    desc: {fontSize:12, color:'#363434', marginTop:6},
      //modal
    modalI: {
        flex: 1,
        alignItems: 'center',
        // flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0.4)',
    },
    innermodal: {
        elevation: 20,
        borderRadius: 10,
        padding:20,
        width:'92%',
        backgroundColor: '#fff',
        ...Platform.select({
            ios: {
               shadowColor: '#000',
               shadowOpacity: 0.22,
               shadowRadius: 2.22,
              shadowOffset: { width: 0, height: 1 },
            },
            android: {
              elevation: 3
            },
           }),
    },
    boxy:{
        padding:(Platform.OS === 'ios') ? 15 : 0,
        paddingLeft: (Platform.OS === 'ios') ? 15 : 15,
        borderWidth:1, borderColor:'#ccc',borderRadius:5
        // marginBottom:15,
      },
      
  

})