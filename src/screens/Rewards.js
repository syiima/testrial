import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/AntDesign"
import FontIcon from "react-native-vector-icons/FontAwesome"

import Header from '../components/Header.js'
import referral from '../assets/icons/referral.png'

class Rewards extends React.Component{
    render(){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#080880'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Rewards' onPress={() => this.props.navigation.goBack()} />
                    
                    <ScrollView  >
                        <View style={{flex:1, padding:20}}>

                        <Text style={styles.mainTitle}>Redeem your rewards</Text>
                        <Text>Enjoy exclusive benefits and rewards from SERV</Text>

                        {/* <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('RewardsPrimo')} >
                        <View style={styles.box}>
                            <View style={styles.icon} >
                                <Icon name='gift' color='#080880' size={26}  />
                            </View>
                            <View >
                                <Text style={styles.topText} >Primo Rewards</Text>
                                <Text style={styles.bottomText} >Get rewarded for your hard work</Text>
                            </View>
                            <FontIcon name="angle-right" color='#000' size={20} style={{position:'absolute', right:15, bottom:15,}} />
                        </View>
                        </TouchableWithoutFeedback> */}
                        <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Referrals')} >
                        <View style={styles.box}>
                            <View style={styles.icon} >
                                <Image source={referral} style={{height:30, width:30}}  />
                            </View>
                            <View style={{marginRight:15, flexShrink:1}}>
                                <Text style={styles.topText} >Referrals</Text>
                                <Text style={styles.bottomText} >Refer a friend or merchant and enjoy benefits together</Text>
                            </View>
                            <FontIcon name="angle-right" color='#000' size={20} style={{position:'absolute', right:15, bottom:15,}} />
                        </View>
                        </TouchableWithoutFeedback>
                        </View>
                    </ScrollView>
                    
                </View>
            </SafeAreaView>
        )
    }
}

Rewards = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(Rewards))
export default Rewards

const styles = StyleSheet.create({
    mainTitle:{fontFamily: "ProximaNova-Bold",color:"#080880",fontSize:16, marginBottom:15},
    box:{
        flexDirection:'row', alignItems:'center',
        borderRadius: 15, 
        marginTop:20,
        padding:20,
        backgroundColor:'#fff',
        flexDirection:'row',
        alignItems:'center',
       
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 3 },
              shadowOpacity: 0.3,
            },
            android: {
              elevation: 3
            },
          }),
    },
    icon:{width:40, height:40, marginRight:15, borderRadius:20, backgroundColor:'#efefef', alignItems:'center', justifyContent:'center'},
    topText :{fontSize:16, marginBottom: 6},
    bottomText:{ fontSize:13, color:'grey'},
})