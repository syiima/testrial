import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions, TouchableWithoutFeedback,Image, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/FontAwesome"

import Header from '../components/Header.js'
import emptyPage from '../assets/icons/emptyBooking.png'
import HistoryComponent from "../components/HistoryComponent.js"

class DisplayHistory extends React.Component{
    constructor(){
        super()
        this.state = {
            history: '',
            currentview: 1
        }
    }
    
    componentDidMount(){
        this.sortAgain()
    }
    
    sortAgain = () => {
        let DATA = this.props.data
        let TEMP =[]
        let b ={}

        for (const month in DATA) { 
            let MONTH = DATA[month];
            MONTH.month = month

             b = {
                month: month,
                history:MONTH
            }

            TEMP.unshift(b)
                
        }  
        // console.warn('month', b)
        this.setState({history: TEMP})

    }

    // openDets = (x) => {
    //     if(x.type !== undefined && x.type === 'walkin'){
    //         firebase.analytics().logScreenView('History_Walkin', 'History_Walkin')
    //         this.props.navigation.navigate('TrackBookingHistoryDetails', {
    //             item:x, 
    //             quotation: x.quotation.masterItems, 
    //         })
    //     }
    // }

    showMonth = () => {
        let display
        let details 
        let DATA = this.state.history

        if(DATA !== '' && DATA.length !== 0){
            // details = MONTH.map(item => {
                
            // })
            display = DATA.map(item => (
                <HistoryComponent data={item} navigation={this.props.navigation}/>
                
            ))

        }

        return display 
    }
    render(){
        return(
            <View>
                {this.showMonth()}
            </View>
        )
    }
}
class VehicleHistory extends React.Component{
    constructor(){
        super()
        this.state = {
            history: '',
            currentview: 1
        }
    }
    
    componentDidMount(){
        this.sortHistory()
    }

    sortHistory(){
        let PLATE = this.props.mobx_carkey.PLATE
        
        firebase.database().ref(`plate_number/${PLATE}/book_history`).once('value', (snp) => {
            if(snp.exists()){
                KEY = Object.keys(snp.val())
                let sorted = {}
                let DATA = []
                KEY.forEach((key) => {
              
                    let a = snp.val()[key]
                    a.bookID = key
            
                    if(a.assign.status !== 'deleted' && a.quotation !== undefined){  
                        if(a.customer_year === '2020' || a.customer_year === '2021'){
                            DATA.unshift(a)
                        }
                       
                        
                    } 
                    
                })

                if(DATA === []){
                    this.setState({history: [], currentview:1})
                } else {
                    let SORTED =  DATA.sort((a,b) => a.timestamp - b.timestamp)
                    this.sort(SORTED)
                }
                  
                

            } else {
                this.setState({history: [], currentview:1})
                // no history
            }
        })
        
    }


    sort = (x) => {
        let DATA = x
        if(DATA !== ''){
            let sorted = {}, b =[]
            let TEMP =[]
           
            for (const key in DATA) {
                let b =  DATA[key];
                let year = parseInt(b.customer_year)
                let month = b.customer_month
                if (typeof sorted[year] === "undefined") {
                    sorted[year] = {};
                }
            
                if (typeof sorted[year][month] === "undefined") {
                    sorted[year][month] = [];
                }
            
              sorted[year][month].push(b);  
            }
            for (const year in sorted) { 
                let YEAR = sorted[year];

                let b = {
                    year: year,
                    history:YEAR
                }
    
                TEMP.unshift(b)
                    
            }   
            this.setState({history: TEMP, currentview: 2})
        }

       
      
    }

    showHistory = () => {
        let display 
        let DATA = this.state.history

       if(DATA !== ''){
            let ITEM = DATA.map(item => (
                <View style={{}} >
                    {/* <TouchableWithoutFeedback onPress={() => this.setState({currentView: item.year})} > */}
                    <View style={{marginTop:15,marginBottom:10}}>
                        <Text style={{fontFamily: "ProximaNova-Bold",fontSize:16}} >{item.year}</Text>
                    </View>
                    {/* </TouchableWithoutFeedback> */}
                    
                  
                    <DisplayHistory data={item.history} navigation={this.props.navigation} />
                
                    
                </View> 
                

            ))
            display = 
            <View >
                {ITEM}
            </View>

       }
        return display 
    }
    
    render(){
        let content 
        if(this.state.currentview === 1){
            content = 
            <View style={{flex:1, alignItems:'center', justifyContent:'center', margin:20, marginTop:0}}>
                <Image source={emptyPage} style={{height: 200, width:200, resizeMode:'contain'}} />
                <Text style={{fontSize:22, fontFamily: "ProximaNova-Bold", marginTop:15, marginBottom:10}}>No Records</Text>
                <Text>All your histories will be displayed here</Text>
              </View>
        } else {
            if(this.state.history !== '' && this.state.history.length !== 0){
                content = 
                <ScrollView style={{padding:20}}>
                    {this.showHistory()}

                    <View style={{height:30}} />
                </ScrollView>
            } else {
                content = 
                <View style={{flex:1, alignItems:'center', justifyContent:'center', margin:20, marginTop:0}}>
                    <Image source={emptyPage} style={{height: 200, width:200, resizeMode:'contain'}} />
                    <Text style={{fontSize:22, fontFamily: "ProximaNova-Bold", marginTop:15, marginBottom:10}}>No Records</Text>
                    <Text>All your histories will be displayed here</Text>
                </View>
            }

            

        }

        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#080880'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Booking History' onPress={() => this.props.navigation.goBack()} />
                    
                    {content}
                </View>
            </SafeAreaView>
        )
    }
}

VehicleHistory = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(VehicleHistory))
export default VehicleHistory

const styles = StyleSheet.create({
    box :{
        backgroundColor:'#fff',
        borderRadius:10,
        marginBottom:15,
        padding:15,
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 3 },
              shadowOpacity: 0.3,
            },
            android: {
              elevation: 3
            },
          }),
    },
})