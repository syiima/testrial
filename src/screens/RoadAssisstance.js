import React from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  Linking,
  TouchableOpacity,
  SafeAreaView,
  Image,
  ScrollView,
  ImageBackground,
  ActivityIndicator,
  Alert,
} from 'react-native';
import { inject, observer, Provider } from 'mobx-react';
import firebase from '@react-native-firebase/app';

import Icon from 'react-native-vector-icons/AntDesign'
// import myeg from '../assets/logo/myegLogo.png'
// import carOut from '../assets/road_assist/carOut.png'
// import batteryOut from '../assets/road_assist/batteryOut.png'
// import fuelOut from '../assets/road_assist/fuelOut.png'
// import tyreOut from '../assets/road_assist/tyreOut.png'
// import expert from '../assets/road_assist/expert.png'
// import startOut from '../assets/road_assist/startOut.png'
import collab from '../assets/road_assist/collab.png'

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

class RoadAssisstance extends React.Component{
  constructor(){
    super()
    this.state = {
      view:1,
    }
  }

  componentDidMount(){
    setTimeout(() => {this.setState({view: 2})}, 800)
  }

  callAssistance = () => {
    // Alert.alert('heyy', 'aaaaaaaaaaaa')
    let FUID = this.props.mobx_auth.FUID;

    firebase.database().ref(`assistance_myAssist/${FUID}`).push({
      opened: firebase.database.ServerValue.TIMESTAMP
    })

    Linking.openURL("tel:0376648088")
  }

  render(){

    // let content 
    // if(this.state.view === 1){
    //   content = 
    //   <View style={{flex:1, alignItems:'center', justifyContent:'center'}} >
    //     <ActivityIndicator style={{ height: 20, width: 20,}} size="small" color="#080880" />
    //     <Text style={{marginTop:5, fontSize:12}} >Loading ...</Text>
    //   </View>
    // } else {
    //   content = 
    //   <View style={{flex:1}}>
    //       <ScrollView >
    //       <View style={{flex:1,padding:20, alignItems:'center', justifyContent:'center'}}>

    //       <View style={{marginBottom:20}}>
    //       <Text style={{textAlign:'center', fontFamily: "ProximaNova-Bold",fontSize:12}}>In collaboration with</Text>
    //       <Image source={myeg} style={{width:WIDTH/2, height: WIDTH /7, resizeMode:'contain'}} />
    //       </View>

    //       <Text style={{textAlign:'center', fontFamily: "ProximaNova-Bold",fontSize:20}}>Road Assistance</Text>
    //       <Text style={{textAlign:'center', fontFamily: "ProximaNova-Bold",fontSize:14, marginTop:5}}>We SERV you in the following situations</Text>

    //       <View style={{flexDirection:'row',  marginTop: 30, justifyContent:'space-between', }}>
    //         <View style={styles.viewImg}>
    //           <Image source={fuelOut} style={styles.img} />
    //             <Text style={styles.imgText}>My car</Text>
    //             <Text style={styles.imgText}>ran out of fuel</Text>
    //         </View>
    //         <View style={styles.viewImg}>
    //           <Image source={carOut} style={styles.img} />
    //           <Text style={styles.imgText}>My car</Text>
    //           <Text style={styles.imgText}>broke down</Text>
    //         </View>
    //         <View style={styles.viewImg}>
    //           <Image source={batteryOut} style={styles.img} />
    //           <Text style={styles.imgText}>My car</Text>
    //           <Text style={styles.imgText}>is out of battery</Text>
    //         </View>
    //       </View>

    //       <View style={{flexDirection:'row', marginTop:30,justifyContent:'space-between', }}>
    //         <View style={styles.viewImg}>
    //           <Image source={tyreOut} style={styles.img} />
    //           <Text style={styles.imgText}>Tyre puncture</Text>
    //           <Text> </Text>
    //         </View>
    //         <View style={styles.viewImg}>
    //           <Image source={expert} style={styles.img} />
    //           <Text style={styles.imgText}>Need an expert</Text>
    //           <Text style={{fontSize:11}}>Foreman Service</Text>
    //         </View>
    //         <View style={styles.viewImg}>
    //           <Image source={startOut} style={styles.img} />
    //           <Text style={styles.imgText}>My car</Text>
    //           <Text style={styles.imgText}>won't start</Text>
    //         </View>
    //       </View>

    //       <View style={{height:20}} />

    //       </View>
    //     </ScrollView>
        


    

    //   </View>
      
    // }

    return(
      <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
        <View style={{flex:1,backgroundColor:'#fff', }}>
        
        
        <TouchableOpacity style={styles.backButton} onPress={() => this.props.navigation.goBack(null)}>
          <Icon name='leftcircle' size={18} color='#fff'  />
          <Text style={{color:'#fff', fontFamily: "ProximaNova-Bold", marginLeft:14}}> Back </Text>
        </TouchableOpacity>
        <Image source={collab} style={{width:WIDTH, height:HEIGHT / 1.5, }} />

        {/* {content} */}
          
         <TouchableOpacity  style={styles.callBox} onPress={() => this.callAssistance()}>
            <Text style={{color:'#fff', fontFamily: "ProximaNova-Bold", textAlign:'center', }}>Call for assistance</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    )
  }
}

RoadAssisstance = inject('mobx_auth', 'mobx_carkey',)(observer(RoadAssisstance))
export default RoadAssisstance

const styles = StyleSheet.create({
  info:{
    alignItems:'center',
    flexDirection:'row',
    marginBottom:15
  },
  textInfo:{
    marginLeft:10,
    fontFamily: "ProximaNova-Bold",
    fontSize:14
  },
  callBox:{
    backgroundColor:'#e85b1a',
    borderRadius:5,
    position:'absolute',
    bottom: 30,
    alignSelf:'center',
    padding:15,
    width:'90%',
    // bottom: 20,

  },
  backButton:{
    borderRadius:50,
    alignSelf:'flex-start',
    margin:10,
    padding:10,paddingRight:20, paddingLeft:20,
    backgroundColor:'#080880',
    flexDirection:'row',
    alignItems:'center',
    justifyContent:"center"
  },
  viewImg :{width: WIDTH / 3.6, marginLeft:2, marginRight:2, alignItems:'center', justifyContent:'center'}, 
  img:{height: WIDTH / 5, width: WIDTH / 5,},
  imgText :{fontFamily: "ProximaNova-Bold", textAlign:'center', fontSize:12 }

})
