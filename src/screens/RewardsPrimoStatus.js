import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,ActivityIndicator,Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from 'react-native-vector-icons/FontAwesome'
import Moment from 'moment';

import Header from '../components/Header.js'
import driver from '../assets/driver.png'
import medic from '../assets/medic.png'

class ConditionalStats extends React.Component{
    
    render(){
  
        let renderColor;
        if(this.props.item.verified === false){
            renderColor =
            <View style={{...styles.stats, backgroundColor:'#ffce00'}}>
            <Text style={{fontFamily: "ProximaNova-Bold", color:'white'}}>Pending</Text>
            </View>
        }
        if(this.props.item.verified === true){
            renderColor =
            <View style={{...styles.stats,backgroundColor:'#38cc26'}}>
            <Text style={{fontFamily: "ProximaNova-Bold", color:'white'}}>Approved</Text>
            </View>
        }
        if(this.props.item.verified === 'decline'){
            renderColor =
            <View style={{...styles.stats, backgroundColor:'#db3035'}}>
            <Text style={{fontFamily: "ProximaNova-Bold", color:'white'}}>Rejected</Text>
            </View>
        }
  
        let renderIcon 
        if(this.props.item.name === 'Frontliners'){
            renderIcon = <Image source={medic} style={{height:Dimensions.get('window').width / 8, width:Dimensions.get('window').width / 8, marginRight: 15, resizeMode:'contain' }} />
        } else {
            renderIcon = <Image source={driver} style={{height:Dimensions.get('window').width / 8, width:Dimensions.get('window').width / 8, marginRight: 15, resizeMode:'contain' }} />
        }


      return(
        <TouchableWithoutFeedback onPress={() => {
          firebase.analytics().logScreenView({
            screen_name: 'eHailingStatus_desc',
            screen_class: 'eHailingStatus_desc',
          }) 
          this.props.navigation.navigate('RewardsStatusDesc' , {item: this.props.item})}}>
        {/* <TouchableWithoutFeedback> */}
            <View style={styles.box}>
                    <View style={{flexDirection:'row'}}>
                    <View style={{flexDirection:'row', alignItems:'center', marginRight:15, flex:2, flexShrink:1, }}>
                        {renderIcon}
                        <View>
                            <Text style={{fontSize:16, flexShrink:1}}>{this.props.item.name}</Text>
                            <Text style={{fontSize:16, flexShrink:1}}>{this.props.item.plate}</Text>
                        </View>
                        
                    </View>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        {renderColor}
                            <Icon name="angle-right" size={16} style={{marginLeft:10}}/>
                    </View>
                    </View>
                {/* <Text style={{fontSize:11, marginTop:10, alignSelf:'flex-start', color:'grey'}} >Applied on: {Moment(this.props.item.date_applied).format('DD MMMM YYYY')} </Text> */}

            </View>
        </TouchableWithoutFeedback>
      )
    }
}

  
class RewardsPrimoStatus extends React.Component{
    constructor(){
        super()
        this.state = {
            biz_data: '',
            pageview:1,
            isLoading: true
        }
    }
    
    componentDidMount(){
        this.fetchVehicles()
    }
    
    fetchVehicles(){
        let that = this
        let FUID = this.props.mobx_auth.FUID
        let DATA = []
  
        firebase.database().ref(`users/${FUID}/vehicles`).once('value').then((snapshot) => {
          if(snapshot.exists()){
            KEY = Object.keys(snapshot.val());
  
            KEY.forEach((plate) => {
              let PLATE = plate  
              firebase.database().ref(`plate_number/${PLATE}/business/`).once('value').then((snapshot) => {
                if(snapshot.exists()){
                    KEY = Object.keys(snapshot.val());
        
                    KEY.forEach((biz_id) => {
                        let x = snapshot.val()[biz_id]
                        x.biz_id = biz_id 
                        x.plate = PLATE
        
                        DATA.push(x)  
                    })
  
                    let SORTED = DATA.sort((a,b) => b.date_applied - a.date_applied) 
                    this.setState({biz_data:SORTED, pageview:2, isLoading:false,})
                  }
                })
  
            })
  
          } else {
            this.setState({biz_data : '', pageview: 1, isLoading:false,})
          }
        })
  
    }
  
  
    renderStatus = () => {
        let DATA = this.state.biz_data
        let dispklay
  
        if(DATA !== ''){
            dispklay = DATA.map( item=> (
                <ConditionalStats navigation={this.props.navigation} item={item} />
            ))
        } else{
            <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                {/* <Image source={sad} style={{height: 80, width:80}} /> */}
                <Text style={{fontSize:22, fontFamily: "ProximaNova-Bold", marginTop:15, marginBottom:10, textAlign:'center'}}>No Primo Rewards (Business) Applied</Text>
                <Text>Get verified with a business and enjoy more rewards!</Text>
            </View>
        }
        
        return dispklay
  
      }
  

    render(){
        let content;

        if(this.state.isLoading === true){
            content =
            <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                <ActivityIndicator size="large" color="#080880" />
                <Text>Loading Primo Rewards Status</Text>
            </View>
        }
        if(this.state.pageview === 1 ){
            content =
            <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                {/* <Image source={sad} style={{height: 80, width:80}} /> */}
                <Text style={{fontSize:22, fontFamily: "ProximaNova-Bold", marginTop:15, marginBottom:10, textAlign:'center'}}>No Primo Rewards (Business) Applied</Text>
                <Text>Get verified with a business and enjoy more rewards!</Text>
            </View>
        }
        if(this.state.pageview == 2){
            content =
            <View style={{margin:15}}>
                {this.renderStatus()}
            </View>
        }

        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#080880'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Rewards Status' onPress={() => this.props.navigation.goBack()} />
                    
                    <ScrollView showsVerticalScrollIndicator={false}>

                        {content}

                    </ScrollView>
                    
                </View>
            </SafeAreaView>
        )
    }
}

RewardsPrimoStatus = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(RewardsPrimoStatus))
export default RewardsPrimoStatus

const styles = StyleSheet.create({
    box: {  alignItems:'center', borderRadius:10, marginBottom:15, padding:20,backgroundColor:'#fff',
    ...Platform.select({
        ios: {
          shadowColor: '#000',
          shadowRadius: 2.22,
          shadowOffset: { width: 0, height: 1 },
          shadowOpacity: 0.22,
        },
        android: {
          elevation: 3
        },
       }),
    },
    stats: {borderRadius:4, padding:5, paddingLeft:20, paddingRight:20, alignItems:'center', justifyContent:'center',},

})