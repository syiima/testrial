import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/AntDesign"
import { NavigationActions, StackActions } from 'react-navigation';

import Header from '../components/Header.js'
import WorkshopInfo from "../components/WorkshopInfo.js"
import Conditional from "../components/WorkshopQuotationConditional.js"
import MiddleModal from '../components/MiddleModal'
import profile from '../assets/empty_profile.png'
import Inspection from '../assets/inspecting_vehicle.png'

class WorkshopPerformService extends React.Component{
    constructor(){
        super()
        this.state = {
            requestData: '',
            logo: '',
            ws_data :'',
            quote : '',
            modalCancel :false,
            status :''

        }
    }
    
    componentDidMount(){
        this.fetchRequest()
        this.fetchWorkshopDets()
        this.statusListener()
    }
    
    fetchRequest(){
        let that =this
        let DATA = []
        let FUID = this.props.mobx_auth.FUID
        let BOOK_ID =  this.props.mobx_retail.R_WALKIN_BOOKID
        let WS_ID =   this.props.mobx_retail.R_ID
        let WSMAIN_ID =   this.props.mobx_retail.R_GROUPID
        let X, QUOTATION, STATUS

        firebase.database().ref(`request_walkin/${WSMAIN_ID}/${WS_ID}/${BOOK_ID}`).once('value').then((snapshot) => {
            if(snapshot.exists()){
                X = snapshot.val()
                QUOTATION = X.quotation
                STATUS = X.status
            } 

            that.setState({requestData : X, quote: QUOTATION, status: STATUS})
        })
    }

    fetchWorkshopDets(){
        let that = this
        let WSDATA = []
        let BOOK_ID =  this.props.mobx_retail.R_WALKIN_BOOKID
        let RETAIL =   this.props.mobx_retail.R_ID
        let RETAIL_MAIN =   this.props.mobx_retail.R_GROUPID
        // console.warn('hellllooo', WSMAIN_ID, WS_ID);
        
        firebase.database().ref(`retail/${RETAIL_MAIN}/${RETAIL}/`).once('value').then((snapshot) => {
            if(snapshot.exists()){
                let a = snapshot.val()
                that.setState({ws_data: a})
            }
        })

        firebase.database().ref(`retail_main/${RETAIL_MAIN}/logo`).once('value').then((snapshot) => {
            if(snapshot.exists()){
                let b = snapshot.val()
                that.setState({logo: b})
            }
        })

    }

    statusListener(){
        //This function is used to control the paid status {Pending, Unpaid, Paid} on the car profile change
        let that = this;
        let CARPLATE = this.props.mobx_carkey.PLATE
        let UID = this.props.mobx_auth.FUID;    
        firebase.database().ref(`plate_number/${CARPLATE}/book_walkin/`).on('child_changed', async function(snapshot) {
          if(snapshot.exists()){

            if(snapshot.val() === "Pending"){
                let status = that.props.mobx_retail.setSTATUS('Pending');
                  that.gotopage('WorkshopQuotation')
            }
  
            if(snapshot.val() === "Complete"){
              that.gotopage('WorkshopPayment')
            }
         
          } 
    
        })
      }


    gotopage = (x) => {    
        if(x === 'WorkshopQuotation'){
            firebase.analytics().logScreenView({
                screen_name: 'Quotation',
                screen_class: 'Quotation',
              }) 

            const navigateAction = StackActions.replace({
                routeName: 'WorkshopQuotation',
                action: NavigationActions.navigate({ routeName: 'WorkshopQuotation' }),
              });
              this.props.navigation.dispatch(navigateAction);

        }
        if(x === 'WorkshopPayment'){
            firebase.analytics().logScreenView({
                screen_name: 'Payment',
                screen_class: 'Payment',
              }) 
            const navigateAction = StackActions.replace({
                routeName: 'WorkshopPayment',
                action: NavigationActions.navigate({ routeName: 'WorkshopPayment' }),
              });
              this.props.navigation.dispatch(navigateAction);

        }
        
    }


    //======= 
    renderInfo = () => {
        let item = this.state.requestData
        let display
  
        if(item !== ''){
            display =    
            <View style={{padding:20}}>
                    <View style={{flexDirection:'row'}}>
                    <Text style={{flex:1}}>Quotation No</Text>
                    <Text style={styles.boldText}>: </Text>
                    <Text style={{...styles.boldText, fontSize:17, flex:1.5}}>{item._requestID}</Text>
                </View>
                <View style={{flexDirection:'row'}}>
                    <Text style={{flex:1}}>Date</Text>
                    <Text style={styles.boldText}>: </Text>
                    <Text style={{...styles.boldText, fontSize:17, flex:1.5}}>{item.customer_day} {item.customer_month} {item.customer_year}</Text>
                </View>
                <View style={{flexDirection:'row'}}>
                    <Text style={{flex:1}}>Time</Text>
                    <Text style={styles.boldText}>: </Text>
                    <Text style={{...styles.boldText, fontSize:17, flex:1.5}}>{item.customer_time} </Text>
                </View>
                <View style={{flexDirection:'row'}}>
                    <Text style={{flex:1}}>Vehicle Plate No</Text>
                    <Text style={styles.boldText}>: </Text>
                    <Text style={{...styles.boldText, fontSize:17, flex:1.5}}>{item.customer_carplate} </Text>
                </View>
            </View>
        
        }
        return display
    }

    renderQuote = () => {
        let item = this.state.quote
        let STATUS = this.state.status
        let display
        if(item !== ''){
            display = 
                <View>
                    <View style={{...styles.stats, backgroundColor:'#FCE5CD', marginTop:20, borderBottomLeftRadius:0, borderBottomRightRadius:0,}}>
                    
                        <View style={{flexDirection:'row', justifyContent:'space-between', marginBottom:15, padding:5}}>
                        <Text style={styles.boldText}>Your Total</Text>
                            <View style={{flexDirection:'row'}}>
                                <Text style={{...styles.boldText, fontSize:12}}>RM</Text>
                                <Text style={{...styles.boldText, fontSize:24}}>{item.total_price}</Text>
                            </View>
                        </View>
  
                        {this.renderInfo()}
                    </View>

                    <View style={styles.boxItem}>
                        <Conditional item={item} status={STATUS} />
                    </View>
  
  
                </View>
        
        }
        return display
    }

    assign_mech = () => {
        let DATA = this.state.requestData
        let display
        if(DATA !== '' && DATA !== undefined){
            if(DATA.assigned_mechanic !== undefined){
                display =
                <View style={{...styles.stats, backgroundColor:'#fff', marginTop:20, marginBottom:0}}>
                    <Text style={{...styles.boldText, fontSize:18, marginBottom:15}}>Your designated mechanic: </Text>
                    <View style={{flexDirection: 'row', alignItems:'center', }}>
                        <View style={{height: 44, width: 44, borderRadius:22, borderWidth:0.5, borderColor:'#fa7910',  marginRight:15, alignItems:'center', justifyContent:'center'}}>
                        <Image source={profile} style={{height: 40, width: 40, borderRadius: 20,}} />
                        </View>
                    <Text >{DATA.assigned_mechanic}</Text>
                    </View>
                    
                </View>
            }
        
        }
        return display

    }



    deleteReqeust = () => {
        let that = this
        let BOOKING_ID =  this.props.mobx_retail.R_WALKIN_BOOKID
        let WORKSHOP_ID =   this.props.mobx_retail.R_ID
        let WORKSHOP_MAIN =   this.props.mobx_retail.R_GROUPID
        let FUID = this.props.mobx_auth.FUID
        let PLATE = this.props.mobx_carkey.PLATE

        let DATA = {
            _book_id :'',
            _book_status: '',
            retail_id: '',
            retail_main: '',
            booking_by:'',
            day:'',
            month:'',
            year: '',
            time : '',
        }        

        // //1.push to history
        let WKIN_DATA
        firebase.database().ref(`request_walkin/${WORKSHOP_MAIN}/${WORKSHOP_ID}/${BOOKING_ID}`).once('value').then((snapshot) => {
            if(snapshot.exists()){
                WKIN_DATA = snapshot.val()
                WKIN_DATA.timestamp_delete = firebase.database.ServerValue.TIMESTAMP
                
                firebase.database().ref(`history_walkin/${WORKSHOP_MAIN}/${WORKSHOP_ID}/${BOOKING_ID}/`).update(WKIN_DATA)
                that.setState({modalCamcel:false})           

            } 
        }).then(() => {
            //2.remove from req_wal
            firebase.database().ref(`request_walkin/${WORKSHOP_MAIN}/${WORKSHOP_ID}/${BOOKING_ID}/`).remove() 
            //3.clear user booking
            firebase.database().ref(`plate_number/${PLATE}/book_walkin/`).update(DATA) 
        }).then(() => {
            // this.props.navigation.navigate('List_Workshop')
            // that._gotopage('New')
            setTimeout(() => Alert.alert('Your previous request has been deleted!', 'You can continue with a new service.'), 1000);            

        }).then(() => {
            this.props.navigation.popToTop();

        })
    }

    goHome = () => {
        // const navigateAction = StackActions.reset({
        //     index: 0,
        //     actions: [NavigationActions.navigate({ routeName: "Track" })],
        //   });
        
        //   this.props.navigation.dispatch(navigateAction);
        this.props.navigation.popToTop();
        
    }

    render(){
        let statusBox;
        let STATUS =  this.props.mobx_retail.STATUS
        let IMG = <Image source={Inspection} style={{height: Dimensions.get('window').width /4, width: Dimensions.get('window').width /4, resizeMode:'contain', marginBottom:15, marginTop:15}}/>

        if(STATUS === 'Requesting'){
            statusBox = 
            <View style={{...styles.stats, backgroundColor:'#F4CCCC', flexDirection:'row',alignItems:'center'}}>
                <View style={{flex: 3,}}>
                    <Text style={{...styles.boldText, fontSize:18, marginBottom:15}}>Service provider is inspecting your vehicle</Text>
                    <Text style={{fontSize:13}}>Please be patient and await for your service provider to produce a preliminary report to you. You will be redirected to a quotation page once inspection is completed.</Text>
                </View>
                {IMG}
            </View>
        }
        if(STATUS === 'Accepted' || STATUS === 'Pending Complete'){
            statusBox = 
            <View style={{...styles.stats, backgroundColor:'#D9EAD3',flexDirection:'row',alignItems:'center'}}>
                <View style={{flex: 3,}}>
                    <Text style={{...styles.boldText, fontSize:18, marginBottom:15}}>Service Provider is performing the magic and service</Text>
                    <Text style={{fontSize:13}}>Please be patient and await for your service provider to complete the service. You will be redirected to a payment page once it is completed.</Text>
                    {this.state.requestData.ets ? <Text style={{marginTop:10,fontFamily: "ProximaNova-Bold", fontSize:11}} >Estimated Time - {this.state.requestData.ets}</Text> : null}
                </View>
                {IMG}
            </View>
        }

        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Performing Service' onPress={this.goHome} />
                    

                <ScrollView showsVerticalScrollIndicator={false}>
                    
                    {statusBox}

                    {this.assign_mech()}

                    <View style={styles.hmm}>
                        <WorkshopInfo navigation={this.props.navigation} data= {this.state.ws_data} logo={this.state.logo} />
                    </View>

                    {STATUS === 'Accepted' || STATUS === 'Pending Complete'
                        ?   this.renderQuote()
                        :   null
                    }

                    {STATUS === 'Requesting' ?
                    <View>

                    <View style={{marginTop:15, marginBottom:15}}>
                         <Text style={{fontFamily:'ProximaNova-Bold', fontSize:17, marginLeft:20, marginTop:10}}>Service Journey</Text>
                        
                         <View style={{...styles.topBox, padding:20}}>
                        <View style={{flexDirection:'row', alignItems:'center'}}>
                            <View style={{...styles.smolCircle, backgroundColor:'#ffce00', marginBottom:-2}} />
                            <Text style={{fontFamily:'ProximaNova-Bold'}}>Preliminary Inspection</Text>
                        </View>
                        <View style={{...styles.circleLine, backgroundColor:'#080880', }} />
                        <View style={{flexDirection:'row', alignItems:'center'}}>
                            <View style={styles.smolCircle} />
                            <Text>Quotation (by Retail) </Text>
                        </View>
                        <View style={styles.circleLine} />
                        <View style={{flexDirection:'row', alignItems:'center'}}>
                            <View style={styles.smolCircle} />
                            <Text>Quotation Confirmation (by User) </Text>
                        </View>
                        <View style={styles.circleLine} />
                        <View style={{flexDirection:'row', alignItems:'center'}}>
                            <View style={styles.smolCircle} />
                            <Text>Perform Service & Inspection</Text>
                        </View>
                        <View style={styles.circleLine} />
                        <View style={{flexDirection:'row', alignItems:'center'}}>
                            <View style={styles.smolCircle} />
                            <Text>Payment</Text>
                        </View>
                        </View>
                    </View>
                        <TouchableWithoutFeedback onPress={() => this.setState({modalCancel:true})}>
                            <View style={{marginBottom:40, marginTop:20}}>
                                <Text style={{fontFamily:'ProximaNova-Bold', color:'red', textAlign:'center'}}>Wait, please cancel my request!</Text>
                            </View>
                        </TouchableWithoutFeedback>

                    </View>

                     : null }


                    <MiddleModal modalName= {this.state.modalCancel}
                        children = {
                            <View>
                                <Text style={{ color:'black', fontFamily: "ProximaNova-Bold", fontSize:30, marginTop:15}}>Heads Up!</Text>
                                <Text style={{marginTop:20, color:'#080880', fontFamily: "ProximaNova-Bold", }}>You are about to cancel your request. You will relinquish all data regarding this request. 
                                </Text>

                                <Text style={{fontFamily:'ProximaNova-Bold', marginTop:15, color:'red',marginBottom:30}}>Are you sure?</Text>

                                <View>
                                    
                                    <TouchableOpacity onPress={() => this.setState({modalCancel:false})} style={{backgroundColor:'#080880', borderRadius:6, padding:15, marginTop:25, paddingLeft:20, paddingRight:20, marginBottom:15, alignItems:'center'}}>
                                    <Text style={{color:'white', fontFamily: "ProximaNova-Bold"}}>Go Back</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={this.deleteReqeust} style={{ padding:15, paddingLeft:20, paddingRight:20, alignItems:'center'}}>
                                    <Text style={{color:'red', fontFamily: "ProximaNova-Bold"}}>Yes, I want to cancel my request</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        }
                    />


                </ScrollView>
                    
                </View>
            </SafeAreaView>
        )
    }
}

WorkshopPerformService = inject('mobx_auth','mobx_retail', 'mobx_carkey', 'mobx_config')(observer(WorkshopPerformService))
export default WorkshopPerformService

const styles = StyleSheet.create({
    boldText :{fontFamily: "ProximaNova-Bold",},
    topBox: {
        backgroundColor:'white',
        borderRadius : 15,
        // borderBottomRightRadius:20,
        margin:15,
        // padding:20,
        // paddingTop:50,
        marginBottom:5,
       
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 0.6,
            },
            android: {
              elevation: 3
            },
          }),
    },
    stats:{
        padding:15, margin:15, borderRadius:15, marginBottom:0,
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 0.4,
            },
            android: {
              elevation: 2
            },
          }),
    },
    boxItem:{
        padding:15, margin:15, borderBottomLeftRadius:15, borderBottomRightRadius:15, marginTop:0, backgroundColor:'white',
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 0.6,
            },
            android: {
              elevation: 2
            },
          }),
    },
    RTA:{backgroundColor:'white', padding:10, paddingLeft:25, paddingRight:25, borderRadius:8, alignSelf:'center', marginTop:-25,
    ...Platform.select({
        ios: {
          shadowColor: '#000',
          shadowRadius: 3,
          shadowOffset: { width: 0, height: 2 },
          shadowOpacity: 0.4,
        },
        android: {
          elevation: 3
        },
      }),
    },
    hmm :{margin: 15, marginTop: 15,backgroundColor:'white', borderRadius:15, marginBottom:0,
    ...Platform.select({
        ios: {
          shadowColor: '#000',
          shadowRadius: 3,
          shadowOffset: { width: 0, height: 2 },
          shadowOpacity: 0.6,
        },
        android: {
          elevation: 3
        },
      }),
    },
    stats:{
        padding:15, margin:15, borderRadius:15, marginBottom:0,
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 0.6,
            },
            android: {
              elevation: 3
            },
          }),
    },

    smolCircle:{backgroundColor:'white', height: 20, width:20, borderRadius:10, zIndex:10, marginRight:15, 
    ...Platform.select({
        ios: {
          shadowColor: '#000',
          shadowRadius: 1,
          shadowOffset: { width: 0, height: 1 },
          shadowOpacity: 0.4,
        },
        android: {
          elevation: 3
        },
      }),},
    circleLine: {backgroundColor:'white', zIndex:-444, width:3, height:35, marginLeft:8,marginTop:-4,
    ...Platform.select({
        ios: {
          shadowColor: '#000',
          shadowRadius: 1,
          shadowOffset: { width: 0, height: 1 },
          shadowOpacity: 0.4,
        },
        android: {
          elevation: 3
        },
      }),},
})