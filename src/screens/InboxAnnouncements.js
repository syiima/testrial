import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,FlatList,Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Moment from 'moment';

import Header from '../components/Header.js'
import MiddleModal from '../components/MiddleModal.js'
import NotificationLoader from "../components/NotificationLoader.js";

class OpenModal extends React.Component{
    render(){
        return(

            <MiddleModal modalName ={this.props.isOpen}
                children = {
                    <View>
                        <View>
                            <Text style={{fontSize:24,fontFamily:'ProximaNova-Bold',marginBottom:20}}>{this.props.itemTitle}</Text>
                            <Text style={{fontSize:14}}>{this.props.itemDescription} </Text>
                            <Text style={{color:'grey', fontSize:12, marginTop:20}}>{Moment(this.props.itemDate).format("DD MMM")} at {Moment(this.props.itemDate).format("HH:MM A" )}</Text>
                        </View>

                        <TouchableWithoutFeedback onPress={this.props.handleClose}>
                            <View style={{backgroundColor:'#080880', width:40, height:40, borderRadius:20, alignItems:'center', alignSelf:'flex-end', justifyContent:'center', marginTop:40}}>
                                <MaterialCommunityIcons name='check' size={20} color='white' />
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                }
            />


        )
    }
}

class InboxAnnouncements extends React.Component{
    constructor(){
        super()
        this.state = {
            announcement: '',
            loadHighlights: true,
            modalNoti: false,
            title: '',
            description: '',
            timestamp:'',
        }
    }
    
    componentDidMount(){
        this.fetchAnnouncements()
    }
    
    fetchAnnouncements(){
        let that = this
        let ANNOUNCEMENT = [];

        firebase.database().ref(`announcement`).once('value').then((snapshot) => {
            if(snapshot.exists()){
                snapshot.forEach(function (child) {
                    // console.warn("apa",child.val())
                    let a = child.val()
                    ANNOUNCEMENT.unshift(a);

                })
                    let SORTED = ANNOUNCEMENT.sort((a,b) => b.timestamp - a.timestamp)
                    that.setState({announcement:SORTED, loadHighlights: false});

                } else {
                    that.setState({announcement:'', loadHighlights: false})
                // console.log("Firebase is empty");
            }
        })
    }


    // ===== 
    openNoti = (item) => {
        let FUID = this.props.mobx_auth.FUID
        let KEY = item.key
        // firebase.database().ref('users/' + FUID + '/personal_notification/' + KEY + '/unread/').set(true)
        this.setState({
            title: item.title,
            description: item.body,
            timestamp: item.timestamp,
            modalNoti: true,
        })
    }

    closeModal=() => {
        this.setState({
            modalNoti: false,
            title: '',
            description: '',
            timestamp: ''
        
        })
    }


    render(){
        let content 

        if(this.state.loadHighlights === false && this.state.announcement !== ''){
            content =
            <View style={{flex:1,}} >
                <OpenModal handleClose={this.closeModal} isOpen={this.state.modalNoti} itemTitle={this.state.title} itemDescription={this.state.description} itemDate={this.state.timestamp}  />
                <FlatList 
                    showsVerticalScrollIndicator={false}
                    data={this.state.announcement}
                    renderItem = {({item}) => 
                    <TouchableWithoutFeedback onPress={() =>  this.openNoti(item)}>
                    <View style={styles.box}>
                        <View style={styles.circle}>
                            <Icon name='envelope-open' size={20} color='#080880' />
                        </View>
        
                        <View style={{ flexDirection:'column', flexShrink:1, marginLeft:15 }}>
                            <Text style={{ fontFamily: 'ProximaNova-Bold',fontSize: 16}}>{item.title}  </Text>
                            <Text style={styles.topText}>{item.body}  </Text>
                            <Text style={{marginTop:3, color:'grey', fontSize:11}}>{Moment(item.timestamp).format("DD MMM")} at {Moment(item.timestamp).format('HH:MM A')} </Text>
                        </View>
                    </View>
                    </TouchableWithoutFeedback>
                    }
                />
            </View>
        } 
        if( this.state.loadHighlights === false && this.state.announcement === '') {
            content = 
            <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                {/* <Image source={Empty} style={{height: 80, width:80}} /> */}
                <Text style={{fontSize:22, fontFamily: "ProximaNova-Bold", marginTop:15, marginBottom:10}}>No Notifications</Text>
                <Text>All your notifications will be displayed here</Text>
            </View>
        }

        if(this.state.loadHighlights === true){
            content = <NotificationLoader />
        }

        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Announcements' onPress={() => this.props.navigation.goBack()} />
                    
                    {content}
                    
                </View>
            </SafeAreaView>
        )
    }
}

InboxAnnouncements = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(InboxAnnouncements))
export default InboxAnnouncements

const styles = StyleSheet.create({
    box :{
        flexDirection:'row',
        borderBottomColor:'#ccc',
        borderBottomWidth:1,
        marginLeft: 20, 
        marginRight:15, 
        padding:15,
        paddingLeft:0
    },
    topText :{fontSize:13, marginTop: 6},
    bottomText:{ fontSize:13, color:'grey'},

    circle :{borderColor:'#080880', borderWidth:1, borderRadius: 20,  height: 40, width:40, alignItems:'center', justifyContent:'center'}

})