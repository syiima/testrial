import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,KeyboardAvoidingView, FlatList, TouchableOpacity, ScrollView, TouchableWithoutFeedback} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/AntDesign"

import Header from '../components/Header.js'
import PickerTextInput  from "../components/PickerTextInput.js"
import FormTextInput from "../components/FormTextInput.js"
import MiddleModal from "../components/MiddleModal.js"
import Button from "../components/Button.js"

const WIDTH = Dimensions.get('window').width - 40

class VehicleEdit2 extends React.Component{
    constructor(){
        super()
        this.state = {
            tire_d: '',
            tire_s:'',
            transmission: '',
            colour:'',
            cc: '',
            loadingstate: false,
            pick_diameter : [],
            pick_size : [],
            modalCC: false,
            modalTransmission: false,
            modalDia: false,
            modalSize: false,
            updating: false,
        }
    }
    
    componentDidMount(){
        this.runSetState()
        this.getTireD()
    }
    
    runSetState(){
        this.setState({
            tire_d: this.props.navigation.state.params.tire_d,
            tire_s: this.props.navigation.state.params.tire_s,
            transmission: this.props.navigation.state.params.transmission,
            colour: this.props.navigation.state.params.colour,
            cc: this.props.navigation.state.params.cc,
            loadingstate:true
        })
    }

    getTireD(){
        let that = this
        let DIAMETER = []
        let CURR_D = this.props.navigation.state.params.tire_d

        firebase.database().ref(`tire_diameter`).once('value').then((snapshot) => {
            if(snapshot.exists()){
                
                KEY = Object.keys(snapshot.val());
    
                KEY.forEach((dia_id) => {
                    let b = {}

                    b.name = snapshot.val()[dia_id]
                    b.key = dia_id
                    DIAMETER.push(b);
                    if(snapshot.val()[dia_id] === CURR_D){
                        this.setState({car_d: dia_id})
                        this.fetchTireSize(dia_id)
                    }
                })
                let SORTED = DIAMETER.sort((a, b) => a.name - b.name)
                that.setState({pick_diameter: SORTED})
                
            }
        })
    }

    fetchTireSize(TIRED){
        let TIRE_D =  TIRED
        let that = this;
        let TIRES = []

        firebase.database().ref(`tire_size/${TIRE_D}`).once('value').then((snapshot) => {
            if(snapshot.exists()){

            // let keys = Object.keys(snapshot.val());
            // keys.forEach( (v_key) => {
                let y = snapshot.val()
                
                // TIRES.push(y);
                    
            // })

            that.setState({pick_size:y, });
            
            } 
        })
    }


    handlePress = (x) => {
        this.setState({tire_d:x.name, modalDia: false,})
        this.fetchTireSize(x.key)
    }


    checkDia = () => {
        let display 

        if(this.state.tire_d === ''){
            display = 
            <PickerTextInput 
                title= 'TYRE SIZE'
                value= 'Select your vehicle tyre size'
                onPress={() =>  Alert.alert('Sorry', 'Please select your vehicle tyre\'s rim size first') }
            /> 
        } else {
            display = 
            <PickerTextInput 
                title= 'TYRE SIZE'
                value={this.state.tire_s ? this.state.tire_s : 'Select your vehicle tyre size'} 
                onPress={() => this.setState({modalSize:true})} 
            /> 
        }

        return display
    }

    update = () => {
        let FUID = this.props.mobx_auth.FUID
        let PLATE = this.props.mobx_carkey.PLATE
        let TIRE_D = this.state.tire_d
        this.setState({updating:true})


        // console.warn('hnnn', );
        let DATA = {
            transmission: this.state.transmission,
            colour: this.state.colour,
            carcc :this.state.cc,
            cartire_d :TIRE_D,
            cartire_s :this.state.tire_s,

        }
    
        firebase.database().ref(`plate_number/${PLATE}`).update(DATA)

        this.props.navigation.state.params.onNvaigateBack()
        this.props.navigation.goBack(null)
    }


    render(){

        let CC = [
            "660","1.0", "1.1", "1.2","1.3","1.4","1.5","1.6","1.7","1.8","1.9","2.0",
            "2.1","2.2","2.3","2.4","2.5","2.6","2.7","2.8","2.9","3.0","3.1","3.2", "3.3",
            "3.4","3.5","3.6"
          ];
        let content_cc = []
        for(var i = 0; i < CC.length; i++){
            let value = CC[i];
            content_cc.push(
              <View>
                <TouchableOpacity
                    style={(i === CC.length - 1) ? styles.noBorderText : styles.borderText}
                    onPress={()=> this.setState({cc:value,modalCC:false})}>
                    <Text style={{fontSize:16}}>{value}</Text>
                </TouchableOpacity>
              </View>
            )
        }

        let TRANSMISSION = ["Auto","Manual"];
        let content_transmission = []
        for(var i = 0; i < TRANSMISSION.length; i++){
            let value = TRANSMISSION[i];
            content_transmission.push(
                <View >
                    <TouchableOpacity 
                        style={(i === TRANSMISSION.length - 1) ? styles.noBorderText : styles.borderText}
                        onPress={()=> this.setState({transmission:value,modalTransmission:false})}>
                    <Text style={{fontSize:16}}>{value}</Text>
                    </TouchableOpacity>
                </View>
            )
        }

        let content_tired
        content_tired =
            <FlatList
                data={this.state.pick_diameter}
                renderItem={({item}) =>
                    <TouchableOpacity  style={styles.borderText}
                        onPress={() => {this.handlePress(item)}}>
                    <Text style={{fontSize:16}}>{item.name}</Text>
                    </TouchableOpacity>
                }
            />

        let SIZE = this.state.pick_size
        let content_size = []
        if(SIZE.length !== 0 && SIZE !== undefined){  
            for(var i = 0; i < SIZE.length; i++){
                let value = SIZE[i];
                content_size.push(
                    <View >
                        <TouchableOpacity 
                            style={(i === SIZE.length - 1) ? styles.noBorderText : styles.borderText}
                            onPress={()=> this.setState({tire_s:value,modalSize:false})}>
                        <Text style={{fontSize:16}}>{SIZE[i]}</Text>
                        </TouchableOpacity>
                    </View>
                )
            }
        }

        let submitButton;
        if(this.state.updating === true){
            submitButton = 
            <Button title='Updating ...' bgColor='#080880' width={WIDTH} marginTop={20}  />
          

        } else {
            submitButton = 
            <Button  onPress={this.update} title='Update' bgColor='#080880' width={WIDTH} marginTop={20} />

        }


        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Edit Details' onPress={() => this.props.navigation.goBack()} />
                    
                <KeyboardAvoidingView behavior={(Platform.OS === 'ios') ? "padding" : null} style={{flex:1}}  enabled>
                    <ScrollView showsVerticalScrollIndicator={false} >
                        <View style={styles.box}>
                            <FormTextInput 
                                title = 'COLOUR'
                                placeholder = 'eg: Black, Red, Midnight Blue'
                                value={this.state.colour} 
                                onChangeText={(x) => this.setState({colour:x})}
                            />
                            <PickerTextInput 
                                title= 'TRANSMISSION'
                                value={this.state.transmission } 
                                onPress={() => this.setState({modalTransmission:true})} 
                            /> 
                            <PickerTextInput 
                                title= 'CC'
                                value={this.state.cc} 
                                onPress={() => this.setState({modalCC:true})} 
                            /> 
                            <PickerTextInput 
                                title= 'RIM SIZE (inches) '
                                value={this.state.tire_d ? this.state.tire_d : 'Select your vehicle tyre rim size'} 
                                onPress={() => this.setState({modalDia:true})} 
                            /> 
                            
                            {this.checkDia()}

                        </View>

                        {submitButton}

                        <View style={{height: 15}} />
                    </ScrollView>



                </KeyboardAvoidingView>

                {/* MODAL */}

                <MiddleModal modalName={this.state.modalCC} height='94%'
                children={
                    <View style={{flex:1}} >
                        <Text style={{fontSize:20,marginBottom:15, fontFamily: "ProximaNova-Bold"}}>Select Vehicle CC</Text>
                        <ScrollView showsVerticalScrollIndicator={false} style={{ width:'100%',}}>
                            {content_cc}
                        </ScrollView>

                         <TouchableOpacity style={styles.button} onPress={()=> this.setState({modalCC:false})}>
                            <Text style={styles.text} >Close</Text>
                        </TouchableOpacity>

                    </View>
                } />

                <MiddleModal modalName ={this.state.modalTransmission}  children={
                    <View>
                       <Text style={{fontSize:20,marginBottom:15, fontFamily: "ProximaNova-Bold"}}>Select Vehicle Transmission</Text>
                           
                       {content_transmission}
                        
                       <TouchableOpacity style={styles.button} onPress={()=> this.setState({modalTransmission:false})}>
                            <Text style={styles.text} >Close</Text>
                        </TouchableOpacity>
                    </View>
                } />

                <MiddleModal modalName={this.state.modalDia} height='94%'
                children={
                    <View style={{flex:1}} >
                        <Text style={{fontSize:20,marginBottom:15, fontFamily: "ProximaNova-Bold"}}>Select Vehicle Tyre Diameter</Text>
                        <ScrollView showsVerticalScrollIndicator={false} style={{ width:'100%',}}>
                            {content_tired}
                        </ScrollView>

                         <TouchableOpacity style={styles.button} onPress={()=> this.setState({modalDia:false})}>
                            <Text style={styles.text} >Close</Text>
                        </TouchableOpacity>

                    </View>
                } />

                <MiddleModal modalName ={this.state.modalSize} height='94%' children={
                    <View style={{flex:1}} >
                        <Text style={{fontSize:20,marginBottom:15, fontFamily: "ProximaNova-Bold"}}>Select Vehicle Tyre Size</Text>
                        <ScrollView showsVerticalScrollIndicator={false} style={{ width:'100%',}}>
                            {content_size}
                        </ScrollView>

                         <TouchableOpacity style={styles.button} onPress={()=> this.setState({modalSize:false})}>
                            <Text style={styles.text} >Close</Text>
                        </TouchableOpacity>

                    </View>
                }  />

                    
                </View>
            </SafeAreaView>
        )
    }
}

VehicleEdit2 = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(VehicleEdit2))
export default VehicleEdit2

const styles = StyleSheet.create({
    box:{ 
        backgroundColor:'#fff', paddingLeft:15, paddingRight:15, paddingTop:25, borderRadius:10, margin:20, 
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowRadius: 3,
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.3,
            },
            android: {
                elevation: 3
            },
        }),
    },
    button :{
        marginTop:15,
        borderRadius:10,
        padding:15,
        backgroundColor:'#080880',
        width: Dimensions.get('window').width - 70,
    },
    text :{
        fontFamily:'ProximaNova-Bold', 
        fontSize:18, 
        color:'white', 
        textAlign:'center'
    },
    borderText: {padding:12, borderBottomColor:'#ccc', borderBottomWidth:0.5},
    noBorderText: {padding:12}
    
})