import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,StatusBar, KeyboardAvoidingView,Dimensions, ImageBackground, Alert,TouchableOpacity, ScrollView,Image, TextInput, SafeAreaView} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import RNExitApp from 'react-native-exit-app'

import upgrade from '../assets/update.png'

const CONST_HEIGHT = Dimensions.get('window').height;
const CONST_WIDTH = Dimensions.get('window').width;

class JailBreak extends React.Component{
  constructor(props){
    super(props);
    this.state = {
    }
  }

  exitApp = () => {
    RNExitApp.exitApp();
  }


  render(){
    return(
      <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
        <StatusBar barStyle="light-content" backgroundColor='#1d1d1d'/>
      <View style={styles.container}>
          <View style={{height:12}}></View>
          <Image source={upgrade} style={styles.img} />
          <View style={{marginBottom: 40}}>
            <Text style={{textAlign:'center', fontSize:18, fontFamily:'ProximaNova-Bold', marginBottom:15}}>Jailbroken / Rooted Device Detected</Text>
  
            <Text style={{textAlign:'center', fontSize:14}}>We have enhanced our security features. Please change your device settings or log in with a non-Jailbroken/Rooted device.</Text>
          </View>
  
  
          <TouchableOpacity style={styles.addi} onPress={this.exitApp}>
            <Text style={{color:'#fff', textAlign:'center', fontFamily:'ProximaNova-Bold',fontSize:17}}>OK, got it!</Text>
          </TouchableOpacity>

        
  
        </View>

        </SafeAreaView>


    )
  }
}

  const styles = {
    container: {
      flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 40,
        backgroundColor:'#fff',
    },
    img: {
      width: Dimensions.get('window').width - 80,
      height:Dimensions.get('window').width /1.5,
      marginBottom: 30,
      resizeMode:'contain'
    },

    addi:{
      backgroundColor:'#080880', 
      padding:15, borderRadius:10, 
      marginBottom: 15,
      width: Dimensions.get('window').width - 80,
      // height:20,
    }
  };
export default JailBreak;
