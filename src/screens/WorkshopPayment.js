import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,Image,ActivityIndicator, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import AntIcon from "react-native-vector-icons/AntDesign";
import Foundation from 'react-native-vector-icons/Foundation';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { NavigationActions, StackActions } from 'react-navigation';
import {cloud_serv_user_sandbox, cloud_serv_user_production} from "../util/FirebaseCloudFunctions.js"

import { credentials } from '../util/BillPlzCredentials.js';

import Header from '../components/Header.js'
import WorkshopInfo from "../components/WorkshopInfo.js"
import Conditional from "../components/WorkshopQuotationConditional.js"
import BottomModal from "../components/BottomModal.js"

import fpx from '../assets/logo/fpxLogo.png';
import tng from '../assets/logo/tngLogo.png'
import Payment from '../assets/payment.png'


class WorkshopPayment extends React.Component{
    constructor(){
        super()
        this.state = {
            finalPrice:'',
            requestData: '',
            quote:'',
            email: '',
            name: '',
            DOB:'',
            bookingID: '',
            checkout_state :false,
            logo: '',
            ws_data :'',
            tng_pay: false,
            paymentType :'',
            modalPayment: false,
            status :''

        }
    }
    
    componentDidMount(){
        this.fetchRequest()
        this.fetchWorkshopDets()
        this.fetchConst()
        this.statusListener()
    }

    componentWillUnmount(){
        let PLATE = this.props.mobx_carkey.PLATE
        firebase.database().ref(`plate_number/${PLATE}/book_walkin/`).off()
    }
    
    fetchRequest(){
        let that =this
        let DATA = []
        let FUID = this.props.mobx_auth.FUID
        let BOOK_ID =  this.props.mobx_retail.R_WALKIN_BOOKID
        let WS_ID =   this.props.mobx_retail.R_ID
        let WSMAIN_ID =   this.props.mobx_retail.R_GROUPID
        let finalP, EMAIL, NAME, REQID, QUOTATION, X, STATUS

        firebase.database().ref(`request_walkin/${WSMAIN_ID}/${WS_ID}/${BOOK_ID}`).once('value').then((snapshot) => {
            if(snapshot.exists()){
                X = snapshot.val()

                finalP = X.quotation.total_price
                EMAIL = X.customer_email
                NAME = X.customer_name
                REQID = X._requestID

                QUOTATION = X.quotation
                STATUS = X.status

                if(X.payment !== undefined){
                    this.setState({paymentType: X.payment})
                }
            } 

            that.setState({requestData : X, quote: QUOTATION, finalPrice: finalP,bookingID: REQID, email: EMAIL, name: NAME, checkout_state: false, status: STATUS})
        })

        firebase.database().ref(`users/${FUID}/DOB`).once('value').then((snp) => {
          if(snp.exists()){
            let X = snp.val() !== undefined ? snp.val() : ''

              this.setState({DOB: X})
          }
        })

    }

    fetchWorkshopDets(){
        let that = this
        let WSDATA = []
        let BOOK_ID =  this.props.mobx_retail.R_WALKIN_BOOKID
        let RETAIL =   this.props.mobx_retail.R_ID
        let RETAIL_MAIN =   this.props.mobx_retail.R_GROUPID
        // console.warn('hellllooo', WSMAIN_ID, WS_ID);
        
        firebase.database().ref(`retail/${RETAIL_MAIN}/${RETAIL}/`).once('value').then((snapshot) => {
            if(snapshot.exists()){
                let a = snapshot.val()
                that.setState({ws_data: a})
            }
        })

        firebase.database().ref(`retail_main/${RETAIL_MAIN}/logo`).once('value').then((snapshot) => {
            if(snapshot.exists()){
                let b = snapshot.val()
                that.setState({logo: b})
            }
        })

    }
    fetchConst = () => {
        let that = this;
    
        firebase.database().ref('consts/').once('value').then( (snapshot) => {
          if(snapshot.exists()){
            that.setState({
              tng_pay: snapshot.val().tng_pay
            })
          }
        })
    }

    statusListener(){
        //This function is used to control the paid status {Pending, Unpaid, Paid} on the car profile change
        let that = this;
        let PLATE = this.props.mobx_carkey.PLATE
        let BOOK_ID =  this.props.mobx_retail.R_WALKIN_BOOKID
        let WS_ID =   this.props.mobx_retail.R_ID
        let WSMAIN_ID =   this.props.mobx_retail.R_GROUPID
        
        firebase.database().ref(`plate_number/${PLATE}/book_walkin/`).on('child_changed', async (snapshot) => {
            if(snapshot.exists()){
                if(snapshot.val() === "Fail"){
                    await Alert.alert("Whoops!", "There was a problem processing your payment. Please try again.")
                    
                    firebase.database().ref(`plate_number/${PLATE}/book_walkin/`).update({_book_status: 'Complete'})
                    firebase.database().ref(`request_walkin/${WSMAIN_ID}/${WS_ID}/${BOOK_ID}/payment/`).remove()
                    that.setState({checkout_state :false})
                    that.props.navigation.navigate('WorkshopPayment')
                }
    
    
                if(snapshot.val() === "Paid"){
                    if(that.state.paymentType === 'tng'){
                        // that.sendTelegramText()
                        that.updateHistory()
                    } else {
                        await Alert.alert("Payment Successful!")
                        that.navigateFeedback()
                    }
                }
               
            } 
    
        })
    }

    checkPaymentType(){
        let FUID = this.props.mobx_auth.FUID
        let BOOK_ID =  this.props.mobx_retail.R_WALKIN_BOOKID
        let WS_ID =   this.props.mobx_retail.R_ID
        let WSMAIN_ID =   this.props.mobx_retail.R_GROUPID

        firebase.database().ref(`request_walkin/${WSMAIN_ID}/${WS_ID}/${BOOK_ID}`).once('value').then((snp) => {
            if(snp.exists()){
                let x = snp.val().paymentType 

                if(x !== undefined){
                    this.setState({paymentType: x})
                }
            }
        })

    }
      
    // ===============================================================
    // ===================== payment gateway ========================
    // ===============================================================
    
    clearState = () => {
        let BOOK_ID =  this.props.mobx_retail.R_WALKIN_BOOKID
        let WS_ID =   this.props.mobx_retail.R_ID
        let WSMAIN_ID =   this.props.mobx_retail.R_GROUPID

        Alert.alert('Cancelled Payment', 'You just cancelled your online payment.')
        firebase.database().ref(`request_walkin/${WSMAIN_ID}/${WS_ID}/${BOOK_ID}/payment/`).remove()
        this.setState({checkout_state:false, paymentType: ''})
    }
    
    billplz = async () => {
       //Function to run billplz function
      let that = this;
      await this.check_description()
  
      let AMOUNT = this.state.finalPrice * 100;
      //GET THE DESCRIPTION VALUE
      let DATA = this.state.requestData;
      let FINAL_DESCRIPTION = 'services'
  
  
      let DESCRIPTION = "<p><b><h4>" + 'Services' + "</h4></b></p>"
      let EMAIL = this.state.email;
      let NAME = this.state.name;
      let usethis;
      let collectionID;
      let AUTH_KEY;
      let CALLBACK_URL;
      // console.warn('nn', EMAIL, NAME);
  
      //Check for the beta and production config
      // console.warn("mana satu", this.props.mobx_config.config);
      if(this.props.mobx_config.config === 0) {
          usethis = credentials.method_sandbox;
          collectionID = "collection_id=" + credentials.collectionID_walkin_beta + "&description=";
          AUTH_KEY = credentials.header_auth_beta;
        CALLBACK_URL = "https://asia-east2-servuserplayground.cloudfunctions.net/updateBillPlzBranch_workshop_asia"

      
      }
      if(this.props.mobx_config.config === 1) {
          usethis = credentials.method_production;
          collectionID = "collection_id=" + credentials.collectionID_production_walkin + "&description=";
          AUTH_KEY = credentials.header_auth_production;
          CALLBACK_URL = "https://asia-east2-serv-v2-production-fcb23.cloudfunctions.net/updateBillPlzBranch_workshop_asia"
      }
  
      
      //URL POST METHOD FOR CREATING BILL IN BILLPLZ
      fetch( usethis, {
        body: collectionID + DESCRIPTION + "&email="+ EMAIL +"&name="+ NAME +"&amount=" + AMOUNT + "&callback_url=" + CALLBACK_URL + "&redirect_url=" + credentials.redirect_url,
        headers: {
          Authorization: AUTH_KEY,
          "Content-Type": "application/x-www-form-urlencoded"
        },
        method: "POST"
      })
      .then(response => {
        return response.json()
      }).then(response => {
        console.warn(response);
        let MINUS;
        if(this.props.mobx_config.config === 0){
          MINUS = credentials.callback_string_sandbox.length;
        }
        if(this.props.mobx_config.config === 1){
          MINUS = credentials.callback_string_production.length;
        }
  
        var str = response.url;
  
        // var x = str.length;
        // var res = str.substr(MINUS, x);
        var x = str.split(/[\s/]+/)
        const res = x[x.length-1]

        // this.setState({billplz_id: res})
        // console.warn(res)
  
        let UID = this.props.mobx_auth.FUID;
        let BOOK_ID =  this.props.mobx_retail.R_WALKIN_BOOKID
        let WS_ID =   this.props.mobx_retail.R_ID
        let WSMAIN_ID =   this.props.mobx_retail.R_GROUPID
        let PROMO_CODE = "";
        let PLATE = this.props.mobx_carkey.PLATE
        
        // console.warn("James Arthur", PROMO_CODE);
  
        firebase.database().ref('request_walkin/' + WSMAIN_ID + '/' + WS_ID + '/' + BOOK_ID).update({billplz_id: res,})


        firebase.database().ref('billplz_ID_walkin/' + res).update({
          booking_id: BOOK_ID,
          customer_uid: UID,
          customer_vehicleID: PLATE,
          promo_code: PROMO_CODE,
          timestamp: firebase.database.ServerValue.TIMESTAMP,
          workshop_id: WS_ID,
          workshop_main: WSMAIN_ID,
        })

        // firebase.database().ref('users/' + UID + )
  
        // Navigate to the browser for the payment screen
        this.props.navigation.navigate("BillPlzWebview", {
          url_link: response.url,
          customer_vehicleID:PLATE,
          _book_id: BOOK_ID,
          final_price: this.state.finalPrice,
          backBut: this.clearState
          // workshop_id: WS_ID,
          // workshop_main: WSMAIN_ID,
        });
  
        that.setState({checkout_state:false})
  
      }).catch(error => {
            let MONTH = (new Date().getMonth()) + 1
            let m = error.message
            firebase.database().ref(`crash_error/${MONTH}/`).push({
                error: m,
                timestamp:Date.now(),
                user: UID,
                platform :Platform.OS,
                version : this.props.mobx_config.Version,
                from: 'billplz payment'
            })
        // console.warn(error.message)
        Alert.alert("Sorry, please try again.", "If the issue persists, please contact our customer service. Thank you!")
        that.setState({checkout_state:false})
      });
    
    
    
    }

    tng = async() => {
        let clearID = AsyncStorage.removeItem('acquirementId')
        // let OPEN_WITH = x
  
        this.setState({ paymentType:'tng',modalTng:false, checkout_state: true, modalPayment: false})
        // alert('Hey, NO!')
        let that = this;
        await this.check_description()
  
        let AMOUNT = this.state.finalPrice * 100;
        let FINAL_DESCRIPTION = 'services'
        let DESCRIPTION = "<p><b><h4>" + 'Services' + "</h4></b></p>"
        let EMAIL = this.state.email;
        let NAME = this.state.name;
        let PLATE = this.props.mobx_carkey.PLATE
  
        let URL,ENVIRONMENT, MONGO, LINK
        if(this.props.mobx_config.config === 0) {
            URL = 'http://pay-staging.serv.my/api/touchngo/createPayment'
            ENVIRONMENT = 'development'
            MONGO = "http://pay-staging.serv.my/api/touchngo/mongo/closeStatus"
            LINK = 'http://pay-staging.serv.my/api/touchngo/updateAcquirementId'
        }
  
        if(this.props.mobx_config.config === 1) {
            URL = 'https://pay.serv.my/api/touchngo/createPayment'
            ENVIRONMENT = 'production'
            MONGO = "https://pay.serv.my/api/touchngo/mongo/closeStatus"
            LINK = 'https://pay.serv.my/api/touchngo/updateAcquirementId'
        }
  
        let FUID = this.props.mobx_auth.FUID;
        let BOOK_ID =  this.props.mobx_retail.R_WALKIN_BOOKID
        let WS_ID =   this.props.mobx_retail.R_ID
        let WSMAIN_ID =   this.props.mobx_retail.R_GROUPID
        let APP_VERSION = this.props.mobx_config.Version
        let OS = Platform.OS
        let REQID = this.state.bookingID
        let DOB = this.state.DOB
  
        let DATA  = {
          body: {
            order: {
                orderTitle: 'Drive In Payment',
                orderAmount: {
                    currency: 'MYR',
                    value: AMOUNT
                },
            },
            signAgreementPay: false,
            envInfo: {
                terminalType: "SYSTEM",
                orderTerminalType: "APP"
            },
            
            subMerchantId: WS_ID, // retail_id
            subMerchantName: 'SERV TECHNOLOGY', // Outlet name
  
          },
          bookingId: BOOK_ID,
          environment: ENVIRONMENT,
          timestamp_invoke: Date.now(),
          status_paid: 'Pending',
          transaction_id: REQID,  //reference
          app_version: APP_VERSION,
          email: EMAIL,
          os_type: OS,
          user_id: FUID,
          name: NAME,
          DOB: DOB,
          type_of_serv: 'walkin',
          vehicle_id: PLATE,
          subMerchantMain: WSMAIN_ID,
        }
  
      // console.log("hmm URL", DATA);
        fetch(URL,{
          method: 'POST',
          headers: {
            "Content-Type": "application/json",
            token : "ofhAei9pg0WMZJ5O8kGSf6aA0yCeKZJryxfn0rFK6uvicPvKkgQ5tHNzbUpU77Ks"
          },
          body: JSON.stringify(DATA)
        })
        .then(resp => {
          console.warn('json', resp);
          if(resp.status === 404){
            Alert.alert("Woops!", "There was a problem creating your payment. Please try again.")
            firebase.database().ref(`request_walkin/${WSMAIN_ID}/${WS_ID}/${BOOK_ID}/payment/`).remove()

            this.setState({checkout_state: false})
            return
          }
          return resp.json()
        })
        .then(res => {
            // console.log("log2", res); //2nd log
            let acquirementID = res.respond_tng.response.body.acquirementId ? res.respond_tng.response.body.acquirementId : null
            let merchantTransId = res.respond_tng.response.body.merchantTransId ? res.respond_tng.response.body.merchantTransId : null
            let URL = res.respond_tng.response.body.checkoutUrl
    
            // console.log('mechID', acquirementID);
    
            if(acquirementID === null || merchantTransId === null){
                // console.warn('whaaa', acquirementID, merchantTransId);
                let MCHTD = res.respond_mongo.data.body.order.merchantTransId
                // console.log('HOOIOJLDKLKFNJKF', MCHTD);
                let N = {
                merchantTransId: MCHTD
                }
    
                fetch(MONGO,{
                    method: 'POST',
                    headers: {
                        "Content-Type": "application/json",
                        token : "ofhAei9pg0WMZJ5O8kGSf6aA0yCeKZJryxfn0rFK6uvicPvKkgQ5tHNzbUpU77Ks"
                },
                    body: JSON.stringify(N)
                })
                Alert.alert("Whoops, Invalid Signature!", "There was a problem creating your payment. Please try again.")
                firebase.database().ref(`request_walkin/${WSMAIN_ID}/${WS_ID}/${BOOK_ID}/payment/`).remove()

                this.setState({checkout_state: false})
                return
            }
    
                this.setState({acquirementID: acquirementID})
                let saveID = AsyncStorage.setItem('acquirementId', acquirementID)
        
            
    
            let P = {
                acquirementId: acquirementID,
                merchantTransId: merchantTransId,
            }
    
            //   console.log('acquirement', acquirementID, merchantTransId);
            fetch(LINK, {
                method: 'POST', 
                headers: {
                    "Content-Type": "application/json",
                    token : 'ofhAei9pg0WMZJ5O8kGSf6aA0yCeKZJryxfn0rFK6uvicPvKkgQ5tHNzbUpU77Ks'
                },
                body: JSON.stringify(P)
            })
    
            this.props.navigation.navigate("TngWebview", {
                url_link: URL,
                customer_vehicleID:PLATE,
                _book_id: BOOK_ID,
                final_price: this.state.finalPrice,
                acquirementId: acquirementID
            });
  
        })
        .catch((err) => {
            
            let MONTH = (new Date().getMonth()) + 1
            let m = err.message
            firebase.database().ref(`crash_error/${MONTH}/`).push({
                error: m,
                timestamp:Date.now(),
                user: FUID,
                platform :Platform.OS,
                version : this.props.mobx_config.Version,
                from: 'tng payment'
            })

            Alert.alert('Sorry', 'An error occured. Please contact SERV Team if the issue still persists');
            this.setState({checkout_state: false})
            return

        })
    }
  
    check_description = (x) => {
        //Function used for setting the checkout to avoid double click for checking out
        this.setState({checkout_state : true, modalPayment:false})
    }

    updateHistory = () => {
      
        let that = this
        let USER_ID = this.props.mobx_auth.FUID;
        let BOOKING_ID =  this.props.mobx_retail.R_WALKIN_BOOKID
        let WORKSHOP_ID =   this.props.mobx_retail.R_ID
        let WORKSHOP_MAIN =   this.props.mobx_retail.R_GROUPID
        let PLATE = this.props.mobx_carkey.PLATE
        let VTYPE = this.props.mobx_carkey.VTYPE;
        let REF 

  
        firebase.database().ref(`plate_number/${PLATE}/book_walkin/`).once('value').then((snp) => {
            if(snp.exists()){
                if(snp.val()._book_status === 'Paid'){        
                    firebase.database().ref(`request_walkin/${WORKSHOP_MAIN}/${WORKSHOP_ID}/${BOOKING_ID}/checklist/`).once('value').then((snapshot) => {
                            if (snapshot.exists()) {
                                CAR_HEALTH = snapshot.val()
                                CAR_HEALTH.timestamp = firebase.database.ServerValue.TIMESTAMP
                        }
                    }).then(() => {
                        if(VTYPE === 'Motorcycle'){
                            REF = `plate_number/${PLATE}/motor_health/`
                        } else { REF = `plate_number/${PLATE}/car_health/`}
                        firebase.database().ref(REF).update(CAR_HEALTH)

                    })
        
                //2. update book_history
                firebase.database().ref(`request_walkin/${WORKSHOP_MAIN}/${WORKSHOP_ID}/${BOOKING_ID}`).once('value').then((snapshot) => {
                    if (snapshot.exists()) {
                        let DATA = snapshot.val()
                        let USER_HISTORY = {
                            _requestID: DATA._requestID,
                            assign: '',
                            checklist: DATA.checklist,
                            customer_nameplace: DATA.customer_nameplace,
                            customer_address: DATA.customer_address,
                            customer_carmake: DATA.customer_carmake,
                            customer_carmodel: DATA.customer_carmodel,
                            customer_carplate: DATA.customer_carplate,
                            customer_caryear: DATA.customer_caryear,
                            customer_day: DATA.customer_day,
                            customer_month: DATA.customer_month,
                            customer_time: DATA.customer_time,
                            customer_year: DATA.customer_year,
                            quotation: DATA.quotation,
                            timestamp: DATA.timestamp,
                            archived: firebase.database.ServerValue.TIMESTAMP,
                            retail_main: WORKSHOP_MAIN,
                            retails_id: WORKSHOP_ID,
                            type: 'walkin'
                        }
                        firebase.database().ref(`plate_number/${PLATE}/book_history/${BOOKING_ID}`).update(USER_HISTORY)
                
                        //3. update 
                        let roadtax_expiry_date = DATA.checklist.roadtax_expiry_date
                        let next_service_date = DATA.checklist.next_service_date
                        let RT_DATE,RT_MONTH, RT_YEAR
                        if(roadtax_expiry_date !== ''){
                            RT_DATE = new Date(roadtax_expiry_date).getDate()
                            RT_MONTH = new Date(roadtax_expiry_date).getMonth()
                            RT_YEAR = new Date(roadtax_expiry_date).getFullYear()
                        }

                
                        let NS_DATE = new Date(next_service_date).getDate()
                        let NS_MONTH = new Date(next_service_date).getMonth()
                        let NS_YEAR = new Date(next_service_date).getFullYear()
                
            
                        firebase.database().ref(`users/${USER_ID}/pushToken`).once('value').then((snapshot) => {
                            if(snapshot.exists()){
                                if(roadtax_expiry_date !== ''){
                                    firebase.database().ref(`reminder_roadtax/${RT_YEAR}/${RT_MONTH}/${RT_DATE}/${USER_ID}`).update({
                                        plate: DATA.customer_carplate,
                                        pushToken: snapshot.val(),
                                        vehicleID: DATA.customer_vehicleID,
                                    })
                                }

                    
                                firebase.database().ref(`reminder_next_service/${NS_YEAR}/${NS_MONTH}/${NS_DATE}/${USER_ID}`).update({
                                plate: DATA.customer_carplate,
                                pushToken: snapshot.val(),
                                vehicleID: DATA.customer_vehicleID,
                                })
                    
                                return null
                            } else {
                                return null
                            }
                        }).catch(err => {
                        console.log('Error ', err.message)
                        //   return 'Error'
                        })
                    }
                })
        
        
                Alert.alert("Payment Successful!")
                this.navigateFeedback()
            }

            if(snp.val()._book_status === "Fail"){
                Alert.alert("Whoops!", "There was a problem processing your payment. Please try again.")
                firebase.database().ref(`request_walkin/${WSMAIN_ID}/${WS_ID}/${BOOK_ID}/payment/`).remove()

                firebase.database().ref(`plate_number/${PLATE}/book_walkin/`).update({_book_status: 'Complete'})
                that.setState({checkout_state :false})
                that.props.navigation.navigate('WorkshopPayment')
            }
  
          } 
        })
    }
  

      
  
    // sendTelegramText = () => {
    //     let TELEGRAM_URL
  
    //     if(this.props.mobx_config.config === 0) {
    //       TELEGRAM_URL = 'https://api.telegram.org/bot272822956:AAEduqgWA9gt6tpxYHKp2h1Opr4ZFNftyNc/sendMessage?chat_id=-314723225&text=';
    //     }
    //     if(this.props.mobx_config.config === 0) {
    //       TELEGRAM_URL = 'https://api.telegram.org/bot272822956:AAEduqgWA9gt6tpxYHKp2h1Opr4ZFNftyNc/sendMessage?chat_id=-313474249&text='
    //     }
    //     let CUSTOMER_NAME = this.state.name
    //     let REQUESTID = this.state.bookingID
    //     let TEXT = 'A user has made payments using TouchnGo e-Wallet on the ' + 'SERV app' + '.%0A%0ACustomer name : ' + CUSTOMER_NAME + '%0ARequest ID : ' + REQUESTID;
    //     let FINAL = TELEGRAM_URL + TEXT
  
    //     fetch(FINAL)
    //     .then(res => res.json())
    //     .then(res => {console.log('succ')})
  
    // }
      
    renderInfo = () => {
        let item = this.state.requestData
        let display
  
        if(item !== ''){
            display =    
            <View style={{padding:20}}>
                    <View style={{flexDirection:'row'}}>
                    <Text style={{flex:1}}>Quotation No</Text>
                    <Text style={styles.boldText}>: </Text>
                    <Text style={{...styles.boldText, fontSize:17, flex:1.5}}>{item._requestID}</Text>
                </View>
                <View style={{flexDirection:'row'}}>
                    <Text style={{flex:1}}>Date</Text>
                    <Text style={styles.boldText}>: </Text>
                    <Text style={{...styles.boldText, fontSize:17, flex:1.5}}>{item.customer_day} {item.customer_month} {item.customer_year}</Text>
                </View>
                <View style={{flexDirection:'row'}}>
                    <Text style={{flex:1}}>Time</Text>
                    <Text style={styles.boldText}>: </Text>
                    <Text style={{...styles.boldText, fontSize:17, flex:1.5}}>{item.customer_time} </Text>
                </View>
                <View style={{flexDirection:'row'}}>
                    <Text style={{flex:1}}>Vehicle Plate No</Text>
                    <Text style={styles.boldText}>: </Text>
                    <Text style={{...styles.boldText, fontSize:17, flex:1.5}}>{item.customer_carplate} </Text>
                </View>
            </View>
        
        }
        return display
    }

    renderQuote = () => {
        let item = this.state.quote
        let STATUS = this.state.status
        let display
        if(item !== ''){
            display = 
                <View>
                    <View style={{...styles.stats, backgroundColor:'#FCE5CD', marginTop:20, borderBottomLeftRadius:0, borderBottomRightRadius:0,}}>
                    
                        <View style={{flexDirection:'row', justifyContent:'space-between', marginBottom:15, padding:5}}>
                        <Text style={styles.boldText}>Your Total</Text>
                            <View style={{flexDirection:'row'}}>
                                <Text style={{...styles.boldText, fontSize:12}}>RM</Text>
                                <Text style={{...styles.boldText, fontSize:24}}>{item.total_price}</Text>
                            </View>
                        </View>
  
                        {this.renderInfo()}
                    </View>

                    <View style={styles.boxItem}>
                        <Conditional item={item} status={STATUS} />
                    </View>
  
  
                </View>
        
        }
        return display
    }

    
    setPayment = (x) => {
        let BOOKING_ID =  this.props.mobx_retail.R_WALKIN_BOOKID
        let WORKSHOP_ID =   this.props.mobx_retail.R_ID
        let WORKSHOP_MAIN =   this.props.mobx_retail.R_GROUPID
        let PLATE = this.props.mobx_carkey.PLATE

        if(x === 'cash'){
            this.setState({paymentType: 'cash', modalPayment: false})
            firebase.database().ref(`request_walkin/${WORKSHOP_MAIN}/${WORKSHOP_ID}/${BOOKING_ID}/payment`).set('cash')
        } else {
            this.setState({paymentType: 'online'})
            firebase.database().ref(`request_walkin/${WORKSHOP_MAIN}/${WORKSHOP_ID}/${BOOKING_ID}/payment`).set('online')
            
            if(x === 'tng'){
                this.tng()
            } else {
                this.billplz()
            }
        }
        
    }


    navigateFeedback = () => {
        const navigateAction = StackActions.replace({
            routeName: 'WorkshopFeedback',
            action: NavigationActions.navigate({ routeName: 'WorkshopFeedback' }),
          });
        this.props.navigation.dispatch(navigateAction);
    }

    render(){
        let content_view
        let IMG = <Image source={Payment} style={{height: Dimensions.get('window').width /4, width:Dimensions.get('window').width /4, resizeMode:'contain',marginLeft:15}}/>

        if(this.state.paymentType === 'online' || this.state.paymentType === '' ){
            content_view =        
            <View style={{...styles.stats, backgroundColor:'#FEF2CC', flexDirection:'row',alignItems:'center'}}>
                <View style={{flex: 3,}}>
                <Text style={{fontFamily:'ProximaNova-Bold', fontSize:16, marginBottom:15}}>Your service is completed! Please select your preferred payment method.</Text>
                <Text style={{fontSize:13}}>Your service is done/complete! Please select your preferred payment method.</Text>
                </View>
                {IMG}
            </View>
        
        }
        if(this.state.paymentType === 'cash' || this.state.paymentType === 'cash_transfer' || this.state.paymentType === 'cash_terminal'){
            content_view = 
            <View style={{...styles.stats, backgroundColor:'#D9EAD3', flexDirection:'row', alignItems:'center'}}>
                <View style={{flex: 3,}}>
                    <Text style={{fontFamily:'ProximaNova-Bold', fontSize:16, marginBottom:15}}>Waiting for payment confirmation by cash</Text>
                    <Text style={{fontSize:13}}>Please be patient and await for your service provider to complete mark your payment by cash as completed. You will be directed to a receipt page once it is completed.</Text>
                </View>
                {IMG}
            </View>
        }

        let buttonPay;

        if(this.state.checkout_state === true && this.state.paymentType === ''){
            buttonPay = 
            <View style={styles.bottomButton}>
                <Text style={{fontFamily:'ProximaNova-Bold', color:'white', fontSize:17}}>Make Payment </Text>
                <ActivityIndicator style={{ height: 20, width: 20, alignSelf:'center'}} size="small" color="#fff" />
            </View>
        } 
        if(this.state.checkout_state === false && this.state.paymentType === ''){
            buttonPay =
            <TouchableOpacity style={styles.bottomButton}  onPress={() => this.setState({modalPayment: true})}>
                <Text style={{fontFamily:'ProximaNova-Bold', color:'white', fontSize:17}}>Make Payment </Text>
                <View style={{flexDirection:'row', alignItems:'center'}}>
                    <Text style={{color:'white', fontSize:11, fontFamily:'ProximaNova-Bold'}}>RM </Text>
                    <Text style={{fontFamily:'ProximaNova-Bold', fontSize:17, color:'white'}}>{this.state.finalPrice}</Text>
                </View>
            </TouchableOpacity>
        } 


        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Payment' onPress={() => this.props.navigation.popToTop()} />
                    
                    <ScrollView showsVerticalScrollIndicator={false}>

                        {content_view}

                        <View style={styles.hmm}>
                            <WorkshopInfo navigation={this.props.navigation} data={this.state.ws_data} logo={this.state.logo} />
                        </View>
                            {this.renderQuote()}

                
                    </ScrollView>

                    {buttonPay}

                    <BottomModal
                        modalName={this.state.modalPayment}
                        onPress={() => this.setState({modalPayment:false})}
                        style={styles.innerModal}
                        children = {
                            <View >          
                                <View style={{padding:20, marginBottom:10, alignItems:'center'}}>
                                    <Text style={{textAlign:'center', color:'#080880', fontFamily:'ProximaNova-Bold', fontSize:20, marginBottom:5}}>Choose a payment method</Text>
                                </View>

                               


                                <View style={{marginLeft:15,marginRight:15}}>
                                {this.state.tng_pay === true ?
                                    <TouchableOpacity  onPress={() => this.setPayment('tng')} style={{padding:15,  flexDirection:'row', alignItems:'center', borderBottomColor:'#ccc',  borderBottomWidth:0.5, }}>
                                        <View style={{height:40, width:40,borderRadius:20, marginRight:18, backgroundColor:'white', borderWidth:0.5, color:'grey', alignItems:'center', justifyContent:'center'}}>
                                            <Image source={tng} style={{height:30, width:30,resizeMode:'contain'}}/>
                                        </View>
                                        <View>
                                            <Text style={{marginBottom:5, }}>Touch 'n Go eWallet</Text>
                                            <Text  style={{color:'grey', fontSize:11}}>Pay with your Touch 'n Go ewallet</Text>
                                        </View>
                                    </TouchableOpacity>

                                    : null }
                                    <TouchableOpacity onPress={() => this.setPayment('cash')} style={{padding:15,  borderBottomColor:'#ccc',  borderBottomWidth:0.5, alignItems:'center',flexDirection:'row', }}>
                                        <View style={{height:40, width:40,borderRadius:20, marginRight:18, backgroundColor:'white', borderWidth:0.5, color:'grey', alignItems:'center', justifyContent:'center'}}>
                                            <Foundation name='dollar' color='green' size={30} />
                                        </View>
                                        <View>
                                            <Text style={{marginBottom:5, }}>Offline payment</Text>
                                            <Text  style={{color:'grey', fontSize:11}}>Pay by cash, bank transfer or card payment to merchant</Text>
                                        </View>
                                    </TouchableOpacity>

                                    <TouchableOpacity  onPress={() => this.setPayment('billplz')} style={{padding:15,  flexDirection:'row', alignItems:'center',}}>
                                        <View style={{height:40, width:40,borderRadius:20, marginRight:18, backgroundColor:'white', borderWidth:0.5, color:'grey', alignItems:'center', justifyContent:'center'}}>
                                            <Image source={fpx} style={{height:20, width:20, resizeMode:'contain'}}/>
                                        </View>
                                        <View>
                                            <Text style={{marginBottom:5, }}>Online Payment</Text>
                                            <Text  style={{color:'grey', fontSize:11}}>Debit/Credit Card and Online Banking</Text>
                                        </View>
                                    </TouchableOpacity>
                                    
                                    

                                    
                                </View>
                                
                                <TouchableOpacity style={styles.closeButton} onPress={()=> this.setState({modalPayment:false})}>
                                    <Text style={{color:'white', fontFamily:'ProximaNova-Bold', fontSize:18}}> Close </Text>
                                </TouchableOpacity>
                          </View>
                        }
                    />

                    
                </View>
            </SafeAreaView>
        )
    }
}

WorkshopPayment = inject('mobx_auth', 'mobx_carkey', 'mobx_retail', 'mobx_config')(observer(WorkshopPayment))
export default WorkshopPayment

const styles = StyleSheet.create({
    stats:{
        padding:15, margin:15, borderRadius:15, marginBottom:0,
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 2.22,
              shadowOffset: { width: 0, height: 1 },
              shadowOpacity: 0.22,
            },
            android: {
              elevation: 3
            },
          }),
    },
    boldText:{fontFamily:'ProximaNova-Bold'},
    bottomButton:{
        backgroundColor:'#080880', padding:15, borderRadius:15, margin:15, alignSelf:'stretch', width:'94%',
        flexDirection:'row', alignItems:'center', justifyContent:'space-between',
        ...Platform.select({
          ios: {
            shadowColor: '#000',
            shadowRadius: 2.22,
            shadowOffset: { width: 0, height: 1 },
            shadowOpacity: 0.22,
          },
          android: {
            elevation: 3
          },
        }),

    },
    boxItem:{
        padding:15, margin:15, borderBottomLeftRadius:15, borderBottomRightRadius:15, marginTop:0, backgroundColor:'white',
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 2.22,
              shadowOffset: { width: 0, height: 1 },
              shadowOpacity: 0.22,
            },
            android: {
              elevation: 3
            },
          }),
    },
    hmm :{margin: 15, marginTop: 15,backgroundColor:'white', borderRadius:15, marginBottom:0,
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 2.22,
              shadowOffset: { width: 0, height: 1 },
              shadowOpacity: 0.22,
            },
            android: {
              elevation: 3
            },
          }),
    },
    innerModal: {     
        borderTopLeftRadius: 10,
        borderTopRightRadius:10,
        width:Dimensions.get('window').width,
        position:'absolute',
        bottom:0,
        backgroundColor: '#fff',
            ...Platform.select({
                ios: {
                    shadowColor: '#000',
                    shadowRadius: 2.22,
                    shadowOffset: { width: 0, height: 1 },
                    shadowOpacity: 0.22,
                    },
                android: {
                    elevation: 3
                },
            }),
    },
    closeButton: {
        backgroundColor: '#080880',
        borderRadius: 10,
        padding: 10,
        alignItems: 'center',
        // width:'90%',
        margin:15
    },
    
})