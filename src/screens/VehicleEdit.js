import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,KeyboardAvoidingView,Dimensions, TouchableWithoutFeedback, FlatList, TouchableOpacity, ScrollView, Touchable} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/AntDesign"

import Header from "../components/Header.js"
import Button from "../components/Button.js"
import PickerTextInput  from "../components/PickerTextInput.js"
import FormTextInput from "../components/FormTextInput.js"
import MiddleModal from "../components/MiddleModal.js"

const WIDTH = Dimensions.get('window').width - 40

class VehicleEdit extends React.Component{
    constructor(){
        super()
        this.state = {
            make: '',
            year: '',
            model: '',
            nickname:'',
            loading: false,
            modalMake: false,
            modalYear: false,
            listMake :''
        }
    }
    
    componentDidMount(){
        this.runSetState()
        this.getListMake()
    }

    runSetState(){
        this.setState({
            make: this.props.navigation.state.params.make,
            year: this.props.navigation.state.params.year,
            model: this.props.navigation.state.params.model,
            nickname: this.props.navigation.state.params.nickname 
        })
    }

    getListMake(){
        let that = this
        let VEHICLE_KEY = this.props.navigation.state.params.type_id
        
        let CAR_MAKE = []

        firebase.database().ref(`vehicle_make/${VEHICLE_KEY}`).once('value').then((snapshot) => {
            if(snapshot.exists()){
                KEY = Object.keys(snapshot.val());
        
                KEY.forEach( (key_id) => {
                    let a = snapshot.val()[key_id].make;          
                    CAR_MAKE.push(a);
                  })

        
                let SORTED_MAKE = CAR_MAKE.sort()
                that.setState({listMake:SORTED_MAKE, });
            }
        })
    }

    update = () => {
        let MAKE = this.state.make
        let MODEL =  this.state.model
        let YEAR = this.state.year
        let NICKNAME = this.state.nickname
        let PLATE = this.props.mobx_carkey.PLATE

        if(MAKE === '' || MODEL === '' || YEAR === '' ){
            Alert.alert('Sorry', 'Please fill in vehicle details')
            return
        }

        this.setState({loading:true})

        let DATA = {
            make: MAKE,
            model: MODEL,
            year: YEAR,
            nickname: NICKNAME
        }
    

        firebase.database().ref(`plate_number/${PLATE}`).update(DATA)

        this.props.navigation.state.params.onNvaigateBack()
        this.props.navigation.goBack(null)
    }


    // ====== 

    render(){

        let MAKE = this.state.listMake
        let content_make = [];
        
        if(MAKE !== '' && MAKE !== undefined){
            for(var i = 0; i < MAKE.length; i++){
                let value = MAKE[i];
                content_make.push(
                  <View >
                    <TouchableOpacity
                      style={(i === MAKE.length - 1) ? styles.noBorderText : styles.borderText}
                      onPress={()=> this.setState({make:value,modalMake:false})}>
                      <Text style={{fontSize:16}}>{value}</Text>
                    </TouchableOpacity>
                  </View>
                )
            }
        }

        let YEAR = new Date().getFullYear();
        let BEFORE = 'before ' + (YEAR - 26)
        let CURRENT_YEAR = [
            YEAR, YEAR - 1, YEAR - 2, YEAR - 3, YEAR - 4, YEAR - 5, YEAR-6,
            YEAR - 7, YEAR - 8, YEAR - 9, YEAR - 10, YEAR - 11, YEAR - 12,
            YEAR - 13, YEAR - 14, YEAR - 16, YEAR - 17, YEAR - 18, YEAR - 19,
            YEAR - 20, YEAR - 21, YEAR - 22, YEAR - 23, YEAR - 24, YEAR - 25,
            BEFORE
        ]

        let content_year = []
        for(var i = 0; i < CURRENT_YEAR.length; i++){
            let value = CURRENT_YEAR[i];
      
            content_year.push(
              <View >
                <TouchableOpacity
                  style={(i === CURRENT_YEAR.length - 1) ? styles.noBorderText : styles.borderText}
                  onPress={()=> this.setState({year:value,modalYear:false})}>
                  <Text style={{fontSize:16}}>{value}</Text>
                </TouchableOpacity>
              </View>
            )
        }


        let submitButton;
        if(this.state.loading === true){
            submitButton = 
            <TouchableWithoutFeedback style={{marginTop:20, marginBottom:15, alignSelf:'center',}}>
                <Button title='Updating ...' bgColor='#080880' width={WIDTH} />
            </TouchableWithoutFeedback>

        } else {
            submitButton = 
            <TouchableOpacity style={{marginTop:20, marginBottom:15, alignSelf:'center',}}  >
                <Button onPress={this.update} title='Update' bgColor='#080880'  width={WIDTH} />
            </TouchableOpacity>

        }


        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Edit Details' onPress={() => this.props.navigation.goBack()} />
                    
                <KeyboardAvoidingView behavior={(Platform.OS === 'ios') ? "padding" : null} style={{flex:1}}  enabled>
                    <ScrollView>
                        <View style={styles.box}>
                            <PickerTextInput 
                                title= 'MAKE'
                                value={this.state.make} 
                                onPress={() => this.setState({modalMake:true})} 
                            /> 

                            <FormTextInput 
                                title = 'MODEL'
                                placeholder = 'eg: Axia, Wira'
                                value={this.state.model} 
                                onChangeText={(x) => this.setState({model:x})}
                            /> 

                            <PickerTextInput 
                                title= 'YEAR'
                                value={this.state.year} 
                                onPress={() => this.setState({modalYear:true})} 
                            /> 

                            <FormTextInput 
                                value={this.state.nickname} 
                                placeholder='Nickname for your vehicle'  
                                title='NICKNAME'
                                onChangeText={(x) => this.setState({nickname:x})}  />

                        </View>

                        {submitButton}

                        
                    </ScrollView>


                </KeyboardAvoidingView>


                {/* MODEL */}
                <MiddleModal modalName={this.state.modalYear} height="94%"
                children={
                    <View style={{flex:1}} >
                        <Text style={{fontSize:20,marginBottom:15, fontFamily: "ProximaNova-Bold"}}>Select Vehicle Year</Text>
                        <ScrollView showsVerticalScrollIndicator={false} style={{ width:'100%',}}>
                            {content_year}
                        </ScrollView>

                         <TouchableOpacity style={styles.button} onPress={()=> this.setState({modalYear:false})}>
                            <Text style={styles.text} >Close</Text>
                        </TouchableOpacity>

                    </View>
                } />

                <MiddleModal modalName={this.state.modalMake} height="94%"           
                children={
                    <View style={{flex:1}} > 
                        <Text style={{fontSize:20,marginBottom:15, fontFamily: "ProximaNova-Bold"}}>Select Vehicle Make</Text>
                            <ScrollView showsVerticalScrollIndicator={false}>
                            {content_make}
                            </ScrollView>

                        <TouchableOpacity style={styles.button} onPress={()=> this.setState({modalMake:false})}>
                            <Text style={styles.text} >Close</Text>
                        </TouchableOpacity>

                    </View>
                } />


                    
                </View>
            </SafeAreaView>
        )
    }
}

VehicleEdit = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(VehicleEdit))
export default VehicleEdit

const styles = StyleSheet.create({
    box:{ 
        backgroundColor:'#fff', paddingLeft:15, paddingRight:15, paddingTop:25, borderRadius:10, margin:20, 
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowRadius: 3,
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.3,
            },
            android: {
                elevation: 3
            },
        }),
    },
    button :{
        marginTop:15,
        borderRadius:10,
        padding:15,
        backgroundColor:'#080880',
        width: Dimensions.get('window').width - 70,
    },
    text :{
        fontFamily:'ProximaNova-Bold', 
        fontSize:18, 
        color:'white', 
        textAlign:'center'
    },
    borderText: {padding:12, borderBottomColor:'#ccc', borderBottomWidth:0.5},
    noBorderText: {padding:12}

})