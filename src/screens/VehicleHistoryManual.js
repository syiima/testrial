import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Dimensions,TextInput,Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/MaterialCommunityIcons"
import FontIcon from 'react-native-vector-icons/FontAwesome'
import Moment from 'moment';
import DatePicker from 'react-native-datepicker';

import Header from '../components/Header.js'
import emptyPage from '../assets/icons/emptyBooking.png'
import MiddleModal from "../components/MiddleModal"
import FormTextInput from "../components/FormTextInput.js"
import PickerTextInput from "../components/PickerTextInput.js"

const months = {
    'January' : 'Jan',
    'February' : 'Feb',
    'March' : 'Mar',
    'April' : 'Apr',
    'May' : 'May',
    'June' : 'June',
    'July' : 'July',
    'August' : 'Aug',
    'September' : 'Sept',
    'October' : 'Oct',
    'November' : 'Nov',
    'December' : 'Dec'
    }

class VehicleHistoryManual extends React.Component{
    constructor(){
        super()
        this.state = {
            history: [],
            price: '',
            description:'',
            mileage:'',
            // date: [],
            date:new Date(),
            modalHistory: false,
            currentview:1,
            day: '',
            month: '',
            year: '',
            workshop: '',
            open : false,
        }
        this.dateTextInput = [];
    }
    
    componentDidMount(){
        this.fetchHistory()
    }
    

    fetchHistory = () => {
        let PLATE = this.props.mobx_carkey.PLATE

        firebase.database().ref(`plate_number/${PLATE}/book_history`).once('value', (snp) => {
            if(snp.exists()){
                KEY = Object.keys(snp.val())
                let sorted = {}
                let DATA = []
                KEY.forEach((key) => {
              
                    let a = snp.val()[key]
                    a.bookID = key
            
                    if(a.assign !== undefined){
                        if(a.assign.status !== 'deleted' && a.quotation !== undefined){  
                            DATA.push(a)
                        } 
                    } else {
                        DATA.push(a)
                    }

                    


                })

                if(DATA === []){
                    this.setState({history: [], currentview:1})
                } else {
                    let SORTED =  DATA.sort((a,b) => b.timestamp - a.timestamp)
                    this.setState({history: SORTED, currentview:2})
                }
                  
            } else {
                this.setState({history: [], currentview:1})
            }
        })
    }


    saveHistory = () => {
        let PLATE = this.props.mobx_carkey.PLATE
        let PRICE = this.state.price
        let DESC = this.state.description
        // let DAY = this.state.day
        // let YEAR = this.state.year
        // let MONTH = this.state.month
        let MILEAGE = this.state.mileage
        let WORKSHOP = this.state.workshop
        let DATE = this.state.date

        // let START = MONTH + '/' + DAY  + '/' + YEAR + ' ' + '12:00'
        // let DATE = Number(new Date(START))
        let MM = new Date(DATE).getTime()
        // MM = Date.parse(MM)
        // console.warn('gg', MM);

        let DATA = {
            price: PRICE,
            mileage: MILEAGE,
            description:  DESC,
            date: MM,
            type: 'self input',
            workshop : WORKSHOP,
            timestamp: Date.now()
        }

        firebase.database().ref(`plate_number/${PLATE}/book_history`)
        .push(DATA)
        .then(() => {
            this.cancel()
            this.fetchHistory()
        })
    }

    cancel = () => {
        this.setState({modalHistory: false,workshop:'', date: '', price:'', description:'', mileage:'' })
    }

    //date
    renderBoxOTP = () => {
        let txt =
        <View style={{flexDirection:'row', alignItems:'center'}}>
            <View style={styles.dateBox}>
                <TextInput
                    maxLength = {2}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    keyboardType="numeric"
                    style={{fontFamily:'ProximaNova-Regular', padding:0,}}
                    placeholder='DD'
                    // onChangeText={v => this.focusNext(j, v)}
                    onChangeText={(x) => this.setState({day:x})}
                    // onKeyPress={e => this.focusPrevious(e.nativeEvent.key, j)}
                    // ref={ref =>  this.dateTextInput[j] = ref }
                    onSubmitEditing={() => this.month.focus()}
                />
            </View>
            <Text style={styles.divider}> / </Text>
            <View style={styles.dateBox}>
                <TextInput
                    maxLength = {2}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    keyboardType="numeric"
                    style={{fontFamily:'ProximaNova-Regular', padding:0,}}
                    placeholder='MM'
                    onChangeText={(x) => this.setState({month:x})}
                    inputRef={ref => this.month = ref}
                    onSubmitEditing={() => this.year.focus()}

                />
            </View>
            <Text style={styles.divider}> / </Text>
            <View style={styles.dateBox}>
                <TextInput
                    maxLength = {4}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    keyboardType="numeric"
                    style={{fontFamily:'ProximaNova-Regular', padding:0,}}
                    placeholder='YYYY'
                    onChangeText={(x) => this.setState({year:x})}
                    inputRef={ref => this.year = ref}
                    // onChangeText={v => this.focusNext(j, v)}
                    // onKeyPress={e => this.focusPrevious(e.nativeEvent.key, j)}
                    // ref={ref =>  this.dateTextInput[j] = ref }
                />
            </View>

                
            </View>
        
        
        return txt;
    }

    focusPrevious(key, index) {
        if (key === 'Backspace' && index !== 0){
            this.dateTextInput[index - 1].focus();
        }
            
    }

    focusNext(index, value) {
        // console.warn('hmm', index, value, this.otpTextInput.length);
        if (index < this.dateTextInput.length - 1 && value) {
            // this.otpTextInput[index + 1].refs[value].focus();
            this.dateTextInput[index + 1].focus()
            
        }
        if (index === this.dateTextInput.length - 1) {
           
            this.dateTextInput[index].blur();
            
        }
        const date = this.state.date;
        date[index] = value;
        this.setState({ date });
        console.warn('index', index);
        if(index === 5){
            let N = this.state.date.join('')        
        }
        
        // this.props.getOtp(otp.join(''));
    }


    renderHistory = () => {
        let display
        let DATA = this.state.history

        if(DATA.length !== 0){
            display = DATA.map(item => {
                return item.type === 'self input' ?
                <TouchableWithoutFeedback onPress={() => this.editDets(item)} >
                    <View style={styles.box}>
                        <View style={{flexDirection: 'row',justifyContent:'space-between', alignItems:'center' }}>
                            <View style={{flex:3, }}>
                                <Text style={{fontFamily: "ProximaNova-Bold", fontSize:15}}>Manual Input</Text>
                                <Text style={{ fontSize:12, marginTop:3}}>{item.workshop !== '' ? item.workshop : '--'}</Text>
                                <Text style={{ fontSize:11, color:'#080880', fontFamily:'ProximaNova-Bold', marginTop:3}}>{Moment(item.date).format('DD MMM YYYY')}, {Moment(item.date).format('h:mm A')}</Text>
                            
                            </View>
                        <View style={{flex:1.5, alignItems:'center', justifyContent:'center' }}>
                            <Text style={{fontFamily: "ProximaNova-Bold",  fontSize:11}}>Mileage</Text>
                            <Text style={{fontFamily: "ProximaNova-Bold", color:'#080880',marginTop:3, fontSize:16}}>{item.mileage !== undefined && item.mileage !== '' ? item.mileage : 'NA'}</Text>
                            <Text style={{fontFamily: "ProximaNova-Bold", marginTop:3, fontSize:11}}>{item.mileage !== undefined && item.mileage !== '' ? 'KM' : null}</Text>
                        </View>
                        </View>
                        <FontIcon name="angle-right" color='#080880' size={15} style={{position:'absolute', right:15, bottom:15,}} />

                    </View>
                   
                </TouchableWithoutFeedback>
                : 
                <TouchableWithoutFeedback onPress={() => this.openDets(item)} >
                    <View style={styles.box}>
                        <View style={{flexDirection: 'row',justifyContent:'space-between', alignItems:'center' }}>
                            <View style={{flex:3, }}>
                                <Text style={{fontFamily: "ProximaNova-Bold", fontSize:15}}>Workshop Drive In</Text>
                                <Text style={{ fontSize:12, marginTop:3}}>{item.customer_nameplace}</Text>
                                <Text style={{ fontSize:11, color:'#080880', fontFamily:'ProximaNova-Bold', marginTop:3}}>{item.customer_day} {months[item.customer_month]} {item.customer_year} , {item.customer_time} </Text>
                            
                            </View>
                        <View style={{flex:1.5, alignItems:'center', justifyContent:'center' }}>
                            <Text style={{fontFamily: "ProximaNova-Bold",  fontSize:11}}>Mileage</Text>
                            <Text style={{fontFamily: "ProximaNova-Bold",marginTop:3, color:'#080880', fontSize:16}}>{item.checklist.customer_car_mileage !== undefined && item.checklist.customer_car_mileage !== '' ? item.checklist.customer_car_mileage : 'NA'}</Text>
                            <Text style={{fontFamily: "ProximaNova-Bold",marginTop:3,  fontSize:11}}>{item.checklist.customer_car_mileage !== undefined && item.checklist.customer_car_mileage !== '' ? 'KM' : null}</Text>
                        </View>
                        </View>
                        <FontIcon name="angle-right" color='#080880' size={15} style={{position:'absolute', right:15, bottom:15,}} />

                    </View>

                    
                </TouchableWithoutFeedback>

                
        
            })
        }

        return display 
    }

    openDets = (x) => {
        if(x.type !== undefined && x.type === 'walkin'){
            firebase.analytics().logScreenView({
                screen_name: 'History_Walkin',
                screen_class: 'CarProfile',
              })
            this.props.navigation.navigate('TrackBookingHistoryDetails', {
                item:x, 
                quotation: x.quotation.masterItems, 
            })
        } 
    }

    editDets = (x) => {
        this.props.navigation.navigate('VehicleHistoryManualEdit', {item: x, onNavigateBack : this.fetchHistory})
    }



    render(){
        let content 
        if(this.state.currentview === 1){
            content = 
            <View style={{flex:1, alignItems:'center', justifyContent:'center', margin:20, marginTop:0}}>
                <Image source={emptyPage} style={{height: 200, width:200, resizeMode:'contain'}} />
                <Text style={{fontSize:22, fontFamily: "ProximaNova-Bold", marginTop:15, marginBottom:10}}>No Records</Text>
                <Text>All your history will be displayed here</Text>
              </View>
        } else {
            if(this.state.history !== '' && this.state.history.length !== 0){
                content = 
                <ScrollView >
                    {this.renderHistory()}

                    <View style={{height:30}} />
                </ScrollView>
            } else {
                content = 
                <View style={{flex:1, alignItems:'center', justifyContent:'center', margin:20, marginTop:0}}>
                    <Image source={emptyPage} style={{height: 200, width:200, resizeMode:'contain'}} />
                    <Text style={{fontSize:22, fontFamily: "ProximaNova-Bold", marginTop:15, marginBottom:10}}>No Records</Text>
                    <Text>All your history will be displayed here</Text>
                </View>
            }

            

        }

        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#000080'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Vehicle History' onPress={() => this.props.navigation.goBack()} />
                    
                    {content}

                    <TouchableWithoutFeedback onPress={() => this.setState({modalHistory: true})} >
                        <View style={styles.bottomButton}>
                            <Icon name='plus-thick' size={28} color='#fff' />
                        </View>
                    </TouchableWithoutFeedback>

                    <MiddleModal modalName={this.state.modalHistory}         
                        children={
                            <View>
                                <Text style={{ color:'black',textAlign:'center', fontFamily: "ProximaNova-Bold", fontSize:18, marginTop:5, marginBottom:20}}>Add Service History</Text>
                                
                                <FormTextInput 
                                    title = 'MILEAGE (KM) '
                                    placeholder='eg: 200000'
                                    value={this.state.mileage} 
                                    onChangeText={(x) => this.setState({mileage:x})}
                                    keyboardType="numeric"
                                    onSubmitEditing={() => this.workshop.focus()}
                                /> 
                                <FormTextInput 
                                    title = 'WORKSHOP / LOCATION'
                                    placeholder='eg: Workshop ABC, Sg Buloh'
                                    value={this.state.workshop} 
                                    onChangeText={(x) => this.setState({workshop:x})}
                                    onSubmitEditing={() => this.description.focus()}
                                /> 
                                <FormTextInput 
                                    title = 'DESCRIPTION'
                                    placeholder='eg: tukar brake pad'
                                    multiline = {true}
                                    value={this.state.description} 
                                    onChangeText={(x) => this.setState({description:x})}
                                    inputRef={ref => this.description = ref}
                                    onSubmitEditing={() => this.price.focus()}
                                /> 
                                <FormTextInput 
                                    title = 'AMOUNT (RM)'
                                    placeholder='eg: 200, 500, 124'
                                    value={this.state.price} 
                                    keyboardType="numeric"
                                    onChangeText={(x) => this.setState({price:x})}
                                    inputRef={ref => this.price = ref}
                                /> 
                                {/* <View style={{marginBottom:20}} >
                                    <Text style={{fontWeight:'bold', fontSize:13,marginBottom:10, color:'#686868' }}>DATE</Text>
                                    <View style={styles.dateBoxesContainer}>
                                        {this.renderBoxOTP()}
                                    </View>

                                </View> */}

                                <View style={{}}>
                                    <Text style={{fontWeight:'bold', fontSize:13,marginBottom:10, color:'#6e6d6d' }}>DATE</Text>
                                    <DatePicker
                                        date={this.state.date}
                                        mode="date"
                                        placeholderText="Select date"
                                        format="DD-MMMM-YYYY"
                                        minDate="01-01-2017"
                                        maxDate="31-12-2050"
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        showIcon={false}
                                       
                                        customStyles={{
                                            datePicker: {
                                                justifyContent:'center'
                                              },
                                            dateInput: {
                                                borderColor:'transparent',
                                                padding:0,
                                                width:'100%'
                                            },
                                            dateTouchBody:{
                                                borderWidth:1, borderColor:'#ccc',borderRadius:5,
                                                width: Dimensions.get('window').width - 80,
                                                justifyContent:'flex-start',
                                                alignItems:'flex-start'
                                                // padding:10,
                                            }
                                        }}
                                        onDateChange={(date) => this.setState({date: date})}
                                        />
                                    </View>
                               

                        
                              
                                <TouchableOpacity onPress={this.saveHistory}  style={{backgroundColor:'#080880', borderRadius:6, padding:17, marginTop:45, paddingLeft:20, paddingRight:20,alignItems:'center'}}>
                                    <Text style={{color:'white', fontFamily: "ProximaNova-Bold"}}>Save</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={this.cancel} style={{ padding:20, marginTop:5, paddingLeft:20, paddingRight:20, alignItems:'center'}}>
                                    <Text style={{color:'#080880', fontFamily: "ProximaNova-Bold"}}>Cancel</Text>
                                </TouchableOpacity>
                            </View>
                        } />
                    
                </View>
            </SafeAreaView>
        )
    }
}

VehicleHistoryManual = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(VehicleHistoryManual))
export default VehicleHistoryManual

const styles = StyleSheet.create({
    box :{
        backgroundColor:'#fff',
        borderRadius:10,
        margin:15,
        marginBottom:0,
        padding:15,
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 3 },
              shadowOpacity: 0.3,
            },
            android: {
              elevation: 3
            },
          }),
    },
    bottomButton : {
        width:48,
        height:48,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#080880',
        borderRadius: 24,
        position:'absolute',
        bottom : 20,
        right:20
    },
    dateBoxesContainer: {
        flexDirection: 'row',
    },

    dateBox: {padding:(Platform.OS === 'ios') ? 6 : 5, borderWidth:1, borderColor:'#a7a7a7',borderRadius:5,},
    divider:{fontFamily:'ProximaNova-Regular',fontSize:22, marginLeft:3, marginRight:3},
    historyBoard: {
        padding:10,
        // marginBottom:15,
        borderBottomWidth:1,
        borderBottomColor:'#ccc',
        backgroundColor:'#f5f5f5',
        borderRadius:10,
      },
  
})