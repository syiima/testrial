import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,Image,ActivityIndicator, PermissionsAndroid, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'

import MaterialIcon from "react-native-vector-icons/MaterialIcons";
import Icon from 'react-native-vector-icons/FontAwesome'
import IonIcons from 'react-native-vector-icons/Ionicons'
import AntIcon from 'react-native-vector-icons/AntDesign'

import DatePicker from 'react-native-datepicker';
import DocumentPicker from 'react-native-document-picker';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import Moment from "moment";

import Header from '../components/Header.js'
import MiddleModal from '../components/MiddleModal'
import FormTextInput from '../components/FormTextInput'
import BottomModal from "../components/BottomModal.js"

import pdf from '../assets/icons/pdf.png'
import camera from '../assets/icons/camera.png'
import library from '../assets/icons/library.png'

class VehicleInsurance extends React.Component{
    constructor(){
        super()
        this.state = {
            data: '',
            modalPolicy: false,
            modalOption: false,
            loading: false,
            expiryDate: new Date(),
            company: '',
            file: '',
            fileName: '',
            type: '',
            key_db:'',
            progressVal :0,
            percentage: '',
            show: true,
            type: '',

        }
    }
    
    componentDidMount(){
        this.fetchInsurance()
    }

    componentWillUnmount(){
        let PLATE = this.props.mobx_carkey.PLATE
        firebase.database().ref(`plate_number/${PLATE}/insurance`).off()
    }

    fetchInsurance = () => {
        let that = this
        let PLATE = this.props.mobx_carkey.PLATE
       
        firebase.database().ref(`plate_number/${PLATE}/insurance`).on('value', (snp) => {
            let DATA = []
            if(snp.exists()){
                let x = snp.val()
                KEY = Object.keys(snp.val());
                KEY.forEach( (key_id) => {
                  let a = snp.val()[key_id];
                  a.key = key_id;  
                  DATA.push(a)
                    
                })

                that.setState({data: DATA, loading:false,})
            } else {
                that.setState({data: '',loading:false})
            }
        }) 
    }
    
    displayData = () => {
        let display 
        let DATA = this.state.data

        if(DATA !== ''){
            display = DATA.map(item => (
                <View style={styles.box} key={item}>
                    <Text style={{fontFamily: "ProximaNova-Bold",marginBottom:15, fontSize:15}}>{item.company_name}</Text>
                    <Text>File type : {item.type} </Text>
                    <Text>Expired on {item.expiry_date} </Text>
                    <View>
                    <View style={{flexDirection:'row', justifyContent:'space-between',marginTop:15,}}>
                    <TouchableOpacity style={styles.leftButton} onPress={() => this.remove(item.key)} >
                        <Text style={{color:'#080880',fontSize:13, fontFamily: "ProximaNova-Bold",}}>Remove</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.rightButton} onPress={() => this.viewFile(item)} >
                        <Text style={{color:'#fff',  fontSize:13, fontFamily: "ProximaNova-Bold",}}>View</Text>
                    </TouchableOpacity>
                    </View>
                    </View>
                </View>
            ))
        }
        return display 
    }

    //=== SELECT FILE ===== 
    openCamera = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                    title: "App Camera Permission",
                    message:"App needs access to your camera to snap photos",
                }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                const options = {quality:1,maxWidth: 800, maxHeight: 800  }
                launchCamera(options, (response) => {
                    this.setState({modalOption: false})
                        
                    if (response.didCancel) {
                        this.setState({modalOption: false})
                    } if (response.error) {
                        Alert.alert('Sorry', "Please try again");
                        this.setState({modalOption: false})
                    } 
                    if(!response.didCancel && !response.error) {
                        const source = response.assets[0].uri;
                        this.setState({
                        file: source,
                        modalOption: false,
                        type:'image'
                        });
                    
                    } 
                });
            }else {
                console.log('Camera permission denied ')
            } 
        } catch (error) {
            console.warn(error)
        }

    }
      
    openLibrary = () => {
        const options = {quality:1,maxWidth: 800, maxHeight: 800 }
        launchImageLibrary(options, (response) => {
            this.setState({modalOption: false})
                if(response.didCancel){
                    this.setState({modalOption: false})
                } 
                if (response.error) {
                    Alert.alert('Sorry', "Please try again");
                    this.setState({modalOption: false})
                } 
                if(!response.didCancel && !response.error) {
                    const source = response.assets[0].uri;
                    this.setState({
                        file: source,
                        modalOption: false,
                        type:'image'
                    });
                }
         });
    
    }
  
    pickPDF = async() =>  {
        let that = this
        
        try {
          const res = await DocumentPicker.pick({
            type:[DocumentPicker.types.pdf]
          })

          console.warn('Type : ' + res.type);
          that.setState({ file: res.uri, fileName: res.name, type: 'pdf', modalOption: false });
        } catch (err) {
          //Handling any exception (If any)
          if (DocumentPicker.isCancel(err)) {
            // Alert.alert('You canceled the picker');
            //If user canceled the document selection
            // alert('Canceled from single doc picker');
          } else {
            Alert.alert('Sorry', 'An error occured. Please contact SERV Team if the issue still persists');
          }
        }
    }


    // ===== FUNC ===== 
    save = () => {
        let that = this
        let PLATE = this.props.mobx_carkey.PLATE
        let FUID = this.props.mobx_auth.FUID 
        let NAME = this.props.mobx_auth.NAME
        let COMP = this.state.company
        let DATE = Moment(this.state.expiryDate).format('DD MMM YYYY')
        let FILE = this.state.file
        let TYPE = this.state.type
        let KEY 

        // console.warn('hellooo', DATE, FILE, TYPE, COMP);

        if(COMP === '' || DATE === ''){
            this.setState({loading:false, })
            Alert.alert('Sorry', 'Please fill in all the details')
            return 
        }
        if(FILE === '' ){
          this.setState({loading:false, })
          Alert.alert('Sorry', 'Please upload a file or image of your insurance policy')
          return 
        }
        if(DATE === 'Invalid date'){
            DATE = Moment().format('DD MMM YYYY')
        } else { DATE = DATE}
        let DATA = {
            company_name : COMP,
            expiry_date : DATE,
            timestamp_added :firebase.database.ServerValue.TIMESTAMP,
            type: TYPE,
            uploaded_by: NAME
        }

        //1.getKey
        KEY = firebase.database().ref(`plate_number/${PLATE}/insurance/`).push().key;

        if(DATE !== '' && COMP !== ''){
            this.setState({loading: true})     
            const refFile= firebase.storage().ref().child(`insurance/${FUID}/${PLATE}/${KEY}`);
            const URLVehicInsu = Platform.OS === 'ios' ? FILE.replace('file://', '') : FILE;
            let uploadTask =  refFile.putFile(URLVehicInsu);


            uploadTask.on('state_changed', function(snapshot) {                
                //UPLOADING IMAGE
                let progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                let yawn  = progress.toFixed(2)
                that.setState({progressVal: yawn, percentage: progress.toString()})
            //   console.warn('Upload is ' + progress + '% done');
                switch (snapshot.state) {
                case 'running':
                    break;
                case 'success':
                    snapshot.ref.getDownloadURL()
                    .then((downloadURL) => {
                    // console.warn('ehh', downloadURL);
                    firebase.database().ref(`plate_number/${PLATE}/insurance/${KEY}`).update({file: downloadURL});
                    }).then(() => {
                        firebase.database().ref(`plate_number/${PLATE}/insurance/${KEY}`).update(DATA)
                    }).then(() => {
                        that.setState({modalPolicy:false, expiryDate:'', company: '',type :'', file: '', fileName: '', loading:false, progressVal:0, percentage:''})
                        that.cancel()
                        that.fetchInsurance()
                    }).then(() => {
                        setTimeout(() => Alert.alert('Success', 'File uploaded.'),500)
                    })
                    break;
                default:
                    break;
                }
            });
        }
    }

    cancel = () => {
        this.setState({modalPolicy: false, expiryDate:'', company: '', file: '', type: '', fileName: '', loading:false,})
    }

    remove = (X) => {
        let KEY = X
        let PLATE = this.props.mobx_carkey.PLATE
    
        firebase.database().ref(`plate_number/${PLATE}/insurance/${KEY}/`).remove()
        this.fetchInsurance()
    }

    uploadingPercentage = () => {
        let yaj
        let PERF = this.state.percentage + '%'
        if(this.state.percentage !== ''){
          yaj = 
            <View style={{height:6, marginTop:10, backgroundColor:'#ccc', borderRadius:4}}>
              <View style={{position:'absolute', backgroundColor: "#fb9900", width:PERF, height:6, borderRadius:4}} />
            </View>
        }
  
        return yaj
    }

    viewFile = (item) => {
        let LINK = item.file
        let TYPE = item.type
        console.warn('ehh', TYPE);
        
        if(TYPE === 'pdf' || TYPE === undefined ){
            this.props.navigation.navigate('PDFWebview', {link: LINK})
        } else {
            this.props.navigation.navigate('Webview', {link: LINK})
        }
    }
  

    render(){

        let submitButton;
        if(this.state.loading === false){
            submitButton =
                <TouchableOpacity onPress={this.save} style={{...styles.saveButton, borderColor:'#080880',backgroundColor:'#080880', }}>
                    <Text style={{color:'#fff',fontSize:18, fontFamily: "ProximaNova-Bold"}}>Save</Text>
                </TouchableOpacity>
        } else {
            submitButton =
                <View style={{...styles.saveButton,borderColor:'#080880',backgroundColor:'#080880',  }}>
                    <ActivityIndicator style={{ alignSelf:'center'}} size="small" color="#fff" />
                </View>
        }

        let IMG
        if(this.state.type === 'pdf' ){
            IMG = 
            <View style={{marginBottom:10,marginLeft:10, marginTop:15, flexDirection:'row', alignItems:'center', justifyContent:'space-between'}}>
                <View style={{flexDirection:'row', alignItems:'center'}}>
                  <IonIcons name='document-text-outline' size={25} />
                  <Text style={{marginLeft:15, fontSize:15}}>{this.state.fileName}</Text>
                </View>
                <IonIcons name='trash-outline' size={25} onPress={() => this.setState({file:'', fileName:'', type:''})} />
            </View>
        }
        if(this.state.type === 'image'){
            IMG = 
            <View style={{marginBottom:10, marginTop:20, alignItems:'center', justifyContent:'center', alignSelf:'center'}}>
                <Image
                source={{uri:this.state.file}}
                style={{height:100, width:100, alignItems:'center',}}
                />
                <AntIcon name='closecircle' size={20} style={{position:'absolute', right:0, top:-6}} onPress={() => this.setState({file:'', fileName:'', type:''})} color='#e85b1a' />
            </View>
        }

        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
            <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Insurance' onPress={() => this.props.navigation.goBack()} />
                    
                    <ScrollView contentContainerStyle={{padding:20}} showsVerticalScrollIndicator={false} >
                        
                        <View style={styles.box}>
                            <Text style={{fontFamily: "ProximaNova-Bold",textAlign:'center', fontSize:15}}>Insurance Policy</Text>
                            {/* <View style={styles.img} /> */}
                            <TouchableOpacity style={styles.button} onPress={() => this.setState({modalPolicy: true}) } >
                                <MaterialIcon name= 'photo-camera'  size={18} color='#fff'/>
                                <Text style={{color:'#fff', marginLeft:15, fontSize:13, fontFamily: "ProximaNova-Bold",}}>Upload Document</Text>
                            </TouchableOpacity>
                        </View>


                        {this.displayData()}
                        
                        <View style={{height:25}} />

                    </ScrollView>
                    <MiddleModal
                        modalName={this.state.modalPolicy}
                        children={
                            <View>
                                <Text style={{fontFamily: "ProximaNova-Bold",textAlign:'center', marginBottom:20, fontSize:15}}>Insurance Policy</Text>
                                <FormTextInput 
                                    title = 'COMPANY NAME'
                                    placeholder = 'eg: ZURICH'
                                    value={this.state.company} 
                                    onChangeText={(x) => this.setState({company:x})}
                                />
                                <View style={{marginBottom:20}}>
                                    <Text style={{fontWeight:'bold', fontSize:13,marginBottom:10, color:'#6e6d6d' }}>EXPIRY DATE</Text>
                                    <DatePicker
                                        date={this.state.expiryDate}
                                        mode="date"
                                        placeholderText="Select date"
                                        format="DD-MMMM-YYYY"
                                        minDate="01-01-2019"
                                        maxDate="31-12-2050"
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        showIcon={false}
                                        customStyles={{
                                            dateInput: {
                                                borderColor:'transparent',
                                                padding:0,
                                                width:'100%'
                                            },
                                            dateTouchBody:{
                                                borderWidth:1, borderColor:'#ccc',borderRadius:5,
                                                width: Dimensions.get('window').width - 80,
                                                justifyContent:'flex-start'
                                                // padding:10,
                                            }
                                        }}
                                        onDateChange={(expiryDate) => this.setState({expiryDate: expiryDate})}
                                        />
                                    <Text style={{color:'#686868', fontSize:12, marginTop:5}}>Please tap on the year/month to change year/month.</Text>

                                </View>
                               

                                <Text style={{fontWeight:'bold', fontSize:13,marginBottom:10,color:'#6e6d6d'}}>UPLOAD DOCUMENT</Text>
                                {this.state.file ? null :
                                    <View style={{flexDirection:'row', alignItems:'center', }}>
                                        <TouchableOpacity onPress={() => this.setState({modalOption:true})} style={styles.uploadButton}>
                                            <Icon name='upload' size={15} color='#fff' />
                                        </TouchableOpacity>
                                        <View style={{borderBottomColor:'#ccc', borderBottomWidth:1, paddingBottom:6, width:'80%'}}>
                                            <Text style={{fontSize:12}}>No file chosen</Text>
                        
                                        </View>
                                    
                                    </View>
                                }

                                {this.uploadingPercentage()}
                        
                                {IMG}


                                <View style={{marginTop:40, marginBottom:5,}}>
                                    {submitButton}
                                    <TouchableOpacity onPress={this.cancel} style={{...styles.saveButton, backgroundColor:'#fff', borderColor:'#080880', }}>
                                        <Text style={{color:'#080880', fontFamily: "ProximaNova-Bold"}}>Cancel</Text>
                                    </TouchableOpacity>
                                </View>

                                <BottomModal
                                    modalName={this.state.modalOption}
                                    onPress={() => this.setState({modalOption:false})}
                                    style={styles.innerModal}
                                    children = {
                                    <View >
                                        <View style={{padding:20, paddingBottom:0, alignItems:'center'}}>
                                            <Text style={{textAlign:'center', fontFamily:'ProximaNova-Bold', fontSize:20, marginBottom:5}}>Upload Option</Text>             
                                        </View>

                                        <View style={{margin:30, marginTop:20,  alignItems:'center', justifyContent:'space-around', flexDirection:'row'}}>
                                            <TouchableOpacity  onPress={this.openCamera} style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                                                <Image source={camera} style={{height:50, width:50}}  />
                                                <Text style={{textAlign:'center', }}>Camera</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity  onPress={this.openLibrary} style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                                                <Image source={library} style={{height:50, width:50}}  />
                                                <Text style={{textAlign:'center', }}>Library</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity  onPress={this.pickPDF} style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                                                <Image source={pdf} style={{height:50, width:50}} />
                                                <Text style={{textAlign:'center',}}>Pdf File</Text>
                                            </TouchableOpacity>

                                        </View>
                                    </View>
                                    }
                                />

                            </View>
                        }
                    />
                    

                    
                </View>
            </SafeAreaView>
        )
    }
}

VehicleInsurance = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(VehicleInsurance))
export default VehicleInsurance

const styles = StyleSheet.create({
    box :{
        backgroundColor:'#fff',
        borderRadius:10,
        marginBottom:15,
        padding:15,
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 3 },
              shadowOpacity: 0.3,
            },
            android: {
              elevation: 3
            },
          }),
    },
    img: {
        height:Dimensions.get('window').width /3,
        marginBottom:15
    },
    button:{
        borderRadius:30,
        width:Dimensions.get('window').width - 80,
        alignItems:'center',
        justifyContent:'center',
        marginTop:15,
        padding:12,
        backgroundColor:'#080880',
        flexDirection:'row',
    }, 
    leftButton:{
        borderRadius:10,
        width:'47%',
        alignItems:'center',
        justifyContent:'center',
        padding:12,
        borderColor:'#080880',
        borderWidth:1
      },
      rightButton:{
        borderRadius:10,
        width:'47%',
        alignItems:'center',
        justifyContent:'center',
        padding:12,
        borderColor:'#080880',
        borderWidth:1,
        backgroundColor:'#080880',
      },
    saveButton:{ borderWidth:0.5, borderRadius:10, padding:15, paddingLeft:20, paddingRight:20, marginBottom:10, alignItems:'center'},
    uploadButton: {backgroundColor:'#080880', alignItems:'center', justifyContent:'center',marginRight:15, padding:8, paddingLeft:14, paddingRight:14, borderRadius:5, 
        ...Platform.select({
        ios: {
            shadowColor: '#000',
            shadowRadius: 2,
            shadowOffset: { width: 0, height: 3},
            shadowOpacity: 0.3,
        },
        android: {
            elevation: 3
        },
        }),
    },
    innerModal: {     
        borderTopLeftRadius: 10,
        borderTopRightRadius:10,
        width:Dimensions.get('window').width,
        position:'absolute',
        bottom:0,
        backgroundColor: '#fff',
            ...Platform.select({
                ios: {
                    shadowColor: '#000',
                    shadowRadius: 2,
                    shadowOffset: { width: 0, height: 3 },
                    shadowOpacity: 0.6,
                },
                android: {
                    elevation: 2
                },
            }),
    },


})