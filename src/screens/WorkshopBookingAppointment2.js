import React from "react"
import {Platform, Alert, Modal,StyleSheet, Text,Keyboard, View,BackHandler, ImageBackground,SafeAreaView, TextInput, ScrollView,Image, Dimensions, TouchableOpacity, TouchableWithoutFeedback,KeyboardAvoidingView} from 'react-native';
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'

import Icon from "react-native-vector-icons/AntDesign"
import MIcon from 'react-native-vector-icons/MaterialIcons';

import Calendar from 'react-native-calendar-datepicker';
import Moment from 'moment';
import { NavigationActions, StackActions } from 'react-navigation';

import Header from '../components/Header.js'
import FormTextInput from "../components/FormTextInput.js"
import Button from "../components/Button.js";
import BorderButton from "../components/BorderButton.js"
import RoundedButton from "../components/RoundedButton.js";
import vehicles from '../assets/vehicles_placeholder.png'

const WIDTH = Dimensions.get('window').width - 70
const WIDTH2 = Dimensions.get('window').width - 30

type State = {
    date?: Moment,
  };

class WorkshopBookingAppointment extends React.Component{
    state: State

    constructor(){
        super()
        this.state = {
            ws_data: '',
            logo:'',
            reason_appt: [],
            view: 1,
            reason:'',
            remark: '',
            time_selected: '',
            loading :false,
            controlTime: new Date().getHours() > 16 ? 1 : 0,
            v_data:'',

            selectedVehicle: []

        }
    }
    
    componentDidMount(){
        this.fetchWorkshopDets()
        this.fetchVehicles()
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        this.backHandler.remove()
    }

    
    fetchWorkshopDets(){
        let that = this
        let REASON = []
        let WS_ID =   this.props.mobx_retail.R_ID
        let WSMAIN_ID =   this.props.mobx_retail.R_GROUPID

        firebase.database().ref(`retail/${WSMAIN_ID}/${WS_ID}`).once('value').then((snapshot) => {
            if(snapshot.exists()){
                let a = snapshot.val()
                let DATA = {
                    name: a.name,
                    address: a.address,
                    phone: a.phone
                }
           
                let APPT_REASON = a.appointment_reason 
                if(APPT_REASON !== undefined){
                    this.setState({reason_appt: APPT_REASON})
                }

                that.setState({ws_data: DATA, })
            }
        })

        //fetchLogo
        firebase.database().ref(`retail_main/${WSMAIN_ID}/logo`).once('value').then((snapshot) => {
            if(snapshot.exists()){
                let a = snapshot.val()
                that.setState({logo: a})
            }
        })
    }

    fetchVehicles(){
        let that = this
        let FUID = this.props.mobx_auth.FUID 
        
        firebase.database().ref(`users/${FUID}/vehicles`).once('value', (snapshot) => {
            let KEY = [];
            
            if(snapshot.exists()){
              KEY = Object.keys(snapshot.val())              
              let V_DATA = [];
              KEY.forEach((key_id) => {
                
                  let PLATE  = key_id 
      
                  firebase.database().ref(`plate_number/${PLATE}`).once('value', (snp) => {
                    if(snp.exists()){
                        let a = snp.val()
                        let DATA ={
                            plate : PLATE,
                            make: a.make,
                            model: a.model,
                            year: a.year,
                            cc: a.carcc,
                            transmission: a.transmission,
                            nickname: a.nickname !== undefined ? a.nickname : '',
                            image: a.picture_car 
                        }
                        
                        if(a.book_walkin === undefined){
                            V_DATA.push(DATA);
                        } else {
                            if(a.book_walkin._book_id === ''){
                                V_DATA.push(DATA);
                            } 
                        }
                        that.setState({v_data:V_DATA,});
                    }
                  })
                
              })
      
            } else {
                //hmmmmmmmm 
            }
          })
      
    }

    
    // ===== render time, reason ==== 

    renderTime = () => {
        let ARRAY = ['9:00 AM','10:00 AM', '11:00 AM', '12:00 PM', '1:00 PM', '2:00 PM', '3:00 PM', '4:00 PM', '5:00 PM', '6:00 PM']
        let display 
        display = ARRAY.map(s => (
            <TouchableOpacity 
                onPress={() => this.setState({ time_selected: s })}
                style={this.state.time_selected === s ? styles.yesPick : styles.noPick}>
                <Text style={[{color: '#000'},this.state.time_selected === s && styles.textyesPick]}>{s}</Text>
            </TouchableOpacity>
        ))

        return display 

    }

    renderReason = () => {
        let display 
        let DATA = this.state.reason_appt

        if(DATA.length !== 0){
           display =  DATA.map(item => (
                <TouchableWithoutFeedback onPress={() => this.setState({reason: item})} >
                    <View style={this.state.reason === item ? styles.picked : styles.nonpicked} >
                        <Text style={[{color: '#000', textAlign:'center'},this.state.reason === item && styles.textyesPick]} >{item}</Text>
                    </View>
                </TouchableWithoutFeedback>
            ))
        }
        
        return display 
    }


    selectVehicle = (plate, data )=> {
        let PREVSTATE = this.state.selectedVehicle;

        if(PREVSTATE.findIndex(data => data.plate === plate) === -1){
            PREVSTATE.push(data) //pick multiple
  
  
            this.setState({ selectedVehicle: PREVSTATE }, () => {
            })
            return;
        }
        if(PREVSTATE.findIndex(data => data.plate === plate) !== -1) {
            PREVSTATE.splice(this.state.selectedVehicle.findIndex(data => data.plate === plate), 1)
  
            this.setState({ selectedVehicle: PREVSTATE }, () => {
            })
            return;
        }

    }

    renderVehicle = () => {
        let display 
        let DATA = this.state.v_data
        
        if(DATA !== ''){
            display = DATA.map(item => (
                <TouchableWithoutFeedback onPress={() =>  this.selectVehicle(item.plate, item)}>
                    <View style={{alignItems:'center', justifyContent:'center', marginBottom:5}} >
                        <View style={styles.image2}>
                        { item.image !== '' && item.image !== undefined
                            ? <Image source={{uri: item.image.downloadURL}} style={{...styles.image, resizeMode:'cover',}}  />
                            : <Image source={vehicles} style={{...styles.image, resizeMode:'contain',}} />}
                        </View>
                        <Text style={{marginTop: 10, marginBottom:10}}>{item.plate} </Text>
                        {this.state.selectedVehicle.findIndex(x => x.plate === item.plate) !== -1 ? 
                        <MIcon name='check-box' size={25}  color='#080880' />
                        :
                        <MIcon name='check-box-outline-blank' size={25}  /> }
                    </View>
                </TouchableWithoutFeedback>
                
            ))
        }

        return display 
    }

    renderSelectedVehicle = () => {
        let display 
        let DATA = this.state.selectedVehicle 

        if(DATA.length !== 0){
            display = DATA.map(item => (
                <View style={{marginBottom:20}}>
                    <Text style={{fontFamily: "ProximaNova-Bold",}} >{item.plate} </Text>
                    <View style={{flexDirection:'row',alignItems:'center', marginTop:15}} >
                        
                        <View style={styles.image2}>
                        { item.image !== '' && item.image !== undefined
                            ? <Image source={{uri: item.image.downloadURL}} style={{...styles.image, resizeMode:'cover',}}  />
                            : <Image source={vehicles} style={{...styles.image, resizeMode:'contain',}} />}
                        </View>
                        <View style={styles.box}>
                            <View style={{paddingBottom:10, borderBottomWidth:0.5, borderBottomColor: '#ccc'}}>
                            <Text style={{textAlign:'center', fontSize:14}}>Appointment </Text>
                            </View>
                            <View style={{flexDirection:'row', marginTop:10, width: Dimensions.get('window').width /2 -15}}>
                                <View style={{flex:2, marginRight:10}}>
                                    <Text style={{textAlign:'center', marginBottom:10, fontSize:11}}>Date</Text>
                                    <Text style={{textAlign:'center'}}>{Moment(this.state.date).format("DD MMM YYYY")}</Text>
                                </View>
                                <View style={styles.mid} />
                                <View style={{flex:2, marginRight:10}}>
                                    <Text style={{textAlign:'center', marginBottom:10, fontSize:11}}>Time</Text>
                                    <Text style={{textAlign:'center'}}>{this.state.time_selected}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            ))
               
        }

        return display 
    }

    showSelected = () => {
        let display 
        let DATA = this.state.selectedVehicle 

        if(DATA.length !== 0){
            display = DATA.map(item => (
                <Text style={styles.desc}>{item.plate}</Text>
            ))
        }
        return display
    }

    showReason = () => {
        let disp, innn
        let REASON = this.state.reason

        if (typeof REASON === 'string'){
            disp = 
            <View>
                <Text style={styles.title}>REASON</Text>
                <Text style={styles.desc}>{REASON}</Text>
            </View>
        }
        if (typeof REASON === 'object'){
            innn = REASON.map((x, index) => (
                <Text style={styles.desc}>{index + 1}. {x} </Text>
            ))

            disp =    
            <View>
                <Text style={styles.title}>REASON</Text>
                {innn}
            </View>
        }
        return disp
    }


    //===== shown view ==== 
    firstview = () => {
        if(this.state.reason_appt.length === 0){
            if(this.state.remark === ''){
                Alert.alert('Sorry','Please state a reason or purpose for making a booking appointment')
            } else {
                this.setState({view:2})
            }
        }
        if(this.state.reason_appt.length !== 0){
            if(this.state.reason.length === 0 || this.state.reason === null){
                Alert.alert('Sorry','We noticed you have not selected any')
            } else {
                this.setState({view:2})
            }
        }
        
    }
    secondview = () => {
        if(this.state.time_selected === ''){
            Alert.alert('Sorry','Please select you preferred time')
            return
        } 
        if(this.state.date === undefined){
            Alert.alert('Sorry','Please select you preferred date')
            return
        } 
        if(this.state.time_selected !== '' && this.state.date !== undefined ) {
            this.setState({view:3})
        }
    }

    thirdview = () => {
        if(this.state.selectedVehicle.length === 0){
            Alert.alert('Select vehicle', 'Please select at least one vehicle')
            return
        } else {
            this.setState({view: 4})
        }
        
    }

    fourthview = () => {
        this.setState({view:5})
    }

    cancel = () => {
        this.setState({reason: '',remark:'', date: '', time_selected: ''})
        this.props.navigation.goBack(null)
    }

    //=== 
    sendTelegram = (x) => {
        let NAME  = this.props.mobx_auth.NAME;
        let PHONE  = this.props.mobx_auth.PHONE;
        let MERCHANT = this.props.mobx_retail.NAMEPLACE
        let REASON = this.state.reason !== '' ? this.state.reason : '-'
        let REMARK = this.state.remark !== '' ? this.state.remark : '-'
        let PLATE = x;
        let DATE = Moment(DATE).format('DD MMMM YYYY')

        if (this.props.mobx_config.config === 0) {
            TELEGRAM_URL = 'https://api.telegram.org/bot272822956:AAEduqgWA9gt6tpxYHKp2h1Opr4ZFNftyNc/sendMessage?chat_id=-1001396604012&text=';
        }
        if (this.props.mobx_config.config === 1) {
            TELEGRAM_URL = 'https://api.telegram.org/bot272822956:AAEduqgWA9gt6tpxYHKp2h1Opr4ZFNftyNc/sendMessage?chat_id=-1001476910252&text='
        }

        if(Platform.OS === 'ios'){
            TELE_TEXT = 'A new vehicle appointment was just made on the SERV User app.\n\nUser name : ' + NAME.toUpperCase() +  ' \nPhone no : ' + PHONE + ' \nUser plate : '  + PLATE    + ' \nMerchant Name : '  + MERCHANT  + ' \nDate Appointment : '  + DATE  + ' \nReason : '  + REASON  + ' \nRemark : '  + REMARK  
      
          } else {
            TELE_TEXT = 'A new vehicle appointment was just made on the SERV User app.%0A%0AUser name : ' + NAME.toUpperCase() + '%0APhone no : ' + PHONE +  '%0AUser plate : ' + PLATE +  '%0AMerchant Name : ' + MERCHANT +  '%0ADate Appointment : ' + DATE +  '%0AReason : ' + REASON +  '%0ARemark : ' + REMARK
      
          }
          let final = TELEGRAM_URL + TELE_TEXT

          fetch(final)
      
    }

    booking = () => {
        this.setState({loading:true})
        let DATE = this.state.date
        let REASON = this.state.reason
        let REMARK = this.state.remark

        //user info
        let FUID = this.props.mobx_auth.FUID;
        let NAME = this.props.mobx_auth.NAME;
        let EMAIL  =this.props.mobx_auth.EMAIL;
        let PHONE  = this.props.mobx_auth.PHONE;


        let NOTES = '';
        let ADDRESS = this.props.mobx_retail.ADDRESS
        let NAMEPLACE =  this.props.mobx_retail.NAMEPLACE
    
        //Data collected from the request form
        let BOOK_TIME = this.state.time_selected
        let BOOK_DAY = Moment(DATE).format('DD')
        let BOOK_MONTH = Moment(DATE).format('MMMM')
        let BOOK_YEAR = Moment(DATE).format('YYYY');

        //check user's app version and OS
        let APP_VERSION = this.props.mobx_config.Version;
        let APP_PLATFORM =  Platform.OS;


        let WS_ID =   this.props.mobx_retail.R_ID
        let WSMAIN_ID =   this.props.mobx_retail.R_GROUPID



        let VEHICLES = this.state.selectedVehicle
        
        let REQ_WALKIN

        for (let i = 0; i < VEHICLES.length; i++) {
            let ITEM = VEHICLES[i]
            let PLATE =  VEHICLES[i].plate

            //1.getKey booking req
            let KEY = firebase.database().ref(`appointment_walkin/${WSMAIN_ID}/${WS_ID}`).push().key;

            //2.get req_id
            let ID = NAME.charAt(0) + NAME.charAt(1) + NAME.charAt(2) + KEY.charAt(1) + KEY.charAt(2) + KEY.charAt(3) + KEY.charAt(4) + KEY.charAt(5) + KEY.charAt(6) + KEY.charAt(7) + KEY.charAt(8);

            this.sendTelegram(PLATE)

            REQ_WALKIN = {
                _requestID: ID,
                customer_address: ADDRESS,
                customer_nameplace: NAMEPLACE,
                //
                customer_carmake: ITEM.make,
                customer_carmodel: ITEM.model,
                customer_carplate: ITEM.plate,
                customer_caryear: ITEM.year,
                customer_cartransmission : ITEM.transmission,
                customer_carcc: ITEM.cc,
        
                customer_email: EMAIL,
                customer_name: NAME,
                customer_phone: PHONE,
        
                customer_notes: NOTES,
                customer_FUID: FUID,
    
                customer_day: BOOK_DAY ,
                customer_month: BOOK_MONTH ,
                customer_year: BOOK_YEAR,
                customer_time: BOOK_TIME,
        
                quotation : {
                  status: false,
                },
                timestamp: firebase.database.ServerValue.TIMESTAMP,
        
                //check user's app version and OS
                app_version: APP_VERSION,
                app_platform: APP_PLATFORM,
                status: 'Appointment',
                reason : REASON,
                remark :REMARK,
              }




            firebase.database().ref(`appointment_walkin/${WSMAIN_ID}/${WS_ID}/${KEY}`)
            .update(REQ_WALKIN)
            .then(() => {
                //4.update user booking stats
                firebase.database().ref(`plate_number/${PLATE}/book_walkin/`).update({
                    _book_id: KEY,
                    _book_status: 'Appointment',
                    retail_id:WS_ID,
                    retail_main:  WSMAIN_ID,
                    day: BOOK_DAY,
                    month: BOOK_MONTH,
                    year: BOOK_YEAR,
                    time: BOOK_TIME,
                    booking_by: NAME
                    },
                )
            })
    
    
        }

        setTimeout(() => {
            this.setState({view:6})
        }, 4000)
        

    
    }


    done = () => {
        firebase.analytics().logScreenView({
            screen_name: 'Appointment',
            screen_class: 'Appointment',
          }) 
        // const navigateAction = StackActions.replace({
        //     routeName: 'Appointment',
        //     action: NavigationActions.navigate({ routeName: 'Appointment' }),
        //     });
        //     this.props.navigation.dispatch(navigateAction);
        this.props.navigation.popToTop();
    }

    render(){
        const BLUE = '#080880';
        const WHITE = '#FFFFFF';
        const GREY = '#BDBDBD';
        const BLACK = '#424242';
        const LIGHT_GREY = '#fafdff';

        let logo 
        if(this.state.logo !== ''){
           logo = <Image source={{uri:this.state.logo}} style={{height: 60, width:60, borderRadius:30,resizeMode:'contain' }}/>
        } else {
            logo = <Image source={{uri:'https://images.says.com/uploads/story_source/source_image/450525/475e.jpg'}} style={{height: 60, width:60, borderRadius:30}}/>
        }

        let content 
        if(this.state.view === 1){
            content = 
            <View style={styles.box}>
                <Text style={{fontFamily: "ProximaNova-Bold",fontSize: 25, textAlign:'center'}}>Appointment</Text>
                <Text style={{fontFamily: "ProximaNova-Bold",color:'#3f3f3f', fontSize: 16, textAlign:'center', marginTop:20, marginBottom:20}}>What is the main reason for your appointment?</Text>

                <View style={{flexWrap:'wrap',marginBottom:20,flexDirection:'row'  }}>
                {this.renderReason()}
                </View>

                <FormTextInput 
                    title = 'REMARKS / NOTES'
                    placeholder = 'eg: I would like to change my tyre next Monday. Any free slots?'
                    multiline={true}
                    value={this.state.remark} 
                    onChangeText={(x) => this.setState({remark:x})}
                /> 

                <Button title='Next' onPress={this.firstview} bgColor='#080880' width= {WIDTH} marginTop={35} />

                <BorderButton onPress={this.cancel} title='Cancel' borderColor='#080880' width= {WIDTH} marginTop={15} />
                
                <View style={{height: 15}} />

            </View>
        }

        //select date/time
        if(this.state.view === 2){
            content = 
            <View>
                <Text style={{fontFamily:'ProximaNova-Bold',marginBottom:5, fontSize:16}}>Please select a date for your service</Text>
                <Text style={{marginBottom:15}}>For same day services, please chat with the merchant before driving in to avoid longer waiting times.</Text>
                <View style={styles.box}>
                    <Calendar
                        onChange={(date) => this.setState({date})}
                        selected={this.state.date}
                        //finalStage="month" / + 1 so that they cant pick today to do servicing
                        minDate={Moment().add(this.state.controlTime, 'days')}
                        maxDate={Moment().add(10, 'years').startOf('day')}
                        //General Styling}
                        style={{
                        borderRadius: 5,
                        alignSelf: 'center',
                        marginLeft:50,
                        marginRight:50,
                        width:'100%',

                        }}
                        barView={{
                        padding: 15,

                        }}
                        barText={{
                        fontFamily: "ProximaNova-Bold",
                        color: BLUE,
                        fontSize:14
                        }}
                        stageView={{
                        padding: 0,
                        }}

                        // Day selector styling
                        dayHeaderView={{
                        borderBottomColor: WHITE,
                        }}
                        dayHeaderText={{
                        fontFamily: "ProximaNova-Bold",
                        color: GREY,
                        }}
                        dayRowView={{
                        borderColor: WHITE,
                        height: 40,
                        }}
                        dayText={{
                        color: BLACK,
                        }}
                        dayDisabledText={{
                        color: GREY,
                        }}
                        dayTodayText={{
                        fontFamily: "ProximaNova-Bold",
                        color: BLUE,
                        }}
                        daySelectedText={{
                        fontFamily: "ProximaNova-Bold",
                        backgroundColor: '#e85b1a',
                        color: WHITE,
                        borderRadius: 10,
                        borderColor: "transparent",
                        overflow: 'hidden',
                        }}
                        // Styling month selector.
                        monthText={{
                        color: BLACK,
                        borderColor: BLACK,
                        }}
                        monthDisabledText={{
                        color: GREY,
                        borderColor: GREY,
                        }}
                        monthSelectedText={{
                        fontFamily: "ProximaNova-Bold",
                        backgroundColor: BLUE,
                        color: WHITE,
                        overflow: 'hidden',
                        }}
                        // Styling year selector.
                        yearMinTintColor={BLUE}
                        yearMaxTintColor={GREY}
                        yearText={{
                        color: BLACK,
                        }}
                    /> 
                </View>

                <View style={{...styles.box,marginBottom:15}}>
                    <Text style={{marginLeft:15, fontFamily:'ProximaNova-Bold',marginBottom:15,fontSize:16}}>Please select a time for your service</Text>
                    <View style={{flexDirection:'row', flexWrap:'wrap',}}>
                        {this.renderTime()}
                    </View>
                    
                </View>

                <View style={styles.box}>
                    <View style={{borderBottomWidth:0.5, borderBottomColor:'#BDBDBD', marginBottom:15}} >
                        <Text style={{textAlign:'center', marginBottom:15, fontSize:16}} >Booking Schedule</Text>
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <View style={{flex:2, marginRight:15, justifyContent:'center', alignItems:'center'}} >
                            <Text style={{ marginBottom:10, }}>Date</Text>
                            <Text style={{fontFamily: "ProximaNova-Bold",fontSize:18, textAlign:'center'}} >{Moment(this.state.date).format("DD MMM YYYY")}</Text>
                        </View>
                        <View style={styles.mid} />
                        <View style={{flex:2, justifyContent:'center', alignItems:'center'}}>
                            <Text style={{ marginBottom:10, }}>Time</Text>
                            <Text style={{fontFamily: "ProximaNova-Bold",fontSize:18}} >{this.state.time_selected} </Text>
                        </View>
                    </View>
                </View>

                
                <Button title='Next' onPress={this.secondview}  bgColor='#080880' width= {WIDTH2} marginTop={35} />

                <BorderButton title='Back'  onPress={() => this.setState({view:1})} borderColor='#080880' width= {WIDTH2} marginTop={15} />

                <View style={{height:15}} />

            </View>
        }

        //select vehicle
        if(this.state.view === 3){
            content = 
            <View>
                <Text style={{marginLeft:15, fontFamily:'ProximaNova-Bold',marginBottom:5, fontSize:16}}>Select the vehicle(s)</Text>
                <Text style={{marginLeft:15,marginBottom:15, fontSize:13}}>Which vehicle(s) would you like to service? </Text>

                <Text style={{marginLeft:15, fontFamily:'ProximaNova-Bold',fontSize:16}}>My vehicles</Text>

               <View style={{flexDirection:'row',justifyContent:'flex-start', flexWrap:'wrap', marginTop:15}}>
                {this.renderVehicle()}
               </View>


                <Button title='Next' onPress={this.thirdview}  bgColor='#080880' width= {WIDTH2} marginTop={35} />

                <BorderButton  onPress={() => this.setState({view:2})} title='Back' borderColor='#080880' width= {WIDTH2} marginTop={15} />

                <View style={{height: 15}} />
            </View>
        }

        //confirmation
        if(this.state.view === 4){
            content = 
            <View>
                <Text style={{marginLeft:15, fontFamily:'ProximaNova-Bold',marginBottom:5, fontSize:18}}>Confirmation</Text>
                <Text style={{marginLeft:15,fontSize:13}}>Please double check the appointment date </Text>

                <View style={{margin:15}}>
                    {this.renderSelectedVehicle()}
                </View>

                <Button title='Next' onPress={this.fourthview}  bgColor='#080880' width= {WIDTH2} marginTop={35} />
                <BorderButton title='Back' onPress={() => this.setState({view:3})}  borderColor='#080880' width= {WIDTH2} marginTop={15} />


            </View>
        }

        let loadingSubmit
        if(this.state.loading === true){
            loadingSubmit = <Button title='Submitting...'  bgColor='#080880' width= {WIDTH2} marginTop={35} />
        } else {
            loadingSubmit =<Button title='Confirm' onPress={this.booking}  bgColor='#080880' width= {WIDTH2} marginTop={35} />
        }

        //summary
        if(this.state.view === 5){
            content = 
            <View>
                <Text style={{marginLeft:15, fontFamily:'ProximaNova-Bold',marginBottom:15, fontSize:16}}>Summary</Text>
                <View style={styles.box}>
                    <View>
                        <Text style={styles.title}>SERVICE DATE & TIME</Text>
                        <Text style={styles.desc}>{Moment(this.state.date).format("DD MMMM YYYY")} {this.state.time_selected} </Text>
                    </View>
                    <View>
                        <Text style={styles.title}>VEHICLE</Text>
                        {this.showSelected()}
                    </View>
                    <View>
                        <Text style={styles.title}>WORKSHOP NAME & ADDRESS</Text>
                        <Text style={styles.desc}>{this.state.ws_data.name}</Text>
                        <Text style={styles.desc}>{this.state.ws_data.address} </Text>
                    </View>
                    {this.state.reason !== '' ? this.showReason() : null}
                    {this.state.remark !== '' ? 
                    <View>
                        <Text style={styles.title}>REMARK</Text>
                        <Text style={styles.desc}>{this.state.remark} </Text>
                    </View> : null}
                </View>

                
                {loadingSubmit}
                <BorderButton title='Back' onPress={this.cancel}  borderColor='#080880' width= {WIDTH2} marginTop={15} />

            </View>
        }

        if(this.state.view === 6){
            content = 
            <View >
                <View style={{alignItems:'center', justifyContent:'center', marginBottom:20}}>
                    <Icon name='checkcircle' color='#14c087' size={30} />
                    <Text style={{fontFamily: "ProximaNova-Bold", marginTop:20, fontSize:20}}>Submitted!</Text>
                </View>
                <View style={styles.box}>
                    <View style={{padding:10, borderBottomColor:'#cdcdcd', borderBottomWidth:0.5}}>
                        <Text style={{textAlign:'center'}}>Booking submitted for approval</Text>
                    </View>
                    <View>
                        <Text style={styles.title}>SERVICE DATE & TIME</Text>
                        <Text style={styles.desc}>{Moment(this.state.date).format("DD MMMM YYYY")} {this.state.time_selected} </Text>
                    </View>
                    <View>
                        <Text style={styles.title}>VEHICLES</Text>
                        {this.showSelected()}
                    </View>


                    <RoundedButton title='Got it!' onPress={this.done} bgColor='#080880' marginTop={25} width={WIDTH} />
                </View> 
                <View>
                    <Text style={styles.title}>Reminder:</Text>
                    <Text style={styles.remind}>1) Your booking has been submitted.</Text>
                    <Text style={styles.remind}>2) Please wait for the merchant to confirm your appointment.</Text>
                    <Text style={styles.remind}>3) Please ensure that you arrive on time for the service provider to perform a preliminary inspection of your vehicle.</Text>
                </View>
                <View style={{height:15}} />
            </View>

        }



        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Book an Appointment' onPress={this.cancel} />
                    
                    <KeyboardAvoidingView
                        behavior={(Platform.OS === 'ios') ? "padding" : null} enabled
                        style={{ flex: 1,}}
                        >
                        <ScrollView style={{flex:1, padding:15}} showsVerticalScrollIndicator={false} >
                        {/* <View style={{margin:15, flexDirection:'row', alignItems:'center'}} >
                            {logo}
                            <View style={{marginLeft:15, marginRight:15, flexShrink:1}} >
                                <Text style={{fontFamily: "ProximaNova-Bold", fontSize:15, marginBottom:5}} >{this.state.ws_data.name} </Text>
                                <Text style={{color:'#3f3f3f'}} >{this.state.ws_data.address} </Text>
                            </View>
                        </View> */}

                        {content}

                        <View style={{height:25}} />
                        </ScrollView>
                        

                    </KeyboardAvoidingView>
                    
                </View>
            </SafeAreaView>
        )
    }
}

WorkshopBookingAppointment = inject('mobx_auth','mobx_retail', 'mobx_carkey', 'mobx_config')(observer(WorkshopBookingAppointment))
export default WorkshopBookingAppointment

const styles = StyleSheet.create({
    noPick: {borderRadius:7, borderColor:'#000',width:'28%', padding:10, justifyContent:'center', alignItems:'center', margin:7.5, borderWidth:0.5},
    textnoPick:{color:'#000', textAlign:'center'},
    textyesPick:{color:'#fff', textAlign:'center'},
    yesPick: {borderRadius:7,backgroundColor:'#e85b1a',width:'28%', padding:10, justifyContent:'center', alignItems:'center', margin:7.5, borderColor:'#e85b1a', borderWidth:0.5,},
    box :{
        borderWidth:0.5,
        borderColor:'#dddddd',
        backgroundColor:'#fff',
        borderRadius:10,
        marginBottom:15,
        padding:15,
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 3 },
              shadowOpacity: 0.3,
            },
            android: {
              elevation: 3
            },
          }),
    },
    picked:{backgroundColor:'#e85b1a',borderWidth:0.5, borderColor:'#e85b1a', width: '40%', margin:7.5, borderRadius:20, padding:10, justifyContent:'center', alignItems:'center',},
    nonpicked:{backgroundColor:'#fff',borderWidth:0.5, borderColor:'#000', width: '40%',margin:7.5, borderRadius:20, padding:10, justifyContent:'center', alignItems:'center',},
    image: {
        width: Dimensions.get('window').width /3.8,
        height:Dimensions.get('window').width /3,
        borderRadius:10,
        backgroundColor:'#fff'
    },
    image2:{
        width: Dimensions.get('window').width /3.8,
        // height:Dimensions.get('window').width /3,
        marginRight:10,
        borderRadius:10,
        backgroundColor:'#fff',
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 3 },
              shadowOpacity: 0.3,
            },
            android: {
              elevation: 3
            },
          }),
    },
    title: {color:'#6e6d6d', fontSize:13,marginTop:10, marginBottom:10, fontWeight:'bold', },
    remind: {color:'#6e6d6d', fontSize:13,marginTop:10, fontWeight:'bold',},
    desc: {color:"#080880", fontFamily: "ProximaNova-Bold",marginBottom:10,},
    mid:{
        borderRightWidth: 1,
        borderRightColor: '#ccc',
        marginTop: 10,
        // marginBottom:10,
      },
})