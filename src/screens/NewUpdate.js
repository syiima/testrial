import React, {Component} from 'react';
import {StatusBar, Platform,StyleSheet,SafeAreaView, Text, View,Button, Keyboard,Dimensions, ImageBackground, Alert,TouchableOpacity, ScrollView,Image, TextInput} from 'react-native';
import { inject, observer } from 'mobx-react';
import Icon from 'react-native-vector-icons/FontAwesome';
import AppLink from 'react-native-app-link';
import RNExitApp from 'react-native-exit-app'

import firebase from '@react-native-firebase/app';
import upgrade from '../assets/update.png'

const CONST_HEIGHT = Dimensions.get('window').height;
const CONST_WIDTH = Dimensions.get('window').width;

class NewUpdate extends React.Component{
    constructor(props){
        super(props);
        this.state = {
        email: '',
        password: '',
        error:'',
        hidePassword: true,


        userInfo: '',
        }
    }

  componentDidMount(){
    this.versionChecker();

  }

  componentWillUnmount(){
    firebase.database().ref('consts/version_code').off()
  }

  versionChecker = () => {
    let that = this;
    let APP_VERSION = this.props.mobx_config.constV;
    firebase.database().ref('consts/version_code').on('value', (snapshot) => {
      if(snapshot.exists()){

        if(APP_VERSION < snapshot.val()){
          that.PromptVersionChanger();
        } else {
          that.props.mobx_auth.setCheck(3);
        }
        

      } 
    })
  }

  PromptVersionChanger = () => {
    let that = this;
    that.props.mobx_auth.setCheck(4);
  }

  update = () => {
    // console.warn("hai");
      AppLink.openInStore({ appName:'Serv', appStoreId:'1284497301', appStoreLocale:'my', playStoreId:'my.serv.customer' }).then(() => {
        // do stuff
      })
      .catch((err) => {
        Alert.alert('Sorry', 'An error occured. Please contact SERV Team if the issue still persists');
        // handle error
      });
  }

  later = () => {
    RNExitApp.exitApp();
  }

  render(){
      return (
        <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
        <StatusBar barStyle="light-content" backgroundColor='#1d1d1d'/>
        <View style={styles.container}>
          <View style={{height:12}}></View>
          <Image source={upgrade} style={styles.img} />
          <View style={{marginBottom: 40}}>
            <Text style={{textAlign:'center', fontSize:18, fontFamily:'ProximaNova-Bold', marginBottom:15}}>Time to update!</Text>
  
            <Text style={{textAlign:'center', fontSize:14}}>We added lots of new features and fixed some bugs to make your experience as smooth as possible</Text>
          </View>
  
  
          <TouchableOpacity style={styles.addi} onPress={this.update}>
            <Text style={{color:'#fff', textAlign:'center', fontFamily:'ProximaNova-Bold',fontSize:17}}>Update now</Text>
          </TouchableOpacity>

          <TouchableOpacity style={{...styles.addi, backgroundColor:'#fff', borderColor:'#080880', borderWidth:1}} onPress={this.later}>
            <Text style={{color:'#080880', textAlign:'center', fontFamily:'ProximaNova-Bold',fontSize:17}}>Later</Text>
          </TouchableOpacity>
  
        </View>
        </SafeAreaView>
        );
    }
  }
  
  const styles = {
    container: {
      flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 40,
        backgroundColor:'#fafdff'
    },
    img: {
      width: Dimensions.get('window').width - 80,
      height:Dimensions.get('window').width -100,
      marginBottom: 30,
      resizeMode:'contain'
    },

    addi:{
      backgroundColor:'#080880', 
      padding:15, borderRadius:10, 
      marginBottom: 15,
      width: Dimensions.get('window').width - 80,
      
      // height:20,
    }
  };

NewUpdate = inject('mobx_auth','mobx_config')(observer(NewUpdate))
export default NewUpdate;

