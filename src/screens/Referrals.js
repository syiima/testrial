import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/AntDesign"

import Header from '../components/Header.js'

class Referrals extends React.Component{
    constructor(){
        super()
        this.state = {
            loading: true,
            referralsData: ''
        }
    }
    
    componentDidMount(){
        this.fetchReferrals()
    }

    fetchReferrals(){
        let DATA = []
      
        firebase.database().ref('challenges/').once('value').then((snapshot) => {
          if(snapshot.exists()){
            KEY = Object.keys(snapshot.val());
    
            KEY.forEach((id) => {
              let a = snapshot.val()[id];
              a.key = id
    
              DATA.unshift(a)
    
            })
    
            this.setState({referralsData : DATA, loading:false,})
          } else{
              this.setState({loading: false,})
          }
        })
    }
    

    navigate = (x) => {
        if(x.cat === 'friend'){
            this.props.navigation.navigate('ReferFriend')
        } else {
            this.props.navigation.navigate('ReferMerchant', {item: x})
        }
    }

    showReferrals = () => {
        let display
        let DATA = this.state.referralsData

        if(DATA.length !== 0){
            display = DATA.map((item) => (
            <View style={styles.box}>
                <TouchableWithoutFeedback onPress={() => this.navigate(item)}>
                    <View >
                    <Image source ={{uri: item.banner}} style={{width: Dimensions.get('window').width - 30, height: Dimensions.get('window').width / 3.4,borderRadius:15, resizeMode:'cover'}}/>
                    </View>
                </TouchableWithoutFeedback>
                
                <Text style={{fontFamily:'ProximaNova-Bold', padding:10, fontSize:15}} >{item.title} </Text>
            </View>

            ))
        }

        return display
    }
    render(){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Referrals' onPress={() => this.props.navigation.goBack()} />
                    
                    {this.showReferrals()}

                    
                </View>
            </SafeAreaView>
        )
    }
}

Referrals = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(Referrals))
export default Referrals

const styles = StyleSheet.create({
    box:{
        backgroundColor:'#fff', width: Dimensions.get('window').width - 30, borderRadius:15, marginBottom:15, margin:15,
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowRadius: 3,
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.6,
            },
            android: {
                elevation: 4
            },
        }),
    }
})