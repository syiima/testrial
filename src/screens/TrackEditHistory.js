import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/AntDesign"
import Moment from 'moment';
import DatePicker from 'react-native-datepicker';

import Header from '../components/Header.js'
import FormTextInput from "../components/FormTextInput.js"
import Button from "../components/Button.js"
import BorderButton from "../components/BorderButton.js"
const WIDTH = Dimensions.get('window').width - 30

class TrackEditHistory extends React.Component{
    constructor(){
        super()
        this.state = {
            saving: false,
            button: 'edit',
            view:1,
        }
    }
    
    componentDidMount(){
        this.setData()
    }

    setData = () => {
        let PRICE = this.props.navigation.state.params.item.price
        let DESC = this.props.navigation.state.params.item.description
        let MILEAGE = this.props.navigation.state.params.item.mileage
        let WORKSHOP = this.props.navigation.state.params.item.workshop
        let DATE = new Date(this.props.navigation.state.params.item.date)

        this.setState({
            price:PRICE,
            description: DESC,
            mileage: MILEAGE,
            workshop: WORKSHOP,
            date: DATE
        })
    }
    

    edit = () => { this.setState({button: 'update', view:2})}
    

    update = () => {
        this.setState({saving:true})
        let PLATE =  this.props.navigation.state.params.item.customer_carplate
        let PRICE = this.state.price
        let DESC = this.state.description
        let MILEAGE = this.state.mileage
        let WORKSHOP = this.state.workshop
        let DATE = this.state.date
        let MM = new Date(DATE).getTime()
        let DATA
        let ID = this.props.navigation.state.params.item.bookID

        if(PRICE !== '' && DESC !== ''&& MILEAGE !== '' && WORKSHOP !== '' ){
            DATA = {
                price: PRICE,
                mileage: MILEAGE,
                description:  DESC,
                date: MM,
                type: 'self input',
                workshop : WORKSHOP,
                timestamp: Date.now()
            }
        } else {
            Alert.alert('Incomplete details', 'Please fill in all the details. Thank you!')
            this.setState({saving:false})
            return
        }

        firebase.database().ref(`plate_number/${PLATE}/book_history/${ID}`)
        .update(DATA)
        .then(() => {
            this.props.navigation.state.params.onNavigateBack()
            this.props.navigation.popToTop()
        })


    }

    beforeRemove = () => {
        Alert.alert(
            'Delete history',
            'Are you sure?',
            [
            {text: 'Delete', onPress: this.remove},
            {text: 'Cancel',}
            ]
        )
    }

    remove = () =>{
        let ID = this.props.navigation.state.params.item.bookID
        let PLATE = this.props.mobx_carkey.PLATE

        firebase.database().ref(`plate_number/${PLATE}/book_history/${ID}`)
        .remove()
        .then(() => {
            this.props.navigation.state.params.onNavigateBack()
            this.props.navigation.popToTop()
        })
    }

    render(){
        let item = this.props.navigation.state.params.item
        
        let saveButton 
        if(this.state.saving === false && this.state.button === 'update'){
            saveButton =  <Button title='Update' bgColor='#080880' width={WIDTH} onPress={this.update} marginTop={35}/>
        } 
        if(this.state.saving === true && this.state.button === 'update'){
            saveButton = <Button title='Updating ...' bgColor='#080880' width={WIDTH} marginTop={35}/>
        }
        if(this.state.saving === false && this.state.button === 'edit'){
            saveButton =  <Button title='Edit' bgColor='#080880' width={WIDTH} onPress={this.edit} marginTop={35}/>
        }

        let content 
        if(this.state.view === 2){
            content = 
            <View>
                <FormTextInput 
                        title = 'MILEAGE (KM) '
                        placeholder='eg: 200000'
                        value={this.state.mileage} 
                        onChangeText={(x) => this.setState({mileage:x})}
                        keyboardType="numeric"
                        onSubmitEditing={() => this.workshop.focus()}
                    /> 
                    <FormTextInput 
                        title = 'WORKSHOP / LOCATION'
                        placeholder='eg: Workshop ABC, Sg Buloh'
                        value={this.state.workshop} 
                        onChangeText={(x) => this.setState({workshop:x})}
                        onSubmitEditing={() => this.description.focus()}
                    /> 
                    <FormTextInput 
                        title = 'DESCRIPTION'
                        placeholder='eg: tukar brake pad'
                        multiline = {true}
                        value={this.state.description} 
                        onChangeText={(x) => this.setState({description:x})}
                        inputRef={ref => this.description = ref}
                        onSubmitEditing={() => this.price.focus()}
                    /> 
                    <FormTextInput 
                        title = 'AMOUNT (RM)'
                        placeholder='eg: 200, 500, 124'
                        value={this.state.price} 
                        onChangeText={(x) => this.setState({price:x})}
                        inputRef={ref => this.price = ref}
                    /> 
                    <View style={{}}>
                        <Text style={{fontWeight:'bold', fontSize:13,marginBottom:10, color:'#6e6d6d' }}>DATE</Text>
                        <DatePicker
                            date={this.state.date}
                            mode="date"
                            placeholderText="Select date"
                            format="DD-MMMM-YYYY"
                            minDate="01-01-2017"
                            maxDate="31-12-2050"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            showIcon={false}
                            
                            customStyles={{
                                datePicker: {
                                    justifyContent:'center'
                                    },
                                dateInput: {
                                    borderColor:'transparent',
                                    padding:0,
                                    width:'100%'
                                },
                                dateTouchBody:{
                                    borderWidth:1, borderColor:'#ccc',borderRadius:5,
                                    width: Dimensions.get('window').width - 30,
                                    justifyContent:'flex-start',
                                    alignItems:'flex-start'
                                    // padding:10,
                                }
                            }}
                            onDateChange={(date) => this.setState({date: date})}
                        />
                    </View>
            </View>

        } else {
            content = 
            <View style={{marginTop:3}}>
                        
                <Text style={styles.mainTitle}>Service Details </Text>
                <View style={styles.box}>
                    <View style={styles.infoBox}>
                            <Text style={{flex:1, }}>Date</Text>
                            <Text style={{fontFamily: "ProximaNova-Bold",flex:3}}>{Moment(item.date).format('DD MMMM YYYY')}</Text>
                        </View>
                        <View style={styles.infoBox}>
                            <Text style={{flex:1,  }}>Time</Text>
                            <Text style={{fontFamily: "ProximaNova-Bold",flex:3 }}>{Moment(item.date).format('h:mm A')}</Text>
                        </View>
                        
                        <View style={styles.infoBox}>
                            <Text style={{flex:1,  }}>Location</Text>
                            <Text style={{fontFamily: "ProximaNova-Bold",flex:3}}>{item.workshop}</Text>

                    </View>

                </View>

                <Text style={styles.mainTitle}>Description </Text>
                <View style={styles.box}>
                    <Text style={{marginBottom:20  }}>{item.description}</Text>
                    <View style={{ flexDirection:'row',justifyContent:'space-between'}}>
                        <Text style={{fontSize:16,fontFamily: "ProximaNova-Bold",}} >Amount </Text>
                        <Text style={{fontSize:20, fontFamily: "ProximaNova-Bold",}}>RM {item.price} </Text>
                    </View>
                </View>
            </View>

        }

        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
          <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'History Details' onPress={() => this.props.navigation.goBack()} />
                    
                    <ScrollView contentContainerStyle={{padding:17}} showsVerticalScrollIndicator={false}>
                        {content}

                        {saveButton}

                        <BorderButton title='Remove' borderColor='red' onPress={this.beforeRemove}  width={WIDTH} marginTop={15}/>

                    </ScrollView>
                    
                    

                </View>
            </SafeAreaView>
        )
    }
}

TrackEditHistory = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(TrackEditHistory))
export default TrackEditHistory

const styles = StyleSheet.create({
    mainTitle :{ fontFamily: "ProximaNova-Bold",fontSize:16, marginBottom:15},
    infoBox :{flexDirection:'row',alignItems:'center', marginBottom:5},
    box :{
        backgroundColor:'#fff',
        borderRadius:10,
        marginBottom:20,
        padding:15,
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 3 },
              shadowOpacity: 0.3,
            },
            android: {
              elevation: 3
            },
          }),
    },
    desc :{fontFamily: "ProximaNova-Bold", marginBottom:5},
})