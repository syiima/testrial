import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/AntDesign"

import Header from '../components/Header.js'
import Button from '../components/Button'
import BorderButton from '../components/BorderButton'
import MiddleModal from '../components/MiddleModal'
import PickerTextInput from '../components/PickerTextInput'

import primoBg from '../assets/primoBg.png'

const WIDTH = Dimensions.get('window').width - 70

class RewardsPrimo extends React.Component{    

    render(){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#080880'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Primo Rewards' onPress={() => this.props.navigation.goBack()} />
                    
                    <ScrollView style={{padding:15}} showsVerticalScrollIndicator={false} >
                        <View style={styles.box}>
                            <View style={{alignItems:'center', justifyContent:'center'}}>
                                <Text style={styles.text}>Expressing our gratitudes to all</Text>
                                <Text style={{fontFamily: "ProximaNova-Bold",color:"#080880",fontSize:22, }}>FRONTLINERS</Text>
                            </View>
                            <Image source={primoBg} style={styles.image} />
                            <Text style={{fontFamily: "ProximaNova-Bold",color:"#080880",textAlign:'center',  marginTop:10, fontSize:18}}>#servprihatin</Text>
                            <Button
                                onPress={() => this.props.navigation.navigate('RewardsPrimoStatus')}
                                title='Check my status'
                                bgColor='#F89800'
                                width={WIDTH}
                                marginTop={10}
                            />

                            <View style={{marginTop:15}}>
                                <Text style={{fontFamily: "ProximaNova-Bold",}}>A Token of Appreciation</Text>
                                <Text style={{marginTop:15}}>SERV would like to reward all frontliners with exclusive deals & promotions for all our services.</Text>
                                <Text style={{marginTop:15}}>Easy Peasy! To apply for our Primo Rewards, just make sure your name, phone number and email are up-to-date.</Text>

                            </View>

                            <BorderButton 
                                onPress={() => this.props.navigation.navigate('RewardsPrimoApply')}
                                title='Claim now'
                                borderColor='#080880'
                                width={WIDTH}
                                marginTop={20}
                            />

                        </View>

                        <View style={{height: 30}} />
                    </ScrollView>
                    

                    

                </View>
            </SafeAreaView>
        )
    }
}

RewardsPrimo = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(RewardsPrimo))
export default RewardsPrimo

const styles = StyleSheet.create({
    box: { alignItems:'center', borderRadius:10, marginBottom:15, padding:20,backgroundColor:'#fff',
    ...Platform.select({
        ios: {
          shadowColor: '#000',
          shadowRadius: 2.22,
          shadowOffset: { width: 0, height: 1 },
          shadowOpacity: 0.22,
        },
        android: {
          elevation: 3
        },
       }),
    },
    image:{
        height: Dimensions.get('window').height / 3, width: '100%',resizeMode:'contain'
    },
    text:{fontFamily: "ProximaNova-Bold",color:"#080880",fontSize:16, textAlign:'center', marginBottom:10}
})