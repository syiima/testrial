import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,PermissionsAndroid,FlatList,KeyboardAvoidingView, ActivityIndicator, Alert,Dimensions, TouchableWithoutFeedback, TouchableOpacity, ScrollView, Image, TextInput, Linking} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'

import Icon from "react-native-vector-icons/FontAwesome";
import Iocon from 'react-native-vector-icons/MaterialCommunityIcons';
import AntIcon from 'react-native-vector-icons/AntDesign'
import MaterialIcon from "react-native-vector-icons/MaterialIcons";

import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import AsyncStorage from '@react-native-async-storage/async-storage';

import {cloud_serv_user_sandbox, cloud_serv_user_production} from "../util/FirebaseCloudFunctions"
import Header from "../components/Header"
import FormTextInput from "../components/FormTextInput"
import PickerTextInput  from "../components/PickerTextInput.js"
import MiddleModal from "../components/MiddleModal"
import BottomModal from "../components/BottomModal"
import Button from "../components/Button"

import owner from '../assets/icons/trueOwner.png'
import shared from '../assets/icons/coowner.png'
import whose from '../assets/icons/whoseOwner.png'
import Kar from '../assets/carr.png';

import car from '../assets/icons/blackCar.png'
import motor from '../assets/icons/motor.png'
import bicycle from '../assets/icons/bike.png'
import truck from '../assets/icons/truck.png'
import haulier from '../assets/icons/haulier.png'
import heli from '../assets/icons/heli.png'
import boat from '../assets/icons/boat.png'
import jet from '../assets/icons/jet.png'
import camera from '../assets/icons/camera.png'
import library from '../assets/icons/library.png'



CONST_WIDTH = Dimensions.get('window').width;
CONST_HEIGHT = Dimensions.get('window').height;

class VehicleAdd extends React.Component{
    constructor(){
        super()
        this.state = {
            showView: 0,
            plate: '',
            make:'Select make',
            model:'',
            cc: '',
            year: 'Select year',
            transmission: 'Automatic',
            modalMake: false,
            modalYear:false,
            modalCC:false,
            modalLocateCC: false,
            tokenU: '',
            secretU: '',
            allPlate: [],
            allList:[],
            listMake: '',
            make_id:'',
            reg_code:'',
            ownership: true,
            existed: false,
            platform: '',
            granttImage: '', 
            granttloading: false,
            relation: '', 
            showHint:false,
            uploading: false,
            //nonserv user
            TOinfo: '',
            phoneVerified:false,
            updatedPhone: '',
            existed: false,
            modalRelation: false,
            modalReqshared: false,
            modalPicture: false,
            modalSuccess: false,
            modalReqOwner : false,
            vehicle_desc :'',
            vehicle_key :'',

        }
    }

    componentDidMount(){
        this.getCSRFToken()
        this.getVehicleType()
        // this.getListMake()

    }

    getCSRFToken = async() => {
        let tokenU =  await AsyncStorage.getItem('CSRFTokenU')
        let secretU =  await AsyncStorage.getItem('CSRFSecretU')
        this.setState({tokenU : tokenU, secretU : secretU})
    }

    getVehicleType(){
        firebase.database().ref(`vehicle_type`).once('value').then((snp) => {
            if(snp.exists()){
                KEY = Object.keys(snp.val());
                KEY.forEach( (type_id) => {
                    let a = snp.val()[type_id];    

                    this.getListMake(type_id, a)
                    })
            }
        })

    }
    
    getListMake = (x,y) => {
        let that = this
        let VEHICLE_KEY = x
        let VEHICLE_DESC = y
        let ALL = this.state.allList
        let CAR_MAKE = []
        let b = {}
        firebase.database().ref('vehicle_make/' + VEHICLE_KEY).once('value').then((snapshot) => {
            if(snapshot.exists()){
                KEY = Object.keys(snapshot.val());
                KEY.forEach( (key_id) => {
                    let a = snapshot.val()[key_id];
            
                    a.make_key = key_id;

                    
            
                    CAR_MAKE.push(a);
                })

                b = {
                    vehicle_key :VEHICLE_KEY,
                    vehicle_desc: VEHICLE_DESC,
                    make: CAR_MAKE
                }
        
                // console.log('agah0', b);
                // let SORTED_MAKE = CAR_MAKE.sort((a, b) => a.make.localeCompare(b.make))
                ALL.push(b)
            }
        })
    }

    handleMake = (item) => {
        this.setState({make_id: item.make_key, make: item.make, modalMake:false})
    }


    //===== SAVE FUNC =============== 
    continue = (z) => {
        if(this.state.plate === ''){
          Alert.alert("Sorry", "Plate number cannot be empty")
          return;
        }
      
        if(this.state.make === 'Select make'){
          Alert.alert("Sorry", "Please select a car brand")
          return;
        }
        if(this.state.model === ''){
          Alert.alert("Sorry", "Model cannot be empty")
          return;
        }
        if(this.state.year === 'Select year'){
          Alert.alert("Sorry", "Year cannot be empty")
          return;
        }
        if(this.state.cc === 'Select cc'){
          Alert.alert("Sorry", "Car CC cannot be empty")
          return;
        }
      
       if(z === 'new'){
            this.setState({showView: 2, existed:false, phoneVerified:false})
        } else {
            if(this.state.platform === 'serv'){
                this.setState({showView: 3, existed: true, phoneVerified:false})
            } else {
                this.setState({showView: 6, existed: true})
            }  
       }
    }
    check = () => {
        let CODE = this.state.reg_code
        let FUID = this.props.mobx_auth.FUID
      
        firebase.database().ref(`referral_code/${CODE}/FUID`).once('value').then((snp) => {
          if(snp.exists()){
            let uid = snp.val()
      
            firebase.database().ref(`users/${uid}/referral_code/referers/${FUID}`).once('value').then((smp) => {
              if(smp.exists()){
                //do nothing
              } else {
                firebase.database().ref(`users/${uid}/referral_code/referers/${FUID}`).set(false)
              }
            })
          }
        })
        
    }
      
    save = () => {
      
        this.setState({modalSuccess:false, modalReqshared: false, modalReqOwner: false, uploading: true,})
      
        let that = this;
        let FUID = this.props.mobx_auth.FUID;
      
        let PLATE = this.state.plate.toUpperCase()
        let RELAT = this.state.relation
        let TOwner = this.state.TOinfo

        let TYPE = this.state.vehicle_key
        let TYPE_DESC = this.state.vehicle_desc
        let DATA 
      
        
        let  OWN = {
            name: this.props.mobx_auth.NAME,
            relationship : RELAT,
            phone: this.props.mobx_auth.PHONE
        }
      
        DATA = {
            make : this.state.make,
            model: this.state.model,
            platform: 'serv',
            timestamp: Date.now(),
            transmission: this.state.transmission,
            carcc: this.state.cc,
            year: this.state.year,
            colour: '',
            type_desc: TYPE_DESC,
            type: TYPE,
            make_id: this.state.make_id,
            picture_car :'',
        }
      
        
        if(this.state.reg_code !== ''){
            this.check()
        
            let CODE = this.state.reg_code
            firebase.database().ref(`referral_code/${CODE}/referral/${FUID}`).update({vehicle: true})
        }
        if(this.state.ownership === true){
            if(this.state.existed === false){
                firebase.database().ref(`users/${FUID}/vehicles/${PLATE}`).update({
                    picture_car:"",
                    status: "approved",
                    true_owner: true,
                })
      
                firebase.database().ref(`plate_number/${PLATE}`).update(DATA)
        
                firebase.database().ref(`plate_number/${PLATE}/owner`).update({
                    FUID: this.props.mobx_auth.FUID,
                    name: this.props.mobx_auth.NAME,
                    email: this.props.mobx_auth.EMAIL,
                    phone: this.props.mobx_auth.PHONE
                })
              
                this.props.navigation.popToTop()
            } else {
                if(this.state.granttImage !== undefined && this.state.granttImage !== ''){
                    firebase.database().ref(`users/${FUID}/vehicles/${PLATE}`).update({
                    picture_car:"",
                    status: "pending",
                    true_owner: true,
                    })
                } else {
                    firebase.database().ref(`users/${FUID}/vehicles/${PLATE}`).update({
                    picture_car:"",
                    status: "approved",
                    true_owner: true,
                    })
                }
              
                if(this.state.platform !== 'non serv'){
                    firebase.database().ref(`users/${TOwner.ownerFUID}/vehicles/${PLATE}`).update({status:'verify'})
                    firebase.database().ref(`plate_number/${PLATE}/user/${FUID}`).update({
                    name: this.props.mobx_auth.NAME,
                    relationship :'not owner',
                    make: this.state.make,
                    model: this.state.model,
                    timestamp: Date.now(),
                    email: this.props.mobx_auth.EMAIL,
                    })
                    this.sendNoti('verify')
                } else {
                    if(this.state.phoneVerified === false){
                        firebase.database().ref(`plate_number/${PLATE}/user/${FUID}`).update({
                            name: this.props.mobx_auth.NAME,
                            relationship :'not owner',
                            make: this.state.make,
                            model: this.state.model,
                            timestamp: Date.now(),
                            email: this.props.mobx_auth.EMAIL,
                        })
            

                        this.sendNoti('verify_manual')
                    } else {
                        firebase.database().ref(`plate_number/${PLATE}`).update({platform: 'serv'})
                        firebase.database().ref(`plate_number/${PLATE}/owner`).update({
                            FUID: this.props.mobx_auth.FUID,
                            email: this.props.mobx_auth.EMAIL,
                            name: this.props.mobx_auth.NAME,
                            phone: this.state.updatedPhone
                        }).then(() => {
                            firebase.database().ref(`users/${FUID}/vehicles/${PLATE}/`).update({status: 'approved' })
                            firebase.database().ref(`users_non_serv/${TOwner.ownerFUID}/vehicles/${PLATE}/`).remove()  
                        }).then(() => {
                            this.props.navigation.state.params.refresh_car()
                            this.props.navigation.goBack(null)
                        })
                }
              }
            }
          } else {
            if(this.state.existed === false){
              firebase.database().ref(`users/${FUID}/vehicles/${PLATE}`).update({
                picture_car:"",
                status: "approved",
                true_owner: false
              })
      
              firebase.database().ref(`plate_number/${PLATE}`).update(DATA)
              this.props.navigation.popToTop()
      
            } else {
      
              firebase.database().ref(`users/${FUID}/vehicles/${PLATE}`).update({
                picture_car:"",
                status: "pending",
                true_owner: false
              }).then(() => {
                firebase.database().ref(`plate_number_request/${PLATE}/users/${FUID}`).update({
                  relationship: RELAT,
                  name: this.props.mobx_auth.NAME,
                  timestamp: Date.now(),
                  status: "pending",
                  phone: this.props.mobx_auth.PHONE
                })
              }).then(() => {
                this.sendNoti('req') 
              })
             
             
            } 
        }
    }
      
    sendNoti = (x) => {
        let PLATE = this.state.plate.toUpperCase()
        let FUID = this.props.mobx_auth.FUID
        let OWNERID = this.state.TOinfo.ownerFUID
        let  NAME = this.props.mobx_auth.NAME
        let PHONE = this.props.mobx_auth.PHONE
        let EMAIL = this.props.mobx_auth.EMAIL
        let TYPE = this.state.vehicle_desc
      
        let URL, TEXT, BODY, Q_FUID, Q_VEHICLE_ID, TELEGRAM_URL, TELE_TEXT
      
        if (this.props.mobx_config.config === 0) {
          URL = cloud_serv_user_sandbox.personalNotifications
          TELEGRAM_URL = 'https://api.telegram.org/bot272822956:AAEduqgWA9gt6tpxYHKp2h1Opr4ZFNftyNc/sendMessage?chat_id=-1001396604012&text=';
        }
        if (this.props.mobx_config.config === 1) {
          URL = cloud_serv_user_production.personalNotifications
          TELEGRAM_URL = 'https://api.telegram.org/bot272822956:AAEduqgWA9gt6tpxYHKp2h1Opr4ZFNftyNc/sendMessage?chat_id=-1001476910252&text='
        }
        
        if(x === 'req'){
          TEXT = '?text=Co-owner request'
          BODY = `&body=A user has requested to co-own your vehicle ${PLATE}.	`  
        } else {
          TEXT = '?text=True owner request'
          BODY = `&body=Another user is claiming this vehicle ${PLATE} as their own. Upload a grant to verify it is yours.	`
        
          if(Platform.OS === 'ios'){
            TELE_TEXT = 'A new vehicle verification request was just made on the SERV User app.\n\nUser name : ' + NAME.toUpperCase() +  ' \nUser email : '  + EMAIL + ' \nPhone no : ' + PHONE  +  ' \nVehicle plate : '  + PLATE + ' \nVehicle type : ' + TYPE 
      
          } else {
            TELE_TEXT = 'A new vehicle verification request was just made on the SERV User app.%0A%0AUser name : ' + NAME.toUpperCase() + ' %0AUser email : '  + EMAIL +  '%0APhone no : ' + PHONE + ' %0AVehicle plate : ' + PLATE + ' %0AVehicle type : '  + TYPE 
      
          }
          let final = TELEGRAM_URL + TELE_TEXT
          fetch(final)
        }
      
        Q_FUID = `&userKey=${OWNERID}`
        Q_VEHICLE_ID = `&vehicle_id=${PLATE}`
        
        FINAL_URL = URL + TEXT + BODY + Q_FUID + Q_VEHICLE_ID
      
        if(x === 'verify_manual'){
          this.props.navigation.state.params.refresh_car()
          this.props.navigation.goBack(null)
        } else {
          fetch(FINAL_URL,{
            credentials:true, 
            headers: {
              csrftoken: this.state.tokenU,
              csrfsecret: this.state.secretU
            }
          }).then(() => {
            this.props.navigation.state.params.refresh_car()
            this.props.navigation.goBack(null)
        
          })
        }
      
    }

    checkPhone = () => {
        let TOPhone = this.state.TOinfo.ownerPhone 
        let Phone = this.state.updatedPhone
        
        if(Phone === TOPhone){
            this.setState({ phoneVerified:true, ownership: true, platform: 'non serv'})
            Alert.alert('Phone number matched', 'Successfully identified you as the true owner. You may upload your vehicle registration certificate or click \'Continue\' to proceed.')
        } else {
            this.setState({phoneVerified: false, showHint:true})
            Alert.alert('Phone number does not match', 'The phone number registered under your user does not match with the phone number used during the workshop manual input. If you are unable to obtain the initial phone number, please upload a vehicle registration certificate to get verified as a true owner for this vehicle.')
        }
    }
      
    manualReq = () => {
        if(this.state.phoneVerified === false){
            if(this.state.granttImage === undefined || this.state.granttImage === ''){
                Alert.alert('Sorry', 'Please upload a picture of the vehicle registration certificate to get verified as the true owner')
                return
            }
        } else {
            // console.warn('good');
            let FUID = this.props.mobx_auth.FUID
            let PLATE = this.state.plate.toUpperCase()
            let PHONE = this.state.updatedPhone
            //updatePhone 
            
            if(this.state.phoneVerified === false){ 
                firebase.database().ref(`plate_number/${PLATE}/user/${FUID}`).update({phone: PHONE})
                firebase.database().ref(`users/${FUID}/`).update({Phone: PHONE })
            
                this.setState({modalSuccess: true})
            }
            if(this.state.phoneVerified === true){ 
                this.setState({modalSuccess: true})
            }
        }
    }

      
      
    //======  check plate ================
    checkPlate = () => {
        let PLATE = this.state.plate.toUpperCase()

        if(this.state.plate === ''){
            Alert.alert('Empty field', 'Please fill in all the details before pressing \'Continue\'. Thank you!')
            return
        }

        firebase.database().ref(`plate_number/${PLATE}`).once('value', (snp) => {
            if(snp.exists()){
                this.fetchOwner(PLATE)
            } else {
                this.continue('new')
            }
        })
       
    }
  
    fetchOwner(X){
        let PLATE = X
        let INFO = []
        let that = this
    
        let FUID = this.props.mobx_auth.FUID
        
        firebase.database().ref(`plate_number/${PLATE}`).once('value', (snp) => {
            if(snp.exists()){
        
                let GRANTT, phoneVerified, USER, TOPHONE
                let PLATFORM  = snp.val().platform   
        
                if(PLATFORM === 'non serv' || PLATFORM === undefined ){
                    TOPHONE = snp.val().owner.phone
                    USER = this.props.mobx_auth.PHONE
        
                if(TOPHONE === USER){phoneVerified = true} else {phoneVerified = false}
                }
        
                if(snp.val().car_registration !== undefined){
                    GRANTT = snp.val().car_registration.file.downloadURL !== undefined ? snp.val().car_registration.file.downloadURL : snp.val().car_registration.file
                }  else {
                    GRANTT = ""
                }
        
                let OWNER = snp.val().owner.FUID
                let DATA = {
                    platform: PLATFORM,
                    granttImg: GRANTT,
                    ownerFUID : OWNER,
                    ownerPhone : TOPHONE
                }
            
                if(OWNER === FUID){
                    Alert.alert("Sorry", "It seems that you have already added this vehicle")
                    this.setState({showView:1, plate : '', make: 'Select make', model:'', year:'Select year', cc:'Select cc', transmission:'Automatic'})
                }  else {
                    this.setState({TOinfo: DATA, platform: PLATFORM, phoneVerified: phoneVerified, updatedPhone: USER, existed:true}, () => this.continue('exists'))
        
                }
            }
        })
    }

    //========= GRANTT UPLOAD ===========
    openCamera = async() => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                    title: "App Camera Permission",
                    message:"App needs access to your camera to snap photos",
                }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED){
                const options = {quality:1}
                launchCamera(options, (response) => {
                this.setState({granttloading: true})
            
                if(response.didCancel){
                    this.setState({modalPicture: false, granttloading:false})
                }
                if (response.error) {
                    Alert.alert('Sorry', "Please try again");
                    this.setState({modalPicture: false, granttloading: false})
                } 
                if(!response.didCancel && !response.error) {
                    const source = response.assets[0].uri;
                    this.setState({
                        granttImage: source,
                        modalPicture: false,
                        granttloading: false
                    });
                    this.publish()
                    }
                });
            } else {
                console.log('Camera permission denied');
            }
        } catch (error) {
            console.warn(error)
        }

    }
    openLibrary = () => {
        let options
        if(Platform.OS === 'ios'){
        options = {quality:1}
        } else{
        options = {quality:1, maxWidth: 800, maxHeight: 800 }
        }
        
        launchImageLibrary(options, (response) => {
            this.setState({modalPicture: false, granttloading: true})
            if(response.didCancel){
                this.setState({modalPicture: false, granttloading: false})
            } 
            if (response.error) {
                Alert.alert('Sorry', "Please try again");
                this.setState({modalPicture: false, granttloading:false})
            } 
            if(!response.didCancel && !response.error) {
                const source = response.assets[0].uri;
                this.setState({
                    granttImage: source,
                    modalPicture: false,
                    granttloading: false,
                });
                this.publish()
            }
        });
    
    }
  
    publish = async() => {
        let FUID = this.props.mobx_auth.FUID
        let PLATE = this.state.plate.toUpperCase()
    
        const imageuri= this.state.granttImage;
        // console.warn('feck', this.state.existed, this.state.platform, this.state.phoneVerified)
    
    
        const URLVehic = Platform.OS === 'ios' ? imageuri.replace('file://', '') : imageuri;
        const refFile = firebase.storage().ref().child(`plate_number_verification/${PLATE}/${FUID}`);
        await refFile.putFile(URLVehic);
        const result = await refFile.getDownloadURL().then(url => {
                let IMAGE_URL = url
                // console.warn('key',url)
            
                if(this.state.existed === true && this.state.platform === 'serv' ){
                    firebase.database().ref(`plate_number_verification/${PLATE}/${FUID}`).update({
                    name: this.props.mobx_auth.NAME,
                    phone: this.props.mobx_auth.PHONE,
                    timestamp: Date.now(),
                    grantt: IMAGE_URL,
                    status:'pending'
                    })  
                } 
                if(this.state.existed === true  && this.state.platform === 'non serv' && this.state.phoneVerified === false ){
                    firebase.database().ref(`plate_number_verification/${PLATE}/${FUID}`).update({
                    name: this.props.mobx_auth.NAME,
                    phone: this.props.mobx_auth.PHONE,
                    timestamp: Date.now(),
                    grantt: IMAGE_URL,
                    status:'pending'
                    })  
                } 
                if(this.state.existed === true && this.state.platform === 'non serv' && this.state.phoneVerified === true){
                    firebase.database().ref(`plate_number/${PLATE}/car_registration`).update({
                    file: IMAGE_URL,
                    timestamp_added: Date.now()
                    });
                }
                if(this.state.existed === false && this.state.platform === 'serv') {
                    firebase.database().ref(`plate_number/${PLATE}/car_registration`).update({
                    file: IMAGE_URL,
                    timestamp_added: Date.now()
                    });
                }    
    
        })
        .catch(error => {
            let MONTH = (new Date().getMonth()) + 1
            let m = error.message
                firebase.database().ref(`crash_error/${MONTH}/`).push({
                    error: m,
                    timestamp:Date.now(),
                    user: FUID,
                    platform :Platform.OS,
                    version : this.props.mobx_config.Version
                })

            Alert.alert('Sorry', 'An error occured. Please contact SERV Team if the issue still persists');
        });
    
    }
  

  
  
    // ======= 
    goBackButton = () => {
        let VIEW = this.state.showView
        let PLATE = this.state.plate.toUpperCase()
      
        if(VIEW > 2){
          this.setState({showView : 1, granttImage: '', relation: '', showHint:false})
        } else {
          this.props.navigation.goBack(null)
        }
    }
      
    handleChange = (text) => {
        let T = text.replace(/[`~!@#$%^&*()_|+\-=?\s;:'",.<>\{\}\[\]\\\/]/gi, '')
        let TP = T.trim()
        TP = TP.toUpperCase()
        
        this.setState({plate: TP})
    }

    showNumHint = () => {
        let display, FIRST, NUMBER
        let BNUM = this.state.TOinfo
        if(BNUM !== '' && BNUM !== undefined){
          let NUM = this.state.TOinfo.ownerPhone
          if(NUM[0] === '+'){
            FIRST = NUM.slice(2, -4)
          } else {
            FIRST = NUM.slice(0, -4)
          }
          let REPLACE = FIRST.replace(/[0-9]/g, "X")
          let LAST = NUM.slice(-4)
    
          if(NUM[0] === '+'){
            NUMBER = '+60' + REPLACE + LAST
          } else {
            NUMBER = REPLACE + LAST
          }
    
          // if(this.state.showHint === true){
            display = 
            <Text style={{fontSize:12, marginLeft:15}}>Saved Number: {NUMBER} </Text>
          // }
    
        }
        
    
        return display 
      }

      
    selectedVehicle = (x) => {
        this.setState({vehicle_desc: x})

        if(x === 'Motorcycle'){
            this.setState({cc :''})
        } else {
            this.setState({cc :'Select cc'})
        }

        let ALLMAKE = this.state.allList
        for (const key in ALLMAKE) {
            let item = ALLMAKE[key]

            if(item.vehicle_desc === x){
                // console.log('aaa', item.make, item, key);
                let CAR_MAKE = item.make

                let SORTED_MAKE = CAR_MAKE.sort((a, b) => a.make.localeCompare(b.make))
                this.setState({vehicle_key : item.vehicle_key, listMake : SORTED_MAKE})
            }
                
            
        }
        
    }
      
    render(){
        let MAKE = this.state.listMake
        let content_make = [];
    
        if(this.state.listMake !== ''){
          content_make =
            <FlatList
              data={MAKE}
              renderItem={({item}) =>
                <TouchableOpacity style={{padding:10, borderBottomColor:'#ccc', borderBottomWidth:0.5}}
                onPress={() => {this.handleMake(item)}}>
                  <Text style={{fontSize:16}}>{item.make}</Text>
                </TouchableOpacity>
              }
            />
        }
        let CC = [
          "660","1.0", "1.1", "1.2","1.3","1.4","1.5","1.6","1.7","1.8","1.9","2.0",
          "2.1","2.2","2.3","2.4","2.5","2.6","2.7","2.8","2.9","3.0","3.1","3.2", "3.3",
          "3.4","3.5","3.6"
        ];
        let content_cc = [];
        for(var i = 0; i < CC.length; i++){
            let value = CC[i];
            content_cc.push(
              <View>
                <TouchableOpacity
                  style={{padding:10, borderBottomColor:'#ccc', borderBottomWidth:0.5}}
                  onPress={()=> this.setState({cc:value,modalCC:false})}>
                  <Text style={{fontSize:16}}>{value}</Text>
                </TouchableOpacity>
              </View>
            )
        }

        let content_year = [];
        let YEAR = new Date().getFullYear();
        let BEFORE = 'before ' + (YEAR - 26)
        let CURRENT_YEAR = [
          YEAR, YEAR - 1, YEAR - 2, YEAR - 3, YEAR - 4, YEAR - 5, YEAR-6,
          YEAR - 7, YEAR - 8, YEAR - 9, YEAR - 10, YEAR - 11, YEAR - 12,
          YEAR - 13, YEAR - 14,YEAR - 15, YEAR - 16, YEAR - 17, YEAR - 18, YEAR - 19,
          YEAR - 20, YEAR - 21, YEAR - 22, YEAR - 23, YEAR - 24, YEAR - 25, YEAR - 26,
          BEFORE
        ]
        for(var i = 0; i < CURRENT_YEAR.length; i++){
            let value = CURRENT_YEAR[i];
            content_year.push(
              <View >
                <TouchableOpacity
                  style={{padding:10, borderBottomColor:'#ccc', borderBottomWidth:0.5}}
                  onPress={()=> this.setState({year:value,modalYear:false})}>
                  <Text style={{fontSize:16}}>{value}</Text>
                </TouchableOpacity>
              </View>
            )
        }
        
        let RELATIONSHIP = ["Father","Mother","Husband","Wife","Brother", "Sister","Uncle","Aunt","Others"];
        let content_relation = []
        for(var i = 0; i < RELATIONSHIP.length; i++){
            let value = RELATIONSHIP[i];
            content_relation.push(
              <View >
                <TouchableOpacity
                  style={{padding:10, borderBottomColor:'#ccc', borderBottomWidth:0.5}}
                  onPress={()=> this.setState({relation:value,modalRelation:false})}>
                  <Text style={{fontSize:16}}>{value}</Text>
                </TouchableOpacity>
              </View>
            )
        }


        let content 
        if(this.state.showView === 0){
            content = 
            <ScrollView contentContainerStyle={{padding:20}} showsVerticalScrollIndicator ={false} >
                <Text style={{fontFamily: "ProximaNova-Bold",fontSize:16, marginBottom:15}}>Select Vehicle Type</Text>

                <View style={{flexDirection:'row', margin:20, justifyContent:'space-between', marginBottom:25}}>
                    <View >
                        <TouchableWithoutFeedback onPress={() => this.selectedVehicle('Car')} >
                        <View style={this.state.vehicle_desc !== 'Car' ? styles.circle : styles.selectedCircle}>
                            <Image source={car} style={styles.iconVehicle} /> 
                        </View>
                        </TouchableWithoutFeedback>
                        <Text style={styles.vehName}>Car</Text>
                    </View>
                    <View >
                        <TouchableWithoutFeedback onPress={() => this.selectedVehicle('Motorcycle')} >
                        <View style={this.state.vehicle_desc !== 'Motorcycle' ? styles.circle : styles.selectedCircle}>
                            <Image source={motor} style={styles.iconVehicle} /> 
                        </View>
                        </TouchableWithoutFeedback>
                        <Text style={styles.vehName}>Motorcycle</Text>
                    </View>
                    <View>
                        <View style={styles.greycircle}>
                            <Image source={bicycle} style={styles.iconVehicle} />
                        </View>
                        <Text style={styles.vehGrey}>Bicycle</Text>
                    </View>
                </View>
                <View style={{flexDirection:'row', margin:20, justifyContent:'space-between' ,marginBottom:25}}>
                    <View>
                        <View style={styles.greycircle}>
                            <Image source={truck} style={styles.iconVehicle} />
                        </View>
                        <Text style={styles.vehGrey}>Truck</Text>
                    </View>
                    <View>
                        <View style={styles.greycircle}>
                            <Image source={haulier} style={styles.iconVehicle} />
                        </View>
                        <Text style={styles.vehGrey}>Haulier</Text>
                    </View>
                    <View>
                        <View style={styles.greycircle}>
                            <Image source={boat} style={styles.iconVehicle} />
                        </View>
                        <Text style={styles.vehGrey}>Boat</Text>
                    </View>
                </View>
                <View style={{flexDirection:'row',margin:20,  justifyContent:'space-between' ,marginBottom:25}}>
                    <View>
                    <View style={styles.greycircle}>
                        <Image source={heli} style={styles.iconVehicle} />
                    </View>
                    <Text style={styles.vehGrey}>Heli</Text>
                    </View>

                    <View>
                    <View style={styles.greycircle}>
                        <Image source={jet} style={styles.iconVehicle} />
                    </View>
                    <Text style={styles.vehGrey}>Private Jet</Text>
                    </View>
                    <View style={{height:70, width:70, borderRadius:35, backgroundColor:'#fafdff'}} />
                    
                </View>

                <Button title='Continue' onPress={() => this.setState({showView: 1})}   marginTop={35} width={CONST_WIDTH - 30} bgColor='#080880'/>
                <View style={{height: 15}} />

            </ScrollView>
        }

        if(this.state.showView === 1 && this.state.vehicle_desc === 'Car'){
            content = 
            <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{padding:20}}>
            <Text style={{fontFamily: "ProximaNova-Bold",fontSize:16, marginBottom:15}}>Please fill in your vehicle details</Text>
            
            <FormTextInput 
                title= 'PLATE NUMBER'
                value={this.state.plate}
                keyboardType={Platform.OS === 'ios' ? 'default' : 'visible-password'}
                placeholder= 'eg: ABC1234, XX4578'
                onChangeText={this.handleChange}
            />
            <PickerTextInput 
                title= 'MAKE'
                value={this.state.make} 
                onPress={() => this.setState({modalMake:true})} 
            /> 
            <FormTextInput 
                title= 'MODEL'
                value={this.state.model}
                placeholder= 'eg: Bezza, Prius, Fiesta'
                autoCapitalize="words"
                onChangeText={(x) => this.setState({model:x})}
            /> 
            <PickerTextInput 
                title= 'YEAR'
                value={this.state.year} 
                onPress={() => this.setState({modalYear:true})} 
            /> 

            <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
                <View style={{ width: CONST_WIDTH - 100 }}>
                <PickerTextInput 
                    title= 'CC'
                    value={this.state.cc} 
                    onPress={() => this.setState({modalCC:true})} 
                /> 
                </View>
                <TouchableOpacity onPress={() => this.setState({modalLocateCC:true})} style={{marginLeft:15}} >
                    <Iocon name="information" size={30} color="#a7a7a7"  />
                </TouchableOpacity>
            </View> 
           
            <View>
                <Text style={{fontWeight:'bold', fontSize:13,marginBottom:10, color:'#6e6d6d' }}>TRANSMISSION</Text>
                <View style={styles.mainBox}>
                <TouchableWithoutFeedback onPress={() => this.setState({transmission: 'Automatic'})}>
                <View style={this.state.transmission === 'Automatic' ? styles.selected : styles.notSelected}>
                    <Text style={this.state.transmission === 'Automatic' ? styles.textS : styles.textNS}>Automatic</Text>
                </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => this.setState({transmission: 'Manual'})}>
                <View style={this.state.transmission === 'Manual' ? styles.selected : styles.notSelected}>
                    <Text style={this.state.transmission === 'Manual' ? styles.textS : styles.textNS}>Manual</Text>
                </View>
                </TouchableWithoutFeedback>
                </View>
            </View> 

            <Button onPress={this.checkPlate} title="Continue" marginTop={35} width={CONST_WIDTH - 30} bgColor='#080880'  />

            <View style={{height: 40}} />
            
        </ScrollView>

        }

        if(this.state.showView === 1 && this.state.vehicle_desc === 'Motorcycle'){
            content = 
            <KeyboardAvoidingView
                behavior={(Platform.OS === 'ios') ? "padding" : null} enabled
                style={{ flex: 1, backgroundColor:'#fafdff', justifyContent:'center'}}
                >
            <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{padding:20}}>
            <Text style={{fontFamily: "ProximaNova-Bold",fontSize:16, marginBottom:15}}>Please fill in your vehicle details</Text>
            
            <FormTextInput 
                title= 'PLATE NUMBER'
                value={this.state.plate}
                keyboardType={Platform.OS === 'ios' ? 'default' : 'visible-password'}
                placeholder= 'eg: ABC1234, XX4578'
                onChangeText={this.handleChange}
            />
            <PickerTextInput 
                title= 'MAKE'
                value={this.state.make} 
                onPress={() => this.setState({modalMake:true})} 
            /> 
            
            <FormTextInput 
                title= 'MODEL'
                value={this.state.model}
                placeholder= 'eg: Ego Solariz, Y15ZR, Rebel 500'
                autoCapitalize="words"
                onChangeText={(x) => this.setState({model:x})}
            />
            <PickerTextInput 
                title= 'YEAR'
                value={this.state.year} 
                onPress={() => this.setState({modalYear:true})} 
            /> 

            
            <FormTextInput 
                title= 'CC'
                value={this.state.cc}
                keyboardType='number-pad'
                placeholder= 'eg: 100, 250, 500'
                autoCapitalize="words"
                onChangeText={(x) => this.setState({cc:x})}
            />
            

            <Button onPress={this.checkPlate} title="Continue" marginTop={35} width={CONST_WIDTH - 30} bgColor='#080880'  />

            <View style={{height: 40}} />
            
            </ScrollView>
        </KeyboardAvoidingView>

        }

        //normal
        if(this.state.showView === 2){
            content = 
            <View style={{margin:20, flex:1, }}>        
                <Text style={{fontSize:16, fontFamily: "ProximaNova-Bold", color:'#080880', }}>Do you want to upload this vehicle's registration certificate to get verified as this vehicle's owner?</Text>
                <Text style={{fontSize:12, fontFamily: "ProximaNova-Bold", color:'tomato', marginTop:8}}>*Optional, you can upload it later under vehicle details</Text>
                
                {this.state.granttImage !== undefined && this.state.granttImage !== '' ? 
                    <TouchableWithoutFeedback onPress={() => this.setState({granttImage: '', modalPicture: true})}>
                        <View style={{height:200, width:200,marginTop:15 }}>
                        <Image source={{uri:this.state.granttImage}} style={{height:200, width:200, borderRadius:10,}} />
                        <AntIcon name="closecircle" size={20} color="tomato" style={{position:'absolute', right:-5, top:-5}} />
                    </View> 
                    </TouchableWithoutFeedback>
                : null
                } 
                { this.state.granttImage === '' || this.state.granttImage === undefined ? 
                    <Button onPress={() => this.setState({modalPicture:true})}  title='Upload Vehicle Registration Certificate' marginTop={15} width={CONST_WIDTH - 40} bgColor='#080880'  />
                : null
                }
      
                <View style={{ position:'absolute', bottom:20,}}>
                    <Button onPress={() => this.setState({modalSuccess:true})}  title='Continue' marginTop={15} width={CONST_WIDTH - 40} bgColor='#080880'  />

                </View>
      
            </View>
        }
        //existed
        if(this.state.showView === 3){
            content = 
            <View style={{flex:1, margin:20}}>
                <Text style={{fontSize:16, fontFamily: "ProximaNova-Bold", color:'#080880',}}>This plate number is already registered with an owner. Is this vehicle yours?</Text>
                {/* <View style={{marginTop: 20}}> */}
                <Image source={whose} style={{height:Dimensions.get('window').height / 3.5,width:Dimensions.get('window').width - 60,marginBottom:15,resizeMode:'contain', alignSelf:'center'}}/>
        
                <Button 
                    onPress={() => this.setState({showView: 4, ownership:false, existed:true})}
                    title='No, I would like to request as shared owner' marginTop={15} width={CONST_WIDTH - 40} bgColor='#080880'  />
                <Button 
                    onPress={() => this.setState({showView: 5, existed:true, ownership:true})}
                    title='Yes, I am the true owner of this vehicle' marginTop={20} width={CONST_WIDTH - 40} bgColor='#080880'  />
                </View>
            // </View>
        }
        //req shared
        let button_relationship
        if(this.state.relation == ''){
            button_relationship = 
            <Button 
                   onPress={() => {Alert.alert('Sorry', 'Please select the relationship between you and the owner')}}
                   title='Submit request' marginTop={20} width={CONST_WIDTH - 40} bgColor='#080880'  />
        } else {
            if(this.state.uploading === false){
                button_relationship = 
                <Button 
                   onPress={() => this.setState({modalReqshared: true})}
                   title='Submit request' marginTop={20} width={CONST_WIDTH - 40} bgColor='#080880'  />
            } else {
                button_relationship = 
                <Button 
                    title='Submitting Request...' marginTop={20} width={CONST_WIDTH - 40} bgColor='#080880'  />
            }
            
        }
        let PLATE = this.state.plate.toUpperCase()
        if(this.state.showView === 4){
            content = 
            <View style={{flex:1, margin:20}}>
                <Image source={shared} style={{height:Dimensions.get('window').height / 3.5,marginBottom:20, width:Dimensions.get('window').width - 60,resizeMode:'contain', alignSelf:'center'}}/>
                <Text style={{fontSize:16, fontFamily: "ProximaNova-Bold", color:'#080880',}}>You are requesting for approval to become a co-owner of this vehicle {PLATE} from the true owner.</Text>
                <Text style={{fontSize:16, fontFamily: "ProximaNova-Bold", color:'#080880',marginBottom:15,marginTop:15}}>What is the relationship between you and the owner of this vehicle?</Text>
                <PickerTextInput 
                    title= 'RELATIONSHIP'
                    value={this.state.relation} 
                    onPress={() => this.setState({modalRelation:true})} 
                /> 
        
                {button_relationship}
                
            </View>
        }

        let submitButton_view5 
        if(this.state.granttImage === undefined || this.state.granttImage === ''){
            submitButton_view5 = 
            <Button 
                onPress={() => {Alert.alert('Sorry', 'Please upload an image of this vehicle registration to get verified.')}} 
                title='Get verified' marginTop={15} width={CONST_WIDTH - 40} bgColor='#080880'  />
            
        } else {
            if(this.state.uploading === false){
                submitButton_view5=
                <Button 
                    onPress={() => this.setState({modalReqOwner:true})}
                    title='Get verified' marginTop={15} width={CONST_WIDTH - 40} bgColor='#080880'  />
                
            } else {
                submitButton_view5 = 
                <Button title='Submitting request ...' marginTop={15} width={CONST_WIDTH - 40} bgColor='#080880'  />
            }
        }
        //existed but owner 
        if(this.state.showView === 5){
            content = 
            <View style={{flex:1, margin:20, marginTop: 0}}>
                <Image source={owner} style={{height:Dimensions.get('window').height / 4,alignSelf:'center', width:Dimensions.get('window').width - 60,resizeMode:'contain'}}/>
                <Text style={{fontSize:14, fontFamily: "ProximaNova-Bold", color:'#080880',}}>You are claiming to be the true owner of this vehicle.</Text>
                <Text style={{fontSize:14, fontFamily: "ProximaNova-Bold",marginTop:6, color:'#080880',}}>Please upload the vehicle's registration certificate to get verified as the owner.</Text>
                
                {this.state.granttloading === true ? 
                <View style={{height:100, width:100, borderRadius:10, backgroundColor:'grey'}}>
                    <ActivityIndicator style={{zIndex:-1}}/>
                </View> : null }

                {this.state.granttImage !== undefined && this.state.granttImage !== '' ? 
                <TouchableWithoutFeedback onPress={() => this.setState({granttImage: '', modalPicture: true})}>
                    <View style={{height:200, width:200, marginTop: 15, alignSelf:'center' }}>
                    <Image source={{uri:this.state.granttImage}} style={{height:200, width:200, borderRadius:10,}} />
                    <AntIcon name="closecircle" size={20} color="tomato" style={{position:'absolute', right:-5, top:-5}} />
                    </View> 
                </TouchableWithoutFeedback>
                : 
                <Button 
                    onPress={() => this.setState({modalPicture: true})}
                    title='Upload Vehicle Registration Certificate' marginTop={15} width={CONST_WIDTH - 40} bgColor='#080880'  />
                }
        
                {this.state.granttImage !== undefined && this.state.granttImage !== '' ? submitButton_view5 : null}
                
            
            </View>
        }

            //non serv 
        if(this.state.showView === 6){
            content = 
            <View style={{flex:1, margin:20}}>
            <ScrollView>
            {this.state.phoneVerified === true ? 
            <View>
                <Text style={{fontSize:16, fontFamily: "ProximaNova-Bold", color:'#080880',marginBottom:7}}>The phone number matched!</Text>
                <Text style={{fontSize:14,}}>Do you want to upload this vehicle registration certificate to get verified as this vehicle's owner?</Text>
                <Text style={{fontSize:12, fontFamily: "ProximaNova-Bold", color:'tomato', marginTop:8}}>*Optional, you can upload it later under vehicle details</Text>
    
            </View> 
            : 
            <View>
                <Text style={{fontSize:13, fontFamily: "ProximaNova-Bold", color:'#080880',}}>The phone number that has been previously saved under this vehicle is not the same as your current phone number.</Text>
                <Text style={{fontSize:12, marginTop:6}}>If this is your vehicle, would you like to update the phone number?</Text>
                
                
                <Text style={{fontSize:13, fontFamily: "ProximaNova-Bold", color:'#080880',marginTop:20}}>Current phone number</Text>
                <View style={{ padding:(Platform.OS === 'ios') ? 10 : null,paddingLeft: 10, marginTop:15, borderRadius: 6,borderColor:'#080880', borderWidth:2, flexDirection:'row', justifyContent:'space-between', alignItems:'center', }}>
                <TextInput 
                    style={{width: '90%', fontFamily:'ProximaNova-Regular'}}
                    value = {this.state.updatedPhone}
                    onChangeText={(x)=> this.setState({ updatedPhone: x })} 
                    keyboardType='phone-pad' 
                    underlineColorAndroid='transparent'
                />
                
                </View>
                <View style={{flexDirection: 'row', alignItems:'center',marginTop:14,  }}>
                <TouchableOpacity style={{backgroundColor:'#080880', padding:14, borderRadius:6, alignSelf:'flex-start'}}  onPress={this.checkPhone}>
                    <Text style={{fontFamily:'ProximaNova-Bold', textAlign:'center', color:'#fff', fontSize:12}} >Recheck</Text>
                </TouchableOpacity>
                {this.showNumHint()}
                </View>
                
                <Text style={{marginTop: 8, color:'red', fontSize:11, }}>* Do note that this phone number will be linked to this vehicle</Text>
                <Text style={{marginTop: 5, color:'red', fontSize:11, }}>** Please press `Recheck` to rematch your phone number with saved number</Text>
            </View>}
    
    
            {this.state.granttImage !== undefined && this.state.granttImage !== '' ? 
                <TouchableWithoutFeedback onPress={() => this.setState({granttImage: '', modalPicture: true})}>
                    <View style={{height:200, width:200,marginTop:20 }}>
                    <Image source={{uri:this.state.granttImage}} style={{height:200, width:200, borderRadius:10,}} />
                    <AntIcon name="closecircle" size={20} color="tomato" style={{position:'absolute', right:-5, top:-5}} />
                </View> 
                </TouchableWithoutFeedback>
                : 
                null
            } 
            { this.state.granttImage === '' || this.state.granttImage === undefined ? 
                <Button 
                onPress={() => this.setState({modalPicture: true})}
                title='Upload Vehicle Registration Certificate' marginTop={20} width={CONST_WIDTH - 40} bgColor='#080880'  />
                
                : null}
            </ScrollView>
    
            {this.state.uploading === false ?
                <Button 
                    onPress={this.manualReq}
                    title='Continue' marginTop={20} width={CONST_WIDTH - 40} bgColor='#080880'  />
            : 
            <Button title='Submitting Request...' marginTop={20} width={CONST_WIDTH - 40} bgColor='#080880'  />
            }
    
            </View>
        }
    
  
    
      
      

        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                    <Header backButton ={true}  headerTitle = 'Add Vehicle' onPress={this.goBackButton} />

                    {content}


                    {/* MODEL */}
                    <MiddleModal modalName={this.state.modalMake} height="94%"           
                        children={
                            <View style={{flex:1}} > 
                                <Text style={{fontSize:20,marginBottom:15, fontFamily: "ProximaNova-Bold"}}>Select Vehicle Make</Text>
                                    <ScrollView showsVerticalScrollIndicator={false}>
                                    {content_make}
                                    </ScrollView>

                                    <Button 
                                    onPress={()=> this.setState({modalMake:false})}
                                    title='Close' marginTop={15} width={CONST_WIDTH - 70} bgColor='#080880'  />

                            </View>
                        } />

                    <MiddleModal modalName={this.state.modalCC} height='94%'
                        children={
                            <View style={{flex:1}} >
                                <Text style={{fontSize:20,marginBottom:15, fontFamily: "ProximaNova-Bold"}}>Select Vehicle CC</Text>
                                <ScrollView showsVerticalScrollIndicator={false} style={{ width:'100%',}}>
                                    {content_cc}
                                </ScrollView>

                                <Button 
                                    onPress={()=> this.setState({modalCC:false})}
                                    title='Close' marginTop={15} width={CONST_WIDTH - 70} bgColor='#080880'  />
                            </View>
                        } />


                    <MiddleModal modalName={this.state.modalYear} height="94%"
                    children={
                        <View style={{flex:1}} >
                            <Text style={{fontSize:20,marginBottom:15, fontFamily: "ProximaNova-Bold"}}>Select Vehicle Year</Text>
                            <ScrollView showsVerticalScrollIndicator={false} style={{ width:'100%',}}>
                                {content_year}
                            </ScrollView>

                            <Button 
                                onPress={()=> this.setState({modalYear:false})}
                                title='Close' marginTop={15} width={CONST_WIDTH - 70} bgColor='#080880'  />

                        </View>
                    } />

                    <MiddleModal modalName={this.state.modalRelation} height="94%"
                    children={
                        <View style={{flex:1}} >
                            <Text style={{fontSize:20,marginBottom:15, fontFamily: "ProximaNova-Bold"}}>Select Relationship</Text>
                            <ScrollView showsVerticalScrollIndicator={false} style={{ width:'100%',}}>
                                {content_relation}
                            </ScrollView>

                            <Button 
                                onPress={()=> this.setState({modalRelation:false})}
                                title='Close' marginTop={15} width={CONST_WIDTH - 70} bgColor='#080880'  />

                        </View>
                    } />

                    <MiddleModal modalName={this.state.modalReqshared}
                        children={
                            <View  style={{alignItems:'center', justifyContent:'center'}}>
                                <Text style={{fontSize:20, fontFamily: "ProximaNova-Bold", color:'#080880', textAlign:'center'}}>Request submitted!</Text>
                                <Text style={{fontSize:12, fontFamily: "ProximaNova-Bold", color:'#080880', textAlign:'center',marginTop:15}}>Please wait for the true owner to approve your request</Text>
                                <View>
                                    <Image source={Kar} style={{height:100, width:100}} />
                                    <Iocon name = "check-circle-outline" color="green" size={30} style={{position:'absolute', right:10, bottom:10}}/>
                                </View>

                                <Button 
                                    onPress={this.save}
                                    title='Continue' marginTop={15} width={CONST_WIDTH - 80} bgColor='#080880'  />

                            </View>
                        }/>

                    <MiddleModal modalName={this.state.modalReqOwner}
                        children={
                            <View  style={{alignItems:'center', justifyContent:'center'}}>
                                <Text style={{fontSize:20, fontFamily: "ProximaNova-Bold", color:'#080880', textAlign:'center'}}>Vehicle Registration Certificate uploaded!</Text>
                                <Text style={{fontSize:12, fontFamily: "ProximaNova-Bold", color:'#080880', textAlign:'center',marginTop:15}}>Please wait for the SERV team to verify and approve your ownership status</Text>
                                <View>
                                    <Image source={Kar} style={{height:100, width:100}} />
                                    <Iocon name = "check-circle-outline" color="green" size={30} style={{position:'absolute', right:10, bottom:10}}/>
                                </View>

                                <Button 
                                    onPress={this.save}
                                    title='Continue' marginTop={15} width={CONST_WIDTH - 80} bgColor='#080880'  />

                            </View>
                        }/>

                    <BottomModal modalName={this.state.modalLocateCC} 
                    style={styles.innerModal} onPress={() => this.setState({modalLocateCC:false})}
                        children= {
                            <View style={{padding:20}} >
                                <Text style={{fontSize:17, fontFamily: "ProximaNova-Bold", marginBottom:10, textAlign:'center', color:'#080880', margin:10}}>Where to locate your car's cubic centimeter (CC) ?</Text>
                                <ScrollView style={{padding:5}} >
                                    <View style={{flexDirection:'row', alignItems:'center', marginBottom:10}}>
                                    <View style={styles.option}>
                                        <Text style={{fontFamily:'ProximaNova-Bold'}}>Option 1</Text>
                                    </View>
                                    <Text style={{padding:5, fontSize:14,fontFamily:'ProximaNova-Bold', }}>Check your car manual </Text>
                                    </View>
                                    <View style={{flexDirection:'row', alignItems:'center', marginBottom:10,}}>
                                    <View style={styles.option}>
                                        <Text style={{fontFamily:'ProximaNova-Bold'}}>Option 2</Text>
                                    </View>
                                    <Text style={{padding:5, fontSize:14,fontFamily:'ProximaNova-Bold',}}>Check the back of your car</Text>
                                    </View>
                                    <View style={{flexDirection:'row', alignItems:'center', marginBottom:5,}}>
                                    <View style={styles.option}>
                                        <Text style={{ fontFamily:'ProximaNova-Bold'}}>Option 3</Text>
                                    </View>
                                    <Text style={{padding:5, fontSize:14,fontFamily:'ProximaNova-Bold',  flexShrink:1}}>Check Vehicle Ownership Certificate or Insurance Policy</Text>
                                    </View>
                                    <View style={{flexDirection:'row', alignItems:'center', marginBottom:5,}}>
                                    <View style={styles.option}>
                                        <Text style={{fontFamily:'ProximaNova-Bold'}}>Option 4</Text>
                                    </View>
                                    <Text style={{padding:5, fontSize:14,fontFamily:'ProximaNova-Bold', flexShrink:1}}>Refer your Vehicle Identififcation Number (VIN)</Text>
                                    </View>
                                    <View style={{flexDirection:'row', alignItems:'center', marginBottom:5,}}>
                                    <View style={styles.option}>
                                        <Text style={{fontFamily:'ProximaNova-Bold'}}>Option 5</Text>
                                    </View>
                                    <Text style={{padding:5, fontSize:14,fontFamily:'ProximaNova-Bold' , flexShrink:1}}>Under the front hood of your car</Text>
                                    </View>
                                    
                                    <Button 
                                    onPress={() => this.setState({modalLocateCC: false})}
                                    title='Close' marginTop={15} width={CONST_WIDTH - 60} bgColor='#080880'  />


                                  
                                </ScrollView>
                            </View>
                            
                        }
                    
                    /> 

                    <MiddleModal modalName={this.state.modalSuccess}
                        children={
                            <View style={{alignItems:'center', justifyContent:'center'}}>
                                <Text style={{fontSize:20, fontFamily: "ProximaNova-Bold", color:'#080880', textAlign:'center'}}>Congrats on owning your vehicle digitally!</Text>
                                <Text style={{fontSize:12, fontFamily: "ProximaNova-Bold", color:'#080880', textAlign:'center',marginTop:15}}>You have successfully added your vehicle</Text>
                                <View>
                                    <Image source={Kar} style={{height:100, width:100}} />
                                    <Iocon name = "check-circle-outline" color="green" size={30} style={{position:'absolute', right:10, bottom:10}}/>
                                </View>
                                <Button 
                                    onPress={this.save}
                                    title='Continue' marginTop={15} width={CONST_WIDTH - 80} bgColor='#080880'  />

                            </View>
                        } />

                    <BottomModal
                        modalName={this.state.modalPicture}
                        onPress={() => this.setState({modalPicture:false})}
                        style={styles.innerModal}
                        children = {
                        <View >
                            <View style={{padding:20, paddingBottom:0, alignItems:'center'}}>
                                <Text style={{textAlign:'center', fontFamily:'ProximaNova-Bold', fontSize:20, marginBottom:5}}>Update Photo</Text>             
                            </View>

                            <View style={{margin:30, marginTop:20,  alignItems:'center', justifyContent:'space-around', flexDirection:'row'}}>
                                <TouchableOpacity  onPress={this.openCamera} style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                                    <Image source={camera} style={{height:40, width:40}} />
                                    <Text style={{textAlign:'center'}}>Camera</Text>
                                </TouchableOpacity>
                            
                            
                                <TouchableOpacity  onPress={this.openLibrary} style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                                    <Image source={library} style={{height:40, width:40}} />
                                    <Text style={{textAlign:'center'}}>Library</Text>
                                </TouchableOpacity>

                            </View>
                        </View>
                        }
                    />

                </View>
            </SafeAreaView>
        )
    }
}
VehicleAdd = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(VehicleAdd))
export default VehicleAdd


const styles = StyleSheet.create({
    top:{
        backgroundColor:'#080880',
        padding:20,
        borderBottomLeftRadius:50,
        flexDirection:'row',
        alignItems:'center'
    },
    mainBox:{
        flexDirection:'row',zIndex:-8, width:CONST_WIDTH - 40,backgroundColor:'#fff',  marginTop:5,borderRadius:20,
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 2},
              shadowOpacity: 0.4,
            },
            android: {
              elevation: 3
            },
          }),
    },
    selected: {
        backgroundColor:'#080880', padding:15, width:(CONST_WIDTH) / 2, borderRadius:20,zIndex:8,flex:1,
       
    },
    notSelected: {backgroundColor:'#fff', padding:15,width:(CONST_WIDTH - 40) / 2, borderRadius:20,zIndex:-8,
        },
    textS: {color:'#fff', textAlign:'center', fontSize:16, fontFamily:'ProximaNova-Bold'},
    textNS:{textAlign:'center', fontSize:16},
    option:{backgroundColor:'#fff', padding:5, paddingLeft:20, paddingRight:20, borderRadius:30, marginRight:15,
        ...Platform.select({
        ios: {
            shadowColor: '#000',
            shadowRadius: 3,
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.6,
        },
        android: {
            elevation: 3
        },
        }), 
    },
    innerModal: {     
        borderTopLeftRadius: 10,
        borderTopRightRadius:10,
        width:Dimensions.get('window').width,
        position:'absolute',
        bottom:0,
        backgroundColor: '#fff',
            ...Platform.select({
                ios: {
                    shadowColor: '#000',
                    shadowRadius: 2,
                    shadowOffset: { width: 0, height: 3 },
                    shadowOpacity: 0.6,
                },
                android: {
                    elevation: 2
                },
            }),
    },
    circle : {height: 70, width:70, borderRadius: 40, borderWidth:0.5, borderColor:'#e8e8e8',alignItems:'center', justifyContent:'center'},
    selectedCircle : {height: 70, width:70, borderRadius: 40, borderWidth:2, borderColor:'#080880', alignItems:'center', justifyContent:'center'},
    greycircle : {height: 70, width:70, borderRadius: 35, borderWidth:0.5, borderColor:'#f5f5f5',backgroundColor:'#f5f5f5', alignItems:'center', justifyContent:'center'},
    iconVehicle :{height: 54, width:54},
    vehName :{fontFamily: "ProximaNova-Bold", marginTop:15, textAlign:'center'}, 
    vehGrey :{fontFamily: "ProximaNova-Bold", marginTop:15,color: '#e8e8e8', textAlign:'center'}, 



})