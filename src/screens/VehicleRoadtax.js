import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert, Image,Dimensions,Switch,ActivityIndicator, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import FontAwesome from "react-native-vector-icons/FontAwesome";
import MaterialIcon from "react-native-vector-icons/MaterialIcons";

import ImagePicker from 'react-native-image-picker';
import Moment from 'moment';
import Calendar from 'react-native-calendar-datepicker';

import Header from '../components/Header.js'
import BottomModal from "../components/BottomModal.js"

type State = {
    date?: Moment,
  };
  
class VehicleRoadtax extends React.Component{
    state: State
    constructor(){
        super()
        this.state = {
            img: '',
            modalPicture: false,
            roadtaxNoti: false,
            roadtaxDate: '',
            uploading: false,
            showCalendar: true,
        }
    }
    
    componentDidMount(){
        this.fetchRoadTax()
    }
    
    fetchRoadTax(){
        let that = this
        let PLATE = this.props.mobx_carkey.PLATE
        let FUID = this.props.mobx_auth.FUID;
        
        let DATE = this.props.navigation.state.params.roadtaxDate
        let replaced = DATE.split('-').join(' ');

        this.setState({roadtaxDate: replaced})

        firebase.database().ref(`plate_number/${PLATE}/roadtax`).once('value', (snp) => {
            if(snp.exists()){
                let x = snp.val().image
    
                    that.setState({ img: x })
            } else {
                //no radtax
            }
        }) 

        firebase.database().ref(`users/${FUID}/notification/road_tax`).once('value', (snap) => {
            if(snap.exists()){
                that.setState({roadtaxNoti: snap.val()})
            }
        } )
    }

    update = () => {
        let PLATE = this.props.mobx_carkey.PLATE
        let IMAGE =  this.state.image

        let DATA = {
            image: IMAGE,
            timestamp: Date.now()
        }

        firebase.database().ref(`plate_number/${PLATE}/roadtax`).update(DATA)

    }

    updateDate = () => {
        let PLATE = this.props.mobx_carkey.PLATE;

        let SDATE = Moment(this.state.date).format('DD-MMMM-YYYY')

        firebase.database().ref(`plate_number/${PLATE}/car_health`).update({roadtax_expiry_date : SDATE})
    }

    // ===== REMINDER ==== 
    toggleSwitch = (value) =>{
        let FUID = this.props.mobx_auth.FUID;
        
       
        if(this.state.roadtaxDate === ''){
            this.setState({roadtaxNoti: false})
            Alert.alert(' Sorry we can\'t remind you yet. ', 'It seems you haven\'t updated your road tax expiration date yet.')
      
        } else {
            this.setState({roadtaxNoti: value})
            firebase.database().ref(`users/${FUID}/notifications`).update({road_tax: value})
            if(value === true){ 
                this.setReminder()
            }
        }
    
        
      }

      
    setReminder = () => {

        let FUID = this.props.mobx_auth.FUID
        let DATE = Moment(this.state.date).format('DD-MM-YYYY')

        let MONTH = (DATE.split('-')[1]) - 1
        let FDAY
        let DAY = (DATE.split('-')[0])
        if (DAY < 10 ){FDAY =  DAY[1]} else {FDAY = DAY}
        
        let YEAR = DATE.split('-')[2]
        let P_TOKEN
        
        firebase.database().ref(`users/${FUID}`).once('value').then((snapshot) => {
        if(snapshot.exists()){
            P_TOKEN = snapshot.val().pushToken
  
            firebase.database().ref( `reminder_roadtax/${YEAR}/${MONTH}/${FDAY}/${FUID}`).update({
                pushToken: P_TOKEN,
                plate: this.props.mobx_carkey.PLATE,
            })
        
        }
        })
    }

    dateChanged = (date) => {
        let M = Moment(date).format('DD MMMM YYYY')
        this.setState({date, roadtaxDate: M})
    }

    // ====== UPLOAD IMG ====
    openCamera = () => {
        const options = {quality:0.5, maxWidth:500, maxHeight:500 }
    
        ImagePicker.launchCamera(options, (response) => {
          this.setState({modalPicture: false})
    
          if(response.didCancel){
            // console.warn('kenapa cancel');
            this.setState({modalPicture: false})
          }
           if (response.error) {
            Alert.alert('Sorry', "Please try again");
            this.setState({modalPicture: false})
          } if(!response.didCancel && !response.error) {
            this.setState({
              img: response.uri,
              modalPicture: false,
            });
            this.uploadImg()
    
          }
        });
    }
      
    openLibrary = () => {
        const options = {quality:0.5, maxWidth:500, maxHeight:500 }
    
        ImagePicker.launchImageLibrary(options, (response) => {
            this.setState({modalPicture: false})
            
            if(response.didCancel){
              // console.warn('kenapa cancel');
              this.setState({modalPicture: false})
            } 
            if(!response.didCancel && !response.error) {
              this.setState({
                img: response.uri,
                modalPicture: false,
              });
              this.uploadImg()
            }
    
             
        });
    
    }
    
    uploadImg = async() => {
        this.setState({ uploading:true, })
        let FUID = this.props.mobx_auth.FUID;
        let PLATE = this.props.mobx_carkey.PLATE;
    
        const imageuri= this.state.img;
        const refFile= await firebase.storage().ref().child(PLATE).child('roadtax')
    
        refFile.putFile(imageuri).then(url => {
            refFile.getDownloadURL().then((url) => {
                let DOWN = url
    
                this.setState({img: DOWN, uploading: false})
                firebase.database().ref(`plate_number/${PLATE}/roadtax`).update({
                    image: DOWN,
                    timestamp: Date.now()
                });
        
                Alert.alert('Success', "Your roadtax has been updated!");
            })
           
            //  this.props.navigation.goBack();
        }).catch(error => {
              // console.warn(error.message);
            Alert.alert('Sorry', 'An error occured. Please contact SERV Team if the issue still persists');
        });
    
      }

    render(){
        const BLUE = '#080880';
        const WHITE = '#FFFFFF';
        const GREY = '#BDBDBD';
        const BLACK = '#424242';
        const LIGHT_GREY = '#fafdff';

        let renderImg 
        if(this.state.uploading === true){
            renderImg = 
            <View style={{...styles.img, alignItems:'center', justifyContent:'center'}} >
                <ActivityIndicator 
                    color = '#080880'
                    size = 'large'
                />
            </View>
        } else {
            if(this.state.img !== ''){
                renderImg = <Image source={{uri: this.state.img}} style={styles.img} />
            } else {
                renderImg = <View style={styles.img} />
            }
        }



        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Road Tax' onPress={() => this.props.navigation.goBack()} />
                
                    <ScrollView style={{padding: 20,}} showsVerticalScrollIndicator={false} >
                        <View style={{marginTop:15, marginBottom: 15}} >
                            <View style={{flexDirection:'row', justifyContent:'space-between', marginBottom:15}} >
                                <View style={{height:36, width: 36,backgroundColor:'#fafdff'}} />
                                
                                <View style={{flex:2, marginRight: 15, }}>
                                    <Text style={{fontFamily: "ProximaNova-Bold",textAlign:'center', fontSize:15}}>Expiry Date</Text>
                                    <Text style={{fontFamily: "ProximaNova-Bold",textAlign:'center', marginTop:15,color:"#080880", fontSize:20}}>{this.state.roadtaxDate}</Text>
                                </View>
                                <TouchableWithoutFeedback onPress={() => this.setState({showCalendar: !this.state.showCalendar})} >
                                <View style={{borderWidth:1,alignSelf:'flex-end', borderColor:'#979797', height:36, width: 36, borderRadius:18, alignItems:'center', justifyContent:'center'}}>
                                    <FontAwesome name='pencil' size={18} color='#979797' />
                                </View>
                                </TouchableWithoutFeedback>
                            </View>

                            {!this.state.showCalendar ? 
                            <View style={styles.box}>
                            <Calendar
                                onChange={this.dateChanged}
                                selected={this.state.date}
                                minDate={Moment().startOf('day')}
                                maxDate={Moment().add(20, 'years').startOf('day')}
                                style={{
                                    borderRadius: 5,
                                    alignSelf: 'center',
                                    marginLeft:50,
                                    marginRight:50,
                                    marginTop:10,
                                    marginBottom:15,
                                    width:'100%'
                                }}
                                barView={{
                                    padding: 15,

                                }}
                                barText={{
                                    fontFamily: "ProximaNova-Bold",
                                    color: '#979797',
                                    fontSize:16
                                }}
                                stageView={{
                                    padding: 0,
                                }}

                                // Day selector styling
                                dayHeaderView={{
                                    borderBottomColor: WHITE,
                                }}
                                dayHeaderText={{
                                    fontFamily: "ProximaNova-Bold",
                                    color: BLACK,
                                }}
                                dayRowView={{
                                    borderColor: WHITE,
                                    height: 40,
                                }}
                                dayText={{
                                    color: BLACK,
                                }}
                                dayDisabledText={{
                                    color: GREY,
                                }}
                                dayTodayText={{
                                    fontFamily: "ProximaNova-Bold",
                                    color: BLUE,
                                }}
                                daySelectedText={{
                                    fontFamily: "ProximaNova-Bold",
                                    backgroundColor: BLUE,
                                    color: WHITE,
                                    borderRadius: 15,
                                    borderColor: WHITE,
                                    overflow: 'hidden',
                                }}
                                // Styling month selector.
                                monthText={{
                                    color: BLACK,
                                    borderColor: BLACK,
                                }}
                                monthDisabledText={{
                                    color: GREY,
                                    borderColor: GREY,
                                }}
                                monthSelectedText={{
                                    fontFamily: "ProximaNova-Bold",
                                    backgroundColor: '#979797',
                                    color: WHITE,
                                    overflow: 'hidden',
                                }}
                                yearMinTintColor={BLUE}
                                yearMaxTintColor={GREY}
                                yearText={{
                                    color: BLACK,
                                }}
                                />
                            </View>
                            : null}

                        </View>

                        <View style={styles.box}>
                            <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between'}} >
                                <View style={{width:'70%', marginRight:25}} >
                                    <View style={{borderBottomWidth: 0.3, color:'#dddddd', padding:14}}>
                                        <Text style={{fontFamily: "ProximaNova-Bold", textAlign:'center', color:"#080880", fontSize:16}} >Expiry Reminder</Text>
                                    </View>
                                    <Text style={{fontSize:14, textAlign:'center', marginTop:14, marginBottom:10}} >Remind me to renew my road tax</Text>
                                </View>

                                <Switch
                                    onValueChange = {this.toggleSwitch}
                                    style={{paddingRight: 10, marginTop:5}}
                                    trackColor={{true: "#080880", false: null}}
                                    ios_backgroundColor={{true:'#ccc'}}
                                    value = {this.state.roadtaxNoti}
                                    />

                            </View>
                        </View>

                        <View style={styles.box}>
                            {renderImg}
                            <TouchableWithoutFeedback onPress={() => this.setState({modalPicture:true})} >
                            <View style={{padding:5, alignItems:'center', justifyContent:'center'}}>
                                <MaterialIcon name= 'photo-camera'  size={30} color="#080880" />
                            </View>
                            </TouchableWithoutFeedback>

                        </View>

                        <View style={{height: 25}} />

                       
                    </ScrollView>
                    <BottomModal
                        modalName={this.state.modalPicture}
                        onPress={() => this.setState({modalPicture:false})}
                        style={styles.innerModal}
                        children = {
                        <View >
                            <View style={{padding:20, paddingBottom:0, alignItems:'center'}}>
                                <Text style={{textAlign:'center', fontFamily:'ProximaNova-Bold', fontSize:20, marginBottom:5}}>Update Photo</Text>             
                            </View>

                            <View style={{margin:30, marginTop:20,  alignItems:'center', justifyContent:'space-around', flexDirection:'row'}}>
                                <TouchableOpacity  onPress={this.openCamera} style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                                    <MaterialIcon name= 'photo-camera'  size={40}/>
                                    <Text style={{textAlign:'center'}}>Camera</Text>
                                </TouchableOpacity>
                            
                            
                                <TouchableOpacity  onPress={this.openLibrary} style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                                    <MaterialIcon name='photo-library' size={40}/>
                                    <Text style={{textAlign:'center'}}>Library</Text>
                                </TouchableOpacity>

                            </View>
                        </View>
                        }
                    />
                    

                </View>
            </SafeAreaView>
        )
    }
}

VehicleRoadtax = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(VehicleRoadtax))
export default VehicleRoadtax

const styles = StyleSheet.create({
    box :{
        backgroundColor:'#fff',
        borderRadius:15,
        marginBottom:15,
        padding:15,
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 3,
              shadowOffset: { width: 0, height: 3 },
              shadowOpacity: 0.3,
            },
            android: {
              elevation: 3
            },
          }),
    },
    img: {
        height: Dimensions.get('window').width / 2,
        width: Dimensions.get('window').width - 70,
        marginBottom:15,
        resizeMode: 'cover' ,
    },
    innerModal: {     
        borderTopLeftRadius: 10,
        borderTopRightRadius:10,
        width:Dimensions.get('window').width,
        position:'absolute',
        bottom:0,
        backgroundColor: '#fff',
            ...Platform.select({
                ios: {
                    shadowColor: '#000',
                    shadowRadius: 2,
                    shadowOffset: { width: 0, height: 3 },
                    shadowOpacity: 0.6,
                },
                android: {
                    elevation: 2
                },
            }),
      },
})