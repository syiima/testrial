import React, { Component } from 'react';
import {
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  SafeAreaView,
  Image,
  StatusBar,
  StyleSheet,
  Platform
} from 'react-native';
import { inject, observer } from 'mobx-react';

const { width, height } = Dimensions.get('window');

import logOption from '../assets/logOption.png';
import Button from '../components/Button.js'
import BorderButton from '../components/BorderButton.js'

import servS from '../assets/logo/1024.png'
import optionlog from '../assets/logo/optionlog.png'

const WIDTH = Dimensions.get('window').width - 40

class LogOption extends Component {
  render() {
    return (
        <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
            <View style={{backgroundColor:'#fafdff', flex:1}}>
            <StatusBar barStyle="light-content" backgroundColor='#1d1d1d'/>
              <View style={{alignItems:'center',flex:1, justifyContent:'center',marginBottom:WIDTH/2 }}>
                <Image source={servS}  style={styles.logo} />
                  <View style={{margin:15, flexShrink:1, alignItems:'center', justifyContent:'center'}}>
                    <Text style={{fontFamily: "ProximaNova-Bold", color:'#080880', fontSize:22, marginBottom:10}}>Welcome to SERV</Text>
                    <Text style={{color:'#080880', fontFamily:'ProximaNova-Bold', fontSize:16}}>Vehicle Ownership Experience, Simplified.</Text>
                  </View>

                <Image source={optionlog} style={{height:Dimensions.get('window').height / 3, width:WIDTH - 60,resizeMode:'contain', alignSelf:'center'}} />
               
              </View>


              <View style={styles.positon}>
                <Text style={{color:'#080880', fontFamily:'ProximaNova-Bold', fontSize:16, }}>Start tracking your vehicles today!</Text>
                <Button title='Log in' onPress={() => this.props.navigation.navigate('LoginScreen')} bgColor='#080880' width= {WIDTH} marginTop={15} />
                <BorderButton onPress={() => this.props.navigation.navigate('RegisterScreen')} title='Sign up' borderColor='#080880' width= {WIDTH} marginTop={15} />
              </View>
              
            </View>
            
        </SafeAreaView>

    );
  }
}

LogOption = inject('mobx_auth',)(observer(LogOption))
export default LogOption


const styles = StyleSheet.create({
    positon:{ position:'absolute', bottom:20, alignSelf:'center'},
    logo :{
      height: Dimensions.get('window').width / 8,
      width :Dimensions.get('window').width / 2.5,
      resizeMode: 'contain',
      alignSelf:'center'
    },

   
})