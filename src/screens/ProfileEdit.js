import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,RefreshControl,KeyboardAvoidingView, ActivityIndicator, Alert,Dimensions, TouchableWithoutFeedback, TouchableOpacity, ScrollView, Image, Button, TextInput, Linking} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/AntDesign"
import MaterialIcon from "react-native-vector-icons/MaterialIcons";
import Moment from 'moment';

import DatePicker from 'react-native-datepicker'
import Calendar from 'react-native-calendar-datepicker';

import FormTextInput from "../components/FormTextInput"
import Header from "../components/Header"
import RoundedBorderButton from "../components/RoundedBorderButton"
import BottomModal from "../components/BottomModal.js"
import profile from '../assets/emptyProfile.png'
import PickerTextInput  from "../components/PickerTextInput.js"
import MiddleModal from "../components/MiddleModal"

type State = {
    date?: Moment,
  };
  

class ProfileEdit extends React.Component{
    state: State

    constructor(){
        super()
        this.state = {
            name:'',
            dob: '',
            bio: '',
            gender: 'Select gender',
            uploading:false,
            new_img: '',
            modalPicture: false,
            uploadingImg: false,
            modalGender: false,
            show: false,
            modalDOB: false,
            
        }
    }

    componentDidMount(){
        this.setInfos()
    }

    setInfos(){
        let DATA = this.props.navigation.state.params.data
        let DOB = DATA.DOB !== undefined && DATA.DOB !== '' ? DATA.DOB : 'Select birth date'
        // let FDOB
        // if(DOB !== undefined){
        //     FDOB = Moment(DOB).format('YYYY-MM-DD') + 'T03:00:00.000Z'
        // } else {
        //     FDOB = new Date()
        // }
        
        this.setState({
            name: DATA.name,
            dob: DOB,
            bio: DATA.bio !== undefined ? DATA.bio : '',
            gender: DATA.gender !== undefined && DATA.gender !== '' ? DATA.gender : 'Select gender',

        })
    }

    savedata = () => {
        let FUID = this.props.mobx_auth.FUID 

        let NAME = this.state.name 
        let DOB = this.state.dob 
        let BIO = this.state.bio
        let GENDER = this.state.gender

        if(NAME === ''){
            Alert.alert('Sorry', 'Name cannot be empty')
            return 
        }

        if(GENDER === 'Select gender'){
            GENDER = ''
        }
        if(DOB === 'Select birth date'){ DOB = ''}

        if(NAME !== ''){ this.setState({uploading: true})}


        firebase.database().ref(`users/${FUID}`).update({
            name: NAME,
            DOB:DOB,
            bio: BIO,
            gender: GENDER,
        })

        this.props.navigation.state.params.refresh_profile()
        this.props.navigation.goBack(null)

    }

    saveDOB = () => {
        let X = this.state.date
        let DATE = Moment(X).format('DD-MMMM-YYYY')

        this.setState({dob: DATE, modalDOB:false})
    }

    // onChange = (date) => {
    //     let currentDate = date || this.state.dob;

    //     console.warn('hmmm', currentDate);
    //     this.setState({
    //         show: false,
    //         dob: currentDate,
    //       });
    // }


    render(){
        const BLUE = '#080880';
        const WHITE = '#FFFFFF';
        const GREY = '#BDBDBD';
        const BLACK = '#424242';
        const LIGHT_GREY = '#fafdff';

        let saveButton 

        if(this.state.uploading === false){
            saveButton = 
            <RoundedBorderButton onPress={this.savedata} 
                title='Save'
                borderColor='#080880' marginTop={20} />
        } else {
            saveButton = 
            <RoundedBorderButton borderColor='#080880' marginTop={20} title='Updating ... ' />
        }

        let GENDER = ["Male","Female"];
        let content_gender = []
        for(var i = 0; i < GENDER.length; i++){
            let value = GENDER[i];
            content_gender.push(
                <View >
                    <TouchableOpacity 
                        style={(i === GENDER.length - 1) ? styles.noBorderText : styles.borderText}
                        onPress={()=> this.setState({gender:value,modalGender:false})}>
                    <Text style={{fontSize:16}}>{value}</Text>
                    </TouchableOpacity>
                </View>
            )
        }


        let bday 

        if(Platform.OS === 'android'){
            bday = 
            <View style={{marginBottom:20}} >
                <Text style={{fontWeight:'bold', fontSize:13,marginBottom:10, color:'#6e6d6d' }}>DATE OF BIRTH</Text>
                <View style={{paddingLeft:10, borderWidth:1, borderColor:'#ccc',borderRadius:5 }}>
                
                <DatePicker
                    date={this.state.dob}
                    mode="date"
                    placeholder="Your Date of Birth"
                    format="DD-MMMM-YYYY"
                    minDate="01-01-1950"
                    maxDate="31-12-2050"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    showIcon={false}
                    customStyles={{
                        dateTouchBody: { width:Dimensions.get('window').width /2 + 14,},
                        dateInput: { alignItems:'flex-start', justifyContent:'center', borderColor:'white', borderWidth:0,},
                        dateText: {color:'black'}
                    
                    // ... You can check the source to find the other keys.
                    }}
                    onDateChange={(date) => {this.setState({dob: date})}}
                />
                </View>
                <Text style={{color:'#686868', fontSize:12, marginTop:5}}>Please tap on the year/month to change year/month.</Text>

            </View>
        } else {
            bday = 
            <PickerTextInput 
                title= 'DATE OF BIRTH'
                value={this.state.dob} 
                onPress={() => this.setState({modalDOB:true})} 
            /> 

        }

        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Edit Profile' onPress={() => this.props.navigation.goBack()} />
                <KeyboardAvoidingView
                    behavior={(Platform.OS === 'ios') ? "padding" : null} enabled
                    style={{ flex: 1,}}
                >
                    <ScrollView style={{flex:1,backgroundColor:'white',padding:20, }} showsVerticalScrollIndicator={false} >


                        <FormTextInput 
                            title= 'FULL NAME'
                            value={this.state.name}
                            placeholder= 'Your name'
                            onChangeText={(x) => this.setState({name:x})}
                        />

                        
                        {bday}


                        <MiddleModal modalName={this.state.modalDOB}
                        children = {
                            <View>
                                <Text style={{fontSize:17, marginBottom:15, marginTop:20, fontFamily: "ProximaNova-Bold", textAlign:'center', color:'#080880'}}>Date of birthday</Text>
                  
                                <View style={{padding:10, paddingLeft: 10, marginBottom:19, borderRadius: 6,borderColor:'#080880', borderWidth:2}}>
                                    <Calendar
                                    onChange={(date) => this.setState({date})}
                                    selected={this.state.date}
                                    minDate={Moment().subtract(60, 'years').startOf('day')}
                                    maxDate={Moment().add(2, 'years').startOf('day')}
                                    //General Styling}
                                    style={{
                                    borderRadius: 5,
                                    alignSelf: 'center',
                                    marginLeft:50,
                                    marginRight:50,
                                    marginTop:10,
                                    marginBottom:15,
                                    width:'100%'
                                    }}
                                    barView={{
                                    padding: 15,

                                    }}
                                    barText={{
                                    fontFamily: "ProximaNova-Bold",
                                    color: BLUE,
                                    fontSize:16
                                    }}
                                    stageView={{
                                    padding: 0,
                                    }}

                                    // Day selector styling
                                    dayHeaderView={{
                                    borderBottomColor: WHITE,
                                    }}
                                    dayHeaderText={{
                                    fontFamily: "ProximaNova-Bold",
                                    color: GREY,
                                    }}
                                    dayRowView={{
                                    borderColor: LIGHT_GREY,
                                    height: 40,
                                    }}
                                    dayText={{
                                    color: BLACK,
                                    }}
                                    dayDisabledText={{
                                    color: GREY,
                                    }}
                                    dayTodayText={{
                                    fontFamily: "ProximaNova-Bold",
                                    color: BLUE,
                                    }}
                                    daySelectedText={{
                                    fontFamily: "ProximaNova-Bold",
                                    backgroundColor: BLUE,
                                    color: WHITE,
                                    borderRadius: 15,
                                    borderColor: "transparent",
                                    overflow: 'hidden',
                                    }}
                                    // Styling month selector.
                                    monthText={{
                                    color: BLACK,
                                    borderColor: BLACK,
                                    }}
                                    monthDisabledText={{
                                    color: GREY,
                                    borderColor: GREY,
                                    }}
                                    monthSelectedText={{
                                    fontFamily: "ProximaNova-Bold",
                                    backgroundColor: BLUE,
                                    color: WHITE,
                                    overflow: 'hidden',
                                    }}
                                    // Styling year selector.
                                    yearMinTintColor={BLUE}
                                    yearMaxTintColor={GREY}
                                    yearText={{
                                    color: BLACK,
                                    }}
                                    />

                                
                                    
                                    
                                </View>
                                <TouchableOpacity onPress={this.saveDOB} style={{backgroundColor:'#080880', borderRadius:10, padding:15, paddingLeft:20, paddingRight:20,  alignItems:'center', marginBottom: 10}}>
                                    <Text style={{color:'white', fontFamily: "ProximaNova-Bold"}}>Continue</Text>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => this.setState({modalDOB:false})} style={{backgroundColor:'#fff', borderWidth:1, borderColor:'grey', borderRadius:10, padding:15, paddingLeft:20, paddingRight:20,  alignItems:'center'}}>
                                    <Text style={{color:'grey', fontFamily: "ProximaNova-Bold"}}>Cancel</Text>
                                </TouchableOpacity>
                                                
                            </View>
                        }
                        
                    />




                        <PickerTextInput 
                            title= 'GENDER'
                            value={this.state.gender} 
                            onPress={() => this.setState({modalGender:true})} 
                        /> 

                        <View style={{marginBottom:20}} >
                            <Text style={{fontWeight:'bold', fontSize:13,marginBottom:10, color:'#6e6d6d' }}>SHORT BIO</Text>
                            <View style={{padding:(Platform.OS === 'ios') ? 10 : 0, borderWidth:1, borderColor:'#ccc',borderRadius:5 }}>
                                <TextInput 
                                    placeholder='eg: I am inevitable'
                                    placeholderTextColor= "#757575"
                                    autoCorrect={false}
                                    autoCapitalize = "none" 
                                    underlineColorAndroid="transparent" 
                                    maxLength={50}
                                    onChangeText={(x) => this.setState({bio:x})}
                                    value={this.state.bio}
                                    style={{fontFamily:'ProximaNova-Regular'}}

                                />
                            </View>
                            <Text style={{color:'#6e6d6d', fontSize:12, marginTop:5}}>Maximum 35 characters.</Text>
                            
                        </View>
                        
                        {saveButton}


                        <View style={{height:25}} />

                    </ScrollView>

                    <BottomModal
                        modalName={this.state.modalPicture}
                        onPress={() => this.setState({modalPicture:false})}
                        style={styles.innerModal}
                        children = {
                        <View >
                            <View style={{padding:20, paddingBottom:0, alignItems:'center'}}>
                                <Text style={{textAlign:'center', fontFamily:'ProximaNova-Bold', fontSize:20, marginBottom:5}}>Update Photo</Text>             
                            </View>

                            <View style={{margin:30, marginTop:20,  alignItems:'center', justifyContent:'space-around', flexDirection:'row'}}>
                                <TouchableOpacity  onPress={this.openCamera} style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                                    <MaterialIcon name= 'photo-camera'  size={40}/>
                                    <Text style={{textAlign:'center'}}>Camera</Text>
                                </TouchableOpacity>
                            
                            
                                <TouchableOpacity  onPress={this.openLibrary} style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                                    <MaterialIcon name='photo-library' size={40}/>
                                    <Text style={{textAlign:'center'}}>Library</Text>
                                </TouchableOpacity>

                            </View>
                        </View>
                        }
                    />

                    <MiddleModal modalName={this.state.modalGender}
                        children = {
                            <View>
                                <Text style={{fontSize:20,marginBottom:15, fontFamily: "ProximaNova-Bold"}}>Select Gender</Text>
                                    
                                {content_gender}
                                    
                                <TouchableOpacity style={styles.button} onPress={()=> this.setState({modalGender:false})}>
                                        <Text style={styles.text} >Close</Text>
                                    </TouchableOpacity>
                            </View>
                        }
                    />
                    </KeyboardAvoidingView>
                </View>
            </SafeAreaView>
        )
    }
}

ProfileEdit = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(ProfileEdit))
export default ProfileEdit

const styles = StyleSheet.create({
    boxText:{marginBottom:15, flexDirection:'row', alignItems:'center'},
    leftSide:{width:Dimensions.get('window').width /4.3, marginRight:10},
    image:{ 
        height:Dimensions.get('window').width/2,width:Dimensions.get('window').width - 40, resizeMode:'cover', alignSelf:'center', marginBottom:25,borderRadius: 10,
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowRadius: 1,
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.4,
            },
            android: {
                elevation: 2
            },
        }),
    },
    button:{
        borderRadius:10,
        width:Dimensions.get('window').width - 40,
        alignItems:'center',
        marginTop:35,
        borderWidth:2,
        borderColor:'#080880',
        borderRadius:30,
        padding:12,
    }, 
    text:{
        fontFamily:'ProximaNova-Bold', 
        fontSize:18, 
        color:'#080880', 
        textAlign:'center'
    },
    innerModal: {     
        borderTopLeftRadius: 10,
        borderTopRightRadius:10,
        width:Dimensions.get('window').width,
        position:'absolute',
        bottom:0,
        backgroundColor: '#fff',
            ...Platform.select({
                ios: {
                    shadowColor: '#000',
                    shadowRadius: 2,
                    shadowOffset: { width: 0, height: 3 },
                    shadowOpacity: 0.6,
                },
                android: {
                    elevation: 2
                },
            }),
    },
    button :{
        marginTop:15,
        borderRadius:10,
        padding:15,
        backgroundColor:'#080880',
        width: Dimensions.get('window').width - 70,
    },
    text :{
        fontFamily:'ProximaNova-Bold', 
        fontSize:18, 
        color:'white', 
        textAlign:'center'
    },
    borderText: {padding:12, borderBottomColor:'#ccc', borderBottomWidth:0.5},
    noBorderText: {padding:12},
    cam : {
        height:40, width:40, alignItems:'center',backgroundColor:'#fff', borderRadius:20, justifyContent:'center',
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowRadius: 1,
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.4,
            },
            android: {
                elevation: 2
            },
        }),
    },

})