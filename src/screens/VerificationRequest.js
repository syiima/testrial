import React from "react"
import {StyleSheet, Text, View, Image,SafeAreaView,KeyboardAvoidingView,Platform,  TouchableOpacity, Dimensions,ScrollView,TextInput,Alert, TouchableWithoutFeedback} from 'react-native';
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {cloud_serv_user_sandbox, cloud_serv_user_production} from "../util/FirebaseCloudFunctions.js"

import Header from '../components/Header.js'
import FormTextInput from "../components/FormTextInput"

import Moment from 'moment';
import shared from '../assets/icons/coowner.png'
import no_photo from '../assets/empty_profile.png'

class VerificationRequest extends React.Component{
    constructor(){
        super()
        this.state = {
            reject_reason: '' ,
            buttonview:1,
            tokenU:'',
            secretU: '',
            selected: '',
            page:1,
            loading:false,  
        }
    }

    componentDidMount(){
        this.getToken()
    }

    getToken = async() => {
        let tokenU =  await AsyncStorage.getItem('CSRFTokenU')
        let secretU =  await AsyncStorage.getItem('CSRFSecretU')
    
        this.setState({tokenU : tokenU, secretU : secretU})
    
    }

    // ======= func ==========

    rejectRequest = () => {
        
        let REASON = this.state.reject_reason
        let FUID = this.props.mobx_auth.FUID
        let NAME = this.props.mobx_auth.NAME
        let PLATE = this.props.mobx_carkey.PLATE

        if(REASON === ''){
            Alert.alert('Sorry', 'Please type in your reason for declining the request. Thank you!')
            return
        } else {
            this.setState({loading: true})
            let SHARER = this.state.selected.uid
            firebase.database().ref(`plate_number_request/${PLATE}/users/${SHARER}/`).update({
                status: 'reject',
                reason: REASON,
                timestamp_reject : Date.now(),
                rejected_by: {
                    FUID: FUID,
                    name :NAME
                }
            })
    
            firebase.database().ref(`users/${SHARER}/vehicles/${PLATE}`).update({status:'reject'})
    
            this.sendNoti('reject')
        }
        
    }

    approveRequest = () => {
        this.setState({loading: true})
        let PLATE = this.props.mobx_carkey.PLATE
        let SHARER = this.state.selected.uid
        let data = this.state.selected
        let FUID = this.props.mobx_auth.FUID
        let NAME = this.props.mobx_auth.NAME

        firebase.database().ref(`users/${SHARER}/vehicles/${PLATE}`).update({status: 'approved'})
        firebase.database().ref(`plate_number_request/${PLATE}/users/${SHARER}`).update({
            status: 'approved',
            timestamp_approved: Date.now(),
            approved_by: {
                FUID: FUID,
                name :NAME
            }
        })
        firebase.database().ref(`plate_number/${PLATE}/user/${SHARER}`).update({
            name: data.name,
            relationship: data.relationship,
            phone: data.phone,
            timestamp_approved: Date.now(),
        })

        this.sendNoti('approve')
        
    }

    sendNoti = (x) => {
        let PLATE = this.props.mobx_carkey.PLATE
        let FUID = this.props.mobx_auth.FUID
        let SHARER = this.state.selected.uid
        
        let URL, TEXT, BODY, Q_FUID, Q_VEHICLE_ID
        
        if (this.props.mobx_config.config === 0) {URL = cloud_serv_user_sandbox.personalNotifications}
        if (this.props.mobx_config.config === 1) {URL = cloud_serv_user_production.personalNotifications}
        
        if(x === 'approve'){
            TEXT = '?text=Co-owner request approved'
            BODY = `&body=The true owner for the vehicle ${PLATE} has approved your request for the vehicle.	`  
        } else {
            TEXT = '?text=Co-owner request rejected'
            BODY = `&body=The true owner for the vehicle ${PLATE} has rejected your request for the vehicle.	`
        
        }
        
        Q_FUID = `&userKey=${SHARER}`
        Q_VEHICLE_ID = `&vehicle_id=${PLATE}`
        
        FINAL_URL = URL + TEXT + BODY + Q_FUID + Q_VEHICLE_ID
        
        console.warn('finalURL', FINAL_URL);
        fetch(FINAL_URL,{
            credentials:true, 
            headers: {
            csrftoken: this.state.tokenU,
            csrfsecret: this.state.secretU
            }
        }).then(() => {
            this.setState({loading: false})
            this.props.navigation.state.params.refresh_profile()
            this.props.navigation.goBack()
        
        })
        
    }

    renderView = () => {
        let disp, button 
        let DATA = this.props.navigation.state.params.data 
        let ITEM = this.state.selected 
        let PLATE = this.props.mobx_carkey.PLATE

        if(this.state.buttonview === 1){
            button = 
            <View style={{ marginTop: 40}}>
                <TouchableOpacity style={styles.buton} onPress={this.approveRequest} >
                    <Text style={{color:'#fff', fontSize:18, fontFamily:'ProximaNova-Bold'}}>Approve</Text>
                </TouchableOpacity>
                
                <TouchableOpacity style={styles.butonNot} onPress={() => this.setState({buttonview: 2})}>
                    <Text style={{color:'tomato', fontSize:18, fontFamily:'ProximaNova-Bold'}}>Reject</Text>
                </TouchableOpacity>
                
            </View>
        } else{
            button = 
            <View style={{ marginTop: 40}}>
                <TouchableOpacity style={{...styles.buton, backgroundColor: 'tomato'}} onPress={this.rejectRequest} >
                    <Text style={{color:'#fff', fontSize:18, fontFamily:'ProximaNova-Bold'}}>Reject Request</Text>
                </TouchableOpacity>
            </View>
        }
        if(this.state.loading === true){
            button = 
            <View style={{ marginTop: 40}}>
                <View style={{...styles.butonNot, borderColor:'#080880'}}>
                    <Text style={{textAlign:'center', fontSize:18, fontFamily:'ProximaNova-Bold'}}>Please wait..</Text>
                </View>
            </View>
        }

        if(this.state.page === 1){
            disp = DATA.map((item) => (
                <TouchableWithoutFeedback onPress={() => {this.setState({page: 2, selected: item})}} >
                    <View style={styles.box} >
                        <Image source={no_photo} style={{height:36, width:36, borderRadius:18, borderWidth:1, borderColor:'#ccc'}}/>
                        <View style={{marginLeft:15, marginRight:10, flex:3,}} >
                            <View style={{flexDirection:'row'}}>
                                <Text style={{fontFamily:'ProximaNova-Bold', fontSize:12}}>NAME :</Text>
                                <Text> {item.name} </Text>
                            </View>
                            <View style={{flexDirection:'row',marginTop:5}}>
                                <Text style={{fontFamily:'ProximaNova-Bold', fontSize:12}}>PHONE :</Text>
                                <Text> +601x-xxxx{item.phone.substr(item.phone.length - 4)} </Text>
                            </View>

                            <Text style={{marginTop:5,fontSize:11, color:'grey'}} >Requested {Moment(item.timestamp).fromNow()}  </Text>
                        </View>
                        <Icon name='angle-right' color='#080880' size={20} style={{alignSelf:'flex-end', marginRight:15}}  />
                    </View>
                </TouchableWithoutFeedback>
            ))
            

        }
        if(this.state.page === 2){
            disp = 
            <View style={{flex:1, margin:20 }}>
                <Image source={shared} style={{height:Dimensions.get('window').height / 4, width:Dimensions.get('window').width - 60,resizeMode:'contain', alignSelf:'center'}}/>

                <View style={{marginTop:15}} >
                    <Text style={{fontSize:16, fontFamily: "ProximaNova-Bold", color:'#080880',}}>{ITEM.name} has requested to be co-owner of this vehicle {PLATE}</Text>
                    <Text style={{fontSize:14, fontFamily: "ProximaNova-Bold", color:'#6e6d6d',marginTop:20}}>RELATIONSHIP</Text>
                    <View style={{ padding:10, paddingLeft: 10, marginTop:10, borderRadius: 6,borderColor:'#ccc', borderWidth:1, flexDirection:'row', justifyContent:'space-between', alignItems:'center', }}>
                        <Text>{ITEM.relationship} </Text>
                    </View>
                </View>

                {this.state.buttonview === 2 ?
                <View style={{marginTop:15}}>
                    <Text style={{fontSize:14, fontFamily: "ProximaNova-Bold", color:'#080880',marginTop:20}}>Please state the reason for rejecting this share request.</Text>
                    <FormTextInput 
                        title= ''
                        value={this.state.reject_reason}
                        placeholder= 'eg: I dont know this person'
                        onChangeText={(x) => this.setState({reject_reason:x})}
                    />
                    
                    {/* <View style={{padding: (Platform.OS === 'ios') ? 10 : 0, paddingLeft: 10, marginTop:10, borderRadius: 6,borderColor:'#080880', borderWidth:2, flexDirection:'row', justifyContent:'space-between', alignItems:'center', }}>
                        <TextInput 
                            onChangeText={(x)=>this.setState({reject_reason:x})} 
                            underlineColorAndroid="transparent" 
                            placeholder="eg: I dont know this person"
                        />
                    </View> */}
                </View>
                : null}

                {button}
            </View>
        }

        return disp 
    }

    

    render(){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Shared Request' onPress={() => this.props.navigation.goBack()} />
                <KeyboardAvoidingView
                        behavior={(Platform.OS === 'ios') ? "padding" : null} enabled
                        style={{ flex: 1, }}
                        >
                    <ScrollView style={{flex:1, margin:15}} showsVerticalScrollIndicator={false} >
                    
                        {this.renderView()}

                        <View style={{height: 15}} />
                        
                    </ScrollView>
                    </KeyboardAvoidingView>
                </View>
            </SafeAreaView>
        )
    }
}

VerificationRequest = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(VerificationRequest))
export default VerificationRequest

const styles = StyleSheet.create({
    top:{
        backgroundColor:'#080880',
        padding:20,
        borderBottomLeftRadius:50,
        flexDirection:'row',
        alignItems:'center'
      },
    buton:{
        backgroundColor: '#080880',
        padding:15,
        alignSelf:'center',
        alignItems:'center',
        marginBottom:15,
        width:CONST_WIDTH - 60,
        borderRadius:8,
      },
      butonNot:{
        backgroundColor: 'white',
        padding:15,
        alignSelf:'center',
        alignItems:'center',
        borderWidth:1, 
        borderColor: 'tomato',
        width:CONST_WIDTH - 60,
        borderRadius:8,
    
      },
      box:{
        borderRadius:15, 
        borderWidth:1, 
        borderColor:'#ccc', 
        flexDirection:'row', 
        justifyContent:'space-between',
        alignItems:'center' , padding:15, paddingRight:5, backgroundColor:'white', marginTop:5, marginBottom:15, flexWrap:'wrap',
      }      
    
})