import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/AntDesign"
import Moment from 'moment';

import Header from '../components/Header.js'
import profile from '../assets/empty_profile.png'

const WIDTH = Dimensions.get('window').width

class WorkshopReviews extends React.Component{

    countStars = () => {
        let DATA = this.props.navigation.state.params.reviews
        let COUNT1= 0
        let COUNT2= 0
        let COUNT3= 0
        let COUNT4= 0
        let COUNT5= 0
        let dispkay

        for (const key in DATA) {
            let item = DATA[key]

            if(item.rate === '5'){ COUNT5 = COUNT5 + 1}
            if(item.rate === '4'){ COUNT4 = COUNT4 + 1}
            if(item.rate === '3'){ COUNT3 = COUNT3 + 1}
            if(item.rate === '2'){ COUNT2 = COUNT2 + 1}
            if(item.rate === '1'){ COUNT1 = COUNT1 + 1}
            

            // RATE.push(item)
        }

        let TOTAL = COUNT5 + COUNT4 + COUNT3 + COUNT2 + COUNT1
        // console.warn('jumla', TOTAL, COUNT5, COUNT4, COUNT3, COUNT2, COUNT1);
        

        dispkay = 
        <View>
            <View style={{flexDirection:'row', marginBottom:10, alignItems:'center'}}>
                <Text style={{fontSize:16, marginRight:10}}>5</Text>
                <View style={styles.ori}>
                <View style={{...styles.colored, width: (COUNT5/TOTAL) * (Dimensions.get('window').width / 1.6)  }} />
                </View>
                
            </View>
            <View style={{flexDirection:'row', marginBottom:10, alignItems:'center'}}>
                <Text style={{fontSize:16, marginRight:10}}>4</Text>
                <View style={styles.ori}>
                <View style={{...styles.colored, width:(COUNT4/TOTAL) * (WIDTH / 1.6) }} />
                </View>

            </View>
            <View style={{flexDirection:'row', marginBottom:10, alignItems:'center'}}>
                <Text style={{fontSize:16, marginRight:10}}>3</Text>
                <View style={styles.ori}>
                <View style={{...styles.colored, width:(COUNT3/TOTAL) * (WIDTH / 1.6) }} />
                </View>
            </View>
            <View style={{flexDirection:'row', marginBottom:10, alignItems:'center'}}>
                <Text style={{fontSize:16, marginRight:10}}>2</Text>
                <View style={styles.ori}>
                <View style={{...styles.colored, width:(COUNT2/TOTAL) * (WIDTH / 1.6) }} />
                </View>
            </View>
            <View style={{flexDirection:'row', marginBottom:10, alignItems:'center'}}>
                <Text style={{fontSize:16, marginRight:10}}>1</Text>
                <View style={styles.ori}>
                <View style={{...styles.colored, width:(COUNT1/TOTAL) * (WIDTH / 1.6) }} />
                </View>
            </View>
           
        </View>


        return dispkay



    }

    showOverall = () => {
        let X = this.props.navigation.state.params.reviews_overall
        // console.warn('REV', X);      
        let display
        let STARS

        if(X !== "" && X !== undefined){
            if(X.rate === 5){
                STARS = 
                <View style={{flexDirection:'row', alignItems:'center',}}>
                        <Icon name='star' color='#ffce00' size={12} style={{marginRight:2}}/>
                        <Icon name='star' color='#ffce00' size={12} style={{marginRight:2}}/>
                        <Icon name='star' color='#ffce00'  size={12} style={{marginRight:2}}/>
                        <Icon name='star' color='#ffce00'  size={12} style={{marginRight:2}}/>
                        <Icon name='star' color='#ffce00'  size={12} style={{marginRight:2}}/>
                    </View>
            }
            if( X.rate <= 1){
                STARS = 
                <View style={{flexDirection:'row', alignItems:'center',}}>
                    <Icon name='star' color='#ffce00' size={12} style={{marginRight:2}}/>
                    <Icon name='star' color='#ccc' size={12} style={{marginRight:2}}/>
                    <Icon name='star' color='#ccc'  size={12} style={{marginRight:2}}/>
                    <Icon name='star' color='#ccc'  size={12} style={{marginRight:2}}/>
                    <Icon name='star' color='#ccc'  size={12} style={{marginRight:2}}/>
                </View>
            }
            if(X.rate >= 2 && X.rate < 3){
                STARS = 
                <View style={{flexDirection:'row', alignItems:'center',}}>
                    <Icon name='star' color='#ffce00' size={12} style={{marginRight:2}}/>
                    {X.rate === 2 ? <Icon name='star'  color='#ffce00' size={12} style={{marginRight:2}}/> :
                    <View style={{marginRight:2}}>
                        <Icon name='star'  color='#ccc' size={12}  />
                        <Icon name='star-half'  color='#ffce00' size={12} style={{position:'absolute', top: 0}} />
                    </View>    
                    }
                    <Icon name='star' color='#ccc'  size={12} style={{marginRight:2}}/>
                    <Icon name='star' color='#ccc'  size={12} style={{marginRight:2}}/>
                    <Icon name='star' color='#ccc'  size={12} style={{marginRight:2}}/>
                </View>
                }
            if( X.rate >= 3 && X.rate < 4){
                STARS = 
                <View style={{flexDirection:'row', alignItems:'center',}}>
                    <Icon name='star' color='#ffce00' size={12} style={{marginRight:2}}/>
                    <Icon name='star' color='#ffce00' size={12} style={{marginRight:2}}/>
                    {X.rate === 3 ? <Icon name='star'  color='#ffce00' size={12} style={{marginRight:2}}/> :
                    <View style={{marginRight:2}}>
                        
                        <Icon name='star'  color='#ccc' size={12}  />
                        <Icon name='star-half'  color='#ffce00' size={12} style={{position:'absolute', top: 0}} />
                    </View>    
                    }
                    <Icon name='star' color='#ccc'  size={12} style={{marginRight:2}}/>
                    <Icon name='star' color='#ccc'  size={12} style={{marginRight:2}}/>
                </View>
                }
            if( X.rate >= 4 && X.rate < 5){
                STARS = 
                <View style={{flexDirection:'row', alignItems:'center',}}>
                    <Icon name='star' color='#ffce00' size={12} style={{marginRight:2}}/>
                    <Icon name='star' color='#ffce00' size={12} style={{marginRight:2}}/>
                    <Icon name='star' color='#ffce00'  size={12} style={{marginRight:2}}/>
                    {X.rate === 4 ? <Icon name='star'  color='#ffce00' size={12} style={{marginRight:2}}/> :
                    <View style={{marginRight:2}}>
                        <Icon name='star'  color='#ccc' size={12}  />
                        <Icon name='star-half'  color='#ffce00' size={12} style={{position:'absolute', top: 0}} />
                    </View>    
                    }
                    <Icon name='star' color='#ccc'  size={12} style={{marginRight:2}}/>
                </View>
                }

            display = 
            <View style={{borderBottomWidth:0.5, borderBottomColor:'#ccc', padding:15, flexDirection:'row'}}>
                <View style={{flex:3}}>
                    {this.countStars()}
                </View>
                <View style={{flex:1, marginLeft:18, alignItems:'center', justifyContent:'center'}}>
                    <Text style={{fontSize:48, fontFamily:'ProximaNova-Bold',}} >{X.rate.toFixed(1)} </Text>
                    {STARS}
                    <Text style={{fontSize:20, }}>( {X.count} )</Text>
                </View>
    
                    
        
            </View>
        } else {
            display = 
            <View style={{borderBottomWidth:0.5, borderBottomColor:'#ccc', padding:15, flexDirection:'row'}}>
                <View style={{flex:3}}>
                    
                    <View style={{flexDirection:'row', marginBottom:10, alignItems:'center'}}>
                    <Text style={{fontSize:16, marginRight:10}}>1</Text>
                    <View style={styles.ori} />

                </View>
                <View style={{flexDirection:'row', marginBottom:10, alignItems:'center'}}>
                    <Text style={{fontSize:16, marginRight:10}}>2</Text>
                    <View style={styles.ori} />

                </View>
                <View style={{flexDirection:'row', marginBottom:10, alignItems:'center'}}>
                    <Text style={{fontSize:16, marginRight:10}}>3</Text>
                    <View style={styles.ori} />

                </View>
                <View style={{flexDirection:'row', marginBottom:10, alignItems:'center'}}>
                    <Text style={{fontSize:16, marginRight:10}}>4</Text>
                    <View style={styles.ori} />

                </View>
                <View style={{flexDirection:'row', marginBottom:10, alignItems:'center'}}>
                    <Text style={{fontSize:16, marginRight:10}}>5</Text>
                    <View style={styles.ori} />

                </View>

                </View>
                <View style={{flex:1, marginLeft:18, alignItems:'center', justifyContent:'center'}}>
                    <Text style={{fontSize:48, fontFamily:'ProximaNova-Bold',}} >0 </Text>
                    <View style={{flexDirection:'row', alignItems:'center',}}>
                        <Icon name='star' color='#ccc' size={12} style={{marginRight:2}}/>
                        <Icon name='star' color='#ccc' size={12} style={{marginRight:2}}/>
                        <Icon name='star' color='#ccc'  size={12} style={{marginRight:2}}/>
                        <Icon name='star' color='#ccc'  size={12} style={{marginRight:2}}/>
                        <Icon name='star' color='#ccc'  size={12} style={{marginRight:2}}/>
                    </View>
                    <Text style={{fontSize:20, }}>( 0 )</Text>
                </View>
    
                 
     
            </View>
        }

        return display
    }

    STARS = (item) => {
        let X = item 
        let STARS 

        if(X === '5'){
            STARS = 
            <View style={{flexDirection:'row', alignItems:'center',}}>
             <Icon name='star' color='#ffce00' size={12} style={{marginRight:2}}/>
             <Icon name='star' color='#ffce00' size={12} style={{marginRight:2}}/>
             <Icon name='star' color='#ffce00'  size={12} style={{marginRight:2}}/>
             <Icon name='star' color='#ffce00'  size={12} style={{marginRight:2}}/>
             <Icon name='star' color='#ffce00'  size={12} style={{marginRight:2}}/>
            </View>
        }
        if(X === '1'){
            STARS = 
            <View style={{flexDirection:'row', alignItems:'center',}}>
                <Icon name='star' color='#ffce00' size={12} style={{marginRight:2}}/>
                <Icon name='star' color='#ccc' size={12} style={{marginRight:2}}/>
                <Icon name='star' color='#ccc'  size={12} style={{marginRight:2}}/>
                <Icon name='star' color='#ccc'  size={12} style={{marginRight:2}}/>
                <Icon name='star' color='#ccc'  size={12} style={{marginRight:2}}/>
            </View>
        }
        if(X >= '2' && X < 3){
            STARS = 
            <View style={{flexDirection:'row', alignItems:'center',}}>
                <Icon name='star' color='#ffce00' size={12} style={{marginRight:2}}/>
                <Icon name='star' color='#ffce00' size={12} style={{marginRight:2}}/>
                <Icon name='star' color='#ccc'  size={12} style={{marginRight:2}}/>
                <Icon name='star' color='#ccc'  size={12} style={{marginRight:2}}/>
                <Icon name='star' color='#ccc'  size={12} style={{marginRight:2}}/>
            </View>
        }
        if(X >= '3' && X < 4){
            STARS =
            <View style={{flexDirection:'row', alignItems:'center',}}>
                <Icon name='star' color='#ffce00' size={12} style={{marginRight:2}}/>
                <Icon name='star' color='#ffce00' size={12} style={{marginRight:2}}/>
                <Icon name='star' color='#ffce00' size={12} style={{marginRight:2}}/>
                <Icon name='star' color='#ccc'  size={12} style={{marginRight:2}}/>
                <Icon name='star' color='#ccc'  size={12} style={{marginRight:2}}/>
            </View>

        }
        if(X >= '4' && X < '5' ){
            STARS = 
            <View style={{flexDirection:'row', alignItems:'center',}}>
                <Icon name='star' color='#ffce00' size={12} style={{marginRight:2}}/>
                <Icon name='star' color='#ffce00' size={12} style={{marginRight:2}}/>
                <Icon name='star' color='#ffce00'  size={12} style={{marginRight:2}}/>
                <Icon name='star' color='#ffce00' size={12} style={{marginRight:2}}/>
                <Icon name='star' color='#ccc'  size={12} style={{marginRight:2}}/>
            </View>
        }

        return STARS
    }
    showReview = () => {
        let DATA = this.props.navigation.state.params.reviews
        let display
        // console.warn('hell', DATA);
        let RATE = []

        if(DATA !== undefined &&  DATA.length !== 0){
            // let M =  DATA.sort((a,b) => b.timestamp - a.timestamp)
            for (const key in DATA) {
                let item = DATA[key]
                RATE.push(item)
            }

            let KKL = RATE.sort((a,b) => b.timestamp - a.timestamp)
            

            display = KKL.map((item, index) => (
                <View style={{ margin:15, borderBottomColor:'grey', borderBottomWidth:0.5, marginBottom:0, paddingBottom:15}}>
                    <View style={{flexDirection:'row',}}>
                        <View style={styles.viewImg}>
                            <Image source={profile} style={styles.img} />
                        </View>
                        <View style={{flexShrink:1}}>
                            <Text style={{fontFamily:'ProximaNova-Bold'}}>{item.name !== '' ? item.name : 'Anonymous'}</Text>
                            
                            <View style={{flexDirection:'row', marginBottom:10,  alignItems:'center'}}>
                                {this.STARS(item.rate)}
                                <Text style={{marginLeft:15,}}>{Moment(item.timestamp).fromNow()}</Text>
                            </View>

                            {item.text !== '' ?  <Text>{item.text}</Text> : null}

                            {item.reply !== undefined ? 
                            <View style ={{marginTop: 15, paddingLeft:4, borderLeftColor:'grey', borderLeftWidth: 2}}>
                                <View style={{flexDirection:'row'}}>
                                    <Text style={{fontFamily:'ProximaNova-Bold', marginRight:10,}} > Response from owner </Text>
                                    <Text>{Moment(item.reply.timestamp).fromNow()}</Text>
                                </View>
                                <Text> {item.reply.text} </Text>

                            </View>     
                            : null  }
                        </View>
                        
                    </View>
                </View>
            ))
                
        
        } else {
            display = <Text style={{marginLeft:15}}> No reviews yet </Text>
        }

        return display
    }


    render(){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Workshop Reviews' onPress={() => this.props.navigation.goBack()} />
                    
                <ScrollView>
                        <Text style={{fontFamily:'ProximaNova-Bold', margin: 15,}}>Ratings</Text>
                        {this.showOverall()}

                         <Text style={{fontFamily:'ProximaNova-Bold', margin: 15,}}>Reviews</Text>

                        {this.showReview()}
                    </ScrollView>
                    
                </View>
            </SafeAreaView>
        )
    }
}

WorkshopReviews = inject('mobx_auth','mobx_retail', 'mobx_carkey', 'mobx_config')(observer(WorkshopReviews))
export default WorkshopReviews

const styles = StyleSheet.create({
    ori: {height:6, borderRadius:5, backgroundColor:'#ccc',width: Dimensions.get('window').width / 1.6 },
    colored: {height: 6, borderRadius:5, backgroundColor: '#ffce00', position:'absolute', left:0, top: 0, },
    img: {height:26, width:26, borderRadius:13,},
    viewImg: {height:30, width:30, borderRadius:15, alignItems:'center', justifyContent:'center', borderWidth:0.5, borderColor:'#ccc', marginRight:15, }
})