import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Image,Dimensions, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/Entypo"

import Header from '../components/Header.js'
import MiddleModal from '../components/MiddleModal'
import Button from '../components/Button'
import RoundedBorderButton from '../components/RoundedBorderButton'

import placeholder from '../assets/vehicles_placeholder.png'
import profile from '../assets/empty_profile.png'

import servS from '../assets/servS_white.png'
import left_arrow from '../assets/icons/left_arrow.png'
import bottom_right from '../assets/icons/bottomright_arrow.png'
import service1 from '../assets/icons/workshop2.png'
import service2 from '../assets/icons/sos2.png'
import service3 from '../assets/icons/insurance2.png'
import more from '../assets/icons/more.png'
import add from '../assets/add_button.png'
import banner from '../assets/DirectLendingBanner.png'
import servnext_banner from '../assets/servnext_banner.png'
import OT1 from '../assets/logo/OT1.jpeg'
import OT2 from '../assets/logo/OT2.jpeg'
import OT3 from '../assets/logo/OT3.jpeg'
import OT4 from '../assets/logo/OT4.jpeg'

import homeActive from '../assets/bottomIcons/homeActive.png'
import homeInactive from '../assets/bottomIcons/homeInactive.png'
import trackActive from '../assets/bottomIcons/trackActive.png'
import trackInactive from  '../assets/bottomIcons/trackInactive.png'
import inboxActive from '../assets/bottomIcons/inboxActive.png'
import inboxInactive from '../assets/bottomIcons/inboxInactive.png'
import qrInactive from '../assets/bottomIcons/qrInactive.png'

const GREY = '#6e6d6d'
const BLUE = '#080880'
const WIDTH = Dimensions.get('window').width

class PopOut extends React.Component{
    render(){
        return (
        this.props.visible === true ? 
            <View style={{...styles.modalPop , top : this.props.top}}>
            {this.props.children}
        </View>
        : null)
       
    }
}

class Onboarding extends React.Component{
    constructor(){
        super()
        this.state = {
            from: 'Register',
            showView: 1,
            pop1: true,
            pop2: false,
            pop3: false,
            pop4: false,
            pop5: false,
            pop6: false,
        }
    }

    componentDidMount(){
        let STATE = this.props.mobx_auth.check 
        if(STATE === 3){
            this.setState(({from :'settings'}))
        }
    }

    renderTop = () => {
        let display 
        if(this.state.showView === 3){
            display = 
            <View style={{flexDirection:'row', justifyContent:'space-between', padding:20,}}>
                <Image source={servS} style={styles.img} />
                
                <View style={{borderWidth:0.5, borderColor:'#fff', height:42, width: 42, borderRadius:21, alignItems:'center', justifyContent:'center'}}>
                    <Image source={profile} style={styles.img} />
                </View>
                
            </View>
        } else {
            display = 
            <View>
                
                <View style={styles.overlay} />
                <View style={{flexDirection:'row', justifyContent:'space-between', padding:20,}}>
                    <Image source={servS} style={styles.img} />
                    
                    <View style={{borderWidth:0.5, borderColor:'#fff', height:42, width: 42, borderRadius:21, alignItems:'center', justifyContent:'center'}}>
                        <Image source={profile} style={styles.img} />
                    </View>
                    
                </View>
                
            </View>
        }
        return display 
    }

    renderVehicle = () => {
        let display 
        if(this.state.showView === 2){
            display = 
            <View style={{flexDirection:'row', justifyContent:'space-around',alignItems:'center' ,  padding:20, zIndex:999}}> 
                <Image source={placeholder} style={styles.imgPlace} />
                <View style={{alignItems:'center', justifyContent:'center'}}>
                    <Image source={add} style={styles.topIcon} />
                    <Text style={{fontFamily: "ProximaNova-Bold",color:'#fff', fontSize:18, marginTop:15}}>Add your first vehicle</Text>
                </View>
            </View>
        } else {
            display = 
            <View>
                <View style={styles.overlay} />
                <View style={{flexDirection:'row', justifyContent:'space-around',alignItems:'center' ,  padding:20}}> 
                <Image source={placeholder} style={styles.imgPlace} />
                <View style={{alignItems:'center', justifyContent:'center'}}>
                    <Image source={add} style={styles.topIcon} />
                    <Text style={{fontFamily: "ProximaNova-Bold",color:'#fff', fontSize:18, marginTop:15}}>Add your first vehicle</Text>
                </View>
                
            </View>
            
            </View>
        }

        return display 
    }

    renderMiddle = () => {
        let display 
        if(this.state.showView === 4){
            display =  
            <View style={{flexDirection:'row', justifyContent:'space-between', margin:20, marginBottom:0}} >
                <View style={styles.icon}>
                    <Image source={service1} style={styles.iconService} />
                    <Text style={styles.textIcon}>Workshop</Text>
                </View>
                <View style={styles.icon}>
                    <Image source={service2} style={styles.iconService} />
                    <Text style={styles.textIcon}>Road Assistance</Text>
                </View>
                <View style={styles.icon}>
                    <Image source={service3} style={styles.iconService} />
                    <Text style={styles.textIcon}>Insurance & Road Tax</Text>
                </View>
                <View style={styles.icon}>
                    <Image source={more} style={styles.iconService} />
                    <Text style={styles.textIcon}>More</Text>
                </View>
            </View>
        } else {
            display = 
            <View>
                <View style={styles.overlay} />
                <View style={{flexDirection:'row', justifyContent:'space-between', margin:20, marginBottom:0}} >
                    <View style={styles.icon}>
                        <Image source={service1} style={styles.iconService} />
                        <Text style={styles.textIcon}>Workshop</Text>
                    </View>
                    <View style={styles.icon}>
                        <Image source={service2} style={styles.iconService} />
                        <Text style={styles.textIcon}>Road Assistance</Text>
                    </View>
                    <View style={styles.icon}>
                        <Image source={service3} style={styles.iconService} />
                        <Text style={styles.textIcon}>Insurance & Road Tax</Text>
                    </View>
                    <View style={{...styles.icon, alignItems:'center', justifyContent:'center'}}>
                        <Image source={more} style={styles.img} />
                        <Text style={styles.textIcon}>More</Text>
                    </View>
                </View>
                
            </View>
        }
        return display
    }

    renderBottom = () => {
        let display 
        if(this.state.showView === 5){
            display = 
            <View style={{...styles.bottomTab, zIndex:9}}>
                <View style={{alignItems:'center', justifyContent:'center'}} >
                <Image source={homeActive} style={{height:30, width:30, }} />
                <Text style={{...styles.bottomText, color:"#080880",}}>Home</Text>
                </View>
                <View style={{alignItems:'center', justifyContent:'center'}}>
                <Image source={trackInactive} style={{height:30, width:30, }} />
                <Text style={styles.bottomText}>Track</Text>
                </View>
                <View style={{alignItems:'center', justifyContent:'center'}}>
                <Image source={inboxInactive} style={{height:30, width:30, }} />
                <Text style={styles.bottomText}>Inbox</Text>
                </View>
               
               


            </View>
        } else {
            display = 
            // <View style={{position:'absolute', bottom: 0, width: WIDTH,}}>
                
                <View style={styles.bottomTab}>
                    <View style={{alignItems:'center', justifyContent:'center'}} >
                        <Image source={homeActive} style={{height:30, width:30, }} />
                        <Text style={{...styles.bottomText, color:"#080880",}}>Home</Text>
                    </View>
                    <View style={{alignItems:'center', justifyContent:'center'}}>
                        <Image source={trackInactive} style={{height:30, width:30, }} />
                        <Text style={styles.bottomText}>Track</Text>
                    </View>
                    <View style={{alignItems:'center', justifyContent:'center'}}>
                        <Image source={inboxInactive} style={{height:30, width:30, }} />
                        <Text style={styles.bottomText}>Inbox</Text>
                    </View>
                    <View style={{backgroundColor: 'rgba(0,0,0,0.7)',left:0,bottom:0, height:74, zIndex:2, position:'absolute', width:WIDTH, flex:1}} />
                </View>
                
               
            // </View>

        }

        return display 

    }
    
    skip = () => {
        this.setState({pop1: false, pop5: false})
        if(this.state.from === 'Register'){
            this.props.mobx_auth.setCheck(3);
        } else {
            this.props.navigation.goBack()
        }
         
    }
    
    render(){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                    <View style={{backgroundColor:'#080880', borderBottomLeftRadius:50,  }}>
                        
                        {this.renderTop()}
                        
                        <View >
                        <View style={styles.overlay} />
                        <Text style={{fontFamily:'ProximaNova-Bold',color:'#fff', fontSize:20, paddingLeft:20,}}>My Vehicles</Text>
                        </View>
                        
                        {this.renderVehicle()}
                        <View >
                        <View style={styles.overlay} />
                        <View style={{height: 15}} />
                        </View>
                    </View>

                    {this.renderMiddle()}

                    <View >
                        <View style={styles.overlay} />
                    <View style={{borderTopWidth:0.5, borderTopColor:'#ccc', paddingTop:15}} >
                    <View style={styles.banner}>
                        <Image source={banner} style={styles.image} />
                        <View style= {styles.circleDiv}>
                            <View style={styles.active} />
                            <View style={styles.inactive} />
                            <View style={styles.inactive} />
                        </View>
                    </View>
                    </View>
                    

                    <View style={{margin: 20}}>
                        <Text style={{marginBottom:15, fontSize:16, fontFamily: "ProximaNova-Bold",}}>What's new</Text>
                        <Image source={servnext_banner} style={{height:Dimensions.get('window').width /2, width: Dimensions.get('window').width - 40,alignSelf:'center', borderRadius:15,  }} />
                    </View>
                    </View>
                    {this.renderBottom()}

                    {/* MODAL */}
                    
                    <PopOut visible={this.state.pop1} top='20%'
                        children={
                            <View style={{backgroundColor:'#fff', alignItems:'center', justifyContent:'center', zIndex:3}}>
                                <Image source={OT1} style={styles.irah} />
                                <Text style={styles.inText}>Hey there! I'm Irah!</Text>
                                <Text style={styles.inText}>Your guide around the SERV app</Text>
                                <Text style={styles.inText}>Ready to begin?</Text>
                                <TouchableOpacity onPress={()=> this.setState({showView:2, pop1: false, pop2:true})}  style={styles.button} >
                                    <Text style={{fontFamily: "ProximaNova-Bold",color: '#fff'}}>Nice to meet you!</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={this.skip} style={{marginTop:20, alignSelf:'center'}}  >
                                    <Text style={styles.textButton}>Skip</Text>
                                </TouchableOpacity>

                            </View>
                        }
                    
                    />

                    <PopOut visible ={this.state.pop2} top='40%'
                        children={
                            <View style={{backgroundColor:'#fff',  zIndex:3}}>
                                <View style={{alignItems:'center', justifyContent:'center', marginBottom:15, marginLeft:10,}}>
                                    <Image source={OT1} style={styles.irah} />
                                    <Image source={left_arrow} style={{position:'absolute', top:10,right:10, height:WIDTH/7, width: WIDTH/7, marginTop:-10, resizeMode:'contain'}} />
                                </View>
                                <Text style={styles.inText}>To start, here is where you <Text style={{fontWeight:'bold'}}>ADD A VEHICLE and TRACK</Text> its conditions.</Text>
                                <Text style={styles.inText}>Cars, bicycles, trucks, scooters, boats, private jets and more! You name it, we’ll add it!</Text>
                                <TouchableOpacity onPress={()=> this.setState({showView:3, pop2:false, pop3:true})}  style={styles.button} >
                                    <Text style={{fontFamily: "ProximaNova-Bold",color: '#fff'}}>OK, I'll add one later!</Text>
                                </TouchableOpacity>
                            </View>
                        }
                    />
                    <PopOut visible ={this.state.pop3} top='14%'
                        children={
                            <View style={{backgroundColor:'#fff', zIndex:3}}>
                                <View style={{alignItems:'center', justifyContent:'center', marginBottom:15}}>
                                    <Image source={OT2} style={styles.irah} />
                                </View>
                                <Image source={left_arrow} style={{position:'absolute', top:10, right:0, height:WIDTH/7, width: WIDTH/7,  resizeMode:'contain'}} />

                                <Text style={styles.inText}>Here’s where you can view your amazing <Text style={{fontWeight:'bold'}}>PROFILE</Text>!</Text>
                                <Text style={styles.inText}>You can change your app settings, profile photo and edit your personal info here.</Text>
                                <Text style={styles.inText}>Can't wait to know more about you!</Text>

                                <TouchableOpacity onPress={()=> this.setState({showView:4, pop3: false, pop4:true})}  style={styles.button} >
                                    <Text style={{fontFamily: "ProximaNova-Bold",color: '#fff'}}>Thanks!</Text>
                                </TouchableOpacity>
                            </View>
                        }
                    
                    />
                    <PopOut visible ={this.state.pop4} top={0}
                        children={
                            <View style={{backgroundColor:'#fff',  zIndex:3}}>
                                
                                    <Image source={OT3} style={styles.irah} />
                                
                                <Text style={styles.inText}>Now most importantly,</Text>
                                <Text style={styles.inText}>You can find <Text style={{fontWeight:'bold'}}>SERVICES</Text> and get the help you need here!</Text>

                                <View style={{ justifyContent:'center', alignItems:'center'}}>
                                <TouchableOpacity onPress={()=> this.setState({showView:5, pop4: false, pop5:true})}  style={styles.button} >
                                    <Text style={{fontFamily: "ProximaNova-Bold",color: '#fff'}}>Cool!</Text>
                                </TouchableOpacity>
                                <Image source={bottom_right} style={{position:'absolute', bottom:10, right:5, height:WIDTH/7, width: WIDTH/7,  resizeMode:'contain'}} />
                                </View>
                                

                            </View>
                        }
                    
                    />
                    <PopOut visible={this.state.pop5} top='35%'
                        children={
                            <View style={{backgroundColor:'#fff',  zIndex:3}}>
                                <Image source={OT4} style={styles.irah} />
                                <Text style={styles.inText}>And this is the main <Text style={{fontWeight:'bold'}}>NAVIGATION BAR</Text>!</Text>
                                <Text style={styles.inText}>Here’s where you can view the homepage, track vehicles, check your feed, inbox or search for something!</Text>

                                <View style={{justifyContent:'center', alignItems:'center'}}>
                                <TouchableOpacity onPress={()=> this.setState({showView:1, pop5: false, pop6:true})}   style={styles.button} >
                                    <Text style={{fontFamily: "ProximaNova-Bold",color: '#fff'}}>That's great!</Text>
                                </TouchableOpacity>
                                <Image source={bottom_right} style={{position:'absolute', bottom:10, right:5, height:WIDTH/7, width: WIDTH/7,  resizeMode:'contain'}} />
                                </View>
                            </View>
                        }
                    />
                    <PopOut visible={this.state.pop6} top='20%'
                        children={
                            <View style={{backgroundColor:'#fff', alignItems:'center', justifyContent:'center', zIndex:3}}>
                                <Image source={OT1} style={styles.irah} />
                                <Text style={{...styles.inText, marginTop:20}}>That’s all from me!</Text>
                                <Text style={styles.inText}>Hoping to be able to SERV you soon!</Text>

                                <TouchableOpacity onPress={this.skip}  style={styles.button} >
                                    <Text style={{fontFamily: "ProximaNova-Bold",color: '#fff'}}>Bye, Irah!</Text>
                                </TouchableOpacity>
                                <View style={{height: 15}} />
                            </View>
                        }
                    
                    />
                   
                    
                    
                    



                    
                </View>
            </SafeAreaView>
        )
    }
}

Onboarding = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(Onboarding))
export default Onboarding

const styles = StyleSheet.create({
    top: {
        backgroundColor:'#080880',
        // padding:20,
        borderBottomLeftRadius:25,
        borderBottomRightRadius: 25,
        // marginBottom:20,
    },
    img :{height:36, width:36,resizeMode:'contain', alignSelf:'center'}, 
    imgPlace :{
        height: WIDTH /3, 
        width: WIDTH /3, 
        marginRight:20,
        // backgroundColor:'#fff'
    },
    topIcon:{
        height:40, width:40,resizeMode:'contain',
    },
    iconService:{
        resizeMode:'cover', alignSelf:'center',
        height:55, width:55
    },
    icon:{
        width:(WIDTH - 30) /4 , 
        marginBottom:20,
        paddingLeft:5, 
        paddingRight:5,
    },
    textIcon:{
        textAlign:'center',
        fontFamily: "ProximaNova-Bold",
        fontSize:13
    },
    banner:{
        
        height: WIDTH / 5,
        margin:15,
        marginBottom:0,
        width:WIDTH - 30,
        marginBottom:0,
        // backgroundColor:'white',
        alignItems:'center',
        justifyContent:'center'
    },
    image :{
        width:WIDTH - 30, 
        height: WIDTH / 5, 
        resizeMode:'cover', 
        borderRadius:15,
    },
    circleDiv : {
        marginTop: 15,
        flexDirection:'row',
        justifyContent:'center',
        alignItems: 'center',
        // width: '100%'
    },
    inactive: {
        height: 6, 
        width:6 ,
        borderRadius: 3,
        margin:3,
        backgroundColor:'rgba(0,0,128, 0.5)'
    },
    active: {
        height: 6,
        width:12,
        borderRadius: 3,
        margin:3,
        backgroundColor:'#080880'
    },
    bottomTab :{flexDirection: 'row', justifyContent: 'space-around', 
    width: WIDTH,
    backgroundColor:'#fff', position:'absolute', bottom: 0, padding:10,
    },
    bottomText : {fontFamily: "ProximaNova-Bold",color:"#ccc", },
    button: {marginTop:20,marginBottom:10, padding:15,paddingLeft:25, paddingRight:25,  borderRadius:10, backgroundColor:'#080880', alignSelf:'center' },
    textButton :{fontFamily: "ProximaNova-Bold", color:"#080880", fontSize:15},
    overlay :{
        backgroundColor: 'rgba(0,0,0,0.7)',
        position:'absolute',
        // top:0,
        // bottom:0,
        width:'100%',
        height:'100%',
        // borderWidth:1,
        zIndex:2,
        flex:1
    },
    irah: {height: WIDTH/4, width:WIDTH/4, borderRadius:WIDTH/2,backgroundColor:'#ccc',marginTop:10, alignSelf:'center'},
    inText : {fontFamily: "ProximaNova-Bold", marginTop:15, fontSize:16},
    modalPop:{
        position:'absolute',
        flex: 1,
        alignSelf:'center',
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 20,
        borderRadius: 10,
        padding:20,
        width:'92%',
        backgroundColor: '#fff',
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOpacity: 0.22,
                shadowRadius: 2.22,
                shadowOffset: { width: 0, height: 1 },
            },
            android: {
                elevation: 3
            },
            }),

    }
})