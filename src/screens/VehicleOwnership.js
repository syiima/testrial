import React from "react"
import {StyleSheet, Text, View, Image,SafeAreaView,KeyboardAvoidingView,Platform,  TouchableOpacity, Dimensions,ScrollView,TextInput,Alert, TouchableWithoutFeedback} from 'react-native';
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from 'react-native-vector-icons/FontAwesome';
import {cloud_serv_user_sandbox, cloud_serv_user_production} from "../util/FirebaseCloudFunctions.js"
import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';

import Header from '../components/Header.js'
import owner from '../assets/owner.png'

CONST_WIDTH = Dimensions.get('window').width;
CONST_HEIGHT = Dimensions.get('window').height;

class VehicleOwnership extends React.Component{
    constructor(){
        super()
        this.state = {
            vdata: '',
            loading: true,
            uid: '',
            tokenU:'',
            secretU: ''
        }
    }

    componentDidMount(){
        this.fetchVehicle()
        this.getToken()
    }

    getToken = async() => {
        let tokenU =  await AsyncStorage.getItem('CSRFTokenU')
        let secretU =  await AsyncStorage.getItem('CSRFSecretU')

        this.setState({tokenU : tokenU, secretU : secretU})

    }


    fetchVehicle = () => {
        let PLATE = this.props.mobx_carkey.PLATE 
        let DATA = []

        firebase.database().ref(`plate_number/${PLATE}/user`).once('value', (snp) => {
            if(snp.exists()){
                KEY = Object.keys(snp.val());

                KEY.forEach( (uid) => {
                    let a = snp.val()[uid];
                    a.uid = uid;
                    
                    if(a.relationship !== 'not owner'){
                        DATA.push(a)

                    }
                    
                })

                this.setState({vdata: DATA, loading:false, uid: ''})
            } else { 
                this.setState({loading : false, uid :''})
                this.props.navigation.goBack()
            }
        })
    }

    renderUsers = () => {
        let display, disp 
        let DATA = this.state.vdata
        let PLATE = this.props.mobx_carkey.PLATE 

        if(DATA.length !== 0){
            disp = DATA.map((item) => (
                <View style={{flexDirection:'row',marginBottom:15, marginTop:5, width: Dimensions.get('window').width - 30,}} >
                    <View style={styles.box}>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{fontFamily:'ProximaNova-Bold', fontSize:11}}> NAME : </Text>
                            <Text> {item.name} </Text>
                        </View>
                        <View style={{flexDirection: 'row', marginTop: 6}}>
                            <Text style={{fontFamily:'ProximaNova-Bold', fontSize:11}}> RELATIONSHIP : </Text>
                            <Text> {item.relationship} </Text>
                        </View>
                        <Text style={{fontSize:11, color:'grey'}} >Approved on {moment(item.timestamp_approved).format('MMM YYYY')} </Text>
                    </View>
                    <TouchableWithoutFeedback onPress={() => {
                        this.setState({uid: item.uid})
                         Alert.alert(
                            'Alert',
                            'Are you sure you want to remove this user? ',
                            [
                              {text: 'NO'},
                  
                              {text: 'YES', onPress: this.removeUser},
                            ]
                          )
                    }} >
                        <View style={{backgroundColor: 'tomato', flex:0.5, borderRadius:8, alignItems:'center', justifyContent:'center'}}>
                        {/* <View style={{...styles.box, flex:1}}> */}
                            <Icon name='trash' color='white' size={15} /> 
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            ))


            display = 
            <View>
                <Text style={{fontSize: 12, fontFamily:'ProximaNova-Bold'}}>Co-owners</Text>
                {disp}
            </View>
        } 

        return display 
    }

    removeUser = () => {
        let UID = this.state.uid 
        let PLATE = this.props.mobx_carkey.PLATE

        firebase.database().ref(`users/${UID}/vehicles/${PLATE}/`).remove()
        firebase.database().ref(`plate_number/${PLATE}/user/${UID}/`).remove()

        this.sendNoti()

    }

    sendNoti = () => {
        let PLATE = this.props.mobx_carkey.PLATE
        let UID = this.state.uid 
      
        let URL, TEXT, BODY, Q_FUID, Q_VEHICLE_ID
      
        if (this.props.mobx_config.config === 0) URL = cloud_serv_user_sandbox.personalNotifications
        if (this.props.mobx_config.config === 1) URL = cloud_serv_user_production.personalNotifications
      
        TEXT = '?text=Co-ownership removed'
        BODY = `&body=The true owner has removed you from being a co-owner of ${PLATE}. If you think this is a mistake, please re-add the vehicle and request co-ownership again.	`  
       
      
        Q_FUID = `&userKey=${UID}`
        Q_VEHICLE_ID = `&vehicle_id=${PLATE}`
        
        FINAL_URL = URL + TEXT + BODY + Q_FUID + Q_VEHICLE_ID
      
        console.warn('finalURL', FINAL_URL);
        fetch(FINAL_URL,{
          credentials:true, 
          headers: {
            csrftoken: this.state.tokenU,
            csrfsecret: this.state.secretU
          }
        }).then(() => {
            this.fetchVehicle()
      
        })
      
      }


    render(){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Vehicle Ownership' onPress={() => this.props.navigation.goBack()} />
                    
                <ScrollView style={{flex:1, margin:15}}>
                    <Image source={owner} style={{height:Dimensions.get('window').height / 4, width:Dimensions.get('window').width - 60,resizeMode:'contain', alignSelf:'center'}}/>
                    <Text style={{fontFamily: 'ProximaNova-Bold', fontSize:18, textAlign:'center',color:'#080880', marginBottom:15}} >Congrats on owning your vehicle digitally</Text>
                    
                    
                    {this.renderUsers()}
                </ScrollView>
            
                    
                </View>
            </SafeAreaView>
        )
    }
}

VehicleOwnership = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(VehicleOwnership))
export default VehicleOwnership

const styles = StyleSheet.create({
    box :{
        borderRadius:15, 
        borderWidth:1, 
        borderColor:'#ccc', 
        padding:15, 
        paddingRight:5, 
        backgroundColor:'white', 
        marginRight: 15, 
        flex:3

    },
    buton:{
        backgroundColor: '#080880',
        padding:15,
        alignSelf:'center',
        alignItems:'center',
        marginTop:30,
        width:CONST_WIDTH -30,
        borderRadius:8,
        ...Platform.select({
         ios: {
           shadowColor: '#000',
           shadowRadius: 3,
           shadowOffset: { width: 0, height: 2 },
           shadowOpacity: 0.6,
         },
         android: {
           elevation: 3
         },
        }),
      },
    })