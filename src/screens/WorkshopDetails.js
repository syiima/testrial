import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Image,Dimensions,Linking,FlatList, TouchableWithoutFeedback,Alert, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Collapsible from 'react-native-collapsible';
import Moment from 'moment';

import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import AntIcon from "react-native-vector-icons/AntDesign";
import Feather from 'react-native-vector-icons/SimpleLineIcons';
import FontIcon from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/Feather';

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import {faCoffee, faMosque, faRestroom, faCouch, faUtensils, faWifi,faCar, faPlug, faParking, faTrash, faMedkit, faStore, faHotel } from "@fortawesome/free-solid-svg-icons";


import Header from '../components/Header.js'
import BottomModal from "../components/BottomModal.js"
import Button from '../components/Button'

import maps_logo from '../assets/maps_logo.png'
import waze_logo from '../assets/waze_logo.png'
import emptyLogo from '../assets/icons/emptyLogo.png'
import vehicleEmpty from '../assets/emptyCar.png'
import motorEmpty from '../assets/emptyMotor.png'

const WIDTH = Dimensions.get('window').width - 30
library.add(fab, faCoffee, faMosque, faRestroom, faCouch,faCar, faUtensils, faWifi, faPlug, faParking, faTrash,faStore, faMedkit, faHotel, )

class WorkshopDetails extends React.Component{
    constructor(){
        super()
        this.state = {
            workshoplogo: '',
            loyalty_data:'',
            collapsed: true,
            modalMap: false,
            lat: 0,
            long:0,
            modalVehicle: false,
            v_data: ''
        }
    }
    
    componentDidMount(){
        this.fetchLogo()
        this.fetchVehicle()
    }

    fetchLogo(){
        let WS_MAIN = this.props.navigation.state.params.item.group_id
        // console.warn('hell', this.props.navigation.state.params.item.workshop_id,WS_MAIN );
        firebase.database().ref(`retail_main/${WS_MAIN}`).once('value').then((snapshot) => {
          if(snapshot.exists()){
            let X = snapshot.val().logo !== undefined ? snapshot.val().logo : ''
            let A 
            
            if(snapshot.val().loyalty !== undefined){
                if(snapshot.val().loyalty.validity !== undefined){
                    let CURRENT_TIME = Date.now()              
                    if(snapshot.val().loyalty.validity > CURRENT_TIME){
                        A = snapshot.val().loyalty
                    } else {
                        A = ''
                    }         
                }
            } else {
                this.setState({loyalty_data: ''})
            }
            this.setState({workshoplogo: X, loyalty_data:A})
  
          }
        })
    }

    fetchVehicle(){
        let that = this;
        let FUID = this.props.mobx_auth.FUID;
        firebase.database().ref(`users/${FUID}/vehicles`).on('value', (snapshot) => {
          let KEY = [];
          if(snapshot.exists()){
            KEY = Object.keys(snapshot.val())
            let V_DATA = [];
            KEY.forEach((key_id) => {
              
                let PLATE  = key_id 
                let STATUS = snapshot.val()[key_id].status
                let OWNERSHIP = snapshot.val()[key_id].true_owner
    
                firebase.database().ref(`plate_number/${PLATE}`).once('value', (snp) => {
                  if(snp.exists()){
                    let a = snp.val()
                    a.plate = PLATE
                    a.true_owner = OWNERSHIP
                    a.status = STATUS
        
                    if(a.book_walkin === undefined){
                        V_DATA.push(a);
                    } else {
                        if(a.book_walkin._book_id === ''){
                            V_DATA.push(a);
                        }
                    }

    
                    that.setState({v_data:V_DATA,});
            
    
                  }
                })
            })
        
          } else {
            that.setState({v_data:[]});
          }
        })
    }


    //========
    showOpenClose = (item) => {
        let display
        let DATA = item
        let TODAY = (Moment().format('dddd')).toLowerCase()
        let TIME = Moment().format('HH:mm:ss') //current
        let STATUS;
        
        if(DATA !== undefined){
            for (const d_key in DATA) {          
                if(TODAY === d_key){
                //closed
                if(DATA[d_key].open === 'closed' || DATA[d_key].open === ''){
                    STATUS = 'Closed'
                }             
                if(DATA[d_key].open !== 'closed' && DATA[d_key].open !== '') {
        
                    // open
                    let finalH
                    let HOURo
                    let HOUR = DATA[d_key].open.split(':')[0]
                    let MINUTE =  (DATA[d_key].open.split(':')[1]).substring(0,2)
                    
                    if(HOUR < 10){
                        HOUR = '0' + HOUR
                    }
        
                    let AMPM = (DATA[d_key].open.split(':')[1]).substring(4,2)
                    // // console.warn('hmmm', AMPM);
                    if(AMPM === 'PM'){
                        let x = HOUR
                        let y = '12'
    
                        HOURo = parseFloat(x) + parseFloat(y)
                    }  else {
                        HOURo = HOUR
                    }
                    finalH = HOUR.toString()
                    let FINAL = finalH + ':' + MINUTE + ':' + '00'
                    
        
                    //CLOSE
                    let HOURc
                    let finalC
                    let HOURC = DATA[d_key].close.split(':')[0]
                    let MINUTEC =  (DATA[d_key].close.split(':')[1]).substring(0,2)
                    
                    if(HOURC < 10){
                        HOURC = 0 + HOURC
                    }
        
                    let AMPMC = (DATA[d_key].close.split(':')[1]).substring(4,2)
                    // console.warn('hmmm', MINUTEC);
                    if(AMPMC === 'PM'){
                        let x = HOURC
                        let y = 12
    
                        HOURc = parseFloat(x) + parseFloat(y)
                    } else{
                        HOURc = HOURC
                    }
                    finalC = HOURc.toString()
                    let CLOSE = finalC + ':' + MINUTEC + ':' + '00'
                    // check if open
                    if(TIME > FINAL && TIME < CLOSE ){
                        STATUS = 'Open until ' + DATA[d_key].close
                    
                    }
                    if(TIME < FINAL || TIME > CLOSE){
                        STATUS = 'Closed. Will open at ' + DATA[d_key].open
                    }
                    
                }
                }
                
                
            }
            } else {
            STATUS = 'Not Available'
            }
            
        display =<Text style={styles.desc}>{STATUS}</Text>
        return display
    }
    toggleExpanded = () => {
        this.setState({ collapsed: !this.state.collapsed });
    };
    showOpenHour = (item) => {
        let X = item
        let display

        if(X !== undefined){
            display = 
        <View>
            <View style={{marginBottom:6, flexDirection:'row',  flexShrink:1}}>
                <Text style={styles.textDay}>Monday</Text>
                <Text style={styles.textTime} > : {item.monday.open}</Text>
                {item.monday.open !== 'closed' ? <Text style={styles.textTime}> - {item.monday.close} </Text> : null}
            </View>
            <View style={{marginBottom:6, flexDirection:'row',  flexShrink:1}}>
                <Text style={styles.textDay}>Tuesday</Text>
                <Text style={styles.textTime}> : {item.tuesday.open} </Text>
                {item.tuesday.open !== 'closed' ? <Text style={styles.textTime}> - {item.tuesday.close} </Text> : null}
            </View>
            <View style={{marginBottom:6, flexDirection:'row',  flexShrink:1}}>
                <Text style={styles.textDay}>Wednesday</Text>
                <Text style={styles.textTime}> : {item.wednesday.open}</Text>
                {item.wednesday.open !== 'closed' ?<Text style={styles.textTime}> - {item.wednesday.close} </Text> : null}
            </View>
            <View style={{marginBottom:6, flexDirection:'row',  flexShrink:1}}>
                <Text style={styles.textDay}>Thursday</Text>
                <Text style={styles.textTime}> : {item.thursday.open}</Text>
                {item.thursday.open !== 'closed' ? <Text style={styles.textTime}> - {item.thursday.close} </Text> : null}
            </View>
            <View style={{marginBottom:6, flexDirection:'row',  flexShrink:1}}>
                <Text style={styles.textDay}>Friday</Text>
                <Text style={styles.textTime}> : {item.friday.open}</Text>
                {item.friday.open !== 'closed' ? <Text style={styles.textTime}> - {item.friday.close} </Text> : null }
            </View>
            <View style={{marginBottom:6, flexDirection:'row',  flexShrink:1}}>
                <Text style={styles.textDay}>Saturday</Text>
                <Text style={styles.textTime}> : {item.saturday.open}</Text>
                {item.saturday.open !== 'closed' ? <Text style={styles.textTime}> - {item.saturday.close} </Text> : null}
            </View>
            <View style={{marginBottom:6, flexDirection:'row',  flexShrink:1}}>
                <Text style={styles.textDay}>Sunday</Text>
                <Text style={styles.textTime}> : {item.sunday.open}</Text>
                {item.sunday.open !== 'closed' ? <Text style={styles.textTime}> - {item.sunday.close} </Text> : null}
            </View>

        </View>}



        return display
    }


    catspecilization = () => {
        let disp, cat, sp, spH
        let item = this.props.navigation.state.params.item

        //add new data (specialize_highlight)
        if(item.specialize_highlight !== undefined) {
            spH = item.specialize_highlight.map((h) => (
                <View style={{borderWidth: 0.5, borderColor: '#080880', padding: 4, paddingLeft:6, paddingRight: 6, borderRadius: 8, marginRight: 6,marginBottom:6, flexDirection: 'row', alignItems: 'center',backgroundColor: '#080880'}}>
                    <FontAwesomeIcon icon={faCar} size={12} color={'white'} />
                    <Text style={{marginLeft:3, fontFamily: "ProximaNova-Bold",color: 'white'}} >{h} </Text>
                </View>
            ))
        }


        if(item.category !== undefined){
            cat = item.category.map((x) => (
                <Text style={styles.desc} > - {x}</Text>
            ))
        }
        if(item.specialize !== undefined){
            sp = item.specialize.map((n) => (
                <View style={{borderWidth:0.5, borderColor: 'black', padding:4, paddingLeft:6, paddingRight:6, borderRadius:8, marginRight:6,marginBottom:6,  flexDirection:'row', alignItems:'center'}}>
                    <FontAwesomeIcon icon={faCar} size={12} color={'black'} />
                    <Text style={{marginLeft:3,fontFamily: "ProximaNova-Bold",}} >{n} </Text>
                </View>
            ))

        }
        
        disp = 
            <View style={{flexShrink:1,borderBottomWidth:0.5, borderBottomColor:'#ccc',paddingBottom:10,  paddingTop:10 }}>
                <Text style={styles.title}>Category & Specialization</Text>
                <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between'}}>
                <View style={{flexShrink:1}}>
                    {item.category !== undefined ?  cat : null} 
                    {item.specialize_highlight !== undefined || item.specialize !== undefined ? 
                    <View style={{flexDirection: 'row', marginTop: 15, flexWrap: 'wrap', }}>
                        {item.specialize_highlight !== undefined ? spH : null }
                        {item.specialize !== undefined ? sp : null}
                    </View> 
                    : null}

                  
                </View>
                    
                </View>
            </View>

        return disp 
    }

    amenities = () => {
        let item = this.props.navigation.state.params.item
        let disp
        if(item.amenities !== undefined){
            disp = item.amenities.map((x) => (
                <View style={{borderWidth:0.5, borderColor: '#080880', marginTop:8, padding:4, paddingLeft:6, paddingRight:6, borderRadius:8, marginRight:6, flexDirection:'row', alignItems:'center'}}>
                    <FontAwesomeIcon icon={x.icon} size={12} color={'#080880'} />
                    <Text style={{fontSize:11,marginLeft:3}} >{x.name}</Text>
                </View>


            ))
                
        }

        return disp
    }

    reviews = () => {
        let REVIEWS = this.props.navigation.state.params.item.reviews
        let OVERALL = this.props.navigation.state.params.item.reviews_overall
        let display

        if(OVERALL !== undefined){
            display = 
            <View style={{ marginTop:10, backgroundColor:'#fff', zIndex:3}}>
                <Text >View all reviews ( {OVERALL.count} )</Text>
            </View>
        }  else {
            display = 
            <View style={{ marginTop:10, backgroundColor:'#fff', zIndex:3}}>
                <Text >View all reviews ( 0 )</Text>
            </View>

        }

        return display
    }

    viewReviews = () => {
        let REVIEWS = this.props.navigation.state.params.item.reviews
        let OVERALL = this.props.navigation.state.params.item.reviews_overall
        
        firebase.analytics().logScreenView({
            screen_name: 'SeeReviews',
            screen_class: 'SeeReviews',
          }) 
        this.props.navigation.navigate('WorkshopReviews', {
        reviews_overall: OVERALL,
        reviews: REVIEWS})
    }


    // ===== 
    vehicleList = () => {
        let display
        let DATA = this.state.v_data
        let CAT = this.props.navigation.state.params.item.category
        let CATTYPE
        if(CAT !== undefined){
            if(CAT.includes('Motorcycle')){
                if(CAT.length > 1){
                    CATTYPE = 'None'
                } else {
                    CATTYPE = 'Motorcycle'
                }
            } else {
                CATTYPE = 'Car'
            }
        }
    

        if(DATA.length !== 0){
            display = DATA.map(item =>{ 
                if(item.type_desc === CATTYPE){
                    return(
                        <View>
                        {item.book_walkin === undefined || item.book_walkin._book_id === '' ?
                            <TouchableOpacity
                                onPress={() => {
                                    let that = this;
                                    let model =  this.props.mobx_carkey.setMODEL(item.model);
                                    let make =   this.props.mobx_carkey.setMAKE(item.make);
                                    let carcc =   this.props.mobx_carkey.setCARCC(item.carcc);
                                    let trans =   this.props.mobx_carkey.setTRANSMISSION(item.transmission);
                                    let year =   this.props.mobx_carkey.setYEAR(item.year);
                                    let plate =   this.props.mobx_carkey.setPLATE(item.plate);
    
                                    this.setState({modalVehicle:false})
    
                                    this.scanSQR()
                                }}
                                style={{padding:15, flexDirection:'row', justifyContent:'space-between', alignItems:'center', borderBottomWidth:0.5, borderBottomColor:'#ccc', marginLeft:15, marginRight:15}}>
                                <View style={{height:40, width:40,}}>
                                <Image source={item.type_desc === 'Motorcycle' ? motorEmpty : vehicleEmpty} style={{height:40, width:40}}/>
                                </View>
                                <View>
                                <Text>{item.make} {item.model}</Text>
                                <Text>{item.carcc}L / {item.year}</Text>
                                </View>
                                <View>
                                <Text>{item.plate}</Text>
                                </View>
    
                            </TouchableOpacity>
                        : null }
                    </View> 
                    )
                } 
                if(CATTYPE === 'None') {
                    return (
                        <View>
                            {item.book_walkin === undefined || item.book_walkin._book_id === '' ?
                                <TouchableOpacity
                                    onPress={() => {
                                        let that = this;
                                        let model =  this.props.mobx_carkey.setMODEL(item.model);
                                        let make =   this.props.mobx_carkey.setMAKE(item.make);
                                        let carcc =   this.props.mobx_carkey.setCARCC(item.carcc);
                                        let trans =   this.props.mobx_carkey.setTRANSMISSION(item.transmission);
                                        let year =   this.props.mobx_carkey.setYEAR(item.year);
                                        let plate =   this.props.mobx_carkey.setPLATE(item.plate);
        
                                        this.setState({modalVehicle:false})
        
                                        this.scanSQR()
                                    }}
                                    style={{padding:15, flexDirection:'row', justifyContent:'space-between', alignItems:'center', borderBottomWidth:0.5, borderBottomColor:'#ccc', marginLeft:15, marginRight:15}}>
                                    <View style={{height:40, width:40,}}>
                                    <Image source={item.type_desc === 'Motorcycle' ? motorEmpty : vehicleEmpty} style={{height:40, width:40}}/>
                                    </View>
                                    <View>
                                    <Text>{item.make} {item.model}</Text>
                                    <Text>{item.carcc}L / {item.year}</Text>
                                    </View>
                                    <View>
                                    <Text>{item.plate}</Text>
                                    </View>
        
                                </TouchableOpacity>
                            : null }
                        </View> 
                    )
                }
            })
        }  else {
            display = 
            <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                <Text style={{fontFamily: "ProximaNova-Bold",}} >Please add at least one vehicle first</Text>
            </View>
        } 
        return display
    }


    scanSQR = () => {
        let x = this.state
        let group_id = this.props.navigation.state.params.item.group_id
        let workshop_id = this.props.navigation.state.params.item.workshop_id
        let address =  this.props.navigation.state.params.item.address
        let nameplace =  this.props.navigation.state.params.item.name

        firebase.analytics().logScreenView({
            screen_name: 'ScanWorkshop_details',
            screen_class: 'ScanWorkshop_details',
          }) 
        this.props.navigation.navigate('WorkshopScan', {
            // qrData : FINAL,
            groupID: group_id,
            workshopID: workshop_id,
            address: address,
            nameplace: nameplace,
        })
    }

    bookingAppoint = () => {
        let group_id = this.props.navigation.state.params.item.group_id
        let workshop_id = this.props.navigation.state.params.item.workshop_id
        let address =  this.props.navigation.state.params.item.address
        let nameplace =  this.props.navigation.state.params.item.name


        let MAIN = this.props.mobx_retail.setR_GROUPID(group_id)
        let REATIL = this.props.mobx_retail.setR_ID(workshop_id)
        let ADD =  this.props.mobx_retail.setADDRESS(address)
        let NAME = this.props.mobx_retail.setNAMEPLACE(nameplace)

    
        firebase.analytics().logScreenView({
            screen_name: 'BookApointment_details',
            screen_class: 'BookApointment_details',
          }) 
        this.props.navigation.navigate('WorkshopBookingAppointment', {
            // qrData : FINAL,
            groupID: group_id,
            workshopID: workshop_id,
            address: address,
            nameplace: nameplace,
        })
    }

    callWS = () => {
        let PHONE = this.props.navigation.state.params.item.phone

        if(PHONE === ''){
            Alert.alert('Sorry', 'The workshop haven\'t updated their phone number.')
        } else {
            if (Platform.OS === 'android') { phoneNumber = `tel:${PHONE}`; }
            else {phoneNumber = `telprompt:${PHONE}`; }
            Linking.openURL(phoneNumber);
        }
    }
    
    GMaps = async() =>{
        this.setState({modalMap: false})
        let ADDRESS = this.props.navigation.state.params.item.address
        let LOCATION = this.props.navigation.state.params.item.location 

        firebase.analytics().logScreenView({
            screen_name: 'gmaps_details',
            screen_class: 'gmaps_details',
          }) 

        if(LOCATION !== undefined){
            let LAT = LOCATION.lat
            let LONG = LOCATION.lng

            Linking.openURL('https://www.google.com/maps/search/?api=1&query=' + LAT + ',' + LONG)

        } else {
            if(ADDRESS !== undefined || ADDRESS !== '' ){
                let URL = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + ADDRESS +'&key=AIzaSyDV3WviHkCYt82VA90LiRLLMB8RGQwGSYA'     
                await fetch(URL)
                    .then(data => data.json())
                    .then(data => {
                        let LAT = data.results[0].geometry.location.lat
                        let LONG = data.results[0].geometry.location.lng
            
                            
                        Linking.openURL('https://www.google.com/maps/search/?api=1&query=' + LAT + ',' + LONG)
                    })
            } else {
                Alert.alert('Sorry', 'The workshop hasn\'t updated their location.')
            }
        }
    }
    
    waze = async() => {
        this.setState({modalMap: false})
        let ADDRESS = this.props.navigation.state.params.item.address
        let LOCATION = this.props.navigation.state.params.item.location 

        firebase.analytics().logScreenView({
            screen_name: 'waze_details',
            screen_class: 'waze_details',
          }) 

        if(LOCATION !== undefined){
            let LAT = LOCATION.lat
            let LONG = LOCATION.lng

            Linking.openURL('https://waze.com/ul?ll=' + LAT + ',' + LONG + '&navigate=yes');
        } else {
            if(ADDRESS !== undefined || ADDRESS !== '' ){
                let URL = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + ADDRESS +'&key=AIzaSyDV3WviHkCYt82VA90LiRLLMB8RGQwGSYA' 
        
                await fetch(URL)
                    .then(data => data.json())
                    .then(data => {
                        let LAT = data.results[0].geometry.location.lat
                        let LONG = data.results[0].geometry.location.lng
        
                        Linking.openURL('https://waze.com/ul?ll=' + LAT + ',' + LONG + '&navigate=yes');
                    })
            } else {
                Alert.alert('Sorry', 'The workshop hasn\'t updated their location.')
            }
        }
        
    }


    render(){
        let item = this.props.navigation.state.params.item
        let showItem 
        if(this.props.navigation.state.params.item.display === '3'){
            showItem = 
            <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-around'}}>
             <TouchableWithoutFeedback onPress={() => this.setState({modalVehicle:true})}>
             <View style={{justifyContent:'center', alignItems:'center'}}>
                 <View style={styles.outerCircle}>
                     <MaterialCommunityIcons name='qrcode-scan' size={20} color='white' />
                 </View>
                 <Text style={{fontFamily:'ProximaNova-Bold', marginTop:10, fontSize:15}}>Scan SQR</Text>
             </View>
             </TouchableWithoutFeedback>
 
             <TouchableWithoutFeedback  onPress={this.bookingAppoint} >
                 <View>
                 <View style={{justifyContent:'center', alignItems:'center'}}>
                 
                 <View style={styles.noouterCircle}>
                     <MaterialCommunityIcons name='calendar-month-outline' size={20} color='#080880' />
                 </View>
                 <Text style={{fontFamily:'ProximaNova-Bold', marginTop:10, fontSize:15}}>Appointment</Text>
             </View>
                 </View>
             
             </TouchableWithoutFeedback>
 
             <TouchableWithoutFeedback onPress={this.callWS} >
             <View style={{justifyContent:'center', alignItems:'center'}}>
                 <View style={styles.noouterCircle}>
                     <FontIcon name='phone' size={20} color='#080880' />
                 </View>
                 <Text style={{fontFamily:'ProximaNova-Bold', marginTop:10, fontSize:15}}>Call</Text>
             </View>
             </TouchableWithoutFeedback>
 
             <TouchableWithoutFeedback  onPress={() => this.setState({modalMap: true})}>
                 
             <View style={{justifyContent:'center', alignItems:'center'}}>
                 <View style={styles.noouterCircle}>
                     <MaterialCommunityIcons name='navigation' size={20} color='#080880' />
                 </View>
                 <Text style={{fontFamily:'ProximaNova-Bold',  marginTop:10, fontSize:15}}>Navigate</Text>
             </View>
             </TouchableWithoutFeedback>
 
         </View>
        }


        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                    <View style={styles.top}>
                        <TouchableWithoutFeedback onPress={() => this.props.navigation.goBack()} >
                            <FontIcon name="chevron-left" size={25} color='#fff' />
                        </TouchableWithoutFeedback>
                        <View style={{flexDirection:'row', alignItems:'center', marginTop:15,marginBottom:15 }}>
                            <View style={styles.outerLogo}>
                            {this.state.workshoplogo !== '' 
                            ? <Image source={{uri:this.state.workshoplogo}} style={{...styles.logo, resizeMode:'contain'}}/> 
                            : <Image source={emptyLogo} style={styles.logo}/>}
                            </View>
                            <Text style={{color:'#fff',fontFamily: "ProximaNova-Bold", fontSize:20, marginLeft:15, flexShrink:1}} >{item.name}</Text>
                        </View>




                        <View style={{ margin:15,marginLeft:0, width:Dimensions.get('window').width / 2.4}}>
                            <View style={{flexShrink:1, marginRight:10}}>    
                                <Text style={{color:'#fff', marginBottom:10}} >{item.address} </Text>
                                <View  style={{flexDirection:'row', alignItems:'center'}}>
                                    <Text style={{color:'#fff', fontFamily: "ProximaNova-Bold", fontSize:15,}}>{ item.reviews_overall !== undefined  ? item.reviews_overall.rate.toFixed(1) : '0.0'}</Text>
                                    <FontIcon name='star' color='#ffce00'  size={18} style={{marginLeft:10}}/>
                                </View>
                            </View> 
                        </View>
                        
                        <TouchableWithoutFeedback onPress={this.viewReviews}>
                            <View style={styles.sideBanner}>
                            {item.banner !== '' 
                            ? <Image source= {{uri: item.banner}} style={styles.banner} />
                            : <Image source= {{uri:'https://images.says.com/uploads/story_source/source_image/450525/475e.jpg'}} style={styles.banner} />}
                            {this.reviews()}
                        </View>
                        </TouchableWithoutFeedback>

                        
                    </View>

                    <ScrollView style={{padding:20, paddingTop:10, marginTop:20, zIndex:0}}  >

                        {showItem}
                        
                        <View style={{paddingBottom:10,marginTop:15,borderBottomWidth:0.5, borderBottomColor:'#ccc',}}>
                            <Text style={styles.title}>Description</Text>
                            <Text style={styles.desc}>{item.description !== '' ? item.description : 'Not Available'}</Text>
                            {item.amenities !== undefined ? 
                            <View style={{flexDirection:'row', flexWrap:'wrap', paddingBottom:10}}>
                                {this.amenities()}
                            </View> : null}
                        </View>

                        <View style={{borderBottomWidth:0.5, borderBottomColor:'#ccc',paddingBottom:10, paddingTop:10}}>
                            <TouchableWithoutFeedback onPress={this.toggleExpanded}>
                            <View>
                                <Text style={styles.title}>Opening hours</Text>
                                <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between'}}>
                                    {this.showOpenClose(item.business_hrs)}
                                    {this.state.collapsed === false ? <AntIcon name='up' size={12} style={{justifyContent:'flex-end'}} /> : <AntIcon name='down' size={12} style={{justifyContent:'flex-end'}} /> }
                                </View>
                               
                            </View>
                            </TouchableWithoutFeedback>
                            <Collapsible collapsed={this.state.collapsed} align="center" >
                            <View style={{marginTop:15}}>
                                {this.showOpenHour(item.business_hrs)}
                            
                            </View>
                            </Collapsible>

                        </View>
                        

                        {item.specialize !== undefined || item.specialize_highlight !== undefined ? this.catspecilization() : null}

                        <View style={{paddingBottom:20,paddingTop:10}}>
                            <Text style={styles.title}>Contact Number</Text>
                            <Text style={styles.desc}>{item.phone !== '' ? item.phone : 'Not Available'}</Text>
                        </View>
                    </ScrollView>


                    <BottomModal
                        modalName={this.state.modalMap}
                        onPress={() => this.setState({modalMap:false})}
                        style={styles.innerModal}
                        children = {
                        <View >
                            <View style={{padding:20, paddingBottom:0, alignItems:'center'}}>
                                <Text style={{textAlign:'center', fontFamily:'ProximaNova-Bold', fontSize:20, marginBottom:5}}>Open with</Text>             
                            </View>

                            <View style={{margin:30, marginTop:20,  alignItems:'center', justifyContent:'space-around', flexDirection:'row'}}>
                                <TouchableOpacity  onPress={this.GMaps} style={{}}>
                                    <Image source={maps_logo} style={{height:50, width:50, marginRight:10, resizeMode:'contain'}}/>
                                    <Text style={{textAlign:'center'}}>Maps</Text>
                                </TouchableOpacity>
                                
                                <TouchableOpacity onPress={this.waze} style={{}}>
                                    <Image source={waze_logo} style={{height:50, width:50, resizeMode:'contain'}}/>
                                    <Text style={{textAlign:'center'}}>Waze</Text>
                                </TouchableOpacity>

                            </View>

                            
                            <Button bgColor='#080880' title='Cancel' width={WIDTH} onPress={()=> this.setState({modalMap:false, latlong: ''})}   />
                            <View style={{height:15}} />
                        </View>
                        }
                    />

                    <BottomModal
                        modalName={this.state.modalVehicle}
                        onPress={() => this.setState({modalVehicle:false})}
                        style={styles.innerModal}
                        children = {
                            <View>          
                                <View style={{padding:15,borderBottomColor:'#080880', alignItems:'center',}}>
                                    <Text style={{textAlign:'center', color:'#080880', fontFamily: "ProximaNova-Bold", fontSize:18}}>Select Vehicle</Text>
                                    <Text style={{marginTop:10,textAlign:'center', color:'red', fontFamily: "ProximaNova-Bold", fontSize:10}}>*If your vehicle is not listed here it is because you have already requested a service for it.</Text>
                                </View>
                                
                                
                                <ScrollView style={{height:Dimensions.get('window').height / 2}}>
                                    {this.vehicleList()}
                                </ScrollView>
                      
                          </View>
                        }
                    />


                       
                </View>
            </SafeAreaView>
        )
    }
}

WorkshopDetails = inject('mobx_auth','mobx_retail', 'mobx_carkey', 'mobx_config')(observer(WorkshopDetails))
export default WorkshopDetails

const styles = StyleSheet.create({
    top:{
        backgroundColor:'#080880',
        padding:20,
        borderBottomLeftRadius:20,
        borderBottomRightRadius: 20,
    },
    title: {color:'#6e6d6d', fontSize:13,marginTop:10, marginBottom:10},
    desc: { fontFamily: "ProximaNova-Bold",marginBottom:10, fontSize:15},
    textDay:{width:Dimensions.get('window').width / 4.5, fontFamily: "ProximaNova-Bold",},
    textTime:{fontFamily: "ProximaNova-Bold",},
    outerCircle:{backgroundColor:'#080880', width:50, height:50, borderRadius:25, alignItems:'center', justifyContent:'center',},
    noouterCircle:{borderColor:'#080880', backgroundColor:'#fff', borderWidth:0.5, width:50, height:50, borderRadius:25, alignItems:'center', justifyContent:'center',},
    innerModal: {     
        borderTopLeftRadius: 10,
        borderTopRightRadius:10,
        width:Dimensions.get('window').width,
        position:'absolute',
        bottom:0,
        backgroundColor: '#fff',
            ...Platform.select({
                ios: {
                    shadowColor: '#000',
                    shadowRadius: 2,
                    shadowOffset: { width: 0, height: 3 },
                    shadowOpacity: 0.6,
                },
                android: {
                    elevation: 2
                },
            }),
    },
    logo : {
        height: 60, width:60,resizeMode:'contain', borderRadius:10,
    },

    outerLogo : {
        height: 60, width:60,  backgroundColor:'#fff',borderRadius:10,
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowRadius: 1,
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.4,
            },
            android: {
                elevation: 2
            },
        }),
    },
    sideBanner : {
        position: 'absolute',
        right:15,
        bottom: -18,
        width : Dimensions.get('window').width /2.2,
        backgroundColor:'#fff',
        borderRadius:15,
        padding:10, 
        alignItems:'center',
        justifyContent:'center',
        zIndex: 4, 
        elevation:4,

        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowRadius: 1,
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.4,
            },
            android: {
                elevation: 2
            },
        }),
    },
    banner : {
        height: Dimensions.get('window').width / 4,
        width : Dimensions.get('window').width / 2.6,
        alignSelf:'center'
        // borderRadius:2,

    }

})