import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,RefreshControl,FlatList, Modal, Alert,Dimensions,PermissionsAndroid, TouchableWithoutFeedback, TouchableOpacity, ScrollView, Image, Button, TextInput, Linking, TouchableOpacityBase, ActivityIndicator} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/AntDesign"
import Icon2 from "react-native-vector-icons/FontAwesome5"
import MaterialIcon from "react-native-vector-icons/MaterialIcons";

import Moment from 'moment';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import BottomModal from "../components/BottomModal.js"
import profile from "../assets/emptyProfile.png"
import settings from '../assets/icons/settings.png'
import camera from '../assets/icons/camera.png'
import library from '../assets/icons/library.png'


class Profile extends React.Component{
    constructor(){
        super()
        this.state = {
            userData:'',
            modalPicture : false,
            image :'',
            final : '',
            imageLoader :true,
            uploaderNote: false,
        }
    }

    componentDidMount(){
        this.fetchUser()
        this.fetchImage()
    }

    fetchUser(){
        let that = this
        let FUID = this.props.mobx_auth.FUID 

        firebase.database().ref(`users/${FUID}`).once('value', (snp) => {
            if(snp.exists()){
                let X = snp.val()

                that.setState({userData: X})
            }
        })
    }

    fetchImage(){
        let FUID = this.props.mobx_auth.FUID 
        firebase.database().ref(`users/${FUID}/user_picture`).once('value', (snp) => {
            if(snp.exists()){
                let X = snp.val()

                if(X.downloadURL !== ''){
                    this.setState({image: X, imageLoader: false,uploaderNote: false})
                } else {
                    this.setState({imageLoader: false,uploaderNote: false,})
                    }

                
            }
        })
    }

    refresh_profile = () => {
        this.fetchUser()
    }

    // ======= PROFILE PHOTO =============
    getPermission = async() => {
        if(Platform.OS === 'android'){
            const grantedRead = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
            {
              title: "OPEN LIBRARY PERMISSION",
              message: "App needs access to read external storage for picture uploads",
            }
        );
    
        if (grantedRead === 'granted') {
              this.openLibrary()
          } else {
            Alert.alert('Sorry', 'Please allow access to storage so you can upload photos from phone gallery or storage.')
          }
    
        } else {
            this.openLibrary()
        }
    }

    openCamera = async() => {
        const options = {quality:1 ,maxWidth: 800, maxHeight: 800}
    
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                    title: "App Camera Permission",
                    message:"App needs access to your camera to snap photos",
                   
                }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                //console.log('Camera permission given')    
                launchCamera(options, (response) => {
                    this.setState({modalPicture: false})
                        
                    if (response.didCancel) {
                        this.setState({modalPicture: false})
                    } if (response.error) {
                        Alert.alert('Sorry', "Please try again");
                        this.setState({modalPicture: false})
                    } 
                    if(!response.didCancel && !response.error) {
                        const sourceCamera = response.assets[0].uri;
                        this.setState({
                            image: sourceCamera,  // just take the uri only from react native image picker 
                            modalPicture: false,
                        });
                        this.updateImg()
                    
                    }
                    
                });
            } else {
                console.log('Camera permission denied')
            }
        } catch (error) {
            console.warn(error)
        }

    }
        
    openLibrary = () => {
        const options = {quality:1,maxWidth: 800, maxHeight: 800 }
    
            launchImageLibrary(options, (response) => {
            this.setState({modalPicture: false})
    
                if(response.didCancel){
                    this.setState({modalPicture: false})
                } 
                    if (response.error) {
                    Alert.alert('Sorry', "Please try again");
                    this.setState({modalPicture: false})
                    } 
                    if(!response.didCancel && !response.error) {
                        const source = response.assets[0].uri;
                        this.setState({
                            image: source,
                            modalPicture: false,
                        });
                        this.updateImg()
                    }
            });
    
    }
        
    updateImg = async() => {
        let FUID = this.props.mobx_auth.FUID;
        let MONTH = (new Date().getMonth()) + 1
        this.setState({ imageLoader:true,uploaderNote: true})
        const imageuri= this.state.image;
        const uploadURI = Platform.OS === 'ios' ? imageuri.replace('file://', '') : imageuri;
        let TIME = Date.now()
        const refFile = firebase.storage().ref().child(`profile_pic/${FUID}/${TIME}`);

        await refFile.putFile(uploadURI);
        const url = await refFile.getDownloadURL().then((url) => {
                let IMAGE_URL = url;  
                firebase.database().ref(`users/${FUID}/user_picture`).update({downloadURL: IMAGE_URL})  
                firebase.database().ref(`flat_users/${FUID}/picture`).update({downloadURL: IMAGE_URL});

                this.setState({image: IMAGE_URL, })
                this.fetchImage()
            })
            .catch(error => {
                let m = error.message
                firebase.database().ref(`crash_error/${MONTH}/`).push({
                    error: m,
                    timestamp:Date.now(),
                    user: FUID,
                    platform :Platform.OS,
                    version : this.props.mobx_config.Version
                })
                Alert.alert('Sorry', 'An error occured. Please contact SERV Team if the issue still persists');
            });

    }
    
    goBack = () => {
        this.props.navigation.state.params.refresh_photo()
        this.props.navigation.goBack(null)
      }

    render(){
        let renderImage 
        let x = this.state.userData
        console.warn('hooii',this.state.image);

        if(this.state.image !== '' && this.state.image !== undefined ){
            renderImage = <Image source={{uri:this.state.image.downloadURL }} style={styles.image} />
        } 
        if(this.state.imageLoader === true) {
            renderImage = 
            <View style={{...styles.image, alignItems:'center', justifyContent: 'center'}} >
                <ActivityIndicator color = '#080880' size = "large"/>
            </View>
        }
        if(this.state.image === '' && this.state.imageLoader === false) {
            renderImage = 
            // <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('ProfileEdit', {data: this.state.userData, refresh_profile: this.refresh_profile})}  >
                <View style={styles.image} >
                <Image source={profile} style={styles.image} />
                </View>
            // </TouchableWithoutFeedback>
        }

        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>

                <ScrollView style={{ marginTop:0}} showsVerticalScrollIndicator={false} >
                    <View >
                        {renderImage}
                        <TouchableOpacity style={{...styles.boxIcon, left:20, top:20}}  onPress={this.goBack} >
                            <Icon2 name='chevron-left' color='#080880'  size={28} /> 
                        </TouchableOpacity>
                        <TouchableOpacity style={{...styles.boxIcon, right:20, top:20}}  onPress={() => this.props.navigation.navigate('Settings')} >
                            <Image source={settings} style={{height:32, width:32}} />
                        </TouchableOpacity>
                        
                        <View style={{flexDirection:'row', position:'absolute', bottom:20, left:20,width:Dimensions.get('window').width - 40, }}>
                            <View style={{width:Dimensions.get('window').width /1.5, flexShrink:1}} >
                                <Text style={{...styles.textShadow, fontSize:25, color:'#fff',fontWeight:'bold', fontFamily:'ProximaNova-Bold', marginBottom:6}} >{x.name} </Text>
                                <Text numberOfLines={1}  style={{...styles.textShadow,fontSize:15, color:'#fff',fontStyle:'italic', fontWeight:'bold',}} >{x.bio !== '' && x.bio !== undefined ? x.bio : ''}</Text>
                            </View>
                        <TouchableOpacity style={{...styles.boxIcon,right:0}}   onPress={() => this.setState({modalPicture: true})} >
                            <Icon name='camera' color='#080880' size={20} />
                        </TouchableOpacity>
                        </View>
                    </View>

                    <View style={{height:50, backgroundColor:'#080880', borderBottomLeftRadius: 40}}/>
                    
                    <View style={{margin:20, }}>
                        <TouchableWithoutFeedback onPress={() => {
                            firebase.analytics().logScreenView({
                                screen_name: 'Edit_Profile1',
                                screen_class: 'Edit_Profile1',
                              })
                            this.props.navigation.navigate('ProfileEdit', {data: this.state.userData, refresh_profile: this.refresh_profile})}}>
                            <View style={{flexDirection: 'row', alignItems:'center', justifyContent:'space-between', marginTop:10, marginBottom:4, marginLeft:5, marginRight:5}}>
                            <Text style={{fontSize:18, fontFamily: "ProximaNova-Bold", }}>My Profile</Text>
                            <Icon name='edit' size={20} />
                            </View>
                        </TouchableWithoutFeedback>


                        <View style={styles.box}>
                            <View style={styles.viewTitle}>
                                <Text style={styles.smolFont}>NAME</Text>
                                <Text style={styles.bigFont}>{x.name} </Text>
                            </View>
                            <View style={styles.viewTitle}>
                                <Text style={styles.smolFont}>BIRTHDAY</Text>
                                <Text style={styles.bigFont}>{x.DOB !== '' ? x.DOB : 'Not Available'} </Text>
                            </View>
                            <View style={styles.viewTitle}>
                                <Text style={styles.smolFont}>GENDER</Text>
                                <Text style={styles.bigFont}>{x.gender !== '' && x.gender !== undefined ? x.gender : 'Not Available'} </Text>
                            </View>
                            <View>
                                <Text style={styles.smolFont}>MEMBERSHIP</Text>
                                <Text style={styles.bigFont}>Since {Moment(x.register_timestamp).format("MMMM YYYY")} </Text>
                            </View>
                        </View>




                    
                    </View> 
                   
                    <View style={{margin:20,marginTop:0 }}>
                        <TouchableWithoutFeedback onPress={() => {
                            firebase.analytics().logScreenView({
                                screen_name: 'Edit_Profile2',
                                screen_class: 'Edit_Profile2',
                              })
                            this.props.navigation.navigate('ProfileEdit2', {data: this.state.userData, refresh_profile: this.refresh_profile})}}>
                            <View style={{flexDirection: 'row', alignItems:'center', justifyContent:'space-between', marginTop:10, marginBottom:4, marginLeft:5, marginRight:5}}>
                            <Text style={{fontSize:18, fontFamily: "ProximaNova-Bold", }}>Contact Info</Text>
                            <Icon name='edit' size={20} />
                            </View>
                        </TouchableWithoutFeedback>
                        <View style={styles.box}>
                            <View style={styles.viewTitle}>
                                <Text style={styles.smolFont}>EMAIL</Text>
                                <Text style={styles.bigFont}>{x.email} </Text>
                            </View>
                            <View style={styles.viewTitle}>
                                <Text style={styles.smolFont}>PHONE</Text>
                                <Text style={styles.bigFont}>{x.Phone} </Text>
                            </View>
                            <View style={styles.viewTitle}>
                                <Text style={styles.smolFont}>ADDRESS</Text>
                                <Text style={styles.bigFont}>{x.Address !== '' ? x.Address : 'Not Available'} </Text>
                            </View>
                            <View style={styles.viewTitle}>
                                <Text style={styles.smolFont}>CITY</Text>
                                <Text style={styles.bigFont}>{x.City !== '' ? x.City : 'Not Available'} </Text>
                            </View>
                            <View style={styles.viewTitle}>
                                <Text style={styles.smolFont}>POSTAL CODE</Text>
                                <Text style={styles.bigFont}>{x.PostalCode !== '' ? x.PostalCode : 'Not Available'} </Text>
                            </View>
                            <View style={styles.viewTitle}>
                                <Text style={styles.smolFont}>STATE</Text>
                                <Text style={styles.bigFont}>{x.State !== '' ? x.State : 'Not Available'} </Text>
                            </View>
                            <View >
                                <Text style={styles.smolFont}>COUNTRY</Text>
                                <Text style={styles.bigFont}>{x.Country !== '' ? x.Country : 'Not Available'} </Text>
                            </View>
                        </View>
                    </View>

                    </ScrollView>

                    <BottomModal
                        modalName={this.state.modalPicture}
                        onPress={() => this.setState({modalPicture:false})}
                        style={styles.innerModal}
                        children = {
                        <View >
                            <View style={{padding:20, paddingBottom:0, alignItems:'center'}}>
                                <Text style={{textAlign:'center', fontFamily:'ProximaNova-Bold', fontSize:20, marginBottom:5}}>Update Photo</Text>             
                            </View>

                            <View style={{margin:30, marginTop:20,  alignItems:'center', justifyContent:'space-around', flexDirection:'row'}}>
                                <TouchableOpacity  onPress={this.openCamera} style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                                    <Image source={camera} style={{height:40, width:40}} />
                                    <Text style={{textAlign:'center'}}>Camera</Text>
                                </TouchableOpacity>
                            
                            
                                <TouchableOpacity  onPress={this.getPermission} style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                                    <Image source={library}  style={{height:40, width:40}}  />
                                    <Text style={{textAlign:'center'}}>Library</Text>
                                </TouchableOpacity>

                            </View>
                        </View>
                        }
                    />

                    {this.state.uploaderNote === true ? 
                        <View style={styles.bottomPop}>
                            <Text>Updating Image ... </Text>
                        </View>   
                    : null }
                    
                </View>
            </SafeAreaView>
        )
    }
}

Profile = inject('mobx_auth', 'mobx_config')(observer(Profile))
export default Profile

const styles = StyleSheet.create({
    boxIcon:{
        position:'absolute', backgroundColor:'#fff', borderRadius:5, height:40, width:40, alignItems:'center', justifyContent:'center',
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowRadius: 1,
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.4,
            },
            android: {
                elevation: 2
            },
        }),
    },
    image:{height:Dimensions.get('window').width/1.5, width:Dimensions.get('window').width, },
    box:{ backgroundColor:'#fff', padding:15, borderRadius:10, marginTop:10,borderWidth:1, borderColor:'#ccc',},
    viewTitle: {marginBottom:20},
    smolFont: {fontSize:11, marginBottom:5, color:'#6e6d6d',},
    bigFont: {fontFamily:'ProximaNova-Bold' },

    leftIcon: { borderRadius:15, height:30, width:30, marginRight:15},
    rightSide : {borderBottomWidth:0.5, borderBottomColor:'#ccc', paddingBottom: 8, width:'80%'},
    textShadow :{
        padding:2,
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 5
    },
    innerModal: {     
        borderTopLeftRadius: 10,
        borderTopRightRadius:10,
        width:Dimensions.get('window').width,
        position:'absolute',
        bottom:0,
        backgroundColor: '#fff',
            ...Platform.select({
                ios: {
                    shadowColor: '#000',
                    shadowRadius: 2,
                    shadowOffset: { width: 0, height: 3 },
                    shadowOpacity: 0.6,
                },
                android: {
                    elevation: 2
                },
            }),
    },
    bottomPop : {
        position: 'absolute', bottom:20, backgroundColor:'#fff', alignSelf:'center', padding:15, borderRadius:20,
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowRadius: 1,
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.4,
            },
            android: {
                elevation: 2
            },
        }),
    },

})