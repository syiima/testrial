import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/AntDesign"
import FeaIcon from 'react-native-vector-icons/FontAwesome';

import Header from '../components/Header.js'

const months = {
    'January' : 'Jan',
    'February' : 'Feb',
    'March' : 'Mar',
    'April' : 'Apr',
    'May' : 'May',
    'June' : 'June',
    'July' : 'July',
    'August' : 'Aug',
    'September' : 'Sept',
    'October' : 'Oct',
    'November' : 'Nov',
    'December' : 'Dec'
}

class TrackActivities extends React.Component{

    showActivities = () => {
        let display 
        let DATA = this.props.navigation.state.params.activity

        if(DATA.length !== 0){
            display =  DATA.map((item) => (
                
                <TouchableWithoutFeedback >
                <View style={styles.ongoing}>      
                    <View style={{flexDirection: 'row',justifyContent:'space-between', alignItems:'center' }}>
                        <View style={{flex:3,  }}>
                        <Text style={{fontFamily: "ProximaNova-Bold", fontSize:18}}>{item.carplate}</Text>

                        <View style={{flexDirection:'row'}}>
                            <Text style={{ fontSize:12}}>{item.carmake} </Text>
                            <Text style={{ fontSize:12}}>{item.carmodel} </Text>
                        </View>
                        
                        <Text style={{ fontSize:12}} >Ref: {item._requestID.toUpperCase()}</Text>
                        <Text style={{ fontSize:12}} >Category: {item.type}</Text>
                        </View>
                        <View style={{flex:2, }}>
                        
                        {   
                            item.book_status === 'Paid' && item.type === 'On-The-Go' ? <Text style={{...styles.stats, color:'green', }}>QR Code</Text>  :
                            item.book_status === 'Requesting' && item.type === 'On-The-Go' ? <Text style={{...styles.stats, color:'#c49e00', }}>Sending Quotation</Text> :
                            item.book_status === 'Unread' && item.type === 'On-The-Go' ? <Text style={{...styles.stats, color:'green', }}>Quotation Ready</Text> :
                            item.book_status === 'Unpaid' || item.book_status === 'Complete' ? <Text style={{...styles.stats, color:'#e85b1a', }}>Awaiting Payment</Text> :

                            item.book_status === 'Appointment Accepted' || item.book_status === 'Appointment'  && item.type === 'Drive-In' ? <Text style={{...styles.stats, color:'green'}}>View Appointment</Text> :
                            item.book_status ==='Appointment Declined' && item.type === 'Drive-In' ? <Text style={{...styles.stats, color:'#e85b1a'}}>Appointment Declined</Text> :
                            item.book_status ==='Appointment Changed' && item.type === 'Drive-In' ? <Text style={{...styles.stats, color:'#c49e00'}}>Appointment Adjusted</Text> :
                            item.book_status === 'Requesting' && item.type === 'Drive-In' ? <Text style={{...styles.stats, color:'#c49e00'}}>Undergoing Inspection</Text> :
                            item.book_status === 'Accepted' && item.type === 'Drive-In' ? <Text style={{...styles.stats, color:'green'}}>Performing Service</Text> :
                            item.book_status === 'Pending' && item.type === 'Drive-In' ? <Text style={{...styles.stats, color:'#c49e00'}}>Review Quotation</Text> :
                            item.book_status === 'Cancel' && item.type === 'Drive-In' ? <Text style={{...styles.stats, color:'#e85b1a'}}>Pending Cancellation</Text> :


                            <Text style={{...styles.stats,color:'green'}}>Rate Service</Text>
                        }

                        <Text style={{fontFamily:'ProximaNova-Bold', color:'#080880', textAlign:'right'}}>{item.day} {months[item.month]} {item.year}</Text>
                        <Text style={{fontFamily:'ProximaNova-Bold', fontSize:11, color:'#080880', textAlign:'right'}}>{item.time}</Text>
                        </View>

                        <View style={{flex:0.5, }} />
                        <FeaIcon name="angle-right" size={20} style={{position:'absolute', right:10, bottom:15,}} />

                    </View>
                </View>

                </TouchableWithoutFeedback>
            ))
        } else {
            display = 
            <View style={{alignItems:'center', justifyContent:'center', flexShrink:1,  marginLeft:15, marginRight:15}}>
                <Text style={{fontSize:11,marginTop:15, textAlign:'center'}}>No current ongoing service. Book now. </Text>
            </View>
        }


        return display 
    }

    render(){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
            <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Activities' onPress={() => this.props.navigation.goBack()} />
                    
                    <ScrollView>
                        {this.showActivities()}

                        <View style={{height:30}} />
                    </ScrollView>
                    
                </View>
            </SafeAreaView>
        )
    }
}

TrackActivities = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(TrackActivities))
export default TrackActivities

const styles = StyleSheet.create({
    ongoing: {
        padding:15,
        margin:15,
        marginBottom:0,
        backgroundColor:'#fff',
        borderRadius:10,
        ...Platform.select({
            ios: {
              shadowColor: '#000',
              shadowRadius: 2,
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 0.4,
            },
            android: {
              elevation: 1
            },
          }),
      },

    stats :{fontFamily:'ProximaNova-Bold', textAlign:'right'}


})