import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Image,Dimensions, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

import Moment from 'moment';

import Header from '../components/Header.js'
import MiddleModal from '../components/MiddleModal.js'
import NotificationLoader from "../components/NotificationLoader.js";
import emptyNti from '../assets/icons/emptyNoti.png'

class OpenModal extends React.Component{
    render(){
        return(
            <MiddleModal modalName ={this.props.isOpen}
                children = {
                    <View>
                        <View>
                            <Text style={{fontSize:24,fontFamily:'ProximaNova-Bold',marginBottom:20}}>{this.props.itemTitle}</Text>
                            <Text style={{fontSize:14}}>{this.props.itemDescription} </Text>
                            <Text style={{color:'grey', fontSize:12, marginTop:20}}>{Moment(this.props.itemDate).format("DD MMM")} at {Moment(this.props.itemDate).format("HH:MM A" )}</Text>
                        </View>

                        <TouchableWithoutFeedback onPress={this.props.handleClose}>
                            <View style={{backgroundColor:'#080880', width:40, height:40, borderRadius:20, alignItems:'center', alignSelf:'flex-end', justifyContent:'center', marginTop:40}}>
                                <MaterialCommunityIcons name='check' size={20} color='white' />
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                }
            />
        )
    }
}

class InboxNotifications extends React.Component{
    constructor(){
        super()
        this.state = {
            got_noti: false,
            notifications: '',
            loadHighlights: true,
            modalNoti: false,
            title: '',
            description: '',
            timestamp:'',
        }
    }
    
    componentDidMount(){
        this.fetchNotifications()
    }
    
    fetchNotifications(){
        // this.setState({loadHighlights:true})
        let that = this
        let FUID = this.props.mobx_auth.FUID
        let NOTI = [];

        firebase.database().ref(`users/${FUID}/personal_notification`).once('value').then((snapshot) => {
            if(snapshot.exists()){
                KEY = Object.keys(snapshot.val());

                KEY.forEach( (key_id) => {
                    let a = snapshot.val()[key_id];
                    a.key = key_id;
                    if(a.unread === false){
                        that.setState({got_noti: true})
                    }
                    
                    NOTI.push(a);

                })
                let SORTED = NOTI.sort((a,b) => b.timestamp - a.timestamp)
                that.setState({notifications:SORTED, loadHighlights :false});            
            } else {
                that.setState({notifications:'',loadHighlights:false })
            }
        })
        
    }

    // ===== 
    openNoti = (item) => {
        let FUID = this.props.mobx_auth.FUID
        let KEY = item.key
        firebase.database().ref(`users/${FUID}/personal_notification/${KEY}/unread/`).set(true)

        this.setState({
            title: item.title,
            description: item.body,
            timestamp: item.timestamp,
            modalNoti: true,
        })
    }

    closeModal=() => {
        this.fetchNotifications()
        this.setState({
            modalNoti: false,
            title: '',
            description: '',
            timestamp: ''
        
        })
    }


    renderNoti = () => {
        let DATA = this.state.notifications
        let dispaly

        dispaly = DATA.map((item, index) => (
            // <TouchableWithoutFeedback onPress={() =>  this._openNoti(item)}>
            <TouchableWithoutFeedback onPress={() =>  this.openNoti(item)}>
            <View style={styles.box}>
                {!item.unread ? 
                <View style={styles.circle}>
                    <Icon name='envelope' size={20} color='#080880' />
                </View>
                : 
                <View style={styles.circle}>
                    <Icon name='envelope-open' size={20} color='#080880' />
                </View> }
                <View style={{ flexDirection:'column', flexShrink:1, marginLeft:15}}>
                    <Text style={{ fontFamily: 'ProximaNova-Bold',fontSize: 16}}>{item.title}  </Text>
                    <Text style={styles.topText}>{item.body}  </Text>
                        <Text style={{marginTop:3, color:'grey', fontSize:11}}>{Moment(item.timestamp).format("DD MMM")} at {Moment(item.timestamp).format('HH:MM A')} </Text>
                </View>
            </View>
            </TouchableWithoutFeedback>
        ))
        return dispaly
    }

    render(){
        let content 
        if( this.state.loadHighlights === false && this.state.notifications !== '') {
            content = 
            <ScrollView >
                <OpenModal handleClose={this.closeModal} isOpen={this.state.modalNoti} itemTitle={this.state.title} itemDescription={this.state.description} itemDate={this.state.timestamp}  />
                {this.renderNoti()}
            </ScrollView>
        }
        if( this.state.loadHighlights === false && this.state.notifications === '') {
            content = 
            <View style={{flex:1, alignItems:'center', justifyContent:'center',  marginTop:-40}}>
                <Image source={emptyNti} style={{height: 200, width:200, resizeMode:'contain', }} />
                <Text style={{fontSize:22, fontFamily: "ProximaNova-Bold", marginBottom:10}}>No Notifications</Text>
                <Text style={{color:'#a7a7a7'}} >We'll notify you when we have updates for you.</Text>
            </View>
        }
        if(this.state.loadHighlights === true){
            content = <NotificationLoader />
        }
        
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'Notifications' onPress={() => this.props.navigation.goBack()} />
                    
                    {content}
                    
                </View>
            </SafeAreaView>
        )
    }
}

InboxNotifications = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(InboxNotifications))
export default InboxNotifications

const styles = StyleSheet.create({
    box :{
        flexDirection:'row',
        borderBottomColor:'#ccc',
        borderBottomWidth:1,
        padding:15,
    },
    circle :{borderColor:'#080880', borderWidth:1, borderRadius: 20,  height: 40, width:40, alignItems:'center', justifyContent:'center'},
    topText :{fontSize:13, marginTop: 6},

})