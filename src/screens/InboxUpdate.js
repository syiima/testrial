import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/AntDesign"
import AppLink from 'react-native-app-link';


import Header from '../components/Header.js'
import Button from "../components/Button"
import upgrade from '../assets/update.png'

const WIDTH = Dimensions.get('window').width - 60

class InboxUpdate extends React.Component{
    constructor(){
        super()
        this.state = {
            updated: true, 
            latest: '',
            loading: false,
        }
    }
    
    componentDidMount(){
        this.versionChecker()
    }

    componentWillUnmount(){
        // firebase.database().ref('consts/version_code').off()
    }
    

    versionChecker = () => {
        let that = this;
        let APP_VERSION = this.props.mobx_config.Version;
        APP_VERSION = parseInt(APP_VERSION.split(".").join(""))

        firebase.database().ref('consts/version').once('value', (snapshot) => {
            if(snapshot.exists()){

                let X = snapshot.val()
                X = parseInt(X.split('.').join(''))

                console.warn('soo', X, APP_VERSION);

                if(APP_VERSION < X){
                    this.setState({updated: false, loading: false,})
                    
                } else {
                    this.setState({updated: true, loading:false,})
                }
            }
        })
    }
    updateApp = () => {
        AppLink.openInStore({ appName:'Serv', appStoreId:'1284497301', appStoreLocale:'my', playStoreId:'my.serv.customer' }).then(() => {
        })
        .catch((err) => {
        Alert.alert('Sorry', 'An error occured. Please contact SERV Team if the issue still persists');
        // handle error
        });
    }

    recheck = () => {
        this.setState({loading: true})
        this.versionChecker()
    }
    
    render(){
        let content 

        let buttonUp 
        if(this.state.loading === true){
            buttonUp =  
            <Button
                title='Checking ... '
                bgColor='#080880'
                marginTop={35}
                width={WIDTH}
            />
        } else {
            buttonUp = 
            <Button
                onPress={this.recheck}
                title='Check again'
                bgColor='#080880'
                marginTop={35}
                width={WIDTH}
            />
        }

        if(this.state.updated === true){
            content = 
            <View style={{ alignItems:'center', justifyContent:'center'}}>
                <Text style={{fontFamily: "ProximaNova-Bold",fontSize:18,}}>You are up-to-date!</Text>
                <Text style={{fontSize:14,margin:15, marginLeft:30, marginRight:30}}>Good news, looks like you are on the latest version of SERV running on your device.</Text>
                {buttonUp}
            </View>
        } else { 
            content= 
            <View style={{ alignItems:'center', justifyContent:'center'}} >
                <Text style={{fontFamily: "ProximaNova-Bold",fontSize:18, padding:15}}>There's a new app update release</Text>
                <Text style={{fontSize:14,margin:15,  marginLeft:30, marginRight:30}}>We fixed some bugs to make your experience as smooth as possible</Text>

                <Button
                    onPress={this.updateApp}
                    title='Update app now'
                    bgColor='#080880'
                    marginTop={35}
                    width={WIDTH}
                  />
            </View>
        }
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                <Header backButton = {true}  headerTitle = 'App Update' onPress={() => this.props.navigation.goBack()} />
                    
                <View style={{flex:1, marginTop:30}} >
                    <Image source={upgrade} style={styles.img} />
                    {content}
                </View>
                    
                </View>
            </SafeAreaView>
        )
    }
}

InboxUpdate = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(InboxUpdate))
export default InboxUpdate

const styles = StyleSheet.create({
    img: {
        width: Dimensions.get('window').width - 80,
        height:Dimensions.get('window').width / 2,
        marginBottom: 30,
        resizeMode:'contain',
        alignSelf:'center'
      },
})