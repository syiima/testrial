import React from "react"
import { StyleSheet, SafeAreaView,Platform, Text, View,Alert,Dimensions,RefreshControl,ActivityIndicator,PermissionsAndroid, TouchableWithoutFeedback,Image, TouchableOpacity, ScrollView} from "react-native"
import { inject, observer } from "mobx-react"
import firebase from '@react-native-firebase/app'
import Icon from "react-native-vector-icons/AntDesign"
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import MaterialIcon from "react-native-vector-icons/MaterialIcons";
import Moment from 'moment';
import AsyncStorage from '@react-native-async-storage/async-storage';
import DatePicker from 'react-native-datepicker';
import Calendar from 'react-native-calendar-datepicker';
import {cloud_serv_user_sandbox, cloud_serv_user_production} from "../util/FirebaseCloudFunctions.js"

import Header from '../components/Header.js'
import BottomModal from "../components/BottomModal.js"
import HealthReport from "../components/HealthReport.js"
import HealthReportMotor from "../components/HealthReportMotor.js"

import MiddleModal from "../components/MiddleModal.js"
import FormTextInput from "../components/FormTextInput.js"

import vehicles from '../assets/emptyCar.png'
import wait from '../assets/icons/step2.png'
import announcement from '../assets/icons/announcement.png'
import record from '../assets/icons/record.png'
import roadtax from '../assets/icons/roadtax.png'
import file from '../assets/icons/file.png'
import camera from '../assets/icons/camera.png'
import library from '../assets/icons/library.png'
import motorEmpty from '../assets/emptyMotor.png'


type State = {
    date?: Moment,
  };
  
  
class VehicleProfile extends React.Component{
    state: State
    constructor(){
        super()
        this.state = {
            isLoading: false,
            ownership: false,
            status: '',
            cardata: '',
            count: 0,
            carphoto: '',
            new_picture: '',
            publish_state:false,
            photoe:false,
            verification_data: '',
            request_data: '',
            modalPicture: false,
            health_check: false,
            modalRoadTax: false,
            roadtax_expiry_date :'',
            allow_view : true,
            verification: '',
            removable: '',
            verification_reason: '',
            tokenU: '',
            secretU: '',
            vehicle_plate: '',
            modalCar: false
        }
    }
    
    componentDidMount(){
        this.check()
        this.fetchImage()
        this.statusListener()
        this.getCSRFToken()
    }

    componentWillUnmount(){
        let FUID = this.props.mobx_auth.FUID;
        let PLATE = this.props.mobx_carkey.PLATE
        firebase.database().ref(`users/${FUID}/vehicles/${PLATE}`).off()
    }

    statusListener(){
        let that = this
        let FUID = this.props.mobx_auth.FUID;
        let PLATE = this.props.mobx_carkey.PLATE
        firebase.database().ref(`users/${FUID}/vehicles/${PLATE}/`).on('child_changed', (snp) => {
            if(snp.exists()){
                that.check()
            }
        })
    }
    
    check = () => {
        this.setState({isLoading:true})
    
        let that = this;
        let FUID = this.props.mobx_auth.FUID;
        let PLATE = this.props.mobx_carkey.PLATE
        
        firebase.database().ref(`users/${FUID}/vehicles/${PLATE}`).once('value', (snp) => {
          if(snp.exists()){
            let B = snp.val()
            let OWNERSHIP = B.true_owner
            let STATUS = B.status
    
            that.setState({ownership:OWNERSHIP, status: STATUS}, () => that.fetchCarProfile())
    
          }
        })
    }

    getCSRFToken = async() => {
        let tokenU =  await AsyncStorage.getItem('CSRFTokenU')
        let secretU =  await AsyncStorage.getItem('CSRFSecretU')
        this.setState({tokenU : tokenU, secretU : secretU})
    }
    
    
    fetchCarProfile(){
    let that = this;
    let PLATE = this.props.mobx_carkey.PLATE;
    let COUNT = 0

    firebase.database().ref(`plate_number/${PLATE}`).once('value').then( (snapshot) => {
        if(snapshot.exists()){
        let x = snapshot.val();
        if(x.user !== undefined){
            let key = Object.keys(snapshot.val().user)
            key.forEach((key_id) => {
            let STATUS = snapshot.val().user[key_id].relationship
            if(STATUS !== 'not owner'){
                COUNT = COUNT + 1
            }
            })
        }

        that.setState({cardata: x, count: COUNT})
        that.checkReq()
        } else {
        that.setState({isLoading:false})
        }
    })
    }

    checkReq = () => {
    
        let that = this
        let PLATE = this.props.mobx_carkey.PLATE
    
        let FUID = this.props.mobx_auth.FUID
        let STATUS = this.state.status
        let OWNERSHIP = this.state.ownership
        let DATA = []
    
    
        if(OWNERSHIP === false){
            firebase.database().ref(`plate_number_request/${PLATE}/users/${FUID}`).once('value', (snp) => {
                if(snp.exists()){
                    let A = snp.val()
    
                    if(A.status !== 'pending'){ this.setState({removable: 'request'}) }
                    if(A.status !== 'approved'){ this.setState({allow_view: false}) }
    
                    
    
                    this.setState({request_data : A, isLoading: false })
    
                } else {
                  this.setState({isLoading: false})
                }
            })
        } else {
            //check req
            firebase.database().ref(`plate_number_request/${PLATE}`).once('value', (mnp) => {
                if(mnp.exists()){
                    let B = mnp.val().users 
                    KEY = Object.keys(B);
            
                    KEY.forEach( (uid) => {
                        let a = B[uid];
                        a.uid = uid;
                        
                        if(a.status === 'pending'){
                            DATA.push(a)
                        }
                    
                    })
    
                    this.setState({request_data: DATA, isLoading: false})
      
                } else {
                    this.setState({isLoading: false})
                }
            })
    
            //check ver 
            if(STATUS !== 'approved'){
                let TOFUID = this.state.cardata.owner.FUID 
                if(TOFUID !== FUID && STATUS !== 'verify' ){this.setState({allow_view :false})}
    
                firebase.database().ref(`plate_number_verification/${PLATE}/${FUID}`).once('value', (snap) => {
                    if(snap.exists()){
                        let C = snap.val()
                        // console.warn('neinnn', C.status);
                        if(C.status === 'approved' || C.status === 'reject'){
                          this.setState({removable: 'verification'}) 
                        }
    
                        this.setState({verification_data: C, isLoading: false})
                        
                    } else {
                        this.setState({isLoading: false})
                    }
                })
            }
        }
    
    }
    

    fetchImage(){
        let FUID = this.props.mobx_auth.FUID;
        let PLATE = this.props.mobx_carkey.PLATE;
    
        firebase.database().ref(`plate_number/${PLATE}/picture_car/downloadURL`).once('value').then((snapshot) => {
          if(snapshot.exists()){
            let X = snapshot.val()
            if(X !== ''){
              Y = true
            } else{
              Y = false
            }

            
            this.setState({carphoto: X, publish_state: false,photoe: Y})
          } else {
            this.setState({carphoto: '' , publish_state :false, photoe: false })
          }          
        })
    
    }
    

    //================ UPLOAD IMAGE CAR================
    openCamera =async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                    title: "App Camera Permission",
                    message:"App needs access to your camera to snap photos",
                }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                //console.log('Camera Permission given')
                const options = {quality:0.5, maxWidth:500, maxHeight:500 }

                launchCamera(options, (response) => {
                    this.setState({modalPicture: false})
                
                    if(response.didCancel){
                        this.setState({modalPicture: false})
                    }
                    if (response.error) {
                        Alert.alert('Sorry', "Please try again");
                        this.setState({modalPicture: false})
                    } if(!response.didCancel && !response.error) {
                        const source = response.assets[0].uri;
                        this.setState({
                            new_picture: source,
                            modalPicture: false,
                        });
                        if(this.state.photoe === true){this.updateImage()} else {this.publish()}
                    }
                });
            } else {
                console.log('Camera permission denied ')
            } 
        } catch (error) {
            console.warn(error)
        }

    }
      
    openLibrary = () => {
        const options = {quality:0.5, maxWidth:500, maxHeight:500 }

        launchImageLibrary(options, (response) => {
            this.setState({modalPicture: false})
            
            if(response.didCancel){
                // console.warn('kenapa cancel');
                this.setState({modalPicture: false})
            } 
            if(!response.didCancel && !response.error) {
                const source = response.assets[0].uri;
                this.setState({
                    new_picture: source,
                    modalPicture: false,
                });
                if(this.state.photoe === true){this.updateImage()} else {this.publish()}
            }
        });

    }
    
    updateImage = () => {
        this.setState({
            publish_state:true,
        })
        let logoRef = this.state.carphoto
        let FUID = this.props.mobx_auth.FUID;
        let PLATE = this.props.mobx_carkey.PLATE;

        var URLref = firebase.storage().refFromURL(logoRef);

        firebase.database().ref(`plate_number/${PLATE}/picture_car`).remove()
        firebase.database().ref(`users/${FUID}/vehicles/${PLATE}/picture_car`).remove()

        // Delete the file
        URLref.delete().then(function() {
        // File deleted successfully
        }).catch(function(error) {
        }).then(() => {
            firebase.database().ref(`plate_number/${PLATE}/picture_car`).update({downloadURL: ""})
        }).then(() => {
            this.publish()
        })

    }
      
    publish = async() => {
        this.setState({
            publish_state:true,
        })
        let FUID = this.props.mobx_auth.FUID;
        let PLATE = this.props.mobx_carkey.PLATE;

        const imageuri= this.state.new_picture;

        const URLProCar = Platform.OS === 'ios' ? imageuri.replace('file://', '') : imageuri;
        const refFile= await firebase.storage().ref().child(`car_pic/${FUID}/${PLATE}`);
        await refFile.putFile(URLProCar);
        const result = await refFile.getDownloadURL().then(url => {
                let IMAGE_URL = url;

                firebase.database().ref(`users/${FUID}/vehicles/${PLATE}/picture_car`).update({downloadURL: IMAGE_URL});
                firebase.database().ref(`plate_number/${PLATE}/picture_car`).update({downloadURL: IMAGE_URL});
    
                Alert.alert('Success', "Your photo has been changed!");
                this.fetchImage()
            })
            .catch(error => {
                Alert.alert('Sorry', 'An error occured. Please contact SERV Team if the issue still persists');
            });

    }
    
    
    refresh = () => {
        this.setState({isLoading: true})
        this.fetchCarProfile()
    }
    //===== ROADTAX ============
    renderRoadtax = () => {
    
        let DATA = this.state.cardata.car_health
        let display
        let FINALDATE
    
        if(DATA !== undefined){
          if(DATA.roadtax_expiry_date !== undefined  && DATA.roadtax_expiry_date !== ''){
            let M = Moment(DATA.roadtax_expiry_date).format('MM/DD/YYYY')
    
            if(M === 'Invalid date'){
              let DATE = DATA.roadtax_expiry_date.split('-')[0]
              let MONTH = DATA.roadtax_expiry_date.split('-')[1]
              let YEAR = DATA.roadtax_expiry_date.split('-')[2]
    
               FINALDATE = MONTH + '/' + DATE + '/' + YEAR 
            } else {
               FINALDATE = M
            }
           
    
            let TODAY_DATE = Moment().format('MM/DD/YYYY')
            let TODAYEPOCH = new Date(TODAY_DATE).getTime()/1000.0
            // let TODAY_MONTH = Moment().format('MM')
            // let TODAY_YEAR = Moment().format('YYYY');
            // console.warn('yell', FINALDATE, M);
    
            // let 
            let USERDATE = new Date(FINALDATE).getTime()/1000.0
    
            if(TODAYEPOCH > USERDATE){
              display=
                <TouchableWithoutFeedback onPress={() => this.setState({modalRoadTax: true})}>
                  <View style={{...styles.box, borderWidth:1, borderColor:'red'}} >
                    <View style={styles.iconView}>
                        <Image source={roadtax} style={styles.icon}  />
                    </View>
                      
                    <View style={{flexShrink:1, flex:3,marginLeft:10, marginRight:10 }}>
                      <Text style={{fontFamily:'ProximaNova-Bold', fontSize:13, marginBottom:10}}>Oh oh.. Past the expiry date!</Text>
                      <Text style={{fontSize:11}}>Expires on {DATA.roadtax_expiry_date}</Text>
                    </View>   
    
    
                  </View>
                  </TouchableWithoutFeedback>
              
            } else {
              // console.warn('fuuuu');
              display = 
              <TouchableWithoutFeedback onPress={() => this.setState({modalRoadTax: true})}>
              <View style={{...styles.box, borderWidth:1, borderColor:'#008000'}} >
                    <View style={styles.iconView}>
                        <Image source={roadtax} style={styles.icon}  />
                    </View>
                <View style={{flexShrink:1, flex:3, marginLeft:10, marginRight:10 }}>
                  <Text style={{fontFamily:'ProximaNova-Bold', fontSize:13, marginBottom:10}}>Hooray! It's still valid</Text>
                  <Text style={{fontSize:11}}>Expires on {this.state.cardata.car_health.roadtax_expiry_date}</Text>
                </View>   
    
    
              </View>
              </TouchableWithoutFeedback>
            }
      
          } else {
            display = 
          <TouchableWithoutFeedback onPress={() => this.setState({modalRoadTax: true})}>
          <View style={{...styles.box, borderWidth:1, borderColor:'#FB9800', backgroundColor:'#FB9800'}} >
            <View style={styles.iconView}>
                <Image source={roadtax} style={styles.icon}  />
            </View>
              
            <View style={{flexShrink:1, flex:3, marginLeft:10, marginRight:10 }}>
              <Text style={{fontFamily:'ProximaNova-Bold', fontSize:13, marginBottom:10, color:'#fff'}}>Oops.. Looks like there's no record.</Text>
              <Text style={{fontSize:11, color:'#fff'}}>Tap here to update.</Text>
            </View>   
    
    
          </View>
          </TouchableWithoutFeedback>
          }
        } else {
          display = 
          <TouchableWithoutFeedback onPress={() => this.setState({modalRoadTax: true})}>
          <View style={{...styles.box, borderWidth:1, borderColor:'#FB9800', backgroundColor:'#FB9800'}} >
            <View style={styles.iconView}>
                <Image source={roadtax} style={styles.icon}  />
            </View>
              
            <View style={{flexShrink:1, flex:3, marginLeft:10, marginRight:10 }}>
              <Text style={{fontFamily:'ProximaNova-Bold', fontSize:13, marginBottom:10, color:'#fff'}}>Oops.. Looks like there's no record.</Text>
              <Text style={{fontSize:11, color:'#fff'}}>Book a service to keep track</Text>
            </View>   
    
    
          </View>
          </TouchableWithoutFeedback>
    
    
        }
    
        // if(this.state.car_data.car_health !== undefined){
        //   roadtax_car = 
          
    
    
          return display
    
    }

    saveRoadTax = () => {
        let FUID = this.props.mobx_auth.FUID
        let PLATE = this.props.mobx_carkey.PLATE;
        let X = this.state.date
    
        let DATE = Moment(X).format('DD-MM-YYYY')
        let SDATE = Moment(X).format('DD-MMMM-YYYY')
        
        this.setState({modalRoadTax: false})
    
        //1.update data
        // console.warn('ehhh', SDATE);
        firebase.database().ref(`plate_number/${PLATE}/car_health`).update({roadtax_expiry_date : SDATE})
        
        // 2.update to reminder
        let MONTH = (DATE.split('-')[1]) - 1
        let FDAY
        // if (DAY < 10 ){FDAY =  DAY[1]} else {FDAY = DAY}
        // let FMONTH
        let DAY = (DATE.split('-')[0])
        if (DAY < 10 ){FDAY =  DAY[1]} else {FDAY = DAY}
        
        
    
        let YEAR = DATE.split('-')[2]
        let P_TOKEN
        // console.warn('year',YEAR, MONTH, FDAY);
    
        firebase.database().ref('users/' + FUID).once('value').then((snapshot) => {
          if(snapshot.exists()){
            P_TOKEN = snapshot.val().pushToken
            // console.warn('hello', 'reminder_roadtax/' + YEAR + '/' + FMONTH + '/' + DAY + '/' + FUID, P_TOKEN);
    
            firebase.database().ref('reminder_roadtax/' + YEAR + '/' + MONTH + '/' + FDAY + '/' + FUID).update({
              pushToken: P_TOKEN,
              plate: this.props.mobx_carkey.PLATE,
            })
          
          }
        })
        
    
        this.fetchCarProfile()
       
    }

    //=== plate related === 
    cancelRequest = () => {
        let FUID = this.props.mobx_auth.FUID
        let PLATE = this.props.mobx_carkey.PLATE 
    
        firebase.database().ref(`plate_number_request/${PLATE}/users/${FUID}/`).remove()
        firebase.database().ref(`users/${FUID}/vehicles/${PLATE}/`).remove() 
    
        this.props.navigation.goBack()
    }

    goBack = () => {
        let PLATE = this.props.mobx_carkey.PLATE 
        let FUID = this.props.mobx_auth.FUID 
        let STATUS = this.state.status 
        
    
        if(this.state.removable === 'request'){
          if(STATUS === 'reject'){
            firebase.database().ref(`plate_number_request/${PLATE}/users/${FUID}/`).remove()
            firebase.database().ref(`users/${FUID}/vehicles/${PLATE}/`).remove()
          } else {
            firebase.database().ref(`plate_number_request/${PLATE}/users/${FUID}/`).remove()
          } 
        }
    
        if(this.state.removable === 'verification'){
          let TOFUID = this.state.cardata.owner.FUID 
    
          if(STATUS === 'reject' && TOFUID !== FUID ){
            firebase.database().ref(`plate_number_verification/${PLATE}/${FUID}/`).remove()
            firebase.database().ref(`users/${FUID}/vehicles/${PLATE}/`).remove()
            firebase.database().ref(`plate_number/${PLATE}/user/${FUID}/`).remove()
          } else {
            firebase.database().ref(`plate_number_verification/${PLATE}/${FUID}/`).remove()
          }
    
        }
      
    
        this.props.navigation.goBack(null)
        
    }
    
    //==== delete car func ===== 
    handleChange = (text) => {
        let T = text.replace(/[`~!@#$%^&*()_|+\-=?\s;:'",.<>\{\}\[\]\\\/]/gi, '')
        let TP = T.trim()
        TP = TP.toUpperCase()
      
        this.setState({vehicle_plate: TP})
    }

    closeModal = () => {
        this.setState({modalCar: false, vehicle_plate: ''})
    }

    sendNotiRemove = () => {
        let PLATE = this.props.mobx_carkey.PLATE
        let FUID = this.props.mobx_auth.FUID
        
        let URL, TEXT, BODY, Q_FUID, Q_VEHICLE_ID
        
        if (this.props.mobx_config.config === 0) URL = cloud_serv_user_sandbox.personalNotifications
        if (this.props.mobx_config.config === 1) URL = cloud_serv_user_production.personalNotifications
        
        firebase.database().ref(`plate_number/${PLATE}/user`).once('value', (snp) => {
            if(snp.exists()){
            let B = snp.val()
            KEY = Object.keys(B);

            KEY.forEach( (uid) => {
                TEXT = '?text=Vehicle removed'
                BODY = `&body=The owner of ${PLATE} has removed this vehicle. If this vehicle is yours, you may re-add this vehicle if you would like to have access to this vehicle.`  
            
                Q_FUID = `&userKey=${uid}`
                Q_VEHICLE_ID = `&vehicle_id=${PLATE}`


                FINAL_URL = URL + TEXT + BODY + Q_FUID + Q_VEHICLE_ID

                fetch(FINAL_URL,{
                credentials:true, 
                headers: {
                    csrftoken: this.state.tokenU,
                    csrfsecret: this.state.secretU
                }
                }).then(() => {
                firebase.database().ref(`users/${uid}/vehicles/${PLATE}/`).remove()
                })
            })
            } 
        })
        .then(() => {
            firebase.database().ref(`users/${FUID}/vehicles/${PLATE}/`).remove()
            firebase.database().ref(`plate_number_request/${PLATE}/`).remove()
            firebase.database().ref(`plate_number_verification/${PLATE}/`).remove()
            firebase.database().ref(`plate_number/${PLATE}/`).remove()
        })
        .then(() => {
            this.setState({modalCar: false})
            this.props.navigation.popToTop();
        })
    }

    removeCheck = () => { 
        let FUID = this.props.mobx_auth.FUID;
        let PLATE = this.props.mobx_carkey.PLATE;
        let OWNERSHIP = this.state.ownership 
        let STATUS = this.state.status 
    
        firebase.analytics().logEvent('button_pressed', { name: 'remove_vehicle'})
    
        firebase.database().ref(`plate_number/${PLATE}/book_walkin/`).once('value').then((snapshot) => {
          if(snapshot.exists()){
            let X = snapshot.val()
    
            if(X._book_id === ''){
              this.setState({modalCar:true})
            } else {
              Alert.alert('Has active booking.', 'Please clear booking related first before deleting this vehicle. You can check TRACK page for booking details.')
              return
            }
          } else {
            this.setState({modalCar:true})
          }
        })
             
    }

    removeVehicle = () => {
        firebase.analytics().logEvent('button_pressed', { name: 'confirm_remove_vehicle'})
    
        let that = this;
        if(this.state.vehicle_plate === '' ){
          Alert.alert('Sorry','Please type in your vehicle\'s plate number')
          return
        } else {
          if(this.state.vehicle_plate === this.props.mobx_carkey.PLATE){
            let PLATE = this.props.mobx_carkey.PLATE;
            let UID = this.props.mobx_auth.FUID;
        
            if(this.state.ownership === false){
              firebase.database().ref('users/' + UID + '/vehicles/' + PLATE).remove()
              firebase.database().ref(`plate_number/${PLATE}/user/${UID}/`).remove()
    
              that.setState({modalCar: false})
              that.props.navigation.popToTop();
    
            } else {
              this.sendNotiRemove()
            }    
          } else {
            Alert.alert('Oh-oh', 'It seems you\'re trying to delete car with a different plate number  ')
          }
    
        }
    }

      

    
    // ==== check  shared ==== 
    request_shared = () => {
        let REQ = this.state.request_data 
        let OWNER = this.state.ownership
        let STATUS = this.state.status
        let VER = this.state.verification

        let disp 
        //approve/reject
        if(OWNER === true && REQ.length !== 0){  
            disp = 
            <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('VerificationRequest', {data: REQ, refresh_profile: this.check })}>
                <View style={{margin:15,backgroundColor:'#fce8c5', marginBottom:0, borderRadius:10, padding:15,alignItems:'center', flexDirection:'row', }}>
                {/* <Image source ={warningplate} style={styles.insideC} /> */}
                <View style={{marginLeft:25, marginRight:10, flexShrink:1}}>
                <Text style={{fontSize:14, }}>Someone has requested to be the shared owner of this vehicle.</Text>
                <Text style={{fontSize:14,color:'#080880', marginTop:6 }}>Tap here to approve or reject the request.</Text>

                </View>
                </View>
            </TouchableWithoutFeedback>
        }

        //pendign approval owner
        if(OWNER === true && STATUS === 'pending'){
        disp = 
            <View style={{margin:15,backgroundColor:'#fce8c5', marginBottom:0, borderRadius:10, padding:15,alignItems:'center', flexDirection:'row', }}>
            {/* <Image source ={warningplate} style={styles.insideC} /> */}
            <View style={{marginLeft:25, marginRight:10, flexShrink:1}}>
            <Text style={{fontSize:14 }}>You have requested to get verified as the true owner</Text>
            <Text style={{fontSize:14,color:'#080880', marginTop:6 }}>Please wait for your request to be approved by the SERV Team.</Text>

            </View>
            </View>
        }

        if(OWNER === true && STATUS === 'verify'){
        disp = 
            <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('VerificationGrant', { refresh_profile: this.check})}>
            <View style={{margin:15,backgroundColor:'#fce8c5', marginBottom:0, borderRadius:10, padding:15,alignItems:'center', flexDirection:'row', }}>
            {/* <Image source ={warningplate} style={styles.insideC} /> */}
            <View style={{marginLeft:25, marginRight:10, flexShrink:1}}>
            <Text style={{fontSize:14, }}>Someone claimed to be the true owner of this vehicle</Text>
            <Text style={{fontSize:14,color:'#080880', marginTop:6 }}>Tap here to upload your vehicle's registration certificate and get verified as the true owner </Text>

            </View>
            </View>
            </TouchableWithoutFeedback>
        }

        return disp 

    }

    mid_content = () => {
        let display, img, disp 
        let STATUS = this.state.status
        let OWNERSHIP = this.state.ownership 
        let PLATE = this.props.mobx_carkey.PLATE
    
        let VERIFICATION = this.state.verification_data
        let REQUEST = this.state.request_data
    
        //1. requester (pending, false )
        if(REQUEST !== ''){
          if(STATUS === 'pending'){
            img = <Image source={wait} style={{height:Dimensions.get('window').height / 4, width:Dimensions.get('window').width - 60,resizeMode:'contain', alignSelf:'center'}}/>
              display = 
              <View style={{margin:20}}>
                  <Text style={{fontSize:16, fontFamily: "ProximaNova-Bold", color:'#080880',marginBottom:6}}>Awaiting Approval from the true owner</Text>
                  <Text style={{fontSize:14, }}>You have requested to be a co-owner of this vehicle.</Text>
                  <Text style={{fontSize:14, marginTop:6 }}>Sit back, relax and please wait for your request to be approved by the true owner</Text>
              </View>
          } 
          if(STATUS === 'reject'){
              img =  <Image source={wait} style={{height:Dimensions.get('window').height / 4, width:Dimensions.get('window').width - 60,resizeMode:'contain', alignSelf:'center'}}/>
    
              display = 
              <View style={{margin:20}}>
                  <Text style={{fontSize:16, fontFamily: "ProximaNova-Bold", color:'#080880',marginBottom:6}}>Co-owner Request Rejected</Text>
                  <Text style={{fontSize:14, }}>Your request to be a co-owner of {PLATE} has been rejected by the true owner </Text>
                  <View style={{flexDirection:'row', marginTop:15, flexWrap:'wrap'}}>
                  <Text style={{fontSize:14,color:'tomato',fontFamily:'ProximaNova-Bold'}}>Reason for rejection : </Text>
                  <Text style={{fontSize:14,color:'tomato',}}>{REQUEST.reason}</Text>
                  </View>
    
                  <Text style={{fontSize:13,marginTop:15 }}>Rejected {Moment(REQUEST.timestamp_reject).fromNow()}</Text>
              </View>
          }
            
        }
    
        //2. not owner(pending, true)
        if(VERIFICATION !== ''){
          if(STATUS === 'pending'){
            img =  <Image source={wait} style={{height:Dimensions.get('window').height / 4, width:Dimensions.get('window').width - 60,resizeMode:'contain', alignSelf:'center'}}/>
    
              display = 
              <View style={{margin:20}}>
                  <Text style={{fontSize:16, fontFamily: "ProximaNova-Bold", color:'#080880',marginBottom:6}}>Awaiting Approval from SERV Team</Text>
                  <Text style={{fontSize:14, }}>You have requested to get verified as the owner of this vehicle.</Text>
                  <Text style={{fontSize:14,marginTop:6, }}>Sit back, relax and please wait for your request to be approved by the SERV Team within 3-5 business days</Text>
              </View>
          }
          if(STATUS === 'reject'){
            img = <Image source={wait} style={{height:Dimensions.get('window').height / 4, width:Dimensions.get('window').width - 60,resizeMode:'contain', alignSelf:'center'}}/>
    
            if(VERIFICATION.reason === ''){
                display = 
                <View style={{margin:20}}>
                  <Text style={{fontSize:16, fontFamily: "ProximaNova-Bold", color:'#080880',marginBottom:6}}>Verification Request Rejected</Text>
                  <Text style={{fontSize:14, }}>Your request to get verified as the true owner has been rejected by the SERV Team. If this is a mistake, please contact the SERV Team.</Text>
                  <Text style={{fontSize:13,color:'ccc',fontFamily:'ProximaNova-Bold', marginTop:15 }}>Rejected {Moment(VERIFICATION.timestamp_reject).fromNow()}</Text>
                </View>
            } else {
                display = 
                <View style={{margin:20}}>
                  <Text style={{fontSize:16, fontFamily: "ProximaNova-Bold", color:'#080880',marginBottom:6}}>Request Rejected </Text>
                  <Text style={{fontSize:14, }}>Your request to get verified as the true owner has been rejected by the SERV Team. If you think this is a mistake, please contact the SERV Team.</Text>
                  <View style={{flexDirection:'row', marginTop:15, flexWrap:'wrap' }}>
                  <Text style={{fontSize:14,color:'tomato',fontFamily:'ProximaNova-Bold', }}>Reason for rejection : </Text>
                  <Text style={{fontSize:14,color:'tomato', }}>{VERIFICATION.reason}</Text>
                  </View>
    
                  <Text style={{fontSize:13, marginTop:15 }}>Rejected {Moment(VERIFICATION.timestamp_reject).fromNow()}</Text>
    
                </View>
            }
          }
        }
    
    
        disp = 
        <View>
          {img}
          {display}
        </View>
    
        return disp 
    }
    
    
    displayHistory = () => {
        let display 
        let DATA = this.state.cardata
       

        if(DATA !== undefined && DATA.length !== 0){
            if(DATA.type_desc === 'Motorcycle'){
                display = 
                <TouchableWithoutFeedback onPress={() => {
                    // firebase.analytics().setCurrentScreen('Car_Records', 'Car_Records')
                    this.props.navigation.navigate('VehicleHistoryManual')}}>
                    <View style={{...styles.box, backgroundColor:'#fff'}} >
                    <View style={styles.iconView}>
                        <Image source={record} style={{height:20, width:20}}  />
                    </View> 
                    <View style={{flexShrink:1, marginLeft:10, marginRight:10 }}>
                        <Text style={{fontFamily:'ProximaNova-Bold', fontSize:13, marginBottom:10, }}>View & add your vehicle records to learn your vehicle's history</Text>
                        <Text style={{fontSize:11, }}>Tap to add and view</Text>
                    </View>   
    
    
                    </View>
                </TouchableWithoutFeedback>
            } else {
                display = 
                <TouchableWithoutFeedback onPress={() => {
                    firebase.analytics().logScreenView({
                        screen_name: 'Car_Records',
                        screen_class: 'Car_Records',
                      })
                    this.props.navigation.navigate('VehicleHistory', {history:DATA.book_history})}}>
                    <View style={{...styles.box, backgroundColor:'#fff'}} >
                    <View style={styles.iconView}>
                        <Image source={record} style={{height:20, width:20}}  />
                    </View>
                        
                    <View style={{flexShrink:1, marginLeft:10, marginRight:10 }}>
                        <Text style={{fontFamily:'ProximaNova-Bold', fontSize:13, marginBottom:10, }}>Previous services and more. Learn the history of your vehicle</Text>
                        <Text style={{fontSize:11, }}>Tap to view</Text>
                    </View>   
    
    
                    </View>
                </TouchableWithoutFeedback>
            }
    
        }
        return display 
    }  

    render(){
        const BLUE = '#080880';
        const WHITE = '#FFFFFF';
        const GREY = '#BDBDBD';
        const BLACK = '#424242';
        const LIGHT_GREY = '#fafdff';

        let x = this.state.cardata
        let PLATE = this.props.mobx_carkey.PLATE

        let vehImage
        console.warn('ehh', this.state.carphoto);
        if(this.state.carphoto !== ''){
            vehImage= <Image source={{uri:this.state.carphoto}} style={styles.img}/>
        } 
        if(this.state.carphoto === '' ){
            vehImage= <Image source={x.type_desc === 'Motorcycle' ? motorEmpty : vehicles} style={{...styles.img,width:Dimensions.get('window').width / 2.2, resizeMode:'cover'}}/>
        }
        if(this.state.publish_state === true){
            vehImage= 
            <View style={{...styles.img, alignItems:'center', justifyContent: 'center'}} >
                <ActivityIndicator color = '#080880' size = "small"/>
            </View>
        }

        let content 
        if(x !== undefined &&  this.state.isLoading === false && this.state.allow_view === true){
            content = 
            <ScrollView showsVerticalScrollIndicator={false} >
            <View style={{marginLeft:15, marginRight:15}} >
                <View style={{...styles.box, marginTop:15, flexDirection:'column'}} >
                    <View style={{flexDirection:'row',justifyContent:'space-between',marginLeft:-5,marginBottom:15,}}>
                        <View style={{width:'60%'}}>
                            {vehImage}
                            <TouchableOpacity
                                onPress={() => this.setState({modalPicture: true})}
                                style={styles.cameraShadow}>
                                <Icon name="camera" color="#080880" size={15}  />
                            </TouchableOpacity >
                        </View>
                       <View style={{marginLeft:15,justifyContent:'center', flexWrap:'wrap', flexShrink:1 }} >
                       <View style={{marginBottom:15}} >
                            <Text style={styles.title} >Plate </Text>
                            <View style={{flexDirection:'row', flexWrap:'wrap'}}>
                            <Text style={styles.titleDesc}>{PLATE} </Text>
                            </View>
                            
                        </View> 
                        <View style={{marginBottom:15,flexShrink: 1 }} >
                            <Text style={styles.title}>Model</Text>
                            <Text style={styles.titleDesc}>{x.make} {x.model} </Text>
                        </View> 
                        <View style={{marginBottom:15}} >
                            <Text style={styles.title}>Colour</Text>
                            <Text style={styles.titleDesc}>{x.colour !== '' ? x.colour : 'Not Available' } </Text>
                        </View> 
                       </View>
                       
                    </View>
                    <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', }} >
                        <View style={{width:Dimensions.get('window').width / 2, marginLeft:10}}>
                            <Text style={{fontSize:16, fontFamily: "ProximaNova-Bold",}} >{x.nickname !== undefined ? x.nickname : ''}</Text>
                        </View>

                        <TouchableWithoutFeedback onPress={() => {
                            firebase.analytics().logScreenView({
                                screen_name: 'Car_details',
                                screen_class: 'Car_details',
                              }) 
                            this.props.navigation.navigate('VehicleDetails', {referesh: this.refresh})} } >
                            <View style={styles.detsButton} >
                                <Text style={{color:'#fff',fontSize:15,textAlign:'center', fontFamily: "ProximaNova-Bold",}} >View details</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </View>
            </View>


            {this.request_shared()}
            <View style={{margin:15, marginBottom:0,}}>
                <Text style={{fontSize:18, fontFamily: "ProximaNova-Bold", marginTop:10, marginBottom:4, marginLeft:5}}>Vehicle Health</Text>
                {x.type_desc === 'Motorcycle' ? <HealthReportMotor navigation={this.props.navigation} health_check={this.state.health_check} data={this.state.cardata} />
                : <HealthReport navigation={this.props.navigation} health_check={this.state.health_check} data={this.state.cardata} /> }
            </View>

        
            <View style={{margin:15,marginBottom:0,}} >
                <Text style={{fontSize:18, marginBottom:15,fontFamily: "ProximaNova-Bold", }}>Malaysian Road Tax</Text>
                {this.renderRoadtax()}
            </View>

            <View style={{margin:15,marginBottom:0,}} >
                <Text style={{fontSize:18, marginBottom:15,fontFamily: "ProximaNova-Bold", }}>Records & History</Text>
                {this.displayHistory()}
            </View>
            
            <View style={{margin:15,marginBottom:0,}} >
                <Text style={{fontSize:18, marginBottom:15,fontFamily: "ProximaNova-Bold", }}>Vehicle Registration</Text>
                <TouchableWithoutFeedback onPress={() => {
                firebase.analytics().logScreenView({
                    screen_name: 'Car_Registration',
                    screen_class: 'Car_Registration',
                  }) 
                this.props.navigation.navigate('VehicleRegistration')}}>
                    <View style={{...styles.box, backgroundColor:'#fff'}} >
                    <View style={styles.iconView}>
                        <Image source={file} style={styles.icon}  />
                    </View>
                        
                    <View style={{flexShrink:1, marginLeft:10, marginRight:10 }}>
                        <Text style={{fontFamily:'ProximaNova-Bold', fontSize:13, marginBottom:10, }}>Keep track of your vehicle's registration</Text>
                        <Text style={{fontSize:11, }}>Tap to add and view</Text>
                    </View>   


                    </View>
                </TouchableWithoutFeedback>

            </View>
            <View style={{margin:15,marginBottom:0,}} >
                <Text style={{fontSize:18, marginBottom:15,fontFamily: "ProximaNova-Bold", }}>Insurance Policy</Text>
                <TouchableWithoutFeedback onPress={() => {
                firebase.analytics().logScreenView({
                    screen_name: 'Car_InsurancePolicy',
                    screen_class: 'Car_InsurancePolicy',
                  }) 
                this.props.navigation.navigate('VehicleInsurance')}}>
                    <View style={{...styles.box, backgroundColor:'#fff'}} >
                    <View style={styles.iconView}>
                        <Image source={file} style={styles.icon}  />
                    </View>
                        
                    <View style={{flexShrink:1, marginLeft:10, marginRight:10 }}>
                        <Text style={{fontFamily:'ProximaNova-Bold', fontSize:13, marginBottom:10, }}>Keep track of your vehicle's insurance policy here</Text>
                        <Text style={{fontSize:11, }}>Tap to add and view</Text>
                    </View>   


                    </View>
                </TouchableWithoutFeedback>

            </View>
            

            

            {this.state.ownership === true && this.state.status === 'approved' && this.state.count > 0 ? 
            <View style={{marginLeft:15, marginRight:15,}}>
                    <Text style={{fontSize:18, fontFamily: "ProximaNova-Bold", marginTop:10, marginBottom:4, marginLeft:5}}>Ownership</Text>
                    <TouchableWithoutFeedback onPress={() => {
                    this.props.navigation.navigate('VehicleOwnership', {data:x})}}>
                    <View style={{...styles.box, backgroundColor:'#fff'}} >
                    <View style={styles.iconView}>
                        <Image source={announcement} style={styles.icon}  />
                    </View>
                        
                    <View style={{flexShrink:1, flex:3, marginLeft:10, marginRight:10 }}>
                        <Text style={{fontFamily:'ProximaNova-Bold', fontSize:13, marginBottom:10, }}>Keep track of your vehicles ownership here</Text>
                        <Text style={{fontSize:11, }}>Tap to view</Text>
                    </View>   


                    </View>
                    </TouchableWithoutFeedback>

                </View> : null}


            <TouchableWithoutFeedback onPress={this.removeCheck} >
                <Text style={{fontSize:18, marginBottom:20, marginTop:20, textAlign:'center', color:'#e85b1a', fontFamily: "ProximaNova-Bold", }}>Remove my vehicle</Text>
            </TouchableWithoutFeedback>           

        </ScrollView>

        }
        //middle page for req
        if(this.state.allow_view === false &&  this.state.isLoading === false){
            content = 
            <ScrollView showsVerticalScrollIndicator={false} refreshControl={
            <RefreshControl refreshing={this.state.isLoading} onRefresh={this._onRefresh_status} /> }>
    
            {this.mid_content()}
    
            <View style={{margin:20, marginTop:20}}>
            <Text style={{fontSize:14, fontFamily: "ProximaNova-Bold", }}>Vehicle Details</Text>
                <View style={styles.smolBox}>
                    <View style={styles.viewTitle}>
                        <Text style={styles.smolFont}>MAKE </Text>

                        <Text style={styles.bigFont}> {x.make} </Text>
                    </View>
                    <View style={styles.viewTitle}>
                        <Text style={styles.smolFont}>MODEL </Text>
                        <Text style={styles.bigFont}> {x.model} </Text>
                    </View>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Text style={styles.smolFont}>CC </Text>
                        <Text style={styles.bigFont}> {x.carcc} </Text>
                    </View>
                </View>
    
                <TouchableWithoutFeedback onPress={this.goBack}>
                    <View style={{backgroundColor: '#080880',
                    padding:15,
                    alignSelf:'center',
                    alignItems:'center',
                    marginTop:40,
                    width:CONST_WIDTH - 40,
                    borderRadius:8,}}>
                    <Text style={{fontFamily:'ProximaNova-Bold', color:'white', textAlign:'center'}}>OK, I understand</Text>
                    </View>
                </TouchableWithoutFeedback>
                {this.state.ownership === false && this.state.status === 'pending' ?
                    <TouchableWithoutFeedback onPress={this.cancelRequest}>
                    <View style={{borderColor: 'tomato',
                        borderWidth:1,
                        padding:15,
                        alignSelf:'center',
                        alignItems:'center',
                        marginTop:15,
                        marginBottom:30,
                        width:CONST_WIDTH - 40,
                        borderRadius:8,}}>
                        <Text style={{fontFamily:'ProximaNova-Bold', color:'tomato', textAlign:'center'}}>Cancel my request</Text>
                    </View>
                    </TouchableWithoutFeedback>
    
                : null}
    
            </View>
    
    
            </ScrollView>
        }
  
  

        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#1d1d1d'}}>
                <View style={{flex:1,backgroundColor:'#fafdff', }}>
                    
                    <Header backButton = {true}  headerTitle = 'My Vehicle' onPress={() => this.props.navigation.goBack()} />

                    {content}

                    <MiddleModal modalName={this.state.modalRoadTax}
                        children = {
                            <View>
                                <Text style={{fontWeight:'bold', marginTop:15, fontSize:13,color:'#686868' }}>Set a date for your road tax expiry</Text>
                                <Text style={{color:'#686868', fontSize:12, marginTop:5, marginBottom:10, }}>Please tap on the year/month to change year/month.</Text>
                                
                                <View style={{padding:10, paddingLeft: 10, marginBottom:19, borderRadius: 6,borderColor:'#080880', borderWidth:2}}>
                                    <Calendar
                                    onChange={(date) => this.setState({date})}
                                    selected={this.state.date}
                                    //finalStage="month" / + 1 so that they cant pick today to do servicing
                                    minDate={Moment().startOf('day')}
                                    maxDate={Moment().add(20, 'years').startOf('day')}
                                    //General Styling}
                                    style={{
                                    borderRadius: 5,
                                    alignSelf: 'center',
                                    marginLeft:50,
                                    marginRight:50,
                                    marginTop:10,
                                    marginBottom:15,
                                    width:'100%'
                                    }}
                                    barView={{
                                    padding: 15,

                                    }}
                                    barText={{
                                    fontFamily: "ProximaNova-Bold",
                                    color: BLUE,
                                    fontSize:16
                                    }}
                                    stageView={{
                                    padding: 0,
                                    }}

                                    // Day selector styling
                                    dayHeaderView={{
                                    borderBottomColor: WHITE,
                                    }}
                                    dayHeaderText={{
                                    fontFamily: "ProximaNova-Bold",
                                    color: GREY,
                                    }}
                                    dayRowView={{
                                    borderColor: LIGHT_GREY,
                                    height: 40,
                                    }}
                                    dayText={{
                                    color: BLACK,
                                    }}
                                    dayDisabledText={{
                                    color: GREY,
                                    }}
                                    dayTodayText={{
                                    fontFamily: "ProximaNova-Bold",
                                    color: BLUE,
                                    }}
                                    daySelectedText={{
                                    fontFamily: "ProximaNova-Bold",
                                    backgroundColor: BLUE,
                                    color: WHITE,
                                    borderRadius: 15,
                                    borderColor: "transparent",
                                    overflow: 'hidden',
                                    }}
                                    // Styling month selector.
                                    monthText={{
                                    color: BLACK,
                                    borderColor: BLACK,
                                    }}
                                    monthDisabledText={{
                                    color: GREY,
                                    borderColor: GREY,
                                    }}
                                    monthSelectedText={{
                                    fontFamily: "ProximaNova-Bold",
                                    backgroundColor: BLUE,
                                    color: WHITE,
                                    overflow: 'hidden',
                                    }}
                                    // Styling year selector.
                                    yearMinTintColor={BLUE}
                                    yearMaxTintColor={GREY}
                                    yearText={{
                                    color: BLACK,
                                    }}
                                    />

                                
                                    
                                    
                                </View>
                                <TouchableOpacity onPress={this.saveRoadTax} style={{backgroundColor:'#080880', borderRadius:10, padding:15, paddingLeft:20, paddingRight:20,  alignItems:'center', marginBottom: 10}}>
                                    <Text style={{color:'white', fontFamily: "ProximaNova-Bold"}}>Continue</Text>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => this.setState({modalRoadTax:false})} style={{backgroundColor:'#fff', borderWidth:1, borderColor:'grey', borderRadius:10, padding:15, paddingLeft:20, paddingRight:20,  alignItems:'center'}}>
                                    <Text style={{color:'grey', fontFamily: "ProximaNova-Bold"}}>Cancel</Text>
                                </TouchableOpacity>
                                                
                            </View>
                        }
                        
                    />
                    
                    <BottomModal
                        modalName={this.state.modalPicture}
                        onPress={() => this.setState({modalPicture:false})}
                        style={styles.innerModal}
                        children = {
                        <View >
                            <View style={{padding:20, paddingBottom:0, alignItems:'center'}}>
                                <Text style={{textAlign:'center', fontFamily:'ProximaNova-Bold', fontSize:20, marginBottom:5}}>Update Photo</Text>             
                            </View>

                            <View style={{margin:30, marginTop:20,  alignItems:'center', justifyContent:'space-around', flexDirection:'row'}}>
                                <TouchableOpacity  onPress={this.openCamera} style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                                    <Image source={camera} style={{height:40, width:40}} />
                                    <Text style={{textAlign:'center'}}>Camera</Text>
                                </TouchableOpacity>
                            
                            
                                <TouchableOpacity  onPress={this.openLibrary} style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                                    <Image source={library} style={{height:40, width:40}} />
                                    <Text style={{textAlign:'center'}}>Library</Text>
                                </TouchableOpacity>

                            </View>
                        </View>
                        }
                    />

                    <MiddleModal modalName={this.state.modalCar} 
                        children = {
                            <View >
                                <Text style={{ color:'black', fontFamily: "ProximaNova-Bold", fontSize:30, marginTop:15}}>Heads Up!</Text>
                                <Text style={{marginTop:20, color:'#000080', fontFamily: "ProximaNova-Bold", }}>You are about to remove your vehicle. You will relinquish ownership of all data regarding this vehicle. 
                                </Text>
                                <Text style = {{color:'#e85b1a',fontFamily: "ProximaNova-Bold", marginBottom:15}}>If this vehicle is not reclaimed within 60 days, all data will be lost to you.</Text>
                                <Text style={{fontFamily:'ProximaNova-Bold', color:'grey',marginBottom:30}}>Are you sure? If yes, please enter your vehicle plate number to confirm deletion.</Text>

                                <FormTextInput 
                                    title= 'PLATE NUMBER'
                                    value={this.state.vehicle_plate}
                                    keyboardType={Platform.OS === 'ios' ? 'default' : 'visible-password'}
                                    placeholder= 'eg: ABC1234, XX4578'
                                    onChangeText={this.handleChange}
                                />

                                <View>
                                    
                                    <TouchableOpacity onPress={this.removeVehicle} style={{backgroundColor:'#e85b1a', borderRadius:5, padding:15, marginTop:25, paddingLeft:20, paddingRight:20, marginBottom:15, alignItems:'center'}}>
                                        <Text style={{color:'#fff', fontFamily: "ProximaNova-Bold"}}>Remove my vehicle</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={this.closeModal} style={{borderWidth:1, borderColor:'#686868', borderRadius:5, padding:15, paddingLeft:20, paddingRight:20, alignItems:'center'}}>
                                        <Text style={{color:'#686868', fontFamily: "ProximaNova-Bold"}}>Cancel</Text>
                                    </TouchableOpacity>
                                </View>


                            </View>
                        }
                    />

                           

                </View>
            </SafeAreaView>
        )
    }
}

VehicleProfile = inject('mobx_auth', 'mobx_carkey', 'mobx_config')(observer(VehicleProfile))
export default VehicleProfile

const styles = StyleSheet.create({
    box :{
        backgroundColor:'#fff',
        borderRadius:10,
        marginBottom:15,
        flexDirection:'row',
        alignItems:'center',
        padding:15,
        ...Platform.select({
            ios: {
               shadowColor: '#000',
               shadowOpacity: 0.22,
               shadowRadius: 2.22,
              shadowOffset: { width: 0, height: 1 },
            },
            android: {
              elevation: 3
            },
           }),
    },
    img: { height:Dimensions.get('window').width / 2,backgroundColor:'#fff', width:Dimensions.get('window').width / 2, borderRadius:8, },
    title: {marginBottom:10, color:'#6e6d6d', fontSize:14 },
    titleDesc:{fontFamily: "ProximaNova-Bold", fontSize:16},
    detsButton:{backgroundColor:'#080880', borderRadius:20, padding:10, width: '40%'  },
    cameraShadow: {
        backgroundColor:'#fff',
        padding:10,
        borderRadius:20,
        position:'absolute',
        bottom:15,
        left:10,
        ...Platform.select({
            ios: {
               shadowColor: '#000',
               shadowOpacity: 0.22,
               shadowRadius: 2.22,
              shadowOffset: { width: 0, height: 1 },
            },
            android: {
              elevation: 3
            },
          }),
    },
    innerModal: {     
        borderTopLeftRadius: 10,
        borderTopRightRadius:10,
        width:Dimensions.get('window').width,
        position:'absolute',
        bottom:0,
        backgroundColor: '#fff',
            ...Platform.select({
                ios: {
                    shadowColor: '#000',
                    shadowRadius: 2,
                    shadowOffset: { width: 0, height: 3 },
                    shadowOpacity: 0.6,
                },
                android: {
                    elevation: 2
                },
            }),
    },
    smolCircle: { alignItems:'center', justifyContent:'center',},
    insideC:{width: Dimensions.get('window').width / 9, height:Dimensions.get('window').width / 9,},
    viewTitle: {marginBottom:20, flexDirection:'row', alignItems:'center'},
    smolFont: {fontSize:11,color:'#6e6d6d',},
    bigFont: {fontFamily:'ProximaNova-Bold' },
    smolBox:{ backgroundColor:'white', padding:15, borderRadius:10, marginTop:10,borderWidth:1, borderColor:'#ccc',},
    iconView:{width:40, height:40, marginRight:15, borderRadius:20, backgroundColor:'#efefef', alignItems:'center', justifyContent:'center'},
    icon:{height:30, width:30}
      
})