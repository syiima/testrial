const credentials = {
    "header_auth_beta" : "Basic NmUzMDk3ZTktMzNkZC00NGFjLWIwMWItYThhZTlhZjY0YWMwOg==",
  
    "header_auth_production": "Basic ZDQ4NmQ5NzgtMTRlZC00M2I1LWI2MjEtYTQ0YzQxYzZkYmU5Og==",
  
    "method_sandbox": "https://billplz-staging.herokuapp.com/api/v3/bills",
  
    "method_production": "https://www.billplz.com/api/v3/bills",
  
    "callback_string_sandbox": "https://billplz-staging.herokuapp.com/bills/",
  
    "callback_string_production": "https://www.billplz.com/bills/",
  
    "redirect_url":"https://nav.serv.my/aGeVTGVjeklhTWgHRTUcEdhiQthpOtoFpbWsBMCZ.html",
  
    "unique_KEY": "aGeVTGVjeklhTWgHRTUcEdhiQthpOtoFpbWsBMCZ",
  
    "collectionID_beta": "x2v4u2ko",
  
    "collectionID_production": "bqi2z5ub",
  
    "collectionID_walkin_beta" : "ovm8tudj",
  
    "collectionID_production_walkin" : "pyv0f397",
    
  }
  
  
  export {credentials};
  