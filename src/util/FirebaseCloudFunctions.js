const SERV_USER_BETA = "https://us-central1-servuserplayground.cloudfunctions.net/"
// const SERV_USER_BETA = "https://us-central1-serv-unique-plate-sandbox.cloudfunctions.net/"
const SERV_PROVIDERADMIN_BETA = "https://us-central1-servprovideradmin.cloudfunctions.net/"
const SERV_USER_PRODUCTION = "https://us-central1-serv-v2-production-fcb23.cloudfunctions.net/"
const SERV_PROVIDERADMIN_PRODUCTION = "https://us-central1-servprovider-v2-production.cloudfunctions.net/"



const cloud_serv_user_sandbox = {
  "addVersionIntoDatabase" :  SERV_USER_BETA + "addVersionIntoDatabase",

  "completejobOnUser" :  SERV_USER_BETA + "completejobOnUser",

  "deleteAnnouncement": SERV_USER_BETA + "deleteAnnouncement",

  "deletejobOnUser": SERV_USER_BETA + "deletejobOnUser",

  "fetchUsers": SERV_USER_BETA + "fetchUsers",

  "jobAssigned" :  SERV_USER_BETA + "jobAssigned",

  "newAnnouncement": SERV_USER_BETA + "newAnnouncement",

  "newPromoCode" :  SERV_USER_BETA + "newPromoCode",

  "updateBillPlzBranch" : SERV_USER_BETA + "updateBillPlzBranch",

  "updatePaymentToUnpaid" : SERV_USER_BETA + "updatePaymentToUnpaid",

  "updateQuotationStatus" : SERV_USER_BETA + "updateQuotationStatus",

  "updateBillPlzBranch_workshop" : SERV_USER_BETA + "updateBillPlzBranch_workshop",

  "twilioPhoneVerificationCode" : SERV_USER_BETA + "twilioPhoneVerificationCode",

  "createCSRFU" : SERV_USER_BETA + "createCSRFU",

  "personalNotifications": SERV_USER_BETA + "personalNotifications",

  "directLending" : SERV_USER_BETA + "directLending"

}




const cloud_serv_provideradmin_sandbox = {
  "addRequestData" : SERV_PROVIDERADMIN_BETA + "addRequestData",

  "bookingdetailsbyUser" : SERV_PROVIDERADMIN_BETA + "bookingdetailsbyUser",

  "newAnnouncement" : SERV_PROVIDERADMIN_BETA + "newAnnouncement",

  "newChatAdmin" : SERV_PROVIDERADMIN_BETA + "newChatAdmin",

  "newJobAssigned" : SERV_PROVIDERADMIN_BETA + "newJobAssigned",

  "newJobRequestAvailable" : SERV_PROVIDERADMIN_BETA + "newJobRequestAvailable",

  "promoCodes" : SERV_PROVIDERADMIN_BETA + "promoCodes",

  "requestToDelete" : SERV_PROVIDERADMIN_BETA + "requestToDelete",

  "updateReceipt" : SERV_PROVIDERADMIN_BETA + "updateReceipt",

  "usePromoCodes" : SERV_PROVIDERADMIN_BETA + "usePromoCodes",

  "updateQuotationRead" : SERV_PROVIDERADMIN_BETA + "updateQuotationRead",

  "userDeleteReason" : SERV_PROVIDERADMIN_BETA + "userDeleteReason",

  "fetchAddonServices" : SERV_PROVIDERADMIN_BETA + "fetchAddonServices",

  "createCSRFP" : SERV_PROVIDERADMIN_BETA + "createCSRFP",

  "deleteRequest" : SERV_PROVIDERADMIN_BETA + "deleteRequest"

}

const cloud_serv_user_production = {

  "addVersionIntoDatabase" :  SERV_USER_PRODUCTION + "addVersionIntoDatabase",

  "completejobOnUser" :  SERV_USER_PRODUCTION + "completejobOnUser",

  "deleteAnnouncement": SERV_USER_PRODUCTION + "deleteAnnouncement",

  "deletejobOnUser": SERV_USER_PRODUCTION + "deletejobOnUser",

  "fetchUsers": SERV_USER_PRODUCTION + "fetchUsers",

  "jobAssigned" :  SERV_USER_PRODUCTION + "jobAssigned",

  "newAnnouncement": SERV_USER_PRODUCTION + "newAnnouncement",

  "newPromoCode" :  SERV_USER_PRODUCTION + "newPromoCode",

  "updateBillPlzBranch" : SERV_USER_PRODUCTION + "updateBillPlzBranch",

  "updatePaymentToUnpaid" : SERV_USER_PRODUCTION + "updatePaymentToUnpaid",

  "updateQuotationStatus" : SERV_USER_PRODUCTION + "updateQuotationStatus",

  "updateBillPlzBranch_workshop" : SERV_USER_PRODUCTION + "updateBillPlzBranch_workshop",

  "twilioPhoneVerificationCode" : SERV_USER_PRODUCTION + "twilioPhoneVerificationCode",

  "createCSRFU" : SERV_USER_PRODUCTION + "createCSRFU",

  "personalNotifications" : SERV_USER_PRODUCTION + "personalNotifications",

  "directLending" : SERV_USER_PRODUCTION + "directLending"



}

const cloud_serv_provideradmin_production = {
  "addRequestData" : SERV_PROVIDERADMIN_PRODUCTION + "addRequestData",

  "bookingdetailsbyUser" : SERV_PROVIDERADMIN_PRODUCTION + "bookingdetailsbyUser",

  "newAnnouncement" : SERV_PROVIDERADMIN_PRODUCTION + "newAnnouncement",

  "newChatAdmin" : SERV_PROVIDERADMIN_PRODUCTION + "newChatAdmin",

  "newJobAssigned" : SERV_PROVIDERADMIN_PRODUCTION + "newJobAssigned",

  "newJobRequestAvailable" : SERV_PROVIDERADMIN_PRODUCTION + "newJobRequestAvailable",

  "promoCodes" : SERV_PROVIDERADMIN_PRODUCTION + "promoCodes",

  "requestToDelete" : SERV_PROVIDERADMIN_PRODUCTION + "requestToDelete",

  "updateReceipt" : SERV_PROVIDERADMIN_PRODUCTION + "updateReceipt",

  "usePromoCodes" : SERV_PROVIDERADMIN_PRODUCTION + "usePromoCodes",

  "updateQuotationRead" : SERV_PROVIDERADMIN_PRODUCTION + "updateQuotationRead",

  "userDeleteReason" : SERV_PROVIDERADMIN_PRODUCTION + "userDeleteReason",

  "createCSRFP" : SERV_PROVIDERADMIN_PRODUCTION + "createCSRFP",


  "deleteRequest" : SERV_PROVIDERADMIN_PRODUCTION + "deleteRequest"


}


export {cloud_serv_user_sandbox, cloud_serv_provideradmin_sandbox, cloud_serv_user_production, cloud_serv_provideradmin_production};
