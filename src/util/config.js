const CONFIG = 1;
const VERSION =  '5.2.0';

//We follow the version code at android
const CONSTV = 211;


const X = {config:CONFIG, version:VERSION, constv: CONSTV};
// if 1 = production
// if 0 = beta
export {X} ;
