import { observable, action, computed, toJS , decorate} from "mobx";

class Car_Key{
  CarKey = '';
  MAKE = ''
  MODEL = ''
  CARCC = ''
  TRANSMISSION = ''
  YEAR = ''
  PLATE = ''

  setCarKey(CarKey){
    this.CarKey = CarKey;
  }
  setMAKE(MAKE) {
    this.MAKE = MAKE;
  }
  setMODEL(MODEL) {
    this.MODEL = MODEL;
  }
  setCARCC(CARCC) {
    this.CARCC = CARCC;
  }
  setTRANSMISSION(TRANSMISSION) {
    this.TRANSMISSION = TRANSMISSION;
  }
  setYEAR(YEAR) {
    this.YEAR = YEAR;
  }
  setPLATE(PLATE) {
    this.PLATE = PLATE;
  }
  setVTYPE(VTYPE){
    this.VTYPE = VTYPE;
  }


}

export default decorate(Car_Key, {
  CarKey: observable,
  setCarKey: action,
  MAKE: observable,
  setMAKE: action,
  MODEL: observable,
  setMODEL: action,
  CARCC: observable,
  setCARCC: action,
  TRANSMISSION: observable,
  setTRANSMISSION: action,
  YEAR: observable,
  setYEAR: action,
  PLATE: observable,
  setPLATE: action,
  VTYPE: observable,
  setVTYPE: action,
});
