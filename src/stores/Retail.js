import { observable, action, computed, toJS , decorate} from "mobx";

class Retail{
  STATUS = ''
  R_WALKIN_ID = ''
  R_GROUPID = ''
  R_ID = ''
  acquirementId = ''

  setSTATUS(STATUS) {
    this.STATUS =STATUS;
  }
  setR_WALKIN_BOOKID(R_WALKIN_BOOKID) {
    this.R_WALKIN_BOOKID = R_WALKIN_BOOKID;
  }
  setR_GROUPID(R_GROUPID) {
    this.R_GROUPID = R_GROUPID;
  }
  setR_ID(R_ID) {
    this.R_ID = R_ID;
  }

  setacquirementId(acquirementId) {
    this.acquirementId = acquirementId;
  }
  setADDRESS(ADDRESS) {
    this.ADDRESS = ADDRESS;
  }
  setNAMEPLACE(NAMEPLACE) {
    this.NAMEPLACE = NAMEPLACE;
  }




}

export default decorate(Retail, {
  STATUS: observable,
  setSTATUS: action,
  R_WALKIN_BOOKID: observable,
  setR_WALKIN_BOOKID: action,
  R_GROUPID: observable,
  setR_GROUPID: action,
  R_ID: observable,
  setR_ID: action,
  acquirementId: observable,
  setacquirementId: action,
  ADDRESS: observable,
  setADDRESS: action,
  NAMEPLACE: observable,
  setNAMEPLACE: action

});
