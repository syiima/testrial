import { observable, action, computed, toJS , decorate} from "mobx";

class Auth {
  check = 0;
  FUID = '';
  profile = {};
  NAME = ''
  EMAIL = ''
  PHONE = ''
  Noti = false
  REFC = ''

  setCheck(check) {
    this.check = check;
  }
  setFUID(FUID) {
    this.FUID = FUID;
  }
  setProfile(profile) {
    this.profile = profile;
  }
  setNAME(NAME) {
    this.NAME = NAME;
  }
  setEMAIL(EMAIL) {
    this.EMAIL = EMAIL;
  }
  setPHONE(PHONE) {
    this.PHONE = PHONE;
  }
  setNoti(Noti) {
    this.Noti = Noti;
  }
  setREFC(REFC){
    this.REFC = REFC
  }

}
export default decorate(Auth, {
  check: observable,
  setCheck: action,
  FUID: observable,
  setFUID: action,
  profile: observable,
  setProfile: action,

  NAME: observable,
  setNAME: action,
  EMAIL: observable,
  setEMAIL: action,
  PHONE: observable,
  setPHONE: action,
  Noti: observable,
  setNoti: action,
  REFC : observable,
  setREFC: action
});
