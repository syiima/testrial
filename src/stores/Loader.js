import { observable, action, computed, toJS , decorate} from "mobx";

class Loader {
  loading = true;

  setLoading(loading) {
    this.loading = loading;
  }

}
export default decorate(Loader, {
  loading: observable,
  setLoading: action
});
