import Auth from './Auth.js';
import Loader from './Loader.js';
import Config from './Config.js';
import Car_Key from './CarKey.js';
import Retail from './Retail.js';

export default {
  mobx_auth: new Auth(),
  mobx_loader: new Loader(),
  mobx_config: new Config(),
  mobx_carkey: new Car_Key(),
  mobx_retail: new Retail(),
}
