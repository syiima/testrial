import { observable, action, computed, toJS , decorate} from "mobx";
import {X} from '../util/config'

class Config{
  config = 0;
  Version = X.version;
  constV = X.constv;
  DB = 1

  setConfig(config){
    this.config = config;
  }
  setDB(DB){
    this.DB = DB;
  }

}

export default decorate(Config, {
  config: observable,
  setConfig: action,
  Version: observable,
  constV: observable,
  DB: observable,
  setDB: action
});
