import React, {Component} from 'react';

import MainAppNavigator from './src/navigators/MainAppNavigator.js';
import { Provider } from 'mobx-react';
import stores from "./src/stores";

import firebase from '@react-native-firebase/app';
import '@react-native-firebase/auth';
import '@react-native-firebase/database';
import '@react-native-firebase/storage';
import '@react-native-firebase/functions';
import '@react-native-firebase/messaging';
import '@react-native-firebase/analytics';

class App extends React.Component{
  render() {
    return (
      <Provider {...stores}>
        <MainAppNavigator/>
       </Provider>
    );
  }
}

export default App;
