/** @format */

import {AppRegistry} from 'react-native';
import App from './App.js';
import {name as appName} from './app.json';
import { typography } from './src/util/typography.js'

typography()
AppRegistry.registerComponent(appName, () => App);

